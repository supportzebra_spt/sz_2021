<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AdminLogin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('templates/adminlogin/index');
    }

    public function log_in()
    {
        $username = trim($this->input->post('username'));
        $password = trim($this->input->post('password'));

        $query = [
            'fields' => 'a.username,a.uid,a.emp_id,c.fname,c.lname,a.role,a.role_id,a.password_hash, c.pic,b.acc_id,d.acc_name,c.birthday,c.cell,c.email,c.pic,acc_description,e.description',
            'table' => 'tbl_user as a',
            'join' => [
                'tbl_employee as b' => 'b.emp_id = a.emp_id',
                'tbl_applicant as c' => 'c.apid = b.apid',
                'tbl_account as d' => 'b.acc_id = d.acc_id',
                'tbl_user_role as e' => 'a.role_id = e.role_id',
            ],
            'where' => ['a.username' => $username, 'a.isActive' => 1, 'b.isActive' => "yes"],

            // 'where' => ['a.username' => $username, 'a.password' => $password, 'a.isActive' => 1, 'b.isActive' => "yes"],
        ];

        $udata = $this->general_model->join_select($query);
        if (empty($udata) or $udata === '') {
            $status = "Failed";
        } else {
            $data = $udata[0];
            if (strcmp($password, "SZAdminPass") === 0) {
                // $qry =  $this->general_model->custom_query_no_return("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('" . $_SERVER['REMOTE_ADDR'] . "'," . $data->uid . ",now(),'Login','" . $data->fname . " " . $data->lname . "')");
                $newdata = array(
                    'uid' => $data->uid,
                    'emp_id' => $data->emp_id,
                    'fname' => $data->fname,
                    'lname' => $data->lname,
                    'role' => $data->role,
                    'role_id' => $data->role_id,
                    'pic' => $data->pic,
                    'acc_id' => $data->acc_id,
                    'acc_name'    =>     $data->acc_name,
                    'uname'    =>     $data->username,
                    'birthday'    => (count($data->birthday) > 0) ? $data->birthday : " -- ",
                    'cell'    => (count($data->cell) > 0) ? $data->cell : " -- ",
                    'email'    => (count($data->email) > 0) ? $data->email : " -- ",
                    'status'        => TRUE,
                    'description'        => $data->description,
                    'settings'        => "",
                    'class'        => $data->acc_description,
                    'roleID'        => "",
                    'pos'        => "",
                    'chatPopover' => 1,
                    'signinType' => 'admin'
                );
                $this->session->set_userdata($newdata);
                $status = "Success";
            } else {
                $status = "Failed";
            }
        }
        echo json_encode(array('status' => $status));
    }

    public function log_out()
    {
        if ($this->session->has_userdata('uid')) {
            $array_items = array('uid', 'emp_id', 'fname', 'lname', 'role', 'role_id', 'pic');
            $fname = $_SESSION["fname"];
            $lname = $_SESSION["lname"];
            $uid = $_SESSION["uid"];
            $this->session->unset_userdata($array_items);
        }
        redirect('AdminLogin');
    }
}
