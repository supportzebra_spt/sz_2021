<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Faqs extends General
{

    protected $title = 'Faqs';

    public function __construct()
    {
        parent::__construct();
    }

    public function faqs_v2()
    {
        $data = [
            'uri_segment'   => $this->uri->segment_array(),
            'title'         => 'FAQs',
        ];
        $this->load_template_view('templates/faqs/faqs_v2', $data);
    }
    public function faqs_v2_view()
    {

        $query = '';
        $id = '';
        $visible = '';


        if ($this->input->post('searchText')) {
            $searchText = $this->input->post('searchText');
            $faqs_1 = " AND faqs.title LIKE '%$searchText%'";
        } else {
            $faqs_1 = '';
        }
        if ($this->input->post('category') != 0) {
            $category = $this->input->post('category');
            $faqs_2 = " AND faqs.faqs_category_id = $category";
        } else {
            $faqs_2 = '';
        }
        if ($this->input->post('visible') == 2) {
            $visible = '';
            $faqs_3 = "";
        } else if ($this->input->post('visible') == 1) {
            $visible = 1;
            $faqs_3 = " AND faqs.isDisplay = $visible";
        } else {
            $visible = 0;
            $faqs_3 = " AND faqs.isDisplay = $visible";
        }
        $emp_id = $this->session->emp_id;
        $faqsQuery = "
        SELECT faqs.faqs_id, faqs.isDisplay, faqs.faqs_category_id, faqs.title, faqs.details
        FROM tbl_faqs faqs, tbl_faqs_category cat
        LEFT JOIN tbl_distro dis ON find_in_set($emp_id, dis.list_emp_id)
        WHERE cat.isDisplay != 0 
        AND faqs.isDisplay != 2
        AND faqs.faqs_category_id = cat.faqs_category_id 
        AND (($emp_id=faqs.emp_id) OR 
        (find_in_set($emp_id,faqs.list_access)) OR 
        (find_in_set($emp_id,faqs.list_emp_id) AND faqs.isDisplay=1) OR
        ($emp_id=cat.emp_id) OR ($emp_id=184) OR 
        ((find_in_set($emp_id,cat.list_access)) AND ((faqs.list_emp_id is null) OR !find_in_set($emp_id,faqs.list_emp_id))) OR 
        (find_in_set($emp_id,cat.list_emp_id) AND faqs.isDisplay=1) OR
        (find_in_set($emp_id,dis.list_emp_id) AND find_in_set(dis.distro_id,cat.list_group_access) AND (faqs.list_emp_id is null OR !find_in_set($emp_id,faqs.list_emp_id))) OR
        (find_in_set($emp_id,dis.list_emp_id) AND find_in_set(dis.distro_id,cat.list_group_emp_id) AND faqs.isDisplay=1))
        $faqs_1 $faqs_2 $faqs_3
        GROUP BY faqs.faqs_id
        order by faqs.isDisplay ASC";
        $faqs = $this->general_model->custom_query($faqsQuery);
        $faqsId = array_column($faqs, 'faqs_category_id');
        $acc_id = $this->session->acc_id;
        $emp_id = $this->session->emp_id;

        $catQuery = "   SELECT cat.list_emp_id, cat.faqs_category_id, cat.faqs_category_name
                        FROM tbl_faqs_category cat
                        WHERE cat.isDisplay = 1";

        $faqs = $this->general_model->custom_query($faqsQuery);

        $cat = $this->general_model->custom_query($catQuery);


        foreach ($cat as $c) {
            $catArray = array();
            foreach ($faqs as $f) {
                if ($c->faqs_category_id == $f->faqs_category_id) {

                    array_push($catArray, $f);
                }
            }
            $c->faqs = $catArray;
        }
        echo json_encode($cat);
    }
    public function faqs_details($id)
    {
        $emp_id = $this->session->emp_id;
        $faq_qry = 
        "SELECT faqs.faqs_id, faqs.isDisplay, faqs.faqs_category_id, faqs.title, faqs.details
        FROM tbl_faqs faqs, tbl_faqs_category cat
        LEFT JOIN tbl_distro dis ON find_in_set($emp_id, dis.list_emp_id)
        WHERE cat.isDisplay != 0 
        AND faqs.isDisplay != 2
        AND faqs.faqs_category_id = cat.faqs_category_id 
        AND (($emp_id=faqs.emp_id) OR 
        (find_in_set($emp_id,faqs.list_access)) OR 
        (find_in_set($emp_id,faqs.list_emp_id) AND faqs.isDisplay=1) OR
        ($emp_id=cat.emp_id) OR ($emp_id=184) OR 
        ((find_in_set($emp_id,cat.list_access)) AND ((faqs.list_emp_id is null) OR !find_in_set($emp_id,faqs.list_emp_id))) OR 
        (find_in_set($emp_id,cat.list_emp_id) AND faqs.isDisplay=1) OR
        (find_in_set($emp_id,dis.list_emp_id) AND find_in_set(dis.distro_id,cat.list_group_access) AND (faqs.list_emp_id is null OR !find_in_set($emp_id,faqs.list_emp_id))) OR
        (find_in_set($emp_id,dis.list_emp_id) AND find_in_set(dis.distro_id,cat.list_group_emp_id) AND faqs.isDisplay=1))
        GROUP BY faqs.faqs_id";
        // $isDisplay = $this->general_model->fetch_specific_val("faqs_id", "faqs_id=$id AND isDisplay=1 AND (emp_id=$emp_id OR find_in_set($emp_id,list_access))", "tbl_faqs");
        $isDisplay = $this->general_model->custom_query($faq_qry);
        if($isDisplay){
            $data = [
            'id'            => $id,
            'uri_segment'   => $this->uri->segment_array(),
            'title'         => 'FAQs Details',
        ];
        $this->load_template_view('templates/faqs/faqs_details', $data);
        } else {
            $this->error_403();
        }
    }
//-------------------- START CONTROLLER GENERAL --------------------//

    public function checkEmployee()
    {
        $emp_id = $this->session->emp_id;
        $qry = 
        "SELECT app.lname, app.fname, app.mname, app.pic, emp.emp_id, dep.dep_name, acc.acc_id, acc.acc_name, acc.acc_description, user.uid
        FROM tbl_applicant app, tbl_employee emp, tbl_account acc, tbl_department dep, tbl_user user
        WHERE emp.isActive = 'yes' AND app.apid = emp.apid AND emp.acc_id = acc.acc_id AND acc.dep_id = dep.dep_id AND user.emp_id = emp.emp_id AND $emp_id!=emp.emp_id
        ORDER BY acc.acc_name ASC, app.lname ASC";
        $data['emp'] = $this->general_model->custom_query($qry);
        $data['dis'] = $this->general_model->fetch_specific_vals("distro_id, distro_name", "isDisplay=1", "tbl_distro", "distro_name ASC");
        echo json_encode($data);
    }

    public function access()
    {
        $emp_id = $this->session->emp_id;
        $uri = 'Faqs/faqs_server';
        $user_id = $this->session->userdata('uid');
        $current_uri = $uri==null?$this->uri->uri_string():$uri;
        $uri_array = explode("/", $current_uri);
        if($uri_array[0] == 'discipline'){
            $current_uri = $uri_array[0].'/'.$uri_array[1];
        }
        $fields = "userAccess.useraccess_id";
        $where = "userAccess.menu_item_id = menuItems.menu_item_id AND menuItems.item_link LIKE '%" . $current_uri . "%' AND userAccess.user_id = $user_id AND is_assign =1";
        $tables = "tbl_user_access userAccess, tbl_menu_items menuItems";
        $record = $this->general_model->fetch_specific_val($fields, $where, $tables);
        $access = (count($record) > 0) ? 1 : 0;
        echo json_encode($access);
    }

    public function isEditor()
    {
        $emp_id = $this->session->emp_id;
        $faqs_id = $this->input->post('faqs_id');
        $qry = 
        "SELECT faqs.faqs_id, faqs.isDisplay, faqs.emp_id faqs_emp_id, app.lname, app.fname, app.mname, faqs.list_emp_id faqs_list_emp_id, faqs.list_access faqs_list_access, sub.faqs_category_id, sub.relationship, sub.emp_id sub_emp_id, sub.list_access sub_list_access, sub.list_emp_id sub_list_emp_id, sub.list_group_access, sub.list_group_emp_id, cat.emp_id cat_emp_id,cat.list_access cat_list_access, cat.list_emp_id cat_list_emp_id
        FROM tbl_faqs faqs, tbl_employee emp, tbl_applicant app, tbl_faqs_category sub
        LEFT JOIN tbl_faqs_category cat ON sub.relationship=cat.faqs_category_id
        WHERE faqs.faqs_id=$faqs_id AND faqs.emp_id = emp.emp_id AND emp.apid = app.apid AND sub.faqs_category_id = faqs.faqs_category_id";
        $check = $this->general_model->custom_query($qry);
        // $check = $this->general_model->fetch_specific_val(
        //     "faqs.faqs_id, faqs.isDisplay", 
        //     "faqs.faqs_id=$faqs_id AND faqs.faqs_category_id = cat.faqs_category_id AND (faqs.emp_id=$emp_id OR find_in_set($emp_id,faqs.list_access) OR cat.emp_id=$emp_id OR (find_in_set($emp_id,cat.list_access) AND NOT find_in_set($emp_id,faqs.list_emp_id)))", "tbl_faqs faqs, tbl_faqs_category cat");
        echo json_encode($check);
    }

    private function access_level_editor($id) 
    {
       $emp_id = $this->session->emp_id;
        $faq_qry = 
        "SELECT faqs.faqs_id
        FROM tbl_faqs faqs, tbl_faqs_category cat
        LEFT JOIN tbl_distro dis ON find_in_set($emp_id, dis.list_emp_id)
        WHERE cat.isDisplay != 0 
        AND faqs.faqs_id=$id
        AND faqs.isDisplay != 2
        AND faqs.faqs_category_id = cat.faqs_category_id 
        AND (($emp_id=faqs.emp_id) OR 
        (find_in_set($emp_id,faqs.list_access)) OR 
        ($emp_id=cat.emp_id) OR 
        ((find_in_set($emp_id,cat.list_access)) AND ((faqs.list_emp_id is null) OR !find_in_set($emp_id,faqs.list_emp_id))) OR 
        (find_in_set($emp_id,dis.list_emp_id) AND find_in_set(dis.distro_id,cat.list_group_access) AND (faqs.list_emp_id is null OR !find_in_set($emp_id,faqs.list_emp_id)) AND !find_in_set(dis.distro_id,cat.list_group_emp_id)))
        GROUP BY faqs.faqs_id";
        $isDisplay = $this->general_model->custom_query($faq_qry);
        if($isDisplay){
            return 1;
        } else {
            return 0;
        }
    }


    public function list_of_employee()
    {
        $list_emp_id = ($this->input->post('list_emp_id') != '') ? $this->input->post('list_emp_id') : 0;
        $distro_id = ($this->input->post('distro_id') != '') ? $this->input->post('distro_id') : 0;
        $emp =        
        'SELECT app.lname, app.fname, app.mname, acc.acc_name, emp.emp_id, GROUP_CONCAT(dis.distro_name) distro_name, if(find_in_set(emp.emp_id, "'.$list_emp_id.'"), 1, 0) checked
        FROM tbl_employee emp
        INNER JOIN tbl_applicant app ON app.apid = emp.apid
        INNER JOIN tbl_account acc ON acc.acc_id = emp.acc_id
        LEFT JOIN tbl_distro dis ON find_in_set(emp.emp_id, dis.list_emp_id) AND dis.isDisplay = 1
        WHERE emp.isActive = "yes"
        GROUP BY emp.emp_id
        ORDER BY checked DESC, app.lname ASC';
        $acc = 
        "SELECT acc.acc_id, acc.acc_name, GROUP_CONCAT(emp.emp_id) list_emp_id
        FROM tbl_employee emp
        INNER JOIN tbl_account acc ON acc.acc_id = emp.acc_id
        GROUP BY acc.acc_id";
        $dis = 
        "SELECT distro_id, distro_name, if(find_in_set(distro_id,'$distro_id'),1,0) checked
        FROM tbl_distro WHERE isDisplay = 1
        ORDER BY distro_name ASC";
        $qry['emp'] = $this->general_model->custom_query($emp);
        $qry['dis'] = $this->general_model->custom_query($dis);
        // $qry['dis'] = $this->general_model->fetch_all("distro_id, distro_name, if(find_in_set(distro_id,'$distro_id'),1,0) checked", "tbl_distro", "distro_name ASC");
        echo json_encode($qry);
    }

    public function list_of_category()
    {
        $emp_id = $this->session->emp_id;
        $selected = $this->input->post('selected');
        $category_id = $this->input->post('category_id');
        $category_id = ($category_id) ? $category_id : 0;
        if($selected === 'cat_only') {
            $qry =      
            "SELECT cat.faqs_category_id, cat.faqs_category_name, cat.relationship, if(cat.faqs_category_id=$category_id, 1, 0) as selected 
            FROM tbl_faqs_category cat
            WHERE ((cat.emp_id = $emp_id) OR (find_in_set($emp_id,cat.list_access))) 
            AND cat.isDisplay = 1
            GROUP BY cat.faqs_category_id
            ORDER BY selected DESC ,cat.faqs_category_name ASC";
        } 
        else if($selected === 'filter') {
            $qry =      
            "SELECT sub.faqs_category_id, sub.faqs_category_name
            FROM tbl_faqs_category sub
            LEFT JOIN tbl_faqs_category cat ON sub.relationship=cat.faqs_category_id
            WHERE 
            ((sub.emp_id = $emp_id) OR 
            (cat.emp_id = $emp_id) OR
            (find_in_set($emp_id,cat.list_access)) OR
            (find_in_set($emp_id,cat.list_emp_id)) OR
            (find_in_set($emp_id,sub.list_emp_id)) OR
            (find_in_set($emp_id,sub.list_access))) AND
            sub.isDisplay = 1
            ORDER BY sub.faqs_category_name ASC";
        } 
        if($qry){
            $data = $this->general_model->custom_query($qry);
        }
        
        echo json_encode($data);
    }

    public function checkList()
    {
        $data = $this->general_model->fetch_all("acc_name, acc_id", "tbl_account", "acc_name ASC");
        echo json_encode($data);
    }

    public function checkId()
    {
        $id = $this->input->post('id');
        $isDisplay = $this->input->post('isDisplay');
        $data = array(
            'isDisplay'  =>  $isDisplay,
        );
        $query = $this->general_model->update_vals($data, "faqs_id =$id", "tbl_faqs");
        echo json_encode($query);
    }

    function general_upload_photo()
    {
        if (isset($_FILES["image"]["name"])) {
            $config['upload_path'] = './uploads/faqs/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image')) {
                $this->upload->display_errors();
                return FALSE;
            } else {
                $data = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = './uploads/faqs/' . $data['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '100%';
                // $config['width'] = 800;
                // $config['height'] = 800;
                $config['new_image'] = './uploads/faqs/' . $data['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo base_url() . 'uploads/faqs/' . $data['file_name'];
            }
        }
    }

    public function get_number_employee()
    {
        $qry = 'SELECT COUNT(emp.emp_id) as count, acc.acc_id, GROUP_CONCAT(emp.emp_id) as emp_id FROM tbl_employee emp, tbl_account acc WHERE emp.isActive = "yes" AND emp.acc_id = acc.acc_id GROUP BY acc.acc_id ORDER BY acc.acc_name';
        $data = $this->general_model->custom_query($qry);
        echo json_encode($data);
    }

    

    function delete_image()
    {
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if (unlink($file_name)) {
            echo 'File Delete Successfully';
        }
    }

 //-------------------- END CONTROLLER GENERAL --------------------//




//-------------------- START CONTROLLER PAGE --------------------//

    public function faqs_category_page()
    {
        $data = [
            'category'      => $this->general_model->fetch_specific_vals('c.faqs_category_id, c.faqs_category_name, c.list_access, c.list_emp_id', 'c.isDisplay = 1 AND c.relationship = 0', 'tbl_faqs_category c', 'c.faqs_category_name ASC'),
            'uri_segment'   => $this->uri->segment_array(),
            'title'         => 'Category',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/faqs/faqs_category', $data);
        }
    }

    public function faqs_create_page()
    {
        $emp_id = $this->session->emp_id;
        $acc_id = $this->session->acc_id;
        $data = [
            'category'     => $this->general_model->fetch_specific_vals("faqs_category_name, faqs_category_id", "$emp_id = emp_id AND isDisplay = 1", "tbl_faqs_category", "faqs_category_name ASC"),
            'names'        => $this->general_model->fetch_specific_vals("app.lname, app.fname, app.mname, emp.emp_id", "emp.apid = app.apid AND emp.isActive = 'yes'", "tbl_applicant app, tbl_employee emp", "app.lname ASC"),
            'uri_segment'  => $this->uri->segment_array(),
            'title' => 'FAQ Create',
        ];
        if ($this->check_access('Faqs/faqs_server_page')) {
            $this->load_template_view('templates/faqs/faqs_create', $data);
        }
    }

    public function faqs_custom_group_page()
    {
        $data = [
            'uri_segment'   => $this->uri->segment_array(),
            'title'         => 'FAQ Custom Page',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/faqs/faqs_custom_group', $data);
        }
    }

    public function faqs_details_monitor_page($id)
    {
        $id = $id;
        $data = [
            'id'            =>   $id,
            'uri_segment'   => $this->uri->segment_array(),
            'title'         => 'FAQ Details',
        ];
        if ($this->check_access('Faqs/faqs_monitoring_page')) {
            $this->load_template_view('templates/faqs/faqs_details_monitoring', $data);
        }
    }

    public function faqs_details_server_page($id)
    {
        $id = $id;
        $emp_id = $this->session->emp_id;
        $acc_id = $this->session->acc_id;
        // $qry = $this->general_model->fetch_specific_val(
        //     "faqs.faqs_id, faqs.emp_id,",
        //     "cat.isDisplay = 1 AND faqs.isDisplay != 2 AND $id=faqs.faqs_id AND faqs.faqs_category_id = cat.faqs_category_id",
        //     "tbl_faqs faqs, tbl_faqs_category cat"
        // );
        // if ($qry) {
            $data = [
                // 'isEditor'      => $qry->emp_id == $emp_id?$emp_id:'',
                'id'            =>   $id,
                'uri_segment'   => $this->uri->segment_array(),
                'title'         => 'FAQ Details',
            ];
            if ($this->check_access('Faqs/faqs_server_page')) {
                $this->load_template_view('templates/faqs/faqs_details_server', $data);
            }
        // }
        //  else {
        //     $this->error_403();
        // }
    }

    public function faqs_edit_page($id)
    {
        if($this->access_level_editor($id)){
            $id = $id;
            $list = $this->general_model->fetch_specific_val("list_emp_id", "faqs_id=$id", "tbl_faqs");
            $qry = "SELECT app.lname, app.fname, app.mname, emp.emp_id, if(find_in_set(emp.emp_id, '$list->list_emp_id'), 1, 0) as selected 
            FROM tbl_applicant app, tbl_employee emp
            WHERE emp.apid = app.apid AND emp.isActive = 'yes'  
            ORDER BY selected  DESC, app.lname ASC";
            $emp_id = $this->session->emp_id;
            $data = [
                'id'            => $id,
                'names'         => $this->general_model->custom_query($qry),
                'category'      => $this->general_model->fetch_specific_vals("faqs_category_name, faqs_category_id", "$emp_id = emp_id", "tbl_faqs_category", "faqs_category_name ASC"),
                'uri_segment'   => $this->uri->segment_array(),
                'title'         => 'FAQ Update',
            ];
            if ($this->check_access('Faqs/faqs_server_page')) {
                $this->load_template_view('templates/faqs/faqs_update', $data);
            }
        }else{
            $this->error_403();
        }
    	
    }

    public function faqs_history_logs_page()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'FAQ History',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/faqs/faqs_history_logs', $data);
        }
    }

    public function faqs_monitoring_page()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'FAQ Monitoring',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/faqs/faqs_monitoring', $data);
        }
    }

    public function faqs_server_page()
    {
        $data = [
            'uri_segment' => $this->uri->segment_array(),
            'title' => 'FAQ Server',
        ];
        if ($this->check_access()) {
            $this->load_template_view('templates/faqs/faqs_server', $data);
        }
    }

//-------------------- END CONTROLLER PAGE --------------------//
    



//-------------------- START CONTROLLER DATA --------------------//

    public function faqs_category_data()
    {
        $empId = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        // $query['query'] = "SELECT cat.faqs_category_id, cat.faqs_category_name, cat.list_emp_id, cat.list_access, rel.faqs_category_name as relation, cat.relationship
        // FROM tbl_faqs_category cat
        // LEFT JOIN tbl_faqs_category rel ON rel.faqs_category_id = cat.relationship
        // WHERE cat.faqs_category_id is not null AND cat.isDisplay = 1 AND (cat.emp_id=$empId OR $empId = 184 OR find_in_set($empId, cat.list_access))";
        $query['query'] = "
        SELECT cat.faqs_category_id, cat.faqs_category_name, cat.list_access, cat.list_emp_id, cat.list_group_access, cat.list_group_emp_id, cat.emp_id, (SELECT GROUP_CONCAT(sub.faqs_category_name) FROM tbl_faqs_category sub WHERE sub.relationship=cat.faqs_category_id AND sub.isDisplay = 1) sub_name FROM tbl_faqs_category cat where relationship = 0 AND isDisplay = 1";
        // $query['search']['append']="";
        // $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = $datatable['query']['searchField'];
            $query['search']['append'] = " AND cat.faqs_category_name LIKE '%$keyword%'";
            $query['search']['total'] = " AND cat.faqs_category_name LIKE '%$keyword%'";
        }
        // if ($datatable['query']['searchCategory'] != '') {
        //     $cat = $datatable['query']['searchCategory'];
        //     $query['search']['append'] .= " AND ((cat.faqs_category_id = $cat) OR cat.relationship = $cat)";
        //     $query['search']['total'] .= " AND ((cat.faqs_category_id = $cat) OR cat.relationship = $cat)";
        // }
        // if ($datatable['query']['searchType'] != '') {
        //     if($datatable['query']['searchType'] == 'main'){
        //         $query['search']['append'] .= " AND cat.relationship = 0";
        //         $query['search']['total'] .= " AND cat.relationship = 0";
        //     } else {
        //         $query['search']['append'] .= " AND cat.relationship != 0";
        //         $query['search']['total'] .= " AND cat.relationship != 0";
        //     }
        // }
        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';


        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .=  $order . ' ' . $sort . $limit;
        }
        $acc = $this->general_model->fetch_all("acc.acc_id, acc.acc_name", "tbl_account acc");
        $data = $this->general_model->custom_query($query['query']);
        // var_dump($data);
        $data_encode = ($data[0]->faqs_category_id) ? $data : '';
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }

    public function faqs_custom_group_data()
    {
        $empId = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        $query['query'] = "SELECT dis.distro_id, dis.distro_name, dis.list_emp_id, dis.list_group_emp_id, dis.emp_id, app.lname, app.fname, app.mname, dis.timeUpdate
        FROM tbl_distro dis, tbl_applicant app, tbl_employee emp
        WHERE isDisplay = 1 AND app.apid = emp.apid AND emp.emp_id = dis.emp_id";
        if ($datatable['query']['searchField'] != '') {
            $keyword = $datatable['query']['searchField'];
            $query['search']['append'] = " AND distro_name LIKE '%$keyword%'";
            $query['search']['total'] = " AND distro_name LIKE '%$keyword%'";
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';


        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $acc = $this->general_model->fetch_all("acc.acc_id, acc.acc_name", "tbl_account acc");
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }

    public function faqs_history_logs_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT h.faqs_history_id, h.faqs_id, h.type, h.time, h.updatedBy, a.lname, a.fname, a.mname, f.title
        FROM tbl_faqs_history h, tbl_applicant a, tbl_employee e, tbl_faqs f
        WHERE a.apid = e.apid 
        AND e.emp_id = h.updatedBy
        AND f.faqs_id = h.faqs_id";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = $datatable['query']['searchField'];
            $query['search']['append'] .= " AND ((f.title LIKE '%$keyword%') OR (a.lname LIKE '%$keyword%') OR (a.fname LIKE '%$keyword%') OR (a.mname LIKE '%$keyword%'))";
            $query['search']['total'] .= " AND ((f.title LIKE '%$keyword%') OR (a.lname LIKE '%$keyword%') OR (a.fname LIKE '%$keyword%') OR (a.mname LIKE '%$keyword%'))";
        }
        if ($datatable['query']['searchType'] != '') {
            $type = $datatable['query']['searchType'];
            $query['search']['append'] .= " AND h.type = $type";
            $query['search']['total'] .= " AND h.type = $type";
        }
        if ($datatable['query']['start'] != '' && $datatable['query']['end'] != '') {
            $start = $datatable['query']['start'];
            $end = $datatable['query']['end'];
            if($start != $end){
                $query['search']['append'] .= " AND h.time BETWEEN '$start' and '$end'";
                $query['search']['total'] .= " AND h.time BETWEEN '$start' and '$end'";
            }else{
                $query['search']['append'] .= " AND h.time LIKE '%$start%'";
                $query['search']['total'] .= " AND h.time LIKE '%$start%'";
            }
            
        }

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';


        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }

    public function faqs_monitoring_data()
    {
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT 
        faqs.faqs_id, 
        faqs.list_emp_id,
        cat.faqs_category_name,
        app.lname, 
        app.fname, 
        app.mname, 
        faqs.title, 
        faqs.details,
        faqs.isDisplay,
        faqs.timeUpdate,
        faqs.emp_id
        FROM 
        tbl_faqs faqs, 
        tbl_faqs_category cat, 
        tbl_employee emp, 
        tbl_applicant app
        WHERE 
        faqs.faqs_category_id = cat.faqs_category_id AND 
        emp.emp_id = faqs.emp_id AND
        emp.apid = app.apid";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = $datatable['query']['searchField'];
            $query['search']['append'] .= " AND ((faqs.title LIKE '%$keyword%') OR (app.lname LIKE '%$keyword%') OR (app.fname LIKE '%$keyword%') OR (app.mname LIKE '%$keyword%'))";
            $query['search']['total'] .= " AND ((faqs.title LIKE '%$keyword%') OR (app.lname LIKE '%$keyword%') OR (app.fname LIKE '%$keyword%') OR (app.mname LIKE '%$keyword%'))";
        }
        if ($datatable['query']['searchType'] != '') {
            $type = $datatable['query']['searchType'];
            $query['search']['append'] .= " AND faqs.isDisplay = $type";
            $query['search']['total'] .= " AND faqs.isDisplay = $type";
        }
        if ($datatable['query']['start'] != '' && $datatable['query']['end'] != '') {
            $start = $datatable['query']['start'];
            $end = $datatable['query']['end'];
            if($start != $end){
                $query['search']['append'] .= " AND date(faqs.timeUpdate) BETWEEN '$start' and '$end'";
                $query['search']['total'] .= " AND date(faqs.timeUpdate) BETWEEN '$start' and '$end'";
            }else{
                $query['search']['append'] .= " AND date(faqs.timeUpdate) LIKE '%$start%'";
                $query['search']['total'] .= " AND date(faqs.timeUpdate) LIKE '%$start%'";
            }
        }
        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }

    public function faqs_server_data()
    {
        $emp_id = $this->session->emp_id;
        $datatable = $this->input->post('datatable');
        $query['query'] = "
        SELECT faqs.faqs_id, faqs.title, faqs.isDisplay, sub.faqs_category_name
        FROM tbl_faqs faqs
        LEFT JOIN tbl_faqs_category sub ON sub.faqs_category_id = faqs.faqs_category_id
        LEFT JOIN tbl_faqs_category cat on sub.relationship = cat.faqs_category_id
        WHERE faqs.isDisplay != 2 AND sub.isDisplay != 0 AND
        ((faqs.emp_id = $emp_id) OR 
        (find_in_set($emp_id,faqs.list_access)) OR 
        (sub.emp_id = $emp_id AND !find_in_set($emp_id,faqs.list_emp_id)) OR
        (find_in_set($emp_id,sub.list_access) AND !find_in_set($emp_id,faqs.list_emp_id)) OR
        (cat.emp_id = $emp_id AND !find_in_set($emp_id,sub.list_emp_id) AND !find_in_set($emp_id,faqs.list_emp_id)) OR
        (find_in_set($emp_id,cat.list_access) AND !find_in_set($emp_id,sub.list_emp_id) AND !find_in_set($emp_id,faqs.list_emp_id)))
        ";
        $query['search']['append']="";
        $query['search']['total']="";
        if ($datatable['query']['searchField'] != '') {
            $keyword = $datatable['query']['searchField'];
            $query['search']['append'] .= " AND faqs.title LIKE '%$keyword%'";
            $query['search']['total'] .= " AND faqs.title LIKE '%$keyword%'";
        }
        if ($datatable['query']['searchCategory'] != '') {
            $cat = $datatable['query']['searchCategory'];
            $query['search']['append'] .= " AND sub.faqs_category_id = $cat";
            $query['search']['total'] .= " AND sub.faqs_category_id = $cat";
        }
        if ($datatable['query']['searchStatus'] != '') {
            $status = $datatable['query']['searchStatus'];
            $query['search']['append'] .= " AND faqs.isDisplay = $status";
            $query['search']['total'] .= " AND faqs.isDisplay = $status";
        }
        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';
        if (isset($query['search']['append'])) {
            $query['query'] .= $query['search']['append'];
            $search = $query['query'] . $query['search']['total'];
            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);
            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($query['query']));
        }
        if (isset($datatable['pagination'])) {
            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  " . $field : '';
            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }
            $query['query'] .= $order . ' ' . $sort . $limit;
        }
        $data = $this->general_model->custom_query($query['query']);
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];
        echo json_encode(['meta' => $meta, 'data' => $data]);
    }

//-------------------- END CONTROLLER DATA --------------------//



//-------------------- START CONTROLLER ADD --------------------//
   public function faqs_custom_group_add()
    {
        $distro_name = $this->input->post('distro_name');
        $check = $this->general_model->fetch_specific_val("distro_id", "distro_name='$distro_name' AND isDisplay = 1", "tbl_distro");
        if(!$check){
            $empId = $this->session->emp_id;
            $data = array(
                'emp_id'                => $empId,
                'isDisplay'             => 1,
                'distro_name'           => $distro_name,
                'list_emp_id'           => $this->input->post('list_emp_id'),
                'list_group_emp_id'     => $this->input->post('list_group_emp_id'),
            );
            $qry = $this->general_model->insert_vals($data, 'tbl_distro');
        } 

        echo json_encode($qry);
    }

    public function faqs_create_add()
    {
        $title = $this->input->post('title');
        $check = $this->general_model->fetch_specific_val("faqs_id", "title='$title' AND isDisplay != 2", "tbl_faqs");
        if(!$check){
            $data = array(
                'faqs_category_id'      => $this->input->post('faqs_category_id'),
                'emp_id'                => $this->session->emp_id,
                'title'                 => $this->input->post('title'),
                'details'               => $this->input->post('details'),
                'isDisplay'             => $this->input->post('isDisplay'),
                'list_emp_id'           => $this->input->post('list_emp_id'),
                'list_access'           => $this->input->post('list_access'),
            );
            $getLastId = $this->general_model->insert_vals_last_inserted_id($data, 'tbl_faqs');
            $data2 = array(
                'faqs_id'               => $getLastId,
                'type'                  => 1,
                'updatedBy'             => $this->session->emp_id
            );
            $this->general_model->insert_vals($data2, 'tbl_faqs_history');
        }
        echo json_encode($getLastId);
    }

    public function faqs_category_add()
    {
        $qry = 0;
        $list_emp_id = $this->input->post('list_emp_id');
        $list_access = $this->input->post('list_access');
        $list_group_emp_id = $this->input->post('list_group_emp_id');
        $list_group_access = $this->input->post('list_group_access');
        $category_name = $this->input->post("faqs_category_name");
        $check = $this->general_model->fetch_specific_val("faqs_category_id", "faqs_category_name='$category_name' AND isDisplay = 1", "tbl_faqs_category");
        if(!$check){
            $empId = $this->session->emp_id;
            $data = array(
                'emp_id'                => $empId,
                'isDisplay'             => 1,
                'relationship'          => $this->input->post('relation'),
                'faqs_category_name'    => $this->input->post('faqs_category_name'),
                'list_emp_id'           => $list_emp_id,
                'list_access'           => $list_access,
                'list_group_emp_id'     => $list_group_emp_id,
                'list_group_access'     => $list_group_access
            );
            $qry = $this->general_model->insert_vals($data, 'tbl_faqs_category');
        }  
        echo json_encode($qry);
    }
//-------------------- END CONTROLLER ADD --------------------//



//-------------------- START CONTROLLER UPDATE --------------------//  

    public function faqs_category_update()
    {
        $faqs_category_id = $this->input->post('faqs_category_id');
        $faqs_category_name = $this->input->post('faqs_category_name');
        $list_access = $this->input->post('list_access');
        $list_emp_id = $this->input->post('list_emp_id');
        $list_group_access = $this->input->post('list_group_access');
        $list_group_emp_id = $this->input->post('list_group_emp_id');
        $update = $this->input->post('update');
        if($update == 'name'){
            $data = array (
                'faqs_category_name' => $faqs_category_name,
            );
        } else if($update == 'editor'){
            $data = array (
                'list_access'        => $list_access,
                'list_group_access'  => $list_group_access,
            );
        } else if($update == 'viewer'){
            $data = array (
                'list_emp_id'        => $list_emp_id,
                'list_group_emp_id'  => $list_group_emp_id,
            );
        } else {
            $data = array (
                'faqs_category_name' => $faqs_category_name,
                'list_access'        => $list_access,
                'list_emp_id'        => $list_emp_id,
            );
        }
        $query = $this->general_model->update_vals($data, "faqs_category_id =$faqs_category_id", "tbl_faqs_category");
        echo json_encode($query);
    }

    public function faqs_custom_group_update()
    {
        $distro_name = $this->input->post('distro_name');
        $distro_id = $this->input->post('distro_id');
        $check = $this->general_model->fetch_specific_val("distro_id", "distro_name='$distro_name' AND distro_id!=$distro_id", "tbl_distro");
        if(!$check){
            $data = array(
                'distro_name'  =>  $distro_name,
                'list_emp_id'  =>  $this->input->post('list_emp_id'),
                'list_group_emp_id' =>  $this->input->post('list_group_emp_id'),
            );
            $query = $this->general_model->update_vals($data, "distro_id=$distro_id", "tbl_distro");
        }
        echo json_encode($query);
    }

    public function faqs_update()
    {
        $faqs_id = $this->input->post('faqs_id');
        $update = $this->input->post('update');

        if($update == 'title'){
            $data = array (
                'title'             => $this->input->post('title'),
            );
        } else if($update == 'category'){
            $data = array (
                'faqs_category_id'  => $this->input->post('category'),
            );
        } else if($update == 'list_access'){
            $data = array (
                'list_access'       => $this->input->post('list_access'),
            );
        } else if($update == 'list_emp_id'){
            $data = array (
                'list_emp_id'       => $this->input->post('list_emp_id'),
            );
        } else if($update == 'is_publish'){
            $data = array (
                'isDisplay'         => $this->input->post('data'),
            );
        } else if($update == 'details'){
            $data = array (
                'details'           => $this->input->post('details'),
            );
        } else {
            $data = array(
                'faqs_category_id'  =>  $this->input->post('faqs_category_id'),
                'title'             =>  $this->input->post('title'),
                'details'           =>  $this->input->post('details'),
                'isDisplay'         =>  $this->input->post('isDisplay'),
                'list_emp_id'       =>  $this->input->post('list_emp_id'),
                'list_access'       =>  $this->input->post('list_access'),
            );
        }
        
        $query = $this->general_model->update_vals($data, "faqs_id =$faqs_id", "tbl_faqs");
        $data2 = array(
            'faqs_id'               => $faqs_id,
            'type'                  => 2,
            'updatedBy'             => $this->session->emp_id
        );
        $this->general_model->insert_vals($data2, 'tbl_faqs_history');
        echo json_encode($query);
    }

//-------------------- END CONTROLLER UPDATE --------------------//



//-------------------- START CONTROLLER DELETE --------------------//

    public function faqs_custom_group_delete()
    {
        $distro_id = $this->input->post('distro_id');
        $data = array(
            'isDisplay'             =>  0,
            'deleteReason'          =>  $this->input->post('deleteReason'),
        );
        $data = $this->general_model->update_vals($data, "distro_id =$distro_id", "tbl_distro");
        echo json_encode($data);
    }

    public function faqs_category_delete()
    {
        $faqs_category_id = $this->input->post('faqs_category_id');
        $data = array(
            'isDisplay'             =>  0,
            'deleteEmp_id'          =>  $this->session->emp_id,
            'deleteReason'          =>  $this->input->post('deleteReason'),
        );
        $data = $this->general_model->update_vals($data, "faqs_category_id =$faqs_category_id", "tbl_faqs_category");
        echo json_encode($data);
    }

    public function faqs_delete()
    {
        $faqs_id = $this->input->post('faqs_id');
        $data = array(
            'isDisplay'             => 2,
            'deleteReason'          => $this->input->post('deleteReason'),
            'deleteBy'              => $this->session->emp_id,
        );
        $data = $this->general_model->update_vals($data, "faqs_id =$faqs_id", "tbl_faqs");
        $data2 = array(
            'faqs_id'               => $faqs_id,
            'type'                  => 3,
            'updatedBy'             => $this->session->emp_id
        );
        $this->general_model->insert_vals($data2, 'tbl_faqs_history');
        echo json_encode($data);
    }

//-------------------- END CONTROLLER DELETE --------------------//



//-------------------- START CONTROLLER UNIQUE --------------------//

    // public function account_list()
    // {
    //     $list_of_account = [];
    //     $distro_emp_id = [];
    //     $distro_id = $this->input->post('distro_id');
    //     $cur = 'SELECT * from tbl_distro dis where dis.distro_id = '.$distro_id;
    //     $data1 = $this->general_model->custom_query($cur);
    //     $distro_emp_id = explode(',',$data1[0]->list_emp_id);
    //     $qry = 'SELECT COUNT(emp.emp_id) as count, acc.acc_id, GROUP_CONCAT(emp.emp_id) as emp_id FROM tbl_employee emp, tbl_account acc WHERE emp.isActive = "yes" AND emp.acc_id = acc.acc_id GROUP BY acc.acc_id ORDER BY acc.acc_name';
    //     $data = $this->general_model->custom_query($qry);

    //     foreach($data as $row){
    //         $account_emp_ids = explode(',',$row->emp_id);
    //         $emp_not_assigned = [];
    //         $emp_not_assigned = array_diff($account_emp_ids, $distro_emp_id);
    //         if(count($emp_not_assigned) == 0){
    //             array_push($list_of_account, $row->acc_id);
    //         }
    //     }
    //     echo json_encode($list_of_account);
    // }

    // public function account_listv2()
    // {
    //     $list_of_account = [];
    //     $category_emp_id = [];
    //     $category_id = $this->input->post('category_id');
    //     $cur = 'SELECT * from tbl_faqs_category cat where cat.faqs_category_id = '.$category_id;
    //     $data1 = $this->general_model->custom_query($cur);
    //     $category_emp_id = explode(',',$data1[0]->list_emp_id);
    //     $qry = 'SELECT COUNT(emp.emp_id) as count, acc.acc_id, GROUP_CONCAT(emp.emp_id) as emp_id FROM tbl_employee emp, tbl_account acc WHERE emp.isActive = "yes" AND emp.acc_id = acc.acc_id GROUP BY acc.acc_id ORDER BY acc.acc_name';
    //     $data = $this->general_model->custom_query($qry);

    //     foreach($data as $row){
    //         $account_emp_ids = explode(',',$row->emp_id);
    //         $emp_not_assigned = [];
    //         $emp_not_assigned = array_diff($account_emp_ids, $category_emp_id);
    //         if(count($emp_not_assigned) == 0){
    //             array_push($list_of_account, $row->acc_id);
    //         }
    //     }
    //     echo json_encode($list_of_account);
    // }

    public function faqs_link()
    {
        $faqs_id = $this->input->post('faqs_list');
        var_dump($faqs_id);
    }

public function create_category_list_employee()
    {
        $emp = 
        'SELECT emp.emp_id, app.lname, app.fname, app.mname, acc.acc_name, dis.distro_id, GROUP_CONCAT(dis.distro_name) distro_name
        FROM tbl_applicant app
        INNER JOIN tbl_employee emp ON app.apid = emp.apid AND emp.isActive = "yes"
        INNER JOIN tbl_account acc ON acc.acc_id = emp.acc_id
        LEFT JOIN tbl_distro dis ON find_in_set(emp.emp_id, dis.list_emp_id) AND dis.isDisplay = 1
        GROUP BY emp.emp_id
        ORDER BY app.lname';
        $qry['emp'] = $this->general_model->custom_query($emp);
        $qry['dis'] = $this->general_model->fetch_specific_vals("distro_id, distro_name", "isDisplay = 1", "tbl_distro", "distro_name ASC");
        echo json_encode($qry);        
    }

    public function update_category_list_employee()
    {
        $category_id = $this->input->post('category_id');
        $checked = $this->general_model->fetch_specific_val('list_emp_id', 'faqs_category_id='.$category_id, 'tbl_faqs_category');
        $list_emp_id = $this->general_model->fetch_specific_val('GROUP_CONCAT(emp_id) list_emp_id', 'isActive = "yes"', 'tbl_employee');
        $emp = 
        'SELECT emp.emp_id, app.lname, app.fname, app.mname, acc.acc_name, GROUP_CONCAT(dis.distro_name) distro_name, if(find_in_set(emp.emp_id, "'.$checked->list_emp_id.'"), 1, 0) checked
        FROM tbl_applicant app
        INNER JOIN tbl_employee emp ON app.apid = emp.apid AND emp.isActive = "yes"
        INNER JOIN tbl_account acc ON acc.acc_id = emp.acc_id
        LEFT JOIN tbl_distro dis ON find_in_set(emp.emp_id, dis.list_emp_id) AND dis.isDisplay = 1
        GROUP BY emp.emp_id
        ORDER BY app.lname ASC';
        $qry = $this->general_model->custom_query($emp);
        echo json_encode($qry);        
    }

    public function faqs_custom_group_list()
    {
        $distro_id = $this->input->post('distro_id');
        $qrya = '
        SELECT d.distro_id, d.distro_name, d.list_emp_id, a.lname, a.fname, a.mname, ac.acc_id, ac.acc_name, e.emp_id
        FROM tbl_distro d, tbl_applicant a, tbl_employee e, tbl_account ac 
        WHERE find_in_set(e.emp_id, d.list_emp_id) AND e.apid = a.apid AND e.acc_id = ac.acc_id AND d.distro_id = 1 AND
        e.isActive = "yes" ';
        $qry1 = $this->general_model->custom_query($qrya);
        $qryb = '
        SELECT d.list_emp_id, a.lname, a.fname, a.mname, ac.acc_id, ac.acc_name, e.emp_id
        FROM tbl_distro d, tbl_applicant a, tbl_employee e, tbl_account ac 
        WHERE !find_in_set(e.emp_id, d.list_emp_id) AND e.apid = a.apid AND e.acc_id = ac.acc_id AND d.distro_id = 1 AND
        e.isActive = "yes" ';
        $qry2 = $this->general_model->custom_query($qryb);
        echo json_encode(['qry1' => $qry1, 'qry2' => $qry2]);
    }

    public function faqs_details_data()
    {
        $id = $this->input->post('id');
        $emp_id = $this->session->emp_id;
        $qry = 
        "SELECT faqs.emp_id, faqs.title, faqs.details, faqs.isDisplay, faqs.timeUpdate, app.lname, app.fname, app.mname, faqs.list_emp_id faqs_list_emp_id, faqs.list_access faqs_list_access, sub.faqs_category_id, sub.relationship, sub.list_access sub_list_access, sub.list_emp_id sub_list_emp_id, cat.list_access cat_list_access, cat.list_emp_id cat_list_emp_id
        FROM tbl_faqs faqs, tbl_employee emp, tbl_applicant app, tbl_faqs_category sub
        LEFT JOIN tbl_faqs_category cat ON sub.relationship=cat.faqs_category_id
        WHERE faqs.faqs_id=$id AND faqs.emp_id = emp.emp_id AND emp.apid = app.apid AND sub.faqs_category_id = faqs.faqs_category_id";
        $query = $this->general_model->custom_query($qry);
        // $query = $this->general_model->fetch_specific_val(
        //     "faqs.emp_id, faqs.title, faqs.details, faqs.isDisplay, faqs.timeUpdate, app.lname, app.fname, app.mname, faqs.list_emp_id faqs_list_emp_id, faqs.list_access faqs_list_access, sub.faqs_category_id, sub.relationship, sub.list_access sub_list_access, sub.list_emp_id sub_list_emp_id",
        //     "faqs.faqs_id=$id AND faqs.emp_id = emp.emp_id AND emp.apid = app.apid AND sub.faqs_category_id = faqs.faqs_category_id",
        //     "tbl_faqs faqs, tbl_employee emp, tbl_applicant app, tbl_faqs_category sub");
        echo json_encode($query);
    }

    public function faqs_subcategory_data()
    {
        $faqs_category_id = $this->input->post('faqs_category_id');
        $data = $this->general_model->fetch_specific_vals("sub.faqs_subcategory_id, sub.faqs_subcategory_name, sub.list_emp_id", "sub.faqs_category_id=$faqs_category_id", "tbl_faqs_subcategory sub", "faqs_subcategory_name ASC");
        echo json_encode($data);
    }

   public function get_faqs_sub_category()
    {
        $emp_id = $this->session->emp_id;
        $faqs_category_id = $this->input->post('faqs_category_id');
        $sub_folder = 
        "SELECT sub.faqs_category_id, sub.faqs_category_name, sub.list_access, sub.list_emp_id, sub.list_group_access, sub.list_group_emp_id
        FROM tbl_faqs_category sub
        LEFT JOIN tbl_faqs_category cat ON sub.relationship=cat.faqs_category_id 
        WHERE sub.relationship=$faqs_category_id AND sub.isDisplay=1 
        AND 
        ((sub.emp_id = $emp_id) OR 
        (find_in_set($emp_id,sub.list_access)) OR
        (find_in_set($emp_id,cat.list_access) AND (!find_in_set($emp_id,sub.list_emp_id))))";
        $qry['subfolder'] = $this->general_model->custom_query($sub_folder);
        $list_access = $this->input->post('list_access');
        $list_emp_id = $this->input->post('list_emp_id');
        $list_group_access = $this->input->post('list_group_access');
        $list_group_emp_id = $this->input->post('list_group_emp_id');
        foreach($qry['subfolder'] as $q => $key){
            $display_access_qry = 
            'SELECT app.lname, app.fname, app.mname, acc.acc_name, emp.emp_id, GROUP_CONCAT(dis.distro_name) distro_name, if(find_in_set(emp.emp_id,"'.$key->list_access.'"),1,0) checked_editor, if(find_in_set(emp.emp_id,"'.$key->list_emp_id.'"),1,0) checked_viewer
            FROM tbl_employee emp
            INNER JOIN tbl_applicant app ON app.apid = emp.apid
            INNER JOIN tbl_account acc ON acc.acc_id = emp.acc_id
            LEFT JOIN tbl_distro dis ON find_in_set(emp.emp_id, dis.list_emp_id) AND dis.isDisplay = 1
            WHERE emp.isActive = "yes"
            GROUP BY emp.emp_id
            ORDER BY app.lname ASC';
            $display_access = $this->general_model->custom_query($display_access_qry);
            $display_group_access_qry = 
            'SELECT dis.distro_id, dis.distro_name, if(find_in_set(dis.distro_id,"'.$key->list_group_access.'"),1,0) checked_group_editor, if(find_in_set(dis.distro_id,"'.$key->list_group_emp_id.'"),1,0) checked_group_viewer
            FROM tbl_distro dis
            WHERE dis.isDisplay = 1
            ORDER BY dis.distro_name ASC';
            $display_group_access = $this->general_model->custom_query($display_group_access_qry);
            $qArray = array();
            foreach ($display_access as $da) {
                array_push($qArray, $da);
            }
            $key->faqs = $qArray;
            $q_group_Array = array();
            foreach ($display_group_access as $da) {
                array_push($q_group_Array, $da);
            }
            $key->faqs_group = $q_group_Array;
        }
        echo json_encode($qry);
    }
    public function sub_category_list_of_employee()
    {
        $list_emp_id = $this->input->post('list_emp_id');
        $check_viewer = $this->input->post('check_viewer');
        $emp =        
        "SELECT app.lname, app.fname, app.mname, acc.acc_name, emp.emp_id, GROUP_CONCAT(dis.distro_name) distro_name, if(find_in_set(emp.emp_id, '$check_viewer'), 1, 0) checked
        FROM tbl_employee emp
        INNER JOIN tbl_applicant app ON app.apid = emp.apid
        INNER JOIN tbl_account acc ON acc.acc_id = emp.acc_id
        LEFT JOIN tbl_distro dis ON find_in_set(emp.emp_id, dis.list_emp_id) AND dis.isDisplay = 1
        WHERE emp.isActive = 'yes' AND find_in_set(emp.emp_id,'$list_emp_id')
        GROUP BY emp.emp_id
        ORDER BY checked DESC, app.lname ASC";
        $data = $this->general_model->custom_query($emp);
        echo json_encode($data);
    }

    public function get_faqs()
    {
        $faqs_id = $this->input->post('faqs_id');
        $qry = $this->general_model->fetch_specific_val("faqs.faqs_id, faqs.faqs_category_id, faqs.list_access, faqs.list_emp_id, faqs.isDisplay, faqs.title, faqs.details", "faqs.faqs_id='$faqs_id'", "tbl_faqs faqs");
        echo json_encode($qry);
    }

public function faqs_get_list()
    {
        $catId = $this->input->post('faqs_category_id');
        $emp_id = $this->session->emp_id;
        $faqsQuery = "
        SELECT faqs.faqs_id, faqs.isDisplay, faqs.faqs_category_id, faqs.title, faqs.details
        FROM tbl_faqs faqs, tbl_faqs_category cat
        LEFT JOIN tbl_distro dis ON find_in_set($emp_id, dis.list_emp_id)
        WHERE cat.isDisplay != 0 
        AND faqs.isDisplay != 2
        AND faqs.faqs_category_id = cat.faqs_category_id 
        AND faqs.faqs_category_id = $catId
        AND (($emp_id=faqs.emp_id) OR 
        (find_in_set($emp_id,faqs.list_access)) OR 
        (find_in_set($emp_id,faqs.list_emp_id) AND faqs.isDisplay=1) OR
        ($emp_id=cat.emp_id) OR 
        ((find_in_set($emp_id,cat.list_access)) AND ((faqs.list_emp_id is null) OR !find_in_set($emp_id,faqs.list_emp_id))) OR 
        (find_in_set($emp_id,cat.list_emp_id) AND faqs.isDisplay=1) OR
        (find_in_set($emp_id,dis.list_emp_id) AND find_in_set(dis.distro_id,cat.list_group_access) AND (faqs.list_emp_id is null OR !find_in_set($emp_id,faqs.list_emp_id))) OR
        (find_in_set($emp_id,dis.list_emp_id) AND find_in_set(dis.distro_id,cat.list_group_emp_id) AND faqs.isDisplay=1))";
        $faqs = $this->general_model->custom_query($faqsQuery);
        echo json_encode($faqs);
    }

//-------------------- END CONTROLLER UNIQUE --------------------//
}
