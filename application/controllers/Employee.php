<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once dirname(__FILE__) . "/General.php";

class Employee extends General
{
    private $administrator = array(6, 426, 321, 316, 222); //uid, jacebel,perry,chee,levi,khalil
    public $logo = "/var/www/html/sz/assets/images/img/logo2.png";
    public $bootstrapcss = '';
    protected $title = array('title' => 'Employee');
    private $philhealth_percentage_2019 = 0.0275;
    private $philhealth_percentage_2020 = 0.030;
    private $philhealth_percentage_2021 = 0.035;
    private $philhealth_percentage_2022 = 0.040;
    private $philhealth_percentage_2023 = 0.045;
    private $philhealth_percentage_2024_2025 = 0.050;
    private $pagibig_percentage = [0.01, 0.02]; //use index 2 if monthly basic is less than 1500
    private $tax_percentage_2018_2022 = array(
        'lowerlimit' => array(0, 250000, 400000, 800000, 2000000, 8000000),
        'upperlimit' => array(250000, 400000, 800000, 2000000, 8000000, 999999999), //99999999999 is for endless
        'taxlowerlimit' => array(0, 0, 30000, 130000, 490000, 2410000),
        'taxonexcess' => array(0, 0.20, 0.25, 0.30, 0.32, 0.35),
    );
    private $tax_percentage_2023 = array(
        'lowerlimit' => array(0, 250000, 400000, 800000, 2000000, 8000000),
        'upperlimit' => array(250000, 400000, 800000, 2000000, 8000000, 999999999), //99999999999 is for endless
        'taxlowerlimit' => array(0, 0, 22500, 102500, 402500, 2202500),
        'taxonexcess' => array(0, 0.15, 0.20, 0.25, 0.30, 0.35),
    );
    private $sss_2019 = array(
        ['lowerlimit' => 0, 'upperlimit' => 2249.99, 'monthly' => 80],
        ['lowerlimit' => 2250, 'upperlimit' => 2749.99, 'monthly' => 100],
        ['lowerlimit' => 2750, 'upperlimit' => 3249.99, 'monthly' => 120],
        ['lowerlimit' => 3250, 'upperlimit' => 3749.99, 'monthly' => 140],
        ['lowerlimit' => 3750, 'upperlimit' => 4249.99, 'monthly' => 160],
        ['lowerlimit' => 4250, 'upperlimit' => 4749.99, 'monthly' => 180],
        ['lowerlimit' => 4750, 'upperlimit' => 5249.99, 'monthly' => 200],
        ['lowerlimit' => 5250, 'upperlimit' => 5749.99, 'monthly' => 220],
        ['lowerlimit' => 5750, 'upperlimit' => 6249.99, 'monthly' => 240],
        ['lowerlimit' => 6250, 'upperlimit' => 6749.99, 'monthly' => 260],
        ['lowerlimit' => 6750, 'upperlimit' => 7249.99, 'monthly' => 280],
        ['lowerlimit' => 7250, 'upperlimit' => 7749.99, 'monthly' => 300],
        ['lowerlimit' => 7750, 'upperlimit' => 8249.99, 'monthly' => 320],
        ['lowerlimit' => 8250, 'upperlimit' => 8749.99, 'monthly' => 340],
        ['lowerlimit' => 8750, 'upperlimit' => 9249.99, 'monthly' => 360],
        ['lowerlimit' => 9250, 'upperlimit' => 9749.99, 'monthly' => 380],
        ['lowerlimit' => 9750, 'upperlimit' => 10249.99, 'monthly' => 400],
        ['lowerlimit' => 10250, 'upperlimit' => 10749.99, 'monthly' => 420],
        ['lowerlimit' => 10750, 'upperlimit' => 11249.99, 'monthly' => 440],
        ['lowerlimit' => 11250, 'upperlimit' => 11749.99, 'monthly' => 460],
        ['lowerlimit' => 11750, 'upperlimit' => 12249.99, 'monthly' => 480],
        ['lowerlimit' => 12250, 'upperlimit' => 12749.99, 'monthly' => 500],
        ['lowerlimit' => 12750, 'upperlimit' => 13249.99, 'monthly' => 520],
        ['lowerlimit' => 13250, 'upperlimit' => 13749.99, 'monthly' => 540],
        ['lowerlimit' => 13750, 'upperlimit' => 14249.99, 'monthly' => 560],
        ['lowerlimit' => 14250, 'upperlimit' => 14749.99, 'monthly' => 580],
        ['lowerlimit' => 14750, 'upperlimit' => 15249.99, 'monthly' => 600],
        ['lowerlimit' => 15250, 'upperlimit' => 15749.99, 'monthly' => 620],
        ['lowerlimit' => 15750, 'upperlimit' => 16249.99, 'monthly' => 640],
        ['lowerlimit' => 16250, 'upperlimit' => 16749.99, 'monthly' => 660],
        ['lowerlimit' => 16750, 'upperlimit' => 17249.99, 'monthly' => 680],
        ['lowerlimit' => 17250, 'upperlimit' => 17749.99, 'monthly' => 700],
        ['lowerlimit' => 17750, 'upperlimit' => 18249.99, 'monthly' => 720],
        ['lowerlimit' => 18250, 'upperlimit' => 18749.99, 'monthly' => 740],
        ['lowerlimit' => 18750, 'upperlimit' => 19249.99, 'monthly' => 760],
        ['lowerlimit' => 19250, 'upperlimit' => 19749.99, 'monthly' => 780],
        ['lowerlimit' => 19750, 'upperlimit' => 999999999, 'monthly' => 800],
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_hiring_positions()
    {
        $data = $this->general_model->fetch_specific_vals("pos_id,pos_details", "isHiring='Yes'", "tbl_position", "pos_details ASC");
        echo json_encode(array('positions' => $data));
    }

    public function get_all_tax_descriptions()
    {
        $data = $this->general_model->fetch_all("tax_desc_id,description", "tbl_tax_description", "tax_desc_id ASC");
        echo json_encode(array('taxdesc' => $data));
    }

    public function get_all_user_roles()
    {
        $data = $this->general_model->fetch_all("role_id,description", "tbl_user_role", "role_id ASC");
        echo json_encode(array('roles' => $data));
    }

    public function get_all_emp_stat()
    {
        $data = $this->general_model->fetch_all("empstat_id,status", "tbl_emp_stat");
        echo json_encode(array('emp_stat' => $data));
    }

    public function get_all_accounts()
    {
        $data = $this->general_model->fetch_all("acc_id,acc_name,acc_description", "tbl_account", "acc_name ASC");
        echo json_encode(array('accounts' => $data));
    }

    public function get_all_departments()
    {
        $data = $this->general_model->fetch_all("dep_id,dep_name,dep_details", "tbl_department", "dep_details ASC");
        echo json_encode(array('departments' => $data));
    }

    public function get_all_active_employees()
    {
        $data = $this->general_model->fetch_specific_vals("a.fname,a.lname,b.emp_id,b.allowUserUpdate", "a.apid=b.apid AND b.isActive='yes'", "tbl_applicant a,tbl_employee b", "a.lname ASC");
        echo json_encode(array('activeEmployees' => $data));
    }

    public function get_all_active_employees_without_subordinates($emp_id)
    {

        $uid = $this->general_model->fetch_specific_val('uid', "emp_id=$emp_id", 'tbl_user')->uid;
        $subordinates = array_column($this->getEmployeeSubordinate($uid),'emp_id');
        if(is_array($subordinates) && COUNT($subordinates) == 0){
            $data = $this->general_model->fetch_specific_vals("a.fname,a.lname,b.emp_id,b.allowUserUpdate", "a.apid=b.apid AND b.isActive='yes'", "tbl_applicant a,tbl_employee b", "a.lname ASC");
        }else{
            $data = $this->general_model->fetch_specific_vals("a.fname,a.lname,b.emp_id,b.allowUserUpdate", "a.apid=b.apid AND b.isActive='yes' AND b.emp_id NOT IN (".implode(',',$subordinates).")", "tbl_applicant a,tbl_employee b", "a.lname ASC");
        }

        echo json_encode(array('activeEmployees' => $data));
    }

    public function dashboard()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/dashboard', $data);
        }
    }

    public function getDashboardDetails()
    {
        $gender = $this->general_model->custom_query("SELECT COUNT(a.gender) as count,a.gender FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' GROUP BY a.gender");
        $agelist = $this->general_model->custom_query("SELECT TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) AS age FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes'");
        $age = [0, 0, 0, 0, 0];
        foreach ($agelist as $agel) {
            if ($agel->age < 18) {
                $age[0]++;
            } elseif ($agel->age >= 18 && $agel->age <= 25) {
                $age[1]++;
            } elseif ($agel->age >= 26 && $agel->age <= 35) {
                $age[2]++;
            } elseif ($agel->age >= 36 && $agel->age <= 45) {
                $age[3]++;
            } else {
                $age[4]++;
            }
        }
        $department = $this->general_model->custom_query("SELECT COUNT(e.dep_id) as count,e.dep_name FROM tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_pos_dep d,tbl_department e,tbl_employee f WHERE a.isActive = 1 AND a.isHistory = 1 AND a.posempstat_id = b.posempstat_id AND b.pos_id=c.pos_id AND c.pos_id=d.pos_id AND c.class='Admin' AND d.dep_id=e.dep_id AND f.isActive = 'yes' AND f.emp_id = a.emp_id GROUP by e.dep_id ORDER BY e.dep_name ASC");
        $account = $this->general_model->custom_query("SELECT COUNT(a.emp_id) as count, b.acc_name FROM tbl_employee a,tbl_account b WHERE a.acc_id = b.acc_id AND b.acc_description = 'Agent' AND a.isActive = 'yes' GROUP BY b.acc_id ORDER BY b.acc_name ASC");
        $site = $this->general_model->custom_query("SELECT code FROM tbl_site");
        $site_details = $this->general_model->custom_query("SELECT COUNT(e.site_ID) as count,c.class,e.code FROM tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_employee d,tbl_site e WHERE a.isActive = 1 AND a.isHistory = 1 AND a.posempstat_id = b.posempstat_id AND b.pos_id=c.pos_id AND d.isActive = 'yes' AND d.emp_id = a.emp_id AND c.site_ID = e.site_ID GROUP BY class,code");
        $birthday = $this->general_model->custom_query("SELECT a.fname,a.lname,a.pic,a.birthday, (366 + DAYOFYEAR(a.birthday) - DAYOFYEAR(NOW())) % 366 as left_days FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive = 'yes' ORDER BY left_days LIMIT 6");

        $worklist = $this->general_model->custom_query("SELECT TIMESTAMPDIFF(YEAR, c.dateFrom, CURDATE()) AS count FROM tbl_applicant a,tbl_employee b,tbl_emp_promote c WHERE a.apid=b.apid AND b.isActive='yes' AND b.emp_id = c.emp_id AND c.isHistory = 1 GROUP BY b.emp_id ORDER BY c.dateFrom");
        $work = [0, 0, 0, 0, 0, 0];
        foreach ($worklist as $workl) {
            if ($workl->count === null) {
            } elseif ($workl->count === '0' || $workl->count === '1') {
                $work[0]++;
            } elseif ($workl->count === '2' || $workl->count === '3') {
                $work[1]++;
            } elseif ($workl->count === '4' || $workl->count === '5') {
                $work[2]++;
            } elseif ($workl->count === '6' || $workl->count === '7') {
                $work[3]++;
            } elseif ($workl->count === '8' || $workl->count === '9') {
                $work[4]++;
            } else {
                $work[5]++;
            }
        }

        $anniversary = $this->general_model->custom_query("SELECT a.fname,a.lname,a.pic,c.dateFrom, (366 + DAYOFYEAR(c.dateFrom) - DAYOFYEAR(NOW())) % 366 as left_days FROM tbl_applicant a,tbl_employee b,tbl_emp_promote c WHERE a.apid=b.apid AND b.isActive = 'yes' AND b.emp_id = c.emp_id AND c.isHistory = 1 AND c.dateFrom IS NOT NULL AND c.dateFrom != '0000-00-00' GROUP BY b.emp_id ORDER BY left_days,c.dateFROM ASC LIMIT 6");
        $pending = $this->general_model->fetch_specific_val("COUNT(employeeRequest_ID) as count", "finalStatus=2", "tbl_employee_request")->count;
        echo json_encode(['gender' => $gender, 'age' => $age, 'department' => $department, 'account' => $account, 'site' => $site, 'site_details' => $site_details, 'birthday' => $birthday, 'work' => $work, 'anniversary' => $anniversary, 'pending' => $pending]);
    }

    //------------------------------------------------------------PROFILES AREA--------------------------------------------------------------------------------
    public function profiles()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $uid = $this->session->uid;
        if (in_array($uid, $this->administrator)) {
            $data['isAdmin'] = true;
        } else {
            $data['isAdmin'] = false;
        }
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/profiles', $data);
        }
    }

    public function getEmployeeDirectory()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchname = $this->input->post('searchname');
        $searchclass = $this->input->post('searchclass');
        $searchaccounts = $this->input->post('searchaccounts');
        $searchactive = $this->input->post('searchactive');
        $searchstatus = $this->input->post('searchstatus');
        $search = array();
        $searchString = "";
        if ($searchname !== '') {
            $search[] = "(a.fname LIKE \"%$searchname%\" OR a.lname LIKE \"%$searchname%\")";
        }
        if ($searchclass !== '') {
            $search[] = "c.acc_description = '$searchclass'";
        }
        if ($searchaccounts !== '') {
            $search[] = "c.acc_id=$searchaccounts";
        }
        if ($searchactive !== '') {
            $search[] = "b.isActive='$searchactive'";
        }
        if ($searchstatus !== '') {
            $search[] = "f.empstat_id=$searchstatus";
        }

        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $references = $this->general_model->custom_query("SELECT previousEmployee_ID FROM tbl_employee_rehire_reference");
        $reference_ids = array();
        foreach ($references as $ref) {
            $reference_ids[] = $ref->previousEmployee_ID;
        }
        $referenceString = (empty($reference_ids)) ? "" : "AND b.emp_id NOT IN (" . implode(',', $reference_ids) . ")";

        if ($searchstatus !== '') {
            $query = "SELECT TRIM(a.fname) as fname,TRIM(a.lname) as lname,a.pic,c.acc_name,b.emp_id,b.isActive FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_emp_promote d,tbl_pos_emp_stat e,tbl_emp_stat f WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND b.emp_id = d.emp_id AND d.isActive = 1 AND d.posempstat_id = e.posempstat_id AND e.empstat_id = f.empstat_id " . $searchString . " " . $referenceString . " ORDER BY lname ASC";
        } else {
            $query = "SELECT TRIM(a.fname) as fname,TRIM(a.lname) as lname,a.pic,c.acc_name,b.emp_id,b.isActive FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.acc_id=c.acc_id " . $searchString . " " . $referenceString . " ORDER BY lname ASC";
        }
        $employees = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $allactiveemployees = $this->general_model->custom_query($query);
        $count = COUNT($allactiveemployees);
        echo json_encode(array('employees' => $employees, 'total' => $count));
    }

    public function allowDirectoryAccess($for)
    {
        $emp_id = $this->session->userdata('emp_id');
        $password = $this->input->post('password');
        if ($for === 'personal') {
            $security_code = $this->general_model->fetch_specific_val("password_hash", "emp_id=$emp_id", "tbl_user")->password_hash;
            $status = (strcmp(sha1($password), $security_code) === 0) ? "Success" : "Failed";
        } else {
            $security_code = $this->general_model->fetch_specific_val("security_code", " a.role_id=b.role_id AND a.emp_id = $emp_id", "tbl_user a,tbl_user_role b")->security_code;
            $status = (strcmp($password, $security_code) === 0) ? "Success" : "Failed";
        }
        echo json_encode(array('status' => $status));
    }

    public function profilePreview()
    {
        $accessor = $this->session->userdata('emp_id');
        $user_access = $this->general_model->fetch_specific_val("*", "emp_id = $accessor", "tbl_employee_user_access");

        $emp_id = $this->input->post('emp_id');

        $employee = $this->general_model->fetch_specific_val("a.fname,a.mname,a.lname,a.pic,b.Supervisor,b.emergencyFname,b.emergencyMname,b.emergencyLname,b.emergencyContact,a.email,a.cell,a.presentAddress,g.acc_name", "a.apid=b.apid AND b.acc_id=g.acc_id AND b.emp_id = $emp_id", "tbl_applicant a,tbl_employee b,tbl_account g");
        $references = $this->general_model->fetch_specific_vals("previousEmployee_ID", "currentEmployee_ID=$emp_id", "tbl_employee_rehire_reference");
        $position = $this->general_model->fetch_specific_val("c.pos_details,d.status", "a.emp_id=$emp_id AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND a.isActive=1 AND a.isHistory=1", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d");
        if ($employee->Supervisor !== null && isset($position->pos_details)) {
            $employee->pos_details = $position->pos_details;
            $employee->status = $position->status;
        } else {
            $employee->pos_details = null;
            $employee->status = null;
        }
        if ($employee->Supervisor !== null && $employee->Supervisor !== '') {
            $supervisor = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.mname,' ',a.lname) as supervisor_name,a.email", "a.apid=b.apid AND b.emp_id=" . $employee->Supervisor, "tbl_applicant a,tbl_employee b");
            $employee->supervisor_name = (isset($supervisor->supervisor_name)) ? $supervisor->supervisor_name : null;
            $employee->supervisor_email = (isset($supervisor->email)) ? $supervisor->email : null;
        } else {
            $employee->supervisor_name = null;
            $employee->supervisor_email = null;
        }
        $employee->references = null;
        if ($references !== null && !empty($references)) {
            $ref_ids = array();
            foreach ($references as $refs) {
                $ref_ids[] = $refs->previousEmployee_ID;
            }
            $references_details = $this->general_model->custom_query("SELECT a.emp_id,a.separationDate,b.dateFrom FROM tbl_employee a,tbl_emp_promote b WHERE a.emp_id = b.emp_id AND a.emp_id IN (" . implode(',', $ref_ids) . ") GROUP BY a.emp_id ORDER BY b.dateFrom DESC");
            $final = array();
            foreach ($ref_ids as $id) {
                $final[$id] = null;
                foreach ($references_details as $ref_detail) {
                    if ($id === $ref_detail->emp_id) {
                        $final[$id] = $ref_detail;
                    }
                }
            }
            $employee->references = $final;
        }
        echo json_encode(array('profile' => $employee, 'user_access' => $user_access));
    }

    public function showDTRCredentials()
    {
        $emp_id = $this->session->userdata('emp_id');
        $dtr_emp_id = $this->input->post('emp_id');
        $password = $this->input->post('password');
        //        $security_code = $this->general_model->fetch_specific_val("security_code", " a.role_id=b.role_id AND a.emp_id = $emp_id", "tbl_user a,tbl_user_role b")->security_code;

        $security_code = $this->general_model->fetch_specific_val("password_hash", "emp_id = $emp_id", "tbl_user")->password_hash;
        $status = (strcmp(sha1($password), $security_code) === 0) ? "Success" : "Failed";
        $dtr_username = "";
        $dtr_password = "";
        if ($status === 'Success') {
            $dtr = $this->general_model->fetch_specific_val("username,password_hash", "emp_id=$dtr_emp_id", "tbl_user");

            $dtr_username = (!isset($dtr->username)) ? "" : $dtr->username;
            $dtr_password = (!isset($dtr->password_hash)) ? "" : $dtr->password_hash;
        }
        echo json_encode(array('status' => $status, 'username' => $dtr_username, 'password' => $dtr_password));
    }

    public function employeeDetails()
    {
        $emp_id = $this->input->post('emp_id');
        $employee = $this->general_model->fetch_specific_val("a.*,b.*,c.acc_name", "a.apid=b.apid AND b.emp_id = $emp_id AND b.acc_id=c.acc_id", "tbl_applicant a,tbl_employee b,tbl_account c");
        $user = $this->general_model->fetch_specific_val("a.username,a.password,a.role_id,b.description as role_description", "a.emp_id = $emp_id AND a.role_id=b.role_id", "tbl_user a,tbl_user_role b");
        $employee->username = (isset($user->username)) ? $user->username : null;
        $employee->password = (isset($user->password)) ? $user->password : null;
        $employee->role_id = (isset($user->role_id)) ? $user->role_id : null;
        $employee->role_description = (isset($user->role_description)) ? $user->role_description : null;
        $family = $this->general_model->fetch_specific_vals("*", "emp_id = $emp_id", "tbl_employee_family");

        //        $taxdesc = $this->general_model->fetch_specific_val("*", "a.emp_id = $emp_id AND a.tax_desc_id=b.tax_desc_id", "tbl_tax_emp a,tbl_tax_description b");
        $pagibigadd = $this->general_model->fetch_specific_val("amount", "emp_id = $emp_id AND isActive=1", "tbl_pagibig_add_contribution");
        $pagibig_additional = (isset($pagibigadd->amount) && $pagibigadd !== null) ? $pagibigadd->amount : '';

        if ($employee->Supervisor !== null && $employee->Supervisor !== '') {
            $supervisor = $this->general_model->fetch_specific_val("CONCAT(a.fname,' ',a.mname,' ',a.lname) as supervisor_name,a.email", "a.apid=b.apid AND b.emp_id=" . $employee->Supervisor, "tbl_applicant a,tbl_employee b");
            $employee->supervisor_name = (isset($supervisor->supervisor_name)) ? $supervisor->supervisor_name : null;
            $employee->supervisor_email = (isset($supervisor->email)) ? $supervisor->email : null;
        } else {
            $employee->supervisor_name = null;
            $employee->supervisor_email = null;
        }
        echo json_encode(array('employee' => $employee, 'pagibig_additional' => $pagibig_additional, 'family' => $family));
    }

    public function employeePositionDetails()
    {
        $emp_id = $this->input->post('emp_id');
        $position = $this->general_model->fetch_specific_val("a.dateFrom,c.pos_name,c.pos_details,c.class,d.status,e.rate,g.dep_name,g.dep_details", "a.emp_id = $emp_id AND a.isActive = 1 AND a.isHistory = 1 AND a.posempstat_id=b.posempstat_id AND b.pos_id = c.pos_id AND b.empstat_id=d.empstat_id AND b.posempstat_id=e.posempstat_id AND c.pos_id=f.pos_id AND f.dep_id=g.dep_id AND e.isActive = 1", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d,tbl_rate e,tbl_pos_dep f,tbl_department g");
        $positionHistory = $this->general_model->fetch_specific_vals("a.emp_promoteID,c.pos_name,c.pos_details,c.class,d.status,a.dateFrom", "a.emp_id = $emp_id AND a.isActive = 0 AND a.isHistory = 1 AND a.posempstat_id=b.posempstat_id AND b.pos_id = c.pos_id AND b.empstat_id=d.empstat_id", "tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d", "a.dateFrom DESC");
        $employee = $this->general_model->fetch_specific_val("salarymode,workDaysPerWeek,confidential", "emp_id = $emp_id", "tbl_employee");
        $rate = ($position != null) ? $position->rate : null;
        $payroll = $this->getGovernmentPayments($employee->salarymode, $rate, $emp_id);
        echo json_encode(array('position' => $position, 'positionHistory' => $positionHistory, 'payroll' => $payroll, 'employee' => $employee));
    }

    private function getGovernmentPayments($salarymode, $rate, $emp_id)
    {
        if (strtolower($salarymode) === 'monthly') {
            $taxArray = $this->tax_percentage_2018_2022;
            $sssArray = $this->sss_2019;
            $sss = null;
            // if ($rate != NULL) {
            //     $sss = $this->general_model->fetch_specific_val("sss_employee", "a.min_range<='" . $rate . "' AND a.max_range>='" . $rate . "' AND a.sss_effec_id=b.sss_effec_id AND b.isActive=1", "tbl_sss as a,tbl_sss_effectivity as b");
            //     if (isset($sss->sss_employee) && $sss !== NULL) {
            //         $sss = $sss->sss_employee / 2;
            //     } else {
            //         $sss = NULL;
            //     }
            // }
            if ($rate != null) {
                foreach ($sssArray as $index => $row) {
                    if ($row['lowerlimit'] <= $rate && $rate < $row['upperlimit']) {
                        $sss = $row['monthly'] / 2;
                    }
                }
            }
            if (date("Y") == 2020) {
                $philhealthToUse = $this->philhealth_percentage_2020;
            } elseif (date("Y") == 2021) {
                $philhealthToUse = $this->philhealth_percentage_2021;
            } elseif (date("Y") == 2022) {
                $philhealthToUse = $this->philhealth_percentage_2022;
            } elseif (date("Y") == 2023) {
                $philhealthToUse = $this->philhealth_percentage_2023;
            } else {
                $philhealthToUse = $this->philhealth_percentage_2024_2025;
            }

            $philhealth = ($rate * $philhealthToUse) / 4; // divide 2 for employer then divide 2 for semi-monthly

            $index = ($rate <= 1500) ? 0 : 1;
            if ($emp_id=='1462') {// SIR RANDY IGNACIO 1462
                $pagibig = 50;
            } else {
                $pagibig = ($rate * $this->pagibig_percentage[$index]) / 2;
            }

            $payroll['sss'] = $sss;
            $payroll['philhealth'] = $philhealth;
            $payroll['pagibig'] = $pagibig;

            $tax = null;
            $gov = ($sss * 2) + ($philhealth * 2) + ($pagibig * 2);

            $annualrate = $rate - $gov;

            $annualrate = $annualrate * 12;
            //------------------------------------------UPDATE NEW CHURVABELS---------------------------------------------
            for ($x = 0; $x < COUNT($taxArray['lowerlimit']); $x++) {
                if ($taxArray['lowerlimit'][$x] <= $annualrate && $annualrate < $taxArray['upperlimit'][$x]) {
                    $excess = ($annualrate - $taxArray['lowerlimit'][$x]) * $taxArray['taxonexcess'][$x];
                    $taxdue = ($taxArray['taxlowerlimit'][$x] + $excess) / 12;
                    $tax = $taxdue / 2;
                }
            }

            $payroll['tax'] = $tax;
        } else {
            //DAILY PAID EMPLOYEES
            $payroll['sss'] = 136.25;
            $payroll['pagibig'] = 68.75;
            $payroll['philhealth'] = 75.00;
            $payroll['tax'] = 0;
        }
        return $payroll;
    }

    public function updateEmployeeProfile()
    {
        $original_value = $this->input->post('original_value');
        $save = $this->input->post('save');
        $table = $this->input->post('table');
        $name = $this->input->post('name');
        $value = $this->input->post('value');
        $new_value = (strlen($value) === 0) ? 'NULL' : "'$value'";
        $emp_id = $this->input->post('emp_id');
        if ($save === 'insert') {
            $query = "INSERT INTO $table ($name,emp_id) VALUES ($new_value,$emp_id)";
            $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
            $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> create a new record of <span class='value'>$name</span> with a value of <span class='value'>'$new_value'</span> for <span class='name'>$nameOf</span>");
        } else {
            if ($table === 'tbl_pagibig_add_contribution') {
                $query = "UPDATE tbl_pagibig_add_contribution SET $table.$name = " . $new_value . " WHERE tbl_pagibig_add_contribution.emp_id=$emp_id ";
            } elseif ($table === 'tbl_tax_emp') {
                $query = "UPDATE tbl_tax_emp SET $table.$name = " . $new_value . " WHERE tbl_tax_emp.emp_id=$emp_id ";
            } else {
                $query = "UPDATE tbl_applicant, tbl_employee ,tbl_user SET $table.$name = " . $new_value . " WHERE tbl_applicant.apid=tbl_employee.apid AND tbl_employee.emp_id=$emp_id AND tbl_employee.emp_id=tbl_user.emp_id";
            }
            $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
            $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> updated the <span class='value'>$name</span>  of <span class='name'>$nameOf</span> from <span class='value'>'$original_value'</span> to <span class='value'>'$new_value'</span>");
        }
        $res = $this->general_model->custom_query_no_return($query);
        $employee = $this->general_model->fetch_specific_val("a.lname,a.fname,b.salarymode,b.workDaysPerWeek,b.Supervisor,c.acc_name,confidential", "a.apid=b.apid AND b.emp_id = $emp_id AND b.acc_id=c.acc_id", "tbl_applicant a, tbl_employee b ,tbl_account c");
        if ($employee->Supervisor !== null) {
            $supervisor = $this->general_model->fetch_specific_val("a.email", "a.apid=b.apid AND b.emp_id = $employee->Supervisor", "tbl_applicant a,tbl_employee b");
            $employee->SupervisorEmail = (isset($supervisor->email)) ? $supervisor->email : null;
        } else {
            $employee->SupervisorEmail = null;
        }
        $position = $this->general_model->fetch_specific_val("c.rate", "a.emp_id = $emp_id AND a.isActive = 1 AND a.isHistory = 1 AND c.isActive = 1 AND a.posempstat_id=b.posempstat_id AND  b.posempstat_id=c.posempstat_id", "tbl_emp_promote a,tbl_pos_emp_stat b, tbl_rate c");
        $rate = ($position != null) ? $position->rate : null;
        $payroll = $this->getGovernmentPayments($employee->salarymode, $rate, $emp_id);
        $status = ($res > 0) ? "Success" : "Failed";
        echo json_encode(array('status' => $status, 'employee' => $employee, 'position' => $position, 'payroll' => $payroll));
    }

    public function updateEmployeeFamily()
    {
        $original_value = $this->input->post('original_value');
        $table = $this->input->post('table');
        $name = $this->input->post('name');
        $value = $this->input->post('value');
        $employeeFamily_ID = $this->input->post('employeeFamily_ID');
        $new_value = (strlen($value) === 0) ? 'NULL' : "'$value'";
        $emp_id = $this->input->post('emp_id');
        $query = "UPDATE tbl_employee_family SET $table.$name = " . $new_value . " WHERE emp_id=$emp_id AND employeeFamily_ID = $employeeFamily_ID";
        $res = $this->general_model->custom_query_no_return($query);

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> updated the family record: <span class='value'>$name</span>  of <span class='name'>$nameOf</span> from <span class='value'>'$original_value'</span> to <span class='value'>'$new_value'</span>");

        $family = $this->general_model->fetch_specific_vals("*", "emp_id = $emp_id", "tbl_employee_family");
        $isActive = $this->general_model->fetch_specific_val("isActive", "emp_id = $emp_id", "tbl_employee")->isActive;
        $status = ($res > 0) ? "Success" : "Failed";
        echo json_encode(array('status' => $status, 'family' => $family, 'isActive' => $isActive));
    }

    public function uploadEmployeeImage()
    {
        $emp_id = $this->input->post('emp_id');
        $source = $this->input->post('source');
        $data = $this->input->post('image');
        $img_arr = explode(";", $data);
        $img_arr_2 = explode(",", $img_arr[1]);
        $d = base64_decode($img_arr_2[1]);
        $imageName = ($source === 'signature') ? 'uploads/signatures/e_signature_' . $emp_id . '.jpg' : 'uploads/profiles/szprofile_' . $emp_id . '.jpg';
        $result = file_put_contents($imageName, $d);

        if ($result === false) {
            $status = "Error";
        } else {
            if ($source === 'signature') {
                $pic = $this->general_model->fetch_specific_val("signature", "emp_id = $emp_id", "tbl_employee")->signature;
                if ($pic === '' || $pic === null || $imageName !== $pic) {
                    $query = "UPDATE tbl_employee SET signature = '$imageName' WHERE emp_id=$emp_id";
                    $res = $this->general_model->custom_query_no_return($query);
                    $status = ($res) ? "Success" : "Error";
                } else {
                    $status = 'Success';
                }
            } else {
                $pic = $this->general_model->fetch_specific_val("a.pic", "a.apid=b.apid AND b.emp_id = $emp_id", "tbl_applicant a,tbl_employee b")->pic;
                if ($pic === '' || $pic === null || $imageName !== $pic) {
                    $query = "UPDATE tbl_applicant, tbl_employee SET tbl_applicant.pic = '$imageName' WHERE tbl_applicant.apid=tbl_employee.apid AND tbl_employee.emp_id=$emp_id";
                    $res = $this->general_model->custom_query_no_return($query);
                    $status = ($res) ? "Success" : "Error";
                } else {
                    $status = 'Success';
                }
            }
        }
        if ($status === 'Success') {
            $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
            $message = ($source === 'signature') ? "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> updated the e-signature of <span class='name'>$nameOf</span>" : "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> updated the picture of <span class='name'>$nameOf</span>";
            $this->create_log_history(5, $this->session->emp_id, $message);

            if ($source === 'profile') {
                // jpg  change the dimension 750, 450 to your desired values
                list($width, $height) = getimagesize($imageName);
                $src = imagecreatefromjpeg($imageName);
                $dst = imagecreatetruecolor(100, 100);
                imagecopyresampled($dst, $src, 0, 0, 0, 0, 100, 100, $width, $height);
                imagejpeg($dst, 'uploads/profiles/szprofile_' . $emp_id . '_thumbnail.jpg');
            }
        }

        echo json_encode(array('pic', $imageName, 'status' => $status, 'source' => $source));
    }

    public function get_schedule_logs()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $emp_id = $this->input->post('emp_id');

        $query = "SELECT a.sched_id,a.sched_date,c.time_start,c.time_end,d.type,d.style,e.emp_id,a.schedtype_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_employee e ON a.emp_id=e.emp_id  INNER JOIN tbl_account g ON e.acc_id=g.acc_id WHERE  date(a.sched_date) between date('$datestart') and date('$dateend') AND e.emp_id = $emp_id ORDER BY a.sched_date ASC";

        $schedulelogs = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $count = COUNT($this->general_model->custom_query($query));
        foreach ($schedulelogs as $schedlog) {
            $schedids[] = $schedlog->sched_id;
            $schedlog->clock_in = null;
            $schedlog->clock_out = null;
            $schedlog->breaklogs = null;
            $schedlog->totalbreakminutes = 0;
            $schedlog->totalbreakseconds = 0;
        }
        $logs = array();
        if (!empty($schedids)) {
            $logs = $this->general_model->fetch_specific_vals("dtr_id,entry,log,sched_id", "type='DTR' AND sched_id IN (" . implode(",", $schedids) . ") ", "tbl_dtr_logs");
        }
        foreach ($schedulelogs as $schedlog) {
            foreach ($logs as $key => $log) {
                if ($schedlog->sched_id === $log->sched_id) {
                    if ($log->entry === 'I') {
                        $schedlog->clock_in = $log;
                    } else {
                        $schedlog->clock_out = $log;
                    }
                    unset($logs[$key]);
                }
            }
            if (isset($schedlog->clock_in->dtr_id) && isset($schedlog->clock_out->dtr_id)) {
                $dtr_in_id = $schedlog->clock_in->dtr_id;
                $dtr_out_id = $schedlog->clock_out->dtr_id;
                $breaklogs = $this->general_model->fetch_specific_vals("dtr_id,entry,log,note", "type='Break' AND dtr_id > $dtr_in_id AND dtr_id < $dtr_out_id AND  emp_id = $emp_id", "tbl_dtr_logs");
                $breaksIn = array();
                $breaksOut = array();
                foreach ($breaklogs as $break) {
                    if ($break->entry === 'I') {
                        $breaksIn[] = $break;
                    } else {
                        $breaksOut[] = $break;
                    }
                }
                $breaks = array();
                foreach ($breaksOut as $key => $brk_out) {
                    foreach ($breaksIn as $brk_in) {
                        $breaks[$key]['breakout'] = $brk_out;
                        if ($brk_out->dtr_id === $brk_in->note) {
                            $breaks[$key]['breakin'] = $brk_in;
                        }
                    }
                }
                $partialbreaktime = 0;
                foreach ($breaks as $brk) {
                    // $breakout = $brk['breakout']->log;
                    // $breakin = $brk['breakin']->log;
                    // $breakouttime = strtotime($breakout);
                    // $breakintime = strtotime($breakin);
                    // $totalseconds = $breakintime - $breakouttime;
                    // $partialbreaktime += $totalseconds;

                    if (isset($brk['breakout']) && isset($brk['breakin'])) {
                        $breakout = $brk['breakout']->log;
                        $breakin = $brk['breakin']->log;
                        $breakouttime = strtotime($breakout);
                        $breakintime = strtotime($breakin);
                        $totalseconds = $breakintime - $breakouttime;
                        $partialbreaktime += $totalseconds;
                    } else {
                        $partialbreaktime += 0;
                    }
                }

                $minutes = floor($partialbreaktime / 60);
                $seconds = $partialbreaktime - $minutes * 60;
                $schedlog->breaklogs = $breaks;
                $schedlog->totalbreakseconds = $seconds;
                $schedlog->totalbreakminutes = $minutes;
            }
        }

        echo json_encode(array('data' => $schedulelogs, 'total' => $count));
    }

    public function get_audit_trail()
    {
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $emp_id = $this->input->post('emp_id');
        $query = "SELECT io_id,log,ipaddress,remark FROM tbl_login_logout_session a,tbl_user b WHERE a.uid=b.uid AND  date(a.log) between date('$datestart') and date('$dateend') AND b.emp_id = $emp_id ORDER BY log ASC";
        $trails = $this->general_model->custom_query($query);

        echo json_encode(array('trails' => $trails));
    }

    public function getPositionPerDepartment()
    {
        $dep_id = $this->input->post('dep_id');
        if ($dep_id === null || $dep_id === '') {
            $positions = $this->general_model->fetch_all("a.pos_id,a.pos_name,a.pos_details", "tbl_position a", "pos_details ASC");
        } else {
            $positions = $this->general_model->fetch_specific_vals("a.pos_id,a.pos_name,a.pos_details", "a.pos_id = b.pos_id AND b.dep_id = $dep_id", "tbl_position a,tbl_pos_dep b", "pos_details ASC");
        }
        echo json_encode(array('positions' => $positions));
    }

    public function rateChecker()
    {
        $empstat_id = $this->input->post('empstat_id');
        $pos_id = $this->input->post('pos_id');
        $rate = $this->general_model->fetch_specific_val('rate', "a.posempstat_id=b.posempstat_id AND b.isActive=1 AND a.empstat_id=$empstat_id AND a.pos_id=$pos_id", 'tbl_pos_emp_stat a,tbl_rate b');

        if ($rate !== null && isset($rate->rate)) {
            echo json_encode(array('status' => "Success"));
        } else {
            echo json_encode(array('status' => "Failed"));
        }
    }

    public function changePosition()
    {
        $changedBy = $this->session->userdata('emp_id');
        $empstat_id = $this->input->post('empstat_id');
        $pos_id = $this->input->post('pos_id');
        $emp_id = $this->input->post('emp_id');
        $dateFrom = $this->input->post('dateFrom');
        $acc_id = $this->general_model->fetch_specific_val('acc_id', "emp_id = $emp_id", 'tbl_employee')->acc_id;
        $accountPositionId = $this->general_model->fetch_specific_val('account_position_ID', "positions_ID=$pos_id AND accounts_ID=$acc_id", 'tbl_account_position');
        if (isset($accountPositionId->account_position_ID)) {
            $this->general_model->update_vals(array('accountPositionId' => $accountPositionId->account_position_ID), "emp_id=$emp_id", "tbl_user");
        }
        $res1 = $this->general_model->update_vals(array('isActive' => 0), "emp_id=$emp_id", "tbl_emp_promote");
        $posempstat_id = $this->general_model->fetch_specific_val('posempstat_id', "empstat_id=$empstat_id AND pos_id=$pos_id", 'tbl_pos_emp_stat')->posempstat_id;
        $res2 = $this->general_model->insert_vals(array('posempstat_id' => $posempstat_id, 'emp_id' => $emp_id, 'dateFrom' => $dateFrom, 'isActive' => 1, 'ishistory' => 1, 'changedBy' => $changedBy), "tbl_emp_promote");

        $salarymode = ($empstat_id === '1' || $empstat_id === 1) ? 'Daily' : 'Monthly';
        $this->general_model->update_vals(array('salarymode' => $salarymode), "emp_id=$emp_id", "tbl_employee");

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> changed the position of <span class='name'>$nameOf</span>");

        $status = ($res1 === 1 && $res2 === 1) ? "Success" : "Failed";
        echo json_encode(array('status' => $status, 'salarymode' => $salarymode));
    }

    public function addFamily()
    {
        $changedBy = $this->session->userdata('emp_id');
        $lname = $this->input->post('lname');
        $fname = $this->input->post('fname');
        $mname = $this->input->post('mname');
        $emp_id = $this->input->post('emp_id');
        $relation = $this->input->post('relation');
        $res = $this->general_model->insert_vals(array('fname' => $fname, 'lname' => $lname, 'mname' => $mname, 'emp_id' => $emp_id, 'relation' => $relation, 'changedBy' => $changedBy), "tbl_employee_family");

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> created a new family record for <span class='name'>$nameOf</span>");

        $family = $this->general_model->fetch_specific_vals("*", "emp_id = $emp_id", "tbl_employee_family");
        $isActive = $this->general_model->fetch_specific_val("isActive", "emp_id = $emp_id", "tbl_employee")->isActive;
        $status = ($res === 1) ? "Success" : "Failed";
        echo json_encode(array('status' => $status, 'family' => $family, 'isActive' => $isActive));
    }

    public function getDeactivateDetails()
    {
        $emp_id = $this->input->post('emp_id');
        $employee = $this->general_model->fetch_specific_val('a.lname,a.fname,b.Supervisor,b.acc_id,b.id_num', "a.apid=b.apid AND b.emp_id = $emp_id", 'tbl_applicant a,tbl_employee b');
        $account = $this->general_model->fetch_specific_val('acc_name', 'acc_id=' . $employee->acc_id, 'tbl_account');
        $supervisor = $this->general_model->fetch_specific_val('a.lname,a.fname', "a.apid=b.apid AND b.emp_id = " . $employee->Supervisor, 'tbl_applicant a,tbl_employee b');
        $startPosition = $this->general_model->fetch_specific_val('c.pos_details,a.dateFrom,d.status', "a.emp_id = $emp_id AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND a.ishistory=1", 'tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d', 'a.dateFrom ASC');
        $endPosition = $this->general_model->fetch_specific_val('c.pos_details,a.dateFrom,d.status', "a.emp_id = $emp_id AND a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND a.isActive=1", 'tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d');
        $final = array(
            'name' => $employee->fname . " " . $employee->lname,
            'id_num' => $employee->id_num,
            'team' => $account->acc_name,
            'supervisor' => $supervisor->fname . "  " . $supervisor->lname,
            'hireDate' => $startPosition->dateFrom,
            'startposition' => $startPosition->pos_details,
            'endposition' => $endPosition->pos_details,
        );
        echo json_encode(array('deactivateDetails' => $final));
    }

    public function deactivateEmployee()
    {
        $changedBy = $this->session->userdata('emp_id');
        $finalInterviewDate = $this->input->post('finalInterviewDate');
        $isRehire = $this->input->post('isRehire');
        $separationDate = $this->input->post('separationDate');
        $reasonForLeaving = $this->input->post('reasonForLeaving');
        $employeeComments = $this->input->post('employeeComments');
        $interviewerComments = $this->input->post('interviewerComments');
        $emp_id = $this->input->post('emp_id');
        $data = array(
            'isActive' => 'no',
            'finalInterviewDate' => $finalInterviewDate,
            'separationDate' => $separationDate,
            'reasonForLeaving' => $reasonForLeaving,
            'isRehire' => $isRehire,
            'interviewerComments' => $interviewerComments,
            'employeeComments' => $employeeComments,
            'changedBy' => $changedBy,
        );
        $res1 = $this->general_model->update_vals($data, "emp_id = $emp_id", "tbl_employee");
        $res2 = $this->general_model->update_vals(array('isActive' => 0), "emp_id = $emp_id", "tbl_user");

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> deactivated the system account of <span class='name'>$nameOf</span>");

        $status = ($res1 === 1 && $res2 === 1) ? "Success" : "Failed";
        echo json_encode(array('status' => $status));
    }

    public function getInactiveEmployees()
    {
        $searchname = $this->input->post('searchname');

        $references = $this->general_model->custom_query("SELECT previousEmployee_ID FROM tbl_employee_rehire_reference");
        $reference_ids = array();
        foreach ($references as $ref) {
            $reference_ids[] = $ref->previousEmployee_ID;
        }
        $references_where = (empty($reference_ids)) ? "" : "AND b.emp_id NOT IN (" . implode(',', $reference_ids) . ")";
        $employees = $this->general_model->custom_query("SELECT b.separationDate,b.emp_id, TRIM(a.fname) as fname,TRIM(a.lname) as lname FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive NOT IN ('yes') $references_where AND (a.fname LIKE '%" . $searchname . "%' OR a.lname LIKE '%" . $searchname . "%' ) ORDER BY lname ASC");
        $empids = array();
        foreach ($employees as $emp) {
            $empids[] = $emp->emp_id;
            $emp->dateFrom = null;
            $emp->checked = 'no';
        }
        if (!empty($empids)) {
            $ends = $this->general_model->custom_query("SELECT dateFrom,emp_id FROM tbl_emp_promote WHERE emp_id IN (" . implode(',', $empids) . ") ");
            foreach ($employees as $emp) {
                foreach ($ends as $key => $e) {
                    if ($e->emp_id === $emp->emp_id) {
                        $emp->dateFrom = $e->dateFrom;
                        unset($ends[$key]);
                    }
                }
            }
        }
        echo json_encode(array('employees' => $employees));
    }

    public function addPreviousAccount()
    {
        $previousEmployee_ID = $this->input->post('previousEmployee_ID');
        $currentEmployee_ID = $this->input->post('currentEmployee_ID');
        $res = $this->general_model->insert_vals(array('currentEmployee_ID' => $currentEmployee_ID, 'previousEmployee_ID' => $previousEmployee_ID), "tbl_employee_rehire_reference");

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$currentEmployee_ID", "tbl_applicant a,tbl_employee b")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> added a previous account for <span class='name'>$nameOf</span>");

        $status = ($res) ? "Success" : "Failed";
        echo json_encode(array('status' => $status));
    }

    public function deletePreviousAccount()
    {
        $previousEmployee_ID = $this->input->post('previousEmployee_ID');
        $currentEmployee_ID = $this->input->post('currentEmployee_ID');
        $res = $this->general_model->delete_vals("currentEmployee_ID = $currentEmployee_ID AND previousEmployee_ID = $previousEmployee_ID", "tbl_employee_rehire_reference");
        $status = ($res) ? "Success" : "Failed";
        echo json_encode(array('status' => $status));
    }

    public function setPrevAccounts()
    {
        $emp_id = $this->input->post('emp_id');
        $references = $this->general_model->fetch_specific_vals("previousEmployee_ID", "currentEmployee_ID=$emp_id", "tbl_employee_rehire_reference");
        $reference_ids = array();
        foreach ($references as $ref) {
            $reference_ids[] = $ref->previousEmployee_ID;
        }
        if (empty($references)) {
            $reference_ids[] = 0;
        }
        $employees = $this->general_model->custom_query("SELECT b.separationDate,b.emp_id, TRIM(a.fname) as fname,TRIM(a.lname) as lname FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.emp_id IN (" . implode(',', $reference_ids) . ") ORDER BY lname ASC");
        $empids = array();
        foreach ($employees as $emp) {
            $empids[] = $emp->emp_id;
            $emp->dateFrom = null;
        }
        if (!empty($employees)) {
            $ends = $this->general_model->custom_query("SELECT dateFrom,emp_id FROM tbl_emp_promote WHERE emp_id IN (" . implode(',', $empids) . ") ");
            foreach ($employees as $emp) {
                foreach ($ends as $key => $e) {
                    if ($e->emp_id === $emp->emp_id) {
                        $emp->dateFrom = $e->dateFrom;
                        unset($ends[$key]);
                    }
                }
            }
        }

        echo json_encode(array('employees' => $employees));
    }

    //---------------------------------PERSONAL PROFILE HERE---------------------------------------------------------------------
    public function personal()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $this->load_template_view('templates/employee/personal', $data);
    }

    public function birthday_list()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/birthday_list', $data);
        }
    }

    public function anniversary_list()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/anniversary_list', $data);
        }
    }

    public function control_panel()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/control_panel', $data);
        }
    }

    public function control_panel_admin()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/control_panel_admin', $data);
        }
    }

    public function setEnableUserUpdate()
    {
        $users = $this->input->post('users');
        $res1 = $this->general_model->update_vals(array('allowUserUpdate' => 1), " emp_id IN (" . implode(',', $users) . ")", "tbl_employee");
        $res2 = $this->general_model->update_vals(array('allowUserUpdate' => 0), " emp_id NOT IN (" . implode(',', $users) . ")", "tbl_employee");
        $status = ($res1 && $res2) ? "Success" : "Failed";
        echo json_encode(array('status' => $status));
    }

    public function saveEmployeeRequest()
    {
        $withChange = $this->input->post('withChange');
        $final = (array) $this->input->post('final_array');
        $family_array = (array) $this->input->post('family_array');
        $final_array = array_merge($final, $family_array);
        $emp_id = $this->session->userdata('emp_id');
        $res = 1;
        if ($withChange === '1') {
            $employeeRequest_ID = $this->general_model->insert_vals_last_inserted_id(array('emp_id' => $emp_id, 'finalStatus' => 2), 'tbl_employee_request');
            foreach ($final_array as $key => $final) {
                $final_array[$key]['status'] = 2;
                $final_array[$key]['employeeRequest_ID'] = $employeeRequest_ID;
            }
            $res = $this->general_model->insert_array_vals($final_array, "tbl_employee_request_details");
        }
        $res2 = $this->general_model->update_vals(array('allowUserUpdate' => 0), "emp_id=$emp_id", "tbl_employee");
        $status = ($res && $res2) ? "Success" : "Failed";
        echo json_encode(array('status' => $status));
    }

    public function getEmployeeRequest()
    {
        $employees = $this->general_model->fetch_specific_vals("c.*,a.lname,a.fname,b.emp_id", "c.finalStatus = 2 AND a.apid=b.apid AND c.emp_id=b.emp_id", "tbl_applicant a,tbl_employee b,tbl_employee_request c", "a.lname ASC");
        $names = [
            'fname' => 'First Name', 'mname' => 'Middle Name', 'lname' => 'Last Name', 'nameExt' => 'Name Extension', 'nickName' => 'Nickname', 'pic' => 'Profile Picture', 'birthday' => 'Birthday', 'birthplace' => 'Birth Place', 'gender' => 'Gender', 'religion' => 'Religion',
            'civilStatus' => 'Civil Status', 'bloodType' => 'Blood Type', 'email' => 'Email', 'educationalAttainment' => 'Educational Attainment', 'highSchool' => 'High School', 'highSchoolYear' => 'High School Year',
            'course' => 'Course', 'lastSchool' => 'Last School Attended', 'lastSchoolYear' => 'Last School Year', 'tel' => 'Telephone Number', 'cell' => 'Mobile Number', 'permanentAddress' => 'Permanent Address',
            'presentAddress' => 'Present Address', 'id_num' => 'ID Number', 'acc_id' => 'Account', 'bir' => 'BIR Number', 'philhealth' => 'Philhealth Number', 'pagibig' => 'Pag-ibig Number', 'sss' => 'SSS Number',
            'isAtm' => 'Is ATM', 'salarymode' => 'Salary Mode', 'workDaysPerWeek' => 'Work Days Per Week', 'confidential' => 'Confidential', 'supervisor' => 'Supervisor', 'emergencyFname' => 'Emergency: First Name',
            'emergencyMname' => 'Emergency: Middle Name', 'emergencyLname' => 'Emergency: Last Name', 'emergencyNameExt' => 'Emergency: Name Extension', 'emergencyContact' => 'Emergency: Contact Number', 'emergencyAddress' => 'Emergency: Address',
            'emergencyRelationship' => 'Emergency: Relationship', 'ccaExp' => 'CCA Experience', 'latestEmployer' => 'Latest Employer', 'lastPositionHeld' => 'Last Position Held', 'inclusiveDates' => 'Inclusive Dates', 'lastEmployerAddress' => 'Last Employer Address',
            'lastEmployerContact' => 'Last Employer Contact',
        ];
        $namesFamily = [
            'fname' => 'First Name', 'mname' => 'Middle Name', 'lname' => 'Last Name', 'extension' => 'Name Extension', 'relation' => 'Relationship', 'age' => 'Age', 'contactNumber' => 'Contact Number',
            'address' => 'Address', 'schoolAttended' => 'School Attended',
        ];
        $requestids = array();
        foreach ($employees as $d) {
            $requestids[] = $d->employeeRequest_ID;
        }
        $requestids = array_unique($requestids);

        if (empty($requestids)) {
            $employees = null;
        } else {
            $requests = $this->general_model->fetch_specific_vals("*", "status=2 AND employeeRequest_ID IN (" . implode(",", $requestids) . ")", "tbl_employee_request_details");
            foreach ($employees as $emp) {
                $emp->requests = array();
                foreach ($requests as $key => $request) {
                    if ($emp->employeeRequest_ID === $request->employeeRequest_ID) {
                        if ($request->type === 'normal') {
                            $emp->normal[] = $request;
                        } else {
                            $emp->family[$request->familyInstance][] = $request;
                        }

                        unset($requests[$key]);
                    }
                }
            }
        }
        echo json_encode(array('requests' => $employees, 'names' => $names, 'namesFamily' => $namesFamily));
    }

    public function approveEmployeeRequest_familyInsert()
    {
        $approver_ID = $this->session->userdata('emp_id');
        $employeeRequestDetails_IDs = $this->input->post('employeeRequestDetails_IDs');
        $employeeRequest_ID = $this->input->post('employeeRequest_ID');

        $emp_id = $this->general_model->fetch_specific_val("emp_id", "employeeRequest_ID = $employeeRequest_ID", "tbl_employee_request")->emp_id;
        $fieldvalues = $this->general_model->fetch_specific_vals("field,originalValue,newValue", "employeeRequestDetails_ID IN ( " . implode(',', $employeeRequestDetails_IDs) . ")", "tbl_employee_request_details");
        $res = array();
        $normal = array();
        $family = array();
        $arr = array('emp_id' => $emp_id);
        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
        foreach ($fieldvalues as $value) {
            $arr += [$value->field => $value->newValue]; //append associative array to existing array

            $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> approved the requested additional family member of <span class='name'>$nameOf</span>: <span class='value'>".$value->field."</span> to <span class='value'>'".$value->newValue."'</span>");
        }
        $res[] = $this->general_model->insert_vals($arr, "tbl_employee_family");
        if (array_sum($res) == count($res)) {
            $res[] = $this->general_model->update_vals(array("approver_ID" => $approver_ID, "status" => 5, 'approvalDate'=>date("Y-m-d H:i:s")), "employeeRequestDetails_ID IN ( " . implode(',', $employeeRequestDetails_IDs) . ")", "tbl_employee_request_details");
        }
        if (array_sum($res) == count($res)) {
            $request = $this->general_model->fetch_specific_vals("*", "employeeRequest_ID = $employeeRequest_ID AND status=2", "tbl_employee_request_details");
            if (!empty($request) || !$request === null) {
                foreach ($request as $req) {
                    if ($req->type === 'normal') {
                        $normal[] = $req;
                    } else {
                        $family[] = $req;
                    }
                }
            }
        }
        if (empty($normal) && empty($family)) {
            $remainingrequest = "empty";
            $res[] = $this->general_model->update_vals(array("finalStatus" => 3), "employeeRequest_ID=$employeeRequest_ID", "tbl_employee_request");
        } elseif (empty($normal)) {
            $remainingrequest = "withoutnormal";
        } elseif (empty($family)) {
            $remainingrequest = "withoutfamily";
        } else {
            $remainingrequest = "complete";
        }
        echo json_encode(array('status' => (array_sum($res) == count($res)) ? "Success" : "Failed", 'remainingrequest' => $remainingrequest));
    }

    public function approveEmployeeRequest()
    {
        $approver_ID = $this->session->userdata('emp_id');
        $employeeRequest_ID = $this->input->post('employeeRequest_ID');
        $employeeRequestDetails_ID = $this->input->post('employeeRequestDetails_ID');
        $style = $this->input->post('style');
        $type = $this->input->post('type');
        $res = array();
        $normal = array();
        $family = array();
        $emp_id = $this->general_model->fetch_specific_val("emp_id", "employeeRequest_ID = $employeeRequest_ID", "tbl_employee_request")->emp_id;
        $employeeRequest = $this->general_model->fetch_specific_val("*", "employeeRequestDetails_ID = $employeeRequestDetails_ID", "tbl_employee_request_details");

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
    
        if ($type == 'normal') {
            $employee = $this->general_model->fetch_specific_val("*", "a.apid=b.apid AND b.emp_id = " . $emp_id, "tbl_applicant a,tbl_employee b");
            $field = $employeeRequest->field;
            if ($employee->$field === $employeeRequest->newValue) {
                $res[] = 1;
            } else {
                switch ($employeeRequest->tableName) {
                    case 'tbl_employee':
                        $res[] = $this->general_model->update_vals(array($field => $employeeRequest->newValue), "emp_id=" . $emp_id, "tbl_employee");
                        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> approved the requested changes of <span class='name'>$nameOf</span>: <span class='value'>".$field."</span> from <span class='value'>".$employeeRequest->originalValue."</span>  to <span class='value'>'".$employeeRequest->newValue."'</span>");
                        break;
                    case 'tbl_applicant':
                        $resa = $this->general_model->custom_query_no_return("UPDATE tbl_applicant a, tbl_employee b SET a." . $employeeRequest->field . " = '" . $employeeRequest->newValue . "' WHERE a.apid = b.apid AND b.emp_id= " . $emp_id);
                        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> approved the requested changes of <span class='name'>$nameOf</span>: <span class='value'>".$employeeRequest->field."</span> from <span class='value'>".$employeeRequest->originalValue."</span>  to <span class='value'>'".$employeeRequest->newValue."'</span>");
                        $res[] = ($resa > 0) ? 1 : 0;
                        break;
                    default:
                        $res[] = 0;
                        break;
                }
            }
            if (array_sum($res) == count($res)) {
                $res[] = $this->general_model->update_vals(array("approver_ID" => $approver_ID, "status" => 5, 'approvalDate'=>date("Y-m-d H:i:s")), "employeeRequestDetails_ID=$employeeRequestDetails_ID", "tbl_employee_request_details");
            }
        } else {
            if ($style === 'delete') {
                $employeeFamily_ID = $employeeRequest->employeeFamily_ID;
                if ($employeeFamily_ID === null || $employeeFamily_ID === 0 || $employeeFamily_ID === '0') {
                    $res[] = 1;
                } else {
                    $res[] = $this->general_model->delete_vals("employeeFamily_ID = $employeeFamily_ID", "tbl_employee_family");
                    $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> approved the requested removal of <span class='name'>$nameOf</span>: <span class='value'>Employee Family ID</span> <span class='value'>'".$employeeFamily_ID."'</span>");
                    if (array_sum($res) == count($res)) {
                        $res[] = $this->general_model->update_vals(array("approver_ID" => $approver_ID, "status" => 5, 'approvalDate'=>date("Y-m-d H:i:s")), "employeeRequest_ID = $employeeRequest_ID AND type='family'", "tbl_employee_request_details");
                    }
                }
            } elseif ($style === 'insert') {
                $res[] = 1;
            } else { //update
                $employeeFamily_ID = $employeeRequest->employeeFamily_ID; //with entry

                $res[] = $this->general_model->update_vals(array($employeeRequest->field => $employeeRequest->newValue), "employeeFamily_ID = $employeeFamily_ID", "tbl_employee_family");
                $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> approved the requested changes of <span class='name'>$nameOf</span>: <span class='value'>".$employeeRequest->field."</span> from <span class='value'>".$employeeRequest->originalValue."</span>  to <span class='value'>'".$employeeRequest->newValue."'</span>");
                if (array_sum($res) == count($res)) {
                    $res[] = $this->general_model->update_vals(array("approver_ID" => $approver_ID, "status" => 5, 'approvalDate'=>date("Y-m-d H:i:s")), "employeeRequestDetails_ID=$employeeRequestDetails_ID", "tbl_employee_request_details");
                }
            }
        }
        if (array_sum($res) == count($res)) {
            $request = $this->general_model->fetch_specific_vals("*", "employeeRequest_ID = $employeeRequest_ID AND status=2", "tbl_employee_request_details");
            if (!empty($request) || !$request === null) {
                foreach ($request as $req) {
                    if ($req->type === 'normal') {
                        $normal[] = $req;
                    } else {
                        $family[] = $req;
                    }
                }
            }
        }
        if (empty($normal) && empty($family)) {
            $remainingrequest = "empty";
            $res[] = $this->general_model->update_vals(array("finalStatus" => 3), "employeeRequest_ID=$employeeRequest_ID", "tbl_employee_request");
        } elseif (empty($normal)) {
            $remainingrequest = "withoutnormal";
        } elseif (empty($family)) {
            $remainingrequest = "withoutfamily";
        } else {
            $remainingrequest = "complete";
        }
        echo json_encode(array('status' => (array_sum($res) == count($res)) ? "Success" : "Failed", 'remainingrequest' => $remainingrequest));
    }

    public function rejectEmployeeRequest()
    {
        $approver_ID = $this->session->userdata('emp_id');
        $employeeRequest_ID = $this->input->post('employeeRequest_ID');
        $employeeRequestDetails_ID = $this->input->post('employeeRequestDetails_ID');
        $style = $this->input->post('style'); //update/insert/delete
        $type = $this->input->post('type'); //normal/family
        $res = array();
        $normal = array();
        $family = array();
        if ($type === 'normal') {
            $res[] = $this->general_model->update_vals(array("approver_ID" => $approver_ID, "status" => 6, 'approvalDate'=>date("Y-m-d H:i:s")), "employeeRequestDetails_ID=$employeeRequestDetails_ID", "tbl_employee_request_details");
        } else { //family
            if ($style === 'delete') {
                $singlerequest = $this->general_model->fetch_specific_val("*", "employeeRequestDetails_ID=$employeeRequestDetails_ID AND status=2", "tbl_employee_request_details");
                $res[] = $this->general_model->update_vals(array("approver_ID" => $approver_ID, "status" => 6, 'approvalDate'=>date("Y-m-d H:i:s")), "employeeRequest_ID = " . $singlerequest->employeeRequest_ID . " AND familyInstance=" . $singlerequest->familyInstance . " AND type='family' AND style='delete'", "tbl_employee_request_details");
            } else {
                $res[] = $this->general_model->update_vals(array("approver_ID" => $approver_ID, "status" => 6, 'approvalDate'=>date("Y-m-d H:i:s")), "employeeRequestDetails_ID=$employeeRequestDetails_ID", "tbl_employee_request_details");
            }
        }
        if (array_sum($res) == count($res)) {
            $request = $this->general_model->fetch_specific_vals("*", "employeeRequest_ID = $employeeRequest_ID AND status=2", "tbl_employee_request_details");
            if (!empty($request) || !$request === null) {
                foreach ($request as $req) {
                    if ($req->type === 'normal') {
                        $normal[] = $req;
                    } else {
                        $family[] = $req;
                    }
                }
            }
        }
        if (empty($normal) && empty($family)) {
            $remainingrequest = "empty";
            $res[] = $this->general_model->update_vals(array("finalStatus" => 3), "employeeRequest_ID=$employeeRequest_ID", "tbl_employee_request");
        } elseif (empty($normal)) {
            $remainingrequest = "withoutnormal";
        } elseif (empty($family)) {
            $remainingrequest = "withoutfamily";
        } else {
            $remainingrequest = "complete";
        }
        echo json_encode(array('status' => (array_sum($res) == count($res)) ? "Success" : "Failed", 'remainingrequest' => $remainingrequest));
    }

    public function getEmployeeUserAccess()
    {
        $users = $this->general_model->fetch_specific_vals("a.emp_id", "a.uid=b.user_id AND b.menu_item_id=c.menu_item_id AND c.item_link='Employee/profiles'", "tbl_user a,tbl_user_access b,tbl_menu_items c");
        $empids = [];
        foreach ($users as $user) {
            $empids[] = $user->emp_id;
        }
        $empids = (empty($empids)) ? [0] : $empids;
        //        $userAccess = $this->general_model->fetch_specific_vals("a.lname,a.fname,c.*", "a.apid=b.apid AND b.emp_id = c.emp_id AND b.isActive = 'yes'", "tbl_applicant a, tbl_employee b, tbl_employee_user_access c", "a.lname ASC");
        $names = $this->general_model->fetch_specific_vals("a.lname,a.fname,b.emp_id", "a.apid=b.apid AND b.isActive = 'yes' AND b.emp_id IN (" . implode(',', $empids) . ")", "tbl_applicant a, tbl_employee b", "a.lname ASC");
        $empids = [];
        foreach ($names as $user) {
            $empids[] = $user->emp_id;
            $user->userAccess = null;
        }
        $userAccess = $this->general_model->fetch_specific_vals("*", "emp_id IN (" . implode(',', $empids) . ")", "tbl_employee_user_access ");
        $norecords = [];
        foreach ($names as $user) {
            foreach ($userAccess as $key => $access) {
                if ($user->emp_id === $access->emp_id) {
                    $user->userAccess = $access;
                    unset($userAccess[$key]);
                }
            }
        }
        foreach ($names as $key => $user) {
            if ($user->userAccess === null) {
                $norecords[] = $user;
                unset($names[$key]);
            }
        }
        echo json_encode(array('users' => $names, 'norecords' => $norecords));
    }

    public function updateEmployeeUserAccess()
    {
        $value = $this->input->post('value');
        $field = $this->input->post('field');
        $emp_id = $this->input->post('emp_id');
        $employeeUserAccess_ID = $this->input->post('employeeUserAccess_ID');
        $res = $this->general_model->update_vals([$field => $value], "employeeUserAccess_ID=$employeeUserAccess_ID", "tbl_employee_user_access");

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=$emp_id", "tbl_applicant a,tbl_employee b")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> updated the user access <span class='value'>$field</span>  of <span class='name'>$nameOf</span>  to <span class='value'>'$value'</span>");

        echo json_encode(['status' => ($res) ? "Success" : "Failed"]);
    }

    public function insertEmployeeUserAccess()
    {
        $changedBy = $this->session->userdata('emp_id');
        $data = $this->input->post('values');
        $data['changedBy'] = $changedBy;
        $data['isActive'] = 1;
        $res = $this->general_model->insert_vals($data, 'tbl_employee_user_access');

        $nameOf = $this->general_model->fetch_specific_val("CONCAT(a.lname,', ',a.fname) as name", "a.apid=b.apid AND b.emp_id=" . $data['emp_id'], "tbl_applicant a,tbl_employee b")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> assigned <span class='name'>$nameOf</span> as a new user for EMS");

        echo json_encode(['status' => ($res) ? "Success" : "Failed"]);
    }

    public function getLogHistory()
    {
        $datestart = $this->input->post('datestart');
        $dateend = $this->input->post('dateend');
        $query = "SELECT * FROM tbl_log_history WHERE createdOn between '$datestart' and  '$dateend' ORDER BY createdOn DESC";
        $history = $this->general_model->custom_query($query);
        echo json_encode(array('history' => $history));
    }

    public function getBirthdayList()
    {
        $searchname = $this->input->post('searchname');
        $classtype = $this->input->post('classtype');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        $searchString = "";
        if ($searchname !== '') {
            $searchString = "AND (a.lname LIKE '%$searchname%' OR a.fname LIKE '%$searchname%'  OR a.mname LIKE '%$searchname%')";
        }
        if ($classtype === 'Agent' || $classtype === 'Admin') {
            $birthdays = $this->general_model->fetch_specific_vals("a.lname,a.fname,a.pic,a.birthday,TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) AS age", "a.apid=b.apid AND b.isActive = 'yes' $searchString AND  MONTH(a.birthday) BETWEEN MONTH('$startdate') AND MONTH('$enddate') AND DAYOFYEAR(a.birthday) BETWEEN DAYOFYEAR('$startdate') AND DAYOFYEAR('$enddate') AND b.acc_id=c.acc_id AND c.acc_description ='$classtype'", "tbl_applicant a,tbl_employee b,tbl_account c ");
        } else {
            $birthdays = $this->general_model->fetch_specific_vals("a.lname,a.fname,a.pic,a.birthday,TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) AS age", "a.apid=b.apid AND b.isActive = 'yes' $searchString AND   MONTH(a.birthday) BETWEEN MONTH('$startdate') AND MONTH('$enddate') AND DAYOFYEAR(a.birthday) BETWEEN DAYOFYEAR('$startdate') AND DAYOFYEAR('$enddate')", "tbl_applicant a,tbl_employee b ");
        }
        echo json_encode(['birthdays' => $birthdays]);
    }

    public function getEmployeeForExports()
    {
        $searchactive = $this->input->post('searchactive');
        $search = array();
        $searchString = "";
        if ($searchactive !== '') {
            $search[] = "b.isActive='$searchactive'";
        }

        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $references = $this->general_model->custom_query("SELECT previousEmployee_ID FROM tbl_employee_rehire_reference");
        $reference_ids = array();
        foreach ($references as $ref) {
            $reference_ids[] = $ref->previousEmployee_ID;
        }
        $referenceString = (empty($reference_ids)) ? "" : "AND b.emp_id NOT IN (" . implode(',', $reference_ids) . ")";
        $query = "SELECT TRIM(a.fname) as fname,TRIM(a.lname) as lname,b.emp_id,b.isActive FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.acc_id=c.acc_id " . $searchString . " " . $referenceString . " ORDER BY lname ASC";

        $employees = $this->general_model->custom_query($query);
        echo json_encode(['employees' => $employees]);
    }

    public function downloadExcel()
    {
        $employeelist = $this->input->post('emp_ids');
        if ($employeelist === null || $employeelist === '') {
            echo "Failed";
        } else {
            $emp_ids = explode("-", $employeelist);
            $query = "SELECT * FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND  b.emp_id IN (" . implode(",", $emp_ids) . ") ORDER BY lname ASC";

            $employees = $this->general_model->custom_query($query);
            $regulationDates = $this->general_model->custom_query("SELECT a.dateFrom,a.emp_id FROM tbl_emp_promote a,tbl_pos_emp_stat b WHERE a.emp_id IN (" . implode(",", $emp_ids) . ") AND a.posempstat_id = b.posempstat_id AND b.empstat_id = 3 AND a.ishistory = 1 GROUP BY a.emp_id ORDER BY a.dateFrom ASC");
            $probiDates = $this->general_model->custom_query("SELECT a.dateFrom,a.emp_id FROM tbl_emp_promote a,tbl_pos_emp_stat b WHERE a.emp_id IN (" . implode(",", $emp_ids) . ") AND a.posempstat_id = b.posempstat_id AND b.empstat_id = 2 AND a.ishistory = 1 GROUP BY a.emp_id ORDER BY a.dateFrom ASC");

            foreach ($employees as $emp) {
                $emp->regularDate = '';
                $emp->probiDate = '';
                foreach ($probiDates as $probiKey => $probi) {
                    if ($emp->emp_id == $probi->emp_id) {
                        $emp->probiDate = $probi->dateFrom;
                        unset($probiDates[$probiKey]);
                    }
                }
                foreach ($regulationDates as $regularKey => $regulation) {
                    if ($emp->emp_id == $regulation->emp_id) {
                        $emp->regularDate = $regulation->dateFrom;
                        unset($regulationDates[$regularKey]);
                    }
                }
            }
            //EXCEL PROPER
            $this->load->library('PHPExcel', null, 'excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Employee Records');
            $this->excel->getActiveSheet()->setShowGridlines(false);
            //------------------------INSERT LOGO-------------------------------------------
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($this->logo);
            $objDrawing->setOffsetX(0); // setOffsetX works properly
            $objDrawing->setOffsetY(5); //setOffsetY has no effect
            $objDrawing->setCoordinates('B1');
            $objDrawing->setHeight(80); // logo height
            // $objDrawing->setWidth(320); // logo width
            // $objDrawing->setWidthAndHeight(200,400);
            $objDrawing->setResizeProportional(true);
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
            $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
            $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
            $this->excel->getActiveSheet()->setCellValue('A1', 'CODE-SZ_EMSExport');
            $this->excel->getActiveSheet()->setCellValue('A3', 'Profile Code');
            $this->excel->getActiveSheet()->setCellValue('B3', 'ID Number');
            $this->excel->getActiveSheet()->setCellValue('C3', 'Last Name');
            $this->excel->getActiveSheet()->setCellValue('D3', 'First Name');
            $this->excel->getActiveSheet()->setCellValue('E3', 'Middle Name');
            $this->excel->getActiveSheet()->setCellValue('F3', 'Name Extension');
            $this->excel->getActiveSheet()->setCellValue('G3', 'Nickname');
            $this->excel->getActiveSheet()->setCellValue('H3', 'Birthday');
            $this->excel->getActiveSheet()->setCellValue('I3', 'Birthplace');
            $this->excel->getActiveSheet()->setCellValue('J3', 'Age');
            $this->excel->getActiveSheet()->setCellValue('K3', 'Religion');
            $this->excel->getActiveSheet()->setCellValue('L3', 'Blood Type');
            $this->excel->getActiveSheet()->setCellValue('M3', 'Gender');
            $this->excel->getActiveSheet()->setCellValue('N3', 'Civil Status');
            $this->excel->getActiveSheet()->setCellValue('O3', 'Present Address');
            $this->excel->getActiveSheet()->setCellValue('P3', 'Permanent Address');
            $this->excel->getActiveSheet()->setCellValue('Q3', 'Telephone Number');
            $this->excel->getActiveSheet()->setCellValue('R3', 'Mobile Number');
            $this->excel->getActiveSheet()->setCellValue('S3', 'Email Address');
            $this->excel->getActiveSheet()->setCellValue('T3', 'SSS Number');
            $this->excel->getActiveSheet()->setCellValue('U3', 'Philhealth Number');
            $this->excel->getActiveSheet()->setCellValue('V3', 'Pag-ibig Number');
            $this->excel->getActiveSheet()->setCellValue('W3', 'TIN/BIR Number');
            $this->excel->getActiveSheet()->setCellValue('X3', 'Educational Attainment');
            $this->excel->getActiveSheet()->setCellValue('Y3', 'High School Attended');
            $this->excel->getActiveSheet()->setCellValue('Z3', 'Year Graduated');
            $this->excel->getActiveSheet()->setCellValue('AA3', 'Last School Attended');
            $this->excel->getActiveSheet()->setCellValue('AB3', 'Year Left');
            $this->excel->getActiveSheet()->setCellValue('AC3', 'Course');
            $this->excel->getActiveSheet()->setCellValue('AD3', 'Emergency Last Name');
            $this->excel->getActiveSheet()->setCellValue('AE3', 'Emergency First Name');
            $this->excel->getActiveSheet()->setCellValue('AF3', 'Emergency Name Extension');
            $this->excel->getActiveSheet()->setCellValue('AG3', 'Emergency Middle Name');
            $this->excel->getActiveSheet()->setCellValue('AH3', 'Emergency Mobile Number');
            $this->excel->getActiveSheet()->setCellValue('AI3', 'Emergency Address');
            $this->excel->getActiveSheet()->setCellValue('AJ3', 'Emergency Relationship');
            $this->excel->getActiveSheet()->setCellValue('AK3', 'With CCA Experience');
            $this->excel->getActiveSheet()->setCellValue('AL3', 'Latest Employer');
            $this->excel->getActiveSheet()->setCellValue('AM3', 'Last Position Held');
            $this->excel->getActiveSheet()->setCellValue('AN3', 'Inclusive Dates');
            $this->excel->getActiveSheet()->setCellValue('AO3', 'Last Employer Address');
            $this->excel->getActiveSheet()->setCellValue('AP3', 'Last Employer Contact');
            $this->excel->getActiveSheet()->setCellValue('AQ3', 'Probationary Date');
            $this->excel->getActiveSheet()->setCellValue('AR3', 'Regularization Date');
            $this->excel->getActiveSheet()->setCellValue('AS3', 'Status');
            $this->excel->getActiveSheet()->setCellValue('AT3', 'Separation Date');

            $emprow = 4;
            foreach ($employees as $emp) {
                $age = date_diff(date_create($emp->birthday), date_create('now'))->y;
                $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $emp->emp_id);
                $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $emp->id_num);
                $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $emp->lname);
                $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $emp->fname);
                $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $emp->mname);
                $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $emp->nameExt);
                $this->excel->getActiveSheet()->setCellValue('G' . $emprow, $emp->nickName);
                $this->excel->getActiveSheet()->setCellValue('H' . $emprow, $emp->birthday);
                $this->excel->getActiveSheet()->setCellValue('I' . $emprow, $emp->birthplace);
                $this->excel->getActiveSheet()->setCellValue('J' . $emprow, $age);
                $this->excel->getActiveSheet()->setCellValue('K' . $emprow, $emp->religion);
                $this->excel->getActiveSheet()->setCellValue('L' . $emprow, $emp->bloodType);
                $this->excel->getActiveSheet()->setCellValue('M' . $emprow, $emp->gender);
                $this->excel->getActiveSheet()->setCellValue('N' . $emprow, $emp->civilStatus);
                $this->excel->getActiveSheet()->setCellValue('O' . $emprow, $emp->presentAddress);
                $this->excel->getActiveSheet()->setCellValue('P' . $emprow, $emp->permanentAddress);
                $this->excel->getActiveSheet()->setCellValue('Q' . $emprow, $emp->tel);
                $this->excel->getActiveSheet()->setCellValue('R' . $emprow, $emp->cell);
                $this->excel->getActiveSheet()->setCellValue('S' . $emprow, $emp->email);
                $this->excel->getActiveSheet()->setCellValue('T' . $emprow, $emp->sss);
                $this->excel->getActiveSheet()->setCellValue('U' . $emprow, $emp->philhealth);
                $this->excel->getActiveSheet()->setCellValue('V' . $emprow, $emp->pagibig);
                $this->excel->getActiveSheet()->setCellValue('W' . $emprow, $emp->bir);
                $this->excel->getActiveSheet()->setCellValue('X' . $emprow, $emp->educationalAttainment);
                $this->excel->getActiveSheet()->setCellValue('Y' . $emprow, $emp->highSchool);
                $this->excel->getActiveSheet()->setCellValue('Z' . $emprow, $emp->highSchoolYear);
                $this->excel->getActiveSheet()->setCellValue('AA' . $emprow, $emp->lastSchool);
                $this->excel->getActiveSheet()->setCellValue('AB' . $emprow, $emp->lastSchoolYear);
                $this->excel->getActiveSheet()->setCellValue('AC' . $emprow, $emp->course);
                $this->excel->getActiveSheet()->setCellValue('AD' . $emprow, $emp->emergencyLname);
                $this->excel->getActiveSheet()->setCellValue('AE' . $emprow, $emp->emergencyFname);
                $this->excel->getActiveSheet()->setCellValue('AF' . $emprow, $emp->emergencyNameExt);
                $this->excel->getActiveSheet()->setCellValue('AG' . $emprow, $emp->emergencyMname);
                $this->excel->getActiveSheet()->setCellValue('AH' . $emprow, $emp->emergencyContact);
                $this->excel->getActiveSheet()->setCellValue('AI' . $emprow, $emp->emergencyAddress);
                $this->excel->getActiveSheet()->setCellValue('AJ' . $emprow, $emp->emergencyRelationship);
                $this->excel->getActiveSheet()->setCellValue('AK' . $emprow, ($emp->ccaExp === '1') ? "Yes" : "No");
                $this->excel->getActiveSheet()->setCellValue('AL' . $emprow, $emp->latestEmployer);
                $this->excel->getActiveSheet()->setCellValue('AM' . $emprow, $emp->lastPositionHeld);
                $this->excel->getActiveSheet()->setCellValue('AN' . $emprow, $emp->inclusiveDates);
                $this->excel->getActiveSheet()->setCellValue('AO' . $emprow, $emp->lastEmployerAddress);
                $this->excel->getActiveSheet()->setCellValue('AP' . $emprow, $emp->lastEmployerContact);
                $this->excel->getActiveSheet()->setCellValue('AQ' . $emprow, $emp->probiDate);
                $this->excel->getActiveSheet()->setCellValue('AR' . $emprow, $emp->regularDate);
                $this->excel->getActiveSheet()->setCellValue('AS' . $emprow, ($emp->isActive === 'yes') ? 'Active' : 'Inactive');
                $this->excel->getActiveSheet()->setCellValue('AT' . $emprow, $emp->separationDate);

                $emprow++;
            }

            $column = $this->excel->getActiveSheet()->getHighestColumn();
            $row = $this->excel->getActiveSheet()->getHighestRow();
            $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '4b4c54')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
            //        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
            $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
            $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
            $this->excel->getActiveSheet()->getProtection()->setSheet(true);
            $this->excel->getActiveSheet()->freezePane('E4');

            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(23);
            $excessletters = ['D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT'];
            foreach ($excessletters as $l) {
                $this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);
            }

            $filename = 'SZ_EMSExport.xlsx'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }
    }
    public function applicant()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/applicant', $data);
        }
    }

    public function get_all_position_per_account()
    {
        $acc_id = $this->input->post('acc_id');
        $positions = $this->general_model->custom_query("SELECT d.pos_id,d.pos_details FROM tbl_account a,tbl_department b, tbl_pos_dep c,tbl_position d WHERE a.acc_id = $acc_id AND a.dep_id = b.dep_id AND b.dep_id = c.dep_id AND c.pos_id = d.pos_id ORDER BY d.pos_details ASC");
        echo json_encode(['positions' => $positions]);
    }

    public function failApplicant()
    {
        $evaluatedBy = $this->session->uid;
        $apid = $this->input->post('apid');
        $failReason = $this->input->post('failReason');
        $res = $this->general_model->update_vals(['failReason' => $failReason, 'isEmployed' => 0, 'isActive' => 0, 'evaluatedBy' => $evaluatedBy], "apid=$apid", "tbl_applicant");
        echo json_encode(['status' => ($res) ? 'Success' : 'Failed']);
    }

    public function passApplicant()
    {
        $changedBy = $this->session->uid;
        $acc_id = $this->input->post('acc_id');
        $pos_id = $this->input->post('pos_id');
        $empstat_id = $this->input->post('empstat_id');
        $startdate = $this->input->post('startdate');
        $apid = $this->input->post('apid');
        $supervisor_id = $this->input->post('supervisor_id');

        $posempstat = $this->general_model->fetch_specific_val("posempstat_id", "pos_id=$pos_id AND empstat_id=$empstat_id", "tbl_pos_emp_stat");

        $posempstat_id = (isset($posempstat->posempstat_id)) ? $posempstat->posempstat_id : null;

        $username = "sz-" . time();
        $password = "sz123"; //DEFAULT PASSWORD
        $salarymode = ($empstat_id === '1' || $empstat_id === 1) ? 'Daily' : 'Monthly';
        if ($posempstat_id === null) {
            $resfinal = 0;
            $withrate = false;
        } else {
            $withrate = true;
            $res1 = $this->general_model->update_vals(['isEmployed' => 1, 'isActive' => 0, 'evaluatedBy' => $changedBy], "apid=$apid", "tbl_applicant");
            if ($res1) {
                $emp_id = $this->general_model->insert_vals_last_inserted_id(['apid' => $apid, 'acc_id' => $acc_id, 'Supervisor' => $supervisor_id,'isAtm' => 0, 'salarymode' => $salarymode, 'workDaysPerWeek' => 5, 'confidential' => 0, 'isActive' => 'yes', 'isFlexiSched' => 0, 'allowUserUpdate' => 0, 'changedBy' => $changedBy], "tbl_employee");
                if ($emp_id === 0 || $emp_id === null) {
                    $resfinal = 0;
                } else {
                    $accountPosition = $this->general_model->fetch_specific_val("account_position_ID", "accounts_ID=$acc_id AND positions_ID = $pos_id", "tbl_account_position");
                    $accountPositionId = (isset($accountPosition->account_position_ID)) ? $accountPosition->account_position_ID : null;
                    $depcode = $this->general_model->fetch_specific_val("b.dep_name", "a.acc_id=$acc_id AND a.dep_id = b.dep_id", "tbl_account a,tbl_department b");
                    $dep_name = (isset($depcode->dep_name)) ? $depcode->dep_name : null;

                    $role_id = 3;
                    $role = "Employee|" . $dep_name;
                    $res2 = $this->general_model->insert_vals(['username' => $username, 'password' => $password, 'password_hash' => sha1($password), 'role' => $role, 'emp_id' => $emp_id, 'role_id' => $role_id, 'isActive' => 1, 'settings' => 'bg-black font-inverse', 'accountPositionId' => $accountPositionId], "tbl_user");
                    if ($res2 && $posempstat_id != null) {
                        $rate = $this->general_model->fetch_specific_val("rate_id,rate", "posempstat_id = $posempstat_id AND isActive = 1", "tbl_rate ");
                        if (!isset($rate->rate_id)) {
                            $this->general_model->insert_vals(['posempstat_id'=>$posempstat_id, 'rate'=>0, 'date_effect'=> date('Y-m-d'), 'isActive' => 1 , 'date_added'=>date('Y-m-d H:i:s'),'added_by'=> $changedBy, 'details' => 'from passing applicant'], 'tbl_rate');
                        }

                        $resfinal = $this->general_model->insert_vals(['posempstat_id' => $posempstat_id, 'emp_id' => $emp_id, 'dateFrom' => $startdate, 'isActive' => 1, 'ishistory' => 1, 'changedBy' => $changedBy], 'tbl_emp_promote');
                    } else {
                        $resfinal = 0;
                    }
                }
            } else {
                $resfinal = 0;
            }
        }

        echo json_encode(['status' => ($resfinal) ? 'Success' : 'Failed', 'username' => $username, 'password' => $password, 'withrate' => $withrate]);
    }

    public function getEmployeeApplicants()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
        $searchname = $this->input->post('searchname');
        $search = array();
        $searchString = "";
        if ($searchname !== '') {
            $search[] = "(a.fname LIKE \"%$searchname%\" OR a.lname LIKE \"%$searchname%\")";
        }
        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $query = "SELECT a.fname,a.mname,a.lname,a.pic,b.pos_details,a.apid FROM tbl_applicant a LEFT JOIN tbl_position b ON a.pos_id = b.pos_id WHERE a.isActive = 1 $searchString ORDER BY lname ASC";

        $employees = $this->general_model->custom_query($query . " LIMIT $limiter,$perPage");
        $allactiveemployees = $this->general_model->custom_query($query);
        $count = COUNT($allactiveemployees);
        echo json_encode(array('employees' => $employees, 'total' => $count));
    }

    public function profilePreviewApplicants()
    {
        $apid = $this->input->post('apid');
        $employee = $this->general_model->custom_query("SELECT a.*,b.pos_details FROM tbl_applicant a LEFT JOIN tbl_position b ON a.pos_id = b.pos_id WHERE a.isActive = 1 AND a.apid=$apid");
        $profile = (empty($employee)) ? null : $employee[0];
        echo json_encode(array('profile' => $profile));
    }

    public function employeeDetailsApplicants()
    {
        $apid = $this->input->post('apid');
        $employee = $this->general_model->custom_query("SELECT a.*,b.pos_details FROM tbl_applicant a LEFT JOIN tbl_position b ON a.pos_id = b.pos_id WHERE a.isActive = 1 AND a.apid=$apid");
        $profile = (empty($employee)) ? null : $employee[0];
        echo json_encode(array('employee' => $profile));
    }

    public function updateApplicantProfile()
    {
        $original_value = $this->input->post('original_value');
        $name = $this->input->post('name');
        $value = $this->input->post('value');
        $new_value = (strlen($value) === 0) ? 'NULL' : "'$value'";
        $apid = $this->input->post('apid');
        $query = "UPDATE tbl_applicant SET $name = " . $new_value . " WHERE apid=$apid";
        $nameOf = $this->general_model->fetch_specific_val("CONCAT(lname,', ',fname) as name", "apid=$apid", "tbl_applicant")->name;
        $this->create_log_history(5, $this->session->emp_id, "<span class='name'>" . $this->session->fname . "  " . $this->session->lname . "</span> updated the <span class='value'>$name</span>  of <span class='name'>$nameOf</span> from <span class='value'>'$original_value'</span> to <span class='value'>'$new_value'</span> from Applicant");

        $res = $this->general_model->custom_query_no_return($query);

        $employee = $this->general_model->fetch_specific_val("fname,lname,b.pos_details", "apid=$apid AND a.pos_id = b.pos_id", "tbl_applicant a,tbl_position b");
        $status = ($res > 0) ? "Success" : "Failed";
        echo json_encode(array('status' => $status, 'employee' => $employee));
    }

    public function downloadPDF($base64 = null)
    {
        if ($base64 === null || $base64 === '') {
            echo "Failed";
        } else {
            $employeelist = base64_decode($base64);
            $emp_ids = explode("-", $employeelist);
            $query = "SELECT a.fname,a.mname,a.lname,a.nameExt,a.pic,a.birthday,a.gender,a.religion,a.civilStatus,a.email,a.tel,a.cell,a.permanentAddress,a.presentAddress,b.bir,b.philhealth,b.pagibig,b.sss,b.Supervisor,b.educationalAttainment,b.course,b.lastSchoolYear,b.lastSchool,b.highSchoolYear,b.highSchool,c.acc_name,d.dep_details,e.dateFrom,g.pos_details,h.status,i.code FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_department d,tbl_emp_promote e,tbl_pos_emp_stat f,tbl_position g,tbl_emp_stat h,tbl_site i WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND  b.emp_id IN (" . implode(",", $emp_ids) . ") AND c.dep_id = d.dep_id AND b.emp_id = e.emp_id AND e.posempstat_id=f.posempstat_id AND f.pos_id=g.pos_id AND f.empstat_id = h.empstat_id AND e.isActive = 1 AND e.isHistory = 1 AND g.site_ID = i.site_id ORDER BY lname ASC";
            $employees = $this->general_model->custom_query($query);

            //load mPDF library
            $this->load->library('m_pdf');
            //load the pdf.php by passing our data and get all data in $html varriable.
            $supervisors_id = [];
            foreach ($employees as $emp) {
                if ($emp->Supervisor !== null && $emp->Supervisor !== '') {
                    $supervisors_id[] = $emp->Supervisor;
                } else {
                    $emp->supervisor_name = null;
                    $emp->supervisor_email = null;
                }
                if (!@getimagesize($emp->pic)) {
                    // $emp->pic = 'assets/images/img/sz_thumbnail.png';
                    $emp->pic = 'assets/images/img/sz.png';
                } else {
                    // $pic = explode('.', $emp->pic);
                    // $emp->pic = $pic[0] . '_thumbnail.jpg';
                }
            }
            if (!empty($supervisors_id)) {
                $supervisors = $this->general_model->custom_query("SELECT a.fname,a.lname,a.email,b.emp_id FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.emp_id IN (" . implode(",", $supervisors_id) . ")");
                foreach ($employees as $emp) {
                    foreach ($supervisors as $sup) {
                        if ($emp->Supervisor === $sup->emp_id) {
                            $emp->supervisor_name = $sup->lname . ', ' . $sup->fname;
                            $emp->supervisor_email = $sup->email;
                        }
                    }
                }
            }
            $data['employees'] = $employees;
            $html = $this->load->view('templates/employee/pdf_template', $data, true);

            $pdfFilePath = "SZ-EMSPrintable.pdf";

            try {
                //actually, you can pass mPDF parameter on this load() function
                $pdf = $this->m_pdf->load();
                $pdf->debug = true;
                //generate the PDF!
                $this->bootstrapcss = ($this->bootstrapcss === '') ? file_get_contents('assets/src/custom/css/w3.css') : $this->bootstrapcss;
                $stylesheet = '<style>' . $this->bootstrapcss . '</style>';
                // apply external css
                $pdf->WriteHTML($stylesheet, 1);
                $pdf->WriteHTML($html, 2);
                //offer it to user via browser download! (The PDF won't be saved on your server HDD)
                header("Content-type:application/pdf");

                // It will be called downloaded.pdf
                header("Content-Disposition:inline;filename='" . $pdfFilePath . "'");
                $pdf->Output($pdfFilePath, "I");
            } catch (\Mpdf\MpdfException $e) {
                echo $e->getMessage();
            }
            exit;
        }
    }

    public function getEmployeesForResolution($uid = null)
    {
        $select = 'b.emp_id,a.fname ,a.mname,a.lname,a.nickName,a.pic,a.birthday,a.birthplace,a.gender,a.religion,a.civilStatus,a.bloodType,a.email,a.tel,a.cell,a.permanentAddress,a.presentAddress,'
            . 'b.id_num,b.acc_id,b.bir,b.philhealth,b.pagibig,b.sss,b.isAtm,b.salarymode,b.workDaysPerWeek,b.confidential,b.Supervisor as supervisor,b.emergencyFname,b.emergencyLname,b.emergencyMname,'
            . 'b.emergencyContact,b.emergencyAddress,b.emergencyRelationship';
        $names = [
            'fname' => 'First Name', 'mname' => 'Middle Name', 'lname' => 'Last Name', 'nickName' => 'Nickname', 'pic' => 'Profile Picture', 'birthday' => 'Birthday', 'birthplace' => 'Birth Place', 'gender' => 'Gender', 'religion' => 'Religion',
            'civilStatus' => 'Civil Status', 'bloodType' => 'Blood Type', 'email' => 'Email', 'tel' => 'Telephone Number', 'cell' => 'Mobile Number', 'permanentAddress' => 'Permanent Address',
            'presentAddress' => 'Present Address', 'id_num' => 'ID Number', 'acc_id' => 'Account', 'bir' => 'BIR Number', 'philhealth' => 'Philhealth Number', 'pagibig' => 'Pag-ibig Number', 'sss' => 'SSS Number',
            'isAtm' => 'Is ATM', 'salarymode' => 'Salary Mode', 'workDaysPerWeek' => 'Work Days Per Week', 'confidential' => 'Confidential', 'supervisor' => 'Supervisor', 'emergencyFname' => 'Emergency: First Name',
            'emergencyMname' => 'Emergency: Middle Name', 'emergencyLname' => 'Emergency: Last Name', 'emergencyContact' => 'Emergency: Contact Number', 'emergencyAddress' => 'Emergency: Address',
            'emergencyRelationship' => 'Emergency: Relationship',
        ];
        $important = [
            'id_num', 'bir', 'philhealth', 'pagibig', 'sss', 'permanentAddress', 'presentAddress', 'bloodType', 'cell',
            'fname', 'mname', 'lname', 'pic', 'birthday', 'gender',
            'civilStatus', 'email', 'id_num', 'acc_id',
            'isAtm', 'salarymode', 'workDaysPerWeek', 'confidential', 'supervisor',
        ];
        if ($uid === null) {
            $data = $this->general_model->custom_query("SELECT $select FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive = 'yes'");
        } else {
            $data = $this->general_model->custom_query("SELECT $select FROM tbl_applicant a,tbl_employee b,tbl_user c WHERE a.apid=b.apid AND b.isActive = 'yes' AND b.emp_id = c.emp_id AND c.uid = $uid");
        }

        $result = array();
        if (!empty($data)) {
            foreach ($data[0] as $key => $d) {
                $result[$key] = array('emp_ids' => [], 'count' => 0);
            }
            foreach ($data as $d) {
                foreach ($d as $field => $value) {
                    if ($value === '' || $value === null || $value === '0000-00-00') {
                        $result[$field]['count']++;
                        $result[$field]['emp_ids'][] = $d->emp_id;
                    }
                }
            }
            foreach ($result as $key => $res) {
                if ($res['count'] === 0) {
                    unset($result[$key]);
                }
            }
        }
        ksort($result, 2);
        echo json_encode(['total' => COUNT($result), 'result' => $result, 'names' => $names, 'important' => $important]);
    }

    public function compareOldPassword()
    {
        $uid = $this->session->userdata('uid');
        $password = $this->input->post('value');
        $dbpassword = $this->general_model->fetch_specific_val("password_hash", "uid=$uid", "tbl_user")->password_hash;
        if (strcmp(sha1($password), $dbpassword) == 0) {
            $status = "Success";
        } else {
            $status = "Failed";
        }
        echo json_encode(['status' => $status]);
    }

    public function changePassword()
    {
        $uid = $this->session->userdata('uid');
        $password = $this->input->post('password');
        $res = $this->general_model->update_vals(['password' => $password, 'password_hash' => sha1($password)], "uid=$uid", "tbl_user");
        if ($res) {
            $status = "Success";
        } else {
            $status = "Failed";
        }
        echo json_encode(['status' => $status]);
    }

    public function getMissingList()
    {
        $emp_ids = $this->input->post('emp_ids');
        $result = $this->general_model->fetch_specific_vals("a.fname,a.lname,b.emp_id", "a.apid=b.apid AND b.emp_id IN ($emp_ids)", "tbl_applicant a,tbl_employee b", "a.lname");
        echo json_encode(['missing' => $result]);
    }
    //NEW CODES STARTS HERE--------------------------------------------------------------------------------------------------------------------------------

    public function file_download()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        // if ($this->check_access()) {
        $this->load_template_view('templates/employee/file_download', $data);
        // }
    }
    public function create_applicant()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $data['departments'] = $this->general_model->custom_query("SELECT dep_id,dep_name,dep_details FROM tbl_department ORDER BY dep_details ASC");
        $data['sites'] = $this->general_model->fetch_all("site_ID,code,siteName", "tbl_site");

        if ($this->check_access()) {
            $this->load_template_view('templates/employee/create_applicant', $data);
        }
    }

    public function getApplicantForExports()
    {
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $query = "SELECT TRIM(fname) as fname,TRIM(lname) as lname, apid FROM tbl_applicant WHERE DATE(createdOn) between DATE('$startDate') and DATE('$endDate') ORDER BY lname ASC";
        $applicants = $this->general_model->custom_query($query);
        echo json_encode(['applicants' => $applicants]);
    }

    public function downloadApplicantExcel()
    {
        $applicantlist = $this->input->post('apids');
        if ($applicantlist === null || $applicantlist === '') {
            echo "Failed";
        } else {
            $apids = explode("-", $applicantlist);
            // $query = "SELECT a.*,b.pos_details FROM tbl_applicant a LEFT JOIN tbl_position b ON a.pos_id = b.pos_id WHERE a.apid IN (" . implode(",", $apids) . ") ORDER BY a.lname ASC";
            $applicants = $this->general_model->custom_query("SELECT * FROM tbl_applicant WHERE apid IN (" . implode(",", $apids) . ") ORDER BY lname ASC");
            $positions = $this->general_model->custom_query("SELECT pos_id,pos_details FROM tbl_position");
            foreach ($applicants as $app) {
                $app->pos_details = "";
                foreach ($positions as $pos) {
                    if ($app->pos_id === $pos->pos_id) {
                        $app->pos_details = $pos->pos_details;
                    }
                }
            }
            //EXCEL PROPER
            $this->load->library('PHPExcel', null, 'excel');
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Applicant Records');
            $this->excel->getActiveSheet()->setShowGridlines(false);
            //------------------------INSERT LOGO-------------------------------------------
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $objDrawing->setPath($this->logo);
            $objDrawing->setOffsetX(0); // setOffsetX works properly
            $objDrawing->setOffsetY(5); //setOffsetY has no effect
            $objDrawing->setCoordinates('B1');
            $objDrawing->setHeight(80); // logo height
            // $objDrawing->setWidth(320); // logo width
            // $objDrawing->setWidthAndHeight(200,400);
            $objDrawing->setResizeProportional(true);
            $objDrawing->setWorksheet($this->excel->getActiveSheet());
            $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
            $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
            $this->excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
            $this->excel->getActiveSheet()->setCellValue('A1', 'CODE-SZ_EMSExport');
            $this->excel->getActiveSheet()->setCellValue('A3', 'Profile Code');
            $this->excel->getActiveSheet()->setCellValue('B3', 'Last Name');
            $this->excel->getActiveSheet()->setCellValue('C3', 'First Name');
            $this->excel->getActiveSheet()->setCellValue('D3', 'Middle Name');
            $this->excel->getActiveSheet()->setCellValue('E3', 'Name Extension');
            $this->excel->getActiveSheet()->setCellValue('F3', 'Nickname');
            $this->excel->getActiveSheet()->setCellValue('G3', 'Birthday');
            $this->excel->getActiveSheet()->setCellValue('H3', 'Birthplace');
            $this->excel->getActiveSheet()->setCellValue('I3', 'Age');
            $this->excel->getActiveSheet()->setCellValue('J3', 'Religion');
            $this->excel->getActiveSheet()->setCellValue('K3', 'Blood Type');
            $this->excel->getActiveSheet()->setCellValue('L3', 'Gender');
            $this->excel->getActiveSheet()->setCellValue('M3', 'Civil Status');
            $this->excel->getActiveSheet()->setCellValue('N3', 'Present Address');
            $this->excel->getActiveSheet()->setCellValue('O3', 'Permanent Address');
            $this->excel->getActiveSheet()->setCellValue('P3', 'Telephone Number');
            $this->excel->getActiveSheet()->setCellValue('Q3', 'Mobile Number');
            $this->excel->getActiveSheet()->setCellValue('R3', 'Email Address');
            $this->excel->getActiveSheet()->setCellValue('S3', 'Character Reference 1');
            $this->excel->getActiveSheet()->setCellValue('T3', 'Character Reference 2');
            $this->excel->getActiveSheet()->setCellValue('U3', 'Character Reference 3');
            $this->excel->getActiveSheet()->setCellValue('V3', 'Source');
            $this->excel->getActiveSheet()->setCellValue('W3', 'With CCA Experience');
            $this->excel->getActiveSheet()->setCellValue('X3', 'Latest Employer');
            $this->excel->getActiveSheet()->setCellValue('Y3', 'Last Position Held');
            $this->excel->getActiveSheet()->setCellValue('Z3', 'Inclusive Dates');
            $this->excel->getActiveSheet()->setCellValue('AA3', 'Last Employer Address');
            $this->excel->getActiveSheet()->setCellValue('AB3', 'Last Employer Contact');
            $this->excel->getActiveSheet()->setCellValue('AC3', 'Reason for Failing');
            $this->excel->getActiveSheet()->setCellValue('AD3', 'Position Applied');
            $this->excel->getActiveSheet()->setCellValue('AE3', 'Employment Status');
            $this->excel->getActiveSheet()->setCellValue('AF3', 'Date Created');

            $emprow = 4;
            foreach ($applicants as $app) {
                $age = date_diff(date_create($app->birthday), date_create('now'))->y;
                $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $app->apid);
                $this->excel->getActiveSheet()->setCellValue('B' . $emprow, $app->lname);
                $this->excel->getActiveSheet()->setCellValue('C' . $emprow, $app->fname);
                $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $app->mname);
                $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $app->nameExt);
                $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $app->nickName);
                $this->excel->getActiveSheet()->setCellValue('G' . $emprow, $app->birthday);
                $this->excel->getActiveSheet()->setCellValue('H' . $emprow, $app->birthplace);
                $this->excel->getActiveSheet()->setCellValue('I' . $emprow, $age);
                $this->excel->getActiveSheet()->setCellValue('J' . $emprow, $app->religion);
                $this->excel->getActiveSheet()->setCellValue('K' . $emprow, $app->bloodType);
                $this->excel->getActiveSheet()->setCellValue('L' . $emprow, $app->gender);
                $this->excel->getActiveSheet()->setCellValue('M' . $emprow, $app->civilStatus);
                $this->excel->getActiveSheet()->setCellValue('N' . $emprow, $app->presentAddress);
                $this->excel->getActiveSheet()->setCellValue('O' . $emprow, $app->permanentAddress);
                $this->excel->getActiveSheet()->setCellValue('P' . $emprow, $app->tel);
                $this->excel->getActiveSheet()->setCellValue('Q' . $emprow, $app->cell);
                $this->excel->getActiveSheet()->setCellValue('R' . $emprow, $app->email);
                $this->excel->getActiveSheet()->setCellValue('S' . $emprow, $app->cr1);
                $this->excel->getActiveSheet()->setCellValue('T' . $emprow, $app->cr2);
                $this->excel->getActiveSheet()->setCellValue('U' . $emprow, $app->cr3);
                $this->excel->getActiveSheet()->setCellValue('V' . $emprow, $app->source);
                $this->excel->getActiveSheet()->setCellValue('W' . $emprow, ($app->ccaExp === '1') ? "Yes" : "No");
                $this->excel->getActiveSheet()->setCellValue('X' . $emprow, $app->latestEmployer);
                $this->excel->getActiveSheet()->setCellValue('Y' . $emprow, $app->lastPositionHeld);
                $this->excel->getActiveSheet()->setCellValue('Z' . $emprow, $app->inclusiveDates);
                $this->excel->getActiveSheet()->setCellValue('AA' . $emprow, $app->lastEmployerAddress);
                $this->excel->getActiveSheet()->setCellValue('AB' . $emprow, $app->lastEmployerContact);
                $this->excel->getActiveSheet()->setCellValue('AC' . $emprow, $app->failReason);
                $this->excel->getActiveSheet()->setCellValue('AD' . $emprow, $app->pos_details);
                $this->excel->getActiveSheet()->setCellValue('AE' . $emprow, ($app->isEmployed === '1') ? "Yes" : "No");
                $this->excel->getActiveSheet()->setCellValue('AF' . $emprow, $app->createdOn);

                $emprow++;
            }

            $column = $this->excel->getActiveSheet()->getHighestColumn();
            $row = $this->excel->getActiveSheet()->getHighestRow();
            $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '4b4c54')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
            //        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
            $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
            $this->excel->getActiveSheet()->getProtection()->setPassword('password sz');
            $this->excel->getActiveSheet()->getProtection()->setSheet(true);
            $this->excel->getActiveSheet()->freezePane('D4');

            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(23);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
            $excessletters = ['D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF'];
            foreach ($excessletters as $l) {
                $this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);
            }

            $filename = 'SZ_EMSExport.xlsx'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }
    }

    //END NEW CODES STARTS HERE----------------------------------------------------------------------------------------------------------------------------

    public function resetPassword()
    {
        //DEFAULT PASSWORD: "sz123"
        $currentEmployee_ID = $this->input->post('currentEmployee_ID');
        $res = $this->general_model->update_vals(['password' => "sz123", 'password_hash' => sha1("sz123")], "emp_id=$currentEmployee_ID", "tbl_user");
        echo json_encode(['status' => ($res) ? "Success" : "Failed"]);
    }

    public function getExistsUsername()
    {
        $username = $this->input->post('value');
        $count = $this->general_model->fetch_specific_val("COUNT(*) count", "username = '$username'", "tbl_user")->count;
        $status = ((int) $count > 0) ? "Exists" : "Available";
        echo json_encode(array('status' => $status));
    }
    public function isStillSupervisor()
    {
        $emp_id = $this->input->post('emp_id');
        $count = $this->general_model->fetch_specific_val("COUNT(*) as cnt", "Supervisor = $emp_id AND isActive = 'yes'", "tbl_employee")->cnt;
        echo json_encode(['isSupervisor' => ($count === '0') ? 'no' : 'yes', 'cnt' => $count]);
    }

    public function save_create_applicant()
    {
        $uid = $this->session->uid;
        $data = $this->input->post();
        $charRefs = array();
        $normal = array();
        foreach ($data as $key => $val) {
            $val = trim($val);
            if ($key !== 0) {
                $key = str_replace('"', '', $key);
                $field = explode('-', $key);
                if (isset($field[1])) {
                    $charRefs[$field[0]][$field[1]] = $val;
                } else {
                    $normal[$key] = ($val === '') ? null : $val;
                }
            }
        }
        foreach ($charRefs as $field => $subfield) {
            $str = $subfield['firstname'] . "$" . $subfield['middlename'] . "$" . $subfield['lastname'] . "|" . $subfield['contactnumber'] . "|" . $subfield['occupation'];
            $normal[$field] = $str;
        }
        $normal['createdBy'] = $uid;
        $normal['isEmployed'] = 0;
        $normal['isActive'] = 1;
        $res = $this->general_model->insert_vals($normal, "tbl_applicant");
        echo json_encode(['status' => ($res) ? "Success" : "Failed"]);
    }
    //MIC MIC CODES
    // CUSTOM APPROVAL SETTINGS
    private function qry_custom_approval_exist($emp_id, $account, $level)
    {
        $fields = "*";
        $where = "emp_id = $emp_id AND account_ID =  $account AND level = $level";
        return $this->general_model->fetch_specific_val($fields, $where, 'tbl_request_custom_approval');
    }
    private function qry_custom_approval_change_exist($requestCustom_id)
    {
        $fields = "app.fname, app.mname, app.lname, custom.account_ID, custom.level, custom.changeType, custom.dateCreated, custom.createdBy";
        $where = "app.apid = emp.apid AND emp.emp_id = changes.emp_id AND changes.requestCustomApproval_ID = custom.requestCustomApproval_ID AND custom.requestCustomApproval_ID = $requestCustom_id";
        $table = "tbl_request_approval_change changes, tbl_request_custom_approval custom, tbl_employee emp, tbl_applicant app";
        return $this->general_model->fetch_specific_val($fields, $where, $table);
    }
    private function delete_custom_approval($custom_approval_id)
    {
        $where['requestCustomApproval_ID'] =  $custom_approval_id;
        return $this->general_model->delete_vals($where, 'tbl_request_custom_approval');
    }
    private function delete_custom_approval_change($custom_approval_id)
    {
        $where['requestCustomApproval_ID'] = $custom_approval_id;
        return $this->general_model->delete_vals($where, 'tbl_request_approval_change');
    }
    private function insert_custom_change($data)
    {
        return $this->general_model->insert_vals($data, 'tbl_request_approval_change');
    }
    private function insert_custom_approval($data)
    {
        return $this->general_model->insert_vals_last_inserted_id($data, 'tbl_request_custom_approval');
    }
    public function add_custom_approval()
    {
        $dateTime = $this->get_current_date_time();
        $record['exist'] = 0;
        $emp_id = $this->input->post('approver_emp_id');
        $account = $this->input->post('acc_id');
        $level = $this->input->post('level');
        $type = $this->input->post('type');
        $emp_id_to = $this->input->post('emp_id_to');
        $assigned = $this->qry_custom_approval_exist($emp_id, $account, $level);
        if (count($assigned) > 0) {
            if ($assigned->changeType == "change") {
                $assigned = $this->qry_custom_approval_change_exist($assigned->requestCustomApproval_ID);// check if exist
            }
        }
        if (count($assigned) > 0) {// notify
            $record['exist'] = 1;
            $record['record'] = $assigned;
        } else {// insert
            $record['exist'] = 0;
            $data['account_ID'] = $account;
            $data['level'] = $level;
            $data['changeType'] = $type;
            $data['emp_id'] = $emp_id;
            $data['dateCreated'] =  $dateTime['dateTime'];
            $data['createdBy'] = $this->session->userdata('emp_id');
            $custom_approval_id = $this->insert_custom_approval($data);
            if ($type == "change") {
                $data2['requestCustomApproval_ID'] = $custom_approval_id;
                $data2['emp_id'] = $emp_id_to;
                $record['success'] = $this->insert_custom_change($data2);
            } else {
                $record['success'] = $custom_approval_id;
            }
        }
        echo json_encode($record);
    }
    public function qry_approvers_name()
    {
        $fields = "DISTINCT(customAppr.emp_id), app.fname, app.lname, app.mname";
        $where = "app.apid = emp.apid AND emp.emp_id = customAppr.emp_id";
        $table = "tbl_request_custom_approval customAppr, tbl_applicant app, tbl_employee emp";
        return $this->general_model->fetch_specific_vals($fields, $where, $table);
    }
    public function get_approvers_name()
    {
        $data = $this->qry_approvers_name();
        echo json_encode($data);
    }
    public function get_custom_approval_datatable()
    {
        $datatable = $this->input->post('datatable');
        $approver_id = $datatable['query']['approverId'];
        $changes_type = $datatable['query']['changeType'];
        $acc_id = $datatable['query']['accountId'];
        $where_condition = "";
        $condition_arr = [];
        // var_dump((int) $changes_type);
        if ((int) $approver_id !== 0) {
            array_push($condition_arr, "customs.emp_id = $approver_id");
        }
        if ((int) $changes_type !== 0) {
            if ((int)$changes_type == 1) {
                $changes_type = 'change';
            } else {
                $changes_type = 'exempt';
            }
            array_push($condition_arr, "customs.changeType = '$changes_type'");
        }
        if ((int) $acc_id !== 0) {
            array_push($condition_arr, "customs.account_ID = $acc_id");
        }
        // var_dump($condition_arr);
        if (count($condition_arr) > 0) {
            $where_condition = "WHERE ".implode(" AND ", $condition_arr);
            ;
        }
        $query['query'] = "SELECT customs.requestCustomApproval_ID customApprovalId, acc.acc_name, customs.level, customs.changeType, customs.dateCreated, customs.emp_id approverEmpId, appApprover.fname approverFname, appApprover.lname approverLname, appApprover.mname, changes.requestApprovalChange_ID, changes.requestCustomApproval_ID customApprovalChangeId, changes.emp_id assignEmpId, appAssign.fname assignFname, appAssign.lname assignLname, appAssign.mname assignMname FROM tbl_request_custom_approval customs LEFT JOIN tbl_request_approval_change changes ON changes.requestCustomApproval_ID = customs.requestCustomApproval_ID INNER JOIN tbl_employee empApprover ON empApprover.emp_id = customs.emp_id INNER JOIN tbl_applicant appApprover ON appApprover.apid = empApprover.apid INNER JOIN tbl_account acc ON acc.acc_id = customs.account_ID LEFT JOIN tbl_employee empAssign ON empAssign.emp_id = changes.emp_id LEFT JOIN tbl_applicant appAssign ON appAssign.apid = empAssign.apid ".$where_condition;
        $data = $this->set_datatable_query($datatable, $query);
        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
    }
    public function remove_custom_approval()
    {
        $custom_approval_id = $this->input->post('customApprovalId');
        $change_type = $this->input->post('changeType');
        $data['custom_approval'] = $this->delete_custom_approval($custom_approval_id);
        if ($change_type == "change") {
            $data['custom_approval_change'] = $this->delete_custom_approval_change($custom_approval_id);
        }
        echo json_encode($data);
    }
    //MIC MIC CODES END

    //--------------------------------------------CONTACT INFORMATION PAGE---------------------------------------------------------
    private function general_getEmpAcc_ids($includeSelf)
    {
        $uid = $this->session->userdata('uid');
        $acc_ids = array();
        if (!in_array($uid, $this->administrator)) {
            $subordinates = $this->getEmployeeSubordinate($uid);
            $empids = array();
            if ($includeSelf === '1') {
                $empids[] = $this->session->emp_id;
            }
            foreach ($subordinates as $s) {
                $empids[] = $s->emp_id;
            }
            if ($this->session->userdata('role_id') === '2') {
                $empids[] = $this->session->userdata('emp_id');
            }
            if (empty($empids)) {
                $query = "SELECT acc_id FROM tbl_employee  WHERE isActive='yes' AND emp_id IN (0)";
            } else {
                $query = "SELECT acc_id FROM tbl_employee WHERE isActive='yes' AND emp_id IN (" . implode(",", $empids) . ")";
            }
        } else {
            $query = "SELECT acc_id FROM tbl_employee WHERE isActive='yes'";
        }
        $account_ids = $this->general_model->custom_query($query);
        foreach ($account_ids as $acc_id) {
            if ($acc_id->acc_id !== null) {
                $acc_ids[] = $acc_id->acc_id;
            }
        }
        if (empty($acc_ids)) {
            $accounts = $this->general_model->fetch_specific_vals("acc_id,acc_name,acc_description", "acc_id IN (0)", "tbl_account", "acc_name ASC");
        } else {
            $unique_acc_ids = array_unique($acc_ids);
            $accounts = $this->general_model->fetch_specific_vals("acc_id,acc_name,acc_description", "acc_id IN (" . implode(",", $unique_acc_ids) . ")", "tbl_account", "acc_name ASC");
        }
        return $accounts;
    }

    public function contacts()
    {
        $data = $this->title;
        $data['uri_segment'] = $this->uri->segment_array();
        $data['accounts'] = $this->general_getEmpAcc_ids(1);
        if ($this->check_access()) {
            $this->load_template_view('templates/employee/contact_information', $data);
        }
    }

    private function getContactsRecord($searchname, $searchclass, $searchaccounts, $withCount = 0, $limiter=null, $perPage=null)
    {
        $search = array();
        $searchString = "";
        if ($searchname !== '' && $searchname !==null) {
            $search[] = "(a.fname LIKE \"%$searchname%\" OR a.lname LIKE \"%$searchname%\")";
        }
        if ($searchclass !== ''&& $searchclass !==null) {
            $search[] = "c.acc_description = '$searchclass'";
        }
        if ($searchaccounts !== ''&& $searchaccounts !==null) {
            $search[] = "c.acc_id=$searchaccounts";
        }
        if (!empty($search)) {
            $searchString = "AND " . implode(" AND ", $search);
        }
        $uid = $this->session->uid;
        $subordinateString = "";
        if (!in_array($this->session->userdata('role_id'), array('1','5'))) {
            if (!in_array($uid, $this->administrator)) {
                $subordinates = $this->getEmployeeSubordinate($uid);
                $empids = array();
                foreach ($subordinates as $s) {
                    $empids[] = $s->emp_id;
                }
                if ($this->session->userdata('role_id') === '2') {
                    $empids[] = $this->session->userdata('emp_id');
                }
                if (empty($empids)) {
                    $subordinateString = " AND emp_id IN (0) ";
                } else {
                    $subordinateString = " AND emp_id IN (" . implode(",", $empids) . ") ";
                }
            }
        }
       
    
        $query = " FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid = b.apid AND b.acc_id = c.acc_id AND b.isActive = 'yes' $searchString $subordinateString";
        $limit = $limiter==null?"": "LIMIT $limiter,$perPage";
        $employees = $this->general_model->custom_query("SELECT a.lname,a.fname,c.acc_name,b.Supervisor,a.cell,a.email,a.presentAddress,b.id_num".$query . " ORDER BY a.lname ASC, a.fname ASC $limit");
        $directSupIds = array_filter(array_unique(array_column($employees, 'Supervisor')));
        if (COUNT($directSupIds)>0) {
            $directSupervisors = $this->general_model->custom_query("SELECT b.emp_id,a.lname,a.fname,b.Supervisor FROM tbl_applicant a, tbl_employee b WHERE b.emp_id IN (".implode($directSupIds, ',').") AND a.apid = b.apid");
            $nextSupervisors = array();
            $nextSupIds = array_filter(array_unique(array_column($directSupervisors, 'Supervisor')));
            if (COUNT($nextSupIds)>0) {
                $nextSupervisors = $this->general_model->custom_query("SELECT b.emp_id,a.lname,a.fname FROM tbl_applicant a, tbl_employee b WHERE b.emp_id IN (".implode($nextSupIds, ',').") AND a.apid = b.apid");
            }
            foreach ($employees as $key => $emp) {
                $emp->directSupervisor = null;
                $emp->nextSupervisorId =null;
                $emp->nextSupervisor = null;
                foreach ($directSupervisors as $dir) {
                    if ($emp->Supervisor == $dir->emp_id) {
                        $emp->directSupervisor = $dir->lname.", ".$dir->fname;
                        $emp->nextSupervisorId = $dir->Supervisor;
                    }
                }
                foreach ($nextSupervisors as $next) {
                    if ($emp->nextSupervisorId == $next->emp_id) {
                        $emp->nextSupervisor = $next->lname.", ".$next->fname;
                    }
                }
            }
        }
        
        if ($withCount) {
            $count = $this->general_model->custom_query("SELECT COUNT(*) as count".$query)[0]->count;
            return array('employees'=>$employees,'total'=>$count);
        } else {
            return array('employees'=>$employees);
        }
    }

    public function getContactsList()
    {
        $limiter = $this->input->post('limiter');
        $perPage = $this->input->post('perPage');
      
        $searchname = $this->input->post('searchname');
        $searchclass = $this->input->post('searchclass');
        $searchaccounts = $this->input->post('searchaccounts');
        
        $record =  $this->getContactsRecord($searchname, $searchclass, $searchaccounts, 1, $limiter, $perPage);
        
        echo json_encode($record);
    }

    public function downloadContactsExcel()
    {
        $searchname = $this->input->post('searchname');
        $searchclass = $this->input->post('searchclass');
        $searchaccounts = $this->input->post('searchaccounts');
        
        $employees =  $this->getContactsRecord($searchname, $searchclass, $searchaccounts)['employees'];
        //EXCEL PROPER
        $this->load->library('PHPExcel', null, 'excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Applicant Records');
        $this->excel->getActiveSheet()->setShowGridlines(false);
        //------------------------INSERT LOGO-------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0); // setOffsetX works properly
        $objDrawing->setOffsetY(5); //setOffsetY has no effect
        $objDrawing->setCoordinates('B1');
        $objDrawing->setHeight(80); // logo height
        // $objDrawing->setWidth(320); // logo width
        // $objDrawing->setWidthAndHeight(200,400);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(35);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
        //set cell A1 content with some text

        $this->excel->getActiveSheet()->setCellValue('A3', 'ID Number');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Last Name');
        $this->excel->getActiveSheet()->setCellValue('C3', 'First Name');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Account');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Supervisor I');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Supervisor II');
        $this->excel->getActiveSheet()->setCellValue('G3', 'Phone Number');
        $this->excel->getActiveSheet()->setCellValue('H3', 'Email Address');
        $this->excel->getActiveSheet()->setCellValue('I3', 'Home Address');

        $emprow = 4;
        foreach ($employees as $emp) {
            $this->excel->getActiveSheet()->setCellValue('A' . $emprow, $emp->id_num);
            $this->excel->getActiveSheet()->setCellValue('B' . $emprow, ucwords($emp->lname));
            $this->excel->getActiveSheet()->setCellValue('C' . $emprow, ucwords($emp->fname));
            $this->excel->getActiveSheet()->setCellValue('D' . $emprow, $emp->acc_name);
            $this->excel->getActiveSheet()->setCellValue('E' . $emprow, $emp->directSupervisor);
            $this->excel->getActiveSheet()->setCellValue('F' . $emprow, $emp->nextSupervisor);
            $this->excel->getActiveSheet()->setCellValue('G' . $emprow, $emp->cell);
            $this->excel->getActiveSheet()->setCellValue('H' . $emprow, $emp->email);
            $this->excel->getActiveSheet()->setCellValue('I' . $emprow, str_replace('|', ",", $emp->presentAddress));

            $emprow++;
        }

        $column = $this->excel->getActiveSheet()->getHighestColumn();
        $row = $this->excel->getActiveSheet()->getHighestRow();
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => '4b4c54')), 'font' => array('bold' => true, 'color' => array('rgb' => 'ffffff'))));
        //        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '3')->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->getStyle('A3:' . $column . '' . $row)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
        $this->excel->getActiveSheet()->freezePane('D4');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(23);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
        $excessletters = ['D', 'E', 'F', 'G', 'H', 'I'];
        foreach ($excessletters as $l) {
            $this->excel->getActiveSheet()->getColumnDimension($l)->setAutoSize(true);
        }

        $filename = 'SZ_EMSExport.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }



    public function getFilteredPositions_v2()
    {
        $searchText = strtolower($this->input->get('searchText'));
        $searchTextArr = explode(" ", $searchText);
        $searchString = implode('|', $searchTextArr);
      
        $results = $this->general_model->custom_query("SELECT pos_id,pos_name,pos_details FROM tbl_position WHERE pos_details LIKE '%$searchText%'   ORDER BY pos_details ASC LIMIT 15");

        echo json_encode($results);
    }
    public function checkExactPosition()
    {
        $searchText = strtolower($this->input->get('searchText'));
        
        $position = $this->general_model->fetch_specific_val("pos_id", "pos_details='$searchText'", "tbl_position");
        if (isset($position)) {
            echo json_encode(['status'=>'Exist','pos_id'=>$position->pos_id]);
        } else {
            echo json_encode(['status'=>'Not Exist','pos_id'=>null]);
        }
    }
}
