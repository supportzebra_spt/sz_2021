<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/General.php");

class Dtr extends General {

 
			  public $logo=  "/var/www/html/sz/assets/images/img/logo2.png";
			// public $logo = "C:\\www\\sz\\assets\\images\\img\\logo2.png";
			protected $title = array('title' => 'DTR');

			public function attendance(){
				date_default_timezone_set('Asia/Manila');
				$data = [];
				$data = $this->title;
				$data['uri_segment'] = $this->uri->segment_array();
				$session = $_SESSION["emp_id"];
			
				$fields = "fname,lname,pic,b.acc_id";
				$where = "a.apid=b.apid and b.isActive='yes' and emp_id=".$session;
				$tables = "tbl_applicant a,tbl_employee b";
				$data["emp"] = $this->general_model->fetch_specific_val($fields, $where, $tables);
		 
				$qry = "select * from tbl_dtr_logs where emp_id='".$session."' order by log desc limit 8";
				$data["emp_logs"] = $this->general_model->custom_query($qry);
				
				$last_log_qry = "select dtr_id,log,a.emp_id,b.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a left join tbl_schedule b on a.sched_id=b.sched_id left join tbl_acc_time c on c.acc_time_id=b.acc_time_id left join tbl_time d on c.time_id=d.time_id where a.emp_id='".$session."' and info is null  order by log desc limit 1";
				// $last_log_qry_DTR = "select dtr_id,log,a.emp_id,a.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a left join tbl_schedule b on a.sched_id=b.sched_id left join tbl_acc_time c on c.acc_time_id=b.acc_time_id left join tbl_time d on c.time_id=d.time_id where a.emp_id='".$session."' and a.type='DTR' and info is null and a.sched_id is not null  order by log desc limit 1"; 
				$last_log_qry_DTR = "select dtr_id,log,a.emp_id,b.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a, tbl_schedule b  left join tbl_acc_time c on c.acc_time_id=b.acc_time_id left join tbl_time d on c.time_id=d.time_id where a.sched_id=b.sched_id and a.emp_id='".$session."' and a.type='DTR' and info is null and a.sched_id is not null  order by log desc limit 1"; 
				$data["emp_last_logs"] = $this->general_model->custom_query($last_log_qry);
				$rezult_last_log = $this->general_model->custom_query($last_log_qry_DTR);
				$data["emp_last_logs_DTR"] = (count($rezult_last_log) >0) ? $rezult_last_log : 0;
 				 
				 if(count($rezult_last_log)>0){
						 $schedDate = $data["emp_last_logs_DTR"][0]->sched_date;
						 $in = $data["emp_last_logs_DTR"][0]->log;
						 $shiftIN =  ($data["emp_last_logs_DTR"][0]->time_start=="12:00:00 AM") ? "23:59" : date_format(date_create($data["emp_last_logs_DTR"][0]->time_start), 'H:i');
						 $shiftOUT = date_format(date_create($data["emp_last_logs_DTR"][0]->time_end), 'H:i');
						 $ampm = explode("|",$this->getTimeGreeting($shiftIN,$shiftOUT));
						 $dateOut = ($ampm[1]=="Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($schedDate))) : $schedDate;
						 $loginNew = (strtotime($in) <= strtotime($schedDate." ".$shiftIN)) ? $schedDate." ".$shiftIN : $in;
						 $logoutNew =  $dateOut." ".$shiftOUT;
							$data["dateOut"] = json_encode($data["emp_last_logs_DTR"]);
							
								if(trim($data["emp_last_logs"][0]->type)!="Break" or  $data["emp_last_logs"][0]->entry !="O"){
					
									 if($data["emp_last_logs_DTR"][0]->entry =="I"){
												$acctime_temp = ($data["emp_last_logs_DTR"][0]->acc_time_id!=null) ? $data["emp_last_logs_DTR"][0]->acc_time_id : 0 ;
 												$get_break = "SELECT sbrk_id,a.bta_id,break_type,hour,min,b.break_id,a.acc_time_id FROM tbl_shift_break a,tbl_break_time_account b,tbl_break c,tbl_break_time d where a.bta_id=b.bta_id and b.break_id=c.break_id and b.btime_id=d.btime_id and isActive=1 and acc_time_id=".$acctime_temp." order by c.break_id ASC";
												
												$get_taken_break = "select distinct(break_type) from tbl_break_time_account c,tbl_break d,tbl_break_time e,tbl_dtr_logs a left join tbl_shift_break b on a.acc_time_id=b.sbrk_id where a.emp_id='".$session."' and b.bta_id=c.bta_id and c.break_id =d.break_id and e.btime_id=c.btime_id  and type='Break' and a.dtr_id>".$data["emp_last_logs_DTR"][0]->dtr_id;
												
												$data["get_break"] = $this->general_model->custom_query($get_break);
												$get_taken_break = $this->general_model->custom_query($get_taken_break);
												$brk = "";
													foreach($get_taken_break as $row){
															$brk .= $row->break_type.",";
														
													}
											$data["get_taken_break"] = explode(",",$brk);
											$data["out_limit"] = $logoutNew;
											
											$limit = date("Y-m-d H:i:s",strtotime("+0 minutes", strtotime($data["out_limit"])));
											$notif = (strtotime(date("Y-m-d H:i:s")) > strtotime($limit)) ? "Over" : "UT"; 
 											$data["limit_remarks"] = $notif;
											$data["limit"] = $limit;
											
											$data["sched_id"] = $data["emp_last_logs"][0]->sched_id;
										 }else{
											$data["limit"] ="";
										 }
								}else{
									header('Location: '.base_url()."dtr/breakPage");
								}
								$data['survey_data'] = $this->_popup_survey($data["emp_last_logs_DTR"][0]->sched_id);
						 }else{
							 $data["emp_last_logs_DTR"] = 0;
							 $data["limit"] ="";
						 }

						 $isexcempt = "Select count(*) as cnt from tbl_dtr_no_rule where emp_id=".$_SESSION["emp_id"];
						 $data["isexcempt"] = $this->general_model->custom_query($isexcempt);
						  // Add survey data
						 

						//echo $get_break;
						  $this->load_template_view('templates/dtr/attendance/index', $data);
				
			}
			public function attendance2(){
				date_default_timezone_set('Asia/Manila');
				$data = [];
				$data = $this->title;
				$data['uri_segment'] = $this->uri->segment_array();
				$session = $_SESSION["emp_id"];
			
				$fields = "fname,lname,pic,b.acc_id";
				$where = "a.apid=b.apid and b.isActive='yes' and emp_id=".$session;
				$tables = "tbl_applicant a,tbl_employee b";
				$data["emp"] = $this->general_model->fetch_specific_val($fields, $where, $tables);
		 
				$qry = "select * from tbl_dtr_logs where emp_id='".$session."' order by log desc limit 8";
				$data["emp_logs"] = $this->general_model->custom_query($qry);
				
				$last_log_qry = "select dtr_id,log,a.emp_id,b.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a left join tbl_schedule b on a.sched_id=b.sched_id left join tbl_acc_time c on c.acc_time_id=b.acc_time_id left join tbl_time d on c.time_id=d.time_id where a.emp_id='".$session."' and info is null  order by log desc limit 1";
				$last_log_qry_DTR = "select dtr_id,log,a.emp_id,b.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a left join tbl_schedule b on a.sched_id=b.sched_id left join tbl_acc_time c on c.acc_time_id=b.acc_time_id left join tbl_time d on c.time_id=d.time_id where a.emp_id='".$session."' and a.type='DTR' and info is null order by log desc limit 1";
				$data["emp_last_logs"] = $this->general_model->custom_query($last_log_qry);
				$data["emp_last_logs_DTR"] = $this->general_model->custom_query($last_log_qry_DTR);
 				 
				 if(count($data["emp_last_logs_DTR"])>0){
						 $schedDate = $data["emp_last_logs_DTR"][0]->sched_date;
						 $in = $data["emp_last_logs_DTR"][0]->log;
						 $shiftIN =  date_format(date_create($data["emp_last_logs_DTR"][0]->time_start), 'H:i');
						 $shiftOUT = date_format(date_create($data["emp_last_logs_DTR"][0]->time_end), 'H:i');
						 $ampm = explode("|",$this->getTimeGreeting($shiftIN,$shiftOUT));
						 $dateOut = ($ampm[1]=="Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($schedDate))) : $schedDate;
						 $loginNew = (strtotime($in) <= strtotime($schedDate." ".$shiftIN)) ? $schedDate." ".$shiftIN : $in;
						 $logoutNew =  $dateOut." ".$shiftOUT;
							$data["dateOut"] = json_encode($data["emp_last_logs_DTR"]);
							
								if(trim($data["emp_last_logs"][0]->type)!="Break" or  $data["emp_last_logs"][0]->entry !="O"){
					
									 if($data["emp_last_logs_DTR"][0]->entry =="I"){
												$acctime_temp = ($data["emp_last_logs_DTR"][0]->acc_time_id!=null) ? $data["emp_last_logs_DTR"][0]->acc_time_id : 0 ;
 												$get_break = "SELECT sbrk_id,a.bta_id,break_type,hour,min,b.break_id,a.acc_time_id FROM `tbl_shift_break` a,tbl_break_time_account b,tbl_break c,tbl_break_time d where a.bta_id=b.bta_id and b.break_id=c.break_id and b.btime_id=d.btime_id and isActive=1 and acc_time_id=".$acctime_temp." order by c.break_id ASC";
												
												$get_taken_break = "select distinct(break_type) from tbl_break_time_account c,tbl_break d,tbl_break_time e,tbl_dtr_logs a left join tbl_shift_break b on a.acc_time_id=b.sbrk_id where a.emp_id='".$session."' and b.bta_id=c.bta_id and c.break_id =d.break_id and e.btime_id=c.btime_id  and type='Break' and a.dtr_id>".$data["emp_last_logs_DTR"][0]->dtr_id;
												
												$data["get_break"] = $this->general_model->custom_query($get_break);
												$get_taken_break = $this->general_model->custom_query($get_taken_break);
												$brk = "";
													foreach($get_taken_break as $row){
															$brk .= $row->break_type.",";
														
													}
											$data["get_taken_break"] = explode(",",$brk);
											$data["out_limit"] = $logoutNew;
											
											$limit = date("Y-m-d H:i:s",strtotime("+0 minutes", strtotime($data["out_limit"])));
											$notif = (strtotime(date("Y-m-d H:i:s")) > strtotime($limit)) ? "Over" : "UT"; 
 											$data["limit_remarks"] = $notif;
											$data["limit"] = $limit;
											
											$data["sched_id"] = $data["emp_last_logs"][0]->sched_id;
										 }else{
											$data["limit"] ="";
										 }
								}else{
									header('Location: '.base_url()."dtr/breakPage");
								}
								$data['survey_data'] = $this->_popup_survey($data["emp_last_logs_DTR"][0]->sched_id);
						 }else{
							 $data["emp_last_logs_DTR"][0] = array();
							 $data["limit"] ="";
						 }

						 $isexcempt = "Select count(*) as cnt from tbl_dtr_no_rule where emp_id=".$_SESSION["emp_id"];
						 $data["isexcempt"] = $this->general_model->custom_query($isexcempt);
						  // Add survey data
						 

						//echo $get_break;
						  $this->load_template_view('templates/dtr/attendance/index2', $data);
				
			}
			
			
			public function mySchedLogs($sched=null){
			$last_log_qry_in = "SELECT * FROM `tbl_dtr_logs` where sched_id=$sched and entry='I' limit 1";
				$dataIn = $this->general_model->custom_query($last_log_qry_in);
			$last_log_qry_out = "SELECT * FROM `tbl_dtr_logs` where sched_id=$sched and entry='O' limit 1";
				$dataOut = $this->general_model->custom_query($last_log_qry_out);
				$arr=array();
  					$arr[0] = array(
					    "dtr_id" => (count($dataIn)>0) ? $dataIn[0]->dtr_id: 0,
					    "dtr_in" => (count($dataIn)>0) ? date_format(date_create($dataIn[0]->log), 'Y-m-d h:i a') : "No Logs",
					    "log" =>  (count($dataIn)>0) ? $dataIn[0]->entry : "No Logs",
 					);
				 
				 
					$arr[1] = array(
					    "dtr_id" => (count($dataOut)>0) ? $dataOut[0]->dtr_id : 0,
					    "dtr_out" => (count($dataOut)>0) ?date_format(date_create($dataOut[0]->log), 'Y-m-d h:i a') : "No Logs",
					    "log" => (count($dataOut)>0) ? $dataOut[0]->entry : "No Logs",
 					);
				 
				 
				echo (count($arr)>0) ? json_encode($arr) : 0;
			
			}
			
		
			public function loadSchedule(){
			date_default_timezone_set('Asia/Manila');
			$acc_id = $this->input->post("acc_id");
				// $qry="SELECT acc_time_id,a.time_id,acc_id,note,time_start,time_end FROM `tbl_acc_time` a,tbl_time b where a.time_id=b.time_id and acc_id=".$acc_id;
				$qry="SELECT acc_time_id,a.time_id,acc_id,note,time_start,time_end FROM `tbl_acc_time` a,tbl_time b where a.time_id=b.time_id and acc_id=".$acc_id." and note='DTR' order by STR_TO_DATE(CONCAT(CURDATE(),' ',time_start), '%Y-%m-%d %l:%i:%s %p') ASC";
				$data = $this->general_model->custom_query($qry);
				echo json_encode($data);
			}
			public function mySched(){
			date_default_timezone_set('Asia/Manila');
			$date = $this->input->post("date");
			$newDate = ($date!=null) ?   explode(" ",$date) : "";
			$month =($date!=null) ? date("m",strtotime($newDate[1]." ".$newDate[2]." ".$newDate[3])) : date("m");  
			$year =($date!=null) ? date("Y",strtotime($newDate[1]." ".$newDate[2]." ".$newDate[3])): date("Y");  
  				// header('Content-Type: application/json');
				$last_log_qry = "select a.*,c.time_start,c.time_end from (SELECT sched_date,sched_id,b.type,acc_time_id FROM `tbl_schedule` a,tbl_schedule_type b where a.schedtype_id=b.schedtype_id and emp_id=".$_SESSION['emp_id']." and MONTH(sched_date)=$month and year(sched_date)=$year) a left join tbl_acc_time b on a.acc_time_id=b.acc_time_id left join tbl_time c on b.time_id=c.time_id";
  				$data = $this->general_model->custom_query($last_log_qry);
				if(count($data)>0){
					foreach($data as $row){
						$arr[] = array(
							"id" => $row->sched_id,
							"title" => ($row->time_start!=null) ? date_format(date_create($row->time_start), 'h:ia') : str_replace(" ","\n",$row->type),
							"start" =>   $row->sched_date,
							"description" =>  ($row->time_end!=null) ? date_format(date_create($row->time_end), 'h:ia') :"",
							"className" =>  "shift".str_replace(" ","",$row->type),
						);	
					}
				}else{
					$arr[] = array(
					    "id" => 1,
					    "title" => "No",
                        "start" =>   date("Y-m-d"),
                        "description" =>  "Shift",
                        "className" =>  "shiftRestDay",
 					);
				}
				echo json_encode($arr);
			}
			public function getTotalLogs($sched_id)
	{
		$dtrLogsIN = $this->general_model->custom_query("SELECT dtr_id,emp_id FROM tbl_dtr_logs where sched_id=" . $sched_id . " and entry='I' order by dtr_id asc limit 1");
		$dtrLogsOUT = $this->general_model->custom_query("SELECT dtr_id,emp_id FROM tbl_dtr_logs where sched_id=" . $sched_id . " and entry='O' order by dtr_id asc limit 1");
		$data = $this->general_model->custom_query("SELECT count(*) as total FROM tbl_dtr_logs where type='DTR'  and emp_id='" . $dtrLogsIN[0]->emp_id . "' and dtr_id between " . $dtrLogsIN[0]->dtr_id . " and " . $dtrLogsOUT[0]->dtr_id);
		return $data[0]->total;
	}
	public function getTotalBreakLogs($sched_id)
	{
		$dtrLogsIN = $this->general_model->custom_query("SELECT dtr_id,emp_id FROM tbl_dtr_logs where sched_id=" . $sched_id . " and entry='I' order by dtr_id asc limit 1");
		$dtrLogsOUT = $this->general_model->custom_query("SELECT dtr_id,emp_id FROM tbl_dtr_logs where sched_id=" . $sched_id . " and entry='O' order by dtr_id asc limit 1");
		$data = $this->general_model->custom_query("SELECT count(*) as total FROM tbl_dtr_logs where type='BREAK' and emp_id='" . $dtrLogsIN[0]->emp_id . "' and dtr_id between " . $dtrLogsIN[0]->dtr_id . " and " . $dtrLogsOUT[0]->dtr_id);

		return $data[0]->total / 2;
	}
	public function getTotalBreak($sched_id)
	{
		$emp_id = $_SESSION["emp_id"];
		$login = $this->getLogin($sched_id);
		$login2 = (count($login) > 0) ? $login[0]->dtr_id : 0;
		$logout = $this->getLogout($sched_id);
		$logout3 = (count($logout) > 0) ? $logout[0]->dtr_id : ($login2 * 1000);
		$llbreak_sbrk = $this->getLunchBreak_sbrk($emp_id, $login2, $logout3);

		$llbreak1 = ($llbreak_sbrk[0]->break != null) ? explode(",", $llbreak_sbrk[0]->break) : array(0, 0);

		$ffbreak_sbrk = $this->getFirstBreak_sbrk($emp_id, $login2, $logout3);

		$ffbreak1 = ($ffbreak_sbrk[0]->break != null) ? explode(",", $ffbreak_sbrk[0]->break) : array(0, 0);

		$llastbreak_sbrk = $this->getLastBreak_sbrk($emp_id, $login2, $logout3);

		$llastbreak1 = ($llastbreak_sbrk[0]->break != null) ? explode(",", $llastbreak_sbrk[0]->break) : array(0, 0);

		$lastbreak_total = ($llastbreak1[0] != 0) ? round(abs(strtotime($llastbreak1[0]) - strtotime($llastbreak1[1])) / 60, 2) : 0;
		$lunchbreak_total = ($llbreak1[0] != 0) ? round(abs(strtotime($llbreak1[0]) - strtotime($llbreak1[1])) / 60, 2) : 0;
		$firstbreak_total = ($ffbreak1[0] != 0) ?  round(abs(strtotime($ffbreak1[0]) - strtotime($ffbreak1[1])) / 60, 2) : 0;
		return $lastbreak_total + $lunchbreak_total + $firstbreak_total;
	}
			/* public function dtrLogSave(){
			date_default_timezone_set('Asia/Manila');
 				$data['type'] = $this->input->post('type');
				$data['entry'] = $this->input->post('entry');
				$data['acc_time_id'] = $this->input->post('acc_time_id');
 				$data['emp_id'] =  $_SESSION["emp_id"];
 				$data['sched_id'] =  $this->input->post('sched_id');
 				$data['note'] =   $this->input->post('loginID');
 				$data['log'] =  date("Y-m-d H:i:s");
				  $attendanceLog = $this->general_model->insert_vals($data, 'tbl_dtr_logs');
 
						if($attendanceLog==1){
								$entry_data = ($data['entry']=="I") ? "I - DTR" : "O - DTR" ;
								$qrytraill =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$_SESSION["uid"].",now(),'".$entry_data."','".$_SESSION["fname"]." ".$_SESSION["lname"]."')");

							if($this->input->post("dtrViolationType_ID")!=null){ 
								// $getSupQry = "SELECT supervisor FROM `tbl_employee` where emp_id=".$_SESSION["emp_id"];
								// $getSupQry = "select uid,fname,lname,a.supervisor from (SELECT supervisor FROM tbl_employee a where emp_id=".$_SESSION["emp_id"].") a,tbl_user b,tbl_applicant c where a.supervisor=b.emp_id";
								$getSupQry = "select uid,fname,lname,a.emp_id  as supervisor from  tbl_employee a, tbl_user b,tbl_applicant c where  b.emp_id = a.emp_id and a.apid=c.apid and b.emp_id=(SELECT supervisor FROM tbl_employee a where emp_id=".$_SESSION["emp_id"].")";
									$supervisor = $this->general_model->custom_query($getSupQry);
								$dataViolation['dtrViolationType_ID'] = $this->input->post("dtrViolationType_ID");
								$dataViolation['supervisor_ID'] = $supervisor[0]->supervisor;
								$dataViolation['user_ID'] = $_SESSION["uid"];
								$dataViolation['sched_id'] =  $this->input->post('sched_id');
								$dataViolation['month'] =  date("F",strtotime($this->input->post('schedDate')));
								$dataViolation['year'] =  date("y",strtotime($this->input->post('schedDate')));

								$violation = $this->general_model->insert_vals($dataViolation, 'tbl_user_dtr_violation');
								if($this->input->post("dtrViolationType_ID")==1){
									$remark = "was late";
								}else if($this->input->post("dtrViolationType_ID")==2){
									$remark = "was undertime";
								}else if($this->input->post("dtrViolationType_ID")==3){
									$remark = "forgot to logout";
								}else{
									$remark = "encountered issues with the DTR";
								}
								$resultSystemNotif =array();
									$message= $_SESSION["fname"]." ".$_SESSION["lname"]." ".$remark." on ".date('F d, Y',strtotime($this->input->post('schedDate')))." shift.";
									$link="dtr/issues";
									$sup= $supervisor[0]->uid;
										$resultSystemNotif = $this->setSystemNotif($message,$link,$sup) ;
										$v= $violation;
									$val  = array("res"=>1,"sup"=>$sup,"emp_id"=>$_SESSION["emp_id"],"systemNotifID"=>$resultSystemNotif);
							}else{
								$v=1;
								$sup = 0;
								$val = array("res"=>1);
							} 
						}else{
							$attendanceLog=0;
							$v=0;
							$sup = 0;
							$val = array("res"=>0);

						}
				  
					 $val["personal"] = $this->personalInfo();
					// $attendanceLog = 1;
					//echo $v." ".$attendanceLog;
				echo ($attendanceLog==1 && $v==1) ? json_encode($val) : 0;  
  			} */
  		
public function dtrLogSave()
	{
		date_default_timezone_set('Asia/Manila');

		$data['type'] = $this->input->post('type');
		$data['entry'] = $this->input->post('entry');
		$data['acc_time_id'] = $this->input->post('acc_time_id');
		$data['emp_id'] =  $_SESSION["emp_id"];
		$data['sched_id'] =  $this->input->post('sched_id');
		$data['note'] =   $this->input->post('loginID');
		$data['rason'] =   $this->input->post('rason');
		$data['log'] =  date("Y-m-d H:i:s");

		$attendanceLog = $this->general_model->insert_vals($data, 'tbl_dtr_logs');
		// $datasched_in['isLocked'] = 14; // lock sched after clock in
		// $wheresched_in = "sched_id = " . $data['sched_id']; // lock sched after clock in
		// $this->general_model->update_vals($datasched_in, $wheresched_in, 'tbl_schedule'); // lock sched after clock in
		if ($attendanceLog == 1) {
			$entry_data = ($data['entry'] == "I") ? "I - DTR" : "O - DTR";
			$qrytraill =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('" . $_SERVER['REMOTE_ADDR'] . "'," . $_SESSION["uid"] . ",now(),'" . $entry_data . "','" . $_SESSION["fname"] . " " . $_SESSION["lname"] . "')"); // Audit Traill for clock in and out

			$getSupQry = "select uid,fname,lname,a.emp_id  as supervisor from  tbl_employee a, tbl_user b,tbl_applicant c where  b.emp_id = a.emp_id and a.apid=c.apid and b.emp_id=(SELECT supervisor FROM tbl_employee a where emp_id=" . $_SESSION["emp_id"] . ")";
			$supervisor = $this->general_model->custom_query($getSupQry);
			$supervisor_ID = $supervisor[0]->supervisor;
			$sched_id =  $this->input->post('sched_id');
			$schedDate = $this->input->post('schedDate');
			$sched_details = $this->qry_sched_details($sched_id);
			if (($data['entry'] == "O")) { // check if Logs are complete
				$numberShiftBreak =  $this->general_model->custom_query("SELECT count(hour) as totalBreak FROM tbl_schedule a,tbl_shift_break b,tbl_break_time_account c,tbl_break_time d where a.acc_time_id=b.acc_time_id and b.bta_id=c.bta_id and c.btime_id=d.btime_id and sched_id=" . $data['sched_id'] . " and b.isActive=1");
				$totalNumShiftBreaks = $numberShiftBreak[0]->totalBreak; // Break Logs (in & out) 

				$totalWorkLogs = $this->getTotalLogs($data['sched_id']);
				$totalWorkBreakLogs = $this->getTotalBreakLogs($data['sched_id']);

				  if (intval($totalWorkBreakLogs) < intval($totalNumShiftBreaks)) {
					$violation_incompletelogs = $this->adddataViolation(6, $supervisor_ID, $sched_id, $schedDate, $_SESSION["uid"], $sched_details->schedtype_id);
					if($violation_incompletelogs['exist']){
						$this->check_qualified_dtr_violation($violation_incompletelogs['record']);
					}
					
					$messageIfComplete = $_SESSION["fname"] . " " . $_SESSION["lname"] . " has an incomplete DTR log entries on " . date('F d, Y', strtotime($this->input->post('schedDate'))) . " shift.";
					$linkIfComplete = "discipline/dms_supervision/dtrViolations/violationLogTab";
					$sup = $supervisor[0]->uid;
					$checkOB = $this->setSystemNotif($messageIfComplete, $linkIfComplete, $sup, 'dms');
				}  
			}
			if (($data['entry'] == "O")) { // check if Overbreak
				$totalShiftBreak =  $this->general_model->custom_query("SELECT (hour*60) as hour,sum(min) as min,count(*) as total FROM tbl_schedule a,tbl_shift_break b,tbl_break_time_account c,tbl_break_time d where a.acc_time_id=b.acc_time_id and b.bta_id=c.bta_id and c.btime_id=d.btime_id and sched_id=" . $data['sched_id'] . " and b.isActive=1");
				if (count($totalShiftBreak) > 0) {
					$totalShiftBreakTime = $totalShiftBreak[0]->hour + $totalShiftBreak[0]->min;

					$totalWorkBreak = $this->getTotalBreak($data['sched_id']);
					  if (intval($totalWorkBreak) > intval($totalShiftBreakTime)) {
						$violation_overbreak = $this->adddataViolation(4, $supervisor_ID, $sched_id, $schedDate, $_SESSION["uid"], $sched_details->schedtype_id);
						if($violation_overbreak['exist']){
							$this->check_qualified_dtr_violation($violation_overbreak['record']);
						}
						$messageOB = $_SESSION["fname"] . " " . $_SESSION["lname"] . " was overbreak on " . date('F d, Y', strtotime($this->input->post('schedDate'))) . " shift.";
						$linkOB = "discipline/dms_supervision/dtrViolations/violationLogTab";
						$sup = $supervisor[0]->uid;
						$checkOB = $this->setSystemNotif($messageOB, $linkOB, $sup, 'dms');
					}  
				}
			}
			if (($data['entry'] == "O")) {
				$datasched['isComplete'] = 1;
				$wheresched = "sched_id = " . $data['sched_id'];
				$this->general_model->update_vals($datasched, $wheresched, 'tbl_schedule');
			}

			if ($this->input->post("dtrViolationType_ID") != null) { //If Clockout is beyond 2 hours after shiftout

				$dtrViolationType_ID = $this->input->post("dtrViolationType_ID");

				$violation = $this->adddataViolation($dtrViolationType_ID, $supervisor_ID, $sched_id, $schedDate, $_SESSION["uid"], $sched_details->schedtype_id);

				$this->check_qualified_dtr_violation($violation['record']);

				if (($data['entry'] == "O")) {
					if ($this->input->post('reason') != null) {
						$dataViolation['explanation'] = $this->input->post('reason');
						$dataViolation['userDtrViolation_ID'] =  $violation['record'];
						$dataViolation['dateCreated'] =  date("Y-m-d H:i");
						$this->general_model->insert_vals($dataViolation, 'tbl_dms_forgot_logout_explanation');
					}
				}


				if ($this->input->post("dtrViolationType_ID") == 1) {
					$remark = "was late";
				} else if ($this->input->post("dtrViolationType_ID") == 2) {
					$remark = "was undertime";
				} else if ($this->input->post("dtrViolationType_ID") == 3) {
					$remark = "clocked out late";
				} else {
					$remark = "encountered issues with the DTR";
				}

				$resultSystemNotif = array();
				$message = $_SESSION["fname"] . " " . $_SESSION["lname"] . " " . $remark . " on " . date('F d, Y', strtotime($this->input->post('schedDate'))) . " shift.";
				$link = "discipline/dms_supervision/dtrViolations/violationLogTab";
				$sup = $supervisor[0]->uid;
				$resultSystemNotif = $this->setSystemNotif($message, $link, $sup, 'dms');
				$v = ($violation > 0) ? 1 : 0;
				$val  = array("res" => 1, "sup" => $sup, "emp_id" => $_SESSION["emp_id"], "systemNotifID" => $resultSystemNotif);
			} else {
				$v = 1;
				$sup = 0;
				$val = array("res" => 1);
			}
		} else {
			$attendanceLog = 0;
			$v = 0;
			$sup = 0;
			$val = array("res" => 0);
		}

		$val["personal"] = $this->personalInfo();
		// $attendanceLog = 1;
		//echo $v." ".$attendanceLog;


		echo ($attendanceLog == 1 && $v == 1) ? json_encode($val) : 0;
	}

	
			public function personalInfo(){
				$emp_id = $_SESSION["emp_id"];
				$query="SELECT b.emp_id,d.uid,a.fname,a.lname,c.acc_description,Supervisor as sup,a.pic,acc_name FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_user d where a.apid=b.apid and b.acc_id=c.acc_id and b.emp_id=d.emp_id and b.emp_id=$emp_id and b.isActive='yes'";
				$result = $this->general_model->custom_query($query);
				if(!empty($result)){
				foreach($result as $row){
					$dtr = $this->dtrStatus($row->emp_id);
					$position = $this->getCurrentPosition($row->emp_id);
					$arr[] = array(
						"emp_id" => $row->emp_id,
						"uid" => $row->uid,
						"fname" => $row->fname,
						"lname" => $row->lname,
						"acc_description" => $row->acc_description,
						"sup" => $row->sup,
						"pic" => $row->pic,
						"dtr" => $dtr,
						"account" => $row->acc_name,
						"position" => $position[0]->pos_details,
					);
					
				}
					return $arr;
				}else{
					return "Empty";
				}
				
			}
			public function issues()
			{
				$this->error_500();
			}
			public function dtrInCheck(){
			date_default_timezone_set('Asia/Manila');
			$emp_id = $_SESSION["emp_id"];
			$dateTime = date("Y-m-d H:i:s");
			$date = date("Y-m-d");
			// $schedQry = "select sched_id,sched_date,time_start,time_end,b.acc_time_id from tbl_schedule b,tbl_acc_time c,tbl_time d where c.acc_time_id=b.acc_time_id and c.time_id=d.time_id and emp_id=$emp_id and sched_date='".$date."'";
			$schedQry = "select DISTINCT(b.sched_id),sched_date,time_start,time_end,b.acc_time_id,e.acc_time_id as dtrAccTime from tbl_acc_time c,tbl_time d, tbl_schedule b left join tbl_dtr_logs e on e.sched_id=b.sched_id where c.acc_time_id=b.acc_time_id and c.time_id=d.time_id and b.emp_id=$emp_id and sched_date='".$date."' and e.acc_time_id is null";
			$sched = $this->general_model->custom_query($schedQry);
			$data=array();
				if(count($sched)>0){
					//echo $sched[0]->time_start."|".$sched[0]->time_end."|".$sched[0]->sched_date."|".$sched[0]->sched_id;
					if($sched[0]->time_start=="12:00:00 AM"){
						$date = date("Y-m-d",strtotime("+1 day", strtotime($sched[0]->sched_date)));
						$shift = $date." ".$sched[0]->time_start;
					}else{
						$shift = $sched[0]->sched_date." ".$sched[0]->time_start;
					}
					$limit_hour = date("H:i",strtotime("-30 minutes", strtotime($shift)));
						$limit_day_12MN = date("Y-m-d",strtotime("-1 day", strtotime($shift)));
					$limit = ($limit_hour=="00:00") ? $limit_day_12MN." 24:00:00" : date("Y-m-d H:i:s",strtotime("-30 minutes", strtotime($shift)));
					$data["login_stat"] = (strtotime(date("Y-m-d H:i:00")) > strtotime($shift)) ? "Late" : "OK";
					$data["login_allowed"] = (strtotime($limit) > strtotime(date("Y-m-d H:i:s"))) ? "Not" : "OK";
					$data["login_allowed_time"] = (strtotime($limit) > strtotime(date("Y-m-d H:i:s"))) ? date("F d, Y h:i A",strtotime($limit)) : "OK";
					$data["limit"] = $limit;
					$data["time_start"] = $sched[0]->time_start;
					$data["time_end"] = $sched[0]->time_end;
					$data["sched_date"] = $sched[0]->sched_date;
					$data["sched_id"] = $sched[0]->sched_id;
					$data["acc_time_id"] = $sched[0]->acc_time_id;
 					  echo json_encode($data);
						// echo date("Y-m-d H:i:s",strtotime("2018-06-04 12:00:00 AM"));

				}else{
					echo 0;
				} 
			}
			
			public function dtrOutcheck(){
				date_default_timezone_set('Asia/Manila');
				$uid = $_SESSION["emp_id"];

				// $last_log_qry = "select dtr_id,log,a.emp_id,a.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a,tbl_schedule b,tbl_acc_time c,tbl_time d where a.sched_id=b.sched_id and c.acc_time_id=b.acc_time_id and c.time_id=d.time_id and a.emp_id='".$uid."' and a.type='DTR' order by log desc limit 1";
				$last_log_qry = "select dtr_id,log,a.emp_id,b.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a left join tbl_schedule b on a.sched_id=b.sched_id left join tbl_acc_time c on c.acc_time_id=b.acc_time_id left join tbl_time d on c.time_id=d.time_id where a.emp_id='".$uid."' and a.type='DTR' and info is null order by log desc limit 1";
				$data["emp_last_logs"] = $this->general_model->custom_query($last_log_qry);
				 
				 $schedDate = $data["emp_last_logs"][0]->sched_date;
				 $in = $data["emp_last_logs"][0]->log;
				  $finalShiftStart = ($data["emp_last_logs"][0]->time_start=="12:00:00 AM") ? "23:59:59" : $data["emp_last_logs"][0]->time_start ; 
				 $shiftIN =  date_format(date_create($finalShiftStart), 'H:i');
				 $shiftOUT = date_format(date_create($data["emp_last_logs"][0]->time_end), 'H:i');
				 $ampm = explode("|",$this->getTimeGreeting($shiftIN,$shiftOUT));
				 $dateOut = ($ampm[1]=="Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($schedDate))) : $schedDate;
				 $loginNew = (strtotime($in) <= strtotime($schedDate." ".$shiftIN)) ? $schedDate." ".$shiftIN : $in;
				 $logoutNew =  $dateOut." ".$shiftOUT;
				 
 					date_default_timezone_set('Asia/Manila');
					$data["out_limit"] = $logoutNew;
					$limit = date("Y-m-d H:i:s",strtotime("+120 minutes", strtotime($data["out_limit"])));
					$notif_logout_remarks = (strtotime(date("Y-m-d H:i:s")) > strtotime($limit)) ? "Over" : "OK"; 
					$notif_UT = (strtotime(date("Y-m-d H:i:s")) > strtotime($logoutNew)) ? "OK" : "UT"; 
					 
					$data["limit_remarks"] = $notif_logout_remarks;
					$data["limit_ut"] = $notif_UT;
					$data["out_limit"] = $finalShiftStart;
					$data["limit_hour"] = "2 hours";
					$data["limit"] = $limit;
					$data["logoutNew"] = $logoutNew;
					$data["loginNew"] = $loginNew;
					$data["ampm"] = $ampm;
					$data["shiftIN"] = $shiftIN;
					$data["shiftOUT"] = $shiftOUT;
					$data["schedDate"] = $data["emp_last_logs"][0]->sched_date;

				 echo json_encode($data);
			}
			
			
			function getTimeGreeting($shiftIN,$shiftOUT){
			date_default_timezone_set('Asia/Manila');
				$getTimeIn = explode(":",$shiftIN);
				$getTimeOut = explode(":",$shiftOUT);
					if(((int)$getTimeIn[0]>0 && (int)$getTimeIn[0]<12) && ((int)$getTimeOut[0]>=0 && (int)$getTimeOut[0]<12)){
						
						$ampm = "AM|AM";
						$l = 1;
					}else{
						$in = ((int)$getTimeIn[0]>=0 && (int)$getTimeIn[0]<=12) ? "AM" : "PM" ;
						$out = (((int)$getTimeOut[0]>=12) && ((int)$getTimeOut[0]<=23)) ? "PM" : "AM" ;
						$ampm =$in."|".$out;
						$l = $in." --- ".$out;
					} 
				
					if($ampm=="PM|AM"){
						$newVal = "Today|Tomorrow|"; 
					}else{
						$newVal = "Today|Today"; 
					}
			  return  $newVal;
			} 
	function getTimeGreeting2(){
		$shiftIN = "12:30:00 AM";
		$shiftOUT = "09:30:00 AM";
			date_default_timezone_set('Asia/Manila');
				$getTimeIn = explode(":",$shiftIN);
				$getTimeOut = explode(":",$shiftOUT);
					if(((int)$getTimeIn[0]>0 && (int)$getTimeIn[0]<12) && ((int)$getTimeOut[0]>=0 && (int)$getTimeOut[0]<12)){
						
						$ampm = "AM|AM";
						$l = 1;
					}else{
						$in = ((int)$getTimeIn[0]>0 && (int)$getTimeIn[0]<=12) ? "AM" : "PM" ;
						$out = (((int)$getTimeOut[0]>=12 && (int)$getTimeOut[0]<=23)) ? "PM" : "AM" ;
						$ampm =$in."|".$out;
						$l = $in." --- ".$out;
					} 
				
					if($ampm=="PM|AM"){
						$newVal = "Today|Tomorrow"; 
					}else{
						$newVal = "Today|Today"; 
					}
			  echo  $newVal;
			} 
			
		
 
			public function viewLogsDetails(){
			date_default_timezone_set('Asia/Manila');
			$sched_id = $this->input->post("sched_id");
			
 			$emp_id = $_SESSION["emp_id"];
 			  	// $query = "SELECT a.sched_id,a.emp_id,a.sched_date,b.type,b.style,(select concat(time_start,' to ',time_end)  from tbl_acc_time as x,tbl_time as y where x.time_id=y.time_id and x.acc_time_id=a.acc_time_id) as times FROM tbl_schedule a,tbl_schedule_type b where a.schedtype_id =b.schedtype_id and a.isActive=1 and emp_id = $emp_id  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' ";
 			  	$query = "SELECT a.sched_id,a.emp_id,a.sched_date,b.type,b.style,(select concat(time_start,' to ',time_end)  from tbl_acc_time as x,tbl_time as y where x.time_id=y.time_id and x.acc_time_id=a.acc_time_id) as times FROM tbl_schedule a,tbl_schedule_type b where a.schedtype_id =b.schedtype_id and a.isActive=1 and emp_id = $emp_id  and sched_id=$sched_id";
 
			$data = $this->general_model->custom_query($query);
			
			foreach($data as $row){
				$login = $this->getLogin($row->sched_id);
				$login1 = (count($login)>0) ?  date_format(date_create($login[0]->log),"F d Y, h:i:s A")  : "<small style='color:#ff93a6'>No Logs</small>";
					$login_log_ifnaa = (count($login)>0) ? $login[0]->log : date("Y-m-d H:i:s");
				$login2 = (count($login)>0) ? $login[0]->dtr_id : 0;
				$loginentry1 = (count($login)>0) ? $login[0]->entry : 0;
				$logout = $this->getLogout($row->sched_id);
				$logout1 = (count($logout)>0) ?  date_format(date_create($logout[0]->log),"F d Y, h:i:s A") : "<small style='color:#ff93a6'>No Logs</small>";
					$logout_log_ifnaa = (count($logout)>0) ? $logout[0]->log : date("Y-m-d H:i:s");
				$logout2 = (count($logout)>0) ? $logout[0]->dtr_id : 0;
				$logout3 = (count($logout)>0) ? $logout[0]->dtr_id : ($login2*1000);
				$logoutentry1 = (count($logout)>0) ? $logout[0]->entry : 0;
					// $llbreak = $this->getLunchBreak($row->emp_id,$login2,$logout3);
						$llbreak_sbrk = $this->getLunchBreak_sbrk($row->emp_id,$login2,$logout3);

							$llbreak1 = ($llbreak_sbrk[0]->break!=null) ? explode(",",$llbreak_sbrk[0]->break) : array(0,0);
					  
					// $ffbreak = $this->getFirstBreak($row->emp_id,$login2,$logout3);
						$ffbreak_sbrk = $this->getFirstBreak_sbrk($row->emp_id,$login2,$logout3);
				
							$ffbreak1 = ($ffbreak_sbrk[0]->break!=null) ? explode(",",$ffbreak_sbrk[0]->break) : array(0,0);
 					
					// $llastbreak = $this->getLastBreak($row->emp_id,$login2,$logout3);
						$llastbreak_sbrk = $this->getLastBreak_sbrk($row->emp_id,$login2,$logout3);
				
							$llastbreak1 = ($llastbreak_sbrk[0]->break!=null) ? explode(",",$llastbreak_sbrk[0]->break) : array(0,0);
					
					$lastbreak_total =  ($llastbreak1[0]!=0) ? round(abs(strtotime($llastbreak1[0])- strtotime($llastbreak1[1])) / 60,2) :  0;
					$lunchbreak_total = ($llbreak1[0]!=0) ? round(abs(strtotime($llbreak1[0])- strtotime($llbreak1[1])) / 60,2) :  0;
					$firstbreak_total = ($ffbreak1[0]!=0) ?  round(abs(strtotime($ffbreak1[0])- strtotime($ffbreak1[1])) / 60,2) :  0;
					$total_break = $lastbreak_total+$lunchbreak_total+$firstbreak_total;
 					$totalWorked = round(abs(strtotime($login_log_ifnaa)- strtotime($logout_log_ifnaa)) / 60,2) ;

				$arr[] = array(
					"sched" => date_format(date_create($row->sched_date), "M d, Y <br>l "),
					"login" => ($login1 != 0) ? date_format(date_create($login1),"h:i:s A") : $login1,
					"login_raw" => $login1,
					"sched_date" => date_format(date_create($row->sched_date), "M d, Y <br>l "),
					"login_id" => $login2,
					"logout_id" => $logout2,
					"login_entry" => $loginentry1,
					"logout" => ($logout1 != 0) ? date_format(date_create($logout1),"h:i:s A") : $logout1,
					"logoutzzzz" => $logout,
					"logout_raw" => $logout1,
					"logout_entry" => $logoutentry1,
					"type" => $row->type,
					"style" => $row->style,
					"emp_id" => $row->emp_id,
					// "emp_id2" => count($llbreak),
					"shift" =>  ($row->times) ? $row->times : $row->type,
					"sched_id" => $row->sched_id,
					"lunchb" =>  ($llbreak1[0]!=0) ? "<u>Out:</u> ".date_format(date_create($llbreak1[0]),"F d Y, h:i:s A")."<br><u>In:</u> ".date_format(date_create($llbreak1[1]),"F d Y, h:i:s A") : "<small style='color:#ff93a6'>No Logs</small>",
					"firstb" => ($ffbreak1[0]!=0) ? "<u>Out:</u> ".date_format(date_create($ffbreak1[0]),"F d Y, h:i:s A")."<br> <u>In:</u> ".date_format(date_create($ffbreak1[1]),"F d Y, h:i:s A") :  "<small style='color:#ff93a6'>No Logs</small>",
					"lastb" => ($llastbreak1[0]!=0) ? "<u>Out:</u> ".date_format(date_create($llastbreak1[0]),"F d Y, h:i:s A")."<br> <u>In:</u> ".date_format(date_create($llastbreak1[1]),"F d Y, h:i:s A") :  "<small style='color:#ff93a6'>No Logs</small>",
					"total_break" => $total_break." mins.",
					"lastbreak_total" => $lastbreak_total,
					"lunchbreak_total" => $lunchbreak_total,
					"firstbreak_total" => $firstbreak_total,
					"worked_total" => $totalWorked,
					"worked_total_idle" => (720-($totalWorked)),
					"_loginNaa" =>$login_log_ifnaa,
					"_logoutNaa" =>$logout_log_ifnaa,
					"_current" =>date("Y-m-d H:i:s"),
				);
			}
			 //echo json_encode($data["sched_date"]);
			  echo json_encode($arr);
 		}
		public function viewLogsDetails2header(){
		 header('Content-Type: application/json');
		$arr= array(
		  array("name"=>"sched_date","title"=>"Schedule Date"),
		  array("name"=>"login","title"=>"Login"),
		  array("name"=>"firstb","title"=>"First Break"),
		  array("name"=>"lunchb","title"=>"Lunch"),
		  array("name"=>"lastb","title"=>"Last Break"),
		  array("name"=>"logout","title"=>"Logout"),
		
		);
		 echo json_encode($arr);
		
		}
		 
		public function getLogin($schedID){
			$qry = $this->db->query("SELECT dtr_id,log,entry FROM `tbl_dtr_logs` where type='DTR' and   entry='I' and sched_id = ".$schedID."");
			return $qry->result();
		}
		public function getLogout($schedID){
			$qry = $this->db->query("SELECT dtr_id,log,entry FROM `tbl_dtr_logs` where type='DTR' and   entry='O' and sched_id = ".$schedID."");
			return $qry->result();
		}
		public function getLunchBreak1($emp_id,$login,$logout){
  			$qry = $this->db->query("select log as break,entry,type from tbl_break_time_account c,tbl_break d,tbl_break_time e,tbl_dtr_logs a left join tbl_shift_break b on a.acc_time_id=b.sbrk_id where a.emp_id='$emp_id' and b.bta_id=c.bta_id and c.break_id =d.break_id and e.btime_id=c.btime_id  and dtr_id>".$login." and dtr_id<".$logout."  and entry='O' and break_type='Lunch' limit 1");
 			return $qry->result();
		}
		public function getLunchBreak1Test($emp_id,$login,$logout){
 			return "select log as break,entry from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Lunch' and dtr_id>".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and entry='O' order by log asc limit 1";
		}
		public function getLunchBreak($emp_id,$login,$logout){
   			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Lunch' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and g.type='Break' limit 1");
			return $qry->result();
		}
		public function getLunchBreak_sbrk($emp_id,$login,$logout){
  			// $qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break  from tbl_acc_time a,tbl_account b, tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g,tbl_shift_break h where a.acc_id= b.acc_id and d.bta_id = h.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id=h.sbrk_id and f.break_type='Lunch' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and g.type='Break' limit 1");
  			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from  tbl_dtr_logs a,tbl_shift_break b,tbl_break_time_account c,tbl_break d where  a.acc_time_id=b.sbrk_id and b.bta_id=c.bta_id and c.break_id=d.break_id and dtr_id> ".$login." and dtr_id<".$logout." and a.emp_id =".$emp_id." and a.type='Break' and break_type='LUNCH' limit 1");
			return $qry->result();
		}
		public function getFirstBreak($emp_id,$login,$logout){
  			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='First Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and g.type='Break' limit 1 ");
				return $qry->result();
			
		}
		public function getFirstBreak_sbrk($emp_id,$login,$logout){
  			// $qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break  from tbl_acc_time a,tbl_account b, tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g,tbl_shift_break h where a.acc_id= b.acc_id and d.bta_id = h.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id=h.sbrk_id and f.break_type='First Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and g.type='Break' limit 1");
  			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from  tbl_dtr_logs a,tbl_shift_break b,tbl_break_time_account c,tbl_break d where  a.acc_time_id=b.sbrk_id and b.bta_id=c.bta_id and c.break_id=d.break_id and dtr_id> ".$login." and dtr_id<".$logout." and a.emp_id =".$emp_id." and a.type='Break' and break_type='FIRST BREAK' limit 1");
			return $qry->result();
		}
		public function getLastBreak($emp_id,$login,$logout){
  			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from tbl_acc_time a,tbl_account b, tbl_time c,tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g where a.acc_id= b.acc_id and a.time_id = c.time_id and d.bta_id = c.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id = a.acc_time_id and break_type='Last Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and g.type='Break' limit 1 ");
			return $qry->result();
		}
		public function getLastBreak_sbrk($emp_id,$login,$logout){
  			// $qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break  from tbl_acc_time a,tbl_account b, tbl_break_time_account d,tbl_break_time e,tbl_break f,tbl_dtr_logs g,tbl_shift_break h where a.acc_id= b.acc_id and d.bta_id = h.bta_id and d.btime_id= e.btime_id and f.break_id = d.break_id and g.acc_time_id=h.sbrk_id and f.break_type='Last Break' and dtr_id> ".$login." and dtr_id<".$logout." and emp_id =".$emp_id." and g.type='Break' limit 1");
  			$qry = $this->db->query("select concat(SUBSTRING_INDEX(group_concat(if(entry='O',log,null)), ',',1),',', SUBSTRING_INDEX(group_concat(if(entry='I',log,null) separator ','), ',',1)) as break from  tbl_dtr_logs a,tbl_shift_break b,tbl_break_time_account c,tbl_break d where  a.acc_time_id=b.sbrk_id and b.bta_id=c.bta_id and c.break_id=d.break_id and dtr_id> ".$login." and dtr_id<".$logout." and a.emp_id =".$emp_id." and a.type='Break' and break_type='LAST BREAK' limit 1");
			return $qry->result();
		}
		public function getbreak_sbrk($emp_id,$login,$logout){
  			$qry = $this->db->query("select dtr_id,log,entry,break_type from tbl_dtr_logs a,tbl_shift_break b,tbl_break_time_account c,tbl_break d where a.acc_time_id=b.sbrk_id and b.bta_id=c.bta_id and c.break_id=d.break_id and dtr_id> $login and dtr_id<$logout and emp_id=$emp_id and info is null and type='Break'");
			return $qry->result();
		}
		public function requestTiTo(){
			date_default_timezone_set("Asia/Manila");
			 $arr = array(
				"dateRequest" => $this->input->post("date"),
				"acc_time_id" => $this->input->post("schedule"),
				"message" => $this->input->post("reason"),
				"actualDate" => $this->input->post("actualDate"),
				"startDateWorked" => $this->input->post("startDateWorked"),
				"startTimeWorked" => $this->input->post("startTimeWorked"),
				"endDateWorked" => $this->input->post("endDateWorked"),
				"endTimeWorked" => $this->input->post("endTimeWorked"),
				"timetotal" => $this->input->post("timetotal"),
				"userID" => $_SESSION["uid"], 
				"dateCreated" => date("Y-m-d H:i:s"),
				"updatedBy" => $_SESSION["uid"], 
			 );				

			$dtrRequest_ID = $this->general_model->insert_vals_last_inserted_id($arr, "tbl_dtr_request"); 
			 
			if ($dtrRequest_ID === 0){
				$status = "Failed";
				echo 0;
			}else{
				$status = "Success";
				$createdOn = date("Y-m-d H:i:s");
				$approvers = $this->set_approvers($createdOn,$_SESSION["uid"],3,'dtrRequest_ID',$dtrRequest_ID);
				$insert_stat['insert_approvers'] = $this->general_model->batch_insert($approvers, 'tbl_dtr_request_approval');
			
					// SET NOTIFICATION
				$notif_mssg = "has sent you a Time-in & Time-out request for ".date("F d, Y",strtotime($this->input->post("date")))." shift. <br><small><b>TITO-ID</b>: " . str_pad($dtrRequest_ID, 8, '0', STR_PAD_LEFT) . " </small>";
				$link = "dtr/request";
				$notif_details = $this->set_notif($_SESSION["uid"], $notif_mssg, $link, $approvers);
				echo json_encode($notif_details);
			}
		}
		public function report(){
				$data = array('title' => 'Report');
				if ($this->check_access()) {
					$this->load_template_view('templates/dtr/attendance/report',$data);
				}
		}
		  public function check_temp_access_sup(){
				// $acc_id = $_SESSION["acc_id"];
				 $emp_id = $this->session->userdata('emp_id');
				 $access_arr = [1464, 813, 1796, 250, 745, 88, 1396, 152, 286, 43, 26, 557];
			  
				if(!in_array($emp_id, $access_arr)){
					//$this->error_403();
					return false;
				}else{
					return true;
				}
			}
		public function request(){
				// if ($this->check_access()) {
				// 	//  $access = $this->check_temp_access_sup();
				// 	  if($access){
				// 			header("location:../tito/approval");

				// 	  }else{
				// 		$data = array('title' => 'DTR Request');
				// 		$this->load_template_view('templates/dtr/attendance/request',$data);  
				// 	  }
					
				// }
				$data = [
					'uri_segment' => $this->uri->segment_array(),
					'title' => 'TITO Approval',
				];
				if ($this->check_access()) {
					$this->load_template_view('templates/tito/approval', $data);
				}
		}
		public function records(){
				$data = array('title' => 'Record');
 				$this->load_template_view('templates/dtr/attendance/record',$data);
		}
		public function dtrmonitor(){
				$data = array('title' => 'Record');
				if ($this->check_access()) {
					$this->load_template_view('templates/dtr/attendance/dtrmonitor',$data);
				}
		}
		public function check_temp_access(){
        $acc_id = $_SESSION["acc_id"]; 
		$access_arr = [23, 28, 69, 80,37];
        
        if(!in_array($acc_id, $access_arr)){
          //  $this->error_403();
            return false;
        }else{
            return true;
        }
    }
	
		public function myrequest(){
			 $access = $this->check_temp_access();
				$data = array('title' => 'Times-in & Time-out Request');
				 if(!$access){
					$this->load_template_view('templates/dtr/attendance/myrequest',$data);
				 }else{
					 header("location:../tito/request");
				 }
		}
		public function attendancereport(){
				$data = array('title' => 'Attendance Report');
 				$this->load_template_view('templates/dtr/attendance/dtr_report',$data);
		}
		
		public function breakPage(){
		date_default_timezone_set('Asia/Manila');
				$data = array('title' => 'Break');
				  // $last_log_qry = "select dtr_id,log,a.emp_id,a.acc_time_id,entry,type,a.sched_id,sched_date,time_start,time_end from tbl_dtr_logs a left join tbl_schedule b on a.sched_id=b.sched_id left join tbl_acc_time c on c.acc_time_id=b.acc_time_id left join tbl_time d on c.time_id=d.time_id where a.emp_id='".$_SESSION["emp_id"]."' order by log desc limit 1";
				  $last_log_qry = "select dtr_id,log,a.emp_id,a.acc_time_id,entry,a.type,a.sched_id,d.break_type,e.hour,e.min from tbl_break_time_account c,tbl_break d,tbl_break_time e,tbl_dtr_logs a left join tbl_shift_break b on a.acc_time_id=b.sbrk_id where a.emp_id='".$_SESSION["emp_id"]."' and b.bta_id=c.bta_id and c.break_id =d.break_id and e.btime_id=c.btime_id and  type='Break' order by log desc limit 1";
				  $now = date("Y-m-d H:i:s");
				$break = "select TIMEDIFF('".$now."',log) as diff FROM tbl_dtr_logs WHERE emp_id='".$_SESSION['emp_id']."' and type='Break' order by dtr_id desc limit 1";

 				$data["emp_last_logs"] = $this->general_model->custom_query($last_log_qry);
 				$data["break"] = $this->general_model->custom_query($break);
					$data["lastlogtype"] = $data["emp_last_logs"][0]->type;
					$data["lastlogentry"] = $data["emp_last_logs"][0]->entry;
				if($data["emp_last_logs"][0]->entry=='O' && $data["emp_last_logs"][0]->type=='Break'){
 				$this->load_template_view('templates/dtr/attendance/break',$data);
				}else{
					redirect('index.php/dtr/attendance');
				}
		}
		
		public function checkTitoRequest(){
		date_default_timezone_set('Asia/Manila');
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$emp_id = $this->input->post("emp_id");
			$titoDetailsqry = "SELECT g.fname,g.lname,a.*,message,a.acc_time_id,time_start,time_end,a.dateCreated,g.pic,b.userId,f.acc_id,h.description FROM tbl_dtr_request a, tbl_dtr_request_approval b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h where a.status_ID=h.status_ID and a.dtrRequest_ID = b.dtrRequest_ID and a.userID=c.uid and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.dtrRequest_ID=".$dtrRequest_ID;
			
			$empPosQry = "SELECT pos_details,d.status FROM tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d where a.posempstat_id =b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and a.isActive=1 and emp_id=".$emp_id;
			$rs["titoDetailsqry"] = $this->general_model->custom_query($titoDetailsqry);
			$rs["empPosQry"] = $this->general_model->custom_query($empPosQry);
			echo (count($rs["titoDetailsqry"])>0) ? json_encode($rs) : 0;
		}
		public function takeBreak(){
		
		date_default_timezone_set('Asia/Manila');
				$arr = array(
 					"emp_id" => $_SESSION["emp_id"],
					"acc_time_id" => $this->input->post("sbrk_id"),
					"entry" => $this->input->post("type"),
					"note" => $this->input->post("note"),
					"log" => date("Y-m-d H:i:s"),
					"type" => "Break",
				 );
				$breakLog["result"] = $this->general_model->insert_vals_last_inserted_id($arr, "tbl_dtr_logs"); 
				$breakLog["personal"] = $this->personalInfo();
				$entry_data = ($this->input->post("type")=="I") ? "I - Back from break" : "O - Out for break" ;
				$qrytraill =  $this->db->query("insert into tbl_login_logout_session(ipaddress,uid,log,remark,name) values('".$_SERVER['REMOTE_ADDR']."',".$_SESSION["uid"].",now(),'".$entry_data."','".$_SESSION["fname"]." ".$_SESSION["lname"]."')");

				echo json_encode($breakLog);
			
		}
		
		public function listRequest($hr=null){
			
			$datatable = $this->input->post('datatable');

			$user_id = $datatable['query']['user_id'];
			if($hr==null){
				#Changed 2/13/2019; included DEADline
				// $query['query'] ="SELECT g.fname,g.lname,a.dtrRequest_ID,message,a.acc_time_id,dateRequest,time_start,time_end,a.dateCreated,g.pic,b.userId,c.emp_id,b.dateTimeStatus,timetotal FROM tbl_dtr_request a, tbl_dtr_request_approval b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g where a.dtrRequest_ID = b.dtrRequest_ID and a.userID=c.uid and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.approvalStatus_ID=4 and b.userId=".$user_id;
				$query['query'] ="SELECT deadline,g.fname,g.lname,a.dtrRequest_ID,message,a.acc_time_id,dateRequest,time_start,time_end,a.dateCreated,g.pic,b.userId,c.emp_id,b.dateTimeStatus,timetotal FROM tbl_dtr_request a, tbl_dtr_request_approval b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_deadline h where a.dtrRequest_ID = b.dtrRequest_ID and a.userID=c.uid and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.approvalStatus_ID=4 and h.request_id=a.dtrRequest_ID and b.userId=".$user_id." and h.userId=".$user_id;
			}else{
				// $query['query'] ="SELECT g.fname,g.lname,a.dtrRequest_ID,message,a.acc_time_id,dateRequest,time_start,time_end,a.dateCreated,g.pic,b.userId,c.emp_id,b.dateTimeStatus,timetotal,h.description,b.approvalStatus_ID  FROM tbl_dtr_request a, tbl_dtr_request_approval b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h where h.status_id = b.approvalStatus_ID and a.dtrRequest_ID = b.dtrRequest_ID and a.userID=c.uid and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.approvalStatus_ID in (12,2)";
				$query['query'] ="SELECT g.fname,g.lname,b.dtrRequest_ID,b.status_ID,time_start,time_end,b.dateCreated,g.pic,b.userId,c.emp_id,h.description,dateRequest,timetotal FROM tbl_dtr_request b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h where b.userID=c.uid and b.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.status_ID=h.status_ID and b.status_ID in (2,12)";
			}
			if (isset($datatable['query']['userAnswerSearch'])) {
					$keyword = $datatable['query']['userAnswerSearch'];
  
					$query['search']['append'] = " and  (g.fname LIKE '%" . $keyword . "%' OR  g.lname LIKE '%" . $keyword . "%' OR  b.dtrRequest_ID LIKE '%" . $keyword. "%') ";

					$query['search']['total'] = " and (g.fname LIKE '%" . $keyword . "%' OR g.lname LIKE '%" . $keyword . "%' OR  b.dtrRequest_ID LIKE '%" . $keyword . "%')";
				}
			$data = $this->set_datatable_query($datatable, $query);

			echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
		}
		public function listRequestRecord($hr=null){
			
			$datatable = $this->input->post('datatable');
			
			$user_id = $datatable['query']['user_id'];
			if($hr==null){
			$query['query'] ="SELECT g.fname,g.lname,a.dtrRequest_ID,a.status_ID,b.approvalStatus_ID,message,a.acc_time_id,dateRequest,time_start,time_end,a.dateCreated,g.pic,b.userId,c.emp_id,h.description as  approverstatus FROM tbl_dtr_request a, tbl_dtr_request_approval b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h where b.approvalStatus_ID =h.status_ID and a.dtrRequest_ID = b.dtrRequest_ID and a.userID=c.uid and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.approvalStatus_ID in (5,6) and b.userId=".$user_id;
			}else{
				$date = $datatable['query']['date'];
				$date = explode(" - ",$date);
				$d1 = date_format(date_create($date[0]),"Y-m-d");
				$d2 = date_format(date_create($date[1]),"Y-m-d");
			$query['query'] ="SELECT g.fname,g.lname,b.dtrRequest_ID,b.status_ID,time_start,time_end,b.dateCreated,g.pic,b.userId,c.emp_id,h.description as approverstatus,dateRequest FROM tbl_dtr_request b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h where b.userID=c.uid and b.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and b.status_ID=h.status_ID and b.status_ID in (5,6) and  dateRequest>= date('$d1') and dateRequest<= date('$d2')";

			}
				if (isset($datatable['query']['searchRecord'])) {
					$keyword = $datatable['query']['searchRecord'];
  
					$query['search']['append'] = " and  (g.fname LIKE '%" . $keyword . "%' OR  g.lname LIKE '%" . $keyword . "%' OR  b.dtrRequest_ID LIKE '%" .$keyword. "%') ";

					$query['search']['total'] = " and (g.fname LIKE '%" . $keyword . "%' OR g.lname LIKE '%" . $keyword . "%' OR  b.dtrRequest_ID LIKE '%" . $keyword . "%')";
				}
			$data = $this->set_datatable_query($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
		}
		
		public function listofmyrequest(){
			
			$datatable = $this->input->post('datatable');

			$user_id = $datatable['query']['user_id'];
			$query['query'] ="SELECT fname,lname,c.description,a.* FROM tbl_dtr_request a,tbl_user b,tbl_status c,tbl_employee d,tbl_applicant e where d.emp_id=b.emp_id and e.apid=d.apid and a.userID=b.uid and a.status_ID=c.status_ID and a.status_ID in (5,6) and a.userID=".$user_id;
				if (isset($datatable['query']['titoRecordSearch'])) {
					$keyword = $datatable['query']['titoRecordSearch'];
  
					$query['search']['append'] = " and  (fname LIKE '%" . $keyword . "%' OR  lname LIKE '%" . $keyword . "%' OR  a.dtrRequest_ID LIKE '%" . (int)$keyword. "%') ";

					$query['search']['total'] = " and (fname LIKE '%" . $keyword . "%' OR lname LIKE '%" . $keyword . "%' OR  a.dtrRequest_ID LIKE '%" . (int)$keyword . "%')";
				}
			$data = $this->set_datatable_query($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
		}
		
		public function showMyLogs(){
			
			$datatable = $this->input->post('datatable');

			$emp_id = $datatable['query']['user_id'];
			$datee = explode("-",$datatable['query']['date']);
			// $query['query'] ="SELECT fname,lname,c.description,a.* FROM tbl_dtr_request a,tbl_user b,tbl_status c where a.userID=b.uid and a.status_ID=c.status_ID and a.status_ID!=2 and a.userID=".$user_id;
			$query['query']  = "SELECT a.sched_id,a.emp_id,a.sched_date,b.type,b.style,(select concat(time_start,' to ',time_end)  from tbl_acc_time as x,tbl_time as y where x.time_id=y.time_id and x.acc_time_id=a.acc_time_id) as times FROM tbl_schedule a,tbl_schedule_type b where a.schedtype_id =b.schedtype_id and a.isActive=1 and emp_id = $emp_id  and sched_date between '".date_format(date_create($datee[0]),"Y-m-d")."' and  '".date_format(date_create($datee[1]),"Y-m-d")."' ";

				if (isset($datatable['query']['titoRecordSearch'])) {
					$keyword = $datatable['query']['titoRecordSearch'];
  
					$query['search']['append'] = " and  (a.sched_id LIKE '%" . (int)$keyword. "%') ";

					$query['search']['total'] = " and ( a.sched_id LIKE '%" . (int)$keyword . "%')";
				}
			$data = $this->set_datatable_query_mark($datatable, $query);

        echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
		}
		
		public function set_datatable_query_mark($datatable, $query){
        $sql = $query['query'];

        $page = $datatable['pagination']['page'];
        $pages = $datatable['pagination']['page'] * $datatable['pagination']['perpage'];
        $perpage = $datatable['pagination']['perpage'];
        $sort = (isset($datatable['sort']['sort'])) ? $datatable['sort']['sort'] : '';
        $field = (isset($datatable['sort']['field'])) ? $datatable['sort']['field'] : '';

        if (isset($query['search']['append'])) {
            $sql .= $query['search']['append'];

            $search = $query['query'] . $query['search']['total'];

            $total = count($this->general_model->custom_query($search));
            $pages = ceil($total / $perpage);

            $page = ($page > $pages) ? 1 : $page;
        } else {
            $total = count($this->general_model->custom_query($sql));
        }

        if (isset($datatable['pagination'])) {

            $offset = $page * $perpage - $perpage;
            $limit = ' LIMIT ' . $offset . ' ,' . $perpage;
            $order = $field ? " ORDER BY  sched_id" : '';

            if ($perpage < 0) {
                $limit = ' LIMIT 0';
            }

            $sql .= $order . ' ' . $sort . $limit;
        }

        $data = $this->general_model->custom_query($sql);
		//echo $this->db->last_query();
        //            var_dump($data);
		$arr = array();
		foreach($data as $row){
				$login = $this->getLogin($row->sched_id);
				$login1 = (count($login)>0) ? $login[0]->log : "<small style='color:#ff93a6'>No Logs</small>";
				$login2 = (count($login)>0) ? $login[0]->dtr_id : 0;
				$loginentry1 = (count($login)>0) ? $login[0]->entry : 0;
				$logout = $this->getLogout($row->sched_id);
				$logout1 = (count($logout)>0) ? $logout[0]->log : "<small style='color:#ff93a6'>No Logs</small>";
				$logout2 = (count($logout)>0) ? $logout[0]->dtr_id : 0;
				$logout3 = (count($logout)>0) ? $logout[0]->dtr_id : ($login2*1000);
				$logoutentry1 = (count($logout)>0) ? $logout[0]->entry : 0;
					// $llbreak = $this->getLunchBreak($row->emp_id,$login2,$logout3);
						$llbreak_sbrk = $this->getLunchBreak_sbrk($row->emp_id,$login2,$logout3);

					  // $llbreak1 = ($llbreak[0]->break!=null) ? explode(",",$llbreak[0]->break) : (($llbreak_sbrk[0]->break!=null) ? explode(",",$llbreak_sbrk[0]->break) : array(0,0));
					  $llbreak1 = ($llbreak_sbrk[0]->break!=null) ? explode(",",$llbreak_sbrk[0]->break) : array(0,0);
					  
				// $ffbreak = $this->getFirstBreak($row->emp_id,$login2,$logout3);
				$ffbreak_sbrk = $this->getFirstBreak_sbrk($row->emp_id,$login2,$logout3);
				
					// $ffbreak1 = ($ffbreak[0]->break!=null) ? explode(",",$ffbreak[0]->break) : (($ffbreak_sbrk[0]->break!=null) ? explode(",",$ffbreak_sbrk[0]->break) : array(0,0));
					$ffbreak1 = ($ffbreak_sbrk[0]->break!=null) ? explode(",",$ffbreak_sbrk[0]->break) : array(0,0);
 					
				// $llastbreak = $this->getLastBreak($row->emp_id,$login2,$logout3);
					$llastbreak_sbrk = $this->getLastBreak_sbrk($row->emp_id,$login2,$logout3);
				
					// $llastbreak1 = ($llastbreak[0]->break!=null) ? explode(",",$llastbreak[0]->break) : ($llastbreak_sbrk[0]->break!=null) ? explode(",",$llastbreak_sbrk[0]->break) : array(0,0);
					$llastbreak1 = ($llastbreak_sbrk[0]->break!=null) ? explode(",",$llastbreak_sbrk[0]->break) : array(0,0);
					
					$lastbreak_total =  ($llastbreak1[0]!=0) ? round(abs(strtotime($llastbreak1[0])- strtotime($llastbreak1[1])) / 60,2) :  0;
					$lunchbreak_total = ($llbreak1[0]!=0) ? round(abs(strtotime($llbreak1[0])- strtotime($llbreak1[1])) / 60,2) :  0;
					$firstbreak_total = ($ffbreak1[0]!=0) ?  round(abs(strtotime($ffbreak1[0])- strtotime($ffbreak1[1])) / 60,2) :  0;
					$total_break = $lastbreak_total+$lunchbreak_total+$firstbreak_total;
				$arr[] = array(
					"sched" => date_format(date_create($row->sched_date), "M d, Y <br>l "),
					"login" => ($login1 != 0) ? date_format(date_create($login1),"h:i:s A") : $login1,
					"login_raw" => ($login1 != 0) ?  date_format(date_create($login1),"M d, Y h:i:s A")  : $login1,
					"sched_date" => date_format(date_create($row->sched_date), "M d, Y <br>l "),
					"login_id" => $login2,
					"logout_id" => $logout2,
					"login_entry" => $loginentry1,
					"logout" => ($logout1 != 0) ? date_format(date_create($logout1),"h:i:s A") : $logout1,
					"logout_raw" => ($logout1 != 0) ? date_format(date_create($logout1),"M d, Y h:i:s A") : $logout1,
					"logout_entry" => $logoutentry1,
					"type" => $row->type,
					"style" => $row->style,
					"emp_id" => $row->emp_id,
					// "emp_id2" => count($llbreak),
 					"shift" =>  ($row->times) ? $row->times : $row->type,
					"sched_id" => $row->sched_id,
					"lunchb" =>  ($llbreak1[0]!=0) ? "Out: ".$llbreak1[0]."<br> In: ".$llbreak1[1] : "<small style='color:#ff93a6'>No Logs</small>",
					"firstb" => ($ffbreak1[0]!=0) ? "Out: ".$ffbreak1[0]."<br> In: ".$ffbreak1[1] :  "<small style='color:#ff93a6'>No Logs</small>",
					"lastb" => ($llastbreak1[0]!=0) ? "Out: ".$llastbreak1[0]."<br> In: ".$llastbreak1[1] :  "<small style='color:#ff93a6'>No Logs</small>",
					"lastb_total" =>  $total_break." mins." ,
					"action" => "<button class='btn btn-sm btn-primary' onclick=test1('test')>Update</button>",
				);
			}
        $meta = [
            "page" => intval($page),
            "pages" => intval($pages),
            "perpage" => intval($perpage),
            "total" => $total,
            "sort" => $sort,
            "field" => $field,
        ];

        $result = [
            'meta' => $meta,
            'data' => $arr,
        ];

        return $result;
    }
		
		
		
		
		public function listofmypendingrequest(){
			$datatable = $this->input->post('datatable');

			$user_id = $datatable['query']['user_id'];
			$query['query'] ="SELECT fname,lname,c.description,a.* FROM tbl_dtr_request a,tbl_user b,tbl_status c,tbl_employee d,tbl_applicant e where d.emp_id=b.emp_id and e.apid=d.apid and a.userID=b.uid and a.status_ID=c.status_ID and a.status_ID in (2,12) and a.userID=".$user_id;
				if(isset($datatable['query']['titoPendingSearch'])){
					$keyword = $datatable['query']['titoPendingSearch'];
					$query['search']['append'] = " and  (fname LIKE '%" . $keyword . "%' OR  lname LIKE '%" . $keyword . "%' OR  a.dtrRequest_ID LIKE '%" . (int)$keyword. "%') ";

					$query['search']['total'] = " and (fname LIKE '%" . $keyword . "%' OR lname LIKE '%" . $keyword . "%' OR  a.dtrRequest_ID LIKE '%" . (int)$keyword . "%')";
				}
			$data = $this->set_datatable_query($datatable, $query);
			echo json_encode(['meta' => $data['meta'], 'data' => $data['data']]);
		}
		public function checkSched(){
			$date= $this->input->post("date");
			$emp_id= $this->input->post("emp_id");
			$qry = "SELECT sched_id,c.acc_time_id,b.type,time_start,time_end,style FROM tbl_schedule_type b,tbl_time d,tbl_schedule a left join tbl_acc_time c on a.acc_time_id=c.acc_time_id where a.schedtype_id = b.schedtype_id and emp_id=$emp_id and sched_date='$date' and c.time_id=d.time_id";
			$qry_rqCnt = "SELECT count(*) as cnt FROM tbl_dtr_request where dateRequest='$date' and userID=".$_SESSION['uid']." and status_ID in(5,2)";
			
			$data["result"] = $this->general_model->custom_query($qry);
			$data["request_count"] = $this->general_model->custom_query($qry_rqCnt);
			echo json_encode($data);
		}
		function checkTotalTime(){
			$start= (date_format(date_create($this->input->post("timeStart")),"H:i")=="00:00") ? strtotime(date_format(date_create($this->input->post("timeStart")),"Y-m-d")." 23:59:59") : strtotime($this->input->post("timeStart"));
			$end= strtotime($this->input->post("timeEnd"));
			$seconds_diff =  $end - $start; 
			// echo $start;
			echo  number_format($seconds_diff/3600, 2);
		}
		function checkTimeOfShift(){
			$shiftIN =  date_format(date_create($this->input->post("timeStart")), 'H:i');
			$shiftOUT = date_format(date_create($this->input->post("timeEnd")), 'H:i');
			$ampm = explode("|",$this->getTimeGreeting($shiftIN,$shiftOUT));
				echo $ampm[0]."|".$ampm[1];
		}
		
		function approveTitoReq($hr=null){
			$dtrRequest_ID = $this->input->post("dtrrequestid");
				$requestor = $this->get_requestor_details($dtrRequest_ID);
				$requestors[0] = ['userId' => $requestor->userId];
			$isApprove = $this->input->post("isApprove");
			$dateRequest = $this->input->post("dateRequest");
			$requestorID = $this->input->post("requestor");
			$approvalLevel = $this->input->post("approvalLevel");
			$comment = $this->input->post("comment");
				$level = $this->checkifLast($dtrRequest_ID);
				if($hr==null){
					$approvalLevel = $this->input->post("approvalLevel");
					$data['approvalStatus_ID'] = $isApprove;
					$data['remarks'] = $comment;
					$where = "dtrRequest_ID=$dtrRequest_ID and approvalLevel=$approvalLevel";
					$update_stat = $this->general_model->update_vals($data, $where, 'tbl_dtr_request_approval');
 				}else{
					$approvalLevel = $level->max+1;
					$dtr['approvalStatus_ID'] = $isApprove;
					$dtr['dtrRequest_ID'] = $dtrRequest_ID;
					$dtr['approvalLevel'] = $level->max+1;
					$dtr['userId'] = $_SESSION["uid"];
					$dtr['remarks'] = $comment;
					$update_stat = $this->general_model->insert_vals($dtr, 'tbl_dtr_request_approval');

				}
				
				if($approvalLevel>=$level->max){ //checks if last approver
					
					if($isApprove==5){		// If final approval	
						$data2['status_ID'] = $isApprove;
						$where2 = "dtrRequest_ID=$dtrRequest_ID";
							$update_stat2 = $this->general_model->update_vals($data2, $where2, 'tbl_dtr_request');
 								$result["approver_level"] ="last";
								$notif_mssg = "Your Time-in Time-out (TITO) request was successfully approved.<br><small><b>TITO-ID</b>: ".str_pad($dtrRequest_ID, 8, '0', STR_PAD_LEFT)." </small>";
								$link = "dtr/myrequest";
								 $result['set_notif_details'] = $this->set_system_notif($notif_mssg, $link, $requestors);
								// $result['set_notif_details'] = $this->set_notif($this->session->userdata('uid'), $notif_mssg, $link, $approvers);

								$result["notif_type"] = "system";
								
								//ADD DTR LOGS
								$checkSchedid  = $this->checkSchedID($dateRequest,$requestorID);
								$result["dateRequest"] = $checkSchedid;

								$record_detail = $this->get_specific_recrd($dtrRequest_ID);
								$login = date_format(date_create($record_detail->startDateWorked." ".$record_detail->startTimeWorked),"Y-m-d H:i:s");
								$logout = date_format(date_create($record_detail->endDateWorked." ".$record_detail->endTimeWorked),"Y-m-d H:i:s");
								 $schedule = $this->addSchedule($record_detail->emp_id,$record_detail->dateRequest,$record_detail->acc_time_id);
								
								$in = $this->dtrInEntry("DTR", $record_detail->acc_time_id, $record_detail->emp_id,$schedule,$login,"I");
								$out = $this->dtrOutEntry("DTR", $record_detail->acc_time_id, $record_detail->emp_id, $schedule,$logout,"O",$in);


				  
					}else{				
						$data2['status_ID'] = 6;
						$where2 = "dtrRequest_ID=$dtrRequest_ID";
							$update_stat2 = $this->general_model->update_vals($data2, $where2, 'tbl_dtr_request');
								$result["approver_level"] ="last";
								$notif_mssg = "Sorry, your Time-in Time-out (TITO) request was not approved.<br><small><b>TITO-ID</b>: ".str_pad($dtrRequest_ID, 8, '0', STR_PAD_LEFT)." </small>";
								$link = "dtr/myrequest";
								$result['set_notif_details'] = $this->set_system_notif($notif_mssg, $link, $requestors);
								$result["notif_type"] = "system";
					}
 				}else{ 
					
						$next_approver = $this->get_next_approver($dtrRequest_ID, ($approvalLevel+1));
						$approvers[0] = ['userId' => $next_approver->userId];
						
					if($isApprove==5){
						$data2['approvalStatus_ID'] = 4;
						$where2 = "dtrRequest_ID=$dtrRequest_ID and approvalLevel=".($approvalLevel+1);
							$update_stat2 = $this->general_model->update_vals($data2, $where2, 'tbl_dtr_request_approval');
							// SET NOTIFICATION TO NEXT APPROVER  --> needs to Modify 
								
								$approval_level_ordinal = $this->ordinal($approvalLevel+1);
								
								$notif_mssg = "forwarded to you the Time-in Time-out (TITO) request of <strong>".$requestor->fname." ".$requestor->lname."</strong> for $approval_level_ordinal Approval.<br><small><b>TITO-ID</b>: ".str_pad($dtrRequest_ID, 8, '0', STR_PAD_LEFT)." </small>";
								$link = "dtr/request";
								$result['set_notif_details'] = $this->set_notif($this->session->userdata('uid'), $notif_mssg, $link, $approvers);
								$result["notif_type"] = "request";
					}else{				
						$data2['status_ID'] = 6;
						$where2 = "dtrRequest_ID=$dtrRequest_ID";
							$update_stat2 = $this->general_model->update_vals($data2, $where2, 'tbl_dtr_request');
								$notif_mssg = "Sorry, your Time-in Time-out (TITO) request was not approved.<br><small><b>TITO-ID</b>: ".str_pad($dtrRequest_ID, 8, '0', STR_PAD_LEFT)." </small>";
								$link = "dtr/myrequest";
								$result['set_notif_details'] = $this->set_system_notif($notif_mssg, $link, $requestors);
								$result["notif_type"] = "system";
								
								$data4['approvalStatus_ID'] = 7;
								$where4 = "dtrRequest_ID=$dtrRequest_ID and approvalLevel>=".($approvalLevel+1);
								$update_stat4 = $this->general_model->update_vals($data4, $where4, 'tbl_dtr_request_approval');
								$this->set_proceeding_approvers_deadline_stat($dtrRequest_ID,$_SESSION["uid"],$approvalLevel);
								
					}
					
					$result["approver_level"] ="not_last";
					
 				
				
				}
				$this->update_ahr_deadline_stat($dtrRequest_ID);
				$result["status_update"] =$update_stat+$update_stat2;
				$result["approver_level_cnt"] = $approvalLevel;
				$result["rsssz"] = $requestor;
				echo json_encode($result);  
				
		}
		private function set_proceeding_approvers_deadline_stat($request_id, $current_approver_id, $current_approval_level){
			$data['status_ID'] = 7;
			$where = "module_ID = 3 AND request_ID = $request_id AND userId != $current_approver_id AND approvalLevel > $current_approval_level";
			return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_deadline');
		}
		private function checkSchedID($dateRequest,$requestor){
			$query = "SELECT distinct(c.sched_id) as sched_id,c.schedtype_id  FROM tbl_dtr_request a,tbl_user b left join tbl_schedule c on b.emp_id=c.emp_id where a.userID=b.uid and b.uid=$requestor and dateRequest='".$dateRequest."' and sched_date='".$dateRequest."' and c.schedtype_id in (1,4,5)";
			$rs = $this->general_model->custom_query($query);
			$delete_stat =0;
			if(count($rs)>0){
				foreach($rs as $row){
					$details = $this->totalHours($row->sched_id,"custom");
					 
					if(count($details)>0){
						if($details["remaining_hour"]>0){
							$where['sched_id'] = $row->sched_id;
							$where['isLocked'] = 0;
							$delete_stat = $this->general_model->delete_vals($where, 'tbl_schedule');  

							if($delete_stat>0){ // Checks if schedule was successfully deleted. Set sched_id in DTR to null.
								$data_dtr_sched['sched_id'] = NULL;
								$where_dtr_sched = "sched_id=".$row->sched_id;
								$this->general_model->update_vals($data_dtr_sched, $where_dtr_sched, 'tbl_dtr_logs');
							}

							
						}
					}
				}
			}else{
					$query_user = "SELECT emp_id from tbl_user where uid=".$requestor;
					$rs_user = $this->general_model->custom_query($query_user);

				$where['sched_date'] = $dateRequest;
				$where['emp_id'] = $rs_user[0]->emp_id;
				$where['isLocked'] = 0;
				$delete_stat = $this->general_model->delete_vals($where, 'tbl_schedule');   
 
			}
			return $delete_stat;
			
		} 
		private function update_ahr_deadline_stat($request_id){
			$data['status_ID'] = 3;
			$user_id = $this->session->userdata('uid');
			$where = "module_ID = 3 AND request_ID = $request_id AND userId = $user_id";
			return $update_stat = $this->general_model->update_vals($data, $where, 'tbl_deadline');
		}
		function addSchedule($emp_id,$sched_date,$acc_time_id){
			$dtr['emp_id'] = $emp_id;
			$dtr['sched_date'] = $sched_date;
			$dtr['acc_time_id'] =  $acc_time_id;
			$dtr['schedtype_id'] =  1;
			$dtr['remarks'] =  "TITO";
			$dtr['updated_by'] = $_SESSION["uid"];
			$dtr['isActive'] =1;
			$dtr['isDouble'] =0;
			// $dtr['isLocked'] =14;
			return $this->general_model->insert_vals_last_inserted_id($dtr, 'tbl_schedule');

		}
		function dtrInEntry($type, $acc_time_id,$emp_id,$sched_id,$log,$entry){
			date_default_timezone_set('Asia/Manila');
			$dtr['type'] = $type;
			$dtr['acc_time_id'] = $acc_time_id;
			$dtr['emp_id'] =  $emp_id;
			$dtr['sched_id'] =  $sched_id;
			$dtr['log'] =  $log;
			$dtr['entry'] = $entry;
			$dtr['info'] = "TITO";
			$violation_late = 1;
			$sched = $this->general_model->custom_query("Select sched_id,a.acc_time_id,time_start,time_end,sched_date from tbl_schedule a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and sched_id=" . $sched_id);
			$time2 = ($sched[0]->time_start == "12:00:00 AM") ? "24:00" : date_format(date_create($sched[0]->time_start), 'H:i');
			$date1 = date_format(date_create($log), 'Y-m-d H:i');
			$date2 = $sched[0]->sched_date . " " . $time2;
			$inData = $this->general_model->insert_vals_last_inserted_id($dtr, 'tbl_dtr_logs');
			if (strtotime($date1) > strtotime($date2)) { //Late
				// $supervisor = $this->get_direct_supervisor_via_emp_id($_SESSION["emp_id"]);
				$supervisor = $this->get_direct_supervisor_via_emp_id($emp_id);
				$uidd = $this->get_emp_details_via_emp_id($emp_id);
				$violation_late = $this->adddataViolation(1, $supervisor->Supervisor, $sched_id, $sched[0]->sched_date, $uidd->uid);
				if($violation_late['exist']){
					$this->check_qualified_dtr_violation($violation_late['record']);
				}
			}
			return $inData;
		}
		function dtrOutEntry($type, $acc_time_id,$emp_id,$sched_id,$log,$entry,$in){
			date_default_timezone_set('Asia/Manila');
			$dtr['type'] = $type;
			$dtr['acc_time_id'] = $acc_time_id;
			$dtr['emp_id'] =  $emp_id;
			$dtr['sched_id'] =  $sched_id;
			$dtr['log'] =  $log;
			$dtr['entry'] = $entry;
			$dtr['note'] = $in;
			$dtr['info'] = "TITO";
			$violation_ut = 1;
			$sched = $this->general_model->custom_query("Select sched_id,a.acc_time_id,time_start,time_end,sched_date from tbl_schedule a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and sched_id=" . $sched_id);
			$time2 = ($sched[0]->time_end == "12:00:00 AM") ? "24:00" : date_format(date_create($sched[0]->time_end), 'H:i');
			$date1 = date_format(date_create($log), 'Y-m-d H:i');
			$shiftIN = date_format(date_create($sched[0]->time_start), 'H:i');
			$shiftOut = date_format(date_create($sched[0]->time_end), 'H:i');
			$ampm = explode("|", $this->getTimeGreeting($shiftIN, $shiftOut));
			// var_dump($sched);
			// $ampm = explode("|", $this->getTimeGreeting($sched[0]->time_start, $sched[0]->time_end));
			// var_dump($ampm);
			$date2 = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($sched[0]->sched_date))) : $sched[0]->sched_date;
			$date2 .= " " . $time2;
			$this->general_model->insert_vals_last_inserted_id($dtr, 'tbl_dtr_logs');
			// var_dump($date1);
			// var_dump($date2);
			if (strtotime($date1) < strtotime($date2)) { // Undertime
				$supervisor = $this->get_direct_supervisor_via_emp_id($_SESSION["emp_id"]);
				$uidd = $this->get_emp_details_via_emp_id($emp_id);
				$violation_ut = $this->adddataViolation(2, $supervisor->Supervisor, $sched_id, $sched[0]->sched_date, $uidd->uid);
				if($violation_ut['exist']){
					$this->check_qualified_dtr_violation($violation_ut['record']);
				}
			}
			// echo $date1." ".$date2." ".$violation_ut;
			// die();
			return $violation_ut;
		}

		private function get_next_approver($request_id, $approval_level)
		{
			$fields = "dtrRequestApproval_ID, userId";
			$where = "dtrRequest_ID = $request_id AND approvalLevel = $approval_level";
			return $record = $this->general_model->fetch_specific_val($fields, $where, 'tbl_dtr_request_approval');
		}
		function get_specific_recrd($dtrRequest_ID)
		{
			$fields = "a.*,b.emp_id";
			$where = " a.userID=b.uid and dtrRequest_ID = $dtrRequest_ID";
			$tables = "tbl_dtr_request a,tbl_user b";

			$record = $this->general_model->fetch_specific_val($fields, $where, $tables);
			return $record;
		}
		 private function get_requestor_details($request_id)
			{
				$fields = "a.uid as userId, fname, lname";
				$where = "a.uid = b.userId AND a.emp_id = c.emp_id  AND c.apid = d.apid  AND b.dtrRequest_ID = $request_id";
				$tables = "tbl_user a, tbl_dtr_request b, tbl_employee c,tbl_applicant d";
				return $requestor = $this->general_model->fetch_specific_val($fields,$where, $tables);
			}
 		function checkifLast($dtrRequest_ID){
			$uid = $_SESSION["uid"];
			$fields = "max(approvalLevel) as max";
			$where = "dtrRequest_ID=$dtrRequest_ID";
			return $this->general_model->fetch_specific_val($fields,$where,"tbl_dtr_request_approval");   
		 
		}
		function viewApproverStatus(){
			date_default_timezone_set('Asia/Manila');
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$emp_id = $_SESSION["emp_id"];
			$titoDetailsqry = "SELECT c.emp_id,g.fname,g.lname,b.dateTimeStatus,b.approvalStatus_ID,b.approvalLevel,b.remarks,dateRequest,time_start,time_end,g.pic,h.description,c.uid  FROM tbl_dtr_request a,tbl_dtr_request_approval b,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g,tbl_status h where b.approvalStatus_ID=h.status_ID and b.userID=c.uid and a.dtrRequest_ID=b.dtrRequest_ID and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id  and a.dtrRequest_ID=".$dtrRequest_ID;
			
			$rs = $this->general_model->custom_query($titoDetailsqry);
			foreach($rs as $row){ 
				$position = $this->getCurrentPosition($row->emp_id);
				$data[]= array(
					"uid" => $row->uid,
					"fname" => $row->fname,
					"lname" => $row->lname,
					"dateTimeStatus" => $row->dateTimeStatus,
					"approvalLevel" => $row->approvalLevel,
					"remarks" => $row->remarks,
					"dateRequest" => $row->dateRequest,
					"pic" => $row->pic,
					"description" => $row->description,
					"approvalStatus_ID" => $row->approvalStatus_ID,
					"position" => $position[0]->pos_details,
					"status" => $position[0]->status,
 				
				);				
			}
			
			echo (count($data)>0) ? json_encode($data) : 0;

		}
		
		function viewRecordTito(){
			date_default_timezone_set('Asia/Manila');
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$emp_id = $_SESSION["emp_id"];
			$titoDetailsqry = "SELECT g.fname,g.lname,a.*,message,dateRequest,time_start,time_end,g.pic FROM tbl_dtr_request a,tbl_user c,tbl_acc_time d,tbl_time e,tbl_employee f,tbl_applicant g where a.userID=c.uid and a.acc_time_id=d.acc_time_id and d.time_id=e.time_id and f.apid=g.apid and c.emp_id=f.emp_id and a.dtrRequest_ID=".$dtrRequest_ID;
			
			
			$rs["titoDetailsqry"] = $this->general_model->custom_query($titoDetailsqry);
			$empPosQry = "SELECT pos_details,d.status FROM tbl_emp_promote a,tbl_pos_emp_stat b,tbl_position c,tbl_emp_stat d where a.posempstat_id =b.posempstat_id and b.pos_id=c.pos_id and b.empstat_id=d.empstat_id and a.isActive=1 and emp_id=".$emp_id;
			$rs["titoDetailsqry"] = $this->general_model->custom_query($titoDetailsqry);
			$rs["empPosQry"] = $this->general_model->custom_query($empPosQry);
			echo (count($rs["titoDetailsqry"])>0) ? json_encode($rs) : 0;

			}
		
		function viewModalApprover(){
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$fields = "*";
			$where = "dtrRequest_ID=$dtrRequest_ID";
			$record = $this->general_model->fetch_specific_val($fields,$where,"tbl_dtr_request");   
			echo json_encode($record);
		}
		function checkTito(){
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$uid = $_SESSION["uid"];
			$fields = "a.status_ID as status,b.approvalStatus_ID as approverStatus,dateRequest,a.userID,approvalLevel,count(*) as cnt";
			$where = "a.dtrRequest_ID=b.dtrRequest_ID and b.userId=$uid and a.dtrRequest_ID=$dtrRequest_ID";
			$record = $this->general_model->fetch_specific_val($fields,$where,"tbl_dtr_request a,tbl_dtr_request_approval b");   
			echo json_encode($record);
		}
		function checkTitoHR(){
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$uid = $_SESSION["uid"];
			$fields = "a.status_ID as status,b.approvalStatus_ID as approverStatus,dateRequest,a.userID,approvalLevel,count(*) as cnt";
			$where = "a.dtrRequest_ID=$dtrRequest_ID and a.status_ID=12";
			$record = $this->general_model->fetch_specific_val($fields,$where,"tbl_dtr_request a,tbl_dtr_request_approval b");   
			echo json_encode($record);
		}
		function checkIfRecordisOngoing(){
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$uid = $_SESSION["uid"];
			$fields = "count(*) as cnt";
			$where = "a.dtrRequest_ID=b.dtrRequest_ID and a.userId=$uid and a.dtrRequest_ID=$dtrRequest_ID and b.approvalStatus_ID NOT IN (2,4, 12)";
			$record = $this->general_model->fetch_specific_val($fields,$where,"tbl_dtr_request a,tbl_dtr_request_approval b");   
			echo json_encode($record);
		}
		 private function remove_tito_approvers($dtrRequest_ID){
			$where['dtrRequest_ID'] = $dtrRequest_ID;
			return $delete_stat = $this->general_model->delete_vals($where, 'tbl_dtr_request_approval');  
		}

		private function remove_tito($dtrRequest_ID){
			$where['dtrRequest_ID'] = $dtrRequest_ID;
			return $delete_stat = $this->general_model->delete_vals($where, 'tbl_dtr_request');
		}
		function deletetitorequest(){
			$dtrRequest_ID = $this->input->post("dtrRequest_ID");
			$delete_stat['delete_deadline_stat'] = $this->remove_deadlines(3, $dtrRequest_ID);
            $delete_stat['delete_approvers_stat'] = $this->remove_tito_approvers($dtrRequest_ID);
            $delete_stat['delete_tito_stat'] = $this->remove_tito($dtrRequest_ID);
			echo json_encode($delete_stat);
		}
	private function _popup_survey($sched_id){
        $data['has_taken'] = true; //set as default
		$data['isAnsweredFollowQues'] = null;
		$data['answer'] = 0;
		$data['answerId'] = 0;
        if ($sched_id != null or $sched_id != 0) {

            $survey_answer = $this->general_model->fetch_specific_vals("*", ['employeeId' => $this->session->userdata('uid'), 'sched_id' => $sched_id], 'tbl_szfive_survey_answers', 'answerId');

            if (!empty($survey_answer)) {
                $data['has_taken'] = true;
                $data['isAnsweredFollowQues'] = $survey_answer[0]->isAnsweredFollowQues;
                $data['answer'] = $survey_answer[0]->answer;
                $data['answerId'] = $survey_answer[0]->answerId;

            } else {
                $data['has_taken'] = false;
				$data['answerId'] = 0;
             }
        }

        $query = "SELECT a.*, b.* FROM tbl_szfive_survey_details as a JOIN tbl_szfive_survey_questions as b ON b.questionId = a.questionId WHERE a.isDefault = 1";

        $survey_data = $this->general_model->custom_query($query);
        $survey_data[0]->choiceLabels = json_decode($survey_data[0]->choiceLabels);
        $count = 0;
        foreach ($survey_data[0]->choiceLabels as $choice) {
            $count = $count + 1;
            $cdata = $this->general_model->fetch_specific_vals("*", ['choiceId' => $choice], 'tbl_szfive_survey_choices', 'label');
            $survey_data[0]->clabels[] = ['choiceId' => $cdata[0]->choiceId, 'num' => $count, 'label' => $cdata[0]->label, 'img' => $cdata[0]->img];
		}
		
		$data['survey_data'] = $survey_data;
		return $data;
    }
	function dtr_current_status(){
		$emp_id = $_SESSION["emp_id"];
		$uid = $_SESSION["uid"];
		$type = trim($this->input->post("class_type"));
		$account = $this->input->post("account");
		$emplist = $this->input->post("emp");
  		
/* 		$append = ($type=="ALL") ? "" : "and c.acc_description='$type'";
		$append2 = ($account=="ALL" || !isset($account)) ? "" : "and c.acc_name = '$account'";
		$query="SELECT b.emp_id,d.uid,a.fname,a.lname,c.acc_description,Supervisor as sup,a.pic,acc_name FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_user d where a.apid=b.apid and b.acc_id=c.acc_id and b.emp_id=d.emp_id and Supervisor=$emp_id and b.isActive='yes' ".$append." ".$append2;

        $result = $this->general_model->custom_query($query);
				$query_acc="SELECT b.emp_id,d.uid,a.fname,a.lname,c.acc_description,Supervisor as sup,a.pic,acc_name FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_user d where a.apid=b.apid and b.acc_id=c.acc_id and b.emp_id=d.emp_id and Supervisor=$emp_id and b.isActive='yes'";

				$result_acc = $this->general_model->custom_query($query_acc);
 */				
		$appends = ($type=="ALL") ? "" : "and d.acc_description='$type'";
		$appends2 = ($account=="ALL" || !isset($account)) ? "" : "and d.acc_name = '$account'";

		$subordinate = $this->employeeSubordinate($uid,$appends,$appends2);
		if(!empty($subordinate)){
		foreach($subordinate as $row){
			$dtr = $this->dtrStatus($row->emp_id);
			$position = $this->getCurrentPosition($row->emp_id);
			$selected = (count($emplist)>0) ? ((in_array($row->emp_id,$emplist)) ? "selected" : "") :"";
						if(count($position)>=1){
							$arr["dtr"][$row->lname."-".$row->fname] = array(
								"emp_id" => $row->emp_id,
								"fname" => $row->fname,
								"lname" => $row->lname,
								"sup" => $row->Supervisor,
								"acc_description" => $row->acc_description,
								"account" => $row->acc_name,
								"dtr" => $dtr,
								"pic" => "",
								"position" => $position[0]->pos_details,
								"selected" => $selected,
								"emplist" => $emplist,
							);
						}
			if($row->acc_description==$type || $type=="ALL"){
				$acc_selected = ($account==$row->acc_name) ? "selected" : "";
				$arr["account"][$row->acc_name] = array($acc_selected);
			}
			
		}
	 
 
			echo json_encode($arr);
		}else{
			echo "Empty";
		}
	}
	public function employeeSubordinate($uid,$append,$append2){
		$emp_id = $this->general_model->fetch_specific_val('emp_id', "uid=$uid", 'tbl_user')->emp_id;
        $query = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,d.acc_name,e.uid FROM tbl_applicant a,tbl_employee b,tbl_account d ,tbl_user e WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND e.emp_id=b.emp_id AND b.Supervisor IN ($emp_id)";
        $data = $this->general_model->custom_query($query);
        $results = $data;
        $stop = false;
        if (!empty($data))
        {
            while ($stop === false)
            {
                foreach ($data as $d)
                {
                    $emps[] = $d->emp_id;
                }
                $query2 = "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,d.acc_name,e.uid FROM tbl_applicant a,tbl_employee b,tbl_account d ,tbl_user e WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND e.emp_id=b.emp_id AND b.Supervisor IN (" . implode(',', $emps) . ")";
                
                $data = $this->general_model->custom_query($query2);
                if (!empty($data))
                {
                    $results = array_merge($results, $data);
                    unset($emps);
                }
                else
                {
                    $stop = true;
                }
                
            }
        }
        usort($results, array($this, "cmp"));
        return $results;
    }
	public function employeeSubordinate2($uid){
		$query ="SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,c.uid,acc_name,a.pic FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.Supervisor=c.emp_id AND b.isActive='yes' AND c.uid=" .$uid;
		$level1 = $this->general_model->custom_query($query);

        if (empty($level1)) {
           // echo 'Empty';
        } else {
            $results = array();
            $results = array_merge($results, $level1);
            foreach ($level1 as $l) {
                $emps[] = $l->emp_id;
            }
				$query2= "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description,a.pic FROM tbl_applicant a,tbl_employee b,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND b.Supervisor IN (" . implode(',', $emps) . ") ";
				$data = $this->general_model->custom_query($query2);

            // $data = $this->ScheduleModel->getSupervisoryEmpNext($emps);
            $results = array_merge($results, $data);
            $data2 = $data;
		
            if (!empty($data)) {
                jumper:
                foreach ($data2 as $d2) {
                    $emps2[] = $d2->emp_id;
                }
				$query3= "SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND b.Supervisor IN (" . implode(',', $emps2) . ")";

				$data2 = $this->general_model->custom_query($query3);

                // $data2 = $this->ScheduleModel->getSupervisoryEmpNext($emps2);
                if (empty($data2)) {
                } else {
                    $results = array_merge($results, $data2);
                    unset($emps2);
                    goto jumper;
                }
            } else {
            }
            usort($results, array($this, "cmp"));
            $results = array_reverse($results);
            // return json_encode($results);
            echo json_encode($results);
        }
    }
	function cmp($a, $b) {
        return strcmp($a->lname, $b->lname);
    }
	private function dtrStatus($emp_id){
		$query = "SELECT * FROM tbl_dtr_logs where emp_id=$emp_id and info is null order by log desc limit 1";
        $result = $this->general_model->custom_query($query);
		$rs = array();
		if(count($result)>0){
		if($result[0]->type=="DTR"){
			if($result[0]->entry=="I"){
				$rs["status"] = "Online";
				$rs["time"]  = $result[0]->log;
			}else{
				$rs["status"] = "Offline";
				$rs["time"]  = $result[0]->log;
			}
		}else{
			$query_break = "SELECT break_type FROM tbl_shift_break a,tbl_break_time_account b,tbl_break c where a.bta_id=b.bta_id and b.break_id=c.break_id and sbrk_id= ".$result[0]->acc_time_id;
			$result_break = $this->general_model->custom_query($query_break);

			$rs["status"] = $result_break[0]->break_type;
			$rs["time"]  = $result[0]->log;
		}
		}else{
			$rs["status"] = "Offline";
			$rs["time"]  = "00:00:00";

		}
		return $rs;
	}
	public function graphreport(){
			ini_set('max_execution_time', 300);
			date_default_timezone_set('Asia/Manila');

 			$dateReq = $this->input->post("date");
			$employeeListRecord = !empty($this->input->post("emp")) ? $this->input->post("emp") : array(0);
			$date = explode("-",$dateReq);

		
			$class = $this->input->post("clazz");
			$account = $this->input->post("account");
		    
			
			if($class=="ALL"){
					$query = "SELECT  b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name FROM tbl_applicant a, tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='yes' order by lname";
			}else{
				if($account!="ALL"){
					$append = (empty($account) or $account == null) ? " and c.acc_description='".$class."'  order by lname" : " and c.acc_id=".$account."  order by lname";
					$query = "SELECT  b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name FROM tbl_applicant a, tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='yes'".$append;
				}else{
					$query ="select b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='yes' and b.emp_id in (".implode(",",$employeeListRecord).")  order by lname";
				}
			}
			
			
			$result = $this->general_model->custom_query($query);
			$arr = array();
			foreach($result as $row){
				$shift = $this->empShiftrange($row->emp_id,$date);
					foreach($shift as $row2 => $sched){
						
						if($sched->type=="Normal" || $sched->type=="WORD" || $sched->type=="Double"){
							$in = $this->empshiftin($row->emp_id,$sched->sched_id);
							$out = $this->empshiftout($row->emp_id,$sched->sched_id);
							$loginID = (count($in)>0) ? $in[0]->dtr_id : 0;
							$logoutID = (count($out)>0) ? $out[0]->dtr_id : ($loginID*1.01);
							$break = $this->getbreak_sbrk($row->emp_id,$loginID,$logoutID);
							  $timeschedule = $this->timeschedule($sched->sched_id);
							  $logs= (count($in)>0) ? $this->totalHours_dtr($sched->sched_id,$row->emp_id,"custom") :0;
							  $statusLog = (count($in)>0) ? "<b style='color:green'>Present</b>" : "<b style='color:red'>Absent</b>";
							  
							$getTotalHours = $this->getTotalShiftHours(date("H:i",strtotime($timeschedule[0]->time_start)),date("H:i",strtotime($timeschedule[0]->time_end)) ,$sched->sched_Date);
							
						}else{
							  $timeschedule="--";
							 $logs= 0;
							 $statusLog =  "<span style='color:".$sched->style."'>".$sched->type."</span>";
							 
								$in = array();
								$out = array();
								$loginID =   0;
								$logoutID = 0;
								$getTotalHours = 0;
								$break = array();

						}
						$arr[$row->acc_name][$row->emp_id."_".$row->lname."_".$row->fname][$sched->sched_Date] = array(
							"id_num" => $row->id_num,
							"descr" => $row->descr,
							"fname" => $row->fname,
							"lname" => $row->lname,
							"sched" => $sched->sched_id,
							"type" => $sched->type,
							"style" => $sched->style,
							"actual_in" => (count($in) >0) ? $in : 0,
							"actual_out" => (count($out) >0) ? $out : 0,
							// "break" =>  $brk,
							"breakz" =>  $break,
							// "newLogin" =>  $newLogin,
							// "newLogout" =>  $newLogout,
							// "totalBreak" =>  $totalBreak,
							 "logs" =>  $logs,
							  "timeschedule" =>  $timeschedule,
							"statusLog" =>  $statusLog,
							"leaveRemarks" =>  $sched->remarks,
							"getTotalHours" =>  $getTotalHours,
						);
						
					}
			}
				echo (count($arr)>0) ? json_encode($arr) : 0;
	}
	
	function getTotalShiftHours($shiftin,$shiftout,$date){
		 if($shiftin=="23:00"){
				$shiftINz = date("H:i",strtotime("-1 second", strtotime($shiftin)));
				$ampm = explode("|", $this->getTimeGreeting($shiftINz, $shiftout));
				$deduct = .0166666666667;
			}else{
				$shiftINz = date_format(date_create($shiftin), 'H:i');
				$ampm = explode("|", $this->getTimeGreeting($shiftINz, $shiftout));
				$deduct = 0;
			}							
		$dateOut = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($date))) : $date;

        $logoutNew = $dateOut . " " . $shiftout;
        $diff_shift = strtotime($logoutNew) - strtotime($date . " " . $shiftINz);
        $totalShift_wo_break = (($diff_shift / 60) / 60)-$deduct;

		return $totalShift_wo_break;
	}
	public function dtr_view_logs($report=null,$isActib=null){
		ini_set('max_execution_time', 300);
 			$dateReq = $this->input->post("date");
			$employeeListRecord = !empty($this->input->post("emp")) ? $this->input->post("emp") : array(0);
			$date = explode("-",$dateReq);
			$isActive =  ($isActib!=null) ? $isActib : "yes";
			if($report!=null){
			$class = $this->input->post("clazz");
			$account = $this->input->post("account");
				if($class=="ALL"){
						$query = "SELECT  b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name FROM tbl_applicant a, tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='".$isActive."' order by lname";
				}else{
					if($account!="ALL"){
						$append = (empty($account) or $account == null) ? " and c.acc_description='".$class."'  order by lname" : " and c.acc_id=".$account."  order by lname";
						$query = "SELECT  b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name FROM tbl_applicant a, tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='".$isActive."' ".$append;
					}else{
						$query ="select b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name from tbl_applicant a,tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='".$isActive."' and b.emp_id in (".implode(",",$employeeListRecord).")  order by lname";
					}
				}
			}else{
				$query ="select b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name from tbl_applicant a,tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='".$isActive."' and b.emp_id in (".implode(",",$employeeListRecord).")  order by lname";
			}
			// echo $query;
			// exit();
			$result = $this->general_model->custom_query($query);
			
			$arr = array();
			foreach($result as $row){
				$DateRangeShift = $this->dateRangeShift($date);
				foreach($DateRangeShift as $row1 => $data){
					$shift = $this->empShift($row->emp_id,$data);
					foreach($shift as $row2 => $sched){
					if($sched->type=="Normal" || $sched->type=="WORD" || $sched->type=="Double"){
							$in = $this->empshiftin($row->emp_id,$sched->sched_id);
							$out = $this->empshiftout($row->emp_id,$sched->sched_id);
							$loginID = (count($in)>0) ? $in[0]->dtr_id : 0;
							$logoutID = (count($out)>0) ? $out[0]->dtr_id : ($loginID*2);
							$break = $this->getbreak_sbrk($row->emp_id,$loginID,$logoutID);

						$timeschedule = $this->timeschedule($sched->sched_id);
						$timeschedule = (count($timeschedule)>0) ? $this->timeschedule($sched->sched_id) : "--";
						  $logs= (count($in)>0) ? $this->totalHours_dtr($sched->sched_id,$row->emp_id,"custom") :0;
						  $statusLog = (count($in)>0) ? "<b style='color:green'>Present</b>" : "<b style='color:red'>Absent</b>";
						  

					}else{
						$timeschedule="--";
						 $logs= 0;
						 $statusLog =  "<span style='color:".$sched->style."'>".$sched->type."</span>";
						 
							$in = array();
							$out = array();
							$loginID =   0;
							$logoutID = 0;
							$break = array();

					}
					$brk = array();
 
						
						$arr[$row->emp_id."_".$row->lname."_".$row->fname][$data][$sched->sched_id] = array(
							"id_num" => $row->id_num,
							"descr" => $row->descr,
							"acc_name" => $row->acc_name,
							"fname" => $row->fname,
							"lname" => $row->lname,
							"sched" => $sched->sched_id,
							"isLeaveName" => ($sched->leavetype!=null) ? " - ".$sched->leavetype : "",
							"type" => $sched->type,
							"style" => $sched->style,
							"actual_in" => (count($in) >0) ? $in : 0,
							"actual_out" => (count($out) >0) ? $out : 0,
							// "break" =>  $brk,
							"breakz" =>  $break,
							// "newLogin" =>  $newLogin,
							// "newLogout" =>  $newLogout,
							// "totalBreak" =>  $totalBreak,
							 "logs" =>  $logs,
							"timeschedule" =>  $timeschedule,
							"statusLog" =>  $statusLog,
  						);
				
					}
				}
			}
				echo (count($arr)>0) ? json_encode($arr) : 0;
			
	}
	
	#get exact time in and out
	
	 public function totalHours_dtr($id,$emp_id, $custom = null)
    {
		// echo $id."-".$emp_id."\n";
    	$time = $this->empshifttime_fetch2($id,$emp_id);

    	$inlog = $this->empshiftin_fetch2($id,$emp_id);
    	$outlog = $this->empshiftout_fetch2($id,$emp_id);
		// echo $emp_id." ".$time[0]->time_end." <br>";
    	// $shiftIN = date_format(date_create($time[0]->time_start), 'H:i');
    	        $shiftOUT = date_format(date_create($time[0]->time_end), 'H:i');
					if($time[0]->time_start=="12:00:00 AM"){
						$shiftIN = date("H:i",strtotime("-1 second", strtotime($time[0]->time_start)));
						$ampm = explode("|", $this->getTimeGreeting($shiftIN, $shiftOUT));
						// $schedDate = date("Y-m-d",strtotime("-1 day", strtotime($time[0]->sched_date)));
 						$deduct = .0166666666667;
  					}else{
						$shiftIN = date_format(date_create($time[0]->time_start), 'H:i');
						$ampm = explode("|", $this->getTimeGreeting($shiftIN, $shiftOUT));
						// $schedDate = $time[0]->sched_date;
						$deduct = 0;
					}
		$schedDate = $time[0]->sched_date;
        // $dateOut = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($schedDate))) : $schedDate;
		$dateOut = ($ampm[1] == "Tomorrow") ? date('Y-m-d', strtotime("+1 day", strtotime($time[0]->sched_date))) : $time[0]->sched_date;

        $logoutNew = $dateOut . " " . $shiftOUT;

        $break = $this->empshiftgetrbreak_fetch($time[0]->acc_time_id);
        $totalBreak = $break[0]->hr + ($break[0]->min / 60);
		$totalBreak = ($totalBreak >=1) ? 1 : $totalBreak;

        if ((count($inlog) > 0 && count($outlog) > 0) )
        {
			if(($inlog[0]->login!="0000-00-00 00:00:00" || $outlog[0]->logout!="0000-00-00 00:00:00")){
				// $in = $schedDate . " " . date_format(date_create($inlog[0]->login), 'H:i');
				$in = $inlog[0]->login;
				$out = $outlog[0]->logout;
				$logout = (strtotime($out) >= strtotime($logoutNew)) ? $logoutNew : $out;

				$loginNew = (strtotime($in) <= strtotime($schedDate . " " . $shiftIN)) ? $schedDate . " " . $shiftIN : $in;

				$diff = strtotime($logout) - strtotime($loginNew);
				$diff_d = $logout." - ".$loginNew;
				$totalWorked = (($diff / 60) / 60) - $totalBreak;
				$totalWorkedwoBreak = (($diff / 60) / 60);
			}else{
				$diff = 0;
				$diff_d = 0;
				$totalWorked = 0;
				$totalWorkedwoBreak = 0;
				$totalShift = 0;
				$loginNew =$schedDate . " " . $shiftIN;
			}
        }
        else
        {
            $diff = 0;
            $diff_d = 0;
            $totalWorked = 0;
            $totalWorkedwoBreak = 0;
            $totalShift = 0;
			$loginNew =$schedDate . " " . $shiftIN;
        }
        $diff_shift = strtotime($logoutNew) - strtotime($schedDate . " " . $shiftIN);
        $totalShift = (($diff_shift / 60) / 60) - $totalBreak;
        $totalShift_wo_break = (($diff_shift / 60) / 60);
 		// $arr["in"] = $in;
 		// $arr["try"] = $in ." <=". $schedDate . " " . $shiftIN;
        $arr["loginNew"] = $loginNew;
        $arr["shiftIN"] = $shiftIN;
        $arr["logoutNew"] = $logoutNew; 
        $arr["totalShift"] = $totalShift; 
        $arr["totalWorked"] = $totalWorked; 
        
		$arr["total_actual_worked"] = $totalWorked-$deduct;
		$arr["total_actual_worked_wo_break"] = $totalWorkedwoBreak-$deduct;
        $arr["total_shift_hours"] = $totalShift-$deduct;
        $arr["total_shift_hours_wo_break"] = $totalShift_wo_break-$deduct;
        $arr["remaining_hour"] = abs($totalShift - $totalWorked);
          $arr["shiftsss"] = $dateOut;
           $arr["totalBreak"] = $totalBreak;
          $arr["logout"] = $logoutNew ;
          $arr["logins"] =   $schedDate . " " . $shiftIN;
          $arr["login"] =   $loginNew;
          if ($custom == null)
          {
          	echo json_encode($arr);
          }
          else
          {
          	return $arr;
          }
      }

      public function empshifttime_fetch2($sched_id,$emp_id)
      {
      	$query = $this->db->query("SELECT time_start,time_end,sched_date,a.acc_time_id FROM tbl_schedule a,tbl_acc_time b,tbl_time c where a.acc_time_id=b.acc_time_id and b.time_id=c.time_id and  a.emp_id=".$emp_id." and sched_id=$sched_id");
      	return $query->result();
      }

      public function empshiftgetrbreak_fetch2($acc_time_id)
      {
      	$query = $this->db->query("SELECT sum(hour) hr,sum(min) min FROM tbl_shift_break a,tbl_break_time_account b,tbl_break_time c where a.bta_id=b.bta_id and b.btime_id=c.btime_id and acc_time_id=$acc_time_id");
      	return $query->result();
      }

      public function empshiftout_fetch2($shift,$emp_id)
      {
      	$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=" . $shift . " and emp_id=".$emp_id."  limit 1");
      	return $query->result();
      }

      public function empshiftin_fetch2($shift,$emp_id)
      {
      	$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=" . $shift . " and emp_id=".$emp_id." limit 1");
      	return $query->result();
      }
	#get exact time in and out
	
	
		public function employee_list(){
			$date = $this->input->post("daterange");
			$class = $this->input->post("clazz");
			$account = $this->input->post("account");
			$isActive = $this->input->post("isActive");
			$append = ($class!="ALL") ? (($account=="") ? "and c.acc_description='".$class."'" : " and c.acc_id=".$account) : "";
			$qry= "SELECT emp_id FROM tbl_applicant a, tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='".$isActive."' ".$append;
			// echo $qry;
			// exit();
			$query = $this->db->query($qry);
			$rs = $query->result();
			$arr = array();
			foreach($rs as $row){
				array_push($arr,$row->emp_id);
			}
			
			  echo json_encode($arr);
		 }
		public function empShift($empid,$date){
			// $query = $this->db->query("SELECT sched_id,type,style FROM  tbl_schedule  a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and a.isActive=1 and sched_date = '".$date."' and emp_id=".$empid);
			$query = $this->db->query("SELECT sched_id,sched_date,type,style,leavetype FROM  tbl_schedule_type b,tbl_schedule  a left join tbl_leave_type c on c.leaveType_ID=a.leavenote where a.schedtype_id = b.schedtype_id and a.isActive=1 and sched_date =  '".$date."' and emp_id=".$empid);
			  return $query->result();
		 }
		 public function timeschedule($timeschedule){
			$query = $this->db->query("SELECT time_start,time_end,note FROM `tbl_schedule` a,tbl_acc_time b,tbl_time c  where a.acc_time_id = b.acc_time_id and b.time_id = c.time_id and sched_id=".$timeschedule);
			  return $query->result();
		 }
		 public function empshiftin($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as login,acc_time_id from tbl_dtr_logs where entry='I' and type='DTR' and sched_id=".$shift." and emp_id=".$empid." limit 1");
			return $query->result();
 		}
		
		public function empshiftout($empid,$shift){
 			$query = $this->db->query("select dtr_id,emp_id,log as logout,acc_time_id from tbl_dtr_logs where entry='O' and type='DTR' and sched_id=".$shift." and emp_id=".$empid." limit 1");
			  return $query->result();
 		}
		public function dateRangeShift($date){
		$strDateFrom = date("Y-m-d", strtotime($date[0]));
		$strDateTo = date("Y-m-d", strtotime($date[1]));
		$aryRange=array();
		$iDateFrom = mktime(0,0,1,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo = mktime(0,0,1,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
			if($iDateTo>=$iDateFrom){
				array_push($aryRange,date('Y-m-d',$iDateFrom));
				 while($iDateFrom<$iDateTo){
					$iDateFrom+=86400; // add 24 hours
						array_push($aryRange,date('Y-m-d',$iDateFrom));
					}
			}
			 return $aryRange;
			 		
		}
  public function export_dtr_log($date1,$date2, $class = NULL, $account = NULL, $isActive = NULL){
				ini_set('max_execution_time',5000);

			$date = array($date1,$date2);

        $this->load->library('PHPExcel', NULL, 'excel');

        //load our new PHPExcel library
        // $this->load->library('excel');
		//activate worksheet number 1
        // $this->excel->setActiveSheetIndex(0);
		//name the worksheet+
		$objPHPExcel = $this->excel;
		$objPHPExcel->setActiveSheetIndex(0)->setTitle("Attendance Logs");
		$sheet = $objPHPExcel->getActiveSheet(0); 


		//-------------------------------------------------------------------
        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath($this->logo);
        $objDrawing->setOffsetX(0);    // setOffsetX works properly
        $objDrawing->setOffsetY(0);  //setOffsetY has no effect
        $objDrawing->setCoordinates('A1');
        // $objDrawing->setHeight(95); // logo height
        // $objDrawing->setWidth(320); // logo width
       $objDrawing->setWidthAndHeight(170,200);
        $objDrawing->setResizeProportional(true);
        $objDrawing->setWorksheet($this->excel->getActiveSheet());
		//----------------------------------------------------------------------
			
				
				
			if($class=="ALL"){
					$query = "SELECT  b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name,supervisor FROM tbl_applicant a, tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='".$isActive."' order by lname";
			}else{
				if($account!="ALL"){
					$append = (empty($account) or $account == null or $account == "null") ? " and c.acc_description='".$class."'  order by lname" : " and c.acc_id=".$account."  order by lname";
					$query = "SELECT  b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name,supervisor FROM tbl_applicant a, tbl_employee b,tbl_account c where a.apid=b.apid and b.acc_id=c.acc_id and b.isActive='".$isActive."' ".$append;
				}else{
					$query ="select b.apid,b.id_num,fname,lname,emp_id,acc_description as descr,acc_name,supervisor from tbl_applicant a,tbl_employee b where a.apid=b.apid and b.isActive='".$isActive."' and b.emp_id in (".implode(",",$employeeListRecord).")  order by lname";
				}
			}
			
			$result = $this->general_model->custom_query($query);
			$j=0;
				$sheet->setCellValueByColumnAndRow(3,1, "Coverage");
				$sheet->setCellValueByColumnAndRow(4,1, $date1." - ".$date2);
				
				$s=4;
				$sheet->setCellValueByColumnAndRow($j++,$s, "ID NUMBER");
				$sheet->setCellValueByColumnAndRow($j++,$s, "LASTNAME");
				$sheet->setCellValueByColumnAndRow($j++,$s, "FIRSTNAME");
				$sheet->setCellValueByColumnAndRow($j++,$s, "SUPERVISOR");
				$sheet->setCellValueByColumnAndRow($j++,$s, "TYPE");
				$sheet->setCellValueByColumnAndRow($j++,$s, "ACCOUNT");
				$sheet->setCellValueByColumnAndRow($j++,$s, "DATE");
				$sheet->setCellValueByColumnAndRow($j++,$s, "TYPE");
				$sheet->setCellValueByColumnAndRow($j++,$s, "SHIFT START");
				$sheet->setCellValueByColumnAndRow($j++,$s, "SHIFT END");
				$sheet->setCellValueByColumnAndRow($j++,$s, "REMARKS");
				$sheet->setCellValueByColumnAndRow($j++,$s, "ACTUAL IN");
				$sheet->setCellValueByColumnAndRow($j++,$s, "ACTUAL OUT");
				$sheet->setCellValueByColumnAndRow($j++,$s, "FIRST BREAK-OUT");
				$sheet->setCellValueByColumnAndRow($j++,$s, "FIRST BREAK-IN");
				$sheet->setCellValueByColumnAndRow($j++,$s, "LUNCH BREAK-OUT");
				$sheet->setCellValueByColumnAndRow($j++,$s, "LUNCH BREAK-IN");
				$sheet->setCellValueByColumnAndRow($j++,$s, "LAST BREAK-OUT");
				$sheet->setCellValueByColumnAndRow($j++,$s, "LAST BREAK-IN");
				$sheet->setCellValueByColumnAndRow($j++,$s, "TOTAL CONSUMED BREAK");
				$sheet->setCellValueByColumnAndRow($j++,$s, "TOTAL WORK HOURS");
				
			$s=5;

			foreach($result as $row){
				
				$DateRangeShift = $this->dateRangeShift($date);
				$supervisor = ($row->supervisor!=null) ? $this->get_supervisor($row->supervisor) : array(array("fname"=>"no","lname"=>" supervisor"));
				foreach($DateRangeShift as $row1 => $data){
					$shift = $this->empShift($row->emp_id,$data);
					foreach($shift as $row2 => $sched){
						$j=0;
					if($sched->type=="Normal" || $sched->type=="WORD" || $sched->type=="Double"){
							$in = $this->empshiftin($row->emp_id,$sched->sched_id);
							$out = $this->empshiftout($row->emp_id,$sched->sched_id);
							$loginID = (count($in)>0) ? $in[0]->dtr_id : 0;
							$logoutID = (count($out)>0) ? $out[0]->dtr_id : ($loginID*1.01);
							$break = $this->getbreak_sbrk($row->emp_id,$loginID,$logoutID);

						$timeschedule = $this->timeschedule($sched->sched_id);
						  $logs= (count($in)>0) ? $this->totalHours_dtr($sched->sched_id,$row->emp_id,"custom") :0;
						  $statusLog = (count($in)>0) ? "Present" : "Absent";
						  // echo $row->fname." ".$sched->sched_date." ".$timeschedule[0]->time_start."<br>";
						  $shiftStart = (count($timeschedule)>0) ? $timeschedule[0]->time_start: "--";
						  $shiftEnd = (count($timeschedule)>0) ? $timeschedule[0]->time_end: "--" ;
							$ain = (count($in)>0) ? $in[0]->login : "No Logs";
							$aout = (count($out)>0) ? $out[0]->logout : "No Logs";
						$newLogin  = (count($logs)>0) ? $logs["login"] : date("Y-m-d H:i:s");
						$newLogout  = (count($logs)>0) ?  $logs["logout"] : date("Y-m-d H:i:s");
						$totalShiftBreak = (count($logs)>0) ?  $logs["totalBreak"] : 0;

					}else{
						$timeschedule="--";
						 $logs= 0;
						 $statusLog =  $sched->type;
						  $shiftStart = "N/A";
						  $shiftEnd = "N/A";
							
							$ain = "No Logs";
							$aout = "No Logs";
							$loginID =   0;
							$logoutID = 0;
							$newLogin =  date("Y-m-d H:i:s");
							$newLogout =  date("Y-m-d H:i:s");
							$totalShiftBreak = 0;
							$break = array();

					}
					$brk = array();
					$fbko="No Logs";
					$fbki="No Logs";

					$lbko="No Logs";
					$lbki="No Logs";
					
					$labko="No Logs";
					$labki="No Logs";
					foreach($break as $row_brk){ 
					
					if($row_brk->break_type=="FIRST BREAK"){
						if($row_brk->entry=="O" && $fbko=="No Logs"){
							$fbko =  $row_brk->log ;
						}
						if($row_brk->entry=="I" && $fbki=="No Logs"){
							$fbki =  $row_brk->log ;
						}
						
					}
					if($row_brk->break_type=="LUNCH"){
						if($row_brk->entry=="O" && $lbko=="No Logs"){
							$lbko =  $row_brk->log ;
						}
						if($row_brk->entry=="I" && $lbki=="No Logs"){
							$lbki =  $row_brk->log ;
						}
						
					}
					if($row_brk->break_type=="LAST BREAK"){
						if($row_brk->entry=="O" && $labko=="No Logs"){
							$labko =  $row_brk->log ;
						}
						if($row_brk->entry=="I" && $labki=="No Logs"){
							$labki =  $row_brk->log ;
						}
						
					}
								// echo $row->fname." ".$row_brk->break_type." ******* ".$row_brk->entry."  *******  ".$lbko."  *******  ".$lbki."<br>";
					}

					$tfb = ($fbko!="No Logs" && $fbki!="No Logs") ? $this->totalmins($fbko,$fbki) : 0; 
					$tlub = ($lbko!="No Logs" && $lbki!="No Logs") ? $this->totalmins($lbko,$lbki) : 0; 
					$tlb = ($labko!="No Logs" && $labki!="No Logs") ? $this->totalmins($labko,$labki) : 0; 
					$leavetype = ($sched->leavetype!=null) ? " - ".$sched->leavetype : "";
					$totalBreak = $tfb+$tlub+$tlb;
					$breakDeductToWork = ($totalBreak >=60) ? ($totalBreak/60): $totalShiftBreak;
					$sup_name = ($row->supervisor!=null || $row->supervisor!="") ? strtoupper($supervisor[0]->fname." ".$supervisor[0]->lname) : "not set";

					$totalWork = ($this->totalmins($newLogin,$newLogout)/60)- $breakDeductToWork;
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->id_num));
						$sheet->setCellValueByColumnAndRow($j++,$s, strtoupper($row->lname));
						$sheet->setCellValueByColumnAndRow($j++,$s,strtoupper($row->fname)); 
						$sheet->setCellValueByColumnAndRow($j++,$s, $sup_name);
						$sheet->setCellValueByColumnAndRow($j++,$s,strtoupper($row->descr));
						$sheet->setCellValueByColumnAndRow($j++,$s,strtoupper($row->acc_name));
						$sheet->setCellValueByColumnAndRow($j++,$s,date_format(date_create($data),"F d, Y"));
						$sheet->setCellValueByColumnAndRow($j++,$s,$sched->type.$leavetype);
						$sheet->setCellValueByColumnAndRow($j++,$s,$shiftStart);
						$sheet->setCellValueByColumnAndRow($j++,$s,$shiftEnd);
						$sheet->setCellValueByColumnAndRow($j++,$s,$statusLog);
						$sheet->setCellValueByColumnAndRow($j++,$s,$ain);
						$sheet->setCellValueByColumnAndRow($j++,$s,$aout);
						$sheet->setCellValueByColumnAndRow($j++,$s,$fbko);
						$sheet->setCellValueByColumnAndRow($j++,$s,$fbki);
						$sheet->setCellValueByColumnAndRow($j++,$s,$lbko);
						$sheet->setCellValueByColumnAndRow($j++,$s,$lbki);
						$sheet->setCellValueByColumnAndRow($j++,$s,$labko);
						$sheet->setCellValueByColumnAndRow($j++,$s,$labki);
						$sheet->setCellValueByColumnAndRow($j++,$s,$totalBreak." min.");
						$sheet->setCellValueByColumnAndRow($j++,$s,number_format($totalWork,2)." hr.");
					$s++;
						
				
					}
				}
				
					
			}
 			$letter= $this->columnLetter($s);
				foreach(range('A','Z') as $columnID) {
 				$sheet->getColumnDimension($columnID)->setAutoSize(true);
			}
				$sheet->setShowGridlines(false);
				$sheet->getStyle("A4:T4")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('2d2d2d');
				$sheet->getStyle('A4:T4')->applyFromArray(array('font' => array('color' => array('rgb' => 'FFFFFF'))));
        $filename = 'DTR_Report_'.$date1.'-'.$date2.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
		
    }
		public function get_supervisor($empid){
			$query = $this->db->query("SELECT fname,lname FROM tbl_applicant a,tbl_employee b where a.apid=b.apid and emp_id=".$empid);
			  return $query->result();
		}
		public function empShiftrange($empid,$date){
			$date1 = date_format(date_create($date[0]),"Y-m-d");
			$date2 = date_format(date_create($date[1]),"Y-m-d");
			$query = $this->db->query("SELECT sched_id,type,style,sched_Date,a.remarks FROM  tbl_schedule  a,tbl_schedule_type b where a.schedtype_id = b.schedtype_id and a.isActive=1 and sched_date >= date('".$date1."') and sched_date <= date('".$date2."') and emp_id=".$empid);
			  return $query->result();
		 }
		function totalmins($d1,$d2){
			$to_time = strtotime($d2);
			$from_time = strtotime($d1);
			return round(abs($to_time - $from_time) / 60,2);
			
		}
		function columnLetter($c){

			$c = intval($c);
			if ($c <= 0) return '';

			$letter = '';

			while($c != 0){
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
			}

			return $letter;

		}
}