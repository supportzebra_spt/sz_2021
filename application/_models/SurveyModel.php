<?php

class SurveyModel extends CI_Model {

  public function __construct()
  {
                // Call the CI_Model constructor
    parent::__construct();
  }
  public function getSequenceQuestion($sequence){
    $query = $this->db->query("SELECT question FROM tbl_survey a,tbl_survey_ans b WHERE a.survey_id=b.survey_id AND a.isActive=1 AND b.isActive=1 AND sequence=$sequence");
    return $query->row();
  }
  public function getHeaderQuestion(){
   $query = $this->db->query("SELECT headerquestion FROM tbl_survey WHERE isActive=1");
   return $query->row();
 }
 public function getAllSurvey(){
   $query = $this->db->query("SELECT * FROM tbl_survey");
   return $query->result();
 }
 public function createnewsurvey($headerquestion,$uid,$date){
  $data = array('headerquestion'=>$headerquestion,'addedBy'=>$uid,'addedOn'=>$date);
  $this->db->insert('tbl_survey',$data);
  return $this->db->insert_id();
}
public function createnewsurveyans($question,$uid,$date,$survey_id,$sequence){
  $data = array('sequence'=>$sequence,'question'=>$question,'survey_id'=>$survey_id,'isActive'=>1,'addedBy'=>$uid,'addedOn'=>$date);
  $this->db->insert('tbl_survey_ans',$data);
}
public function setActiveSurvey($survey_id){
  $this->db->where('survey_id',$survey_id);
  $this->db->update('tbl_survey',array('isActive'=>1));
  return $this->db->affected_rows();
}
public function deactivateAllSurvey(){
  $this->db->update('tbl_survey',array('isActive'=>0));  
  return $this->db->affected_rows();
}
// public function getCampNotifLatestPerAccount(){
//   $query = $this->db->query("SELECT *,DATE_FORMAT(due_date,'%M %d, %Y') as format_due_date FROM tbl_campaign_notifications a,tbl_account b WHERE a.acc_id=b.acc_id AND isActive=1 AND b.acc_description='Agent' ORDER BY b.acc_name ASC");
//   return $query->result();
// }
public function getAgentCampaigns(){
  $query = $this->db->query("SELECT * FROM tbl_account WHERE acc_description='Agent' ORDER BY acc_name");
  return $query->result();
}
public function createnewcampnotif($title,$message,$acc_id){
  $this->db->insert('tbl_campaign_notifications',array('acc_id'=>$acc_id,'message'=>trim($message),'title'=>trim($title),'date_posted'=>date("Y-m-d H:i:s")));
  return $this->db->affected_rows();
}
public function getUserAcc_id($uid){
 $query = $this->db->query("SELECT acc_id FROM tbl_user a,tbl_employee b WHERE uid='$uid' AND a.emp_id=b.emp_id");
 return $query->row();
}
public function getCampNotif($acc_id){
  $query = $this->db->query("SELECT a.*,b.acc_name FROM tbl_campaign_notifications a,tbl_account b WHERE a.acc_id=$acc_id AND a.acc_id=b.acc_id and a.isActive=1 ORDER BY cn_id DESC LIMIT 1");
  return $query->row();
}
public function getNotificationPerCampaign($acc_id){
  $this->db->where(array('acc_id'=>$acc_id,'isActive'=>1));
  $this->db->from('tbl_campaign_notifications');
  return $this->db->get()->row();
}
public function getAccNotificationList($acc_id){
  $this->db->where('acc_id',$acc_id);
  $this->db->order_by('title','ASC');
  $this->db->from('tbl_campaign_notifications');
  return $this->db->get()->result();
}
public function updatecampnotifActive($value,$cn_id){
  $this->db->where('cn_id',$cn_id);
  $this->db->update('tbl_campaign_notifications',array('isActive'=>$value));
  return $this->db->affected_rows();
}

public function deactivateAllNotifyAcc($acc_id,$cn_id){
  $this->db->where('acc_id',$acc_id);
  $this->db->where_not_in('cn_id', $cn_id);
  $this->db->update('tbl_campaign_notifications',array('isActive'=>0));
  return $this->db->affected_rows();
}
public function getNotifdetails($cn_id){
  $this->db->where('cn_id',$cn_id);
  $this->db->from('tbl_campaign_notifications');
  return $this->db->get()->row();
}
public function updatenotifdetails($cn_id,$title,$message){
  $this->db->where('cn_id',$cn_id);
  $this->db->update('tbl_campaign_notifications',array('title'=>$title,'message'=>$message));
  return $this->db->affected_rows();
}
}