<?php

class RequestModel extends CI_Model {

  public function __construct()
  {
                // Call the CI_Model constructor
    parent::__construct();
  }
  public function getLeaveList(){
    $this->db->from('tbl_leave');
    $this->db->where('isActive',1);
    $this->db->order_by("leave_name", "asc");
    return $this->db->get()->result();
  }
  public function checkleavedaterange($fromDate,$toDate,$uid){
    $query = $this->db->query("SELECT DISTINCT(d.type) FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_schedule_type d,tbl_schedule f,tbl_user g WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND b.emp_id=f.emp_id AND date(f.sched_date) between date('".$fromDate."') and date('".$toDate."') AND g.emp_id=b.emp_id AND d.schedtype_id=f.schedtype_id AND b.isActive='yes' AND g.uid=".$uid." ORDER BY a.lname");
    return $query->result();
  }
  public function checkdateexistrequest($date,$uid){
    $query = $this->db->query("SELECT a.* FROM tbl_request_leave a, tbl_user b WHERE a.requested_by=b.emp_id AND date('$date') between date(a.start_date) and date(a.end_date) AND b.uid=$uid");
    return $query->row();
  }
  public function getSupervisor($uid){
    $query = $this->db->query("SELECT b.emp_id,b.Supervisor FROM tbl_user a,tbl_employee b WHERE a.emp_id=b.emp_id AND a.uid=".$uid);
    return $query->row();
  }
  public function sendrequestleave($values){
    $this->db->insert('tbl_request_leave',$values);
    return $this->db->affected_rows();
  }
  public function getLeaveMyRequests($uid){
    $query = $this->db->query("SELECT a.*,e.leave_name FROM tbl_user b, tbl_applicant c,tbl_employee d, tbl_request_leave a LEFT JOIN tbl_leave e ON a.details=e.leave_id WHERE a.requested_by=b.emp_id AND b.emp_id=d.emp_id AND d.apid=c.apid AND type='Leave Request' AND b.uid=".$uid);
    return $query->result();
  }
  public function getLeaveMyRequests2($uid,$status){
   $query = $this->db->query("SELECT a.request_id,a.details,a.reason,a.start_date,(SELECT end_date FROM tbl_request_leave x WHERE x.token=a.token ORDER BY request_id DESC LIMIT 1)as end_date,a.requested_by,a.requested_on,a.responded_by,a.responded_on,a.reason_reject,a.status,a.type,a.leave_paidtrack,a.token,e.leave_name FROM tbl_user b, tbl_applicant c,tbl_employee d, tbl_request_leave a LEFT JOIN tbl_leave e ON a.details=e.leave_id WHERE a.requested_by=b.emp_id AND b.emp_id=d.emp_id AND d.apid=c.apid AND b.uid=$uid AND a.status='$status' GROUP BY token");
   return $query->result();
 }
 public function getLeaveRequestedToMe($uid){
   $query = $this->db->query("SELECT a.*,e.leave_name FROM tbl_user b, tbl_applicant c,tbl_employee d, tbl_request_leave a LEFT JOIN tbl_leave e ON a.details=e.leave_id WHERE a.responded_by=b.emp_id AND b.emp_id=d.emp_id AND d.apid=c.apid AND type='Leave Request' AND b.uid=".$uid);
   return $query->result();
 }
 public function getLeaveRequestedToMe2($uid,$status){
  $query = $this->db->query("SELECT a.request_id,a.details,a.reason,a.start_date,(SELECT end_date FROM tbl_request_leave x WHERE x.token=a.token ORDER BY request_id DESC LIMIT 1)as end_date,a.requested_by,a.requested_on,a.responded_by,a.responded_on,a.reason_reject,a.status,a.type,a.leave_paidtrack,a.token,e.leave_name FROM tbl_user b, tbl_applicant c,tbl_employee d, tbl_request_leave a LEFT JOIN tbl_leave e ON a.details=e.leave_id WHERE a.responded_by=b.emp_id AND b.emp_id=d.emp_id AND d.apid=c.apid AND b.uid=$uid AND a.status='$status' GROUP BY token");

  return $query->result();
}

public function getEmployeeDetails($emp_id){
  $query = $this->db->query("SELECT lname,fname,pic from tbl_applicant a,tbl_employee b where a.apid=b.apid AND b.emp_id=".$emp_id);
  return $query->row();
}
public function getRequestDetails($token){
  $query = $this->db->query("SELECT a.details,a.reason,a.start_date,(SELECT end_date FROM tbl_request_leave x WHERE x.token=a.token ORDER BY request_id DESC LIMIT 1)as end_date,a.requested_by,a.requested_on,a.responded_by,a.responded_on,a.reason_reject,a.status,a.type,a.token,e.leave_name FROM tbl_user b, tbl_applicant c,tbl_employee d,tbl_request_leave a LEFT JOIN tbl_leave e ON a.details=e.leave_id WHERE a.responded_by=b.emp_id AND b.emp_id=d.emp_id AND d.apid=c.apid AND a.token = '$token' LIMIT 1");
  return $query->row();
}
public function leaverequestRejected($token,$reason_reject,$responded_on){
  $data = array(
    'responded_on' => $responded_on,
    'reason_reject' => $reason_reject,
    'status' => "Rejected"
    );
  $this->db->where('token',$token);
  $this->db->update('tbl_request_leave',$data);
  return $this->db->affected_rows();
}
public function leaverequestAccepted($token,$responded_on){
  $data = array(
    'responded_on' => $responded_on,
    'status' => 'Accepted'
    );
  $this->db->where('token',$token);
  $this->db->update('tbl_request_leave',$data);
  return $this->db->affected_rows();
}
public function checkExistSchedule($emp_id,$date){
  $this->db->from('tbl_schedule');
  $this->db->where(array('emp_id'=>$emp_id,'sched_date'=>$date));
  return $this->db->get()->row();
}
public function requestNewSchedule($emp_id,$date,$leavenote){
  $data = array(
    'emp_id' => $emp_id,
    'sched_date' => $date,
    'schedtype_id' => 3,
    'acc_time_id' => NULL,
    'remarks' => "Request",
    'leavenote'=>$leavenote
    );
  $this->db->insert('tbl_schedule',$data);
  return $this->db->affected_rows();
}
public function requestUpdateSchedule($emp_id,$date,$updater_empid,$responded_on,$leavenote){
  $data = array(
    'schedtype_id' => 3,
    'acc_time_id' => NULL,
    'remarks' => "Request",
    'updated_by' => $responded_on,
    'leavenote'=>$leavenote
    );
  $this->db->where(array('emp_id'=>$emp_id,'sched_date'=>$date));
  $this->db->update('tbl_schedule',$data);
  return $this->db->affected_rows();
}
public function checkIfSchedLeave($date,$empid){
 $query = $this->db->query("SELECT * FROM tbl_schedule WHERE schedtype_id=3 AND emp_id=$empid AND sched_date='$date'");
 return $query->row(); 
}
public function updateRequestDecline($token,$thereasonreject){
  $data = array(
    'status' => 'Rejected',
    'reason_reject' => $thereasonreject
    );
  $this->db->where('token',$token);
  $this->db->update('tbl_request_leave',$data);
  return $this->db->affected_rows();
}
public function getScheduleDetails($schedids){
  $query = $this->db->query("SELECT a.sched_id,DATE_FORMAT(a.sched_date,'%W - %b %d, %Y') as schedule_date,c.time_start,c.time_end,d.type,d.style,a.emp_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id WHERE a.sched_id IN (".implode(',',$schedids).") ORDER BY a.sched_date");
  return $query->result();
}
public function updatesched($schedid,$acc_time_id,$schedtype_id){
  $updatedon = date('Y-m-d H:i:s');
  $data = array(
    'schedtype_id'=>$schedtype_id,
    'acc_time_id'=>$acc_time_id,
    'remarks'=>"Leave Request"
    );
  $this->db->where('sched_id',$schedid);
  $this->db->update('tbl_schedule',$data);
  return $this->db->affected_rows();
}
public function getBirthdate($uid){
  $query = $this->db->query("SELECT a.birthday FROM tbl_applicant a, tbl_employee b, tbl_user c WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND c.uid=$uid");
  return $query->row();
}

//NEW CODES ------------------------------------------------------------------------------

public function getListOfPositions($classesSTR,$statusesSTR){
  $query = $this->db->query("SELECT b.posempstat_id,a.pos_name,a.pos_details,a.class,c.status FROM tbl_position a,tbl_pos_emp_stat b,tbl_emp_stat c WHERE a.pos_id=b.pos_id AND c.empstat_id=b.empstat_id AND b.isActive=1 AND a.class IN ($classesSTR) AND c.status IN ($statusesSTR) ORDER BY pos_details");
  return $query->result();
}
public function getLeaveCredits($leaveidsSTR,$posempstat_id){
  $query = $this->db->query("SELECT * FROM tbl_leave_credit WHERE isActive=1 AND posempstat_id=$posempstat_id AND leave_id IN ($leaveidsSTR)");
  return $query->result();
}

public function getLeaveListWOVS(){
 $query = $this->db->query("SELECT * FROM tbl_leave WHERE leave_id NOT IN (1,2) ORDER BY leave_name");
 return $query->result();
}
public function getLeaveListSpecific($leaveidsSTR){
  $query = $this->db->query("SELECT * FROM tbl_leave WHERE leave_id IN ($leaveidsSTR) ORDER BY leave_name");
  return $query->result();
}

public function getLeaveCreditId($posempstat_id,$leave_id){
  $query = $this->db->query("SELECT * FROM tbl_leave_credit WHERE posempstat_id=$posempstat_id AND leave_id=$leave_id AND isActive=1");
  return $query->row();
}

public function insertleavecredit($posempstat_id,$leave_id,$value){
  $data = array('posempstat_id'=>$posempstat_id,'leave_id'=>$leave_id,'isPaid'=>'1','credit'=>$value,'isActive'=>1);
  $this->db->insert('tbl_leave_credit',$data);
  return $this->db->affected_rows();
}
public function updateleavecredit($leavecredit_id,$value){
 $this->db->where('leavecredit_id',$leavecredit_id);
 $this->db->update('tbl_leave_credit',array('credit'=>$value));
 return $this->db->affected_rows();
}
public function getUser($uid){
  $this->db->where('uid',$uid);
  $this->db->from('tbl_user');
  return $this->db->get()->row();
}
public function getLeaveRequestofUser($emp_id,$leavetype){
  $query = $this->db->query("SELECT * FROM `tbl_request_leave` WHERE status='Accepted' AND details=$leavetype AND YEAR(start_date) = YEAR(CURDATE())
   AND requested_by=$emp_id");
  return $query->result();
}

public function getPromoteVals($emp_id){
  $this->db->where(array('emp_id'=>$emp_id,'isActive'=>1));
  $this->db->from('tbl_emp_promote');
  return $this->db->get()->row();
}

public function sendBatchRequest($values){
  $this->db->insert_batch('tbl_request_leave', $values);
  return $this->db->affected_rows(); 
}
//NEW CODES -----------------------------------------------
public function getEmployeePosition($uid){
  $query = $this->db->query("SELECT d.*,e.* FROM tbl_user a, tbl_emp_promote b, tbl_pos_emp_stat c,tbl_emp_stat d,tbl_position e WHERE a.emp_id=b.emp_id AND b.posempstat_id=c.posempstat_id AND c.empstat_id=d.empstat_id AND c.pos_id=e.pos_id AND b.isActive=1 AND a.uid=$uid");
  return $query->row();
}
public function getRegularizationDate($uid){
  $query = $this->db->query("SELECT b.* FROM tbl_user a, tbl_emp_promote b, tbl_pos_emp_stat c,tbl_emp_stat d WHERE a.emp_id=b.emp_id AND b.posempstat_id=c.posempstat_id AND c.empstat_id=d.empstat_id AND a.uid=$uid AND b.ishistory=1 AND d.status='Regular' ORDER BY dateFrom ASC LIMIT 1");
  return $query->row();
}
}