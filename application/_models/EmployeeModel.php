<?php

class EmployeeModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getpromoteid($empid) {
        $query = $this->db->query("SELECT a.posempstat_id as id, c.emp_promoteID as pid, c.dateTo FROM tbl_pos_emp_stat as a,tbl_emp_stat as b,tbl_emp_promote as c WHERE a.posempstat_id=c.posempstat_id AND a.empstat_id=b.empstat_id AND c.emp_id='" . $empid . "' AND c.isActive=1");
        return $query->row();
    }

    public function employeefail($empid, $reason, $faildate) {
        $data = array(
            'isActibReason' => $reason,
            'dateEnd' => $faildate,
        );
        $this->db->where('emp_id', $empid);
        $this->db->update('tbl_employee', $data);
        return $this->db->affected_rows();
    }

    public function getAllDepartments() {
        $this->db->from('tbl_department');
        return $this->db->get()->result();
    }

    public function getAllPositions() {
        $query = $this->db->query('SELECT a.*,b.dep_id FROM tbl_position as a,tbl_pos_dep as b WHERE a.pos_id=b.pos_id ORDER BY a.pos_details');
        return $query->result();
    }

    public function getposempstat($posid, $empstatid) {
        $query = $this->db->query("SELECT posempstat_id as id FROM tbl_pos_emp_stat WHERE pos_id='" . $posid . "' AND empstat_id='" . $empstatid . "'");
        return $query->row();
    }

    public function promoteregular($empid, $posempstat_id, $date) {
        $data = array(
            'emp_id' => $empid,
            'posempstat_id' => $posempstat_id,
            'dateFrom' => $date,
            'isActive' => 1
        );
        $this->db->insert('tbl_emp_promote', $data);
        return $this->db->affected_rows();
    }

    public function getCCAid() {
        $query = $this->db->query("SELECT pos_id FROM tbl_position WHERE pos_name='CCA1'");
        return $query->row();
    }

    //--------------------------------------------------FOR TBL_USER----------------------------------------
    public function getTblUserData($empid) {
        $this->db->from('tbl_user');
        $this->db->where('emp_id', $empid);
        return $this->db->get()->row();
    }

    public function getName($empid) {
        $query = $this->db->query("SELECT fname,lname FROM tbl_applicant as a,tbl_employee as b WHERE a.apid=b.apid AND b.emp_id='" . $empid . "' AND b.isActive='yes'");
        return $query->row();
    }

    public function insertTblUserUname($empid, $value, $fname, $lname) {
        $data = array(
            'pic' => 'img/avatar3.png',
            'username' => $value,
            'fname' => $fname,
            'lname' => $lname,
            'emp_id' => $empid,
            'islock' => 1,
            'isActive' => 1,
            'settings' => 'bg-black font-inverse'
        );
        $this->db->insert('tbl_user', $data);
        return $this->db->affected_rows();
        // return $this->db->last_query();
    }

    public function insertTblUserPass($empid, $value, $fname, $lname) {
        $data = array(
            'pic' => 'img/avatar3.png',
            'password' => $value,
            'fname' => $fname,
            'lname' => $lname,
            'emp_id' => $empid,
            'islock' => 1,
            'isActive' => 1,
            'settings' => 'bg-black font-inverse'
        );
        $this->db->insert('tbl_user', $data);
        return $this->db->affected_rows();
        // return $this->db->last_query();
    }

    public function insertTblUserRole($empid, $value, $fname, $lname, $role) {
        $data = array(
            'pic' => 'img/avatar3.png',
            'fname' => $fname,
            'lname' => $lname,
            'role' => $role,
            'emp_id' => $empid,
            'role_id' => $value,
            'islock' => 1,
            'isActive' => 1,
            'settings' => 'bg-black font-inverse'
        );
        $this->db->insert('tbl_user', $data);
        return $this->db->affected_rows();
        // return $this->db->last_query();
    }

    public function updatetTblUserUname($empid, $value) {
        $this->db->where('emp_id', $empid);
        $this->db->update('tbl_user', array('username' => $value));
        return $this->db->affected_rows();
    }

    public function updatetTblUserPass($empid, $value) {
        $this->db->where('emp_id', $empid);
        $this->db->update('tbl_user', array('password' => $value));
        return $this->db->affected_rows();
    }

    public function updatetTblUserRole($empid, $value, $roler) {
        $this->db->where('emp_id', $empid);
        $this->db->update('tbl_user', array('role_id' => $value, 'role' => $roler));
        return $this->db->affected_rows();
    }

    //-----------------------------------ONETIMESYNC--------------------------
    public function getAllActiveEmployees() {
        // $query = $this->db->query("SELECT b.emp_id,a.fname,a.lname,b.dtr_uname,b.dtr_password FROM tbl_applicant as a,tbl_employee as b WHERE a.apid=b.apid AND b.isActive='yes'");
        $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.fname,a.lname,b.dtr_uname,b.dtr_password,g.dep_name FROM tbl_applicant as a,tbl_employee as b,tbl_emp_promote as c, tbl_pos_emp_stat as d,tbl_position as e,tbl_pos_dep as f,tbl_department as g WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND c.posempstat_id=d.posempstat_id AND d.pos_id=e.pos_id AND e.pos_id=f.pos_id AND f.dep_id=g.dep_id AND b.isActive='yes' AND d.isActive=1 AND c.isActive=1");
        return $query->result();
    }

    public function insertTblUser($emp_id, $dtr_uname, $dtr_password, $fname, $lname, $dep_name) {
        $data = array(
            'pic' => 'img/avatar3.png',
            'username' => $dtr_uname,
            'password' => $dtr_password,
            'fname' => $fname,
            'lname' => $lname,
            'role' => 'Employee|' . $dep_name,
            'emp_id' => $emp_id,
            'role_id' => 3,
            'islock' => 1,
            'isActive' => 1,
            'settings' => 'bg-black font-inverse'
        );
        $this->db->insert('tbl_user', $data);
        return $this->db->affected_rows();
    }

    public function updatetTblUser($emp_id, $dtr_uname, $dtr_password, $fname, $lname, $dep_name) {
        $data = array(
            'username' => $dtr_uname,
            'password' => $dtr_password,
            'fname' => $fname,
            'lname' => $lname,
            'role' => $dep_name
        );
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_user', $data);
        return $this->db->affected_rows();
    }

    public function getEmpDept($empid) {
        $query = $this->db->query("SELECT g.dep_name FROM tbl_employee as b,tbl_emp_promote as c, tbl_pos_emp_stat as d,tbl_position as e,tbl_pos_dep as f,tbl_department as g WHERE b.emp_id=c.emp_id AND c.posempstat_id=d.posempstat_id AND d.pos_id=e.pos_id AND e.pos_id=f.pos_id AND f.dep_id=g.dep_id AND b.isActive='yes' AND d.isActive=1 AND c.isActive=1 AND b.emp_id=" . $empid);
        return $query->row();
    }

    public function getRoleDesc($value) {
        $this->db->select('description');
        $this->db->from('tbl_user_role');
        $this->db->where('role_id', $value);
        return $this->db->get()->row();
    }

//--------------------SECURITY------------------------------------

    public function updateConfidentiality($emp_id, $activevalue) {
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee', array('confidential' => $activevalue));
        return $this->db->affected_rows();
    }

    public function matchUserCode($user, $code) {
        $query = $this->db->query("SELECT * FROM tbl_user as a,tbl_user_role as b WHERE a.role_id=b.role_id AND b.security_code='" . $code . "' AND a.uid='" . $user . "'");
        return $query->row();
    }

    public function matchEmployeeCode($user, $code) {
        $this->db->where(array('uid' => $user, 'code' => $code));
        $this->db->from('tbl_user');
        return $this->db->get()->row();
    }

//EMPLOYEE EMAIL
    public function getEmpEmail() {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.email FROM tbl_applicant as a,tbl_employee as b WHERE a.apid=b.apid AND b.isActive='yes' ORDER BY a.lname");
        return $query->result();
    }

    public function getEmpAnniv() {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.anniversary FROM tbl_applicant as a,tbl_employee as b WHERE a.apid=b.apid AND b.isActive='yes' ORDER BY a.lname");
        return $query->result();
    }

    public function getEmpPreview($emp_id) {
        $query = $this->db->query("SELECT g.status,pos_name,pos_details,b.id_num,a.pic,a.fname,a.lname,a.mname,a.gender,DATE_FORMAT(a.birthday,'%M %d, %Y') as birthday,TIMESTAMPDIFF(YEAR, a.birthday, CURDATE()) AS age,a.cell,presentAddress as address,a.religion,bloodType, b.emergency_fname,b.emergency_mname,b.emergency_lname,b.emergency_contact,h.username,h.password,(SELECT CONCAT(fname,' ',lname) FROM tbl_applicant as x,tbl_employee as z WHERE x.apid=z.apid AND z.emp_id=b.Supervisor) as Supervisor from tbl_applicant a,tbl_employee b,tbl_pos_emp_stat c,tbl_emp_promote d,tbl_position e,tbl_emp_stat g,tbl_user h where b.emp_id=h.emp_id AND g.empstat_id = c.empstat_id and a.apid=b.apid and b.emp_id= d.emp_id and c.posempstat_id = d.posempstat_id and e.pos_id = c.pos_id and d.isActive=1 and c.isActive=1 and b.emp_id=" . $emp_id);
        return $query->row();
    }

    public function getAccountLists() {
        $query = $this->db->query("SELECT acc_id as value , acc_name as text FROM tbl_account ORDER BY acc_name ASC");
        return $query->result();
    }

    public function deactivateEmployee($empid, $resigntype, $clearance, $rehire, $separation, $resigndetails) {
        $data = array(
            'isActive' => 'No',
            'isActibReason' => $resigntype,
            'rehire' => $rehire,
            'dateEnd' => $separation,
            'clearance' => $clearance,
            'remarks' => $resigndetails
        );
        $this->db->where('emp_id', $empid);
        $this->db->update('tbl_employee', $data);
        return $this->db->affected_rows();
    }

    public function getPagibigContrib($emp_id, $amount) {
        $this->db->from('tbl_pagibig_add_contribution');
        $this->db->where('emp_id', $emp_id);
        $this->db->where('amount', $amount);
        return $this->db->get()->row();
    }

    public function resetAllPagibigContrib($emp_id) {
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_pagibig_add_contribution', array('isActive' => 0));
    }

    public function insertPagibigContrib($emp_id, $amount) {
        $this->db->insert('tbl_pagibig_add_contribution', array('emp_id' => $emp_id, 'amount' => $amount, 'isActive' => 1));
        return $this->db->affected_rows();
    }
    
    public function reactivatePagibigContrib($pibig_add_id){
        $this->db->where('pibig_add_id', $pibig_add_id);
        $this->db->update('tbl_pagibig_add_contribution', array('isActive' => 1)); 
        return $this->db->affected_rows();
    }

}
