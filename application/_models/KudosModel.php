<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class KudosModel extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

    /*function get_names($addAmbassadorName)
    {
        $data   = array();
        $term   = strtolower( addslashes( trim( urldecode($addAmbassadorName) ) ) );
        $temp   = $this->db->select('fname as name')->like('fname', $term, 'LEFT')->get('tbl_applicant')->result_array();
        $data   = json_encode($temp);
        //echo "<pre>";print_r($data);echo "</pre>";die;
        return $data;

      }*/
      function get_accounts()
      {
        $query = $this->db->query("SELECT acc_id, UPPER(acc_name) as account FROM tbl_account WHERE acc_description LIKE '%Agent%' ORDER BY acc_name ASC");
        return $query->result();
      }

      function another_getName()
      {
        $query = $this->db->query("SELECT tbl_employee.emp_id, tbl_account.acc_name, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS fullname FROM tbl_applicant INNER JOIN tbl_employee ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_employee.acc_id=tbl_account.acc_id ORDER BY tbl_applicant.fname ASC");
        return $query->result();
      }

      function get_agents($campaign_id)
      {
        $campaign_id1 = mysqli_real_escape_string($this->db->conn_id,trim($campaign_id));
        $query = $this->db->query("SELECT tbl_employee.emp_id, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS fullname FROM tbl_applicant INNER JOIN tbl_employee ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_employee.acc_id=tbl_account.acc_id WHERE tbl_account.acc_id='".$campaign_id1."' ORDER BY tbl_applicant.fname ASC");
        return $query->result();
      }

      function get_names()
      {
        $query = $this->db->query("SELECT tbl_employee.emp_id, tbl_account.acc_id, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS name FROM tbl_applicant INNER JOIN tbl_employee ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_account.acc_id=tbl_employee.acc_id WHERE tbl_account.acc_description LIKE '%Agent%' AND tbl_employee.isActive LIKE '%yes%'");
        return $query->result();

      }

      function get_kudos()
      {
        $query =  $this->db->query("SELECT tbl_kudos.kudos_id, tbl_kudos.emp_id, tbl_kudos.revision, tbl_kudos.revised_by, DATE_FORMAT(tbl_kudos.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS ambassador, UCASE(tbl_account.acc_name) AS campaign, tbl_kudos.acc_id, tbl_kudos.kudos_type, tbl_kudos.client_name, tbl_kudos.phone_number, tbl_kudos.client_email, tbl_kudos.kudos_card_pic, tbl_kudos.comment, tbl_kudos.request_note, tbl_kudos.requested_by, tbl_kudos.added_by, tbl_kudos.reward_type, tbl_kudos.proofreading, tbl_kudos.kudos_card, DATE_FORMAT(tbl_kudos.added_on,'%M %d %Y %h:%i %p') AS date_given, tbl_kudos.reward_status, tbl_kudos.file FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid");
        return $query->result();
        header('Content-Type: image/jpeg');
      }

      function get_kudos_pf()
      {
        $query =  $this->db->query("SELECT tbl_kudos.kudos_id, tbl_kudos.emp_id, tbl_kudos.revision, tbl_kudos.revised_by, DATE_FORMAT(tbl_kudos.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS ambassador, tbl_account.acc_name AS campaign, tbl_kudos.acc_id, tbl_kudos.kudos_type, tbl_kudos.client_name, tbl_kudos.phone_number, tbl_kudos.client_email, tbl_kudos.comment, tbl_kudos.added_by, tbl_kudos.reward_type, tbl_kudos.proofreading, tbl_kudos.kudos_card, DATE_FORMAT(tbl_kudos.added_on,'%M %d %Y') AS date_given, tbl_kudos.reward_status, tbl_kudos.file FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE DATE(tbl_kudos.added_on) > DATE(DATE_ADD(NOW(),INTERVAL -1 MONTH)) ORDER BY tbl_kudos.proofreading DESC");
        return $query->result();
        header('Content-Type: image/jpeg');
      }

      function get_requested()
      {
        $query =  $this->db->query("SELECT tbl_kudos.kudos_id, tbl_kudos.emp_id, tbl_kudos.revision, tbl_kudos.revised_by, DATE_FORMAT(tbl_kudos.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS ambassador, tbl_account.acc_name AS campaign, tbl_kudos.acc_id, tbl_kudos.requested_by, tbl_kudos.request_note, tbl_kudos.kudos_type, tbl_kudos.client_name, tbl_kudos.phone_number, tbl_kudos.client_email, tbl_kudos.comment, tbl_kudos.added_by, tbl_kudos.reward_type, tbl_kudos.proofreading, tbl_kudos.kudos_card, DATE_FORMAT(tbl_kudos.added_on,'%M %d %Y') AS date_given, tbl_kudos.reward_status, tbl_kudos.file FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE tbl_kudos.reward_status LIKE '%Requested%'");
        return $query->result();
        header('Content-Type: image/jpeg');
      }

      function get_approved()
      {
        $query =  $this->db->query("SELECT tbl_kudos.kudos_id, tbl_kudos.emp_id, tbl_kudos.revision, tbl_kudos.revised_by, DATE_FORMAT(tbl_kudos.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS ambassador, tbl_account.acc_name AS campaign, tbl_kudos.acc_id, tbl_kudos.approved_by, DATE_FORMAT(tbl_kudos.approved_on,'%M %d %Y %h:%i %p') AS date_approved, DATE_FORMAT(tbl_kudos.requested_on,'%M %d %Y %h:%i %p') AS date_requested, tbl_kudos.requested_by, tbl_kudos.request_note, tbl_kudos.kudos_type, tbl_kudos.client_name, tbl_kudos.phone_number, tbl_kudos.client_email, tbl_kudos.comment, tbl_kudos.added_by, tbl_kudos.reward_type, tbl_kudos.proofreading, tbl_kudos.kudos_card, DATE_FORMAT(tbl_kudos.added_on,'%M %d %Y') AS date_given, tbl_kudos.reward_status, tbl_kudos.file FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE tbl_kudos.reward_status LIKE '%Approved%'");
        return $query->result();
        header('Content-Type: image/jpeg');
      }

      function get_kudos_count()
      {
        date_default_timezone_set('Asia/Manila');
        $year=date("Y");
        $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE YEAR(added_on)='".$year."' GROUP BY month ORDER BY month_number ASC");
        return $query->result();
      }

      function get_kudos_top()
      {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->db->query("SELECT DATE_FORMAT(added_on, '%m') AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE MONTH(added_on)='".$month."' AND YEAR(added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC LIMIT 5");
        return $query->result();
      }

      function get_kudos_all_agents()
      {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE MONTH(added_on)='".$month."' AND YEAR(added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC");
        return $query->result();
      }

      function get_kudos_top_campaigns()
      {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->db->query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY campaign ORDER BY kudos_count DESC LIMIT 5");
        return $query->result();
      }

      function get_kudos_by_campaigns()
      {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->db->query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, UPPER(tbl_account.acc_name) AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY campaign ORDER BY kudos_count DESC");
        return $query->result();
      }

      function get_kudos_drilldown()
      {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->db->query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC");
        return $query->result();
      }

      function get_kudos_drilldown2()
      {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->db->query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid GROUP BY month, ambassador ORDER BY kudos_count ASC");
        return $query->result();
      }

      function get_kudos_drilldown3()
      {
        date_default_timezone_set('Asia/Manila');
        $month=date("m");
        $year=date("Y");
        $query = $this->db->query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, UPPER(tbl_account.acc_name) AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY month, ambassador ORDER BY kudos_count ASC");
        return $query->result();
      }



      function add_kudos($emp_id,$campaign,$k_type,$c_name,$p_number,$e_add,$comment,$supervisor,$p_reward,$pfrd,$k_card,$r_status, $file_sc)
      {
        $emp_id1 =mysqli_real_escape_string($this->db->conn_id,trim($emp_id));
        $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
        $k_type1 =mysqli_real_escape_string($this->db->conn_id,trim($k_type));
        $c_name1 =mysqli_real_escape_string($this->db->conn_id,trim($c_name));
        $p_number1 =mysqli_real_escape_string($this->db->conn_id,trim($p_number));
        $e_add1 =mysqli_real_escape_string($this->db->conn_id,trim($e_add));
        $comment1 =mysqli_real_escape_string($this->db->conn_id,trim($comment));
        $supervisor1 =mysqli_real_escape_string($this->db->conn_id,trim($supervisor));
        $file_sc1 =mysqli_real_escape_string($this->db->conn_id,trim($file_sc));
        $file1 =mysqli_real_escape_string($this->db->conn_id,trim($file));
        $p_reward1 =mysqli_real_escape_string($this->db->conn_id,trim($p_reward));
        $pfrd1 =mysqli_real_escape_string($this->db->conn_id,trim($pfrd));
        $k_card1 =mysqli_real_escape_string($this->db->conn_id,trim($k_card));
        $r_status1 =mysqli_real_escape_string($this->db->conn_id,trim($r_status));

        $query =  $this->db->query("insert into tbl_kudos(emp_id,acc_id,kudos_type,client_name,phone_number,client_email,comment,uid,file,reward_type,proofreading,kudos_card,reward_status,added_on) values('$emp_id1','$campaign1','$k_type1','$c_name1','$p_number1','$e_add1','$comment1','$supervisor1','$file_sc1','$p_reward1','$pfrd1','$k_card1','$r_status1',now())");
      }

      function save_update($k_id,$emp_id,$campaign,$c_name,$p_number,$e_add,$comment,$supervisor,$k_type,$file_sc,$p_reward,$pfrd,$k_card,$r_status, $r_note, $r_by)
      {
        $emp_id1 =mysqli_real_escape_string($this->db->conn_id,trim($emp_id));
        $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
        $c_name1 =mysqli_real_escape_string($this->db->conn_id,trim($c_name));
        $p_number1 =mysqli_real_escape_string($this->db->conn_id,trim($p_number));
        $e_add1 =mysqli_real_escape_string($this->db->conn_id,trim($e_add));
        $comment1 =mysqli_real_escape_string($this->db->conn_id,trim($comment));
        $supervisor1 =mysqli_real_escape_string($this->db->conn_id,trim($supervisor));
        $k_type1 =mysqli_real_escape_string($this->db->conn_id,trim($k_type));
        $file_sc1 =mysqli_real_escape_string($this->db->conn_id,trim($file_sc));
        $p_reward1 =mysqli_real_escape_string($this->db->conn_id,trim($p_reward));
        $pfrd1 =mysqli_real_escape_string($this->db->conn_id,trim($pfrd));
        $k_card1 =mysqli_real_escape_string($this->db->conn_id,trim($k_card));
        $r_status1 =mysqli_real_escape_string($this->db->conn_id,trim($r_status));
        $r_note1 =mysqli_real_escape_string($this->db->conn_id,trim($r_note));
        $r_by1 =mysqli_real_escape_string($this->db->conn_id,trim($r_by));
        date_default_timezone_set('Asia/Manila');
        $date =date("Y-m-d h:i:s");

        $query =  $this->db->query("update tbl_kudos set emp_id='".$emp_id1."', acc_id='".$campaign1."',kudos_type='".$k_type1."', client_name='".$c_name1."', phone_number='".$p_number1."', client_email='".$e_add1."', comment='".$comment1."', uid='".$supervisor1."', kudos_card_pic='".$file_sc1."', reward_type='".$p_reward1."', proofreading='".$pfrd1."',kudos_card='".$k_card1."', reward_status='".$r_status1."',  request_note='".$r_note1."', requested_by='".$r_by1."', requested_on='".$date."' where kudos_id='".$k_id."'");
      }

      function save_update_opt($k_id,$emp_id,$campaign,$c_name,$p_number,$e_add,$comment,$supervisor,$k_type,$file_sc,$p_reward,$pfrd,$k_card,$r_status, $r_note, $r_by)
      {
        $emp_id1 =mysqli_real_escape_string($this->db->conn_id,trim($emp_id));
        $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
        $c_name1 =mysqli_real_escape_string($this->db->conn_id,trim($c_name));
        $p_number1 =mysqli_real_escape_string($this->db->conn_id,trim($p_number));
        $e_add1 =mysqli_real_escape_string($this->db->conn_id,trim($e_add));
        $comment1 =mysqli_real_escape_string($this->db->conn_id,trim($comment));
        $supervisor1 =mysqli_real_escape_string($this->db->conn_id,trim($supervisor));
        $k_type1 =mysqli_real_escape_string($this->db->conn_id,trim($k_type));
        $file_sc1 =mysqli_real_escape_string($this->db->conn_id,trim($file_sc));
        $p_reward1 =mysqli_real_escape_string($this->db->conn_id,trim($p_reward));
        $pfrd1 =mysqli_real_escape_string($this->db->conn_id,trim($pfrd));
        $k_card1 =mysqli_real_escape_string($this->db->conn_id,trim($k_card));
        $r_status1 =mysqli_real_escape_string($this->db->conn_id,trim($r_status));
        $r_note1 =mysqli_real_escape_string($this->db->conn_id,trim($r_note));
        $r_by1 =mysqli_real_escape_string($this->db->conn_id,trim($r_by));
        date_default_timezone_set('Asia/Manila');
        $date =date("Y-m-d h:i:s");

        $query =  $this->db->query("update tbl_kudos set emp_id='".$emp_id1."', acc_id='".$campaign1."',kudos_type='".$k_type1."', client_name='".$c_name1."', phone_number='".$p_number1."', client_email='".$e_add1."', comment='".$comment1."', uid='".$supervisor1."', file='".$file_sc1."', reward_type='".$p_reward1."', proofreading='".$pfrd1."',kudos_card='".$k_card1."', reward_status='".$r_status1."',  request_note='".$r_note1."', requested_by='".$r_by1."', requested_on='".$date."' where kudos_id='".$k_id."'");
      }

      function save_update_opt2($k_id,$emp_id,$campaign,$c_name,$p_number,$e_add,$comment,$supervisor,$k_type,$file,$p_reward,$pfrd,$k_card,$r_status, $r_note, $r_by)
      {
        $emp_id1 =mysqli_real_escape_string($this->db->conn_id,trim($emp_id));
        $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
        $c_name1 =mysqli_real_escape_string($this->db->conn_id,trim($c_name));
        $p_number1 =mysqli_real_escape_string($this->db->conn_id,trim($p_number));
        $e_add1 =mysqli_real_escape_string($this->db->conn_id,trim($e_add));
        $comment1 =mysqli_real_escape_string($this->db->conn_id,trim($comment));
        $supervisor1 =mysqli_real_escape_string($this->db->conn_id,trim($supervisor));
        $k_type1 =mysqli_real_escape_string($this->db->conn_id,trim($k_type));
        $file1 =mysqli_real_escape_string($this->db->conn_id,trim($file));
        $p_reward1 =mysqli_real_escape_string($this->db->conn_id,trim($p_reward));
        $pfrd1 =mysqli_real_escape_string($this->db->conn_id,trim($pfrd));
        $k_card1 =mysqli_real_escape_string($this->db->conn_id,trim($k_card));
        $r_status1 =mysqli_real_escape_string($this->db->conn_id,trim($r_status));
        $r_note1 =mysqli_real_escape_string($this->db->conn_id,trim($r_note));
        $r_by1 =mysqli_real_escape_string($this->db->conn_id,trim($r_by));
        date_default_timezone_set('Asia/Manila');
        $date =date("Y-m-d h:i:s");

        $query =  $this->db->query("update tbl_kudos set emp_id='".$emp_id1."', acc_id='".$campaign1."',kudos_type='".$k_type1."', client_name='".$c_name1."', phone_number='".$p_number1."', client_email='".$e_add1."', comment='".$comment1."', uid='".$supervisor1."', file='".$file1."', reward_type='".$p_reward1."', proofreading='".$pfrd1."',kudos_card='".$k_card1."', reward_status='".$r_status1."',  request_note='".$r_note1."', requested_by='".$r_by1."' where kudos_id='".$k_id."'");
      }

      function approveCash($k_id,$r_status,$a_by)
      {
        $r_status1 =mysqli_real_escape_string($this->db->conn_id,trim($r_status));
        $a_by1 =mysqli_real_escape_string($this->db->conn_id,trim($a_by));
        date_default_timezone_set('Asia/Manila');
        $date =date("Y-m-d h:i:s");
        $query =  $this->db->query("update tbl_kudos set reward_status='".$r_status1."', approved_by='".$a_by1."', approved_on='".$date."' where kudos_id='".$k_id."'");
      }

      function releaseCash($k_id,$r_status)
      {
        $r_status1 =mysqli_real_escape_string($this->db->conn_id,trim($r_status));
        $query =  $this->db->query("update tbl_kudos set reward_status='".$r_status1."' where kudos_id='".$k_id."'");
      }

      function update_proofreading($pfrd,$comment,$revision,$r_by,$k_id)
      {
        $pfrd1 =mysqli_real_escape_string($this->db->conn_id,trim($pfrd));
        $comment1 =mysqli_real_escape_string($this->db->conn_id,trim($comment));
        $revision1 =mysqli_real_escape_string($this->db->conn_id,trim($revision));
        $r_by1 =mysqli_real_escape_string($this->db->conn_id,trim($r_by));
        $k_id1 =mysqli_real_escape_string($this->db->conn_id,trim($k_id));
        date_default_timezone_set('Asia/Manila');
        $date =date("Y-m-d h:i:s");
        $query =  $this->db->query("update tbl_kudos set revision='".$revision1."', comment='".$comment1."', revised_by='".$r_by1."', revised_on='".$date."', proofreading='".$pfrd1."' where kudos_id='".$k_id."'");
        return $this->db->affected_rows();
      }

      function getDateRange($startDate,$endDate)
      {
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE added_on >= '".$startDate1." ".$startTime."' AND added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");
        return $query->result();
      }

      function getDateRangeDrilldown($startDate,$endDate)
      {
        $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
        $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
        $startTime = '00:00:00';
        $endTime = '23:59:59';
        $query = $this->db->query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND added_on <= '".$endDate1." ".$endTime."' GROUP BY month, ambassador ORDER BY kudos_count ASC");
        return $query->result();
      }

      function getDateRangeDrilldown2()
      {
      $startDate1 = '2017-01-01';//mysqli_real_escape_string($this->db->conn_id,trim($startDate));
      $endDate1 = '2017-03-01';//mysqli_real_escape_string($this->db->conn_id,trim($endDate));
      $startTime = '00:00:00';
      $endTime = '23:59:59';
      $query = $this->db->query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND added_on <= '".$endDate1." ".$endTime."' GROUP BY month, ambassador ORDER BY kudos_count ASC");
      return $query->result();
    }

    function getDateRange2($startDate,$endDate,$campaign)
    {
      $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
      $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
      $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
      $startTime = '00:00:00';
      $endTime = '23:59:59';
      $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(tbl_kudos.added_on, '%m'), ' ',DATE_FORMAT(tbl_kudos.added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_account.acc_id=tbl_kudos.acc_id WHERE tbl_kudos.acc_id='".$campaign1."' AND tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");
      return $query->result();
    }

    function getDateRange3($startDate,$endDate)
    {
      $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
      $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
      $startTime = '00:00:00';
      $endTime = '23:59:59';
      $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY ambassador ORDER BY kudos_count DESC");
      return $query->result();
    }
    function getEmployee($empid)
    {
     $query = $this->db->query("SELECT tbl_account.acc_id, tbl_account.acc_name, tbl_applicant.fname, tbl_applicant.lname, tbl_employee.id_num, tbl_employee.emp_id FROM tbl_applicant  INNER JOIN tbl_employee ON tbl_applicant.apid=tbl_employee.apid INNER JOIN tbl_account ON tbl_account.acc_id=tbl_employee.acc_id WHERE tbl_employee.id_num='".$empid."'");
     return $query->result();
   }

   function insertExcel($emp_id,$acc_id,$k_type,$r_type,$c_name,$c_pnumber,$c_email,$c_comment,$uid,$d_given)
   {
    $emp_id1 =mysqli_real_escape_string($this->db->conn_id,trim($emp_id));
    $acc_id1 =mysqli_real_escape_string($this->db->conn_id,trim($acc_id));
    $k_type1 =mysqli_real_escape_string($this->db->conn_id,trim($k_type));
    $c_name1 =mysqli_real_escape_string($this->db->conn_id,trim($c_name));
    $c_pnumber1 =mysqli_real_escape_string($this->db->conn_id,trim($c_pnumber));
    $c_email1 =mysqli_real_escape_string($this->db->conn_id,trim($c_email));
    $c_comment1 =mysqli_real_escape_string($this->db->conn_id,trim($c_comment));
    $uid1 =mysqli_real_escape_string($this->db->conn_id,trim($uid));
    $r_type1 =mysqli_real_escape_string($this->db->conn_id,trim($r_type));
    $proofreading = 'Pending';
    $kudos_card = 'Pending';
    $reward_status = 'Pending';
    
    $query = $this->db->query("insert into tbl_kudos(emp_id,acc_id,kudos_type,client_name,phone_number,client_email,comment,uid,reward_type,proofreading,kudos_card,reward_status,added_on) values('$emp_id1','$acc_id1','$k_type1','$c_name1','$c_pnumber1','$c_email1','$c_comment1','$uid1','$r_type1','$proofreading','$kudos_card','$reward_status','$d_given')");
  }
 
    function getkudosthisyear(){
    $year = date("Y");
    $query = $this->db->query("SELECT DATE_FORMAT(added_on, '%M %Y') AS monthyear, COUNT(*) as cnt FROM tbl_kudos WHERE YEAR(added_on)='".$year."' GROUP BY monthyear ORDER BY DATE_FORMAT(added_on, '%m %Y') ASC");
    return $query->result();
  }


    function getkudosmonthlythisyear(){
    $year = date("Y");$month = date("M");
    $query = $this->db->query("SELECT COUNT(*) as counter,DATE_FORMAT(tbl_kudos.added_on,'%M %Y') as monthyear, tbl_applicant.fname, tbl_applicant.lname, tbl_applicant.mname FROM tbl_kudos,tbl_employee,tbl_applicant WHERE  tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' GROUP BY tbl_applicant.apid , DATE_FORMAT(added_on, '%M %Y') ORDER BY tbl_applicant.lname ASC");
    return $query->result();
  }

 //-------------chartx2-----------------
    function getkudoscurrentmonth(){
    $year = date("Y");$month = date("n");
    $query = $this->db->query("SELECT COUNT(*) as counter,DATE_FORMAT(tbl_kudos.added_on,'%M %Y') as monthyear, tbl_applicant.fname, tbl_applicant.lname, tbl_applicant.mname FROM tbl_kudos,tbl_account,tbl_employee,tbl_applicant WHERE tbl_kudos.acc_id=tbl_account.acc_id AND tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' AND MONTH(added_on)='".$month."' GROUP BY tbl_applicant.apid,DATE_FORMAT(added_on, '%M %Y') ORDER BY tbl_applicant.lname ASC");
    return $query->result();
  }

 //-------------chartx3-----------------
    function getkudostop5(){
   $year = date("Y");$month = date("n");
   $query = $this->db->query("SELECT COUNT(*) as counter, tbl_applicant.fname, tbl_applicant.lname, tbl_applicant.mname FROM tbl_kudos,tbl_account,tbl_employee,tbl_applicant WHERE tbl_kudos.acc_id=tbl_account.acc_id AND tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' AND MONTH(added_on)='".$month."' GROUP BY tbl_applicant.apid,DATE_FORMAT(added_on, '%M %Y') ORDER BY counter DESC LIMIT 5");
   return $query->result();
 }

 //-------------chartx4-----------------
   function getkudoscampaigntop(){
   $year = date("Y");$month = date("n");
   $query = $this->db->query("SELECT COUNT(*) as counter,DATE_FORMAT(tbl_kudos.added_on,'%M %Y') as monthyear, tbl_account.acc_name as name FROM tbl_kudos,tbl_account,tbl_employee,tbl_applicant WHERE tbl_kudos.acc_id=tbl_account.acc_id AND tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' AND MONTH(added_on)='".$month."' GROUP BY tbl_account.acc_id,DATE_FORMAT(added_on, '%M %Y') ORDER BY name ASC LIMIT 5");
   return $query->result();
 }

 //-------------USED IN ALL-----------------
   function getcampaignamabassadors(){
 $year = date("Y");$month = date("n");
    $query = $this->db->query("SELECT COUNT(*) as counter,DATE_FORMAT(tbl_kudos.added_on,'%M %Y') as monthyear, tbl_account.acc_name as campaignname,tbl_applicant.fname, tbl_applicant.lname, tbl_applicant.mname FROM tbl_kudos,tbl_account,tbl_employee,tbl_applicant WHERE tbl_kudos.acc_id=tbl_account.acc_id AND tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' AND MONTH(added_on)='".$month."' GROUP BY tbl_applicant.apid,DATE_FORMAT(added_on, '%M %Y') ORDER BY tbl_applicant.lname ASC ");
    return $query->result();
}

 //-------------charter5-----------------
   function getkudoscampaign(){
   $year = date("Y");$month = date("n");
   $query = $this->db->query("SELECT COUNT(*) as counter,DATE_FORMAT(tbl_kudos.added_on,'%M %Y') as monthyear, tbl_account.acc_name as name FROM tbl_kudos,tbl_account,tbl_employee,tbl_applicant WHERE tbl_kudos.acc_id=tbl_account.acc_id AND tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' AND MONTH(added_on)='".$month."' GROUP BY tbl_account.acc_id,DATE_FORMAT(added_on, '%M %Y') ORDER BY name ASC ");
   return $query->result();
 }

 //-------------charter6-----------------
   function getnewcampaigntopscore(){
   $year = date("Y");$month = date("n");
   $query = $this->db->query("SELECT DISTINCT COUNT(*) as scores,DATE_FORMAT(tbl_kudos.added_on,'%M %Y') as monthyear FROM tbl_kudos,tbl_account,tbl_employee,tbl_applicant WHERE tbl_kudos.acc_id=tbl_account.acc_id AND tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' AND MONTH(added_on)='".$month."' GROUP BY tbl_account.acc_id,DATE_FORMAT(added_on, '%M %Y') ORDER BY scores DESC LIMIT 5");
   return $query->result();
 }
   function getnewambassadortopscore(){
     $year = date("Y");$month = date("n");
   $query = $this->db->query("SELECT DISTINCT COUNT(*) as scores,DATE_FORMAT(tbl_kudos.added_on,'%M %Y') as monthyear FROM tbl_kudos,tbl_account,tbl_employee,tbl_applicant WHERE tbl_kudos.acc_id=tbl_account.acc_id AND tbl_kudos.emp_id=tbl_employee.emp_id AND tbl_employee.apid=tbl_applicant.apid AND YEAR(added_on)='".$year."' AND MONTH(added_on)='".$month."' GROUP BY tbl_applicant.apid,DATE_FORMAT(added_on, '%M %Y') ORDER BY scores DESC LIMIT 5");
   return $query->result();
 }




 //---------------FOR PENDING KUDOS-----------------
   function getKudosPending(){
    $this->db->where('kudos_card','Pending');
    return $this->db->count_all_results('tbl_kudos');
  }
}
?>