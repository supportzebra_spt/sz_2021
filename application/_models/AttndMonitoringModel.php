<?php

class AttndMonitoringModel extends CI_Model {

	public function __construct()
	{
                // Call the CI_Model constructor
		parent::__construct();
	}
	public function getEmployeeDetails($employeelist){
		$query = $this->db->query("SELECT emp_id,lname,fname,acc_id FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.emp_id IN (".implode(',',$employeelist).") ORDER BY lname ASC");
		return $query->result();
	}
	
	// public function getDTRIns($emp_id,$dates){
	// 	$query = $this->db->query("SELECT a.sched_id,a.dtr_id as dtr_id_in,DATE_FORMAT(a.log,'%Y-%m-%d %h:%i %p') as log_in,DATE_FORMAT(a.log,'%Y-%m-%d') as date FROM tbl_dtr_logs a WHERE entry='I' AND type='DTR' AND date(a.log) IN (".'\''.implode('\',\'',$dates).'\''.") AND a.emp_id=$emp_id");
	// 	return $query->result();
	// }
	// public function getDTROuts($emp_id,$dates){
	// 	$query = $this->db->query("SELECT a.dtr_id as dtr_id_out,DATE_FORMAT(a.log,'%Y-%m-%d %h:%i %p') as log_out,note FROM tbl_dtr_logs a WHERE entry='O' AND type='DTR' AND date(a.log) IN (".'\''.implode('\',\'',$dates).'\''.") AND a.emp_id=$emp_id");
	// 	return $query->result();
	// }
	public function getDTRIns($emp_id,$start,$end){
		$query = $this->db->query("SELECT DATE(a.log) as login_date,a.emp_id,a.sched_id,a.dtr_id as dtr_id_in,DATE_FORMAT(a.log,'%Y-%m-%d <br><i>%h:%i %p</i>') as log_in_view,DATE_FORMAT(a.log,'%Y-%m-%d %h:%i %p') as log_in,DATE_FORMAT(a.log,'%Y-%m-%d') as date FROM tbl_dtr_logs a WHERE entry='I' AND type='DTR' AND date(a.log) between date(DATE_SUB('$start', INTERVAL 1 DAY)) and date('$end') AND a.emp_id=$emp_id ORDER BY dtr_id ASC");
		return $query->result();
	}
	public function getDTROuts($emp_id,$start,$end){
		$query = $this->db->query("SELECT a.dtr_id as dtr_id_out,DATE_FORMAT(a.log,'%Y-%m-%d <br><i>%h:%i %p</i>') as log_out_view,DATE_FORMAT(a.log,'%Y-%m-%d %h:%i %p') as log_out,note FROM tbl_dtr_logs a WHERE entry='O' AND type='DTR' AND date(a.log) between date('$start') and date(DATE_ADD('$end', INTERVAL 2 DAY)) AND a.emp_id=$emp_id ORDER BY dtr_id ASC");
		return $query->result();
	}
	public function getEmpSchedule($emp_id,$date){
		$query = $this->db->query("SELECT c.time_start,c.time_end,d.type,a.leavenote,a.sched_date,a.sched_id FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id LEFT JOIN tbl_leave f ON f.leave_id=a.leavenote WHERE a.sched_date='$date' AND a.emp_id=$emp_id ORDER BY a.sched_date");
		return $query->row();
	}
	
	public function getLeaveNote($leavenote){
		$this->db->select('leave_name');
		$this->db->from('tbl_leave');
		$this->db->where('leave_id',$leavenote);
		return $this->db->get()->row();
	}

	public function getBreaks($break_id,$emp_id,$dtr_id_in,$dtr_id_out){
		$query = $this->db->query("SELECT DISTINCT(a.entry),a.dtr_id,DATE_FORMAT(a.log,'%Y-%m-%d <br><i>%h:%i %p</i>') as logview,a.dtr_id,DATE_FORMAT(a.log,'%Y-%m-%d %h:%i %p') as log,a.note,e.break_type FROM tbl_dtr_logs a,tbl_acc_time b,tbl_time c,tbl_break_time_account d,tbl_break e WHERE a.acc_time_id=b.acc_time_id AND b.time_id=c.time_id AND c.bta_id=d.bta_id AND d.break_id=e.break_id AND a.emp_id=$emp_id AND a.dtr_id BETWEEN '$dtr_id_in' AND '$dtr_id_out' AND e.break_id=$break_id");
		return $query->result();
	}
	// public function getEmpSchedules($emp_id,$dates){
	// 	$query = $this->db->query("SELECT a.sched_date,a.sched_id,b.type,acc_time_id,leavenote FROM tbl_schedule a,tbl_schedule_type b WHERE a.schedtype_id=b.schedtype_id AND a.emp_id=$emp_id AND a.sched_date IN (".'\''.implode('\',\'',$dates).'\''.")");
	// 	return $query->result();
	// }
	// public function getEmployeeDTRlogsWithSched(){
	// 	$query = $this->db->query("SELECT a.sched_id,a.dtr_id as dtr_id_in,DATE_FORMAT(a.log,'%M %d,%Y %h:%i %p') as log_in FROM tbl_dtr_logs a WHERE entry='I' AND type='DTR' AND date(a.log) between date('".$fromDate."') and date('".$toDate."') AND a.emp_id=$emp_id ORDER BY a.dtr_id");
	// 	return $query->result();
	// }
	// public function getDTRIn($emp_id,$date){
	// 	$query = $this->db->query("SELECT a.dtr_id as dtr_id_in,DATE_FORMAT(a.log,'%M %d,%Y %h:%i %p') as log_in FROM tbl_dtr_logs a WHERE entry='I' AND type='DTR' AND date(a.log) = '$date' AND a.emp_id=$emp_id");
	// 	return $query->row();
	// }
	// public function getDTROut($dtr_id){
	// 	$query = $this->db->query("SELECT a.dtr_id as dtr_id_out,DATE_FORMAT(a.log,'%M %d,%Y %h:%i %p') as log_out FROM tbl_dtr_logs a WHERE entry='O' AND type='DTR' AND a.note=$dtr_id");
	// 	return $query->row();
	// }

	public function getBreakTime($breaktype,$acc_id){
		$query = $this->db->query("SELECT e.* FROM tbl_acc_time a,tbl_time b,tbl_break_time_account c,tbl_break d,tbl_break_time e WHERE a.acc_id=$acc_id AND a.time_id=b.time_id AND b.bta_id=c.bta_id AND c.break_id=d.break_id AND c.btime_id=e.btime_id AND d.break_type='$breaktype'");
		return $query->row();
	}

	//NEW CODES

	public function getEmpScheduleBySchedID($emp_id,$schedid){
		$query = $this->db->query("SELECT c.time_start,c.time_end,d.type,a.leavenote FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id LEFT JOIN tbl_leave f ON f.leave_id=a.leavenote WHERE a.sched_id=$schedid AND a.emp_id=$emp_id ORDER BY a.sched_date");
		return $query->row();
	}

}
