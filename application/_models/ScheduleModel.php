<?php

class ScheduleModel extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getAccount($desc) {
        $this->db->from('tbl_account');
        $this->db->where('acc_description', $desc);
        $this->db->order_by("acc_name", "asc");
        return $this->db->get()->result();
    }

    public function getAccountEmployee($acc_id) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' AND b.acc_id='" . $acc_id . "' ORDER BY a.lname DESC");
        return $query->result();
    }

    public function getAllEmployees() {
        $max = $this->db->query("SET SESSION group_concat_max_len = 1000000;");


        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.id_num,GROUP_CONCAT(CONCAT(e.time_start,' - ',e.time_end) ORDER BY
      STR_TO_DATE(e.time_start, '%l:%i:%s %p'))as shift FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_acc_time d,tbl_time e WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND c.acc_id=d.acc_id AND d.time_id=e.time_id AND b.isActive='yes' AND e.bta_id=1 GROUP BY b.emp_id ORDER BY a.lname");
        return $query->result();
    }

    public function getAllEmployeesPerType($acc_description) {
        $max = $this->db->query("SET SESSION group_concat_max_len = 1000000;");

        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.id_num,GROUP_CONCAT(CONCAT(e.time_start,' - ',e.time_end) ORDER BY
      STR_TO_DATE(e.time_start, '%l:%i:%s %p'))as shift FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_acc_time d,tbl_time e WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND c.acc_id=d.acc_id AND d.time_id=e.time_id AND acc_description='" . $acc_description . "' AND b.isActive='yes' AND e.bta_id=1 GROUP BY b.emp_id ORDER BY a.lname");
        return $query->result();
    }

    public function getAllEmployeesInList($employeelist) {
        $max = $this->db->query("SET SESSION group_concat_max_len = 1000000;");
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.id_num,GROUP_CONCAT(CONCAT(e.time_start,' - ',e.time_end) ORDER BY STR_TO_DATE(e.time_start, '%l:%i:%s %p')) as shift FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_acc_time d,tbl_time e WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND c.acc_id=d.acc_id AND d.time_id=e.time_id AND b.emp_id IN (" . implode(',', $employeelist) . ") AND b.isActive='yes' AND e.bta_id=1 GROUP BY b.emp_id ORDER BY a.lname");
        return $query->result();
    }

    public function checkAll($fromDate, $toDate) {
        $query = $this->db->query("SELECT b.id_num,a.lname,a.fname,b.emp_id,GROUP_CONCAT(DATE_FORMAT(sched_date,'%b %d, %Y') ORDER BY sched_date)as sched_date FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_schedule f WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND b.emp_id=f.emp_id AND date(f.sched_date) between date('" . $fromDate . "') and date('" . $toDate . "') AND b.isActive='yes' GROUP BY b.emp_id ORDER BY a.lname");
        return $query->result();
    }

    public function checkPerType($fromDate, $toDate, $acc_description) {
        $query = $this->db->query("SELECT b.id_num,a.lname,a.fname,b.emp_id,GROUP_CONCAT(DATE_FORMAT(sched_date,'%b %d, %Y') ORDER BY sched_date)as sched_date FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_schedule f WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND acc_description='" . $acc_description . "' AND b.emp_id=f.emp_id AND date(f.sched_date) between date('" . $fromDate . "') and date('" . $toDate . "') AND b.isActive='yes' GROUP BY b.emp_id ORDER BY a.lname");
        return $query->result();
    }

    public function checkPerAccountEmployee($fromDate, $toDate, $employeelist) {
        $query = $this->db->query("SELECT b.id_num,a.lname,a.fname,b.emp_id,GROUP_CONCAT(DATE_FORMAT(sched_date,'%b %d, %Y') ORDER BY sched_date)as sched_date FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_schedule f WHERE a.apid=b.apid AND b.acc_id=c.acc_id  AND b.emp_id IN (" . implode(',', $employeelist) . ") AND b.emp_id=f.emp_id AND date(f.sched_date) between date('" . $fromDate . "') and date('" . $toDate . "') AND b.isActive='yes' GROUP BY b.emp_id ORDER BY a.lname");
        return $query->result();
    }

    public function getschedtypenotnormal() {
        $query = $this->db->query("SELECT * FROM tbl_schedule_type WHERE type not in('Leave-WP','Leave-WoP','Double','Normal','WORD')");
        return $query->result();
    }

    public function getAccTimeId($emp_id, $start, $end) {
        $query = $this->db->query("SELECT c.acc_time_id FROM tbl_employee as a,tbl_account as b,tbl_acc_time as c,tbl_time as d WHERE a.acc_id=b.acc_id AND b.acc_id=c.acc_id AND d.time_id=c.time_id AND time_start='" . $start . "' AND time_end='" . $end . "' AND a.emp_id=" . $emp_id);
        return $query->row();
    }

    public function savemultiplesched($values) {
        $this->db->insert('tbl_schedule', $values);
        return $this->db->affected_rows();
    }

    public function updatemultiplesched($values, $sched_id, $where) {
        if ($where != '') {
            $this->db->where($where);
        }

        $this->db->where('sched_id', $sched_id);
        $this->db->update('tbl_schedule', $values);
        return $this->db->affected_rows();
    }

    public function checkifExistSchedule($emp_id, $sched_date) {
        $query = $this->db->query("SELECT * FROM tbl_schedule WHERE emp_id=" . $emp_id . " AND sched_date='" . $sched_date . "'");
        return $query->row();
    }

    public function checkifExistSchedule2($emp_id, $sched_date) {
        $query = $this->db->query("SELECT c.time_start,c.time_end,d.type FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id LEFT JOIN tbl_leave_type f ON f.leaveType_ID=a.leavenote WHERE a.sched_date='$sched_date' AND a.emp_id=$emp_id ORDER BY a.sched_date");
        return $query->row();
    }

// public function getSchedAllNames($fromDate,$toDate){
//   $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.lname,a.fname FROM tbl_applicant a,tbl_employee b, tbl_schedule c WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND date(c.sched_date) between date('".$fromDate."') and date('".$toDate."') ORDER BY a.lname");
//   return $query->result();
// }
    public function getSchedAllNames() {
        $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.lname,a.fname FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' ORDER BY a.lname");
        return $query->result();
    }

// public function getSchedPerTypeNames($fromDate,$toDate,$acc_description){
//   $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.lname,a.fname FROM tbl_applicant a,tbl_employee b, tbl_schedule c,tbl_account d WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND b.acc_id=d.acc_id AND date(c.sched_date) between date('".$fromDate."') and date('".$toDate."') AND d.acc_description='".$acc_description."' ORDER BY a.lname");
//   return $query->result();
// }
    public function getSchedPerTypeNames($acc_description) {
        $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.lname,a.fname FROM tbl_applicant a,tbl_employee b, tbl_schedule c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND d.acc_description='" . $acc_description . "' AND b.isActive='yes' ORDER BY a.lname");
        return $query->result();
    }

// public function getSchedPerAccountNames($fromDate,$toDate,$employeelist){
//   $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.lname,a.fname FROM tbl_applicant a,tbl_employee b, tbl_schedule c WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND date(c.sched_date) between date('".$fromDate."') and date('".$toDate."') AND b.emp_id IN (".implode(',',$employeelist).") ORDER BY a.lname");
//   return $query->result();
// }
    public function getSchedPerAccountNames($employeelist) {
        $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.lname,a.fname FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' AND b.emp_id IN (" . implode(',', $employeelist) . ") ORDER BY a.lname");
        return $query->result();
    }

    public function getSchedofEmp($fromDate, $toDate, $emp_id) {
        //------------------without leave name-------------------
        // $query = $this->db->query("SELECT a.sched_id,DATE_FORMAT(a.sched_date,'%W - %b %d, %Y') as schedule_date,c.time_start,c.time_end,d.type,d.style,a.leavenote FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id WHERE date(a.sched_date) between date('".$fromDate."') and date('".$toDate."') AND a.emp_id=".$emp_id." ORDER BY a.sched_date");
        $query = $this->db->query("SELECT a.sched_id,DATE_FORMAT(a.sched_date,'%W - %b %d, %Y') as schedule_date,c.time_start,c.time_end,d.type,d.style,a.leavenote,f.leaveType,f.leaveType_ID,d.schedtype_id,a.acc_time_id,d.style FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id LEFT JOIN tbl_leave_type f ON f.leaveType_ID=a.leavenote WHERE date(a.sched_date) between date('" . $fromDate . "') and date('" . $toDate . "') AND a.emp_id=" . $emp_id . " and a.isDouble=0 ORDER BY a.sched_date");
        return $query->result(); 
    }

    public function getSchedofEmp2($fromDate, $toDate, $emp_id) {
        $query = $this->db->query("SELECT a.sched_id,DATE_FORMAT(a.sched_date,'%b %d, %Y') as schedule_date,c.time_start,c.time_end,d.type,d.style FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id WHERE date(a.sched_date) between date('" . $fromDate . "') and date('" . $toDate . "') AND a.emp_id=" . $emp_id . " AND (a.schedtype_id=1 OR a.schedtype_id=4 OR a.schedtype_id=5) and a.isActive=1 ORDER BY a.sched_date");
        return $query->result();
    }

    public function getSchedTimeEmp($emp_id) {
        $query = $this->db->query("SELECT CONCAT('TIME',e.time_id) as value,CONCAT(e.time_start,' - ',e.time_end)as text FROM tbl_applicant a,tbl_employee b,tbl_account c,tbl_acc_time d,tbl_time e WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND c.acc_id=d.acc_id AND d.time_id=e.time_id AND b.isActive='yes' AND e.bta_id=1 AND b.emp_id=" . $emp_id . " UNION SELECT CONCAT('SCHED',schedtype_id) as value, type as text FROM tbl_schedule_type WHERE type!='Normal'");
        return $query->result();
    }

    public function getCalendarEmpSched($uid) {
        // $query = $this->db->query("SELECT a.sched_date,c.time_start,c.time_end,d.type,d.style,a.leavenote FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_user e ON e.emp_id=a.emp_id WHERE e.uid=".$uid." ORDER BY a.sched_date");

        $query = $this->db->query("SELECT a.sched_date,c.time_start,c.time_end,d.type,d.style,f.leaveType FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id INNER JOIN tbl_user e ON e.emp_id=a.emp_id LEFT JOIN tbl_leave_type f ON f.leaveType_ID=a.leavenote WHERE e.uid=$uid  and a.isActive=1 ORDER BY a.sched_date");
        return $query->result();
    }

    public function getCalendarLegend() {
        $this->db->from('tbl_schedule_type');
		$this->db->where('type !=', "Double");
		$this->db->order_by('type', 'ASC');

        return $this->db->get()->result();
    }
	public function getCalendarLegend2() {
        $this->db->from('tbl_schedule_type');
		$this->db->order_by('type', 'ASC');

        return $this->db->get()->result();
    }
	public function getAllLeaveTypes() {
        $this->db->from('tbl_leave_type');
		$this->db->where('isActive', "1");

        return $this->db->get()->result();
    }

    public function getAccTimeIdWORD($emp_id) {
        $query = $this->db->query("SELECT acc_time_id FROM tbl_acc_time a, tbl_employee b WHERE a.isDefault=1 AND b.acc_id=a.acc_id AND b.emp_id=" . $emp_id);
        return $query->row();
    }

    public function getTimeSched($emp_id) {
        $query = $this->db->query("SELECT c.acc_time_id,d.time_id,d.time_start,d.time_end FROM tbl_employee a, tbl_account b, tbl_acc_time c, tbl_time d WHERE a.acc_id=b.acc_id AND b.acc_id=c.acc_id AND c.time_id=d.time_id AND bta_id=1 AND a.emp_id=" . $emp_id . " ORDER BY STR_TO_DATE(d.time_start, '%l:%i:%s %p')");
        return $query->result();
    }
 

    public function getTimeSched2($emp_id) {
        $query = $this->db->query("SELECT c.acc_time_id,d.time_id,d.time_start,d.time_end FROM tbl_employee a, tbl_account b, tbl_acc_time c, tbl_time d WHERE a.acc_id=b.acc_id AND b.acc_id=c.acc_id AND c.time_id=d.time_id AND bta_id=1 AND a.emp_id=" . $emp_id . " ORDER BY STR_TO_DATE(d.time_start, '%l:%i:%s %p')");
        return $query->result();
    }

    public function confirmdoubleshift($emp_id, $date) {
        $query = $this->db->query("SELECT acc_time_id,sched_id,b.type,isActive from tbl_schedule a,tbl_schedule_type b where a.schedtype_id=b.schedtype_id and emp_id=$emp_id and sched_date='" . $date . "' and isDouble=1");
        return $query->result();
    }

    public function addDoubleShiftSched($emp_id, $acc_time_id, $scheddate, $switchstate) {
        $data = array(
            'emp_id' => $emp_id,
            'sched_date' => $scheddate,
            'schedtype_id' => $switchstate,
            'acc_time_id' => $acc_time_id,
            'isDouble' => 1,
            'remarks' => 'Manual',
        );
        $this->db->insert('tbl_schedule', $data);
        return $this->db->affected_rows();
    }

    public function updateDoubleShiftSched($emp_id, $acc_time_id, $sched_id, $switchstate) {
        $data = array(
            'acc_time_id' => $acc_time_id,
            'updated_by' => $this->session->userdata('uid'),
            'updated_on' => 'now()',
            'schedtype_id' => $switchstate,
        );
        $this->db->where('sched_id', $sched_id);
        $this->db->update('tbl_schedule', $data);
        return $this->db->affected_rows();
    }

    public function checkdeleteDoubleshift($sched_id) {
        $query = $this->db->query("SELECT  * from tbl_dtr_logs where sched_id=" . $sched_id);
        return $query->result();
    }

    public function deleteDoubleshift($sched_id, $val) {
        $data = array(
            'isActive' => $val,
        );
        $this->db->where('sched_id', $sched_id);
        $this->db->update('tbl_schedule', $data);
        return $this->db->affected_rows();
    }

    public function updatesched($leave_id, $schedid, $acc_time_id, $schedtype_id, $emp_id, $uid) {
         $data = array(
            'emp_id' => $emp_id,
            'schedtype_id' => $schedtype_id,
            'acc_time_id' => $acc_time_id,
            'updated_by' => $uid,
            'leavenote' => $leave_id
        );
        $this->db->where('sched_id', $schedid);
        $this->db->update('tbl_schedule', $data);
          return $this->db->affected_rows();
         
    }

    public function insertsched($leave_id, $acc_time_id, $schedtype_id, $scheddate, $emp_id) {
        $data = array(
            'emp_id' => $emp_id,
            'sched_date' => $scheddate,
            'schedtype_id' => $schedtype_id,
            'acc_time_id' => $acc_time_id,
            'remarks' => 'Manual',
            'leavenote' => $leave_id,
            'updated_by' => $_SESSION["uid"]
        );
        $this->db->insert('tbl_schedule', $data);
        return $this->db->affected_rows();
    }

    public function getSupervisoryEmpLevel1($uid) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.Supervisor=c.emp_id AND b.isActive='yes' AND c.uid=" . $uid);
        return $query->result();
    }

    public function getSupervisoryEmpLevel1AgentULevel1($uid) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND d.acc_description='Agent' AND b.Supervisor=c.emp_id AND b.isActive='yes' AND d.dep_id=1 and acc_description='Agent'");
        return $query->result();
    }

    public function insertManagerSched($empid) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND b.emp_id=" . $empid . " limit 1");
        return $query->result();
    }

    public function getSupervisoryEmpNext($emp_id) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND b.isActive='yes' AND b.Supervisor IN (" . implode(',', $emp_id) . ")");
        return $query->result();
    }

    public function getSupervisoryEmpLevel1Admin($uid) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND d.acc_description='Admin' AND b.Supervisor=c.emp_id AND b.isActive='yes' AND c.uid=" . $uid);
        return $query->result();
    }

    public function getSupervisoryEmpNextAdmin($emp_id) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND d.acc_description='Admin' AND b.isActive='yes' AND b.Supervisor IN (" . implode(',', $emp_id) . ")");
        return $query->result();
    }

    public function getSupervisoryEmpLevel1Agent($uid) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_user c,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND d.acc_description='Agent' AND b.Supervisor=c.emp_id AND b.isActive='yes' AND c.uid=" . $uid);

        return $query->result();
    }

    public function getSupervisoryEmpNextAgent($emp_id) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,b.Supervisor,d.acc_description FROM tbl_applicant a,tbl_employee b,tbl_account d WHERE a.apid=b.apid AND b.acc_id=d.acc_id AND d.acc_description='Agent' AND b.isActive='yes' AND b.Supervisor IN (" . implode(',', $emp_id) . ")");
        return $query->result();
    }

    public function getLeaveTypes() {
        $this->db->from('tbl_leave');
        $this->db->order_by('leave_name', 'asc');
        return $this->db->get()->result();
    }

// public function getAllDTR($fromDate,$toDate){
//  $query = $this->db->query("SELECT a.sched_id,a.dtr_id as dtr_id_in,DATE_FORMAT(a.log,'%M %d,%Y %r') as log_in,a.emp_id,(SELECT dtr_id FROM tbl_dtr_logs WHERE note=a.dtr_id and entry='O') as dtr_id_out,(SELECT DATE_FORMAT(log,'%M %d,%Y %r') FROM tbl_dtr_logs WHERE note=a.dtr_id) as log_out FROM tbl_dtr_logs a WHERE entry='I' AND type='DTR' AND date(a.log) between date('".$fromDate."') and date('".$toDate."')");
//  return $query->result();
// }

    public function getEmployeeName($emp_id) {
        $query = $this->db->query("SELECT lname,fname FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.emp_id=" . $emp_id);
        return $query->row();
    }

    public function schedupdaterdtr($sched_id, $dtr_id, $acc_time_id) {
        $data = array('sched_id' => $sched_id, 'acc_time_id' => $acc_time_id);
        $this->db->where('dtr_id', $dtr_id);
        $this->db->update('tbl_dtr_logs', $data);
        return $this->db->affected_rows();
    }

//-----------------------SAMPLE
    public function getAllDTRin($fromDate, $toDate) {
        $query = $this->db->query("SELECT a.sched_id as sched_id_in,a.dtr_id as dtr_id_in,DATE_FORMAT(a.log,'%M %d,%Y %r') as log_in,a.emp_id FROM tbl_dtr_logs a WHERE entry='I' AND type='DTR' AND date(a.log) between date('" . $fromDate . "') and date('" . $toDate . "') ORDER BY a.dtr_id");
        return $query->result();
    }

    public function getAllDTRout($fromDate, $toDate) {
        $query = $this->db->query("SELECT a.sched_id as sched_id_out,a.note,a.dtr_id as dtr_id_out,DATE_FORMAT(a.log,'%M %d,%Y %r') as log_out FROM tbl_dtr_logs a WHERE entry='O' AND type='DTR' AND date(a.log) between date('" . $fromDate . "') and DATE_ADD('" . $toDate . "', INTERVAL 2 DAY)");
        return $query->result();
    }

    public function getAllEmpIdPerType($acc_description) {
        $query = $this->db->query("SELECT emp_id FROM tbl_employee a, tbl_account b WHERE a.acc_id=b.acc_id AND b.acc_description='" . $acc_description . "' AND a.isActive='yes'");
        return $query->result();
    }

    public function getAllEmpIdPerAccountEmp($employeelist) {
        $query = $this->db->query("SELECT emp_id FROM tbl_employee WHERE emp_id IN (" . implode(',', $employeelist) . ") AND isActive='yes'");
        return $query->result();
    }

    public function getAccTimeIdFromSched($sched_id) {
        $this->db->select('acc_time_id');
        $this->db->where('sched_id', $sched_id);
        $this->db->from('tbl_schedule');
        return $this->db->get()->row();
    }

    public function getAccountsList($acc_description) {
        $query = $this->db->query("SELECT * FROM tbl_account WHERE acc_description LIKE '%$acc_description%' ORDER BY acc_name ASC");
        return $query->result();
    }

    public function getAccountSched($acc_id) {
        $query = $this->db->query("SELECT a.acc_time_id,b.time_start,b.time_end,a.isDefault FROM tbl_acc_time a,tbl_time b WHERE b.bta_id=1 AND a.time_id=b.time_id AND a.acc_id=$acc_id");
        return $query->result();
    }

    public function changeaccountdefault($acc_time_id) {
        $this->db->where('acc_time_id', $acc_time_id);
        $this->db->update('tbl_acc_time', array('isDefault' => 1));
        return $this->db->affected_rows();
    }

    public function getAccountList() {
        // $query = $this->db->query("SELECT * FROM tbl_account a LEFT JOIN tbl_department b ON a.dep_id=b.dep_id");
        $query = $this->db->query("SELECT * FROM tbl_account a LEFT JOIN tbl_department b ON a.dep_id=b.dep_id LEFT JOIN tbl_acc_time c ON a.acc_id=c.acc_id INNER JOIN tbl_time d ON c.time_id=d.time_id WHERE c.isDefault=1");
        return $query->result();
    }

    public function getDepartments() {
        $this->db->from('tbl_department');
        $this->db->order_by('dep_details', 'ASC');
        return $this->db->get()->result();
    }

    public function getScheduleList() {
        $query = $this->db->query("SELECT * FROM tbl_time WHERE bta_id=1 ORDER BY STR_TO_DATE(time_start, '%l:%i:%s %p')");
        return $query->result();
    }

    public function createAccount($acc_name, $acc_description, $dep_id,$uacc_level,$uacc_bct,$uacc_reference) {
        $data = array(
            'acc_name' => $acc_name,
            'acc_description' => $acc_description,
            'dep_id' => $dep_id,
            'accountLevel' => $uacc_level,
            'beforeCallingtime' => $uacc_bct,
            'accountReference' => $uacc_reference,
        );

        $this->db->insert('tbl_account', $data);
        return $this->db->insert_id();
    }

    public function assignDefaultSched($time_id, $acc_id) {
        $data = array(
            'time_id' => $time_id,
            'acc_id' => $acc_id,
            'note' => 'DTR',
            'isDefault' => 1
        );
        $this->db->insert('tbl_acc_time', $data);
        return $this->db->affected_rows();
    }

    public function getAccountSpecific($acc_id) {
        $query = $this->db->query("SELECT * FROM tbl_account a LEFT JOIN tbl_department b ON a.dep_id=b.dep_id LEFT JOIN tbl_acc_time c ON a.acc_id=c.acc_id INNER JOIN tbl_time d ON c.time_id=d.time_id WHERE c.isDefault=1 AND a.acc_id=$acc_id");
        return $query->row();
    }
	 

    public function updateAccount($acc_id, $acc_name, $acc_description, $dep_id,$uacc_level,$uacc_bct,$uacc_reference){
        $data = array(
            'acc_name' => $acc_name,
            'acc_description' => $acc_description,
            'dep_id' => $dep_id,
            'accountLevel' => $uacc_level,
            'beforeCallingtime' => $uacc_bct,
            'accountReference' => $uacc_reference,
        );
        $this->db->where('acc_id', $acc_id);
        $this->db->update('tbl_account', $data);
        return $this->db->affected_rows();
    }

    public function updateDefaultSched($acc_time_id) {
        $this->db->where('acc_time_id', $acc_time_id);
        $this->db->update('tbl_acc_time', array('isDefault' => NULL));
        return $this->db->affected_rows();
    }

    public function getAccTimeDetails($acc_id, $time_id) {
        $this->db->where(array('acc_id' => $acc_id, 'time_id' => $time_id));
        $this->db->from('tbl_acc_time');
        return $this->db->get()->row();
    }

    public function reassignDefaultSched($acc_time_id) {
        $this->db->where('acc_time_id', $acc_time_id);
        $this->db->update('tbl_acc_time', array('isDefault' => 1));
        return $this->db->affected_rows();
    }

//--------------------NICCA LATEST--------------------------

    public function getAllDTRinLogs($fromDate, $toDate) {
        $query = $this->db->query("SELECT a.sched_id,a.dtr_id as dtr_id_in,DATE_FORMAT(a.log,'%M %d,%Y') as log_in_date,DATE_FORMAT(a.log,'%r') as log_in_time,a.emp_id FROM tbl_dtr_logs a WHERE entry='I' AND type='DTR' AND date(a.log) between date('" . $fromDate . "') and date('" . $toDate . "') ORDER BY a.dtr_id");
        return $query->result();
    }

    public function getAllDTRoutLogs($fromDate, $toDate) {
        $query = $this->db->query("SELECT a.note,a.dtr_id as dtr_id_out,DATE_FORMAT(a.log,'%M %d,%Y') as log_out_date,DATE_FORMAT(a.log,'%r') as log_out_time FROM tbl_dtr_logs a WHERE entry='O' AND type='DTR' AND date(a.log) between date('" . $fromDate . "') and date('" . $toDate . "')");
        return $query->result();
    }

    public function getSched($sched_id) {
        $query = $this->db->query("SELECT CONCAT(c.time_start,' - ',c.time_end) as sched_time FROM tbl_schedule a LEFT JOIN tbl_acc_time b ON b.acc_time_id=a.acc_time_id LEFT JOIN tbl_time c ON c.time_id=b.time_id INNER JOIN tbl_schedule_type d ON d.schedtype_id=a.schedtype_id LEFT JOIN tbl_leave_type f ON f.leaveType_ID=a.leavenote WHERE a.sched_id=$sched_id ORDER BY a.sched_date");
        return $query->row();
    }

    public function updateDtrLogs($newtime, $dtr_id) {
        $this->db->query("UPDATE tbl_dtr_logs SET log = CONCAT(date(log),' $newtime') WHERE dtr_id=$dtr_id;");
        return $this->db->affected_rows();
    }

    public function getDTRLog($dtr_id) {
        $this->db->where('dtr_id', $dtr_id);
        $this->db->from('tbl_dtr_logs');
        return $this->db->get()->row();
    }

    public function insertDtrHistory($log, $dtr_id, $currentdate, $uid) {
        $data = array(
            'dtr_id' => $dtr_id,
            'OrigTime' => $log,
            'updated_on' => $currentdate,
            'updated_by' => $uid
        );
        $this->db->insert('tbl_dtr_history', $data);
        return $this->db->affected_rows();
    }

    public function getTeamAll($acc_id) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND b.isActive='yes' AND c.acc_id=$acc_id ORDER BY lname DESC");
        return $query->result();
    }

    public function getTeamDesc($acc_id, $desc) {
        $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id FROM tbl_applicant a,tbl_employee b,tbl_account c WHERE a.apid=b.apid AND b.acc_id=c.acc_id AND b.isActive='yes' AND c.acc_id=$acc_id AND c.acc_description='$desc' ORDER BY lname DESC");
        return $query->result();
    }

    public function getAccountUser($uid) {
        $query = $this->db->query("SELECT acc_id FROM tbl_user a,tbl_employee b WHERE a.emp_id=b.emp_id AND a.uid=$uid");
        return $query->row();
    }

//--------------------------------------START OF SHIFTS --------------------------------------------------------------
    public function getAccShifts($acc_id) {
        $query = $this->db->query("SELECT acc_time_id,acc_id,a.time_id,DATE_FORMAT(STR_TO_DATE(time_start, '%l:%i:%s %p'),'%h:%i:%s %p') as time_start,DATE_FORMAT(STR_TO_DATE(time_end, '%l:%i:%s %p'),'%h:%i:%s %p')as time_end FROM tbl_acc_time a,tbl_time b WHERE a.time_id=b.time_id AND a.acc_id=$acc_id AND a.note='dtr' ORDER BY STR_TO_DATE(time_start, '%l:%i:%s %p')");
        return $query->result();
    }

    public function getAdminAccounts() {
        $this->db->where('acc_description', 'Admin');
        $this->db->from('tbl_account');
        $this->db->order_by('acc_name', 'ASC');
        return $this->db->get()->result();
    }

    public function getAgentAccounts() {
        $this->db->where('acc_description', 'Agent');
        $this->db->from('tbl_account');
        $this->db->order_by('acc_name', 'ASC');
        return $this->db->get()->result();
    }

    public function getAccTimeBreak($acc_time_id, $break_type) {
        $query = $this->db->query("SELECT * FROM tbl_shift_break a,tbl_break_time_account b, tbl_break c,tbl_break_time d WHERE a.acc_time_id=$acc_time_id AND a.bta_id=b.bta_id AND b.break_id=c.break_id AND b.btime_id=d.btime_id AND c.break_type='$break_type' AND a.isActive=1");
        return $query->row();
    }

    public function getAvailableAccTimeBreak($break_type) {
        $query = $this->db->query("SELECT * FROM tbl_break_time_account b, tbl_break c,tbl_break_time d WHERE b.break_id=c.break_id AND b.btime_id=d.btime_id AND c.break_type='$break_type'");
        return $query->result();
    }

    public function resetShiftBreak($acc_time_id) {
        $this->db->where('acc_time_id', $acc_time_id);
        $this->db->update('tbl_shift_break', array('isActive' => 0));
        return $this->db->affected_rows();
    }

    public function getShiftBreak($bta_id, $acc_time_id) {
        $this->db->where(array('acc_time_id' => $acc_time_id, 'bta_id' => $bta_id));
        $this->db->from('tbl_shift_break');
        return $this->db->get()->row();
    }

    public function insertShiftBreak($acc_time_id, $bta_id) {
        $data = array('acc_time_id' => $acc_time_id, 'bta_id' => $bta_id, 'isActive' => 1);
        $this->db->insert('tbl_shift_break', $data);
        return $this->db->affected_rows();
    }

    public function updateShiftBreak($sbrk_id) {
        $this->db->where('sbrk_id', $sbrk_id);
        $this->db->update('tbl_shift_break', array('isActive' => 1));
        return $this->db->affected_rows();
    }

    public function getAllAccount() {
        $this->db->from('tbl_account');
		$this->db->order_by('acc_name');

        return $this->db->get()->result();
    }

    /*     public function createTime($time_start, $time_end) {
      $this->db->insert('tbl_time', array('time_start' => $time_start, 'time_end' => $time_end));
      return $this->db->insert_id();
      } */

    public function createTime($time_start, $time_end) {
        $this->db->insert('tbl_time', array('time_start' => $time_start, 'time_end' => $time_end, 'time_break' => "", 'bta_id' => 1));
        return $this->db->insert_id();
    }

    public function createAccTime($acc_id, $time_id) {
        $this->db->insert('tbl_acc_time', array('time_id' => $time_id, 'acc_id' => $acc_id, 'note' => 'DTR'));
        return $this->db->insert_id();
    }

    public function checkTimeExists($time_end, $time_start) {
        $this->db->where(array('time_start' => $time_start, 'time_end' => $time_end));
        $this->db->from('tbl_time');
        return $this->db->get()->num_rows();
    }

//----------------------------------------------END OF SHIFT------------------------------------------------------------------
    public function getAllTime() {
        $this->db->where('time_break !=', 'BREAK');
        $this->db->from('tbl_time');
        return $this->db->get()->result();
    }

    //----------------------------OLD ACCOUNT SHIFT-----------------------------------

    public function getAccShifts_old($acc_id) {
        $query = $this->db->query("SELECT * FROM tbl_acc_time a,tbl_time b WHERE a.time_id=b.time_id AND a.acc_id=$acc_id AND a.note='dtr' AND bta_id=1 ORDER BY STR_TO_DATE(time_start, '%l:%i:%s %p')");
        return $query->result();
    }

    public function getAccBreaks_old($acc_id) {
        $query = $this->db->query("SELECT * FROM tbl_acc_time a, tbl_time b,tbl_break_time_account c,tbl_break d,tbl_break_time e WHERE a.time_id=b.time_id AND b.bta_id=c.bta_id AND c.break_id=d.break_id AND c.btime_id=e.btime_id AND a.acc_id=$acc_id AND a.note='BREAK'");
        return $query->result();
    }

    public function getNotShifts_old($timeids) {
        $query = $this->db->query("SELECT * FROM tbl_time WHERE time_id NOT IN (" . implode(',', $timeids) . ") AND bta_id=1 ORDER BY STR_TO_DATE(time_start, '%l:%i:%s %p')");
        return $query->result();
    }

    public function createAccTime_old($acc_id, $time_id) {
        $this->db->insert('tbl_acc_time', array('time_id' => $time_id, 'acc_id' => $acc_id, 'note' => 'DTR'));
        return $this->db->affected_rows();
    }

    public function getTimebta_old($bta_id) {
        $this->db->where('bta_id', $bta_id);
        $this->db->from('tbl_time');
        return $this->db->get()->row();
    }

    public function createAccTimeBreak_old($acc_id, $time_id) {
        $this->db->insert('tbl_acc_time', array('time_id' => $time_id, 'acc_id' => $acc_id, 'note' => 'BREAK'));
        return $this->db->affected_rows();
    }

    public function getNotBreaks_old($where) {
        $query = $this->db->query("SELECT * FROM tbl_break_time_account a,tbl_break b,tbl_break_time c WHERE a.break_id=b.break_id AND a.btime_id=c.btime_id $where AND a.bta_id!=1 ");
        return $query->result();
    }

}
