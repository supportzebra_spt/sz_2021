<?php

class GeneralModel extends CI_Model 
{
	
	/**************SELECT*******************/
    public function fetchSpecificValue($fields, $where, $tables, $order = NULL) { //get 1  record
        $this->db->select($fields);
        $this->db->where($where);
        if ($order !== NULL) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($tables);
        return $query->row();
    }
    public function fetchSpecificValues($fields, $where, $tables, $order = NULL) { //get more than 1 records
        $this->db->select($fields);
        $this->db->where($where);
        if ($order !== NULL) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($tables);
        return $query->result();
    }

    public function fetchAllValues($fields, $tables, $order = NULL) { //get all records
        $this->db->select($fields);
        if ($order !== NULL) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($tables);
        return $query->result();
    }
	
		/**************Insert*******************/

    public function insertValues($data, $table) // simple insert
    {
        $this->db->trans_start();
        $this->db->insert($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }
	public function batchInsert($data, $table){
        $this->db->trans_start();
        $this->db->insert_batch($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        } 
    }
    public function insertValuesArray($data, $table) // Optional
    {
        $this->db->trans_start();
        for($loop = 0; $loop<count($data);$loop++){         
            $this->db->insert($table, $data[$loop]);  
        }  
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }
	
    public function insertValuesWthLastInsertId($data, $table) //Simple Insert and return last inserted ID
    {
        $this->db->trans_start();
        $this->db->insert($table, $data);
        $lastInsertId = $this->db->insert_id();
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return $lastInsertId;
        }
    }
	public function batchInsertWithFirstInsertId($data, $table){ // Batch Insert and return first inserted ID
        $this->db->trans_start();
        $this->db->insert_batch($table, $data);
        $firstInsertedId = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return $firstInsertedId;
        } 
    }
	
		/**************Update*******************/
    public function updateValue($data, $where, $table){
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }
	
		/**************Delete*******************/
    public function deleteValues($where, $table){
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->delete($table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }
    
    /**************Custom*******************/
	public function customQuery($qry) { //custom query
        $query = $this->db->query($qry);
        return $query->result();
    }

}
