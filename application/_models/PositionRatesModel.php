<?php

class PositionRatesModel extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getPosRates() {
        $query = $this->db->query("SELECT a.rate_id,a.isActive,a.rate,c.pos_name,c.pos_details,d.status,c.class,f.dep_name,f.dep_details FROM tbl_rate as a,tbl_pos_emp_stat as b, tbl_position as c,tbl_emp_stat as d, tbl_pos_dep as e, tbl_department as f WHERE a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND c.pos_id=e.pos_id AND e.dep_id=f.dep_id AND b.isActive=1 AND a.isActive=1");
        return $query->result();
    }

    public function getPosStatRates() {
        $query2 = $this->db->query("SELECT d.dep_id,a.pos_id,pos_name,pos_details,GROUP_CONCAT(CONCAT(empstat_id,'|',b.status)) as status FROM tbl_position as a,tbl_emp_stat as b,tbl_pos_dep as c,tbl_department as d WHERE c.pos_id=a.pos_id AND c.dep_id=d.dep_id AND a.pos_id NOT IN (SELECT DISTINCT(pos_id) FROM tbl_pos_emp_stat WHERE posempstat_id IN (SELECT DISTINCT(posempstat_id) FROM tbl_rate WHERE isActive=1) AND empstat_id=b.empstat_id) GROUP BY pos_id ORDER BY pos_details");
        return $query2->result();
    }

    public function getAllDept() {
        $query3 = $this->db->query("SELECT * FROM tbl_department ORDER BY dep_details");
        return $query3->result();
    }

    public function getPositionAllowance() {
        $query = $this->db->query("SELECT a.pos_id,a.pos_name,a.pos_details,c.empstat_id,c.status,GROUP_CONCAT(CONCAT(d.allowance_id,'|',d.value,'|',d.allowance_name,'|',d.posempstat_id) ORDER BY d.allowance_name)as value FROM tbl_position as a,tbl_pos_emp_stat as b, tbl_emp_stat as c, tbl_allowance as d WHERE a.pos_id=b.pos_id AND b.empstat_id=c.empstat_id AND d.posempstat_id=b.posempstat_id AND d.isActive=1 GROUP BY a.pos_id,c.empstat_id");
        return $query->result();
    }

    public function getPosEmpStatID($posid, $empstat) {
        $this->db->select('posempstat_id');
        $this->db->from('tbl_pos_emp_stat');
        $this->db->where(array('pos_id' => $posid, 'empstat_id' => $empstat, 'isActive' => 1));
        return $this->db->get()->row();
    }

    public function getRates($posempstatid, $ratevalue) {
        $query2 = $this->db->query("SELECT rate_id,rate FROM tbl_rate WHERE posempstat_id = " . $posempstatid . " AND rate LIKE '" . $ratevalue . "%'");
        return $query2->result();
    }

    public function insertNewRate($data2) {
        $this->db->insert('tbl_rate', $data2);
        return $this->db->affected_rows();
    }

    public function updateActiveRate($rate_id, $activevalue) {
        $this->db->where('rate_id', $rate_id);
        $this->db->update('tbl_rate', array('isActive' => $activevalue));
        return $this->db->affected_rows();
    }

    public function insertPosEmpStat($data) {
        $this->db->insert('tbl_pos_emp_stat', $data);
        return $this->db->insert_id();
    }

    public function getRateId($status_id, $position_id, $rate) {
        return $this->db->query("SELECT b.rate_id FROM tbl_pos_emp_stat as a,tbl_rate as b WHERE a.posempstat_id=b.posempstat_id AND a.empstat_id=" . $status_id . " AND a.pos_id=" . $position_id . " AND b.rate=" . $rate . "")->row();
    }

    public function insertNewPosEmpStat($posid, $empstat) {
        $data = array(
            'pos_id' => $posid,
            'empstat_id' => $empstat,
            'isActive' => 1,
            'date' => DATE('Y-m-d')
        );
        $this->db->insert('tbl_pos_emp_stat', $data);
        return $this->db->insert_id();
    }

    public function insertNewAllowance($name, $posempstatid, $value) {
        $data = array(
            'allowance_name' => $name,
            'description' => $name,
            'value' => $value,
            'isActive' => 1,
            'posempstat_id' => $posempstatid
        );
        $this->db->insert('tbl_allowance', $data);
        return $this->db->affected_rows();
    }

    public function getPosStatAllowance() {
        // return $this->db->query("SELECT d.dep_id,a.pos_id,pos_name,pos_details,GROUP_CONCAT(CONCAT(empstat_id,'|',b.status)) as status FROM tbl_position as a,tbl_emp_stat as b,tbl_pos_dep as c,tbl_department as d WHERE c.pos_id=a.pos_id AND c.dep_id=d.dep_id AND a.pos_id NOT IN (SELECT DISTINCT(pos_id) FROM tbl_pos_emp_stat WHERE posempstat_id IN (SELECT DISTINCT(posempstat_id) FROM tbl_allowance WHERE isActive=1) AND empstat_id=b.empstat_id) GROUP BY pos_id ORDER BY pos_details")->result();
        $query = $this->db->query("SELECT d.dep_id,a.pos_id,pos_name,pos_details,GROUP_CONCAT(CONCAT(empstat_id,'|',b.status)) as status FROM tbl_position as a,tbl_emp_stat as b,tbl_pos_dep as c,tbl_department as d WHERE c.pos_id=a.pos_id AND c.dep_id=d.dep_id AND a.pos_id IN (SELECT DISTINCT(pos_id) FROM tbl_pos_emp_stat WHERE posempstat_id IN (SELECT DISTINCT(posempstat_id) FROM tbl_rate WHERE isActive=1) AND posempstat_id NOT IN (SELECT DISTINCT(posempstat_id) FROM tbl_allowance WHERE isActive=1) AND empstat_id=b.empstat_id) GROUP BY pos_id ORDER BY pos_details");
        return $query->result();
    }

    public function updateActiveAllowance($allowanceid, $activevalue) {
        $this->db->where('allowance_id', $allowanceid);
        $this->db->update('tbl_allowance', array('isActive' => $activevalue));
        return $this->db->affected_rows();
    }

    public function getPositions() {
        $taxdesc = $this->db->query("SELECT CONCAT(a.pos_details,' - ',c.status) as text,b.posempstat_id as value FROM tbl_position as a,tbl_pos_emp_stat as b,tbl_emp_stat as c,tbl_rate d WHERE d.posempstat_id=b.posempstat_id AND a.pos_id=b.pos_id AND c.empstat_id=b.empstat_id AND b.isActive=1 AND d.isActive=1 ORDER BY a.pos_details ASC");
        return $taxdesc->result();
    }

    public function getEmpPos() {
        $query = $this->db->query("SELECT c.posempstat_id,b.emp_id,a.lname,a.fname FROM tbl_employee as b LEFT JOIN tbl_emp_promote as c ON b.emp_id=c.emp_id AND c.isActive=1 INNER JOIN tbl_applicant as a ON a.apid=b.apid AND b.isActive='yes' ORDER BY a.lname");
        return $query->result();
    }

    //bonuses
    public function getAllBonus() {
        $this->db->from('tbl_bonus');
        return $this->db->get()->result();
    }

    public function newBonus($data) {
        $this->db->insert('tbl_bonus', $data);
        return $this->db->affected_rows();
    }

    public function updateActiveBonus($bonus_id, $activevalue) {
        $this->db->where('bonus_id', $bonus_id);
        $this->db->update('tbl_bonus', array('isActive' => $activevalue));
        return $this->db->affected_rows();
    }

    public function getPosStatBonus() {
        $query = $this->db->query("SELECT d.dep_id,a.pos_id,pos_name,pos_details,GROUP_CONCAT(CONCAT(empstat_id,'|',b.status)) as status FROM tbl_position as a,tbl_emp_stat as b,tbl_pos_dep as c,tbl_department as d WHERE c.pos_id=a.pos_id AND c.dep_id=d.dep_id AND a.pos_id IN (SELECT DISTINCT(pos_id) FROM tbl_pos_emp_stat WHERE posempstat_id IN (SELECT DISTINCT(posempstat_id) FROM tbl_rate WHERE isActive=1) AND posempstat_id NOT IN (SELECT DISTINCT(posempstat_id) FROM tbl_pos_bonus WHERE isActive=1) AND empstat_id=b.empstat_id) GROUP BY pos_id ORDER BY pos_details");
        return $query->result();
    }

    public function getPositionBonus() {
        $query = $this->db->query("SELECT b.posempstat_id,a.pos_id,a.pos_name,a.pos_details,c.empstat_id,c.status,GROUP_CONCAT(CONCAT(e.pbonus_id,'|',e.amount,'|',f.bonus_name,'|',f.bonus_id) ORDER BY f.bonus_id)as bonus FROM tbl_position as a,tbl_pos_emp_stat as b, tbl_emp_stat as c, tbl_pos_bonus as e,tbl_bonus as f WHERE a.pos_id=b.pos_id AND b.empstat_id=c.empstat_id AND b.posempstat_id=e.posempstat_id AND e.bonus_id=f.bonus_id AND e.isActive=1 GROUP BY b.posempstat_id ORDER BY a.pos_name");
        return $query->result();
    }

    public function getBonusList() {
        $this->db->from('tbl_bonus');
        $this->db->where('isActive', 1);
        return $this->db->get()->result();
    }

    public function insertPosBonus($data) {
        $this->db->insert('tbl_pos_bonus', $data);
        return $this->db->affected_rows();
    }

    public function countBonus() {
        $query = $this->db->query("SELECT COUNT(*) as count FROM tbl_bonus WHERE isActive=1");
        return $query->row();
    }

    public function deactivatePositionBonus($pbonusid) {
        $this->db->where('pbonus_id', $pbonusid);
        $this->db->update('tbl_pos_bonus', array('isActive' => 0));
        return $this->db->affected_rows();
    }

    public function updatePositionBonus($posempstatid, $bonusid, $amount) {
        $data = array(
            'posempstat_id' => $posempstatid,
            'bonus_id' => $bonusid,
            'amount' => $amount,
            'isActive' => 1
        );
        $this->db->insert('tbl_pos_bonus', $data);
        return $this->db->affected_rows();
    }

    public function getPosAvailableBonus($posempstatid, $ids) {
        $query = $this->db->query("SELECT * FROM tbl_bonus WHERE bonus_id NOT IN (SELECT bonus_id FROM tbl_pos_bonus WHERE posempstat_id='" . $posempstatid . "' AND bonus_id IN ( '" . implode($ids, "', '") . "' ))");
        return $query->result();
    }

    //NEW CODES-------------------------------------------------------------------------------------
    public function getEmployeelist() {
        $query = $this->db->query("SELECT emp_id,lname,fname,pic FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' ORDER BY lname ASC");
        return $query->result();
    }

    public function getEmpPosHistory($emp_id) {
        $query = $this->db->query("SELECT emp_promoteID,b.posempstat_id,a.dateFrom as date,a.isActive,c.pos_id,d.empstat_id,pos_details,pos_name,class,d.status FROM tbl_emp_promote a,tbl_pos_emp_stat b, tbl_position c,tbl_emp_stat d WHERE a.posempstat_id=b.posempstat_id AND b.pos_id=c.pos_id AND b.empstat_id=d.empstat_id AND a.emp_id=$emp_id AND isHistory=1 ORDER BY isActive");
        return $query->result();
    }

    public function updateemppromotehist($date, $emp_promoteid) {
        $this->db->where('emp_promoteID', $emp_promoteid);
        $this->db->update('tbl_emp_promote', array('dateFrom' => $date));
        return $this->db->affected_rows();
    }

    public function getPosStatAll() {
        $query2 = $this->db->query("SELECT d.dep_id,a.pos_id,pos_name,pos_details FROM tbl_position as a,tbl_emp_stat as b,tbl_pos_dep as c,tbl_department as d WHERE c.pos_id=a.pos_id AND c.dep_id=d.dep_id GROUP BY pos_id ORDER BY pos_details");
        return $query2->result();
    }

    public function insertnewpositionhistory($empid, $posempstat_id, $date) {
        $data = array('posempstat_id' => $posempstat_id, 'emp_id' => $empid, 'dateFrom' => $date);
        $this->db->insert('tbl_emp_promote', $data);
        return $this->db->affected_rows();
    }

    public function getEmployeeATM() {
        $query = $this->db->query("SELECT b.emp_id,a.fname,a.lname,a.mname,b.isAtm,b.atm_account_number FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' ORDER BY a.lname ");
        return $query->result();
    }

    public function updateATM($emp_id, $activevalue) {
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee', array('isAtm' => $activevalue));
        return $this->db->affected_rows();
    }
	public function updateATMdetails($emp_id, $value) {
        $this->db->where('emp_id', $emp_id);
        $this->db->update('tbl_employee', array('atm_account_number' => $value));
         return $this->db->affected_rows();
        // return $this->db->last_query();
    }

    public function disableHistory($emp_promoteid) {
        $this->db->where('emp_promoteID', $emp_promoteid);
        $this->db->update('tbl_emp_promote', array('isHistory' => 0));
        return $this->db->affected_rows();
    }

    public function getAllPosition() {
        $this->db->from('tbl_position');
        $this->db->order_by('pos_details', 'ASC');
        return $this->db->get()->result();
    }

    public function createnewposition($code, $position, $classification, $isHiring) {
        $data = array(
            'pos_name' => $code,
            'pos_details' => $position,
            'class' => $classification,
            'isHiring' => $isHiring
        );
        $this->db->insert('tbl_position', $data);
        return $this->db->insert_id();
    }

    public function updateposition($pos_id, $code, $position, $classification, $isHiring) {
        $data = array(
            'pos_name' => $code,
            'pos_details' => $position,
            'class' => $classification,
            'isHiring' => $isHiring
        );
        $this->db->where('pos_id', $pos_id);
        $this->db->update('tbl_position', $data);
        return $this->db->affected_rows();
    }

    public function getAllPositionWithDept() {
        $query = $this->db->query("SELECT * FROM tbl_position a,tbl_department b,tbl_pos_dep c WHERE a.pos_id=c.pos_id AND b.dep_id=c.dep_id");
        return $query->result();
    }

    public function createPosDep($dep_id, $pos_id) {
        $this->db->insert('tbl_pos_dep', array('pos_id' => $pos_id, 'dep_id' => $dep_id));
        return $this->db->affected_rows();
    }

    public function getPosDep($pos_id) {
        $this->db->select('pos_dep_id');
        $this->db->where('pos_id', $pos_id);
        $this->db->from('tbl_pos_dep');
        return $this->db->get()->row();
    }

    public function updatePosDep($pos_dep_id, $dep_id) {
        $this->db->where('pos_dep_id', $pos_dep_id);
        $this->db->update('tbl_pos_dep', array('dep_id' => $dep_id));
        return $this->db->affected_rows();
    }

}
