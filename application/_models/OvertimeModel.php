<?php

class OvertimeModel extends CI_Model 
{
    public function fetchSpecificValue($fields, $where, $tables){
        $this->db->select($fields);
        $this->db->where($where);
        $query = $this->db->get($tables);
        return $query->row();
    }
    public function fetchSpecificValues($fields, $where, $tables){
        $this->db->select($fields);
        $this->db->where($where);
        $query = $this->db->get($tables);
        return $query->result();
    }
    public function insertValues($data, $table)
    {
        $this->db->trans_start();
        $this->db->insert($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }
    public function insertValuesArray($data, $table)
    {
        $this->db->trans_start();
        for($loop = 0; $loop<count($data);$loop++){         
            $this->db->insert($table, $data[$loop]);  
        }  
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }
    public function insertValuesWthLastInsertId($data, $table)
    {
        $this->db->trans_start();
        $this->db->insert($table, $data);
        $lastInsertId = $this->db->insert_id();
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return $lastInsertId;
        }
    }
    public function updateValue($data, $where, $table){
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }

    public function deleteValues($where, $table){
        $this->db->trans_start();
        $this->db->where($where);
        $this->db->delete($table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
               return 0;
        } else {
               return 1;
        }
    }
}
