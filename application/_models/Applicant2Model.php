<?php

class Applicant2Model extends CI_Model {

  public function __construct()
  {
                // Call the CI_Model constructor
    parent::__construct();
  }
  public function getAllApplicants(){
    $query = $this->db->query("SELECT apid,fname,mname,lname,gender,pic,presentAddrezs,pos_name,pos_details,cell FROM tbl_applicant a,tbl_position b WHERE a.pos_id=b.pos_id AND isActive='yes' AND (a.status!='employ' OR a.status IS NULL) ORDER BY lname");
    return $query->result();
  }
  public function getApplicantDetails($apid){
    $query = $this->db->query("SELECT a.*,b.*,TIMESTAMPDIFF(YEAR,birthday,now()) as age FROM tbl_applicant a,tbl_position b WHERE a.pos_id=b.pos_id AND isActive='yes' AND apid=$apid");
    return $query->row();
  }
  public function getPositionApplied(){
    $query = $this->db->query("SELECT pos_id as value,pos_details as text FROM tbl_position WHERE isHiring='Yes' ORDER BY pos_details ASC");
    return $query->result();
  }
  public function updateApplicantValue($apid,$name,$value){
    $data = array($name => $value);
    $this->db->where('apid', $apid);
    $this->db->update('tbl_applicant', $data);
    return $this->db->affected_rows();
  }
  public function getCharRef($apid,$name){
    $this->db->select($name);
    $this->db->where('apid',$apid);
    $this->db->from('tbl_applicant');
    return $this->db->get()->row();
  }
  public function failApplicant($apid){
    $data = array(
      'status' => 'not',
      'isActive' => 'No',
      'PF' => 'No'
    );
    $this->db->where('apid',$apid);
    $this->db->update('tbl_applicant',$data);
    return $this->db->affected_rows();
  }
  public function getDepartment($pos_id){
    $this->db->from('tbl_pos_dep');
    $this->db->where('pos_id',$pos_id);
    return $this->db->get()->row();
  }
  public function getPOsInDep($dep_id){
    $query = $this->db->query("SELECT c.posempstat_id as value, CONCAT(a.pos_details,' - ',d.status) as text FROM tbl_position a,tbl_pos_dep b,tbl_pos_emp_stat c,tbl_emp_stat d,tbl_rate e WHERE e.posempstat_id=c.posempstat_id AND a.pos_id=b.pos_id AND b.dep_id=$dep_id AND c.pos_id=a.pos_id AND c.empstat_id=d.empstat_id AND status!='Regular' AND class='Admin' AND e.isActive=1");
    return $query->result();
  }
  public function getCCAPosition(){
    $query = $this->db->query("SELECT c.posempstat_id as value, a.pos_details as text FROM tbl_position a,tbl_pos_emp_stat c,tbl_emp_stat d,tbl_rate e WHERE e.posempstat_id=c.posempstat_id  AND c.pos_id=a.pos_id AND c.empstat_id=d.empstat_id AND status='Trainee' AND class='Agent' AND e.isActive=1");
    return $query->result();
  }
  public function createEmployee($apid,$effectivedate){
    $data = array(
      'apid' => $apid,
      'isActive' => 'yes',
      'dateFrom' => $effectivedate,
      'dateStarted' => $effectivedate,
      'isAlarm' => 1,
      'confidential' => 0,
      'salarymode' => 'Monthly'
    );
    $this->db->insert('tbl_employee',$data);
    return $this->db->insert_id();
  }
  public function createEmpPromote($emp_id,$posempstat_id,$effectivedate){
    $data = array(
      'posempstat_id'=>$posempstat_id,
      'emp_id'=>$emp_id,
      'dateFrom'=>$effectivedate,
      'isActive'=>1,
      'date'=>date('Y-m-d')
    );
    $this->db->insert('tbl_emp_promote',$data);
    return $this->db->affected_rows();
  }

  public function getEmpDetailsWithDep($emp_id){
    $query = $this->db->query("SELECT DISTINCT(b.emp_id),a.fname,a.lname,g.dep_name FROM tbl_applicant as a,tbl_employee as b,tbl_emp_promote as c, tbl_pos_emp_stat as d,tbl_position as e,tbl_pos_dep as f,tbl_department as g WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND c.posempstat_id=d.posempstat_id AND d.pos_id=e.pos_id AND e.pos_id=f.pos_id AND f.dep_id=g.dep_id AND b.isActive='yes' AND d.isActive=1 AND c.isActive=1 AND b.emp_id=$emp_id");
    return $query->row();
  }
  public function createUser($emp_id,$fname,$lname,$dep_name){
    $data = array(
      'emp_id'=>$emp_id,
      'fname'=>$fname,
      'lname'=>$lname,
      'role'=>'Employee'.$dep_name,
      'role_id'=>3,
      'islock'=>1,
      'isActive'=>1,
      'settings'=>"bg-black font-inverse",
      'code'=>'eo'
    );
    $this->db->insert('tbl_user',$data);
    return $this->db->affected_rows();
  }
  public function employApplicant($apid){
    $data = array('isActive'=>'No','status'=>'employ','PF'=>'yez');
    $this->db->where('apid',$apid);
    $this->db->update('tbl_applicant',$data);
    return $this->db->affected_rows();
  }
}