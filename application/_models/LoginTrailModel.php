<?php

class LoginTrailModel extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getAllUsers() {
        $this->db->from('tbl_user');
        $this->db->order_by('lname', 'ASC');
        return $this->db->get()->result();
    }

    public function getAuditTrailLogsAll($from, $to) {
        $query = $this->db->query("SELECT * FROM tbl_login_logout_session WHERE log BETWEEN '$from' AND '$to' ORDER BY log");
        return $query->result();
    }

    public function getAuditTrailLogsUser($from, $to, $uid) {
        $query = $this->db->query("SELECT * FROM tbl_login_logout_session WHERE log BETWEEN '$from' AND '$to' AND uid=$uid ORDER BY log");
        return $query->result();
    }

}
