<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General_model.php";

class Szfive_model extends General_model
{
 private function getEmpStat($emp_id){
   return $this->general_model->fetch_specific_val("c.status as emp_stat","a.emp_id = $emp_id AND a.posempstat_id = b.posempstat_id AND b.empstat_id = c.empstat_id","tbl_emp_promote a,tbl_pos_emp_stat b,tbl_emp_stat c")->emp_stat; 
}
private function getSupervisor($emp_id){
   return $this->general_model->fetch_specific_val("a.fname,a.lname,a.email","a.apid=b.apid AND b.emp_id = $emp_id","tbl_applicant a,tbl_employee b"); 
}
public function datatable_survey_query($datatable)
{
    $query['query'] = "SELECT a.*, b.*
    FROM tbl_szfive_survey_details as a
    JOIN tbl_szfive_survey_questions as b ON b.questionId = a.questionId
    ";

    if (isset($datatable['query']['surveySearch'])) {

        $keyword = $datatable['query']['surveySearch'];

        $query['search']['append'] = " WHERE a.choiceLabels LIKE '%" . $keyword . "%' OR  b.question LIKE '%" . $keyword . "%' OR  a.surveyId LIKE '%" . $keyword . "%'";

        $query['search']['total'] = " WHERE a.choiceLabels LIKE '%" . $keyword . "%' OR  b.question LIKE '%" . $keyword . "%' OR  a.surveyId LIKE '%" . $keyword . "%'";
    }

    return $query;
}

public function get_highfive()
{
   //      $query['query'] = "SELECT a.id as hiFiveId, j.pos_name, j.pos_details, a.*, DATE_FORMAT(a.dateCreated, '%b %e') as date, DATE_FORMAT(a.dateCreated, '%M %e, %Y %r') as dateAnswered, a.dateCreated as dateSubmitted, CONCAT(e.fname, ' ', e.lname) as user_name, b.role as position, c.Supervisor, c.acc_id, c.apid, e.email, c.emp_stat, c.supEmail, c.isActive, b.emp_id, d.*, e.pic, c.Supervisor as supervisor, b.uid, l.link
			// FROM tbl_szfive_high_fives as a
			// JOIN tbl_user as b ON b.uid = a.senderId
			// JOIN tbl_employee as c ON c.emp_id = b.emp_id
		 //    LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
   //          LEFT JOIN tbl_applicant as e ON e.apid = c.apid
   //          -- LEFT JOIN tbl_user as f ON f.emp_id = a.recipientId
   //          LEFT JOIN tbl_emp_promote as h ON (h.emp_id = c.emp_id AND h.isActive = 1)
   //          LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id)
   //          LEFT JOIN tbl_position as j ON j.pos_id = i.pos_id
   //          -- LEFT JOIN tbl_szfive_high_five_likes as k ON k.hiFiveId = a.id
   //          LEFT JOIN tbl_system_notification as l ON l.systemNotification_ID = a.notificationId 
   //      ";
//--------------------------NICCA REVISION-----------------------------------------

    $query['query'] = "SELECT a.id as hiFiveId, j.pos_name, j.pos_details, a.*, DATE_FORMAT(a.dateCreated, '%b %e') as date, DATE_FORMAT(a.dateCreated, '%M %e, %Y %r') as dateAnswered, a.dateCreated as dateSubmitted, CONCAT(e.fname, ' ', e.lname) as user_name, b.role as position, c.Supervisor,(SELECT email FROM tbl_applicant y,tbl_employee z WHERE y.apid=z.apid AND z.emp_id = c.Supervisor) as supEmail, c.acc_id, c.apid, e.email, m.status as emp_stat,  c.isActive, b.emp_id, d.*, e.pic, c.Supervisor as supervisor, b.uid, l.link
    FROM tbl_szfive_high_fives as a
    JOIN tbl_user as b ON b.uid = a.senderId
    JOIN tbl_employee as c ON c.emp_id = b.emp_id
    LEFT JOIN tbl_account as d ON d.acc_id = c.acc_id
    LEFT JOIN tbl_applicant as e ON e.apid = c.apid
    LEFT JOIN tbl_emp_promote as h ON (h.emp_id = c.emp_id AND h.isActive = 1)
    LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id)
    LEFT JOIN tbl_position as j ON j.pos_id = i.pos_id
    LEFT JOIN tbl_emp_stat as m ON i.empstat_id = m.empstat_id
    LEFT JOIN tbl_system_notification as l ON l.systemNotification_ID = a.notificationId ";
    return $query;
}

public function user_mention($criteria)
{
    $query = "SELECT DISTINCT(a.uid) as id, CONCAT(c.fname, ' ', c.lname) as name, CONCAT('" . $criteria['img'] . "', c.pic) as avatar, a.uid as type
    FROM tbl_user as a
    JOIN tbl_employee as b ON b.emp_id = a.emp_id
    JOIN tbl_applicant as c ON c.apid = b.apid
    LEFT JOIN tbl_szfive_high_fives as d ON d.senderId = a.uid
    WHERE a.uid <> " . $criteria['uid'] . $criteria['append']." and b.isActive='yes'";

    return $query;
}

public function get_top_highfive($criteria)
{
    $img = $criteria['img'];
    $id = $criteria['id'];
    $table = $criteria['table'];

    $query = "SELECT t.uid, d.fname, d.lname, u.$id, CONCAT('$img', d.pic) as pic, j.pos_details, c.acc_description, c.acc_name, count(*) as count  FROM $table as u JOIN tbl_user as t ON t.uid = u.$id JOIN tbl_employee as b ON b.emp_id = t.emp_id JOIN tbl_account as c ON c.acc_id = b.acc_id JOIN tbl_applicant as d ON d.apid = b.apid LEFT JOIN tbl_emp_promote as h ON (h.emp_id = b.emp_id AND h.isActive = 1) LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id) LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id) ";

    if (isset($criteria['uid'])) {
        $query .= " WHERE u.$id = " . $criteria['uid'];
    }

    $query .= " GROUP BY $id ORDER BY count(*) DESC LIMIT 1";

    return $this->general_model->custom_query($query);
}

public function get_user_details($criteria)
{
    $img = base_url() . "assets/images/";

    $query = "SELECT a.uid as id, f.fname as firstname, f.lname as lastname, CONCAT(f.fname, ' ', f.lname) as name, CONCAT('$img', f.pic) as avatar, a.uid as type, b.Supervisor as supervisorId, j.pos_details, c.acc_description, k.dep_name, k.dep_details, c.acc_name, a.emp_id, f.email, (SELECT CONCAT(fname, ' ', lname) FROM tbl_applicant y,tbl_employee z WHERE y.apid=z.apid AND z.emp_id = b.Supervisor)  as supervisorName
    FROM tbl_user as a
    JOIN tbl_employee as b ON b.emp_id = a.emp_id
    JOIN tbl_account as c ON c.acc_id = b.acc_id
    JOIN tbl_applicant as f ON f.apid = b.apid
    LEFT JOIN tbl_emp_promote as h ON (h.emp_id = b.emp_id AND h.isActive = 1) 
    LEFT JOIN tbl_pos_emp_stat as i ON (i.posempstat_id = h.posempstat_id) 
    LEFT JOIN tbl_position as j ON (j.pos_id = i.pos_id)
    LEFT JOIN tbl_szfive_high_fives as d ON d.senderId = a.uid
    LEFT JOIN tbl_department as k ON k.dep_id = c.dep_id
    WHERE a.uid = " . $criteria['uid'] . " LIMIT 1";

    return $query;
}

public function get_review_details($criteria)
{
    $query[0] = "SELECT subTable.*, g.fname as reviewerFname, b.lname as reviewerLname, e.fname as answerFname, e.lname as answerLname 
    FROM (SELECT a.reviewId, a.isReviewed, a.answerId, a.reviewerId, a.dateUpdated as dateReviewed, b.dateCreated as dateAnswered, c.sched_date, TIME_TO_SEC(TIMEDIFF(a.dateUpdated, b.dateCreated)) as diff 
    FROM tbl_szfive_reviews as a
    JOIN tbl_szfive_survey_answers as b ON b.answerId = a.answerId
    LEFT JOIN tbl_schedule as c ON c.sched_id = b.sched_id 
    ORDER BY diff ".$criteria['sort'].") as subTable
    JOIN tbl_user as b ON b.uid = subTable.reviewerId 
    JOIN tbl_szfive_survey_answers as c ON c.answerId = subTable.answerId 
    LEFT JOIN tbl_schedule as d ON d.sched_id = c.sched_id 
    LEFT JOIN tbl_user as e ON e.uid = c.employeeId
    LEFT JOIN tbl_employee f ON e.emp_id = f.emp_id
    LEFT JOIN tbl_applicant g ON g.apid=f.apid";

    $query[1] = "SELECT a.answerId, a.isReviewed, a.reviewerId, g.fname as reviewerFname, g.lname as reviewerLname, a.dateUpdated as dateReviewed, 
    c.dateCreated as dateAnswered, c.sched_id, d.sched_date, g.fname as answerFname, g.lname as answerLname 
    FROM tbl_szfive_reviews as a 
    JOIN tbl_user as b ON b.uid = a.reviewerId 
    JOIN tbl_szfive_survey_answers as c ON c.answerId = a.answerId 
    LEFT JOIN tbl_schedule as d ON d.sched_id = c.sched_id 
    LEFT JOIN tbl_user as e ON e.uid = c.employeeId
    LEFT JOIN tbl_employee f ON f.emp_id = e.emp_id
    LEFT JOIN tbl_applicant g ON f.apid=g.apid";

    return $query[$criteria['num']];
}
}
