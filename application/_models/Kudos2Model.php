<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kudos2Model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  function get_kudos()
  {
    $query =  $this->db->query("SELECT tbl_kudos.kudos_id, tbl_kudos.emp_id, tbl_kudos.revision, tbl_kudos.revised_by, DATE_FORMAT(tbl_kudos.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS ambassador, UCASE(tbl_account.acc_name) AS campaign, tbl_kudos.acc_id, tbl_kudos.kudos_type, tbl_kudos.client_name, tbl_kudos.phone_number, tbl_kudos.client_email, tbl_kudos.kudos_card_pic, tbl_kudos.comment, tbl_kudos.request_note, tbl_kudos.requested_by, tbl_kudos.added_by, tbl_kudos.reward_type, tbl_kudos.proofreading, tbl_kudos.kudos_card, DATE_FORMAT(tbl_kudos.added_on,'%M %d %Y %h:%i %p') AS date_given, tbl_kudos.reward_status, tbl_kudos.file FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid");
    return $query->result();
    header('Content-Type: image/jpeg');
  }
  function get_accounts()
  {
    $query = $this->db->query("SELECT acc_id, UPPER(acc_name) as account FROM tbl_account WHERE acc_description LIKE '%Agent%' ORDER BY acc_name ASC");
    return $query->result();
  }

  function get_names()
  {
    $query = $this->db->query("SELECT tbl_employee.emp_id, tbl_account.acc_id, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS name FROM tbl_applicant INNER JOIN tbl_employee ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_account.acc_id=tbl_employee.acc_id WHERE tbl_account.acc_description LIKE '%Agent%' AND tbl_employee.isActive LIKE '%yes%'");
    return $query->result();

  }
  function another_getName()
  {
    $query = $this->db->query("SELECT tbl_employee.emp_id, tbl_account.acc_name, CONCAT(tbl_applicant.lname, ', ', tbl_applicant.fname) AS fullname FROM tbl_applicant INNER JOIN tbl_employee ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_employee.acc_id=tbl_account.acc_id ORDER BY tbl_applicant.fname ASC");
    return $query->result();
  }
  function get_kudos_count()
  {
    date_default_timezone_set('Asia/Manila');
    $year=date("Y");
    $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE YEAR(added_on)='".$year."' GROUP BY month ORDER BY month_number ASC");
    return $query->result();
  }
  function get_kudos_top()
  {
    date_default_timezone_set('Asia/Manila');
    $month=date("m");
    $year=date("Y");
    $query = $this->db->query("SELECT DATE_FORMAT(added_on, '%m') AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE MONTH(added_on)='".$month."' AND YEAR(added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC LIMIT 5");
    return $query->result();
  }
  function get_kudos_all_agents()
  {
    date_default_timezone_set('Asia/Manila');
    $month=date("m");
    $year=date("Y");
    $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid WHERE MONTH(added_on)='".$month."' AND YEAR(added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC");
    return $query->result();
  }

  function get_kudos_top_campaigns(){
    date_default_timezone_set('Asia/Manila');
    $month=date("m");
    $year=date("Y");
    $query = $this->db->query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY campaign ORDER BY kudos_count DESC LIMIT 5");
    return $query->result();
  }
  function get_kudos_by_campaigns(){
    date_default_timezone_set('Asia/Manila');
    $month=date("m");
    $year=date("Y");
    $query = $this->db->query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, UPPER(tbl_account.acc_name) AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY campaign ORDER BY kudos_count DESC");
    return $query->result();
  }

  function get_kudos_drilldown(){
    date_default_timezone_set('Asia/Manila');
    $month=date("m");
    $year=date("Y");
    $query = $this->db->query("SELECT COUNT(*) AS kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY ambassador ORDER BY kudos_count DESC");
    return $query->result();
  }

  function get_kudos_drilldown2(){
    date_default_timezone_set('Asia/Manila');
    $month=date("m");
    $year=date("Y");
    $query = $this->db->query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, tbl_account.acc_name AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid GROUP BY month, ambassador ORDER BY kudos_count ASC");
    return $query->result();
  }

  function get_kudos_drilldown3(){
    date_default_timezone_set('Asia/Manila');
    $month=date("m");
    $year=date("Y");
    $query = $this->db->query("SELECT COUNT(*) as kudos_count, DATE_FORMAT(tbl_kudos.added_on, '%m') AS month_number, UPPER(tbl_account.acc_name) AS campaign, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month FROM tbl_kudos INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id INNER JOIN tbl_employee ON tbl_employee.emp_id=tbl_kudos.emp_id INNER JOIN tbl_applicant ON tbl_applicant.apid=tbl_employee.apid WHERE MONTH(tbl_kudos.added_on)='".$month."' AND YEAR(tbl_kudos.added_on)='".$year."' GROUP BY month, ambassador ORDER BY kudos_count ASC");
    return $query->result();
  }
  function getDateRange($startDate,$endDate){
    $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
    $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
    $startTime = '00:00:00';
    $endTime = '23:59:59';
    $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count FROM tbl_kudos WHERE added_on >= '".$startDate1." ".$startTime."' AND added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");
    return $query->result();
  }
  function getDateRange2($startDate,$endDate,$campaign){
    $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
    $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
    $campaign1 =mysqli_real_escape_string($this->db->conn_id,trim($campaign));
    $startTime = '00:00:00';
    $endTime = '23:59:59';
    $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(tbl_kudos.added_on, '%m'), ' ',DATE_FORMAT(tbl_kudos.added_on, '%Y')) AS month_number, CONCAT(DATE_FORMAT(added_on, '%M'), ' ',DATE_FORMAT(added_on, '%Y')) AS month, COUNT(*) as kudos_count, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_account ON tbl_account.acc_id=tbl_kudos.acc_id WHERE tbl_kudos.acc_id='".$campaign1."' AND tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY month ORDER BY month_number ASC");
    return $query->result();
  }
  function getDateRange3($startDate,$endDate){
    $startDate1 =mysqli_real_escape_string($this->db->conn_id,trim($startDate));
    $endDate1 =mysqli_real_escape_string($this->db->conn_id,trim($endDate));
    $startTime = '00:00:00';
    $endTime = '23:59:59';
    $query = $this->db->query("SELECT CONCAT(DATE_FORMAT(added_on, '%m'), ' ',DATE_FORMAT(added_on, '%Y')) AS month_number, COUNT(*) AS kudos_count, CONCAT (tbl_applicant.fname, ' ', tbl_applicant.lname) AS ambassador, tbl_account.acc_name AS campaign FROM tbl_kudos INNER JOIN tbl_employee ON tbl_kudos.emp_id=tbl_employee.emp_id INNER JOIN tbl_applicant ON tbl_employee.apid=tbl_applicant.apid INNER JOIN tbl_account ON tbl_kudos.acc_id=tbl_account.acc_id WHERE tbl_kudos.added_on >= '".$startDate1." ".$startTime."' AND tbl_kudos.added_on <= '".$endDate1." ".$endTime."' GROUP BY ambassador ORDER BY kudos_count DESC");
    return $query->result();
  }
  //-----------------CODES NICCA--------------------
  public function getKudos($kudos_id){
    $query = $this->db->query("SELECT b.pic,b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file, h.pos_details, i.status FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d, tbl_employee e, tbl_emp_promote f, tbl_pos_emp_stat g, tbl_position h, tbl_emp_stat i WHERE i.empstat_id=g.empstat_id AND h.pos_id=g.pos_id AND f.posempstat_id=g.posempstat_id AND f.emp_id=e.emp_id AND a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND f.isActive=1 AND a.kudos_id=$kudos_id AND f.emp_id=c.emp_id ORDER BY a.added_on DESC");
    return $query->row();
  }

  public function getKudosAll(){
    $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id ORDER BY a.added_on DESC");
    return $query->result();
  }
  public function getKudosCard(){
    $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND a.proofreading='Done' AND a.kudos_card!='Done' ORDER BY a.added_on DESC");
    return $query->result();
  }
  public function submitrequest($kudos_id,$note,$requested_by){
    $arr = array('request_note'=>$note,'requested_on'=>date("Y-m-d H:i:s"),'requested_by'=>$requested_by,'reward_status'=>'Requested');
    $this->db->where('kudos_id',$kudos_id);
    $this->db->update('tbl_kudos',$arr);
    return $this->db->affected_rows();
  }
  public function getEmployeeDetails($uid){
    $query = $this->db->query("SELECT * FROM tbl_applicant a,tbl_employee b, tbl_user c WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND c.uid=$uid");
    return $query->row();
  }
  public function uploadkudoscard($file_sc,$kudosid){
    $values = array('kudos_card'=>'Done','kudos_card_pic'=>$file_sc);
    $this->db->where('kudos_id',$kudosid);
    $this->db->update('tbl_kudos',$values);
    return $this->db->affected_rows();
  }
  public function uploadscreenshot($file_sc,$kudosid){
    $values = array('file'=>$file_sc);
    $this->db->where('kudos_id',$kudosid);
    $this->db->update('tbl_kudos',$values);
    return $this->db->affected_rows();
  }

  public function getKudosRequest(){
    $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND a.reward_status='Pending' ORDER BY a.added_on DESC");
    return $query->result();
  }

  public function getKudosAdd(){
    $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by , a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND a.reward_status!='Released' AND a.kudos_card!='Done' AND a.proofreading!='Done' ORDER BY a.added_on DESC");
    return $query->result();
  }
  public function getEmployeeAgent($empid){
   $query = $this->db->query("SELECT tbl_account.acc_id, tbl_account.acc_name, tbl_applicant.fname, tbl_applicant.lname, tbl_employee.id_num, tbl_employee.emp_id FROM tbl_applicant  INNER JOIN tbl_employee ON tbl_applicant.apid=tbl_employee.apid INNER JOIN tbl_account ON tbl_account.acc_id=tbl_employee.acc_id WHERE tbl_employee.id_num='".$empid."' AND tbl_account.acc_description='Agent'");
   return $query->result();
 }
 public function createkudos($emp_id,$acc_id,$k_type,$r_type,$c_name,$c_pnumber,$c_email,$c_comment,$uid,$d_given,$via){
  $arr = array(
    'emp_id'=> $emp_id,
    'acc_id'=> $acc_id,
    'kudos_type'=> $k_type,
    'client_name'=> $c_name,
    'phone_number'=> $c_pnumber,
    'client_email'=> $c_email,
    'comment'=> $c_comment,
    'added_by'=> $uid,
    'reward_type'=> $r_type,
    'proofreading'=> 'Pending',
    'kudos_card'=> 'Pending',
    'reward_status' => 'Pending',
    'added_on' =>  $d_given,
    'added_via' => $via
  );
  $this->db->insert('tbl_kudos',$arr);
  return $this->db->insert_id();
}
public function getAllAccounts(){
  $this->db->from('tbl_account');
  $this->db->where('acc_description','Agent');
  $this->db->order_by("acc_name", "asc");
  return $this->db->get()->result();
}
public function getEmpInAccounts($acc_id){
 $query = $this->db->query("SELECT lname,fname,emp_id FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' AND b.acc_id=$acc_id ORDER BY lname ASC");
 return $query->result();
}
public function updatekudos($kudosid,$emp_id,$acc_id,$k_type,$r_type,$c_name,$c_pnumber,$c_email,$c_comment,$updated_by,$dateupdated){
  $arr = array(
    'emp_id'=> $emp_id,
    'acc_id'=> $acc_id,
    'kudos_type'=> $k_type,
    'client_name'=> $c_name,
    'phone_number'=> $c_pnumber,
    'client_email'=> $c_email,
    'comment'=> $c_comment,
    'updated_by'=> $updated_by,
    'updated_on'=> $dateupdated,
    'reward_type'=> $r_type
  );
  $this->db->where('kudos_id',$kudosid);
  $this->db->update('tbl_kudos',$arr);
  return $this->db->affected_rows();
}
public function getKudosProofread(){
  $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND a.proofreading='Pending' ORDER BY a.added_on DESC");
  return $query->result();
}
public function submitrevision($kudos_id,$revision,$revised_by){
  $arr = array('revision'=>$revision,'revised_on'=>date("Y-m-d H:i:s"),'revised_by'=>$revised_by,'proofreading'=>'Done');
  $this->db->where('kudos_id',$kudos_id);
  $this->db->update('tbl_kudos',$arr);
  return $this->db->affected_rows();
}
public function getKudosApprove(){
  $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND a.reward_status='Requested' ORDER BY a.added_on DESC");
  return $query->result();
}
public function approvekudos($kudos_id,$name){
  $this->db->where('kudos_id',$kudos_id);
  $this->db->update('tbl_kudos',array('reward_status'=>'Approved','approved_by'=>$name,'approved_on'=>date("Y-m-d H:i:s")));
  return $this->db->affected_rows();
}
public function rejectkudos($kudos_id,$rejectnote,$name){
  $this->db->where('kudos_id',$kudos_id);
  $this->db->update('tbl_kudos',array('reward_status'=>'Rejected','reject_note'=>$rejectnote,'approved_by'=>$name,'approved_on'=>date("Y-m-d H:i:s")));
  return $this->db->affected_rows();
}
public function getKudosCustom($startdate,$enddate){
  $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND date(a.added_on) BETWEEN date('$startdate') AND date('$enddate') ORDER BY a.added_on DESC");
  return $query->result();
}
public function getKudosRelease(){
  $query = $this->db->query("SELECT b.lname,b.fname,d.acc_name,a.kudos_id, a.emp_id, a.revision, a.revised_by, DATE_FORMAT(a.revised_on,'%M %d %Y %h:%i %p') AS date_revised, CONCAT(b.lname, ', ', b.fname) AS ambassador, UCASE(d.acc_name) AS campaign, a.acc_id, a.kudos_type, a.client_name, a.phone_number, a.client_email, a.kudos_card_pic, a.comment, a.request_note, a.requested_by, a.added_by, a.reward_type, a.proofreading, a.kudos_card, DATE_FORMAT(a.added_on,'%M %d %Y %h:%i %p') AS date_given, a.reward_status, a.file FROM tbl_kudos a,tbl_applicant b,tbl_employee c,tbl_account d WHERE a.emp_id=c.emp_id AND b.apid=c.apid AND d.acc_id=a.acc_id AND (a.reward_status='Approved' OR a.reward_status='On Hold') ORDER BY a.added_on DESC");
  return $query->result();
}
public function releaseupdatekudos($kudos_id,$status){
  $this->db->where('kudos_id',$kudos_id);
  $this->db->update('tbl_kudos',array('reward_status'=>$status));
  return $this->db->affected_rows();
}
public function getrequestors(){
  $query = $this->db->query("SELECT a.ksetting_id,a.role,d.fname,d.lname,c.emp_id,e.pos_details,i.dep_name,i.dep_details,h.status FROM tbl_kudos_notify a,tbl_user b,tbl_employee c, tbl_applicant d, tbl_position e,tbl_emp_promote f,tbl_pos_emp_stat g,tbl_emp_stat h,tbl_department i,tbl_pos_dep j WHERE b.emp_id=c.emp_id AND c.apid=d.apid AND a.uid=b.uid AND a.role = 'Request' AND a.isActive=1 AND c.isActive='yes' AND c.emp_id=f.emp_id AND f.posempstat_id=g.posempstat_id AND g.pos_id=e.pos_id AND g.empstat_id=h.empstat_id AND f.isActive=1 AND e.pos_id=j.pos_id AND j.dep_id=i.dep_id ORDER BY lname ASC");
  return $query->result();
}
public function getreleasers(){
  $query = $this->db->query("SELECT a.ksetting_id,a.role,d.fname,d.lname,c.emp_id,e.pos_details,i.dep_name,i.dep_details,h.status FROM tbl_kudos_notify a,tbl_user b,tbl_employee c, tbl_applicant d, tbl_position e,tbl_emp_promote f,tbl_pos_emp_stat g,tbl_emp_stat h,tbl_department i,tbl_pos_dep j WHERE b.emp_id=c.emp_id AND c.apid=d.apid AND a.uid=b.uid AND a.role = 'Release' AND a.isActive=1 AND c.isActive='yes' AND c.emp_id=f.emp_id AND f.posempstat_id=g.posempstat_id AND g.pos_id=e.pos_id AND g.empstat_id=h.empstat_id AND f.isActive=1 AND e.pos_id=j.pos_id AND j.dep_id=i.dep_id ORDER BY lname ASC");
  return $query->result();
}
public function getapprovers(){
  $query = $this->db->query("SELECT a.ksetting_id,a.role,d.fname,d.lname,c.emp_id,e.pos_details,i.dep_name,i.dep_details,h.status FROM tbl_kudos_notify a,tbl_user b,tbl_employee c, tbl_applicant d, tbl_position e,tbl_emp_promote f,tbl_pos_emp_stat g,tbl_emp_stat h,tbl_department i,tbl_pos_dep j WHERE b.emp_id=c.emp_id AND c.apid=d.apid AND a.uid=b.uid AND a.role = 'Approve' AND a.isActive=1 AND c.isActive='yes' AND c.emp_id=f.emp_id AND f.posempstat_id=g.posempstat_id AND g.pos_id=e.pos_id AND g.empstat_id=h.empstat_id AND f.isActive=1 AND e.pos_id=j.pos_id AND j.dep_id=i.dep_id ORDER BY lname ASC");
  return $query->result();
}
public function updateActiveNotify($ksetting_id,$active){
  $this->db->where('ksetting_id',$ksetting_id);
  $this->db->update('tbl_kudos_notify',array('isActive'=>$active));
  return $this->db->affected_rows();
}
public function getDepartments(){
  $this->db->from('tbl_department');
  $this->db->order_by('dep_details','ASC');
  return $this->db->get()->result();
}
public function getEmpInDept($dep_id){
  $query = $this->db->query("SELECT a.fname,a.lname,h.uid FROM tbl_applicant a,tbl_employee b, tbl_emp_promote c, tbl_pos_emp_stat d, tbl_position e, tbl_pos_dep f, tbl_department g, tbl_user h WHERE a.apid=b.apid AND b.emp_id=c.emp_id AND c.posempstat_id=d.posempstat_id AND d.pos_id=e.pos_id AND e.pos_id=f.pos_id AND f.dep_id=g.dep_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND c.isActive=1 AND d.isActive=1 AND g.dep_id=$dep_id AND e.class='Admin' ORDER BY lname ASC");
  return $query->result();
}
public function getNotifyPerRole($role){
  $this->db->where(array('role'=>$role,'isActive'=>1));
  $this->db->from('tbl_kudos_notify');
  return $this->db->get()->result();
}
public function getKudosNotify($uid,$role){
  $this->db->where(array('role'=>$role,'uid'=>$uid));
  $this->db->from('tbl_kudos_notify');
  return $this->db->get()->row();
}
public function insertKudosNotify($uid,$role){
  $data = array(
    'role'=>$role,
    'uid'=>$uid,
    'isActive'=>1
  );
  $this->db->insert('tbl_kudos_notify',$data);
  return $this->db->affected_rows();
}
public function updateKudosNotify($ksetting_id){
  $this->db->where('ksetting_id',$ksetting_id);
  $this->db->update('tbl_kudos_notify',array('isActive'=>1));
  return $this->db->affected_rows();
}
}
