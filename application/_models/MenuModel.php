<?php

class MenuModel extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function fetchMenu()
        {
                $query =  $this->db->query('SELECT * from tbl_menu_tab_item a,tbl_menu_items b where b.tab_id=a.tab_id order by tab_name ASC');
                return $query->result();
        }
		public function addMenu($desc,$link,$tab)
        { 
		$query =  $this->db->query("insert into tbl_menu_items(item_title,item_link,tab_id,is_active) value('".$desc."','".$link."',".$tab.",0)");
		}
		public function updateMenu($desc,$link,$tab,$isactib,$menuID)
        { 
		$query =  $this->db->query("update tbl_menu_items set item_title='".$desc."',item_link='".$link."',tab_id=".$tab.",is_active=".$isactib." where menu_item_id=".$menuID." ");
		}
		public function fetchMenuTabs(){ 
		$query =  $this->db->query("SELECT * FROM tbl_menu_tab_item");
		return $query->result();

		}
	 

}