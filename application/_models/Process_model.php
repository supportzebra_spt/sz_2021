<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once dirname(__FILE__) . "/General_model.php";

class Process_model extends General_model
{
	public function get_allfolders($word=null){

		$append = ($word!=null) ? " AND folder.folderName like '%$word%'" : "";

		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' $append";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextallfolders($lastid,$str=null){
		$append = ($str!=null) ? " AND folder.folderName like '%$str%'" : "";

		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=app.apid"
		. " AND folder.referenceFolder='1'"
		. " AND folder.folder_ID!='1'"
		. " AND folder.folder_ID >$lastid $append";
		$this->db->limit(6);
		$this->db->order_by("folder.folder_ID", "asc");
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allprocess($word=null){
		$append = ($word!=null) ? " AND process.processTitle like '%$word%'" : "";
		$this->db->select('process.*, app.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where="process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID $append";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextallprocesses($lastid,$str=null){
		$append = ($str!=null) ? " AND process.processTitle like '%$str%'" : "";
		$this->db->select('process.*, app.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where="process.createdBy=user.uid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=app.apid"
		. " AND process.folder_ID=folder.folder_ID"
		. " AND process.process_ID >$lastid $append";
		$this->db->limit(6);
		$this->db->order_by("process.process_ID", "asc");
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_allchecklist(){
		$this->db->select('*');
		$this->db->from('tbl_processST_checklist');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_userinfo($userid){
		$this->db->select('applicant.fname,applicant.mname,applicant.lname');
		$this->db->from('tbl_applicant applicant, tbl_employee employee, tbl_user user');
		$where="user.uid=$userid"
		. " AND user.emp_id=employee.emp_id"
		. " AND employee.apid=applicant.apid";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_process($folder_ID){
		$this->db->select('process.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_processST_folder folder');
		$where="process.folder_ID=$folder_ID"
		. " AND folder.folder_ID=$folder_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_subfolders($folder_ID){
		$this->db->select('folder.*,app.*');
		$this->db->from('tbl_processST_folder folder,tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.referenceFolder=$folder_ID"
		. " AND user.emp_id=employee.emp_id" 
		. " AND employee.apid=app.apid"
		. " AND folder.createdBy=user.uid";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_answers($task_ID,$checklist_ID){
		$this->db->select('*');
		$this->db->from('tbl_processST_subtask subtask, tbl_processST_component_subtask compsub,tbl_processST_answer ans');
		$where="subtask.task_ID=$task_ID"
		. " AND subtask.subTask_ID=compsub.subTask_ID"
		. " AND ans.subtask_ID=subtask.subTask_ID"
		. " AND ans.checklist_ID=$checklist_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();		
	}
	public function get_task($process_ID){
		$this->db->select('task.*,process.*');
		$this->db->from('tbl_processST_task task, tbl_processST_process process');
		$where="task.process_ID=$process_ID"
		. " AND task.process_ID=process.process_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_answerabletask($checklist_ID){
		$this->db->select('task.*,checkStatus.*');
		$this->db->from('tbl_processST_task task, tbl_processST_checklistStatus checkStatus');
		$where="task.task_ID=checkStatus.task_ID"
		. " AND checkStatus.checklist_ID=$checklist_ID";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_checklist($process_ID){
		$this->db->select('checklist.*');
		$this->db->from('tbl_processST_checklist checklist');
		$where="checklist.process_ID=$process_ID";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_nextallchecklist($process_ID,$lastid){
		$this->db->select('checklist.*');
		$this->db->from('tbl_processST_checklist checklist');
		$where="checklist.process_ID=$process_ID"
		. " AND checklist.checklist_ID > $lastid";
		$this->db->limit(6);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_checklistStat(){
		$this->db->select('*');
		$this->db->from('tbl_processST_checklistStatus');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_singlefolderinfo($folder_ID){
		$result = $this->db->get_where('tbl_processST_folder', array( 
			'folder_ID'=>$folder_ID));
		return $result->row();
	}
	public function get_singleprocessinfo($process_ID){
		$result = $this->db->get_where('tbl_processST_process', array( 
			'process_ID'=>$process_ID));
		return $result->row();
	}
	public function get_singlechecklistinfo($checklist_ID){
		$result = $this->db->get_where('tbl_processST_checklist', array( 
			'checklist_ID'=>$checklist_ID));
		return $result->row();
	}
	public function get_singleCheckstatinfo($checkliststat_ID){
		$this->db->from('tbl_processST_checklistStatus');
		$this->db->where(array('checklistStatus_ID'=>$checkliststat_ID));
		return $this->db->get()->row_array();
	}
	public function get_checklistStatusID($checklist_ID,$task_ID){
		$this->db->select('check.checklistStatus_ID');
		$this->db->from('tbl_processST_checklistStatus check');
		$where="check.task_ID=$task_ID"
		. " AND check.checklist_ID=$checklist_ID";
		$this->db->where($where);
		return $this->db->get('tbl_processST_checklistStatus');
	}
	public function get_empid($uid){
		$this->db->select('user.emp_id');
		$this->db->from('tbl_user user');
		$where="user.uid=$uid";
		$this->db->where($where);
		return $this->db->get('tbl_user');
	}
	public function get_answerid($subtask,$checklist){
		$this->db->select('ans.answer_ID');
		$this->db->from('tbl_processST_answer ans');
		$where="ans.subtask_ID=$subtask"
		. " AND ans.checklist_ID=$checklist";
		$this->db->where($where);
		return $this->db->get('tbl_processST_answer');
	}
	public function get_deleteanswerid($checklist,$answer){
		$this->db->select('ans.answer_ID');
		$this->db->from('tbl_processST_answer ans');
		$where="ans.checklist_ID=$checklist"
		. " AND ans.answer=$answer";
		$this->db->where($where);
		return $this->db->get('tbl_processST_answer');
	}
	//
	public function get_dropdowninfo($stat){
		$this->db->select('folder.*, app.*');
		$this->db->from('tbl_processST_folder folder, tbl_user user, tbl_applicant app, tbl_employee employee');
		$where="folder.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND folder.referenceFolder='1' AND folder.folder_ID!='1' AND folder.status_ID=$stat";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_dropdownproinfo($stat){
		$this->db->select('process.*, app.*,folder.*');
		$this->db->from('tbl_processST_process process, tbl_user user, tbl_processST_folder folder,tbl_applicant app, tbl_employee employee');
		$where="process.createdBy=user.uid AND user.emp_id=employee.emp_id AND employee.apid=app.apid AND process.folder_ID=folder.folder_ID AND process.processstatus_ID=$stat";
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function add_folder($data){
		$this->db->insert('tbl_processST_folder', $data);
	}
	public function add_checklist($data){
		$this->db->insert('tbl_processST_checklist', $data);
	}
	public function add_processfolderM($data){
		$this->db->insert('tbl_processST_process', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	public function add_anotherchecklist($data){
		$this->db->insert('tbl_processST_checklist', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	public function add_task($data){
		$this->db->insert('tbl_processST_task', $data);
	}
	public function add_anothertask($data){
		$this->db->insert('tbl_processST_task', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	public function add_checklistStatus($data){
		$this->db->insert('tbl_processST_checklistStatus', $data);
	}
	public function addbatch_checkstatus($data,$table){
		$this->db->trans_start();
		$this->db->insert_batch($table, $data);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	public function delete_answer($id){
		$where="answer_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_answer');
	}
	public function delete_process($id){
		$where="folder_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_process');
	}
	public function delete_option($id){
		$where="componentSubtask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_component_subtask');
	}
	public function delete_compsubtask($id){
		$where="subTask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_component_subtask');
	}
	public function delete_subtask($id){
		$where="subTask_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_subtask');
	}
	public function delete_checklist($id){
		$where="process_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_checklist');      
	}
	public function delete_folder($id){
		$where="folder_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_folder');
	}
	public function delete_task($id){
		$where="task_ID=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_task');
	}
	public function delete_subfolder($id){
		$where="referenceFolder=$id";
		$this->db->where($where);
		$this->db->delete('tbl_processST_folder');
	}
	public function fetch_update($id){
		$this->db->from('tbl_processST_folder');
		$this->db->where(array('folder_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function fetchprocess_update($id){
		$this->db->from('tbl_processST_process');
		$this->db->where(array('process_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function fetchchecklist_update($id){
		$this->db->from('tbl_processST_checklist');
		$this->db->where(array('checklist_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function fetchtask_update($id){
		$this->db->from('tbl_processST_task');
		$this->db->where(array('task_ID'=> $id));
		return $this->db->get()->row_array();
	}
	public function updatefolder_process($id,$data){
		$this->db->where('folder_ID',$id);
		$this->db->update('tbl_processST_folder',$data);
	}
	public function update_process($id,$data){
		$this->db->where('process_ID',$id);
		$this->db->update('tbl_processST_process',$data);
	}
	public function update_checklist($id,$data){
		$this->db->where('checklist_ID',$id);
		$this->db->update('tbl_processST_checklist',$data);
	}
	public function update_taskdetails($id,$data){
		$this->db->where('task_ID',$id);
		$this->db->update('tbl_processST_task',$data);
	}
	public function update_taskStatus($id,$data){
		$this->db->where('checklistStatus_ID',$id);
		$this->db->update('tbl_processST_checklistStatus',$data);
	}
	public function update_taskAllStatus($id,$data){
		$this->db->where('checklist_ID',$id);
		$this->db->update('tbl_processST_checklistStatus',$data);
	}
	public function update_checklistdetails($id,$data){
		$this->db->where('checklist_ID',$id);
		$this->db->update('tbl_processST_checklist',$data);
	}
	public function update_subtaskdetails($id,$data){
		$this->db->where('subTask_ID',$id);
		$this->db->update('tbl_processST_subtask',$data);
	}
	public function update_taskduedate($id,$data){
		$this->db->where('checklistStatus_ID',$id);
		$this->db->update('tbl_processST_checklistStatus',$data);
	}
	public function update_compsubtaskdetails($id,$data){
		$this->db->where('componentSubtask_ID',$id);
		$this->db->update('tbl_processST_component_subtask',$data);
	}
	public function update_answerdetails($ansid,$data){
		$this->db->where('answer_ID',$ansid);
		$this->db->update('tbl_processST_answer',$data);
	}
	public function update_inputformdetails($id,$data){
		$this->db->where('subTask_ID',$id);
		$this->db->update('tbl_processST_subtask',$data);
	}
	public function count_checklistStatus($id){
		$this->db->where(array('checklist_ID'=>$id));
		$this->db->from('tbl_processST_checklistStatus');
		return $this->db->count_all_results();
	}
/*	public function search_folder($data){
		$this->db->select('*');
		$this->db->from('tbl_processST_folder');
		$this->db->like('folderName',$data);
		return $this->db->get()->row_array();
	}*/
	public function count_folders($lastid){
		$this->db->where(array('folder_ID',$lastid));
		$this->db->from('tbl_processST_folder');
		return $this->db->count_all_results();
	}
	public function count_all_folders(){
		$this->db->where(array('referenceFolder'=>'1'));
		$this->db->from('tbl_processST_folder');
		return $this->db->count_all_results();
	}
	public function count_processtemp($lastid){
		$this->db->where(array('process_ID',$lastid));
		$this->db->from('tbl_processST_process');
		return $this->db->count_all_results();
	}
	public function count_checklist($lastid){
		$this->db->where(array('checklist_ID',$lastid));
		$this->db->from('tbl_processST_checklist ');
		return $this->db->count_all_results();
	}
	public function countallforms($id){
		$this->db->where(array('task_ID',$id));
		$this->db->from('tbl_processST_subtask');
		return $this->db->count_all_results();
	}
	public function check_answerexist($subtaskid,$checklistid){
		$result = $this->db->get_where('tbl_processST_answer', array( 
			'subtask_ID'=>$subtaskid, 'checklist_ID'=>$checklistid));
		$count = $result->num_rows();
		return $count;
	}
}