<?php

class PayrollModel extends CI_Model {

  public function __construct()
  {
                // Call the CI_Model constructor
    parent::__construct();
  }
  public function getPhilhealthDates(){
    $query = $this->db->query("SELECT p_effec_id,DATE_FORMAT(effectivity_date,'%M %d, %Y') as effectivity_date FROM tbl_philhealth_effectivity as pe WHERE pe.p_effec_id NOT IN (SELECT DISTINCT tbl_philhealth.p_effec_id FROM tbl_philhealth) ORDER BY effectivity_date");
    return $query->result();
  }
  public function savePhilhealth($values){
    $this->db->insert('tbl_philhealth', $values);
  }
  public function getActivePhilhealth(){
    $query = $this->db->query("SELECT p.* FROM tbl_philhealth_effectivity as pe, tbl_philhealth as p WHERE pe.p_effec_id=p.p_effec_id AND pe.isActive = 1");
    return $query->result();
  }
  public function getSelectedPhilhealth($id){
    $query = $this->db->query("SELECT p.* FROM tbl_philhealth as p,tbl_philhealth_effectivity as pe WHERE pe.p_effec_id = p.p_effec_id AND pe.p_effec_id=$id");
    return $query->result();
  }
  public function getPhilhealthUsedDates(){
    $query = $this->db->query("SELECT p_effec_id,DATE_FORMAT(effectivity_date,'%M %d, %Y') as effectivity_date,isActive FROM tbl_philhealth_effectivity as pe WHERE pe.p_effec_id IN (SELECT DISTINCT tbl_philhealth.p_effec_id FROM tbl_philhealth) ORDER BY effectivity_date");
    return $query->result();
  }
  public function getTaxType(){
    $this->db->from('tbl_tax_type');
    return $this->db->get()->result();
  }
  public function saveTax($descid,$salary,$deducid){
    $values = array(
      'tax_desc_id' => $descid,
      'salarybase' => $salary,
      'deduction_id' => $deducid
    );
    $this->db->insert('tbl_tax', $values);
  }
  public function getTaxActive(){
    $query = $this->db->query("SELECT tdesc.description,GROUP_CONCAT(salarybase) as salarybase,GROUP_CONCAT(deduction)as deduction,GROUP_CONCAT(col)as col,GROUP_CONCAT(concat(round(percent*100),'%') ORDER BY percent ASC) as percent,rate,tt.description as type, te.t_effec_id FROM tbl_tax as t2,tbl_tax_deduction as td,tbl_tax_percent as tp, tbl_tax_description as tdesc,tbl_tax_type as tt,tbl_tax_effectivity as te WHERE t2.deduction_id=td.deduction_id AND td.percent_id=tp.percent_id AND t2.tax_desc_id=tdesc.tax_desc_id AND tt.type_id=td.type_id AND td.t_effec_id = te.t_effec_id AND te.isActive = 1 group by tdesc.description ORDER BY tax_id");
    return $query->result();
  }
  public function getTaxDates(){
    $query = $this->db->query("SELECT t_effec_id,DATE_FORMAT(effectivity_date,'%M %d, %Y') as effectivity_date FROM tbl_tax_effectivity as te WHERE te.t_effec_id NOT IN (SELECT DISTINCT t_effec_id FROM tbl_tax_deduction) ORDER BY effectivity_date");
    return $query->result();
  }
  public function getTaxUsedDates(){
    $query = $this->db->query("SELECT t_effec_id,DATE_FORMAT(effectivity_date,'%M %d, %Y') as effectivity_date,isActive FROM tbl_tax_effectivity as te WHERE te.t_effec_id IN (SELECT DISTINCT t_effec_id FROM tbl_tax_deduction) ORDER BY effectivity_date");
    return $query->result();
  }
  public function getDeductions(){

   $query = $this->db->query('SELECT deduction FROM tbl_tax_deduction LIMIT 8');
   return $query->result();
 }
 public function saveDeduction($col,$deduction,$percentid,$typeid,$dateid){
  $values = array(
    'col' => $col,
    'deduction' => $deduction,
    'percent_id' => $percentid,
    'type_id' => $typeid,
    't_effec_id' => $dateid
  );

  $this->db->insert('tbl_tax_deduction', $values);
  return $this->db->insert_id();
}
public function getSelectedTax($id){
 $query = $this->db->query("SELECT tdesc.description,GROUP_CONCAT(salarybase) as salarybase,GROUP_CONCAT(deduction)as deduction,GROUP_CONCAT(col)as col,GROUP_CONCAT(concat(round(percent*100),'%') ORDER BY percent ASC) as percent,rate,tt.description as type, te.t_effec_id FROM tbl_tax as t2,tbl_tax_deduction as td,tbl_tax_percent as tp, tbl_tax_description as tdesc,tbl_tax_type as tt,tbl_tax_effectivity as te WHERE t2.deduction_id=td.deduction_id AND td.percent_id=tp.percent_id AND t2.tax_desc_id=tdesc.tax_desc_id AND tt.type_id=td.type_id AND td.t_effec_id = te.t_effec_id AND te.t_effec_id = $id group by tdesc.description ORDER BY tax_id");
 return $query->result();
}

public function getSSSDates(){
  $query = $this->db->query("SELECT sss_effec_id,DATE_FORMAT(effectivity_date,'%M %d, %Y') as effectivity_date FROM tbl_sss_effectivity as se WHERE se.sss_effec_id NOT IN (SELECT DISTINCT tbl_sss.sss_effec_id FROM tbl_sss) ORDER BY effectivity_date");
  return $query->result();
}
public function saveSSS($values){
  $this->db->insert('tbl_sss', $values);
}
public function getSSSUsedDates(){
  $query = $this->db->query("SELECT sss_effec_id,DATE_FORMAT(effectivity_date,'%M %d, %Y') as effectivity_date,isActive FROM tbl_sss_effectivity as se WHERE se.sss_effec_id IN (SELECT DISTINCT tbl_sss.sss_effec_id FROM tbl_sss) ORDER BY effectivity_date");
  return $query->result();
}

public function getActiveSSS(){
  $query = $this->db->query("SELECT min_range,max_range,salary_credit,sss_employer,sss_employee,ec_employer,(sss_employer+sss_employee) as subtotal,(sss_employer+ec_employer) as sss_employer2,(sss_employer+sss_employee+ec_employer) as total FROM tbl_sss_effectivity as se, tbl_sss as s WHERE se.sss_effec_id=s.sss_effec_id AND se.isActive = 1");
  return $query->result();
}
public function getSelectedSSS($id){
  $query = $this->db->query("SELECT min_range,max_range,salary_credit,sss_employer,sss_employee,ec_employer,(sss_employer+sss_employee) as subtotal,(sss_employer+ec_employer) as sss_employer2,(sss_employer+sss_employee+ec_employer) as total FROM tbl_sss_effectivity as se, tbl_sss as s WHERE se.sss_effec_id=s.sss_effec_id AND se.sss_effec_id=$id");
  return $query->result();
}
public function getPagibig(){
  $query = $this->db->query("SELECT pagibig_id,round(employeeshare*100) as employeeshare,round(employershare*100) as employershare,DATE_FORMAT(effectivity_date,'%M %d, %Y') as effectivity_date,isActive FROM tbl_pagibig");
  return $query->result();
}
public function insertPagibig($values){
  $this->db->insert('tbl_pagibig', $values);
  return $this->db->insert_id();
}
public function getHoliday() {
  $query = $this->db->query("SELECT holiday_id,DATE_FORMAT(date,'%b %d') as date2,date,holiday,description,type,site FROM tbl_holiday");
  return $query->result();
}
public function insertHoliday($values){
  $this->db->insert('tbl_holiday', $values);
  return $this->db->insert_id();
}
public function savepayrolldeduction($values){

  $this->db->insert('tbl_payroll_deductions', $values);
  return $this->db->insert_id();
}
public function getpayrolldeductions(){
  $query = $this->db->query('select pdeduct_id as id,deductionname,(select fname from tbl_user a,tbl_employee b,tbl_applicant c where a.emp_id=b.emp_id and b.apid=c.apid and uid = addedBy ) as addedBy,(select fname from tbl_user a,tbl_employee b,tbl_applicant c where a.emp_id=b.emp_id and b.apid=c.apid and uid =updatedBy) as updatedBy,description from tbl_payroll_deductions');
  return $query->result();
}
public function savepayrolladdition($values){

  $this->db->insert('tbl_payroll_addition', $values);
  return $this->db->affected_rows();
}
public function getpayrolladditions(){
  $query = $this->db->query('select defaultval,paddition_id as id,additionname,(select fname from tbl_user a,tbl_employee b,tbl_applicant c where a.emp_id=b.emp_id and b.apid=c.apid and uid = addedBy ) as addedBy,(select fname from tbl_user a,tbl_employee b,tbl_applicant c where a.emp_id=b.emp_id and b.apid=c.apid and uid =updatedBy) as updatedBy,description from tbl_payroll_addition');
  return $query->result();
}
public function updateAddition($id,$values){
  $this->db->where('paddition_id', $id);
  $this->db->update('tbl_payroll_addition', $values);
  return $this->db->affected_rows();
}
public function updateDeduction($id,$values){
  $this->db->where('pdeduct_id', $id);
  $this->db->update('tbl_payroll_deductions', $values);
  return $this->db->affected_rows();
}

//--------------------------------------------------------------ADDITIONS----------------------------------------------------------------
public function getAllCoverageAdd(){
 $query = $this->db->query("SELECT * FROM tbl_payroll_coverage WHERE coverage_id NOT IN(SELECT DISTINCT(coverage_id) FROM tbl_payroll_emp_addition)");
 return $query->result();
}
public function getAllCoverageDeduct(){
 $query = $this->db->query("SELECT * FROM tbl_payroll_coverage WHERE coverage_id NOT IN(SELECT DISTINCT(coverage_id) FROM tbl_payroll_emp_deduction)");
 return $query->result();
}
public function getAllActiveEmployee(){
  $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,c.pos_name,f.status,b.id_num FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND b.isActive='yes' AND d.isActive=1 AND e.isActive=1 ORDER BY a.lname");
  return $query->result();
}

public function getCoverage($id){
  $this->db->from('tbl_payroll_coverage');
  $this->db->where('coverage_id',$id);
  return $this->db->get()->row();
}
public function getAllAdditions(){
  $this->db->where('isActive',1);
  $this->db->from('tbl_payroll_addition');
  $this->db->order_by('paddition_id', 'ASC');
  return $this->db->get()->result();
}
public function saveEmployeeAdditions($values){
  $this->db->insert('tbl_payroll_emp_addition', $values);
  return $this->db->affected_rows();
}

public function getAllWithValueCoverage(){
  $query = $this->db->query("SELECT * FROM tbl_payroll_coverage as a ORDER BY a.coverage_id ASC");
  return $query->result();
}
public function getEmployeeAdditions($coverage_id){
  $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status,g.coverage_id,GROUP_CONCAT(i.paddition_id) as paddition_id,GROUP_CONCAT(h.value) as value,GROUP_CONCAT(i.additionname) as additionname FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f, tbl_payroll_coverage as g, tbl_payroll_emp_addition as h, tbl_payroll_addition as i WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND g.coverage_id=h.coverage_id AND i.paddition_id=h.paddition_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND d.isActive=1 AND g.coverage_id='".$coverage_id."' GROUP BY b.emp_id ORDER BY a.lname");
  return $query->result();
}
//---------------------------------------------------------DEDUCTIONS----------------------------------------------------------------------

public function getAllDeductions(){
  $this->db->where('isActive',1);
  $this->db->from('tbl_payroll_deductions');
  return $this->db->get()->result();
}
public function saveEmployeeDeductions($values){
  $this->db->insert('tbl_payroll_emp_deduction', $values);
  return $this->db->affected_rows();
}

public function getAllWithValueCoverageDeduction(){
  $query = $this->db->query("SELECT DISTINCT(a.coverage_id),daterange,status FROM tbl_payroll_coverage as a, tbl_payroll_emp_deduction as b WHERE a.coverage_id=b.coverage_id ORDER BY a.coverage_id ASC");
  return $query->result();
}
public function getEmployeeDeductions($coverage_id){
  $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status,g.coverage_id,GROUP_CONCAT(i.pdeduct_id) as pdeduct_id,GROUP_CONCAT(h.value) as value,GROUP_CONCAT(i.deductionname) as deductionname FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f, tbl_payroll_coverage as g, tbl_payroll_emp_deduction as h, tbl_payroll_deductions as i WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND g.coverage_id=h.coverage_id AND i.pdeduct_id=h.pdeduct_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND d.isActive=1 AND g.coverage_id='".$coverage_id."' GROUP BY b.emp_id ORDER BY a.lname");
  return $query->result();
}

//------------------------------------------------update------------------------------------------------------------------
public function updateEmpDeduct($emp_id,$coverage_id,$pdeduct_id,$value){
  $this->db->query("UPDATE tbl_payroll_emp_deduction SET value='".$value."' WHERE emp_id='".$emp_id."' AND coverage_id='".$coverage_id."' AND pdeduct_id='".$pdeduct_id."'");
  return $this->db->affected_rows();
  // return $this->db->last_query();
}

public function updateEmpAdd($emp_id,$coverage_id,$paddition_id,$value){
  $this->db->query("UPDATE tbl_payroll_emp_addition SET value='".$value."' WHERE emp_id='".$emp_id."' AND coverage_id='".$coverage_id."' AND paddition_id='".$paddition_id."'");
  return $this->db->affected_rows();
}

public function isExistCoverageAdd($coverage_id){
  $query = $this->db->query("SELECT DISTINCT(coverage_id) FROM tbl_payroll_emp_addition WHERE coverage_id=".$coverage_id);
  return $query->row();
}
public function isExistCoverageDeduct($coverage_id){
  $query = $this->db->query("SELECT DISTINCT(coverage_id) FROM tbl_payroll_emp_deduction WHERE coverage_id=".$coverage_id);
  return $query->row();
}
//---
public function countAllUsers(){
 $query = $this->db->query("SELECT COUNT(*)as count FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND b.isActive='yes' AND d.isActive=1");
 return $query->row();
}
//----addition
public function getEmpAddExistAdditions($coverage_id,$emp_id,$addids){
  $query = $this->db->query("SELECT * FROM tbl_payroll_addition WHERE paddition_id NOT IN (SELECT paddition_id FROM tbl_payroll_emp_addition WHERE emp_id='".$emp_id."' AND coverage_id='".$coverage_id."' AND paddition_id IN ( '" . implode($addids, "', '") . "' ))");
  return $query->result();
}
public function countAdditions(){
  $query = $this->db->query("SELECT COUNT(*) as count FROM tbl_payroll_addition WHERE isActive=1");
  return $query->row();
}
public function addExistEmpAddition($data){
  $this->db->insert('tbl_payroll_emp_addition', $data);
  return $this->db->affected_rows();
}
public function getUsersNotListedAdd($coverage_id){
  $query = $this->db->query("SELECT DISTINCT(emp_id) FROM tbl_payroll_emp_addition WHERE coverage_id='".$coverage_id."'");
  return $query->result();
}
public function getAllActiveEmployeeAdd($empids){
  $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND b.isActive='yes' AND d.isActive=1 AND b.emp_id NOT IN ( '" . implode($empids, "', '") . "' )ORDER BY a.lname");
  return $query->result();
}
//--deduction
public function getEmpAddExistDeductions($coverage_id,$emp_id,$deductids){
  $query = $this->db->query("SELECT * FROM tbl_payroll_deductions WHERE pdeduct_id NOT IN (SELECT pdeduct_id FROM tbl_payroll_emp_deduction WHERE emp_id='".$emp_id."' AND coverage_id='".$coverage_id."' AND pdeduct_id IN ( '" . implode($deductids, "', '") . "' ))");
  return $query->result();
}
public function countDeductions(){
  $query = $this->db->query("SELECT COUNT(*) as count FROM tbl_payroll_deductions WHERE isActive=1");
  return $query->row();
}
public function addExistEmpDeduction($data){
  $this->db->insert('tbl_payroll_emp_deduction', $data);
  return $this->db->affected_rows();
}
public function getUsersNotListedDeduct($coverage_id){
  $query = $this->db->query("SELECT DISTINCT(emp_id) FROM tbl_payroll_emp_deduction WHERE coverage_id='".$coverage_id."'");
  return $query->result();
}
public function getAllActiveEmployeeDeduct($empids){
  $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND b.isActive='yes' AND d.isActive=1 AND b.emp_id NOT IN ( '" . implode($empids, "', '") . "' )ORDER BY a.lname");
  return $query->result();
}

public function getCoverageStatus($id){
  $this->db->select('status');
  $this->db->from('tbl_payroll_coverage');
  $this->db->Where('coverage_id',$id);
  return $this->db->get()->row();
}


//--------------------------TAX TYPE---------------------------------------------
public function getEmpTaxtype(){
  $query = $this->db->query("SELECT c.tax_desc_id,b.emp_id,a.lname,a.fname FROM tbl_employee as b LEFT JOIN tbl_tax_emp as c ON b.emp_id=c.emp_id AND c.isActive=1 INNER JOIN tbl_applicant as a ON a.apid=b.apid AND b.isActive='yes' ORDER BY a.lname");
  return $query->result();
}
public function getTaxDesc(){
  $taxdesc = $this->db->query("SELECT tax_desc_id as value,description as text FROM tbl_tax_description WHERE isActive=1");
  return $taxdesc->result();
}
//new
public function getPosEmpStatId($emp_id){
  $this->db->select('emp_promoteID');
  $this->db->from('tbl_emp_promote');
  $this->db->where(array('emp_id'=>$emp_id,'isActive'=>1));
  return $this->db->get()->row();
}
public function onPayroll($coverage_id,$emp_promoteID){
  $this->db->from('tbl_payroll');
  $this->db->where(array('coverage_id'=>$coverage_id,'emp_promoteId'=>$emp_promoteID,'isActive'=>1));
  return $this->db->get()->row();
}

//NEW CODES

public function getAllEmpAdditionPerCoverage($emp_id,$coverage_id){
  $query = $this->db->query("SELECT * FROM tbl_payroll_emp_addition WHERE emp_id=$emp_id AND coverage_id=$coverage_id");
  return $query->result();
}
public function getAllEmpDeductionPerCoverage($emp_id,$coverage_id){
  $query = $this->db->query("SELECT * FROM tbl_payroll_emp_deduction WHERE emp_id=$emp_id AND coverage_id=$coverage_id");
  return $query->result();
}
public function empAddChecker($emp_id,$coverage_id,$paddition_id){
  $query = $this->db->query("SELECT * FROM tbl_payroll_emp_addition WHERE emp_id=$emp_id AND coverage_id=$coverage_id AND paddition_id=$paddition_id");
  return $query->result();
}
public function updateEmployeeAdditions($emp_id,$coverage_id,$paddition_id,$value){
  $this->db->where(array('emp_id'=>$emp_id,'coverage_id'=>$coverage_id,'paddition_id'=>$paddition_id));
  $this->db->update('tbl_payroll_emp_addition',array('value'=>$value));
  return $this->db->affected_rows();
}
public function empDedChecker($emp_id,$coverage_id,$pdeduct_id){
  $query = $this->db->query("SELECT * FROM tbl_payroll_emp_deduction WHERE emp_id=$emp_id AND coverage_id=$coverage_id AND pdeduct_id=$pdeduct_id");
  return $query->result();
}
public function updateEmployeeDeductions($emp_id,$coverage_id,$pdeduct_id,$value){
  $this->db->where(array('emp_id'=>$emp_id,'coverage_id'=>$coverage_id,'pdeduct_id'=>$pdeduct_id));
  $this->db->update('tbl_payroll_emp_deduction',array('value'=>$value));
  return $this->db->affected_rows();
}
public function getEmpPaytype(){
  $query = $this->db->query("SELECT lname,fname,b.emp_id,salarymode FROM tbl_applicant a,tbl_employee b WHERE a.apid=b.apid AND b.isActive='yes' ORDER BY lname ASC");
  return $query->result();
}

public function updateSalaryMode($value,$emp_id){
  $this->db->where('emp_id',$emp_id);
  $this->db->update('tbl_employee',array('salarymode'=>$value));
  return $this->db->affected_rows();
}

//----------------------NEW EMPLOYEE ADDITIONS CODES------------------------------------
public function getEmployeeAdditions2($stafftype,$status,$coverage_id,$site){
	$append = ($site!=null) ? " and c.pos_details like '%".$site."%'" : " and c.pos_details not like '%cebu%'";
  $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status,g.coverage_id,GROUP_CONCAT(i.paddition_id) as paddition_id,GROUP_CONCAT(h.value) as value,GROUP_CONCAT(i.additionname) as additionname FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f, tbl_payroll_coverage as g, tbl_payroll_emp_addition as h, tbl_payroll_addition as i WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND g.coverage_id=h.coverage_id AND i.paddition_id=h.paddition_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND d.isActive=1 AND g.coverage_id='".$coverage_id."' AND f.status='$status' AND c.class='$stafftype' ".$append."  GROUP BY b.emp_id ORDER BY a.lname");
  return $query->result();
}
public function getEmployeeAdditionsProbReg($stafftype,$stat,$coverage_id){
	$status = ($stat=='c')  ? " and b.confidential=1" : " and b.confidential=0" ;
  $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status,g.coverage_id,GROUP_CONCAT(i.paddition_id) as paddition_id,GROUP_CONCAT(h.value) as value,GROUP_CONCAT(i.additionname) as additionname FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f, tbl_payroll_coverage as g, tbl_payroll_emp_addition as h, tbl_payroll_addition as i WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND g.coverage_id=h.coverage_id AND i.paddition_id=h.paddition_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND d.isActive=1 AND g.coverage_id='".$coverage_id."' AND (f.status='Probationary' OR f.status='Regular') AND c.class='$stafftype' ".$status." GROUP BY b.emp_id ORDER BY a.lname");
  return $query->result();
}
public function getCoverageDate($coverage_id){
  $this->db->where('coverage_id',$coverage_id);
  $this->db->select('daterange');
  $this->db->from('tbl_payroll_coverage');
  return $this->db->get()->row();
}
public function getSpecificActiveEmployee($stafftype,$status,$site){
	$append = ($site!=null) ? " and c.pos_details like '%".$site."%'" : " and c.pos_details not like '%cebu%'";
  $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,c.pos_name,f.status,b.id_num FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND b.isActive='yes' AND d.isActive=1 AND e.isActive=1 AND f.status='$status' AND c.class='$stafftype' ".$append." ORDER BY a.lname");
  return $query->result();
}
public function getSpecificActiveEmployeeProbReg($stafftype,$stat){
  $status = ($stat=='c')  ? " and b.confidential=1" : " and b.confidential=0" ;

  $query = $this->db->query("SELECT a.lname,a.fname,b.emp_id,c.pos_name,f.status,b.id_num FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND b.isActive='yes' AND d.isActive=1 AND e.isActive=1 AND (f.status='Probationary' OR f.status='Regular') AND c.class='$stafftype' ".$status."  ORDER BY a.lname");
  return $query->result();
}
public function getEmployeeDeductions2($stafftype,$status,$coverage_id,$site){
	$append = ($site!=null) ? " and c.pos_details like '%".$site."%'" : " and c.pos_details not like '%cebu%'";
  $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status,g.coverage_id,GROUP_CONCAT(i.pdeduct_id) as pdeduct_id,GROUP_CONCAT(h.value) as value,GROUP_CONCAT(i.deductionname) as deductionname FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f, tbl_payroll_coverage as g, tbl_payroll_emp_deduction as h, tbl_payroll_deductions as i WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND g.coverage_id=h.coverage_id AND i.pdeduct_id=h.pdeduct_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND d.isActive=1 AND g.coverage_id='".$coverage_id."' AND f.status='$status' AND c.class='$stafftype' ".$append." GROUP BY b.emp_id ORDER BY a.lname");
  return $query->result();
}
public function getEmployeeDeductionsProbReg($stafftype,$stat,$coverage_id){
 $status = ($stat=='c')  ? " and b.confidential=1" : " and b.confidential=0" ;

 $query = $this->db->query("SELECT a.fname,a.lname,b.emp_id,c.pos_name,f.status,g.coverage_id,GROUP_CONCAT(i.pdeduct_id) as pdeduct_id,GROUP_CONCAT(h.value) as value,GROUP_CONCAT(i.deductionname) as deductionname FROM tbl_applicant as a,tbl_employee as b,tbl_position as c,tbl_emp_promote as d,tbl_pos_emp_stat as e, tbl_emp_stat as f, tbl_payroll_coverage as g, tbl_payroll_emp_deduction as h, tbl_payroll_deductions as i WHERE a.apid=b.apid AND b.emp_id=d.emp_id AND d.posempstat_id=e.posempstat_id AND e.pos_id=c.pos_id AND e.empstat_id=f.empstat_id AND g.coverage_id=h.coverage_id AND i.pdeduct_id=h.pdeduct_id AND h.emp_id=b.emp_id AND b.isActive='yes' AND d.isActive=1 AND g.coverage_id='".$coverage_id."' AND (f.status='Probationary' OR f.status='Regular') ".$status." AND c.class='$stafftype' GROUP BY b.emp_id ORDER BY a.lname");
 return $query->result();
}

/***************************Mark********************************/
public function updatePayrollPeriodStatus($status,$cid){
  $this->db->where('coverage_id',$cid);
  $this->db->update('tbl_payroll_coverage',array('status'=>$status));
  return $this->db->affected_rows();
}
public function deletePrevCoverageEmpAdditions($coverage_id, $emp_id) {
	$this->db->query("DELETE FROM tbl_payroll_emp_addition WHERE coverage_id=$coverage_id AND emp_id=$emp_id");
}
public function deletePrevCoverageEmpDeductions($coverage_id, $emp_id) {
	$this->db->query("DELETE FROM tbl_payroll_emp_deduction WHERE coverage_id=$coverage_id AND emp_id=$emp_id");
}

public function pagibig_report_index(){
	$query = $this->db->query("select GROUP_CONCAT(coverage_id,'|',daterange) as daterange,month from tbl_payroll_coverage where coverage_id>=89 group by month");
	return $query->result();
}
public function pagibig_get_coverage($coverage_id){
	// $query = $this->db->query("select * from tbl_payroll_coverage where coverage_id=$coverage_id");
	// $query = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=(select coverage_id from tbl_payroll_coverage where period=1 and month='".$coverage_id."')");
	$query = $this->db->query("SELECT * from tbl_payroll_coverage where coverage_id=$coverage_id");
	return $query->result();
}
public function pagibig_contri($coverage_id){
	
	// $query = $this->db->query("SELECT pagibig,lname,fname,mname,pos_name,rate,bir,a.birthday,b.emp_id FROM `tbl_applicant` a,tbl_employee b,tbl_emp_promote c,tbl_pos_emp_stat d,tbl_position e,tbl_emp_stat f,tbl_rate g,tbl_payroll h,tbl_payroll_coverage i where a.apid=b.apid and b.emp_id=c.emp_id and c.posempstat_id = d.posempstat_id and d.pos_id= e.pos_id and f.empstat_id=d.empstat_id and g.posempstat_id = d.posempstat_id and h.emp_promoteId = c.emp_promoteId and i.coverage_id=h.coverage_id and i.coverage_id=".$coverage_id." and f.status in ('Probationary','Regular') and  b.isActive='Yes' and g.isActive=1  and d.isActive=1 and c.isActive=1 order by lname"); 
	$query = $this->db->query("SELECT pagibig,lname,fname,mname,pos_name,rate,bir,a.birthday,b.emp_id,b.salarymode FROM `tbl_applicant` a,tbl_employee b,tbl_emp_promote c,tbl_pos_emp_stat d,tbl_position e,tbl_emp_stat f,tbl_rate g,tbl_payroll h,tbl_payroll_coverage i where a.apid=b.apid and b.emp_id=c.emp_id and c.posempstat_id = d.posempstat_id and d.pos_id= e.pos_id and f.empstat_id=d.empstat_id and g.posempstat_id = d.posempstat_id and h.emp_promoteId = c.emp_promoteId and i.coverage_id=h.coverage_id and i.coverage_id=(select coverage_id from tbl_payroll_coverage where period=1 and month='".$coverage_id."') and f.status in ('Probationary','Regular') and b.isActive='Yes' and g.isActive=1 and d.isActive=1 and c.isActive=1 order by lname
    "); 
	return $query->result();

}
public function tax_contri($coverage_id,$emp_promoteId=0,$cidYear){
	$empP = ($emp_promoteId!=0) ? 'and c.emp_promoteId='.$emp_promoteId : '';
  $query = $this->db->query("SELECT payroll_id,c.emp_promoteId,quinsina,sss_details,phic_details,hdmf_details,bir_details,lname,fname,mname,period,d.emp_id,salarymode, a.isDaily from tbl_payroll a,tbl_payroll_coverage b,tbl_emp_promote c,tbl_employee d,tbl_applicant e where a.coverage_id=b.coverage_id and c.emp_promoteId=a.emp_promoteId and c.emp_id=d.emp_id and e.apid=d.apid and month='".$coverage_id."' and year='".$cidYear."' $empP order by lname"); 
  return $query->result();
}
public function atm_report($coverage_id,$atmType){
	if($atmType==3){
		$andIsATM="";
	}else{
		$andIsATM=" and b.isAtm=".$atmType;
   
	}
	$query = $this->db->query("SELECT distinct(b.emp_id),lname,fname,mname,pos_name,rate,atm_account_number,isAtm,h.final FROM `tbl_applicant` a,tbl_employee b,tbl_emp_promote c,tbl_pos_emp_stat d,tbl_position e,tbl_emp_stat f,tbl_rate g,tbl_payroll h,tbl_payroll_coverage i where a.apid=b.apid and b.emp_id=c.emp_id and c.posempstat_id = d.posempstat_id and d.pos_id= e.pos_id and f.empstat_id=d.empstat_id and g.posempstat_id = d.posempstat_id and h.emp_promoteId = c.emp_promoteId and i.coverage_id=h.coverage_id and i.coverage_id=".$coverage_id." and f.status in ('Probationary','Regular') and  b.isActive='Yes' and g.isActive=1  and d.isActive=1 and c.isActive=1 ".$andIsATM." order by lname"); 
	return $query->result();

}
public function pagibig_contri_add(){
	
	$query = $this->db->query("SELECT * FROM `tbl_pagibig_add_contribution` where isActive=1"); 
	return $query->result();

}
public function payroll_account($payrollC,$empzType){
	$query = $this->db->query("SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where emp_type like '%".$empzType."%' and coverage_id=$payrollC and SUBSTRING_INDEX(z.account, '|', 2)=SUBSTRING_INDEX(a.account, '|', 2)) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and emp_type like '%".$empzType."%' and coverage_id=$payrollC group by SUBSTRING_INDEX(trim(a.account), '|', 2) order by headcount asc"); 
 return $query->result();
}
public function payroll_account2($payrollC,$empzType){
	$query = $this->db->query("SELECT a.*,d.fname,d.lname,c.emp_id, (select count(*) from tbl_payroll z where emp_type like '%".$empzType."%' and coverage_id=$payrollC and z.account=a.account) as headcount FROM `tbl_payroll`a,tbl_emp_promote b,tbl_employee c,tbl_applicant d where b.emp_promoteId = a.emp_promoteId and b.emp_id=c.emp_id and d.apid=c.apid and emp_type like '%".$empzType."%' and coverage_id=$payrollC"); 
 return $query->result();
}
public function additionName(){
  $query = $this->db->query("SELECT * FROM `tbl_payroll_addition`"); 
  return $query->result();
}
public function bonuzName(){
  $query = $this->db->query("SELECT * FROM `tbl_bonus`"); 
  return $query->result();
}

}