<?php

class ApplicantModel extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
 
        public function fetchUSers()
        {
                $query =  $this->db->query('select uid,username,password,fname,lname,role,description from tbl_user as a,tbl_user_role b where a.role_id=b.role_id');
                return $query->result();
        }
 public function updateApplicantss($fName,$mName,$lName,$nextension,$bday,$civilStatus,$relijon,$gender,$cell,$tel,$presentAddrezs,$permanentAddrezs,$pos_id,$Source,$ccaExp,$cr1,$cr2,$cr3,$apid){
		$query =  $this->db->query(" update tbl_applicant set fname='".$fName."',mname='".$mName."',lname='".$lName."',nextension='".$nextension."',birthday='".$bday."',tel='".$tel."',cell='".$cell."',gender='".$gender."',source='".$Source."',ccaExp='".$ccaExp."',pos_id=(select pos_id from tbl_position where pos_details='".$pos_id."'),presentAddrezs='".$presentAddrezs."',permanentAddrezs='".$permanentAddrezs."',relijon='".$relijon."',civilStatus='".$civilStatus."',cr1='".$cr1."',cr2='".$cr2."',cr3='".$cr3."' where apid=".$apid."");
 } 
 public function addApplicantRecord($fname,$mname,$lname,$mobile,$Gender,$position,$source,$ccexpo,$address,$Religion,$cr1,$cr2,$cr3,$tel){ 
		$query =  $this->db->query("insert into tbl_applicant(fname,mname,lname,cell,gender,source,ccaExp,isActive,PF,pos_id,pic,dateCreated,addrezs,relijon,cr1,cr2,cr3,tel) value('".$fname."','".$mname."','".$lname."','".$mobile."','".$Gender."','".$source."','".$ccexpo."','yes','yez',(select pos_id from tbl_position where pos_details='".$position."'),'img/avatar5.png',now(),'".$address."','".$Religion."','".$cr1."','".$cr2."','".$cr3."','".$tel."')");
}
public function addWritten($result,$remark,$dates,$apid,$pos_id){
	$exam 	="Written";
 
  
$qry = $this->db->query("select count(*) + 1 as a from tbl_test where apid =".$apid." and test_info='".$exam."'");
$rowz = $qry->row();

 $qry2 = $this->db->query("select count(*) + 1 as a from tbl_test where apid =".$apid."");
 $rowz2 = $qry2->row();

 		$query =  $this->db->query("insert into tbl_test(pos_id,test_info,count,result,remarks,date_taken,uid,apid,times) values($pos_id,'$exam',".$rowz->a.",'$result','$remark','".$dates."',".$_SESSION['uid'].",$apid,".$rowz2->a.")");
}
public function addInterview($result,$remark,$dates,$apid,$pos_id,$interviewer,$level){
	$exam 	="Interview";
 
  
$qry = $this->db->query("select count(*) + 1 as a from tbl_test where apid =".$apid." and test_info='".$exam."'");
$rowz = $qry->row();

 $qry2 = $this->db->query("select count(*) + 1 as a from tbl_test where apid =".$apid."");
 $rowz2 = $qry2->row();

 		$query =  $this->db->query("insert into tbl_test(pos_id,test_info,count,result,remarks,date_taken,uid,apid,times,time_count,interviewer) values($pos_id,'$exam',".$rowz->a.",'$result','$remark','".$dates."',".$_SESSION['uid'].",$apid,".$rowz2->a.",'".$level."','".$interviewer."' )");
}

//===============================NICCA CODES=========================================
 public function findApplicant($username,$password){
  $query = $this->db->query("SELECT pos.pos_details,app.apid,app.fname,app.lname,app.mname,app.username,app.password,app.pos_id FROM tbl_applicant as app , tbl_position as pos WHERE app.username = '".$username."' AND app.password = '".$password."' AND pos.pos_id=app.pos_id");
  return $query->row();
}
public function getQuestions($set){
  $this->db->from('tbl_writtenexam');
  $this->db->where('examset',$set);
  return $this->db->get()->result();
}
public function getSets(){
  $query = $this->db->query("SELECT DISTINCT (examset) FROM tbl_writtenexam");
  return $query->result_array();
}
public function insertTest($data){
  $this->db->insert('tbl_test', $data);
}
public function isFinished($apid,$posid){
  $this->db->from('tbl_test');
  $this->db->where(array('apid'=>$apid,'pos_id'=>$posid));
  return $this->db->get()->result();
}
}