<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
<style>
a#backBtn>span:hover {
    color:black !important;
}
</style>
    <meta charset="utf-8" />
    <title>Error 403|Forbidden Access</title>
    <meta name="description" content="Initialized via remote ajax json data">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!--begin::Web font -->
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
		function readSystemNotificationOnPage() {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>/General/readNotificationOnPage",
                    data: {
                        mainNotifTable: 'tbl_system_notification',
                        subNotifTable: 'tbl_system_notification_recipient',
                        mainNotifID: 'systemNotification_ID',
                        subNotifID: 'systemNotificationRecipient_ID',
                        link: window.location.href
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        getLatestSystemNotifications();
                        $("#system_notif_count").html(res.unreadCount);
                    }
                });
            }
			$(function(){
				readSystemNotificationOnPage();
			});
    </script>
    <!--end::Web font -->
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/img/favicon.ico" />


    <script src="<?php echo base_url(); ?>assets/js/app.js" type="text/javascript"></script>
</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid  m-error-1" style="background-image: url(<?php echo base_url(); ?>assets/media/error_img/underconstruction.jpg);">
        <div class="col-12 p-3">
            <p class="mt-5 m-error_desc" style="margin-top:unset !important">
                <a id="backBtn" class="btn btn-outline-metal m-btn m-btn--icon text-dark" href="javascript:history.go(-1)" style="text-decoration: none;border-color: black !important;font-size: 1.2rem;">
                    <span class="text-light">
                        <i class="fa fa-chevron-left pr-1"></i>
                        BACK TO PREVIOUS PAGE
                    </span>
                </a>
            </p>
        </div>
    </div>
</div>
</body>
</html>