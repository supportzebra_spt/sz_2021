<!DOCTYPE html>
<html lang="en">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <style type="text/css">
        #papaz {
            width:80%
        }
        #papaz {
            display:auto;
        }

        @media screen and (max-width: 1000px) {
            #papaz {
                width:100% !important
            }

        }
        @media screen and (max-width: 767px) {
            #papaz {
                display:none !important
            }

        }
    </style>
    <head>
        <meta charset="utf-8" />
        <title>Error 404|Page Not Found</title>
        <meta name="description" content="Initialized via remote ajax json data">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--begin::Web font -->
        <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
                WebFont.load({
                    google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
                    active: function () {
                        sessionStorage.fonts = true;
                    }
                });
        </script>
        <!--end::Web font -->
        <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/img/favicon.ico" />


        <script src="<?php echo base_url(); ?>assets/js/app.js" type="text/javascript"></script>
    </head>
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">


            <div class="m-grid__item m-grid__item--fluid m-grid">
                <div >
                    <div class="row" style='height:100vh;margin:0'>

                        <div class="col-md-6">
                            <img id='papaz' src="<?php echo base_url('assets/images/img/papaz2.png') ?>" alt="" style='display: block;margin-top:30px;margin-left: auto;margin-right: auto;width: 80%;'/>
                        </div>
                        <div class="col-md-6 text-center" style='background:#eee;'>
                            <h1 style='margin-top:60px;color:#444;font-size:180px;font-weight:600;margin-bottom: 0;'>
                                404      
                            </h1> 
                            <p style='color:#ccc;font-size:110px;font-weight:600'>
                                ERROR
                            </p>
                            <br />
                            <br />
                            <p class="m-error_description" style='color:#666;font-size:25px;'>
                                We can't seem to find the page you're looking for.
                            </p>
                            <a class="btn btn-outline-metal m-btn m-btn--icon text-dark" href="javascript:history.go(-1)" style="text-decoration: none;">
                                <span class="text-dark">
                                    <i class="fa fa-chevron-left pr-1"></i>
                                    BACK TO PREVIOUS PAGE
                                </span>

                            </a>
                            <br />
                            <br />
                        </div>
                    </div>
                </div>

            </div>              

        </div>
        <!-- end:: Page -->

    </body>
</html>