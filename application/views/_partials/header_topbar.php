<style type="text/css">
	a.custom-header-link {
		text-decoration: none !important;
	}

	a.custom-header-link:hover .m-widget4__item {
		background-color: #eee !important
	}

	#topbar_notifications_logs .m-timeline-2:before {
		left: 90px !important;
	}

	#topbar_notifications_logs .m-timeline-2__item-cricle {
		left: 79px !important;
	}

	#topbar_notifications_logs .m-timeline-2__item-text {
		padding-left: 95px !important;
	}

	#topbar_notifications_logs .m-timeline-2__item-time {
		padding-left: 20px !important;
	}

	#topbar_notifications_logs .m-list-timeline__time {
		width: 130px;
	}

	@media (max-width: 992px) {
		#header-time {
			color: white !important;
			text-align: center;
		}

	}

	body {
		/*cursor:  url(<?php echo base_url('assets/images/img/valentine-cursor.cur') ?>), auto;*/
	}

	.topbar-markread.disabled {
		color: gray;
		pointer-events: none
	}

	#all-notifications-dropdown {
		width: 490px !important;
	}

	#all-notifications-dropdown .m-tabs-line .m-tabs__item {
		margin-right: 15px !important;
	}


	::-webkit-scrollbar {
	width: 8px;
	}

	::-webkit-scrollbar-track {
	background: rgba(0, 0, 0, 0.2);
	}

	::-webkit-scrollbar-thumb {
	/*background:#8D87D4;*/
	background:#6B6C77;
	}

	::-webkit-scrollbar-thumb:hover {
	/*background: #5A54A1;*/
	background:#414251;
	}
</style>
<header class="m-grid__item    m-header " data-minimize-offset="200" data-minimize-mobile-offset="200">
	<div class="m-container m-container--fluid m-container--full-height">
		<div class="m-stack m-stack--ver m-stack--desktop">
			<div class="m-stack__item m-brand  m-brand--skin-dark ">
				<div class="m-stack m-stack--ver m-stack--general">
					<div class="m-stack__item m-stack__item--middle m-brand__logo">
						<a href="#" class="m-brand__logo-wrapper">
							<img alt="" src="<?php echo base_url(); ?>/assets/images/img/logo.png" style="width: 150px;" />
						</a>
					</div>
					<div class="m-stack__item m-stack__item--middle m-brand__tools">
						<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block
						">
							<span></span>
						</a>
						<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>
						<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
							<span></span>
						</a>
						<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
							<i class="flaticon-more"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
				<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
				<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">

					<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
						<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" id="header-time"></li>
					</ul>
				</div>
				<div id="m_header_topbar" class="m-topbar m-stack m-stack--ver m-stack--general">
					<div id="headerTopBar" class="m-stack__item m-topbar__nav-wrapper header-topbar">
						<ul class="m-topbar__nav m-nav m-nav--inline">
							<li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light m-dropdown--open" data-dropdown-toggle="click" aria-expanded="true">
								<a href="<?php echo base_url('Announcement/announcement_list'); ?>" class="m-nav__link" data-toggle="m-tooltip" title="" data-html="true" data-content="And here's some amazing <b>HTML</b> content. It's very <code>engaging</code>. Right?">
									<span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
									<span class="m-nav__link-icon"><span class="m-nav__link-icon"><img src="<?php echo base_url('assets/images/img/announce.png') ?>" style="margin-left: 10px;width:25px"></span></span>
								</a>

							</li>
							<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" data-dropdown-toggle="click" data-dropdown-persistent="true">
								<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_iconxx">
									<!-- <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span> -->
									<span id="topbar-badge" class="m-nav__link-badge m-badge m-badge--danger" style="left: 70%;position: absolute;font-size: 10px;padding: 0 5px;top: 10px;">0</span>
									<span class="m-nav__link-icon"><i class="flaticon-music-2"></i></span>
								</a>
								<div class="m-dropdown__wrapper" id="all-notifications-dropdown">
									<span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__header m--align-center" style="background: url(<?php echo base_url(); ?>assets/images/img/bg-sidebar.png); background-size: cover;">
											<span class="m-dropdown__header-title notif-title">Notifications</span>
											<span class="m-dropdown__header-subtitle notif-subtitle">User Notifications</span>
										</div>
										<div class="m-dropdown__body" style="padding:10px">
											<div class="m-dropdown__content">
												<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--dark" role="tablist" style=" margin-bottom: 0px !important;padding:0px 10px">
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
															Request <span class="m-badge m-badge--brand m-badge--wide" style="font-size: 10px;padding:0px 8px;" id="request_notif_count"></span>
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_general" role="tab">System
															<span class="m-badge m-badge--info m-badge--wide" style="font-size: 10px;padding:0px 8px;" id="general_notif_count"></span>
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_snapsz" role="tab">SnapSZ
															<span class="m-badge m-badge--success m-badge--wide" style="font-size: 10px;padding:0px 8px;" id="snapsz_notif_count"></span>
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_dms" role="tab">DMS
															<span class="m-badge m-badge--danger m-badge--wide" style="font-size: 10px;padding:0px 8px;" id="dms_notif_count"></span>
														</a>
													</li>
													<!--  <li class="nav-item m-tabs__item">
                                                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">Audit Trail</a>
                                                    </li> -->
												</ul>
												<div class="tab-content">
													<input type="hidden" id="limiter_notifications" value="0" />
													<div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
														<div class="text-right m-0"><a href="#" class="m-link topbar-markread" data-type="normal" style="font-size:10px">Mark all as read</a></div>
														<div class="m-scrollable" data-scrollable="true" data-max-height="350" data-mobile-max-height="350" id="scroll_notifications">
															<div id="container_notifications" class="m-widget4  pb-1 pt-0 px-0"">

                                                			</div>
                                                			<div id="loadMore" style="display:none">
																<button class="btn btn-secondary btn-block btn-sm m-btn--icon">
																	<span>
																		<i class="fa flaticon-refresh"></i>
																		<span>Load More</span>
																	</span>
																</button>
															</div>
														</div>
													</div>
													<input type="hidden" id="limiter_general_notifications" value="0" />
													<div class="tab-pane" id="topbar_notifications_general" role="tabpanel">
														<div class=" text-right m-0"><a href="#" class="m-link topbar-markread" data-type="general" style="font-size:10px">Mark all as read</a></div>
														<div class="m-scrollable" data-scrollable="true" data-max-height="350" data-mobile-max-height="350" id="scroll_notifications2">
															<div id="container_general_notifications" class="m-widget4 pb-1 pt-0 px-0">
															</div>
															<div id="loadMoreGeneral" style="display:none">
																<button class="btn btn-secondary btn-block btn-sm m-btn--icon">
																	<span>
																		<i class="fa flaticon-refresh"></i>
																		<span>Load More</span>
																	</span>
																</button>
															</div>
														</div>
													</div>
													<input type="hidden" id="limiter_snapsz_notifications" value="0" />
													<div class="tab-pane" id="topbar_notifications_snapsz" role="tabpanel">
														<div class=" text-right m-0"><a href="#" class="m-link topbar-markread" data-type="snapsz" style="font-size:10px">Mark all as read</a></div>
														<div class="m-scrollable" data-scrollable="true" data-max-height="350" data-mobile-max-height="350" id="scroll_notifications3">
															<div id="container_snapsz_notifications" class="m-widget4 pb-1 pt-0 px-0">
															</div>
															<div id="loadMoreSnapsz" style="display:none">
																<button class="btn btn-secondary btn-block btn-sm m-btn--icon">
																	<span>
																		<i class="fa flaticon-refresh"></i>
																		<span>Load More</span>
																	</span>
																</button>
															</div>
														</div>
													</div>

													<input type="hidden" id="limiter_dms_notifications" value="0" />
													<div class="tab-pane" id="topbar_notifications_dms" role="tabpanel">
														<div class=" text-right m-0"><a href="#" class="m-link topbar-markread" data-type="dms" style="font-size:10px">Mark all as read</a></div>
														<div class="m-scrollable" data-scrollable="true" data-max-height="350" data-mobile-max-height="350" id="scroll_notifications4">
															<div id="container_dms_notifications" class="m-widget4 pb-1 pt-0 px-0">
															</div>
															<div id="loadMoreDms" style="display:none">
																<button class="btn btn-secondary btn-block btn-sm m-btn--icon">
																	<span>
																		<i class="fa flaticon-refresh"></i>
																		<span>Load More</span>
																	</span>
																</button>
															</div>
														</div>
													</div>
													<!--  <input type="hidden" id="limiter_trail_notifications" value="0"/>
                                                    <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                                        <div class="m-scrollable" data-scrollable="true" data-max-height="350" data-mobile-max-height="350"   id="scroll_notifications3" style="padding:10px 10px">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="m-timeline-2">
                                                                            <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30"  id="container_trail_notifications">
    
                                                                            </div>
                                                                        </div>
                                                                    <div class="m-list-timeline m-list-timeline--skin-light">
                                                                        <div class="m-list-timeline__items"  id="container_trail_notifications">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <br />
                                                                    <div id="loadMoreTrail" style="display:none">
                                                                        <button class="btn btn-secondary btn-block btn-sm m-btn--icon">
                                                                            <span>
                                                                                <i class="fa flaticon-refresh"></i>
                                                                                <span>Load More</span>
                                                                            </span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div> -->
												</div>
											</div>
										</div>
									</div>
							</li>

							<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
								<a href="#" class="m-nav__link m-dropdown__toggle">
									<span class="m-topbar__userpic">
										<img src="<?php echo base_url($session['pic']); ?>" onerror="noImage(<?php echo $session['uid']; ?>)" class="m--img-rounded m--marginless m--img-centered header-userpic<?php echo $session['uid']; ?>" alt="" />
									</span>
									<span class="m-topbar__username m--hide">Nick</span>
								</a>
								<div class="m-dropdown__wrapper">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__header m--align-center" style="background: url(<?php echo base_url(); ?>assets/images/img/bg-sidebar.png); background-size: cover;">
											<div class="m-card-user m-card-user--skin-dark" style="background: #ffffffc7;border-radius: 20px;padding: 5px;">
												<div class="m-card-user__pic">
													<img src="<?php echo base_url($session['pic']); ?>" onerror="noImage(<?php echo $session['uid']; ?>)" class="m--img-rounded m--marginless header-userpic<?php echo $session['uid']; ?>" style="border: 1px solid #afabab;" />
												</div>
												<div class="m-card-user__details">
													<span class="m-card-user__name m--font-weight-500 user-name" style="font-size: inherit;font-weight: bold;"><?php echo $session['fname'] . ' ' . $session['lname']; ?></span>
													<!-- <a href="#" class="m-card-user__email m--font-weight-300 m-link user-role" style="font-size: smaller;    color: #36a3f7 !important;"><?php echo $session['acc_name']; ?></a> -->

													<span class="m-card-user__email m--font-weight-300  m--font-info" style="font-size: smaller;"><?php echo $session['acc_name']; ?></span>
												</div>
											</div>
										</div>
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav m-nav--skin-light">
													<li class="m-nav__section m--hide">
														<span class="m-nav__section-text">Section</span>
													</li>
													<li class="m-nav__item">
														<a href="<?php echo base_url('Employee/personal'); ?>" class="m-nav__link">
															<i class="m-nav__link-icon flaticon-profile-1"></i>
															<span class="m-nav__link-title">
																<span class="m-nav__link-wrap">
																	<span class="m-nav__link-text">My Personal Profile</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-nav__item">
														<a href="<?php echo base_url('Discipline/dms_personal'); ?>" class="m-nav__link">
															<i class="m-nav__link-icon fa fa-balance-scale"></i>
															<span class="m-nav__link-title">
																<span class="m-nav__link-wrap">
																	<span class="m-nav__link-text">Discipline Management</span>
																	<span class="m-nav__link-badge">
																		<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">new</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-nav__item  text-center">
														<hr />
														<?php if ($this->session->signinType === 'admin') : ?>
														<a href="<?php echo base_url('AdminLogin/log_out'); ?>" class="btn m-btn--pill btn-outline-danger m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>

														<?php else : ?>
														<a href="<?php echo base_url('Login/log_out'); ?>" class="btn m-btn--pill btn-outline-danger m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>

														<?php endif; ?>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var perPage = 10;

		function getLatestNotifications(limiter = 0) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('Leave/get_latest_notifications'); ?>",
				data: {
					limiter: limiter
				},
				cache: false,
				success: function(json) {
					json = JSON.parse(json.trim());
					if (json.status === 'Empty') {
						$("#loadMore").hide();
					} else {
						$("#limiter_notifications").val(limiter);
						var counter = 0;

						$("#container_notifications").append("");
						$.each(json.notifications, function(key, obj) {
							if (obj.status === 'unread') {
								var readClass = "background:#eee";
							} else {
								var readClass = "background:#fff";
							}
							var str = '<a href="<?php echo base_url(); ?>' + obj.link + '" class="custom-header-link" data-status="' + obj.status + '" data-link="' + obj.link + '" data-notifrecipientid="' + obj.notificationRecipient_ID + '"> ';
							str += '<div class="m-widget4__item" style="border-bottom: .07rem solid #ddd;padding:5px 5px;' + readClass + '">';
							str += '<div class="m-widget4__img m-widget4__img--logo">';
							str += '<img src="<?php echo base_url(); ?>' + obj.pic + '" onerror="noImage(' + obj.uid + ')" class="user-pic' + obj.uid + ' header-userpic' + obj.uid + '" >';
							str += '</div>';
							str += '<div class="m-widget4__info" style="padding-right: 0 !important">';
							str += '<span class="m-widget4__sub">';
							str += '<span style="color:#018BCA;font-weight:450;">' + obj.sender + '</span> ';
							str += '<span style="font-weight:420;color:#555;font-size:12px;">' + obj.message + '</span>';
							//                                if (obj.status === 'unread') {
							//                                    str += '<span class="m-badge m-badge--wide pull-right" style="font-size:9px;margin-top: 5px;background:#8AC543;color:white">UNREAD</span>';
							//                                }

							str += '</span><br />';
							if (obj.status === 'unread') {
								str += '<span class="pull-right" style="font-size:9px;color:#F4516C"><i class="fa fa-envelope"></i></span>';
							} else {
								str += '<span class="pull-right" style="font-size:9px;color:#34BFA3"><i class="fa fa-envelope-open"></i></span>';
							}
							str += '<span style="color:#726BCA;font-size:10px;font-weight:450;" data-toggle="m-tooltip" data-placement="right" title="" data-skin="dark" data-original-title="' + moment(obj.createdOn).format('MMM DD, YYYY hh:mm A') + '">' + obj.timeElapsed + '</span>';
							str += '</div>';
							str += '</div>';
							str += '</a>';
							$("#container_notifications").append(str);
							counter++;
						});
						if (counter === perPage) {
							$("#loadMore").show();
						} else {
							$("#loadMore").hide();
						}
					}
					mApp.initTooltips();
				}
			});
		}

		function getLatestSystemNotifications(type, limiter = 0) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('Leave/get_latest_system_notifications'); ?>/" + type,
				data: {
					limiter: limiter
				},
				cache: false,
				success: function(json) {
					json = JSON.parse(json.trim());
					if (json.status === 'Empty') {
						if (type === 'dms') {
							$("#loadMoreDms").hide();
						} else if (type === 'snapsz') {
							$("#loadMoreSnapsz").hide();
						} else {
							$("#loadMoreGeneral").hide();
						}
					} else {

						if (type === 'dms') {
							$("#limiter_dms_notifications").val(limiter);
						} else if (type === 'snapsz') {
							$("#limiter_snapsz_notifications").val(limiter);
						} else {
							$("#limiter_general_notifications").val(limiter);
						}

						var counter = 0;
						$.each(json.notifications, function(key, obj) {
							if (obj.status === 'unread') {
								var readClass = "background:#eee";
							} else {
								var readClass = "background:#fff";
							}
							var str = '<a href="<?php echo base_url(); ?>' + obj.link + '" class="custom-header-link" data-status="' + obj.status + '" data-link="' + obj.link + '" data-systemnotifrecipientid="' + obj.systemNotificationRecipient_ID + '"> ';
							str += '<div class="m-widget4__item" style="border-bottom: .07rem solid #ddd;padding:5px 5px;' + readClass + '">';
							str += '<div class="m-widget4__img m-widget4__img--logo">';
							str += '<img src="<?php echo base_url('assets/images/img/excited.png'); ?>" >';
							str += '</div>';
							str += '<div class="m-widget4__info" style="padding-right: 0 !important">';
							str += '<span class="m-widget4__sub">';
							str += '<span style="font-weight:420;color:#555;font-size:12px;">' + obj.message + '</span>';

							str += '</span><br />';
							if (obj.status === 'unread') {
								str += '<span class="pull-right" style="font-size:9px;color:#F4516C"><i class="fa fa-envelope"></i></span>';
							} else {
								str += '<span class="pull-right" style="font-size:9px;color:#34BFA3"><i class="fa fa-envelope-open"></i></span>';
							}
							str += '<span style="color:#726BCA;font-size:10px;font-weight:450;" data-toggle="m-tooltip" data-placement="right" title="" data-skin="dark" data-original-title="' + moment(obj.createdOn).format('MMM DD, YYYY hh:mm A') + '">' + obj.timeElapsed + '</span>';
							str += '</div>';
							str += '</div>';
							str += '</a>';
							if (type === 'dms') {
								$("#container_dms_notifications").append(str);
							} else if (type === 'snapsz') {
								$("#container_snapsz_notifications").append(str);
							} else {
								$("#container_general_notifications").append(str);
							}
							counter++;
						});
						if (counter === perPage) {
							if (type === 'dms') {
								$("#loadMoreDms").show();
							} else if (type === 'snapsz') {
								$("#loadMoreSnapsz").show();
							} else {
								$("#loadMoreGeneral").show();
							}

						} else {
							if (type === 'dms') {
								$("#loadMoreDms").hide();
							} else if (type === 'snapsz') {
								$("#loadMoreSnapsz").hide();
							} else {
								$("#loadMoreGeneral").hide();
							}

						}
					}
					mApp.initTooltips();
				}
			});
		}

		function readNotificationOnPage(callback) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>/General/readNotificationOnPage",
				data: {
					mainNotifTable: 'tbl_notification',
					subNotifTable: 'tbl_notification_recipient',
					mainNotifID: 'notification_ID',
					subNotifID: 'notificationRecipient_ID',
					link: window.location.href
				},
				cache: false,
				success: function(res) {
					res = JSON.parse(res.trim());
					getLatestNotifications();

					if (parseInt(res.unreadCount) === 0) {
						$("#topbar_notifications_notifications").find('.topbar-markread').addClass("disabled");
					} else {
						$("#topbar_notifications_notifications").find('.topbar-markread').removeClass("disabled");
					}
					$("#request_notif_count").html(res.unreadCount);
					$("#topbar-badge").html(parseInt($("#request_notif_count").text()) + parseInt($("#general_notif_count").text()) + parseInt($("#snapsz_notif_count").text()) + parseInt($("#dms_notif_count").text()));
					callback();
				}
			});
		}

		function readSystemNotificationOnPage(callback) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>/General/readNotificationOnPage",
				data: {
					mainNotifTable: 'tbl_system_notification',
					subNotifTable: 'tbl_system_notification_recipient',
					mainNotifID: 'systemNotification_ID',
					subNotifID: 'systemNotificationRecipient_ID',
					link: window.location.href
				},
				cache: false,
				success: function(res) {
					res = JSON.parse(res.trim());
					getLatestSystemNotifications("dms");
					getLatestSystemNotifications("snapsz");
					getLatestSystemNotifications("general");
					if (parseInt(res.dms_unread) === 0) {
						$("#topbar_notifications_dms").find('.topbar-markread').addClass("disabled");
					} else {
						$("#topbar_notifications_dms").find('.topbar-markread').removeClass("disabled");
					}
					if (parseInt(res.snapsz_unread) === 0) {
						$("#topbar_notifications_snapsz").find('.topbar-markread').addClass("disabled");
					} else {
						$("#topbar_notifications_snapsz").find('.topbar-markread').removeClass("disabled");
					}
					if (parseInt(res.general_unread) === 0) {
						$("#topbar_notifications_general").find('.topbar-markread').addClass("disabled");
					} else {
						$("#topbar_notifications_general").find('.topbar-markread').removeClass("disabled");
					}
					$("#dms_notif_count").html(res.dms_unread);
					$("#snapsz_notif_count").html(res.snapsz_unread);
					$("#general_notif_count").html(res.general_unread);

					$("#topbar-badge").html(parseInt($("#request_notif_count").text()) + parseInt($("#general_notif_count").text()) + parseInt($("#snapsz_notif_count").text()) + parseInt($("#dms_notif_count").text()));
					callback();
				}
			});
		}
		$(".topbar-markread").click(function(e) {
			e.preventDefault();
			var table = "";
			var where = "";
			var type = $(this).data('type');
			if (type === 'general' || type === 'snapsz' || type === 'dms') {
				table = 'tbl_system_notification_recipient';
				where = " AND reference = '" + type + "'";
			} else if (type === 'normal') {
				table = 'tbl_notification_recipient'
			}
			if (table !== "") {
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>/General/mark_as_read_nofication",
					data: {
						table: table,
						where: where
					},
					cache: false,
					success: function(res) {
						res = JSON.parse(res.trim());
						if (res.status === 'Success') {
							$.notify({
								message: 'Successfully marked notifications as read'
							}, {
								type: 'success',
								timer: 1000
							});
						} else {
							$.notify({
								message: 'An error occurred while processing your actions'
							}, {
								type: 'danger',
								timer: 1000
							});
						}
						socket.emit('markAllAsRead', {
							uid: <?php echo $this->session->uid; ?>,
							type: type
						});
					}
				});
			}

		})

		function shakerAndBlinker() {

			var countA = parseInt($("#request_notif_count").text());
			var countB = parseInt($("#general_notif_count").text());
			var countC = parseInt($("#snapsz_notif_count").text());
			var countD = parseInt($("#dms_notif_count").text());
			if (countA === 0 && countB === 0 && countC === 0 && countD === 0) {
				// Animated Notification Icon 
				$('#m_topbar_notification_iconxx .m-nav__link-icon').removeClass('m-animate-shake');
				// $('#m_topbar_notification_iconxx .m-nav__link-badge').removeClass('m-animate-blink');
			} else {
				// Animated Notification Icon 
				$('#m_topbar_notification_iconxx .m-nav__link-icon').addClass('m-animate-shake');
				// $('#m_topbar_notification_iconxx .m-nav__link-badge').addClass('m-animate-blink');
			}
		}
		//             function getMyAuditTrail(limiter = 0) {
		//                 $.ajax({
		//                     type: "POST",
		//                     url: "<?php echo base_url('General/getMyAuditTrail'); ?>",
		//                     data: {
		//                         limiter: limiter
		//                     },
		//                     cache: false,
		//                     success: function (json) {
		//                         json = JSON.parse(json.trim());
		//                         if (json.status === 'Empty') {
		//                             $("#loadMoreTrail").hide();
		//                         } else {

		//                             $("#limiter_trail_notifications").val(limiter);
		//                             var counter = 0;
		//                             $.each(json.audittrail, function (key, obj) {
		//                                 var str = '';
		// //                                if (limiter === 0 && counter === 0) {
		// //                                    str += '<div class="m-timeline-2__item">';
		// //                                } else {
		// //                                    str += '<div class="m-timeline-2__item  m--margin-top-30">';
		// //                                }
		// //                                str += '<span class="m-timeline-2__item-time text-center" style="line-height:1"><span style="font-size:13px">' + moment(obj.log).format('hh:mm A') + '</span> <br /> <span style="font-size:10px">' + moment(obj.log).format('MMM DD, YYYY') + '</span></span>';
		// //                                str += '<div class="m-timeline-2__item-cricle">';
		// //                                str += '<i class="fa fa-genderless m--font-' + obj.colorclass + '"></i>';
		// //                                str += '</div>';
		// //                                str += '<div class="m-timeline-2__item-text  m--padding-top-5" style="font-size:14px">';
		// //                                str += '' + obj.remark + '';
		// //                                str += '</div>';
		// //                                str += '</div>';
		//                                 str += '<div class="m-list-timeline__item">';
		//                                 str += '<span class="m-list-timeline__badge m-list-timeline__badge--' + obj.colorclass + '"></span>';
		//                                 str += '<span class="m-list-timeline__text" style="font-size:14px"><b>' + obj.remark + '</b> <br /> <small>(IP : ' + obj.ipaddress + ')</small></span>';
		//                                 str += '<span class="m-list-timeline__time"> <small>' + moment(obj.log).format('MMM DD, YYYY') + '</small> ' + moment(obj.log).format('hh:mm A') + '</span>';
		//                                 str += '</div>';
		//                                 $("#container_trail_notifications").append(str);
		//                                 counter++;
		//                             });
		//                             if (counter === perPage) {
		//                                 $("#loadMoreTrail").show();
		//                             } else {
		//                                 $("#loadMoreTrail").hide();
		//                             }
		//                         }
		//                     }
		//                 });
		//             }
		$(function() {
			readNotificationOnPage(function() {
				readSystemNotificationOnPage(function() {
					shakerAndBlinker();
				});
			});
			// getMyAuditTrail();
			socket.on('new notification', function(obj) {
				// console.log(obj);
				if (obj.status === 'unread') {
					var readClass = "background:#eee";
				} else {
					var readClass = "background:#fff";
				}
				var str = '<a href="<?php echo base_url(); ?>' + obj.link + '" class="custom-header-link" data-status="' + obj.status + '" data-link="' + obj.link + '" data-notifrecipientid="' + obj.notificationRecipient_ID + '"> ';
				str += '<div class="m-widget4__item" style="border-bottom: .07rem solid #ddd;padding:5px 5px;' + readClass + '">';
				str += '<div class="m-widget4__img m-widget4__img--logo">';
				str += '<img src="<?php echo base_url(); ?>' + obj.pic + '" onerror="noImage(' + obj.uid + ')" class="header-userpic' + obj.uid + '" >';
				str += '</div>';
				str += '<div class="m-widget4__info" style="padding-right: 0 !important">';
				str += '<span class="m-widget4__sub">';
				str += '<span style="color:#018BCA;font-weight:450;">' + obj.sender + '</span> ';
				str += '<span style="font-weight:420;color:#555;font-size:12px;">' + obj.message + '</span>';
				str += '</span><br />';
				if (obj.status === 'unread') {
					str += '<span class="pull-right" style="font-size:9px;color:#F4516C"><i class="fa fa-envelope"></i></span>';
				} else {
					str += '<span class="pull-right" style="font-size:9px;color:#34BFA3"><i class="fa fa-envelope-open"></i></span>';
				}
				str += '<span style="color:#726BCA;font-size:10px;font-weight:450;" data-toggle="m-tooltip" data-placement="right" title="" data-skin="dark" data-original-title="' + moment(obj.createdOn).format('MMM DD, YYYY hh:mm A') + '">' + obj.timeElapsed + '</span>';
				str += '</div>';
				str += '</div>';
				str += '</a>';
				$("#container_notifications").prepend(str);
				$("#request_notif_count").html(obj.unreadCount);
				if (parseInt(obj.unreadCount) === 0) {
					$("#topbar_notifications_notifications").find('.topbar-markread').addClass("disabled");
				} else {
					$("#topbar_notifications_notifications").find('.topbar-markread').removeClass("disabled");
				}
				$("#topbar-badge").html(parseInt($("#request_notif_count").text()) + parseInt($("#general_notif_count").text()) + parseInt($("#snapsz_notif_count").text()) + parseInt($("#dms_notif_count").text()));
				shakerAndBlinker();
				// showDeskopNotif(obj.sender, obj.message, baseUrl + "/assets/images/img/happy.gif", baseUrl + '/' + obj.link);
				mApp.initTooltips();
			});
			socket.on('new system_notification', function(obj) {
				if (obj.status === 'unread') {
					var readClass = "background:#eee";
				} else {
					var readClass = "background:#fff";
				}
				var str = '<a href="<?php echo base_url(); ?>' + obj.link + '" class="custom-header-link" data-status="' + obj.status + '" data-link="' + obj.link + '" data-systemnotifrecipientid="' + obj.systemNotificationRecipient_ID + '"> ';
				str += '<div class="m-widget4__item" style="border-bottom: .07rem solid #ddd;padding:5px 5px;' + readClass + '">';
				str += '<div class="m-widget4__img m-widget4__img--logo">';
				str += '<img src="<?php echo base_url('assets/images/img/excited.png'); ?>" >';
				str += '</div>';
				str += '<div class="m-widget4__info" style="padding-right: 0 !important">';
				str += '<span class="m-widget4__sub">';
				str += '<span style="font-weight:420;color:#555;font-size:12px;">' + obj.message + '</span>';
				str += '</span><br />';
				if (obj.status === 'unread') {
					str += '<span class="pull-right" style="font-size:9px;color:#F4516C"><i class="fa fa-envelope"></i></span>';
				} else {
					str += '<span class="pull-right" style="font-size:9px;color:#34BFA3"><i class="fa fa-envelope-open"></i></span>';
				}
				str += '<span style="color:#726BCA;font-size:10px;font-weight:450;" data-toggle="m-tooltip" data-placement="right" title="" data-skin="dark" data-original-title="' + moment(obj.createdOn).format('MMM DD, YYYY hh:mm A') + '">' + obj.timeElapsed + '</span>';
				str += '</div>';
				str += '</div>';
				str += '</a>';
				if (obj.reference === 'dms') {
					$("#container_dms_notifications").prepend(str);
					$("#dms_notif_count").html(obj.unreadCount);
					if (parseInt(obj.unreadCount) === 0) {
						$("#topbar_notifications_dms").find('.topbar-markread').addClass("disabled");
					} else {
						$("#topbar_notifications_dms").find('.topbar-markread').removeClass("disabled");
					}
				} else if (obj.reference === 'snapsz') {
					$("#container_snapsz_notifications").prepend(str);
					$("#snapsz_notif_count").html(obj.unreadCount);
					if (parseInt(obj.unreadCount) === 0) {
						$("#topbar_notifications_snapsz").find('.topbar-markread').addClass("disabled");
					} else {
						$("#topbar_notifications_snapsz").find('.topbar-markread').removeClass("disabled");
					}
				} else {
					$("#container_general_notifications").prepend(str);
					$("#general_notif_count").html(obj.unreadCount);
					if (parseInt(obj.unreadCount) === 0) {
						$("#topbar_notifications_general").find('.topbar-markread').addClass("disabled");
					} else {
						$("#topbar_notifications_general").find('.topbar-markread').removeClass("disabled");
					}
				}
				$("#topbar-badge").html(parseInt($("#request_notif_count").text()) + parseInt($("#general_notif_count").text()) + parseInt($("#snapsz_notif_count").text()) + parseInt($("#dms_notif_count").text()));
				shakerAndBlinker();
				// showDeskopNotif("System Notice", obj.message, baseUrl + "/assets/images/img/happy.gif", baseUrl + '/' + obj.link);
				mApp.initTooltips();
			});
			$("#loadMore").on('click', "button", function() {
				var limiter = parseInt($("#limiter_notifications").val());
				limiter = limiter + perPage;
				getLatestNotifications(limiter);
			});
			$("#loadMoreGeneral").on('click', "button", function() {
				var limiter = parseInt($("#limiter_general_notifications").val());
				limiter = limiter + perPage;
				getLatestSystemNotifications("general", limiter);
			});
			$("#loadMoreSnapsz").on('click', "button", function() {
				var limiter = parseInt($("#limiter_snapsz_notifications").val());
				limiter = limiter + perPage;
				getLatestSystemNotifications("snapsz", limiter);
			});
			$("#loadMoreDms").on('click', "button", function() {
				var limiter = parseInt($("#limiter_dms_notifications").val());
				limiter = limiter + perPage;
				getLatestSystemNotifications("dms", limiter);
			});

			$("#loadMoreTrail").on('click', "button", function() {
				var limiter = parseInt($("#limiter_trail_notifications").val());
				limiter = limiter + perPage;
				getMyAuditTrail(limiter);
			});
			$("#container_notifications").on('click', ".custom-header-link", function() {
				var status = $(this).data('status');
				var link = $(this).data('link');
				var notification_recipient_id = $(this).data('notifrecipientid');
				var new_link = "<?php echo base_url(); ?>" + link;
				if (status === 'unread') {
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>/General/readNotification",
						data: {
							recipient_ID: notification_recipient_id,
							subNotifTable: 'tbl_notification_recipient'
						},
						cache: false,
						success: function(res) {
							res = res.trim();
							res = JSON.parse(res);
							if (res.status === 'Success') {
								$(this).css('background', '#565565');
								window.location = new_link;
							} else {
								swal('Error', 'Error in reading notification', 'error');
							}

						}
					});
				} else {
					window.location = new_link;
				}
			});
			$("#container_system_notifications").on('click', ".custom-header-link", function() {
				var status = $(this).data('status');
				var link = $(this).data('link');
				var notification_recipient_id = $(this).data('systemnotifrecipientid');
				var new_link = "<?php echo base_url(); ?>" + link;
				if (status === 'unread') {
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>/General/readNotification",
						data: {
							recipient_ID: notification_recipient_id,
							subNotifTable: 'tbl_system_notification_recipient'
						},
						cache: false,
						success: function(res) {
							res = res.trim();
							res = JSON.parse(res);
							if (res.status === 'Success') {
								$(this).css('background', '#565565');
								window.location = new_link;
							} else {
								swal('Error', 'Error in reading notification', 'error');
							}

						}
					});
				} else {
					window.location = new_link;
				}
			});

			socket.on('responseTime', function(data) {
				var tz = moment.tz(new Date(data), "Asia/Manila");
				var date = moment(tz).format('MMMM DD, YYYY');
				var weekday = moment(tz).format('dddd');
				var time = moment(tz).format('hh:mm:ss A');
				var dateTime = moment(tz).format('Y-MM-DD HH:mm:ss');
				$('#header-time').html('<h2 style="line-height:0.7;margin-top:6px;font-weight:600 !important">' + time + '</h2><p style="font-size:12px;font-weight:400;line-height:1;margin-bottom:0;">' + date + ' - ' + weekday + '</p>');
				if (time == '11:59:59 PM') {
					checkDeadline();
				}
			});
			// socket.on('getipaddress', function(obj) {
			// 	$("#header-time").data('ipaddress',obj.ipaddress);
			// });
			socket.on('markAllAsRead', function(data) {

				if (data.type === 'general') {
					$("#container_general_notifications").html("");
					getLatestSystemNotifications("general");
					$("#general_notif_count").html(0);
					$("#topbar_notifications_general").find('.topbar-markread').addClass("disabled");
				} else if (data.type === 'normal') {
					$("#container_notifications").html("");
					getLatestNotifications();
					$("#request_notif_count").html(0);
					$("#topbar_notifications_notifications").find('.topbar-markread').addClass("disabled");

				} else if (data.type === 'snapsz') {
					$("#container_snapsz_notifications").html("");
					getLatestSystemNotifications("snapsz");
					$("#snapsz_notif_count").html(0);
					$("#topbar_notifications_snapsz").find('.topbar-markread').addClass("disabled");
				} else if (data.type === 'dms') {
					$("#container_dms_notifications").html("");
					getLatestSystemNotifications("dms");
					$("#dms_notif_count").html(0);
					$("#topbar_notifications_dms").find('.topbar-markread').addClass("disabled");
				}
				$("#topbar-badge").html(parseInt($("#request_notif_count").text()) + parseInt($("#general_notif_count").text()) + parseInt($("#snapsz_notif_count").text()) + parseInt($("#dms_notif_count").text()));
			});
		});
	</script>
	<script>
		var protocol = window.location.protocol;
		var pathName = window.location.pathname;
		var host = window.location.host;
		var baseUrl = '';
		var match = host.match(/\d{1,3}/g);
		var ipHost = (match != null ? match : 0);

		if (host == 'localhost' || ipHost.length == 4) {
			var pathArray = pathName.split('/');
			baseUrl = protocol + '//' + host + '/' + pathArray[1];
		} else {
			baseUrl = protocol + '//' + host;
		}
		function noImageGeneral(element) {
			$(element).attr('src', baseUrl + '/assets/images/img/sz.png');
		}
		function noImage(id) {
			$('.header-userpic' + id).attr('src', baseUrl + '/assets/images/img/sz_thumbnail.png');
		}
	</script>
	
</header>