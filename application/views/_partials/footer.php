<!-- begin::Quick Sidebar -->
<style>
    #irUL {
        cursor: pointer;
    }    /* DMS TASK CHANGES */
    .m-timeline-2:before {
        content: '';
        position: absolute;
        left: 6.2rem !important;
        width: .214rem;
        height: 100%;
    }
    .m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-text {
        color: #151515 !important;
    }
    .m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-time {
        color: #000000 !important;
    }
    .dmsTaskLink{
        color: black!important;
    }
    #dmsTaskFloat.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
        border-left-color: #FFEB3B !important;
    }
    /* DMS TASK CHANGES END */
</style>
<button id="chat_sidebar_toggle" class="m--hide"></button>
<div id="chat_sidebar" class="m-quick-sidebar m-quick-sidebar--skin-light p-0" style="overflow:hidden;z-index:2000 !important">
    <div class="m-quick-sidebar__content">
        <div id="chat_header">
            <!--<div class="m-stack m-stack--ver m-stack--general py-3 px-3" style="height: 80px !important;background-image: url('<?php echo base_url("assets/logo-chat.png"); ?>');background-size: contain;background-position: center;">-->
            <div class="m-stack m-stack--ver m-stack--general p-3" style="height: 60px !important;    background-image: linear-gradient(to right, #000000,#486071);">
                <!-- #2d404e -->
                <div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:5px">
                    <span id="chat_sidebar_close" class="m-quick-sidebar__close" style="top:0px;left:6px"><i class="la la-close" style="font-size:13px"></i></span>
                </div>
                <div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:50px">
                    <img src="<?php echo base_url($session['pic']); ?>" alt="" class="chat-header-img" style="background: #fff;" onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>';" />
                    <span class="m-badge  m-badge--green m-badge--dot badge-chat" style="margin-top: 25px;margin-left: -15px;border: 2px solid white;height:14px !important;width:14px !important"></span>
                </div>
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--fluid px-2">
                    <h5 class="mb-0" style="font-size:16px;color:#eee">
                        <?php echo $this->session->lname . ', ' . $this->session->fname; ?></h5>
                    <p class="mb-0" style="font-size:12px;color:#bbb"><?php echo $this->session->acc_name; ?></p>
                </div>
                <div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:50px">
                    <img src="<?php echo base_url("assets/chat.png"); ?>" style="width: 100%;">
                </div>

            </div>
            <ul class="nav nav-pills nav-justified mb-0" role="tablist" id="tab-chat">
                <li class="nav-item pt-1 pb-2">
                    <a class="nav-link active" data-toggle="tab" href="#tab-messages"><i class="fa fa-comments"></i>
                        MESSAGES</a>
                </li>
                <li class="nav-item pt-1 pb-2">
                    <a class="nav-link" data-toggle="tab" href="#tab-contacts"><i class="fa fa-address-book-o"></i>
                        CONTACTS</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active show" id="tab-messages" role="tabpanel">
                    <div class="pl-2 pr-1 m-scrollable">
                        <div class="list-group text-center mr-1" id="chat_userlist"></div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-contacts" role="tabpanel">
                    <div class="p-3  m-scrollable">
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" class="form-control m-input m-input--pill" placeholder="Search Contacts ..." id="chat-search-name">
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                        </div>
                    </div>
                    <div id="loader-contacts" style="margin:auto;background:'orange';width:90%">
                       <div style="float:left;margin-right:10px">
                            <div class="m-spinner m-spinner--warning"></div>
                            <div class="m-spinner m-spinner--warning"></div>
                            <div class="m-spinner m-spinner--warning"></div>
                       </div>
                        <p class="m--font-bolder m--font-italic" style="color:#888 !important;font-size:13px">Getting Contacts...</p>
                    </div>
                    <div class="pl-2 pr-1 m-scrollable">
                        <div class="list-group text-center mr-1" id="chat_searchlist"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="div-messages" style="display:none">
            <div class="m-stack m-stack--ver m-stack--general" style="border-bottom:2px solid #444;padding:15px 0;width:100%;background-image: linear-gradient(to right, #000000,#486071);">
                <div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:50px"><a href="" id="btn-backtochatlist" class="m-link m-link--state" style="color:#888"><i class="fa fa-chevron-left" style="font-size:16px;vertical-align: middle"></i></a></div>
                <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                    <h4 class="m--font-boldest cut-text mb-0 text-capitalize" id="chatmate-name" style="font-size:17px;color:white"></h4>
                </div>
            </div>

            <div class="px-3 py-0" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
                <div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
                    <div class="m-messenger__messages" data-limiter="0" data-isfirstmessage='yes'>

                    </div>

                    <div class="m-messenger__notes"></div>
                    <div class="m-messenger__seperator" style="margin-top:10px !important;"></div>

                    <div class="m-messenger__form">
                        <div class="m-messenger__form-controls">
                            <textarea name="" id="" class="m-messenger__form-input-2 form-control m-input" maxlength="500" placeholder="Type a message here..."></textarea>
                        </div>
                        <div class="m-messenger__form-tools">
                            <a href="#" class="m-messenger__form-attachment inactive">
                                <i class="la la-send"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
<!-- end:: Body -->
<footer class="m-grid__item m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                <span class="m-footer__copyright">
                    2018 &copy;<a href="#" class="m-link">SupportZebra</a>
                </span>
            </div>
           <!--  <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Company Details</span>
                        </a>
                    </li>
                    <li class="m-nav__item m-nav__item">
                        <a href="#" class="m-nav__link" data-html="true" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                            <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                        </a>
                    </li>
                </ul>
            </div> -->
        </div>
    </div>
</footer>
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<!--<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
    <i class="la la-arrow-up"></i>
</div>-->


<div class="m-scroll-top" id="chat_toggler" style="display: block">
    <!--<i class="fa fa-bolt" style="font-size:35px;color:white;margin-top:7px"></i>-->
    <span class="m-nav__link-badge m-badge" id="chat-blinker" style="position: absolute;
          right: 0px;
          top: -5px;
          padding: 1px 3px;
          color: white;
          font-weight: 500;
          background: rgb(245, 67, 55);
          border-radius: 2px;
          line-height: 15px;
          min-height: 15px;
          min-width: 16px;
          display:none;
          border:1px solid #ddd"></span>
    <img src="<?php echo base_url("assets/chat.png"); ?>" style="width: 100%;">
</div>

<ul data-toggle="modal" class="m-nav-sticky" style="margin-top: 10px;padding: 0rem;border-top-left-radius: 0.8rem;border-bottom-left-radius: 0.8rem;border-top: 1px solid rgb(202, 202, 202);border-left: 1px solid rgb(202, 202, 202);border-bottom: 1px solid rgb(202, 202, 202); cursor: pointer;display:none" id="dmsTaskFloat" data-target="#dmsTaskModal">
    <li class="m-nav-sticky__item" id="dmsTask">
        <a class="m-animate-blink" ><i class="fa fa-exclamation-circle text-warning" style="font-size: 2rem;"></i></a>
    </li>
</ul>

<ul class="m-nav-sticky" style="margin-top: 60px;border-top: 1px solid rgb(202, 202, 202);border-left: 1px solid rgb(202, 202, 202);border-bottom: 1px solid rgb(202, 202, 202);" id="irUL">
    <!-- <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="Layout Builder">
        <a href="index2b54.html?page=builder&amp;demo=default"><i class="la la-cog"></i></a>
    </li> -->
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="File" data-placement="left" data-original-title="Layout Builder" id="irLI">
        <a data-toggle="modal" data-target="#FileSpecialIRModal"><img src="<?php echo base_url("assets/images/img/ir.png"); ?>" style="width: 30px;"></a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="File" data-placement="left" data-original-title="Layout Builder" id="irLI2" style="display:none">
        <a data-toggle="modal" data-target="#FileSpecialIRModal"><img src="<?php echo base_url("assets/images/img/ir2.png"); ?>" style="width: 100%;"></a>
    </li>
    <!-- <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Showcase" data-placement="left">
        <a href=""><i class="la la-eye"></i></a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Pre-sale Chat" data-placement="left">
        <a href="" ><i class="la la-comments-o"></i></a>
    </li>
    
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="Purchase">
        <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank"><i class="la la-cart-arrow-down"></i></a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="Documentation">
        <a href="https://keenthemes.com/metronic/documentation.html" target="_blank"><i class="la la-code-fork"></i></a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="Support">
        <a href="https://keenthemes.com/forums/forum/support/metronic5/" target="_blank"><i class="la la-life-ring"></i></a>
    </li> -->
</ul>
<div class="modal fade" id="qualifiedDetailsModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">DTR Violation Details</h5>
                <button type="button" class="close closeModalBtn" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 col-lg-3">
                                <img id="empPicView" src="" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                            </div>
                            <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                <div class="row">
                                    <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="qualifiedViewEmployeeName">Jamelyn A. Sencio
                                    </div>
                                    <div class="col-md-12 text-center text-md-left"><span id="qualifiedViewEmployeeJob" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span><span id="qualifiedViewPosEmpStat" style="font-size: 11px;color: #0012ff;"></span>
                                    </div>
                                    <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="qualifiedViewEmployeeAccount">ONYX Enterprises</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="row p-2">
                            <div class="col-12">
                                <div class="row pl-4 pr-4">
                                    <span class="font-weight-bold mr-2">Incident Date:</span>
                                    <span class="" id="qualifiedViewDateIncident">Dec 4, 2018</span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row pl-4 pr-4">
                                    <span class="font-weight-bold mr-2">Incident:</span>
                                    <span class="" id="qualifiedViewIncidentType" style="color: #f50000;font-weight: 600;">Forgout to Logout</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row pl-5 pr-5" id="incidentDetailsView"></div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Expected Course Of
                                Action</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-12 pl-5 pr-5">
                            <div class="col-md-12 ahr_quote pt-3 pb-3 text-center" style="background: #e7ffe8 !important;">
                                <blockquote style="margin: 0 0 0rem;">
                                    <i class="fa fa-quote-left"></i>
                                    <span class="mb-0" id="qualifiedExpectedActionView" style="font-size: 13px"></span>
                                    <i class="fa fa-quote-right"></i>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div id="withRuleView" style="display:none">
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR Rule</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-1 font-weight-bold">
                                    Rule:
                                </div>
                                <div class="col-12 col-md-11 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">
                                    <i style="font-size: 13px; font-weight: 400;" id="qualifiedRuleView"></i>
                                </div>
                                <div class="col-12">
                                    <div class="row" id="withPreviousUserDtrViolationView">
                                        <div class="col-12 mt-2 font-weight-bold">
                                            Previous Consecutive Occurence:
                                        </div>
                                        <div class="col-12 mt-2" id="previousDetailsView">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Supervisor's Note</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-12 pl-5 pr-5">
                            <div class="row py-3" style="background: aliceblue;margin-left: 0.1rem;border-left: 8px solid #545455!important;margin-right: 0.1rem;">
                                <div class="col-sm-12 col-md-4 col-lg-2">
                                    <img id="directSupPic" src="" onerror="noImageFound(this)" width="50" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-10 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 text-center text-md-left" style="font-size: 1rem;" id="directSupName">Mark Jeffrey Magparoc</div>
                                        <div class="col-md-12 text-center text-md-left" id="directSupJob" style="font-size: 0.8rem;/* margin-top: -8px !important; */font-weight: bolder;color: darkcyan;">
                                            Senior Software Developer II</div>
                                        <div class="col-md-12 pt-3 font-italic" id="directSupNote">Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- DMS TASK CHANGES -->
<div class="modal fade" id="dmsTaskModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-balance-scale text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">DMS Process Involvement</h5>
                <button type="button" class="close closeModalBtn" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-warning alert-dismissible fade show  m-alert m-alert--air m-alert--outline m-alert--outline-2x py-2 px-3 text-dark" role="alert" style="font-size: 0.9rem !important;#f5d289 !important;">
                            You are informed to take care of the following DMS involvement. Just simply click the clickable items and you will be redirected to the specific DMS working page.                      
                        </div>
                        <!-- <p>You are informed to take care of the following DMS involvement. Just simply click the clickable items and you will be redirected to the specific DMS working page.</p> -->
                    </div>
                    <div class="col-12 px-1">
                        <div class="col-12">
                            <div class="m-timeline-2">
                                <div class="m-timeline-2__items">
                                    <div class="col-12 px-0">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item confirmWitnessDiv" style="display:none">
                                        <span class="m-timeline-2__item-time" id="confirmWitnessCount"></span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-eye m--font-danger"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text  m--padding-top-5">
                                            that <span id="witnessNeed" style="font-size: 1rem;font-weight: 400;"></span> your <i>confirmation</i> as <b>WITNESS</b></br>
                                            <span style="font-size: 0.8rem;font-weight: 400;">Click an IR ID to confirm:</span sty><br>
                                            <span style="font-size: 0.8rem;font-weight: 400;" id="confirmIds"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 confirmWitnessDiv" style="display:none">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item mySubExplanationDiv" style="display:none">
                                        <span class="m-timeline-2__item-time" id="mySubExplanationCount"></span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-pencil-square-o m--font-danger"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text">
                                            <a class="dmsTaskLink" href="<?php echo base_url('Discipline/dms_personal/subjectExplanationTab')?>">that <span id="mySubExplanationNeed" style="font-size: 1rem;font-weight: 400;"></span> your <i>explanation</i> as <b>Subject</b></a><br>
                                            <span style="font-size: 0.8rem;font-weight: 400;">Approach <span id="mySupervisor" style="font-size: 0.8rem;font-weight: 600;"></span> and provide the following IR IDs:</span><br>
                                            <span style="font-size: 0.8rem;font-weight: 400;" id="mySubExplanationIds"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 mySubExplanationDiv" style="display:none">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item subordinateExplainDiv" style="display:none">
                                        <span class="m-timeline-2__item-time" id="subordinateExplainCount">10 IR's</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-comments m--font-primary"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text">
                                            <a class="dmsTaskLink" href="<?php echo base_url('discipline/dms_supervision/irSupervision/irSup_explanation')?>">that <span id="subordinateExplainNeed" style="font-size: 1rem;font-weight: 400;"></span> to be explained by your subordinates</a></br>
                                            <span style="font-size: 0.8rem;font-weight: 400;">Please follow-up the witnesses (if added) to confirm. And then follow up subjected subordinates to provide their explanation the soonest possible:</span><br>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 subordinateExplainDiv" style="display:none;">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item fileDtrViolation" style="display:none">
                                        <span class="m-timeline-2__item-time" id="fileDtrViolationCount">10 IR's</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-file-text-o m--font-primary"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text pb-3">
                                           <a class="dmsTaskLink" href="<?php echo base_url('discipline/dms_supervision/dtrViolations/fileDtrViolationIrTab')?>">that <span class="fileDtrViolationLinking" style="font-size: 1rem;font-weight: 400;"></span> <b>DTR RELATED</b>, and <span class="fileDtrViolationLinking" style="font-size: 1rem;font-weight: 400;"></span> now ready for <i>IR Filing</i>.
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 fileDtrViolation" style="display:none;">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item myRecommendationDiv" style="display:none">
                                        <span class="m-timeline-2__item-time" id="recommendationCount">10 IR's</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-users m--font-primary"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text">
                                            <a class="dmsTaskLink" href="<?php echo base_url('discipline/dms_supervision/irSupervision/irSup_recommendation')?>"> that <span id="recommendationNeed" style="font-size: 1rem;font-weight: 400;"></span> your <i>recommendation</i> as <b>RECOMMENDING SUPERVISOR</b></a>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 myRecommendationDiv" style="display:none;">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item myClientRecommendationDiv" style="display:none">
                                        <span class="m-timeline-2__item-time" id="clientRecommendationCount">10 IR's</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-superpowers m--font-primary"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text">
                                            <a class="dmsTaskLink" href="<?php echo base_url('discipline/dms_supervision/irSupervision/irSup_clientConfirmation')?>">that <span id="clientRecommendationNeed" style="font-size: 1rem;font-weight: 400;"></span> your <i>recommendation</i> according to the <b>CLIENT'S CONFIRMATION</b></a>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 myClientRecommendationDiv" style="display:none;">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item effectivityDatesDiv" style="display:none">
                                        <span class="m-timeline-2__item-time"id="effectivityDatesCount">10 IR's</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-calendar m--font-primary"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text">
                                            <a class="dmsTaskLink" href="<?php echo base_url('discipline/dms_supervision/irSupervision/irSup_setDates')?>"> that <span id="effectivityDatesNeed" style="font-size: 1rem;font-weight: 400;"></span> to be <i>set</i> with the specified <b>EFFECTIVITY DATE/S</b> </a>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 effectivityDatesDiv" style="display:none;">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item eroRecommendationDiv" style="display:none">
                                        <span class="m-timeline-2__item-time" id="eroRecommendationCount">10 IR's</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-legal m--font-success"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text">
                                            <a class="dmsTaskLink" href="<?php echo base_url('discipline/dms_ero/eroRecommendationTab')?>">that <span id="eroRecommendationNeed" style="font-size: 1rem;font-weight: 400;"></span> your <i>recommendation</i> as <b>EMPLOYEE RELATION'S OFFICER</b></a>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 eroRecommendationDiv" style="display:none;">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                    <div class="m-timeline-2__item uploadDocumentsDiv" style="display:none">
                                        <span class="m-timeline-2__item-time" id="uploadDocumentsCount">10 IR's</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i class="fa fa-file-pdf-o m--font-success"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text uploadDocumentsDiv" style="display:none;">
                                            <a class="dmsTaskLink" href="<?php echo base_url('discipline/dms_ero/eroUploadTab')?>"> that <span id="uploadDocumentsNeed" style="font-size: 1rem;font-weight: 400;"></span> to be <i>uploaded</i> with the <b>NECESSARY DOCUMENTS</b></a>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0 uploadDocumentsDiv" style="display:none;">
                                        <hr style="margin-top: 0.2rem !important;border-style: dashed;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- DMS TASK CHANGES END -->


<div class="modal fade" id="irRecommendationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <form id="qualifiedDtrVioForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Recommendation Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close closeModalBtn" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
                        <div id="recommendationDetails"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="irAnnexModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="qualifiedDtrVioForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Similar Incident Reports</h5>
                    <button type="button" class="close closeModalBtn" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body px-4 py-0">
                    <div class="row py-3" style="background: #f1f1f1;margin-right: -21px !important;margin-left: -21px !important;border-bottom: 1px solid #d4d4d4;">
                        <div class="col-12 col-md-9">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-3"><img src="" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block" id="annexSubPic"></div>
                                <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="annexSubName"></div>
                                        <div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="annexSubPosition"><span style="font-size: 11px;color: #0012ff;" id="annexSubPositionStat"></span></div>
                                        <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="annexSubAccount"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="row mx-2 mt-md-0 mt-2" style="border-style: dashed !important;border: 2px #ff9393;background: #ffdada;">
                                <div class="col-6 col-md-12 text-right text-md-center font-weight-bold" style="font-size: 2.3rem;" id="annexCount"></div>
                                <div class="col-6 col-md-12 text-md-center pt-3 px-0 pt-md-0" id="annexLabel"></div>
                            </div>
                        </div>
                    </div>
                    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="320">
                        <div class="m-widget3" id="annexDetails">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Charise start code -->
<!--begin: Charise :Modal-->
<div class="modal fade" id="modal_displayevidences" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header custom-evidence-modal">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #fff !important;">
                    <!-- <i class="fa fa-paperclip"></i>  -->
                    Evidences
                </h5>
                <button type="button" id="media_closebutton2" class="close closeModalBtn" data-dismiss="modal" aria-label="Close" style="color: #fff !important;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mb-5" style="margin-top:-21px !important">
                <div class="m-portlet__body">
                    <div class="row" data-irid="0" id="row_irid">
                        <div class="col-lg-12 col-md-12 ">
                            <label class="label_attachment" id="label_attach"></label>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <label class="label_select" id="">Attachment Types</label>
                                    <select class="form-control m-select" id="evidence-types">
                                        <option value="">All</option>
                                        <option value="1">Photos</option>
                                        <option value="2">Videos</option>
                                        <option value="3">Audios</option>
                                        <option value="4">Files</option>
                                        <option value="5">Others</option>
                                    </select>
                                </div>
                                <!-- 
                                <div class="col-lg-6 col-md-12">
                                    <label class="label_select" id="">Attachment Purpose</label>
                                    <select class="form-control m-select" id="attachment-purpose">
                                        <option value="">All</option>
                                        <option value="1">Evidences</option>
                                        <option value="4">HR attachment</option>
                                    </select>
                                </div> -->
                            </div>
                            <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-5 text-center" style="max-height: 250px; height: 250px; position: relative; overflow: visible;padding: 13px;">
                                <div class="row" id="media_display">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- File an IR modal -->

<!-- start: charise modal -->
<div class="modal fade" id="FileSpecialIRModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="specialIRForm" name="specialIRForm" method="post" data-formtype="add">
            <div class="modal-content">
                <div class="modal-header" style="padding: 18px !important;">
                    <div class="col-8">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <!-- <i class="fa fa-user text-light"
                                style="font-size: 21px;"></i>&nbsp;&nbsp;  -->
                            File Incident Report</h5>
                        <div id="qip_check" style="display: none">&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #80e49b;"><i class="fa fa-check-circle" style="color: #80e49b;"></i> QIP
                                Assigned</span></div>
                    </div>
                    <div class="col-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;"><span aria-hidden="true">×</span></button>
                    </div>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-3" style="margin-top:auto;margin-bottom:auto;">
                                    <img id="empPicSpecialir" src="http://3.18.221.50/sz/assets/images/img/sz.png" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                    <div class="row">
                                        <div class="form-group col-md-10 mt-1 mb-1 text-center text-md-left select2-container" style="font-size: 19px;" id="empName">
                                            <select class="form-control m-select2" id="employee_names_irSub" name="employee_names_irSub" onchange="evaluate_employeeselected()">
                                            </select>
                                        </div>
                                        <div class="col-md-10 text-center text-md-left ml-sm-3" style="font-size: 15px;font-weight: bolder;color: darkcyan;" id="positionDesc">{ Position }</div>
                                        <div class="col-md-10 mb-1 text-center text-md-left ml-sm-3" style="font-size: 15px" id="accountName">{ Account / Department }</div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div id="selectname_filereport" class="row" style="display: none">
                                <div class="col-12">
                                    <!-- <div class="row pl-5 pr-5">
                                        <div class="col-12 font-weight-bold"> Subject ( Person of Interest ) :</div>
                                        <div class="col-12 col-md-9" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType">
                                            <select class="form-control m-select2" id="employee_names_irSub" onchange="evaluate_employeeselected()">
                                            </select>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row pl-5 pr-5">
                                        <div class="col-12 text-center font-weight-bold" id="incident_label" data-incident="1">Filing Type:</div>
                                        <div class="col-12 text-center">
                                            <h5 style="font-size: 91% !important" class="editableStyle" data-column="Type of Incident" id="incident_select" data-escalationid="7" onmouseover="selectPopupIncidentType(this)">NORMAL
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" id="discipline_details" data-dcid="0" data-dcsid="0">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <br>
                            <div class="row pl-5 pr-5">
                                <div class="form-group col-6" id="dom_incident">
                                    <div class="font-weight-bold" >Date of Incident:</div><input name="incidentDate" placeholder="Click to select date of incident" id="incidentDate" onmouseover="fetchdatepicker_ir()" class='form-control m-input input_customize incidentclass' readonly>
                                </div>
                                <div class="form-group col-6">
                                    <div class="font-weight-bold">Time of Incident:</div><input id="incidentTime" placeholder="Click to select time of incident" onmouseover="fetchtimepicker()" class='form-control m-input input_customize' name="incidentTime" readonly>
                                </div>
                            </div>
                            <div class="row pl-5 pr-5">
                                <div class="col-12 font-weight-bold">
                                    Description of Incident:
                                </div>
                                <div class="form-group col-12">
                                    <textarea class='form-control m-input' name='descriptionIncident' id='descriptionIncident' rows='1' data-min-rows='1' style='resize: none; overflow: hidden; height: 55px;background-color: #f1f1f1' placeholder='Type incident description here...' data-mentions-input='true'></textarea>
                                </div>
                                <br><br><br>
                                <div class="col-12 font-weight-bold">
                                    Place of Incident:
                                </div>
                                <div class="form-group col-12">
                                    <input type="text" class='form-control m-input input_customize' name='placeIncident' id='placeIncident' rows='1' placeholder='Type place of incident here...' data-mentions-input='true'>
                                </div>
                                <br><br><br>
                                <div class="col-12 font-weight-bold">
                                    Expected Course of Action:
                                </div>
                                <div class="form-group col-12">
                                    <textarea class='form-control m-input' name='courseactionIncident' id='courseactionIncident' rows='1' data-min-rows='1' style='resize: none; overflow: hidden; height: 55px;background-color: #f1f1f1' placeholder='Type expected course of action here...' data-mentions-input='true'></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div id="offense_det_action" data-filing="0" class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Offense Details and
                                    Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5" id="alert_notif" style="display: none">
                            </div><br>
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold" id="natureoffense" data-ongoing="0">Nature
                                    of Offense:</div>
                                <div class="form-group col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType">
                                    <select class="form-control m-select2" name="offensetypes" id="offense-types">
                                    </select>
                                </div><br><br><br>
                                <div class="col-12 col-md-3 font-weight-bold">Offense:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="">
                                    <select class="form-control m-select2" name="offenses_select" id="offenses">
                                    </select>
                                </div><br><br><br>
                                <div class="col-12 col-md-3 font-weight-bold">Category:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="">
                                    <select class="form-control m-select2" name="category_select" id="categories">
                                    </select>
                                </div><br><br>
                                <span id="pendingtext_display" class="text-center pendingIRlabel" data-pendingir="0"></span>
                            </div>
                        </div>
                        <div id="withIrHistory_display" class="col-12" style="display: none" data-prescid="0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row p-3">
                                        <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Incident Report
                                            History</span>
                                        <div class="col">
                                            <hr style="margin-top: 0.8rem !important;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row pl-5 pr-5">
                                        <div class="col-12 mt-2" id="irHistoryDetails">
                                            <table class="table m-table table-hover table-sm table-bordered" id="table-IR" style="font-size:13px">
                                                <thead style="background: #555;color: white;">
                                                    <tr>
                                                        <th class="text-center">IR-ID</th>
                                                        <th class="text-center">Incident Date</th>
                                                        <th class="text-center">Suggested</th>
                                                        <th class="text-center">Final</th>
                                                        <th class="text-center">Cure Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="irhistory_content">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" id="suggested_dis_label" data-occurence="0">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Suggested Disciplinary
                                    Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-4" id="suggested_dis_display" data-dacategoryid="0" data-periodmonth="0">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold">Offense Level:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="offense_level">--</div><br><br>
                                <div class="col-12 col-md-3 font-weight-bold">Disciplinary Action:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 15px; font-weight: 400;color: darkcyan;" id="disciplinary_action">
                                    --</div><br><br>
                                <div id="period_label" class="col-12 col-md-3 font-weight-bold">Period:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="period_month" data-period="0">--</div><br><br>
                                <div id="cureend_label" class="col-12 col-md-3 font-weight-bold">Cure End:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="pres_end" data-cend="0" data-term="0">--</div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Witnesses </span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <br>
                            <div class="row pl-5 pr-5">
                                <select class="form-control m-select2" id="witnesses_select" placeholder="Select Multiple Witnesses" name="qualifiedWitnesses" style="width: 100% !important" multiple>
                                    <option value="1">hi</option>
                                    <option value="2">hello</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-paperclip" style="font-size: 1.1rem;"></i> Evidences </span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <br>
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400;text-align: center;margin:auto" id="qualifiedPeriod">
                                    <span>Each file to attach should not exceed to <strong style="color:#558e82;">5MB.</strong> Allowed file types: <strong style="color:#c096ca"> doc, docx, pdf, txt, xml, xlsx, xlsm, ppt, pptx,
                                            jpeg, jpg, png, gif, mp4, mov, 3gp, wmv, avi, mpeg, mp3, wav.
                                        </strong> <br>If you wish to upload <strong style="color:#558e82"> LINKS
                                        </strong>, we advise you to copy and paste them in a text file then upload
                                        it.</span>
                                    <br><br>
                                    <label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent" id="add_evidences_button"><i class="fa fa-plus"></i> Upload Evidences
                                        <input type="file" name="upload_evidence_files[]" id="upload_evidence_files_fileir" style="display: none;" onchange="makeFileList_fileir()" multiple>
                                    </label>
                                    <br><label id="files_select_label_fileir" style="color:green;"></label>
                                </div><br>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400;margin:auto;text-align:-webkit-left" id="">
                                    <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-3 col-12" style="max-height: 100px; height: 100px; position: relative; overflow: visible;display:none" id="display_div_file_fileir">
                                        <ol id="fileList_fileir">
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i>
                        Close</button>
                    <button type="submit" id="submitIRbutton" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"><i class="fa fa-check"></i>File IR</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="modal fade" id="uploadExplanationModal_general" tabindex="-1" aria-labelledby="exampleModalLabel" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content custom_modal_forpdf">
            <div class="modal-body">
                <div id="explanationpdf_display"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color:white">x</span></button>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="uploadExplanationModal_general" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <form id="qualifiedDtrVioForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Recommendation Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div> -->
<!-- end:Charise :Modal -->

<div class="modal fade yu" id="audio_modal_evidences" tabindex="-1" role="dialog" aria-labelledby='exampleModalLabel' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content custom_modal'>
            <div class='modal-body'>
                <div class='col-12' id="audio_evidence_player">
                    <!-- <audio controls style='width: -webkit-fill-available;'>
                        <source src='" + baseUrl + "/" + data.link +"' type='audio/mp3'>
                        <source src='" + baseUrl + "/" + data.link +"' type='audio/mpeg'>
                        <source src='" + baseUrl + "/" + data.link +"' type='audio/3gp'>
                    </audio> -->
                </div>
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i>
                        Close</button> -->
                <button type='button' class='btn btn-link' data-dismiss="modal">Close</button>";
            </div>
        </div>
    </div>
</div>




<!-- Charise end code -->

</body>
<script type="text/javascript">
    $("#irUL").mouseover(function() {
        $("#irUL").css("width", "100px");
        $("#irLI").css("display", "none");
        $("#irLI2").css("display", "block");
    });
    $("#irUL").mouseout(function() {
        $("#irUL").css("width", "40px");
        $("#irLI").css("display", "block");
        $("#irLI2").css("display", "none");
    });
    var messagePerPage = 50;
    var isTooltipSmall = false;
    $('.m-messenger__form').on('change keyup keydown paste cut', '.m-messenger__form-input-2', function() {
        var original = $('.m-messenger__form').data('originalheight');
        if ($(this).val().trim() === '') {
            $(".m-messenger__messages").height(original);
            $(this).css('height', '40px');
        } else {
            $(".m-messenger__messages").height(original - $(this).height() + 20);
            $(this).css('height', this.scrollHeight);
        }

        var custom_spinners =
            '<span class="custom-spinner"><div class="custom-bounce1"></div><div class="custom-bounce2"></div><div class="custom-bounce3"></div></span>';
        if ($.trim($(this).val()) === '') {
            $(".m-messenger__form-attachment").removeClass('active');
            $(".m-messenger__form-attachment").addClass('inactive');
            socket.emit('typing', {
                message: "",
                receiver_ID: $("#chatmate-name").data('emp_id'),
                sender_ID: "<?php echo $this->session->emp_id; ?>"
            });
        } else {
            $(".m-messenger__form-attachment").removeClass('inactive');
            $(".m-messenger__form-attachment").addClass('active');
            socket.emit('typing', {
                message: '<?php echo $this->session->fname; ?> is typing ' + custom_spinners,
                receiver_ID: $("#chatmate-name").data('emp_id'),
                sender_ID: "<?php echo $this->session->emp_id; ?>"
            });
        }
    }).find('.m-messenger__form-input-2').change();
    var chatSidebar = function() {
        var topbarAsideChat = $('#chat_sidebar');
        var topbarAsideToggleChat = $('#chat_sidebar_toggle');

        var initMessages = function() {
            var messenger = $('#m_quick_sidebar_tabs_messenger');

            if (messenger.length === 0) {
                return;
            }

            var messengerMessages = messenger.find('.m-messenger__messages');
            var init = function() {
                var height = topbarAsideChat.outerHeight(true) - 10 - 160;
                messengerMessages.css('height', height);
                $('.m-messenger__form').data('originalheight', height);
                var height2 = topbarAsideChat.outerHeight(true) - 10 - 180;
                var height3 = topbarAsideChat.outerHeight(true) - 10 - 110;
                var divContacts = $("#chat_searchlist").parent();
                var divMessages = $("#chat_userlist").parent();
                divContacts.css('height', height2);
                divMessages.css('height', height3);

                divContacts.mCustomScrollbar({
                    scrollInertia: 0,
                    autoDraggerLength: true,
                    autoHideScrollbar: true,
                    autoExpandScrollbar: false,
                    alwaysShowScrollbar: 0,
                    alwaysTriggerOffsets: false,
                    axis: divContacts.data('axis') ? divContacts.data('axis') : 'y',
                    mouseWheel: {
                        scrollAmount: 120,
                        preventDefault: true
                    },
                    setHeight: (height2 ? height2 : ''),
                    theme: "minimal-dark"
                });

                divMessages.mCustomScrollbar({
                    scrollInertia: 0,
                    autoDraggerLength: true,
                    autoHideScrollbar: true,
                    autoExpandScrollbar: false,
                    alwaysShowScrollbar: 0,
                    alwaysTriggerOffsets: false,
                    axis: divMessages.data('axis') ? divMessages.data('axis') : 'y',
                    mouseWheel: {
                        scrollAmount: 120,
                        preventDefault: true
                    },
                    setHeight: (height3 ? height3 : ''),
                    theme: "minimal-dark"
                });

                messengerMessages.mCustomScrollbar({
                    scrollInertia: 0,
                    autoDraggerLength: true,
                    autoHideScrollbar: true,
                    autoExpandScrollbar: false,
                    alwaysShowScrollbar: 0,
                    alwaysTriggerOffsets: false,
                    axis: messengerMessages.data('axis') ? messengerMessages.data('axis') : 'y',
                    theme: "minimal-dark",
                    setTop: "1000px",
                    mouseWheel: {
                        scrollAmount: 70,
                        preventDefault: true
                    },
                    setHeight: (height ? height : ''),
                    callbacks: {
                        onTotalScrollBackOffset: 0,
                        alwaysTriggerOffsets: false,
                        onTotalScrollBack: function() {
                            //                                if (this.mcs.top === 0) {
                            var limiter = messengerMessages.data('limiter');
                            //                                    var messagesContainer = $(".m-messenger__messages").find(".mCSB_container");
                            var messagesContainer = this.mcs.content;

                            var oldContentHeight = $(".m-messenger__messages .mCSB_container")
                                .innerHeight();

                            var emp_id = $("#chatmate-name").data('emp_id');
                            var img = $("#chatmate-name").data('img');
                            $.ajax({
                                type: "GET",
                                url: socket_link +
                                    '/api/getMessages/<?php echo $this->session->emp_id; ?>/' +
                                    emp_id + '/' + limiter + '/' + messagePerPage,
                                cache: false,
                                //                                        beforeSend: function () {
                                //                                            mApp.block(".m-messenger__messages", {
                                //                                                overlayColor: '#000000',
                                //                                                type: 'loader',
                                //                                                state: 'brand',
                                //                                                size: 'lg'
                                //                                            });
                                //                                        },
                                success: function(res) {
                                    var messages = res.messages;
                                    if (messages.length === 0) {

                                    } else {

                                        messagesContainer.prepend(
                                            '<div id="chatcontainer-' + limiter +
                                            '"></div>');
                                        var chatContainer = $('#chatcontainer-' +
                                            limiter);

                                        var previousDate = $(
                                            ".m-messenger__datetime:first").data(
                                            'datetime');
                                        var previousHourMinute = $(
                                            ".m-messenger__wrapper:first").data(
                                            'timevalue');
                                        var previousType = ($(
                                                ".m-messenger__message:first")
                                            .hasClass('m-messenger__message--out')
                                        ) ? "OUT" : "IN";
                                        var keys = Object.keys(messages).reverse();

                                        var isDatetimeRemoved = false;
                                        if (previousDate === moment(messages[0]
                                                .createdOn).format("YYYY-MM-DD")) {
                                            //                                                    $(".m-messenger__datetime:first").css('visibility','hidden');
                                            $(".m-messenger__datetime:first").hide();
                                            isDatetimeRemoved = true;
                                        }

                                        var temptype = (messages[0].sender_ID + '' ===
                                            "<?php echo $this->session->emp_id; ?>"
                                        ) ? "OUT" : "IN";
                                        if (previousType === temptype) {
                                            if (previousHourMinute === moment(moment(
                                                    messages[0].createdOn).format(
                                                    'YYYY-MM-DD hh:mm')).valueOf()) {
                                                $(".m-messenger__wrapper").first().find(
                                                    '.m-messenger__message-username'
                                                ).remove();
                                                $(".m-messenger__wrapper").first().find(
                                                        '.m-messenger__message-pic img')
                                                    .remove();
                                            }
                                        }
                                        var adjustPixelsforDatetime = isDatetimeRemoved;
                                        $.each(keys, function(i, key) {
                                            var newDateFormatted =
                                                chatDateFormat(messages[key]
                                                    .createdOn, res.datetime);
                                            var newDate = moment(messages[key]
                                                .createdOn).format(
                                                "YYYY-MM-DD");
                                            var newTime = moment(messages[key]
                                                .createdOn).format(
                                                "hh:mm A");
                                            var newTimeValue = moment(moment(
                                                        messages[key].createdOn)
                                                    .format('YYYY-MM-DD hh:mm'))
                                                .valueOf();
                                            var type = (messages[key]
                                                .sender_ID + '' ===
                                                "<?php echo $this->session->emp_id; ?>"
                                            ) ? "OUT" : "IN";


                                            var tempString = "";
                                            if (previousDate !== newDate ||
                                                isDatetimeRemoved === true) {
                                                chatContainer.append(
                                                    '<div class="m-messenger__datetime" data-datetime="' +
                                                    newDate +
                                                    '" style="position:relative;font-weight:400"><hr style="background:#efefef" /><div style="position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);background:white;padding:0 10px;color:#8A8D91;font-size:11px">' +
                                                    newDateFormatted +
                                                    '</div></div>');
                                                isDatetimeRemoved = false;
                                            }

                                            if (previousType !== type) {
                                                var isInsert = true;
                                            } else {
                                                if (previousHourMinute !==
                                                    newTimeValue) {
                                                    var isInsert = true;
                                                } else {
                                                    var isInsert = false;
                                                }
                                            }
                                            if (isInsert) {
                                                if (type === "IN") {
                                                    tempString +=
                                                        '<div class="m-messenger__wrapper" data-timevalue="' +
                                                        newTimeValue + '">'

                                                        +
                                                        '<div class="m-messenger__message m-messenger__message--in">' +
                                                        '<div class="m-messenger__message-username">' +
                                                        newTime +
                                                        '</div>' +
                                                        '<div class="m-messenger__message-pic">' +
                                                        '<img src="<?php echo base_url() ?>' +
                                                        img + '"  alt=""/>' +
                                                        '</div>' +
                                                        '<div class="m-messenger__message-body">'

                                                        +
                                                        '<div class="m-messenger__message-arrow"></div>' +
                                                        '<div class="m-messenger__message-content">'

                                                        +
                                                        '<div class="m-messenger__message-text">' +
                                                        unescape(messages[key].message) +
                                                        '</div>' +
                                                        '</div>' +
                                                        '</div>' +
                                                        '</div>' +
                                                        '</div>';

                                                } else {
                                                    tempString +=
                                                        '<div class="m-messenger__wrapper" data-timevalue="' +
                                                        newTimeValue + '">'

                                                        +
                                                        '<div class="m-messenger__message m-messenger__message--out">' +
                                                        '<div class="m-messenger__message-username">' +
                                                        newTime +
                                                        '</div>' +
                                                        '<div class="m-messenger__message-body">'

                                                        +
                                                        '<div class="m-messenger__message-arrow"></div>' +
                                                        '<div class="m-messenger__message-content">' +
                                                        '<div class="m-messenger__message-text">' +
                                                        unescape(messages[key].message) +
                                                        '</div>' +
                                                        '</div>' +
                                                        '</div>' +
                                                        '</div>' +
                                                        '</div>';
                                                }


                                            } else {
                                                if (type === "IN") {
                                                    $('<div class="m-messenger__wrapper" data-timevalue="' +
                                                            newTimeValue + '">'

                                                            +
                                                            '<div class="m-messenger__message m-messenger__message--in">' +
                                                            '<div class="m-messenger__message-pic">' +
                                                            '</div>' +
                                                            '<div class="m-messenger__message-body">'

                                                            +
                                                            '<div class="m-messenger__message-arrow"></div>' +
                                                            '<div class="m-messenger__message-content">'

                                                            +
                                                            '<div class="m-messenger__message-text">' +
                                                            unescape(messages[key].message) +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>')
                                                        .insertAfter($(
                                                                chatContainer)
                                                            .find(
                                                                ".m-messenger__wrapper:last"
                                                            ));
                                                } else {
                                                    $('<div class="m-messenger__wrapper" data-timevalue="' +
                                                            newTimeValue + '">'

                                                            +
                                                            '<div class="m-messenger__message m-messenger__message--out">' +
                                                            '<div class="m-messenger__message-body">'

                                                            +
                                                            '<div class="m-messenger__message-arrow"></div>' +
                                                            '<div class="m-messenger__message-content">' +
                                                            '<div class="m-messenger__message-text">' +
                                                           unescape( messages[key].message) +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>')
                                                        .insertAfter($(
                                                                chatContainer)
                                                            .find(
                                                                ".m-messenger__wrapper:last"
                                                            ));
                                                }

                                            }

                                            chatContainer.append(tempString);
                                            previousDate = newDate;
                                            previousHourMinute = newTimeValue;
                                            previousType = type;

                                            $(".m-messenger__form-attachment")
                                                .removeClass('active');
                                            $(".m-messenger__form-attachment")
                                                .addClass('inactive');
                                        });


                                        $('.m-messenger__message-pic img').on('error',
                                            function() {
                                                $(this).attr('onerror', null);
                                                $(this).attr('src',
                                                    '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>'
                                                );
                                            });
                                        $(".m-messenger__messages").data('limiter',
                                            limiter + messagePerPage);

                                        var heightDiff = $(
                                            ".m-messenger__messages .mCSB_container"
                                        ).innerHeight() - oldContentHeight;
                                        $(".m-messenger__messages").mCustomScrollbar(
                                            "update");
                                        $(".m-messenger__messages").mCustomScrollbar(
                                            "scrollTo", "-=" + heightDiff, {
                                                scrollInertia: 0,
                                                timeout: 0
                                            });

                                    }
                                    //                                            mApp.unblock(".m-messenger__messages");
                                }
                            });
                            //                                }
                        }
                    }
                });
            };

            init();

            // reinit on window resize
            mUtil.addResizeHandler(init);
        };
        var initOffcanvasChat = function() {
            topbarAsideChat.mOffcanvas({
                class: 'm-quick-sidebar',
                overlay: true,
                toggle: topbarAsideToggleChat,
                close: chat_sidebar_close
            }).on('click', function() {
                alert("HERE");
            });
            initMessages();
        };
        return {
            init: function() {
                if (topbarAsideChat.length === 0) {
                    return;
                }
                initOffcanvasChat();
            }
        };
    }();
    $(function() {
        getBlinkerData();
        chat_display();
        getDmsCounts();
        if (parseInt('<?php echo $this->session->chatPopover; ?>') != 0) {
            var messages = [{
                    title: "Hi There!!",
                    content: "Would you like to share something to us?"
                },
                {
                    title: "Nice to see you.",
                    content: "Feel free to ask me anything and I'll try to answer."
                },
                {
                    title: "Good Morning!!",
                    content: "I'd like to chat with you."
                },
                {
                    title: "Hello!",
                    content: "I am very excited to talk with you."
                }
            ];
            var minimum = 0;
            var maximum = messages.length - 1;
            var randomnumber = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
            $("#chat_toggler").data('fromClick', false);
            $("#chat_toggler").popover({
                container: 'body',
                trigger: 'click',
                placement: 'top',
                html: true,
                template: '\
                    <div class="m-popover popover m-popover--skin-light popover-chat-parent" role="tooltip">\
                    <h3 class="popover-header"></h3>\
                    <div class="popover-body popover-chat"></div>\
                    </div>',
                content: '<div class="m-stack m-stack--ver m-stack--general"><div class="m-stack__item m-stack__item--center m-stack__item--top p-2" style="width:20px"><span class="popover-chat-close"><i class="fa fa-times-circle"></i></span></div><div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--fluid p-2"><h5 class="popover-chat-title ml-2">' +
                    messages[randomnumber].title + '</h5><p class="popover-chat-content mb-0 ml-2">' +
                    messages[randomnumber].content +
                    '</p></div><div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:55px"><img src="<?php echo base_url('uploads/profiles/szprofile_980.jpg') ?>" alt="" class="popover-chat-img"/></div></div>'
            });
            $("#chat_toggler").popover('show');
            $('body').scrollLeft(20);
            $("#chat_toggler").on('hide.bs.popover', function() {
                if ($(this).data('fromclick')) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        chatSidebar.init();

    });

     function chat_display(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>Meszenger/chat_display",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if(data.status!=0){
                    $('#chat_toggler').slideDown();
                }else{
                    $('#chat_toggler').slideUp();
                }
            }
        });
    }

    function chatDateFormat(createdOn, currentDate, mini = false) {
        if (mini) {
            var newDateFormatted = moment(createdOn).format("M/DD/YYYY");

            if (moment(createdOn).format('YYYY-MM-DD') === moment(currentDate).format('YYYY-MM-DD')) {
                newDateFormatted = moment(createdOn).format('hh:mm A');
            } else if ((moment(createdOn).format('YYYY-MM-DD') === moment(currentDate).subtract(1, 'day').format(
                    'YYYY-MM-DD'))) {
                newDateFormatted = 'Yesterday';
            } else if ((moment(createdOn).format('YYYY-MM-DD') >= moment(currentDate).subtract(7, 'day').format(
                    'YYYY-MM-DD'))) {
                newDateFormatted = moment(createdOn).format('dddd');
            }
        } else {
            var newDateFormatted = moment(createdOn).format("dddd, MMM DD, YYYY");

            if (moment(createdOn).format('YYYY-MM-DD') === moment(currentDate).format('YYYY-MM-DD')) {
                newDateFormatted = 'Today';
            } else if ((moment(createdOn).format('YYYY-MM-DD') === moment(currentDate).subtract(1, 'day').format(
                    'YYYY-MM-DD'))) {
                newDateFormatted = 'Yesterday';
            } else if ((moment(createdOn).format('YYYY-MM-DD') >= moment(currentDate).subtract(7, 'day').format(
                    'YYYY-MM-DD'))) {
                newDateFormatted = moment(createdOn).format('dddd');
            }
        }

        return newDateFormatted;
    }

    function getChatMates() {
        var userContainer = $("#chat_userlist");
        $.ajax({
            type: "GET",
            url: socket_link + '/api/getChatMates/<?php echo $this->session->emp_id; ?>',
            cache: false,
            success: function(res) {
                var string = "";
                userContainer.html("");
                $.each(res.chatmates, function(i, row) {
                    var isOnline = false;
                    $.each(res.online, function(ii, user) {
                        if (user.emp_id === row.emp_id) {
                            isOnline = true;
                        }
                    });
                    var pic = row.pic? row.pic.split("."):[];
                    var img = pic.length>0?pic[0] + "_thumbnail.jpg": null ;
                    var chatmate_status = (isOnline) ? "m-badge--green" : "";
                    var formattedDate = '<p class="last-chat-time">' + chatDateFormat(row.createdOn, res
                        .datetime, true) + '</p>';
                    var withUnread = (row.totalUnread === 0) ? '' :
                        '<p class="mb-0 text-right" style="margin-right:10px"><span class="m-badge m-badge--wide badge-unread">' +
                        row.totalUnread + '</span></p>';
                    var fontweight = (row.totalUnread === 0) ? '' : 'font-weight:800;color:#444';
                    string +=
                        '<a href="#" class="list-group-item list-group-item-action px-0" style="padding:8px 0;" data-email="' +
                        row.email + '" data-img="' + img + '" data-emp_id="' + row.emp_id +
                        '" data-fname="' + row.fname + '" data-chatstatus="' + chatmate_status +
                        '"  data-lname="' + row.lname +
                        '"><div class="m-stack m-stack--ver m-stack--general"><div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:60px">' +
                        '<img data-src="<?php echo base_url() ?>' + img +
                        '" class="img-chatmate lazyload" alt="" /><span class="m-badge  ' +
                        chatmate_status +
                        ' m-badge--dot badge-chat" style="margin-top: 24px;margin-left: -10px;border: 2px solid white;height:14px !important;width:14px !important" data-emp_id="' +
                        row.emp_id + '"></span></div>' +
                        '<div class="m-stack__item m-stack__item--center m-stack__item--middle"><p class="chat-username cut-text text-capitalize" style="' +
                        fontweight + '">' + row.lname + ', ' + row.fname +
                        '</p><p class="chat-lastmessage cut-text">' + unescape(row.message) + '</p></div>' +
                        '<div class="m-stack__item m-stack__item--center m-stack__item--middle"  style="width:80px">' +
                        formattedDate + '' + withUnread + '</div>' +
                        '</div></a>';
                });

                userContainer.html(string);
                if (string === '') {
                    userContainer.html(
                        '<div class="p-2"><div class="m-alert m-alert--outline alert alert-info" role="alert">No recent messages... </div></div>'
                    );

                } else {
                    userContainer.html(string);
                }
                $("#chat_userlist").find();


                $('.img-chatmate').on('error', function() {
                    $(this).attr('onerror', null);
                    $(this).attr('src',
                        '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
                });

            }
        });
    }

    function getOnlineUsers(callback) {
        $.ajax({
            type: "GET",
            url: socket_link + '/api/getOnlineUsers',
            cache: false,
            success: function(res) {
                callback(res);
            }
        });

    }

    function getBlinkerData() {
        $.ajax({
            type: "GET",
            url: socket_link + '/api/getBlinkerData/<?php echo $this->session->emp_id; ?>',
            cache: false,
            success: function(res) {
                var unreadCount = res.rows[0].count;
                if (unreadCount === 0) {
                    $("#chat-blinker").hide();
                } else {
                    $("#chat-blinker").html(unreadCount);
                    $("#chat-blinker").show();

                }
            }
        });

    }

    function unique_array(array) {
        return array.filter(function(el, index, arr) {
            return index == arr.indexOf(el);
        });
    }

    function getChatContacts() {

        var userContainer = $("#chat_searchlist");

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('General/getChatContacts'); ?>",
            data: {
                emp_id: "<?php echo $this->session->emp_id; ?>"
            },
             beforeSend: function () {
              $("#loader-contacts").show();
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res);
                getOnlineUsers(function(socketObj) {
                    var string = "";
                    var previousLetter = ''; //A
                    userContainer.html("");
                    $.each(res.results, function(i, row) {
                        var isOnline = false;
                        $.each(socketObj.online, function(ii, user) {
                            if (user.emp_id + '' === row.emp_id + '') {
                                isOnline = true;
                            }
                        });
                   var pic = row.pic? row.pic.split("."):[];
                    var img = pic.length>0?pic[0] + "_thumbnail.jpg": null ;
                        var chatmate_status = (isOnline) ? "m-badge--green" : "";

                        for (var count = 0; count < 27; count++) {
                            var letter = String.fromCharCode(65 + count);
                            if (row.lname.charAt(0) === letter) {
                                if (String.fromCharCode(65 + count) !== previousLetter) {
                                    string +=
                                        '<div style="position:relative;font-weight:400;padding:0 10px;" class="list-letter" data-letter="' +
                                        letter +
                                        '"><hr style="background:#e2dddd;padding-top:1px"><div style="position:absolute;top:50%;transform:translate(0%,-50%);background:white;padding:0 10px;color:#eb8923;font-size:20px" >' +
                                        letter + '</div></div>';
                                    previousLetter = letter;
                                }
                            }
                        }
                        string +=
                            '<a href="#" class="list-group-item list-group-item-action px-0" style="padding:5px 0;" data-email="' +
                            row.email + '" data-img="' + img + '" data-emp_id="' + row.emp_id +
                            '" data-fname="' + row.fname + '" data-chatstatus="' +
                            chatmate_status + '"  data-lname="' + row.lname +
                            '"><div class="m-stack m-stack--ver m-stack--general"><div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:60px">' +
                            '<img data-src="<?php echo base_url() ?>' + img +
                            '"  class="img-chatmate lazyload" data-sizes="auto" alt="" /><span class="m-badge  ' +
                            chatmate_status +
                            ' m-badge--dot badge-chat" style="margin-top: 24px;margin-left: -10px;border: 2px solid white;height:14px !important;width:14px !important" data-emp_id="' +
                            row.emp_id + '"></span></div>' +
                            '<div class="m-stack__item m-stack__item--center m-stack__item--middle"><p class="chat-username cut-text text-capitalize">' +
                            row.lname + ', ' + row.fname + '</p></div>' +
                            '</div></a>';
                    });

                    userContainer.html(string);
                    $('.img-chatmate').on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src',
                            '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
                    });
                });
                $("#loader-contacts").hide();
            }
        });
    }

    function chatPopoverSession() {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Login/chatPopoverSession'); ?>",
            data: {
                value: 0
            },
            cache: false,
            success: function(res) {
                console.log("UPDATED SESSION");
            }
        });
    }
    $('[href="#tab-contacts"]').one('click', function() {
        getChatContacts();
    });
    $("body").on("click", ".popover-chat-close", function() {
        $("#chat_toggler").data('fromclick', true);
        $("#chat_toggler").popover('dispose');
        chatPopoverSession();
    });
    $("#chat_toggler").click(function() {
        $("#chat_toggler").data('fromclick', true);
        $("#chat_toggler").popover('dispose');
        getChatMates();
        $("#chat_sidebar_toggle").click();
        chatPopoverSession();
    });
    $("#chat-search-name").keyup(function() {
        $(".list-letter").hide();
        var value = $(this).val();
        var filter = value.toUpperCase();
        var li = $("#chat_searchlist").find('.list-group-item');
        $.each(li, function(key, item) {
            var name = $(item).find(".chat-username").text();
            if (name.toUpperCase().indexOf(filter) > -1) {
                $(item).show();
                $(".list-letter[data-letter='" + name.charAt(0) + "']").show();
            } else {
                $(item).hide();
            }
        });

        $("#tab-contacts .m-scrollable").mCustomScrollbar("scrollTo", "top", {
            scrollInertia: 0
        });
    });

    $("#chat_userlist,#chat_searchlist").on('click', '.list-group-item', function(event) {
        var that = this;
        $("#chat_header").hide();
        event.preventDefault();
        var emp_id = $(that).data('emp_id');
        var img = $(that).data('img');
        var fname = $(that).data('fname');
        var fnamesub = fname.split(' ');
        var lname = $(that).data('lname');
        var email = $(that).data('email');
        var chatstatus = ($(that).find('.m-badge--green')[0]) ? "m-badge--green" : "";
        $("#chatmate-name").html(lname + ', ' + fname + ' <span class="m-badge ' + chatstatus +
            ' m-badge--dot badge-chat ml-2" data-emp_id="' + emp_id + '"></span>');


        var messagesContainer = $(".m-messenger__messages").find(".mCSB_container");
        $("#chatmate-name").data('emp_id', emp_id);
        $("#chatmate-name").data('fname', fname);
        $("#chatmate-name").data('lname', lname);
        $("#chatmate-name").data('img', img);
        $("#chatmate-name").data('email', email);
        var limiter = 0;
        $.ajax({
            type: "GET",
            url: socket_link + '/api/getMessages/<?php echo $this->session->emp_id; ?>/' + emp_id + '/' +
                limiter + '/' + messagePerPage,
            cache: false,
            success: function(res) {
                var withUnread = false;
                var previousDate = "";
                var previousHourMinute = "";
                var previousType = "";
                messagesContainer.html("");
                var messages = res.messages;
                messagesContainer.prepend('<div id="chatcontainer-' + limiter + '"></div>');
                var chatContainer = $('#chatcontainer-' + limiter);
                if (messages.length === 0 && limiter === 0) {
                    var largepic = img.split('_thumbnail.jpg');
                    messagesContainer.html('<div class=" p-4 mt-4" id="parent-sayhi">' +
                        '<div class="text-center"><img src="<?php echo base_url() ?>' + largepic[
                            0] + '.jpg" alt="" class="nomessage-img"/></div>' +
                        '<div class="text-center py-3 mt-3" style="background:#eee">' +
                        '<h4 class="mb-3">' + lname + ', ' + fname + '</h4>' +
                        '<a href="#" class="btn btn-dark m-btn m-btn--custom m-btn--icon m-btn--pill" style="padding:10px 50px !Important" id="btn-sayhi"><span><i class="fa fa-hand-stop-o"></i><span>Say Hi</span></span></a>'

                        +
                        '</div>' +
                        '</div>');
                    $(".m-messenger__messages").data('isfirstmessage', true);
                } else {

                    $(".m-messenger__messages").data('isfirstmessage', false);
                    $.each(Object.keys(messages).reverse(), function(i, key) {

                        var newDateFormatted = chatDateFormat(messages[key].createdOn, res
                            .datetime);
                        var newDate = moment(messages[key].createdOn).format("YYYY-MM-DD");
                        var newTime = moment(messages[key].createdOn).format("hh:mm A");
                        var newTimeValue = moment(moment(messages[key].createdOn).format(
                            'YYYY-MM-DD hh:mm')).valueOf();
                        var type = (messages[key].sender_ID + '' ===
                            "<?php echo $this->session->emp_id; ?>") ? "OUT" : "IN";
                        var tempString = "";

                        if (previousType !== type) {
                            var isInsert = true;
                        } else {
                            if (previousHourMinute !== newTimeValue) {
                                var isInsert = true;
                            } else {
                                var isInsert = false;
                            }
                        }

                        if (messages[key].isRead === 0 && withUnread === false && type ===
                            'IN') {
                            chatContainer.append(
                                '<div class="m-messenger__unread" style="position:relative;font-weight:400"><hr style="background:#3393DD;padding-top:1px" /><div style="position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);background:white;padding:0 10px;color:#3393DD;font-size:11px">Unread messages</div></div>'
                            );
                            withUnread = true;
                            isInsert = true;
                        }
                        if (previousDate !== newDate) {
                            chatContainer.append(
                                '<div class="m-messenger__datetime" data-datetime="' +
                                newDate +
                                '" style="position:relative;font-weight:400"><hr style="background:#efefef" /><div style="position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);background:white;padding:0 10px;color:#8A8D91;font-size:11px">' +
                                newDateFormatted + '</div></div>');
                        }

                        if (isInsert) {
                            if (type === "IN") {
                                tempString +=
                                    '<div class="m-messenger__wrapper" data-timevalue="' +
                                    newTimeValue + '">'

                                    +
                                    '<div class="m-messenger__message m-messenger__message--in">' +
                                    '<div class="m-messenger__message-username">'
                                    //                                        + fnamesub[0] + ', ' 
                                    +
                                    newTime +
                                    '</div>' +
                                    '<div class="m-messenger__message-pic">' +
                                    '<img src="<?php echo base_url() ?>' + img + '"  alt=""/>' +
                                    '</div>' +
                                    '<div class="m-messenger__message-body">'

                                    +
                                    '<div class="m-messenger__message-arrow"></div>' +
                                    '<div class="m-messenger__message-content">'

                                    +
                                    '<div class="m-messenger__message-text">' +
                                    unescape(messages[key].message) +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';

                            } else {
                                tempString +=
                                    '<div class="m-messenger__wrapper" data-timevalue="' +
                                    newTimeValue + '">'

                                    +
                                    '<div class="m-messenger__message m-messenger__message--out">' +
                                    '<div class="m-messenger__message-username">' +
                                    newTime +
                                    '</div>' +
                                    '<div class="m-messenger__message-body">'

                                    +
                                    '<div class="m-messenger__message-arrow"></div>' +
                                    '<div class="m-messenger__message-content">' +
                                    '<div class="m-messenger__message-text">' +
                                    unescape(messages[key].message) +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                            }


                        } else {
                            if (type === "IN") {
                                $('<div class="m-messenger__wrapper" data-timevalue="' +
                                    newTimeValue + '">'

                                    +
                                    '<div class="m-messenger__message m-messenger__message--in">'
                                    //                                        + '<div class="m-messenger__message-username">'
                                    //                                        + '</div>'
                                    +
                                    '<div class="m-messenger__message-pic">' +
                                    '</div>' +
                                    '<div class="m-messenger__message-body">'

                                    +
                                    '<div class="m-messenger__message-arrow"></div>' +
                                    '<div class="m-messenger__message-content">'

                                    +
                                    '<div class="m-messenger__message-text">' +
                                    unescape(messages[key].message) +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>').insertAfter($(".m-messenger__wrapper:last"));
                            } else {
                                $('<div class="m-messenger__wrapper" data-timevalue="' +
                                    newTimeValue + '">'

                                    +
                                    '<div class="m-messenger__message m-messenger__message--out">'
                                    // + '<div class="m-messenger__message-username">'
                                    //  + '</div>'
                                    +
                                    '<div class="m-messenger__message-body">'

                                    +
                                    '<div class="m-messenger__message-arrow"></div>' +
                                    '<div class="m-messenger__message-content">' +
                                    '<div class="m-messenger__message-text">' +
                                    unescape(messages[key].message) +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>').insertAfter($(".m-messenger__wrapper:last"));
                            }

                        }
                        chatContainer.append(tempString);
                        previousDate = newDate;
                        previousHourMinute = newTimeValue;
                        previousType = type;

                        $(".m-messenger__form-attachment").removeClass('active');
                        $(".m-messenger__form-attachment").addClass('inactive');
                    });
                }

                $('.m-messenger__message-pic img').on('error', function() {
                    $(this).attr('onerror', null);
                    $(this).attr('src',
                        '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
                });
                $('.nomessage-img').on('error', function() {
                    $(this).attr('onerror', null);
                    $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                });
                $(".m-messenger__messages").mCustomScrollbar("update");

                $(".m-messenger__messages").mCustomScrollbar("scrollTo", "bottom", {
                    scrollInertia: 0
                });

                $(".m-messenger__messages").data('limiter', limiter + messagePerPage);

                socket.emit('readAllMessages', {
                    receiver_ID: "<?php echo $this->session->emp_id; ?>",
                    sender_ID: $("#chatmate-name").data('emp_id')
                });
            }
        });
        $("#div-messages").show();
        $("#div-nomessage").hide();


    });
    $("#btn-backtochatlist").click(function(e) {
        e.preventDefault();
        $(".m-messenger__form-attachment").removeClass('active');
        $(".m-messenger__form-attachment").addClass('inactive');
        $(".m-messenger__form-input-2").val("");
        $("#chat_header").show();
        $("#div-messages").hide();
        $(".m-messenger__notes").html("");
        socket.emit('readAllMessages', {
            receiver_ID: "<?php echo $this->session->emp_id; ?>",
            sender_ID: $("#chatmate-name").data('emp_id')
        });
        socket.emit('typing', {
            message: "",
            receiver_ID: $("#chatmate-name").data('emp_id'),
            sender_ID: "<?php echo $this->session->emp_id; ?>"
        });
        $("#chatmate-name").removeData('emp_id');
        $("#chatmate-name").removeData('fname');
        $("#chatmate-name").removeData('lname');
        $("#chatmate-name").removeData('img');
    });

    $(".btn-newchat").click(function(e) {
        e.preventDefault();
    });

    $(".m-messenger__messages").on('click', "#btn-sayhi", function(e) {
        e.preventDefault();
        $(".m-messenger__form-input-2").val('Hi...');
        $(".m-messenger__form-attachment").click();
    });
    $('.m-messenger__form-input-2').keypress(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode === 13 && !event.shiftKey) {
            $(".m-messenger__form-attachment").click();
            event.preventDefault();
            return false;
        }

    });
    $(".m-messenger__form-attachment").click(function() {
        var receiver_id = $("#chatmate-name").data('emp_id');
        var message = $(".m-messenger__form-input-2").val();
        var sender_id = "<?php echo $this->session->emp_id; ?>";
        if ($(".m-messenger__messages").data('isfirstmessage') === 'yes') {
            $(".m-messenger__messages").find(".mCSB_container").html('');
            $(".m-messenger__messages").data('isfirstmessage', 'no');
        }
        if (message.trim() !== '') {
            $(".m-messenger__form-input-2").val('');

            $(".m-messenger__form-attachment").removeClass('active');
            $(".m-messenger__form-attachment").addClass('inactive');
            socket.emit('newChatMessage', {
                message: message.trim(),
                receiver_id: receiver_id,
                sender_id: sender_id,
                fname: $("#chatmate-name").data('fname'),
                lname: $("#chatmate-name").data('lname'),
                img: $("#chatmate-name").data('img'),
                email: $("#chatmate-name").data('email'),
                sfname: '<?php echo $this->session->fname; ?>',
                slname: '<?php echo $this->session->lname; ?>',
                simg: '<?php echo $this->session->pic; ?>',
                semail: '<?php echo $this->session->email; ?>'
            });
            socket.emit('typing', {
                message: "",
                receiver_ID: $("#chatmate-name").data('emp_id'),
                sender_ID: "<?php echo $this->session->emp_id; ?>"
            });
            if (receiver_id + '' === '980' && "<?php echo $this->session->email; ?>" !== '') { //emailing feature
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('Cronjobs/sendEmail'); ?>",
                    data: {
                        name: '<?php echo $this->session->lname . ", " . $this->session->fname; ?>',
                        pic: "<?php echo $this->session->pic; ?>",
                        message: message.trim(),
                        sender_email: "<?php echo $this->session->email; ?>",
                        recipient_email: $("#chatmate-name").data('email')
                    },
                    cache: false,
                    success: function(res) {
                        // console.log("UPDATED SESSION");
                    }
                });
            }

        }
    });
    socket.on('newChatMessage', function(obj) {
        if (obj.receiver_ID + '' === '<?php echo $this->session->emp_id; ?>') {
            var useEmp_id = obj.sender_ID;
            var useFname = obj.sfname;
            var useLname = obj.slname;
            var useImg = obj.simg;
            var useEmail = obj.semail;
        } else { //sender_Id === session->emp_id
            var useEmp_id = obj.receiver_ID;
            var useFname = obj.fname;
            var useLname = obj.lname;
            var useImg = obj.img;
            var useEmail = obj.email;
        }

        //AUTO UPDATE LIST--------------------------------------------------
        var $listgroupSender = $("#chat_userlist").find(".list-group-item[data-emp_id='" + useEmp_id + "']");
        if ($listgroupSender.length === 0) {
            var isOnline = false;
            $.each(obj.online, function(ii, user) {
                if (user.emp_id + '' === obj.receiver_ID + '') {
                    isOnline = true;
                }
            });
            var chatmate_status = (isOnline) ? "m-badge--green" : "";
            var formattedDate = '<p class="last-chat-time">' + chatDateFormat(obj.createdOn, obj.datetime, true) +
                '</p>';
            var withUnread =
                '<p class="mb-0 text-right" style="margin-right:10px"><span class="m-badge m-badge--wide badge-unread">1</span></p>';
            var fontweight = 'font-weight:800;color:#444';
            var listgroupString =
                '<a href="#" class="list-group-item list-group-item-action px-0" style="padding:8px 0;" data-email="' +
                useEmail + '" data-img="' + useImg + '" data-emp_id="' + useEmp_id + '" data-fname="' + useFname +
                '" data-chatstatus="' + chatmate_status + '"  data-lname="' + useLname +
                '"><div class="m-stack m-stack--ver m-stack--general"><div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:60px">' +
                '<img src="<?php echo base_url() ?>' + useImg +
                '" class="img-chatmate " alt="" /><span class="m-badge  ' + chatmate_status +
                ' m-badge--dot badge-chat" style="margin-top: 24px;margin-left: -10px;border: 2px solid white;height:14px !important;width:14px !important" data-emp_id="' +
                useEmp_id + '"></span></div>' +
                '<div class="m-stack__item m-stack__item--center m-stack__item--middle"><p class="chat-username cut-text text-capitalize" style="' +
                fontweight + '">' + useLname + ', ' + useFname + '</p><p class="chat-lastmessage cut-text">' + unescape(obj
                .message) + '</p></div>' +
                '<div class="m-stack__item m-stack__item--center m-stack__item--middle"  style="width:80px">' +
                formattedDate + '' + withUnread + '</div>' +
                '</div></a>';
            $("#chat_userlist").prepend(listgroupString);
            $("#chat_userlist").find('.m-alert').parent().remove();
        } else {
            if ($listgroupSender.prevAll().length !== 0) {
                $listgroupSender.parent().prepend($listgroupSender.hide().delay(200).show(500));
            }
            var $badgeunread = $listgroupSender.find(".badge-unread");
            if ($badgeunread.length !== 0) { //naay badge daan
                var value = parseInt($badgeunread.text());
                $badgeunread.html(value + 1);
            } else {
                $('<p class="mb-0 text-right" style="margin-right:10px"><span class="m-badge m-badge--wide badge-unread">1</span></p>')
                    .insertAfter($listgroupSender.find('.last-chat-time'));
            }
            $listgroupSender.find('.chat-username').css({
                'font-weight': 800,
                'color': '#444'
            });
            $listgroupSender.find('.last-chat-time').html(chatDateFormat(obj.createdOn, obj.createdOn, true));
            $listgroupSender.find('.chat-lastmessage').html(unescape(obj.message));
        }

        //END OF AUTO UPDATE LIST-------------------------------------------
        var emp_id = $("#chatmate-name").data('emp_id');
        if (emp_id + '' === obj.sender_ID + '' || obj.sender_ID + '' === "<?php echo $this->session->emp_id; ?>") {
            $(".m-messenger__messages").find('#parent-sayhi').remove();
            var messagesContainer = $(".m-messenger__messages").find(".mCSB_container");
            var newTimeValue = moment(moment(obj.createdOn).format('YYYY-MM-DD hh:mm')).valueOf();
            var newDateFormatted = chatDateFormat(obj.createdOn, obj.createdOn);
            var newDate = moment(obj.createdOn).format("YYYY-MM-DD");
            var newTime = moment(obj.createdOn).format("hh:mm A");
            var tempString = "";
            var previousDate = $(".m-messenger__datetime:last").data('datetime');

            if (previousDate !== newDate) {
                messagesContainer.append('<div class="m-messenger__datetime" data-datetime="' + newDate +
                    '" style="position:relative;font-weight:400"><hr style="background:#efefef" /><div style="position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);background:white;padding:0 10px;color:#8A8D91;font-size:11px">' +
                    newDateFormatted + '</div></div>');
            }
            var previousType = ($(".m-messenger__wrapper:last").find('.m-messenger__message').hasClass(
                'm-messenger__message--in') ? "IN" : "OUT");
            var previousHourMinute = $(".m-messenger__wrapper:last").data('timevalue');
            if (previousType !== obj.type) {
                var isInsert = true;
            } else {
                if (previousHourMinute !== newTimeValue) {
                    var isInsert = true;
                } else {
                    var isInsert = false;
                }
            }

            if (isInsert) {
                if (obj.type === "IN") {
                    tempString += '<div class="m-messenger__wrapper" data-timevalue="' + newTimeValue + '">'

                        +
                        '<div class="m-messenger__message m-messenger__message--in">' +
                        '<div class="m-messenger__message-username">'
                        //                            + fnamesub[0] + ', ' 
                        +
                        newTime +
                        '</div>' +
                        '<div class="m-messenger__message-pic">' +
                        '<img src="<?php echo base_url() ?>' + useImg + '"  alt=""/>' +
                        '</div>' +
                        '<div class="m-messenger__message-body">'

                        +
                        '<div class="m-messenger__message-arrow"></div>' +
                        '<div class="m-messenger__message-content">'

                        +
                        '<div class="m-messenger__message-text">' +
                        unescape(obj.message) +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                } else {
                    tempString += '<div class="m-messenger__wrapper" data-timevalue="' + newTimeValue + '">'

                        +
                        '<div class="m-messenger__message m-messenger__message--out">' +
                        '<div class="m-messenger__message-username">' +
                        newTime +
                        '</div>' +
                        '<div class="m-messenger__message-body">'

                        +
                        '<div class="m-messenger__message-arrow"></div>' +
                        '<div class="m-messenger__message-content">' +
                        '<div class="m-messenger__message-text">' +
                        unescape(obj.message) +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                }


            } else {
                if (obj.type === "IN") {
                    $('<div class="m-messenger__wrapper" data-timevalue="' + newTimeValue + '">'

                        +
                        '<div class="m-messenger__message m-messenger__message--in">'
                        //                                        + '<div class="m-messenger__message-username">'
                        //                                        + '</div>'
                        +
                        '<div class="m-messenger__message-pic">' +
                        '</div>' +
                        '<div class="m-messenger__message-body">'

                        +
                        '<div class="m-messenger__message-arrow"></div>' +
                        '<div class="m-messenger__message-content">'

                        +
                        '<div class="m-messenger__message-text">' +
                        unescape(obj.message) +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>').insertAfter($(".m-messenger__wrapper:last"));
                } else {
                    $('<div class="m-messenger__wrapper" data-timevalue="' + newTimeValue + '">'

                        +
                        '<div class="m-messenger__message m-messenger__message--out">'
                        //                                        + '<div class="m-messenger__message-username">'
                        //                                        + '</div>'
                        +
                        '<div class="m-messenger__message-body">'

                        +
                        '<div class="m-messenger__message-arrow"></div>' +
                        '<div class="m-messenger__message-content">' +
                        '<div class="m-messenger__message-text">' +
                        unescape(obj.message) +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>').insertAfter($(".m-messenger__wrapper:last"));
                }

            }

            messagesContainer.append(tempString);
            $(".m-messenger__messages").mCustomScrollbar("scrollTo", "bottom", {
                scrollInertia: 0
            });

            $('.m-messenger__message-pic img').on('error', function() {
                $(this).attr('onerror', null);
                $(this).attr('src', '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
            });
            $('.img-chatmate').on('error', function() {
                $(this).attr('onerror', null);
                $(this).attr('src', '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
            });
        }
        if ($("#div-messages").css('display') === 'block' && obj.receiver_ID + '' ===
            '<?php echo $this->session->emp_id; ?>') {
            socket.emit('readAllMessages', {
                receiver_ID: "<?php echo $this->session->emp_id; ?>",
                sender_ID: $("#chatmate-name").data('emp_id')
            });
        }
        getBlinkerData();
    });
    socket.on('typing', function(obj) {
        var emp_id = $("#chatmate-name").data('emp_id');
        if (emp_id + '' === obj.sender_ID + '') {
            $(".m-messenger__notes").html(obj.message);
        }
    });
    socket.on('changeOnlineStatus', function(obj) {
        if (obj.status === 'online') {
            $(".badge-chat[data-emp_id='" + obj.emp_id + "']").addClass('m-badge--green');
        } else {
            $(".badge-chat[data-emp_id='" + obj.emp_id + "']").removeClass('m-badge--green');
        }
    });
    socket.on('readAllMessages', function(obj) {

        var $badgeunread = $("#chat_userlist").children(".list-group-item[data-emp_id='" + obj.sender_ID + "']")
            .find(".badge-unread");
        if ($badgeunread.length !== 0) {
            $badgeunread.parent().remove();
            $("#chat_userlist").children(".list-group-item[data-emp_id='" + obj.sender_ID + "']").find(
                '.chat-username').css({
                'font-weight': 401,
                'color': '#58585A'
            });
        }
        getBlinkerData();
    });
</script>
<!-- START CHA CHA CODES -->
<script>
    // Media display function
    function fetch_media_attachment(irid, callback) {
        var et = $("#evidence-types").val();
        // var ap = $("#attachment-purpose").val();
        var stringx = "";
        var audio_stringxx = "";
        $.ajax({
            url: baseUrl + "/discipline/fetch_report_evidences/" + irid,
            type: "POST",
            data: {
                evidencetype: et,
                purpose: 1
            },
            cache: false,
            success: function(data) {
                var result = JSON.parse(data);
                callback(result);
                var total_evidences = result.length;
                $("#label_attachment").text(total_evidences + " Attachments");
                var file_name = "";
                if (result.display.length != 0) {
                    $.each(result.display, function(key, data) {
                        file_name = data.link.replace("uploads/dms/evidences/", " ");
                        if (data.mediaType == "image/jpeg" || data.mediaType == "image/png" || data
                            .mediaType == "image/jpg") {
                            stringx +=
                                "<div class='col-lg-4 col-md-12 mb-4 attachedFile' data-filename='" +
                                file_name + "'>";
                            stringx += "<a href='" + baseUrl + "/" + data.link +
                                "' data-lightbox='roadtrip'>";
                            stringx += "<img src='" + baseUrl + "/" + data.link +
                                "' class='h_wdisplay z-depth-1'>";
                            stringx += "</a>";
                            stringx += "<div class='col-lg-12 col-md-6 mb-4 text-truncate'>";
                            stringx += "<span class='evidence_file_name'>" + file_name + "</span>";
                            stringx += "</div>";
                            // if (data.incidentReportAttachmentPurpose_ID == "4") {
                            //     stringx += "<button type='link' onclick='remove_attachment(" + data
                            //         .ir_attachment_ID + "," + irid + ")' style='display:none' class='xbutton'>x</button>";
                            // }
                            stringx += "</div>";
                        } else if (data.mediaType == "video/mp4" || data.mediaType == "video/mov" ||
                            data
                            .mediaType == "video/3gp" || data.mediaType == "video/mpeg" || data
                            .mediaType ==
                            "video/avi" || data.mediaType == "video/mpg" || data.mediaType ==
                            "video/m4v") {
                            stringx +=
                                "<div class='col-lg-4 col-md-6 mb-4 attachedFile' data-filename='" +
                                file_name + "'>";
                            stringx += "<div id='vidBox'>";
                            stringx += "<div id='videCont'>";
                            stringx += "<video width='120' height='86' id='example' controls>";
                            stringx += "<source src='" + baseUrl + "/" + data.link +
                                "' type='video/mp4'>";
                            stringx += "<source src='" + baseUrl + "/" + data.link +
                                "' type='video/mov'>";
                            stringx += "<source src='" + baseUrl + "/" + data.link +
                                "' type='video/avi'>";
                            stringx += "<source src='" + baseUrl + "/" + data.link +
                                "' type='video/mpg'>";
                            stringx += "<source src='" + baseUrl + "/" + data.link +
                                "' type='video/m4v'>";
                            stringx += "</video>";
                            stringx += "<div class='col-lg-12 col-md-6 mb-4 text-truncate'>";
                            stringx += "<span class='evidence_file_name'>" + file_name + "</span>";
                            stringx += "</div>";
                            stringx += "</div>";
                            stringx += "</div>";
                            // if (data.incidentReportAttachmentPurpose_ID == "4") {
                            //     stringx += "<button type='link' onclick='remove_attachment(" + data
                            //         .ir_attachment_ID + "," + irid + ")' class='xbutton'>x</button>";
                            // }
                            stringx += "</div>";
                        } else if (data.mediaType == "audio/mp3" || data.mediaType == "audio/3gp" ||
                            data
                            .mediaType == "audio/wav" || data.mediaType == "audio/mpeg") {
                            stringx +=
                                "<div class='col-lg-4 col-md-6 mb-4 text-truncate attachedFile' data-filename='" +
                                file_name + "'>";
                            // stringx += "<div>";
                            stringx +=
                                "<a href='#' class='a_audio_link' data-toggle='modal' data-target='#audio_modal_evidences' id='attached_button_audio' data-audiolink='" +
                                data.link + "' >";
                            stringx += "<i class='fa fa-music icon_size2'></i>";
                            stringx += "<br><span>" + file_name + "</span>";
                            stringx += "</a>";
                            // if (data.incidentReportAttachmentPurpose_ID == "4") {
                            //     stringx += "<button type='link' onclick='remove_attachment(" + data
                            //         .ir_attachment_ID + "," + irid + ")' class='xbutton'>x</button>";
                            // }
                            stringx += "</div>";
                        } else {
                            stringx +=
                                "<div class='col-lg-4 col-md-12 mb-4 text-truncate attachedFile' style='position: relative;' data-filename='" +
                                file_name + "'>";
                            stringx += "<a target='_blank' href='" + baseUrl + "/" + data.link +
                                "'>";
                            if (data.mediaType == "application/vnd.open") {
                                stringx += "<i class='fa fa-file-word-o icon_size'></i>";
                            } else if (data.mediaType == "application/pdf") {
                                stringx += "<i class='fa fa-file-pdf-o icon_size'></i>";
                            } else if (data.mediaType == "application/vnd.ms-e") {
                                stringx += "<i class='fa fa-file-excel-o icon_size'></i>";
                            } else {
                                stringx += "<i class='fa fa-file icon_size'></i>";
                            }
                            stringx += "<br><span>" + file_name + "</span>";
                            stringx += "</a>";
                            // if (data.incidentReportAttachmentPurpose_ID == "4") {
                            //     stringx += "<button type='link' onclick='remove_attachment(" + data
                            //         .ir_attachment_ID + "," + irid + ")' class='xbutton'>x</button>";
                            // }
                            stringx += "</div>";
                        }
                    });
                    console.log("exist");
                } else {
                    stringx += "<div class='col-lg-12 col-md-12 mb-4 text-center mt-5'>";
                    stringx += ""
                    stringx +=
                        "<h4><i class='fa fa-paperclip' style='color:crimson;font-size:unset'></i> No Evidences</h4>";
                    stringx += "</div>";
                    console.log("none");
                }
                $("#media_display").html(stringx);
            }
        });
    }
    // media end function here
    //Display of uploaded explanation in pdf
    function uploadedExplanationGeneral(irId) {
        // Query here
        console.log(irId);
        $.ajax({
            type: "POST",
            url: baseUrl + "/discipline/get_irattachment_uploaded",
            data: {
                irid: irId,
                purpose: 5
            },
            cache: false,
            success: function(res) {
                var result = JSON.parse(res);
                console.log(result);
                var ex_pdflink = result[0].link;
                var options = {
                    pdfOpenParams: {
                        navpanes: 0,
                        toolbar: 0,
                        statusbar: 0,
                        pagemode: 'thumbs',
                        view: "FitH"
                    }
                };
                var myPDF = PDFObject.embed(baseUrl + "/" + ex_pdflink, "#explanationpdf_display", options);
                console.log(baseUrl + "/" + ex_pdflink);
                console.log((myPDF) ? "PDFObject successfully embedded" : "Uh-oh, the embed didn't work.");
                $('.ir_details_template').parents('.modal.fade.show').attr('id');
                $('#uploadExplanationModal_general').data('prevmodal', $('.ir_details_template').parents(
                    '.modal.fade.show').attr('id'));
                $('#' + $('.ir_details_template').parents('.modal.fade.show').attr('id')).modal('hide');
                $('#uploadExplanationModal_general').modal();

                // $('#uploadExplanationModal_general').modal();

            },
            error: function(res) {
                swal(
                    'Oops!',
                    'Something is wrong please contact adminstrator!',
                    'error'
                )
            }
        });
    }
</script>
<!-- END OF CHA CHA CODES -->

<!-- START OF MIC MIC CODES -->
<script type="text/javascript">
    // DMS Shared Modal
    function initQualifiedDtrView(qualifiedIdVal, withPrev) {
        $.when(fetchPostData({
            qualifiedViolationId: qualifiedIdVal
        }, '/discipline/dtr_details_view')).then(function(qualified) {
            let qualifiedObj = $.parseJSON(qualified.trim());
            let incidentDetails = "";
            let excess = "";
            let expectedActionDetails = "";
            let allowableTable = "";
            let coveredTable = "";
            let missingLogsDetails = "";
            let missingLogs = "";
            let expectedAction = "";
            let narrativeDetails = ""
            let pronoun = "";
            let allowableTableBody = "";
            let coveredTableBody = "";
            let shiftTime = qualifiedObj.details.shift.split('-');
            let reasonVal = "";
            // console.log(dtrViolationContent);
            $('#qualifiedDetailsModal').data('qualifiedid', qualifiedIdVal);
            $('#qualifiedViewEmployeeName').text(toTitleCase(qualifiedObj.details.emp_details.fname + " " +
                qualifiedObj.details.emp_details.lname));
            console.log('mao ni ang image');
            console.log(baseUrl + "/" + qualifiedObj.details.emp_details.pic);
            $('#empPicView').attr('src', baseUrl + "/" + qualifiedObj.details.emp_details.pic);
            $('#qualifiedViewEmployeeName').data('empid', qualifiedObj.details.emp_details.emp_id);
            $('#qualifiedViewEmployeeJob').text(qualifiedObj.details.emp_details.positionDescription);
            $('#qualifiedViewPosEmpStat').text(" -" + qualifiedObj.details.emp_details.empStatus);
            $('#qualifiedViewPosEmpStat').data('posEmpStat', qualifiedObj.details.emp_details.empStatus);
            $('#qualifiedViewEmployeeJob').data('position', qualifiedObj.details.emp_details.positionDescription);
            $('#qualifiedViewEmployeeAccount').text(toTitleCase(qualifiedObj.details.emp_details
                .accountDescription));
            $('#qualifiedViewEmployeeAccount').data('account', toTitleCase(qualifiedObj.details.emp_details
                .accountDescription));
            // Date and Incidents
            // $('#qualifiedDateIncident').text(moment(qualifiedObj.details.sched_date, 'Y-MM-DD').format('MMM DD, Y'));
            // $('#qualifiedDateIncident').data('incidentDate', qualifiedObj.details.sched_date);
            $('#qualifiedViewIncidentType').text(toTitleCase(qualifiedObj.details.violation_type));
            $('#withRuleView').hide();
            if (qualifiedObj.details.emp_details.gender == 'Male'){
                pronoun = "his";
            } else {
                pronoun = "her";
            }
            if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) { // OB
                $('#qualifiedViewDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_end'],
                    'Y-MM-DD HH:mm').format('MMM DD, Y'));
                $('#qualifiedViewDateIncident').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                    'shift_end'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
                $('#qualifiedViewDateIncident').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                    'shift_end'], 'Y-MM-DD HH:mm').format('kk:mm'));
                expectedAction = "Should have not exceeded from the " + qualifiedObj.details.incident_details
                    .total_allowable + " allowable break duration";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                        .emp_details.lname) + " has consumed a total of " + qualifiedObj.details.incident_details
                    .total_covered + " break duration, which is " + qualifiedObj.details.incident_details.excess +
                    " more than " + pronoun + " allowable break of " + qualifiedObj.details.incident_details
                    .total_allowable + ".";
                incidentDetails = '<div class="col-12">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 mt-2 font-weight-bold">Break Details:</div>';
                excess = '<div class="col-12 mb-2">' +
                    '<span class="font-weight-bold mr-2">Over Break Duration:</span>' +
                    '<span class="">' + qualifiedObj.details.incident_details.excess + '</span>' +
                    '</div>';

                if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                    $.each(qualifiedObj.details.incident_details.break, function(index, value) {
                        let clock_in = 'N/A';
                        let clock_out = 'N/A';
                        if(value.in != null){
                            clock_in = moment(value.in, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A');
                        }
                        if(value.out != null){
                            clock_out = moment(value.out, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A');
                        }
                        allowableTableBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + value.type + '</td>' +
                            '<td class="text-center">' + value.allowable + '</td>' +
                            '</tr>';
                        coveredTableBody += '<tr>' +
                            '<td class="text-center">' + clock_in  + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + clock_out + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                            '</tr>';
                    })
                    allowableTable = '<div class="col-12 col-md-4">' +
                        '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                        '<thead style="background: #555;color: white;">' +
                        '<tr>' +
                        '<th class="text-center">Type</th>' +
                        '<th class="text-center">Allowable</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        allowableTableBody +
                        '</tbody>' +
                        '</table>' +
                        '</div>';
                    coveredTable = '<div class="col-12 col-md-8 pl-0">' +
                        '<table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">' +
                        '<thead style="background: #555;color: white;">' +
                        '<tr>' +
                        '<th class="text-center">Out for Break</th>' +
                        '<th class="text-center">Back from Break</th>' +
                        '<th class="text-center">Covered</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        coveredTableBody +
                        '</tbody>' +
                        '</table>' +
                        '</div>';
                    incidentDetails += allowableTable + "" + coveredTable + "" + excess + "" +
                        expectedActionDetails;
                } else {
                    // alert error
                }
            } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 5) { // ABSENT
                $('#qualifiedViewDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_start'],
                    'Y-MM-DD HH:mm').format('MMM DD, Y'));
                $('#qualifiedViewDateIncident').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                    'shift_start'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
                $('#qualifiedViewDateIncident').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                    'shift_start'], 'Y-MM-DD HH:mm').format('kk:mm'));
                $('#withRule').hide();
                expectedAction =
                    "Should have taken all necessary DTR transaction that proves you are present at work.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                        .emp_details.lname) + " was determinded to have no DTR Log transactions in " + pronoun +
                    " " + qualifiedObj.details.shift + " shift last " + moment(qualifiedObj.details.sched_date,
                        'Y-MM-DD').format('MMM DD, Y') + ", which proves "+pronoun+" absence on the said schedule date.";
                incidentDetails = '<div class="col-12 col-md-6">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 col-md-6 text-md-right mb-2">' +
                    '<span class="font-weight-bold mr-2">Logs:</span>' +
                    '<span class="">No Log Records Found</span>' +
                    '</div>';
            } else if ((parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6) || (parseInt(
                    qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7)) {
                $('#qualifiedViewDateIncident').text(moment(qualifiedObj.details.dated_shift['shift_end'],
                    'Y-MM-DD HH:mm').format('MMM DD, Y'));
                $('#qualifiedViewDateIncident').data('incidentDate', moment(qualifiedObj.details.dated_shift[
                    'shift_end'], 'Y-MM-DD HH:mm').format('Y-MM-DD'));
                $('#qualifiedViewDateIncident').data('incidentTime', moment(qualifiedObj.details.dated_shift[
                    'shift_end'], 'Y-MM-DD HH:mm').format('kk:mm'));

                expectedAction =
                    "Should have taken all the necessary DTR Transactions to complete all the required DTR Logs.";
                narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj.details
                        .emp_details.lname) + " was determinded to have no logs of the following: " + qualifiedObj
                    .details.incident_details.missing.join(", ");
                incidentDetails = '<div class="col-12 col-md-4">' +
                    '<span class="font-weight-bold mr-2">Shift:</span>' +
                    '<span class="">' + qualifiedObj.details.shift + '</span>' +
                    '</div>' +
                    '<div class="col-12 col-md-3 text-md-right">' +
                    '<span class="font-weight-bold mr-2">Missing Logs:</span>' +
                    '</div>';
                if (parseInt(qualifiedObj.details.incident_details.error) == 0) {
                    $.each(qualifiedObj.details.incident_details.missing, function(index, value) {
                        missingLogs += '<li>' + value + '</li>';
                    })
                    missingLogsDetails = '<div class="col-12 col-md-5">' +
                        '<ol>' + missingLogs +
                        '</ol>' +
                        '</div>';
                    incidentDetails += missingLogsDetails;
                } else {
                    // alert error
                }
            } else {
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1) {
                    expectedAction = "Should have clocked in at least 30 minutes before " + shiftTime[0];
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                            .details.emp_details.lname) + " clocked in by " + moment(qualifiedObj.details
                            .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                            .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                        "), which is " + qualifiedObj.details.incident_details.duration + " later than " + pronoun +
                        " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_start'],
                            'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                            'shift_start'], 'Y-MM-DD HH:mm').format('MMM DD, Y') + ") shift start.";
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                    expectedAction = "Should have not clocked out before " + shiftTime[1] + " to avoid Undertime.";
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                            .details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details
                            .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                            .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                        "), which is " + qualifiedObj.details.incident_details.duration + " earlier before " +
                        pronoun + " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'],
                            'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                            'shift_end'], 'Y-MM-DD HH:mm').format('MMM DD, Y') + ") shift end.";
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 3) {
                    expectedAction = "Should have not clocked out beyond " + qualifiedObj.details.incident_details
                        .allowable_logout_formatted + " clock out allowance after" + shiftTime[1];
                    narrativeDetails = toTitleCase(qualifiedObj.details.emp_details.fname + " " + qualifiedObj
                            .details.emp_details.lname) + " clocked out by " + moment(qualifiedObj.details
                            .incident_details.log, 'Y-MM-DD HH:mm:ss').format('h:mm A') + " (" + moment(qualifiedObj
                            .details.incident_details.log, 'Y-MM-DD HH:mm:ss').format('MMM DD, Y') +
                        "), which is " + qualifiedObj.details.incident_details.duration + " past from " + pronoun +
                        " actual scheduled " + moment(qualifiedObj.details.dated_shift['shift_end'],
                            'Y-MM-DD HH:mm').format('h:mm A') + " (" + moment(qualifiedObj.details.dated_shift[
                            'shift_end'], 'Y-MM-DD HH:mm').format('MMM DD, Y') +
                        ") shift end exceeding the allowable clock-out allowance of " + qualifiedObj.details
                        .incident_details.allowable_logout_formatted + ".";
                    reasonVal = '<div class="col-12 pt-2">' +
                        '<div class="row">' +
                        '<div class="col-12 col-md-2 font-weight-bold">Reason:</div>' +
                        '<div class="col-12 col-md-10 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"><i class="fa fa-quote-left mr-1" style="font-size: 0.9rem;"></i>' +
                        qualifiedObj.details.reason.explanation +
                        '<i class="fa fa-quote-right ml-1" style="font-size: 0.9rem;"></i></div>' +
                        '</div>' +
                        '</div>';
                }
                $('#qualifiedViewDateIncident').text(moment(qualifiedObj.details.incident_details.log,
                    'Y-MM-DD HH:mm:ss').format('MMM DD, Y'));
                $('#qualifiedViewDateIncident').data('incidentDate', moment(qualifiedObj.details.incident_details
                    .log, 'Y-MM-DD HH:mm:ss').format('Y-MM-DD'));
                $('#qualifiedViewDateIncident').data('incidentTime', moment(qualifiedObj.details.incident_details
                    .log, 'Y-MM-DD HH:mm:ss').format('kk:mm'));
                var durationVal = parseFloat(qualifiedObj.details.incident_details.duration) / 60;
                incidentDetails = '<div class="col-12 col-md-5">' +
                    '<p class = "font-weight-bold mb-0" > Shift: </p> ' +
                    '<p class = "" id="qualifiedShift" >' + qualifiedObj.details.shift + '</p>' +
                    '</div> ' +
                    '<div class = "col-12 col-md-4" >' +
                    '<p class = "font-weight-bold mb-0">' + qualifiedObj.details.incident_details.log_label +
                    ':</p>' +
                    '<p class = "" >' + moment(qualifiedObj.details.incident_details.log, 'Y-MM-DD HH:mm:ss')
                    .format('MMM DD, Y h:mm A') + '</p>' +
                    '</div>' +
                    '<div class = "col-12 col-md-3" >' +
                    '<p class = "font-weight-bold mb-0"> Duration: </p> ' +
                    '<p class = "">' + qualifiedObj.details.incident_details.duration + '</p> ' +
                    '</div>';
            }

            incidentDetails += '<div class="col-12 pl-3 pr-3">' +
                '<div class="col-md-12 ahr_quote pt-3 pb-3 text-center">' +
                '<blockquote style="margin: 0 0 0rem;">' +
                '<i class="fa fa-quote-left"></i>' +
                '<span class="mb-0 mr-1 ml-1" id="narrativeIncidentDetails">' + narrativeDetails + '</span>' +
                '<i class="fa fa-quote-right"></i>' +
                '</blockquote>' +
                '</div>' +
                '</div>';
            $('#incidentDetailsView').html(incidentDetails + reasonVal);
            $('#incidentDetailsView').data('narrative', narrativeDetails);

            // Rule
            // Check if previous occurence exist
            if (parseInt(qualifiedObj.details.with_prev) == 1) {
                var withPreviousDetails = "";
                var withIrHistoryDetails = "";
                var previousDetailsHeader = "";
                var previousDetailsBody = "";
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 4) {
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center">Allowable</th>' +
                        '<th class="text-center">Covered</th>' +
                        '<th class="text-center">Over Break</th>';
                    $.each(qualifiedObj.details.previous_details, function(index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date,
                                'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.allowable + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.covered + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.ob + '</td>' +
                            '</tr>';
                    });
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 1 || parseInt(
                        qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                    var durationLabel = "Late";
                    if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 2) {
                        durationLabel = "Undertime"
                    }
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center">Clock In</th>' +
                        '<th class="text-center">' + durationLabel + '</th>';
                    $.each(qualifiedObj.details.previous_details, function(index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date,
                                'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.log,
                                'Y-MM-DD HH:mm:ss').format('MMM DD, Y HH:mm:ss A') + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.formatted_duration +
                            '</td>' +
                            '</tr>';
                    });
                } else if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 6 || parseInt(
                        qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) == 7) {
                    previousDetailsHeader = '<th class="text-center">Date</th>' +
                        '<th class="text-center">Shift</th>' +
                        '<th class="text-center"> Note</th>';
                    $.each(qualifiedObj.details.previous_details, function(index, value) {
                        previousDetailsBody += '<tr>' +
                            '<td class="text-center" style="font-weight:400">' + moment(value.date,
                                'Y-MM-DD').format('MMMM DD, Y') + '</td>' +
                            '<td class="text-center">' + value.shift + '</td>' +
                            '<td class="text-center" style="font-weight:400">' + value.note + '</td>' +
                            '</tr>';
                    });
                }
                withPreviousDetails =
                    '<table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">' +
                    '<thead style="background: #555;color: white;">' +
                    '<tr>' + previousDetailsHeader +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' + previousDetailsBody +
                    '</tbody>' +
                    '</table>';
                $('#previousDetailsView').html(withPreviousDetails);
                $('#withPreviousUserDtrViolationView').show();
            } else {
                $('#withPreviousUserDtrViolationView').hide();
            }
            $('#qualifiedExpectedActionView').text(expectedAction);
            $('#directSupPic').attr('src', baseUrl + "/" + qualifiedObj.details.sup_details.pic)
            $('#directSupName').text(toTitleCase(qualifiedObj.details.sup_details.fname + " " + qualifiedObj.details
                .sup_details.lname));
            $('#directSupJob').text(qualifiedObj.details.sup_details.positionDescription);
            $('#directSupNote').text(qualifiedObj.details.ir_qualified_details.supervisorsNote);
            $('#qualifiedExpectedActionView').data('expectedAction', expectedAction);
            // Check if IR history exist
            console.log(qualifiedObj.details.with_rule);
            if (parseInt(qualifiedObj.details.with_rule)) {
                $('#qualifiedRuleView').html(qualifiedObj.details.ir_rule.rule);
                if (parseInt(qualifiedObj.details.user_dtr_violation.dtrViolationType_ID) != 5) {
                    $('#withRuleView').show();
                } else {
                    $('#withRuleView').hide();
                }
            }
            $('.ir_details_template').parents('.modal.fade.show').attr('id');
            $('#qualifiedDetailsModal').data('prevmodal', $('.ir_details_template').parents('.modal.fade.show')
                .attr('id'));
            $('#qualifiedDetailsModal').data('withprev', withPrev);
            $('#' + $('.ir_details_template').parents('.modal.fade.show').attr('id')).modal('hide')
            $('#qualifiedDetailsModal').modal('show');
        })
    }

    function init_ir_recommendation_details(irIdVal) {
        $.when(fetchPostData({
            irId: irIdVal
        }, '/discipline/get_recommendation_details')).then(function(details) {
            let recommendationDetails = $.parseJSON(details.trim());
            if (parseInt(recommendationDetails.exist)) {
                $('#recommendationDetails').html(recommendationDetails.content);
                $('.ir_details_template').parents('.modal.fade.show').attr('id');
                $('#irRecommendationModal').data('prevmodal', $('.ir_details_template').parents('.modal.fade.show')
                    .attr('id'));
                $('#' + $('.ir_details_template').parents('.modal.fade.show').attr('id')).modal('hide')
                $('#irRecommendationModal').modal('show');
            } else {
                swal("Recommendation Details Not found!",
                    "Please report this error immediately to the System Administrator", "error");
            }

        });
    }

    function init_ir_annexes(irIdVal) {
        $.when(fetchPostData({
            irId: irIdVal
        }, '/discipline/get_ir_annexes_details')).then(function(details) {
            let annexDetails = $.parseJSON(details.trim());
            let irLabel = "Similar IR";
            if (parseInt(annexDetails.with_annexes)) {
                console.log(baseUrl + '/' + annexDetails.emp_details['pic'])
                $('#annexSubPic').attr('src', baseUrl + '/' + annexDetails.emp_details['pic']);
                $('#annexSubName').text(annexDetails.emp_details['name']);
                $('#annexSubPosition').text(annexDetails.emp_details['position']);
                $('#annexSubPositionStat').text(' - ' + annexDetails.emp_details['pos_status']);
                $('#annexSubAccount').text(annexDetails.emp_details['account']);
                $('#annexCount').text(annexDetails.num_annex);
                if (parseInt(annexDetails.num_annex) > 1) {
                    irLabel += "s";
                }
                $('#annexLabel').text(irLabel);
                let annex_content = "";
                $.each(annexDetails.annex_details, function(index, value) {
                    // console.log(value['details']);
                    let annex_num = index + 1;
                    let evidenceLabel = "Attached Evidence"
                    let annexEvidence = '';
                    let annexWitness = '';
                    if (parseInt(value['evidences'].ir_attachment)) {
                        if (parseInt(value['evidences'].ir_attachment) > 1) {
                            evidenceLabel += "s";
                        }
                        annexEvidence +=
                            '<div class="col-12 mt-2"><span class="font-weight-bold mr-2">Evidence Found:</span><span class="viewIrDetails irAnnexEvidence" data-irid="' +
                            value['details'].incidentReport_ID + '">' +
                            value['evidences'].ir_attachment + ' ' + evidenceLabel + '</span></div>';

                    }
                    if (parseInt(value['with_witnesses'])) {
                        console.log(value['witnesses']);
                        let witnessList = "<ol>";
                        $.each(value['witnesses'], function(index2, value2) {
                            witnessList += "<li>" + toTitleCase(value2.fname + " " + value2.lname + " - " + value2.description) + "</li>";
                        })
                        witnessList += "</ol>";
                        annexWitness += '<div class="col-12 mt-2 row"><span class="font-weight-bold mr-2 col-1">Witness:</span><span class="col">' + witnessList + '</span></div>';
                    }
                    annex_content +=
                        '<div class="m-widget3__item px-4 pb-3 pt-2 my-4 rounded" style="background: #f1f1f1;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border-left: 10px solid #607D8B;">' +
                        '<div class="m-widget3__header">' +
                        '<div class="m-widget3__user-img">' +
                        '<img class="m-widget3__img" src="' + baseUrl + '/' + value['details'].sourcePic + '" onerror="noImageFound(this)" alt="">' +
                        '</div>' +
                        '<div class="m-widget3__info">' +
                        '<span class="m-widget3__username">' + toTitleCase(value['details'].sourceFname +
                            " " + value['details'].sourceLname) + '</span><br>' +
                        '<span class="m-widget3__time">Filed last ' + moment(value['details'].dateTimeFiled,
                            'Y-MM-DD HH:mm:ss').format('MMM DD, Y h:mm:ss A') + '</span>' +
                        '</div>' +
                        '<span class="m-widget3__status m--font-danger">#' + annex_num + '</span>' +
                        '</div>' +
                        '<div class="m-widget3__body">' +
                        '<div class="col-12 px-0">' +
                        '<div class="row">' +
                        '<div class="col-12 col-md-6"><span class="font-weight-bold mr-2">Date:</span><span class="">' +
                        moment(value['details'].incidentDate, 'Y-MM-DD').format('MMM DD, Y') +
                        '</span></div>' +
                        '<div class="col-12 col-md-6 text-md-right mt-2 mt-md-0"><span class="font-weight-bold mr-2">Time:</span><span class="">' +
                        moment(value['details'].incidentTime, 'HH:mm').format('h:mm A') + '</span></div>' +
                        '<div class="col-12 mt-2"><span class="font-weight-bold mr-2">Place:</span><span class="">' +
                        value['details'].place + '</span></div>' +
                        '<div class="col-12 mt-2"><span class="font-weight-bold mr-2">Details:</span><span class="">' +
                        value['details'].details + '</span></div>' +
                        '<div class="col-12 mt-2"><span class="font-weight-bold mr-2">Expected Action:</span><span class="">' +
                        value['details'].expectedAction + '</span></div>' +
                        annexEvidence +
                        annexWitness +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                });
                $('#annexDetails').html(annex_content);
                $('.ir_details_template').parents('.modal.fade.show').attr('id');
                $('#irAnnexModal').data('prevmodal', $('.ir_details_template').parents('.modal.fade.show').attr(
                    'id'));
                $('#' + $('.ir_details_template').parents('.modal.fade.show').attr('id')).modal('hide')
                $('#irAnnexModal').modal('show');
            }
        });
    }
    $('.ir_details_template').on('click', '#recommendationDetailsView', function() {
        init_ir_recommendation_details($(this).data('irid'));
    })
    $('.ir_details_template').on('click', '#viewQualifiedDetails', function() {
        initQualifiedDtrView($(this).data('qualifiedid'), 1);
    })
    $('#irableDtrViolationRecord, #irAbleDtrViolationRecordMonitoringDatatable, #dismissedQualifiedDtrViolations').on('click', '.viewQualifiedDetailsRecord', function() {
        initQualifiedDtrView($(this).data('qualifiedid'), 0);
    })
    $('.ir_details_template').on('click', '#irAnnexes', function() {
        init_ir_annexes($(this).data('irid'));
        $('#irAnnexModal').data('showmain', 0);
    })

    $('#annexDetails').on('click', '.irAnnexEvidence', function() {
        $('#modal_displayevidences').data('irid', $(this).data('irid'));
        console.log($(this).data('irid'));
        console.log($('#modal_displayevidences').data('irid'));
        fetch_media_attachment($(this).data('irid'), function(result) {
            // $('#modal_displayevidences').data('annex', 1);
            // $('.ir_details_template').parents('.modal.fade.show').attr('id');
            $('#modal_displayevidences').data('prevmodal', 'irAnnexModal');
            $('#modal_displayevidences').data('modaltype', 'annex');
            $('#irAnnexModal').data('showmain', 1);
            $('#irAnnexModal').modal('hide');
            $('#modal_displayevidences').modal('show');
        });
    })
    function initDmsTaskPopover(){
        $("#dmsTaskFloat").popover({
            container: 'body',
            trigger: 'click',
            placement: 'left',
            html: true,
            template: '\
            <div class="m-popover popover m-popover--skin-light" role="tooltip" style="border-radius: 1.2rem !important;box-shadow: 3px 3px 15px 5px rgba(69,65,78,.25) !important;z-index: 1!important;">\
                <div class="arrow" style="top: 35px;"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body" style="background:linear-gradient(63deg, rgba(255,255,255,1) 0%, rgba(253, 255, 0, 0.85) 70%, rgba(255,222,0,1) 97%);border-radius: 1rem;padding:0.5rem !important"></div>\
            </div>',
            content: '<div class="m-stack m-stack--ver m-stack--general">'+
                '<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--fluid p-2">'+
                '<h5 class="mb-0 row" style="color: #131212;font-weight: 400;font-size: 0.9rem;">'+
                    '<span class="col-1"><i class="fa fa-exclamation-circle mr-2" style="font-size: 1.8rem;"></i></span><span class="col-10"><b>Ooops!</b> You currently have urgent Incident Report involvement/s</span>'+
                '</h5>'+
                    // '<h5 class="mb-0" style="color: #131212;font-weight: 400;font-size: 1rem;"><i class="fa fa-exclamation-circle mr-2"></i>Ooops! You currently have urgent Incident Report involvement/s</h5>'+
                    // '<p class="popover-chat-content mb-0 ml-2">test</p>'+
                    '</div>'+
                '<div class="m-stack__item m-stack__item--center m-stack__item--top" style="width: 20px;">'+
                    '<span class="close-popover-dms"><i class="fa fa-times-circle"></i></span>'+
                    '</div>'+
                '</div>'
        });
        $('#dmsTaskFloat').popover('show');
    }
        
    // DMS TASK CHANGES
    function getDmsCounts(){
        $.when(fetchGetData('/discipline/get_dms_counts')).then(function(dmsCounts) {
            let dmsCountObj = $.parseJSON(dmsCounts.trim());
            console.log(dmsCountObj);
            if((parseInt(dmsCountObj.file_dtr_violation) > 0)||(parseInt(dmsCountObj.my_witness) > 0)||(parseInt(dmsCountObj.subordinate_sub_explanation) > 0)||(parseInt(dmsCountObj.my_sub_explanation) > 0)||(parseInt(dmsCountObj.ir_my_recommendation) > 0)||(parseInt(dmsCountObj.ir_my_client_confirmation) > 0)||(parseInt(dmsCountObj.ir_my_ero_recommendation) > 0)||(parseInt(dmsCountObj.upload_documents) > 0)){
                if(parseInt(dmsCountObj.my_witness) > 0){// My Subject Explanation
                    $(".confirmWitnessDiv").show();
                    let confirmWitnessCount = parseInt(dmsCountObj.my_witness)+ " IR";
                    let confirmWitnessNeed = "needs";
                    let witnessList = [];
                    // WITNESS
                    if(parseInt(dmsCountObj.my_witness) > 1){
                        confirmWitnessCount = parseInt(dmsCountObj.my_witness)+ " IRs";
                        confirmWitnessNeed = "need";
                    }
                    $.each(dmsCountObj.my_witness_list, function(index, value){
                        witnessList[index] = '<a href="'+baseUrl + '/discipline/witness_confirmation/'+value.incidentReport_ID+'">'+value.incidentReport_ID.padLeft(8)+'</a>';
                    })
                    $('#confirmWitnessCount').text(confirmWitnessCount);
                    $('#witnessNeed').text(confirmWitnessNeed);
                    $('#confirmIds').html(witnessList.join(",&nbsp;"));
                }
                if(parseInt(dmsCountObj.my_sub_explanation) > 0){
                    $(".mySubExplanationDiv").show();
                    let mySubExplanationCount = parseInt(dmsCountObj.my_sub_explanation)+ " IR";
                    let mySubExplanationNeed = "needs";
                    let mySubExplanationWitnessList = [];
                    if(parseInt(dmsCountObj.my_sub_explanation) > 1){
                        mySubExplanationCount = parseInt(dmsCountObj.my_sub_explanation)+ " IRs";
                        mySubExplanationNeed = "need";
                    }
                    $.each(dmsCountObj.my_sub_explanation_list, function(index, value){
                        mySubExplanationWitnessList[index] = value.incidentReport_ID.padLeft(8);
                    })
                    $('#mySupervisor').text(dmsCountObj.my_supervisor.fname+" "+dmsCountObj.my_supervisor.lname);
                    $('#mySubExplanationCount').text(mySubExplanationCount);
                    $('#mySubExplanationNeed').text(mySubExplanationNeed);
                    $('#mySubExplanationIds').html(mySubExplanationWitnessList.join(",&nbsp;"));
                }
                if(parseInt(dmsCountObj.subordinate_sub_explanation) > 0){
                    $(".subordinateExplainDiv").show();
                    let subordinateExplanationCount = parseInt(dmsCountObj.subordinate_sub_explanation)+ " IR";
                    let subordinateExplanationNeed = "needs";
                    if(parseInt(dmsCountObj.subordinate_sub_explanation) > 1){
                        subordinateExplanationCount = parseInt(dmsCountObj.subordinate_sub_explanation)+ " IRs";
                        subordinateExplanationNeed = "need";
                    }
                    $('#subordinateExplainCount').text(subordinateExplanationCount);
                    $('#subordinateExplainNeed').text(subordinateExplanationNeed);
                }
                if(parseInt(dmsCountObj.subordinate_sub_explanation) > 0){
                    $(".subordinateExplainDiv").show();
                    let subordinateExplanationCount = parseInt(dmsCountObj.subordinate_sub_explanation)+ " IR";
                    let subordinateExplanationNeed = "needs";
                    if(parseInt(dmsCountObj.subordinate_sub_explanation) > 1){
                        subordinateExplanationCount = parseInt(dmsCountObj.subordinate_sub_explanation)+ " IRs";
                        subordinateExplanationNeed = "need";
                    }
                    $('#subordinateExplainCount').text(subordinateExplanationCount);
                    $('#subordinateExplainNeed').text(subordinateExplanationNeed);
                }
                if(parseInt(dmsCountObj.file_dtr_violation) > 0){
                    $(".fileDtrViolation").show();
                    let fileDtrViolationCount = parseInt(dmsCountObj.file_dtr_violation)+ " IR";
                    let fileDtrViolationLinking = "is";
                    if(parseInt(dmsCountObj.file_dtr_violation) > 1){
                        fileDtrViolationCount = parseInt(dmsCountObj.file_dtr_violation)+ " IRs";
                        fileDtrViolationLinking = "are";
                    }
                    $('.fileDtrViolationLinking').text(fileDtrViolationLinking);
                    $('#fileDtrViolationCount').text(fileDtrViolationCount);
                }
                if(parseInt(dmsCountObj.ir_my_recommendation) > 0){
                    $(".myRecommendationDiv").show();
                    let recommendationCount = parseInt(dmsCountObj.ir_my_recommendation)+ " IR";
                    let recommendationNeed = "needs";
                    if(parseInt(dmsCountObj.ir_my_recommendation) > 1){
                        recommendationCount = parseInt(dmsCountObj.ir_my_recommendation)+ " IRs";
                        recommendationNeed = "need";
                    }
                    $('#recommendationCount').text(recommendationCount);
                    $('#recommendationNeed').text(recommendationNeed);
                }
                if(parseInt(dmsCountObj.ir_my_client_confirmation) > 0){
                    $(".myClientRecommendationDiv").show();
                    let clienRecommendationCount = parseInt(dmsCountObj.ir_my_client_confirmation)+ " IR";
                    let clienRecommendationNeed = "needs";
                    if(parseInt(dmsCountObj.ir_my_client_confirmation) > 1){
                        clienRecommendationCount = parseInt(dmsCountObj.ir_my_client_confirmation)+ " IRs";
                        clienRecommendationNeed = "need";
                    }
                    $('#clientRecommendationCount').text(clienRecommendationCount);
                    $('#clientRecommendationNeed').text(clienRecommendationNeed);
                }
                if(parseInt(dmsCountObj.effectivity_dates) > 0){
                    $(".effectivityDatesDiv").show();
                    let effectivityDatesCount = parseInt(dmsCountObj.effectivity_dates)+ " IR";
                    let effectivityDatesNeed = "needs";
                    if(parseInt(dmsCountObj.effectivity_dates) > 1){
                        effectivityDatesCount = parseInt(dmsCountObj.effectivity_dates)+ " IRs";
                        effectivityDatesNeed = "need";
                    }
                    $('#effectivityDatesCount').text(effectivityDatesCount);
                    $('#effectivityDatesNeed').text(effectivityDatesNeed);
                }
                if(parseInt(dmsCountObj.effectivity_dates) > 0){
                    $(".effectivityDatesDiv").show();
                    let effectivityDatesCount = parseInt(dmsCountObj.effectivity_dates)+ " IR";
                    let effectivityDatesNeed = "needs";
                    if(parseInt(dmsCountObj.effectivity_dates) > 1){
                        effectivityDatesCount = parseInt(dmsCountObj.effectivity_dates)+ " IRs";
                        effectivityDatesNeed = "need";
                    }
                    $('#effectivityDatesCount').text(effectivityDatesCount);
                    $('#effectivityDatesNeed').text(effectivityDatesNeed);
                }
                if(parseInt(dmsCountObj.ir_my_ero_recommendation) > 0){
                    $(".eroRecommendationDiv").show();
                    let eroRecommendationCount = parseInt(dmsCountObj.ir_my_ero_recommendation)+ " IR";
                    let eroRecommendationNeed = "needs";
                    if(parseInt(dmsCountObj.ir_my_ero_recommendation) > 1){
                        eroRecommendationCount = parseInt(dmsCountObj.ir_my_ero_recommendation)+ " IRs";
                        eroRecommendationNeed = "need";
                    }
                    $('#eroRecommendationCount').text(eroRecommendationCount);
                    $('#eroRecommendationNeed').text(eroRecommendationNeed);
                }
                if(parseInt(dmsCountObj.upload_documents) > 0){
                    $(".uploadDocumentsDiv").show();
                    let uploadDocumentsCount = parseInt(dmsCountObj.upload_documents)+ " IR";
                    let uploadDocumentsNeed = "needs";
                    if(parseInt(dmsCountObj.upload_documents) > 1){
                        uploadDocumentsCount = parseInt(dmsCountObj.upload_documents)+ " IRs";
                        uploadDocumentsNeed = "need";
                    }
                    $('#uploadDocumentsCount').text(uploadDocumentsCount);
                    $('#uploadDocumentsNeed').text(uploadDocumentsNeed);
                }
                  // Subordinates Subject Explanation
                    // DTR Violation filing
                    // Recommendation
                $('#dmsTaskFloat').fadeIn();
                initDmsTaskPopover();
            }else{
                $('#dmsTaskFloat').hide();
            }
        });
        
    }
    $("body").on("click", ".close-popover-dms, #dmsTaskFloat", function() {
        $("#dmsTaskFloat").popover('dispose');
    });
    $("#dmsTaskModal").on('hidden.bs.modal', function(){
        initDmsTaskPopover();
    })
    // DMS TASK CHANGES END
</script>

<!-- END OF MIC MIC CODES -->
<!-- START OF CHA CHA CODES -->
<script>
    // HIDDEN MODALS
    $('#modal_displayevidences').on('hidden.bs.modal', function() {
        // console.log($(this).data('modaltype'));
        if ($(this).data('modaltype') == 'annex') {
            $('#irAnnexModal').data('showmain', 0);
        }
        $('#' + $(this).data('prevmodal')).modal('show');
    })

    $('#qualifiedDetailsModal').on('hidden.bs.modal', function() {
        if (parseInt($(this).data('withprev')) == 1) {
            $('#' + $(this).data('prevmodal')).modal('show');
        }
    })

    $('#irAnnexModal').on('hidden.bs.modal', function() {
        console.log($(this).data('showmain'));
        if ($(this).data('showmain') == 0) {
            $('#' + $(this).data('prevmodal')).modal('show');
        }
    })

    $('#irRecommendationModal').on('hidden.bs.modal', function() {
        $('#' + $(this).data('prevmodal')).modal('show');
    })

    $('#uploadExplanationModal_general').on('hidden.bs.modal', function() {
        $('#' + $(this).data('prevmodal')).modal('show');
    })

    // ------------------------------------------------

    $('.ir_details_template').on('click', '#irEvidence', function() {
        // console.log('annexes');
        $('#modal_displayevidences').data('irid', $(this).data('irid'));
        fetch_media_attachment($(this).data('irid'), function(result) {
            // $('#modal_displayevidences').data('annex', 1);
            // $('.ir_details_template').parents('.modal.fade.show').attr('id');
            $('#modal_displayevidences').data('prevmodal', $('.ir_details_template').parents('.modal.fade.show').attr('id'));
            $('#modal_displayevidences').data('modaltype', 'none');
            $('#' + $('.ir_details_template').parents('.modal.fade.show').attr('id')).modal('hide');
            $('#modal_displayevidences').data('irid', $(this).data('irid'));
            $('#modal_displayevidences').modal('show');
        });
    })

    $('.ir_details_template').on('click', '#uploadedExplanation', function() {
        uploadedExplanationGeneral($(this).data('irid'));
    })
    $('#evidence-types').on('change', function() {
        var irid = $('#modal_displayevidences').data('irid');
        fetch_media_attachment(irid, function(result) {});
    });

    function dismiss_audio_modal_evidences(irattachmentid) {
        $('#audio_modal_evidences' + irattachmentid).modal('toggle');
    }

    $('#media_display').on('mouseover', '.attachedFile', function() {
        let filename = $(this).data('filename');
        // $(this).find('.xbutton').show();

        $(this).tooltip({
            title: filename,
            placement: "top"
        });
        $(this).tooltip('show')
    })

    $('#media_display').on('mouseleave', '.attachedFile', function() {
        // $(this).find('.xbutton').hide();
        $(this).tooltip('hide')
    })

    function testt(link) {
        console.log("gg");
    }

    $('#media_display').on('click', '.a_audio_link', function() {
        link = $(this).data('audiolink');
        audio_stringxx = "";
        audio_stringxx += "<audio controls style='width: -webkit-fill-available;'>";
        audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mp3'>";
        audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mpeg'>";
        audio_stringxx += "<source src='" + baseUrl + "/" + link + "' type='audio/mpeg'>";
        audio_stringxx += "</audio>";
        $("#audio_evidence_player").html(audio_stringxx);
    })
</script>

<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision_cha.js"></script>
<!-- END OF CHA CHA CODES -->

</html>