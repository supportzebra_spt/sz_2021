<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Anil Labs - Codeigniter mail templates</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
   <div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header">
   <img style="Margin-left: auto;Margin-right: auto;" src="https://lh3.googleusercontent.com/NZNZzuQogtKpP32IPLfU7ycNwp-VO_a3pqnAB4Owr0sRkJdDEHENGxRulIG2eyEtLcWQdVR5NZOsStRx4zhQePBcFviP2V_F=w1366-h768-rw-no"></div>
	
	<br>
	<br>
	<br>
	<div style="width: 59%;margin: 0 auto;">
	<h2>You've been assigned!</h2>
	<h3><?php echo $senderName; ?> assigned you the checklist:</h3>
	<br>
		<div style="font-family:Helvetica,Arial,sans-serif;font-size:22px;font-weight:600;color:#2c98e3;text-align:center;text-decoration: none;">
		 <?php echo $title; ?>
		</div>
	<br>
		
		<?php 
			if(trim($dueDate)!="0000-00-00 00:00:00"){
				echo "<h4 style='text-align:center;'>Due on ".$dueDate." </h4>";
			}
		?>
	<br>
	<div align="center" style="padding-top:15px;padding-right:30px;padding-bottom:15px;padding-left:30px;border-radius:4px;background-color:#2c98e3;border-color:#357ebd;width: 30%;margin: 0 auto;">
		<a href="<?php echo $link; ?>" style="color: #fff !important;text-decoration: none;"> See the checklist  </a>
	</div>
	</div>
</div>
</body>
</html> 