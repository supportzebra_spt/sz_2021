<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Additional Hour Request Reporting</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Additional Hour Request Reporting</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
            <div>
                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
                    aria-expanded="true">
                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                        <i class="la la-plus m--hide"></i>
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__section m-nav__section--first m--hide">
                                            <span class="m-nav__section-text">Quick Actions</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                <span class="m-nav__link-text">Activity</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                <span class="m-nav__link-text">Messages</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                <span class="m-nav__link-text">FAQ</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                <span class="m-nav__link-text">Support</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit">
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-content ">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="m-portlet m-portlet--head-solid-bg" style="">
                    <div class="m-portlet__head col-md-12" style="padding: 1.2rem 2.2rem !important;background-color: #5e697d;">
                        <div class="row">
                            <div class="col-9 col-sm-8 col-md-6 col-lg-6 col-xl-6" style="margin-top: 6px;">
                                <h5 class=" text-white">
                                    <i class="fa fa-bar-chart-o text-light mr-3" style="font-size: 26px;"></i>Filtered Reports</h5>
                            </div>
                            <div class="col-3 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                                <div class="pull-right">
                                    <button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" id="downloadAhrReportBtn" style="display: none">
                                        <i class="fa fa-download"></i>
                                    </button>
                                    <button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air " id="toggleFilter">
                                        <i class="fa fa-filter"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body p-0 col-md-12">
                        <div class="row p-3" id="filters" style="background: rgba(13, 42, 56, 0.18);margin-left: 0px;margin-right: 0px;border: 1px solid transparent; display: none">
                            <div class="col-sm-6 col-md-4 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="font-weight-bold">Date Range</label>
                                    <div class="input-group pull-right">
                                        <input type="text" class="form-control m-input bg-light" readonly="" placeholder="Select date range" id="filteredReportDateRange">
                                        <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Schedule">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-6 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="font-weight-bold">Number Of:</label>
                                    <select class="custom-select form-control m-input quantityType" id="quantityType2" name="quantityType"><option value="1">Request</option><option value="2"> Hours Rendered </option></select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="font-weight-bold">Payment Stat:</label>
                                    <select class="custom-select form-control m-input" id="paymentStat" name="paymentStat">
                                        <option value="0">ALL</option>
                                        <option value="3">Released</option>
                                        <option value="2">Unreleased</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="font-weight-bold">AHR Type</label>
                                    <select class="custom-select form-control m-input" id="ahrTypeReport" name="ahrTypeReport" placeholder=""><option value="0">ALL</option><option value="1" class="m--font - transform - u">AFTER</option><option value="2" class="m--font - transform - u">PRE</option><option value="3" class="m--font - transform - u">WORD</option></select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="font-weight-bold">AHR Status</label>
                                    <select class="custom-select form-control" id="ahrStatus" name="ahrStatus" placeholder="Select a Shift">
                                        <option value="5">Approved</option>
                                        <option value="6">Disapproved</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-4">
                                <div class="form-group m-form__group">
                                    <label class="font-weight-bold">Employee Type</label>
                                    <select class="custom-select form-control" id="empType" name="empType" placeholder="Select a Shift">
                                        <option value="0">ALL</option>
                                        <option value="Admin">Admin</option>
                                        <option value="Agent">Ambassador</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row ml-0 mr-0 p-4">
                            <div id="filteredChart" class="filteredChart bg-black col-md-12" data-style="dark" data-theme="bg-white"></div>
                            <div class="no-data col-md-12 p-5 noFilteredData" style="text-align: center;display: inline-block;">
                                <div class="m-demo-icon__preview">
                                    <i style="font-size: 40px;" class="flaticon-notes"></i>
                                </div>
                                <span class="m-demo-icon__class">No Result Found</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="m-portlet m-portlet--head-solid-bg" style="">
                    <div class="m-portlet__head col-md-12" style="padding: 1.2rem 2.2rem !important;background-color: #5e697d;">
                        <div class="row">
                            <div class="col-9 col-sm-8 col-md-6 col-lg-6 col-xl-6" style="margin-top: 6px;">
                                <h5 class=" text-white">
                                    <i class="fa fa-table text-light mr-3" style="font-size: 26px;"></i>Report Details</h5>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding: 1.2rem 2.2rem !important;">
                        <div class="row">
                            <div class="no-data col-md-12 noFilteredData" style="text-align: center;display: inline-block;">
                                <div class="m-demo-icon__preview">
                                    <i style="font-size: 40px;" class="flaticon-notes"></i>
                                </div>
                                <span class="m-demo-icon__class">No Result Found</span>
                            </div>
                            <div class="col-md-12" id="accountTable" style="display:none;">
                                <table class="table table-striped m-table m-table--head-bg-metal">
                                    <thead>
                                        <tr class="text-center">
                                            <th id="accountLabel" class="text-dark font-weight-bold">Account</th>
                                            <th id="countLabel" class="text-dark font-weight-bold">Number Request</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center" id="bodyCount"></tbody>
                                </table>
                            </div>
                            <div class="col-md-12" id="empDatatable" style="display:none;">
                                <div class="row">
                                    <div class="col-md-5 mb-3">
                                        <select class="form-control m-select2 empSelect" id="empSelect1" name="empSelect1" data-toggle="m-tooltip" title="Select Employee Name"></select>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group m-form__group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search by AHR ID, Date Filed, Schedule Date" id="employeeAhrRecordSearch">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data shown in the table below.</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="employeeAhrRecordDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="recordInfoAhrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="approvalAhrForm" novalidate="novalidate" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Additional Hour Request Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="row">
                               <div class="col-12 col-sm-3 col-md-3 col-lg-3">
                                    <img id="empPicApproval" src="" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-9">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-sm-left" style="font-size: 19px;" id="empNameRecord"></div>
                                        <div class="col-md-12 text-center text-sm-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="empJobRecord"></div>
                                        <div class="col-md-12 mb-1 text-center text-sm-left" style="font-size: 12px" id="empAccountRecord"></div>
                                    </div>
                                </div>
                            </div>
                            <hr style="padding-top:1px;" class="bg-primary">
                            <div class="col-md-12 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">AHR Number:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="ahrNoRecord">00000100</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Filed:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="dateFiledRecord">Aug 10, 2018 12:44 AM</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Filing Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="filingTypeRecord">rendered</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Overtime Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7 .text-capitalize" id="ahrTypeRecord">After Shift</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Rendered:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="dateRenderedRecord">Aug 16, 2017</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="shiftRecord">11:00:00 PM 8:00:00 AM</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="shiftTypeRecord">Normal</div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Status:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="approvalStatRecord">approved</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 borderLeft stack-border-left-only">
                            <div class="col-md-12 mb-3">
                                <label>
                                    <b>REASON:</b>
                                </label>
                                <div class="col-md-12 ahr_quote pt-3 pb-1">
                                    <blockquote>
                                        <i class="fa fa-quote-left"></i>
                                        <span class="mb-0 text-capitalize" id="reasonRecord"></span>
                                        <i class="fa fa-quote-right"></i>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>
                                    <b>RENDER DETAILS:</b>
                                </label>
                                <table class="table table-bordered table-hover">
                                    <tbody class=" font-size-12">
                                        <tr>
                                            <th scope="row">START:</th>
                                            <td id="startOtRecord"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">END</th>
                                            <td id="endOtRecord"></td>
                                        </tr>
                                        <tr id="breakRecord">
                                            <th scope="row">BREAK</th>
                                            <td id="breakRecordVal"></td>
                                        </tr>
                                        <tr id="bctRecord">
                                            <th scope="row">BCT</th>
                                            <td id="bctRecordVal"></td>
                                        </tr>
                                        <tr id="tardRecord">
                                            <th scope="row">TARDINESS</th>
                                            <td>
                                                <div class="col-md-12" id="lateRecord"></div>
                                                <div class="col-md-12" id="utRecord"></div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <label>
                                    <b>COVERAGE:</b>
                                </label>
                                <div class="form-group">
                                    <h3 id="renderedOtRecord" class="font-size-39">00 hr, 00 min</h3>
                                    <span class="font-size-12 pull-right">overtime</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Close</button>
                </div>
        </div>
        </form>
    </div>
</div>
<div class="modal fade" id="approversInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Approval Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
                    <div id="approvalBody"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- <script src="<?php echo base_url();?>node_modules/moment/moment.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/src/custom/plugins/yearSelector/lib/year-select.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/ahr_common.js"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/ahr_reporting.js"></script>
<script src="<?php echo base_url();?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>