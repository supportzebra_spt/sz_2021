<style>
    td.disabled.day {
            background: #dcdcdc !important;
            border-radius: 0;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Personal Additional Hour Request</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Personal Additional Hour Request</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
            <div>
                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
                    aria-expanded="true">
                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                        <i class="la la-plus m--hide"></i>
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__section m-nav__section--first m--hide">
                                            <span class="m-nav__section-text">Quick Actions</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                <span class="m-nav__link-text">Activity</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                <span class="m-nav__link-text">Messages</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                <span class="m-nav__link-text">FAQ</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                <span class="m-nav__link-text">Support</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit">
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-content ">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show alert-warning d-none" role="alert" id="dueNotif">
                    <div class="m-alert__icon">
                        <i class="fa fa-warning" style="font-size: 30px;"></i>
                        <span></span>
                    </div>
                    <div class="m-alert__text m--font-danger text-center" style="padding: 0.45rem 1.25rem !important; font-size: 16px;">
                        You have &nbsp;<strong><span class="dueCount" style="font-size: 21px;"></span></strong> Additional Hour <span id="requestForm"></span>&nbsp;that your approvers Missed to approve on time <span id="checkPersonalRequest" class="text-info btn" style="font-size: 10px;">(View Request Tab below for Details)</span>
                    </div>
                </div>
                <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                 <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#pendingPersonalAhrRequest" role="tab">
                                        <i class="fa fa-bell"></i> Request</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#personalAhrRecord" role="tab">
                                        <i class="la la-th-list"></i> Records</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">                   
                        <div class="tab-content">
                            <div class="tab-pane fade show active col-md-12" id="pendingPersonalAhrRequest" role="tabpanel">
                                <div class="row">
                                    <div class="col-12 col-md-2 col-lg-3 col-xl-2">
                                        <label for="">
                                            <b>Overall Count:</b>
                                        </label>
                                        <div class="row">
                                            <div class="col-12 col-sm-6 col-md-12">
                                                <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                    <div class="m-alert__icon mini_alert bg-secondary">
                                                        <span class="request m-alert--icon-solid m-alert__icon m-animate-shake" style="padding: 7px 0px 7px 0px !important;">
                                                            <i class="la la-bell text-info"></i>
                                                        </span>
                                                        <span style="border-left-color: #343a40 !important;"></span>
                                                    </div>
                                                    <div class="m-alert__text text-center mini_alert text-dark">
                                                        <strong class="font-size-20" id="requestCount">0</strong>
                                                        <br>
                                                        <span class="text-info" style="font-weight: 500;">Request</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12">
                                                <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde !important;">
                                                    <div class="m-alert__icon mini_alert bg-secondary">
                                                        <span class="pending m-alert--icon-solid m-alert__icon fa-spin" style="padding: 7px 3px 7px 4px !important;">
                                                            <i class="fa fa-spinner text-primary"></i>
                                                        </span>
                                                        <span style="border-left-color: #343a40 !important;"></span>
                                                    </div>
                                                    <div class="m-alert__text text-center mini_alert text-dark">
                                                        <strong class="font-size-20" id="pendingCount">0</strong>
                                                        <br>
                                                        <span class="text-primary" style="font-weight: 500;">Pending</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12">
                                                <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde !important;">
                                                    <div class="m-alert__icon mini_alert bg-secondary">
                                                        <span class="missed m-alert--icon-solid m-alert__icon m-animate-blink" style="padding: 7px 3px 7px 4px !important;">
                                                            <i class="fa fa-warning text-warning"></i>
                                                        </span>
                                                        <span style="border-left-color: #343a40 !important;"></span>
                                                    </div>
                                                    <div class="m-alert__text text-center mini_alert text-dark">
                                                        <strong class="font-size-20 dueCount">0</strong>
                                                        <br>
                                                        <span class="text-warning" style="font-weight: 500;">Missed</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-10 col-lg-9 col-xl-10">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 mb-3" data-toggle="m-tooltip" title="Request Status">
                                                    <select class="custom-select form-control" id="requestStatus" name="requestStatus">
                                                        <option value="0">ALL</option>
                                                        <option value="2">Pending</option>
                                                        <option value="12">Missed</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-7">
                                                    <div class="form-group m-form__group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search by AHR ID, Schedule, Date Filed" id="personalPendingAhrSearch">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-search"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data below.</span>

                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-2 mb-3">
                                                    <div class="dropdown">
                                                        <button class="btn btn-outline-success m-btn m-btn--icon m-btn--outline-2x pull-right" type="button" aria-haspopup="true"
                                                            aria-expanded="false" id="unrenderedOnly" href="#" data-toggle="modal"
                                                            data-target="#renderedAhrFilingModal" transacton="add">Request</button>
<!--                                                        <button class="btn btn-outline-success m-btn m-btn--icon m-btn--outline-2x dropdown-toggle pull-right d-none" type="button"
                                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">Request</button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; transform: translate3d(589px, 39px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#renderedAhrFilingModal" transacton="add">
                                                                <i class="fa fa-bell"></i>Rendered</a>
                                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#unrenderedAhrFilingModal" transacton="add">
                                                                <i class="fa fa-cloud-upload"></i>Unrendered</a>
                                                        </div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="personalAhrPendingDatatable"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="personalAhrRecord" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-xl-7">
                                                <div class="form-group m-form__group row">
                                                    <label class="col-form-label col-4 col-sm-4 col-md-4 col-lg-3 col-xl-4">
                                                        <b>REQUEST STATUS</b>
                                                    </label>
                                                    <div class="col-8 col-sm-8 col-md-8 col-lg-9 col-xl-8">
                                                        <div class="input-group-append">
                                                            <div class="m-radio-inline">
                                                                <label class="m-radio m-radio--check-bold m-radio--state-success">
                                                                    <input type="radio" name="approvalStat" value="5" checked> Approved
                                                                    <span></span>
                                                                </label>
                                                                <label class="m-radio m-radio--check-bold m-radio--state-danger">
                                                                    <input type="radio" name="approvalStat" value="6"> Disapproved
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-5">
                                                <div class="form-group m-form__group row">
                                                    <label class="col-form-label col-sm-3 col-md-4 col-lg-3 col-xl-4">
                                                        <b>SCHED DATE</b>
                                                    </label>
                                                    <div class="col-sm-9 col-md-8 col-lg-9 col-xl-8">
                                                        <div class="form-group m-form__group row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                                    <input type='text' class="form-control m-input" readonly placeholder="Select date range" id="personalDateRange" />
                                                                    <div class="input-group-append" data-toggle="m-tooltip" title="Date Filed">
                                                                        <span class="input-group-text">
                                                                            <i class="la la-calendar-check-o"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-top: -25px;">
                                        <hr style="padding-top:1px;" class="bg-metal">
                                    </div>
                                    <div class="col-md-12 mb-1">
                                        <div class="form-group m-form__group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search by AHR ID, Schedule, Date Filed" id="personalAhrRecordSearch">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data below.</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="personalAhrRecordDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>
                </div>
                <!-- <button type="button" class="btn btn-secondary" id="testBtn">TEST</button> -->


            </div>
            <!-- </div> -->
            <!-- MODALS -->
            <div class="modal fade" id="renderedAhrFilingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static"
                data-keyboard="false" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <form id="renderedAhrFilingForm" method="post">
                            <div class="modal-header">
                                <span>
                                    <i class="flaticon flaticon-clock text-light mr-1" style="font-size: 26px;"></i>
                                </span>
                                <h5 class="modal-title mt-2" id="exampleModalLabel">Additional Hour Request Form <span class="text-success" style="font-size: 12px;">(RENDERED OVERTIME<span class="btn p-1" data-toggle="m-popover" title="RENDERED OVERTIME" data-content="This is a type of overtime that is rendered in the past."><i class="fa fa-question-circle text-light"></i></span>)</span></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="errNotifyRenderedAhr" class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert"
                                    style=" border-color: rgb(255, 184, 34) !important;">
                                    <div class="m-alert__icon mini_alert bg-danger">
                                        <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 3px 7px 4px !important;">
                                            <i class="fa fa-warning text-light"></i>
                                        </span>
                                        <span style="border-left-color: #343a40 !important;"></span>
                                    </div>
                                    <div class="m-alert__text text-center mini_alert text-danger" id="errNotifyRenderedAhrMssg"></div>
                                </div>
                                <div id="infoNotifyRenderedAhr" class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert"
                                    style="border-color: rgb(54, 163, 247) !important;">
                                    <div class="m-alert__icon mini_alert bg-info">
                                        <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 3px 7px 4px !important;">
                                            <i class="fa fa-info-circle text-light"></i>
                                        </span>
                                        <span style="border-left-color: #343a40 !important;"></span>
                                    </div>
                                    <div class="m-alert__text text-center mini_alert text-primary" id="infoNotifyRenderedAhrMssg"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-4 stack-no-border">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="">#
                                                        <span>1</span> Specify AHR Details</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="margin-top: -12px;">
                                                <hr style="padding-top:1px;" class="bg-primary">
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">AHR Type</label>
                                                    <select class="custom-select form-control" id="ahrTypeRendered" name="ahrType">
                                                        <option value="1" class="m--font - transform - u">AFTER</option>
                                                        <option value="2" class="m--font - transform - u">PRE</option>
                                                        <option value="3" class="m--font - transform - u">WORD</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Date Rendered</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control m-input dateRendered datePicker" readonly="" id="dateRendered" name="dateRendered" placeholder="Select a Date">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Shift</label>
                                                    <select class="custom-select form-control" id="shift" name="shift" placeholder="Select a Shift">
                                                        <option value="" hidden>Select a Shift</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="borderLeft col-md-12 col-lg-4 stack-border-left-only">
                                        <div id="preAfterDetails" class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div id="guide2" class="">#2 Specify Actual Time In</div>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="margin-top: -12px;">
                                                <hr style="padding-top:1px;" class="bg-primary">
                                            </div>
                                            <div class="col-md-12">
                                                <label for="recipient-name" class="form-control-label">DTR DETAILS</label>
                                                <table class="table table-bordered table-hover">
                                                    <tbody class=" font-size-12">
                                                        <tr>
                                                            <th scope="row">Shift Type</th>
                                                            <td id="preAfterShiftType"></td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row" id="dtrLogLabel">Clock Out</th>
                                                            <td id="dtrLogVal"></td>
                                                        </tr>
                                                        <tr id="bct">
                                                            <th scope="row">BCT</th>
                                                            <td id="bctVal"></td>
                                                        </tr>
                                                        <tr id="dtrTardNotWord">
                                                            <th scope="row">Tardiness</th>
                                                            <td id="dtrTardNotWordVal" class="text-danger"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="recipient-name" class="form-control-label" id="inputTimeLabel">ACTUAL DTR OUT</label>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Date</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control m-input datePicker" readonly="" id="actualDtrDate" name="actualDtrDate" placeholder="Select a Date">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-12">
                                                <div class="form-group">
                                                    <div class="input-group timepicker">
                                                        <label class="form-control-label">Time</label>
                                                        <div class="input-group">
                                                            <input id="actualDtrTime" name="actualDtrTime" type="text" class="form-control m-input" readonly="" placeholder="Select a Time">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="la la-exclamation-circle"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="wordDetails" style="display: none;">
                                            <div class="form-group">
                                                <div id="guide2" class="">#2 Review WORD DETAILS</div>
                                            </div>
                                            <hr style="padding-top:1px;" class="bg-primary">
                                            </br>
                                            <label for="recipient-name" class="form-control-label">DTR DETAILS</label>
                                            <table class="table table-bordered table-hover">
                                                <tbody class=" font-size-12">
                                                    <tr>
                                                        <th scope="row">Shift Type</th>
                                                        <td id="wordShiftType"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">DTR IN</th>
                                                        <td id="wordDtrIn"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">DTR OUT</th>
                                                        <td id="wordDtrOut"></td>
                                                    </tr>
                                                    <tr id="dtrTard">
                                                        <th scope="row">Tardiness</th>
                                                        <td id="dtrTardVal" class="text-danger"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">TOTAL GIVEN BREAK</th>
                                                        <td id="breakVal"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="borderLeft col-md-12 col-lg-4 stack-border-left-only">
                                        <div class="form-group">
                                            <div class="">#3 Indicate your Reason</div>
                                        </div>
                                        <hr style="padding-top:1px;" class="bg-primary">
                                        </br>
                                        <div class="form-group">
                                            <h3 id="renderedOt" class="font-size-28">00 hr, 00 min</h3>
                                            <span class="font-size-12 pull-right">overtime</span>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Reason</label>
                                            <textarea class="form-control m-input text-counter" id="reason" name="reason" rows="5" placeholder="Input your reason here..." maxlength="100"></textarea>
                                            <span class="character-remaining" style="font-size: 13px;font-weight: 400;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary" id="requestAhrBtn">File Request</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="unrenderedAhrFilingModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
                data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <form id="unrenderedAhrForm" method="post">
                            <div class="modal-header">
                                 <span>
                                    <i class="flaticon flaticon-clock text-light mr-1" style="font-size: 26px;"></i>
                                </span>
                                <h5 class="modal-title mt-2" id="exampleModalLabel">Additional Hour Request Form <span class="text-warning" style="font-size: 12px;">(UNRENDERED OVERTIME<span class="btn p-1" data-toggle="m-popover" title="UNRENDERED OVERTIME" data-content="This is a pre-approved type of overtime that it can only be rendered in the future if approved by the Supervisors."><i class="fa fa-question-circle text-light"></i></span>)</span></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="errNotifyUnrenderedAhr" class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert"
                                    style=" border-color: rgb(255, 184, 34) !important;">
                                    <div class="m-alert__icon mini_alert bg-danger">
                                        <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 3px 7px 4px !important;">
                                            <i class="fa fa-warning text-light"></i>
                                        </span>
                                        <span style="border-left-color: #343a40 !important;"></span>
                                    </div>
                                    <div class="m-alert__text text-center mini_alert text-danger" id="errNotifyUnrenderedAhrMssg"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-6 borderLeft stack-no-border">
                                        <div class="form-group">
                                            <div class="">#
                                                <span>1</span> Specify AHR Details</div>
                                        </div>
                                        <hr style="padding-top:1px;" class="bg-primary">
                                        </br>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="recipient-name" class="form-control-label">AHR Type</label>
                                                    <select class="custom-select form-control" id="ahrTypeUnrendered" name="ahrType"></select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="recipient-name" class="form-control-label">Date Schedule</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control m-input dateRenderedManual" readonly="" id="schedDate" name="schedDate">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" id="shiftField">
                                            <div class="form-group">
                                                <label class="form-control-label">Shift</label>
                                                <select class="custom-select form-control" id="shiftunrendered" name="shiftunrendered" placeholder="Select a Shift"></select>
                                            </div>
                                        </div>
                                        <div class="form-group" id="shiftWord">
                                            <div class="form-group">
                                                <label class="form-control-label">Shift</label>
                                                <select class="form-control m-select2 select2" id="shiftWordUnrendered" name="shiftWord" placeholder="Select a Shift"></select>
                                            </div>
                                        </div>
                                        <div class="form-group" id="additional">
                                            <table class="table table-bordered table-hover">
                                                <tbody class=" font-size-12">
                                                    <tr>
                                                        <th scope="row" id="additionalLabel"></th>
                                                        <td id="additionalVal"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="recipient-name" class="form-control-label">Start Date</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control m-input datePicker dateRenderedManual" readonly="" id="startDate" name="startDate">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="recipient-name" class="form-control-label">Start Time</label>
                                                    <div class="input-group timepicker">
                                                        <input type="text" class="form-control m-input unrenderedTimePicker" readonly="" id="startTime" name="startTime">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-exclamation-circle"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">End Date</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control m-input dateRenderedManual" readonly="" id="endDate" name="endDate">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">End Time</label>
                                                    <div class="input-group timepicker">
                                                        <input type="text" class="form-control m-input unrenderedTimePicker" readonly="" id="endTime" name="endTime">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-exclamation-circle"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6 borderLeft stack-border-left-only">
                                        <div class="form-group">
                                            <div class="">#2 Indicate your Reason</div>
                                        </div>
                                        <hr style="padding-top:1px;" class="bg-primary">
                                        </br>
                                        <div class="form-group">
                                            <span class="font-size-39" id="unrenderedOt">00 hr, 00 min</span>
                                            <span class="font-size-12 pull-right">overtime</span>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Reason</label>
                                            <textarea class="form-control m-input text-counter" id="reasonManual" name="reasonManual" maxlength="100"  rows="5" placeholder="Input your reason here..."></textarea>
                                            <span class="character-remaining" style="font-size: 13px;font-weight: 400;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary" id="requestManualAhrBtn">File Request</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="recordInfoAhrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
                data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <form id="approvalAhrForm" novalidate="novalidate" method="post">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Additional Hour Request Info</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-6">
                                        <div class="row">
                                            <div class="col-12 col-sm-3 col-md-3 col-lg-3">
                                                <img id="empPicApproval" src="" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                            </div>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-9">
                                                <div class="row">
                                                    <div class="col-12 col-md-12 mb-1 text-center text-sm-left" style="font-size: 19px;" id="empNameRecord"></div>
                                                    <div class="col-md-12 text-center text-sm-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="empJobRecord"></div>
                                                    <div class="col-md-12 mb-1 text-center text-sm-left" style="font-size: 12px" id="empAccountRecord"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr style="padding-top:1px;" class="bg-primary">
                                        <div class="col-md-12 mb-3">
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">AHR Number:</label>
                                                <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="ahrNoRecord">00000100</div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Filed:</label>
                                                <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="dateFiledRecord">Aug 10, 2018 12:44 AM</div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Filing Type:</label>
                                                <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="filingTypeRecord">rendered</div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Overtime Type:</label>
                                                <div class="col-6 col-sm-7 col-md-7 .text-capitalize" id="ahrTypeRecord">After Shift</div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Rendered:</label>
                                                <div class="col-6 col-sm-7 col-md-7" id="dateRenderedRecord">Aug 16, 2017</div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift:</label>
                                                <div class="col-6 col-sm-7 col-md-7" id="shiftRecord">11:00:00 PM 8:00:00 AM</div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift Type:</label>
                                                <div class="col-6 col-sm-7 col-md-7" id="shiftTypeRecord">Normal</div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Status:</label>
                                                <div class="col-6 col-sm-7 col-md-7" id="approvalStatRecord">approved</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6 borderLeft stack-border-left-only">
                                        <div class="col-md-12 mb-3">
                                            <label>
                                                <b>REASON:</b>
                                            </label>
                                            <div class="col-md-12 ahr_quote pt-3 pb-1">
                                                <blockquote>
                                                    <i class="fa fa-quote-left"></i>
                                                    <span class="mb-0 text-capitalize" id="reasonRecord"></span>
                                                    <i class="fa fa-quote-right"></i>
                                                </blockquote>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label>
                                                <b>RENDER DETAILS:</b>
                                            </label>
                                            <table class="table table-bordered table-hover">
                                                <tbody class=" font-size-12">
                                                    <tr>
                                                        <th scope="row">START:</th>
                                                        <td id="startOtRecord"></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">END</th>
                                                        <td id="endOtRecord"></td>
                                                    </tr>
                                                    <tr id="breakRecord">
                                                        <th scope="row">BREAK</th>
                                                        <td id="breakRecordVal"></td>
                                                    </tr>
                                                    <tr id="bctRecord">
                                                        <th scope="row">BCT</th>
                                                        <td id="bctRecordVal"></td>
                                                    </tr>
                                                    <tr id="tardRecord">
                                                        <th scope="row">TARDINESS</th>
                                                        <td>
                                                            <div class="col-md-12" id="lateRecord"></div>
                                                            <div class="col-md-12" id="utRecord"></div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <label>
                                                <b>COVERAGE:</b>
                                            </label>
                                            <div class="form-group">
                                                <h3 id="renderedOtRecord" class="font-size-39">00 hr, 00 min</h3>
                                                <span class="font-size-12 pull-right">overtime</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Close</button>
                            </div>
                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="approversInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Approval Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
                    <div id="approvalBody"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/src/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>node_modules/moment/moment.js" type="text/javascript"></script> -->
<script src="<?php echo base_url();?>assets/src/custom/js/ahr_common.js"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/personal_ahr2.js"></script>
<!-- <script src="<?php echo base_url();?>assets/src/custom/js/personal_ahr.js"></script> -->