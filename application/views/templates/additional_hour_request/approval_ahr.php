<style>
.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell, .m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell, .m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell {
    display: table-cell;
    font-size: 0.85rem !important;
}
.m-checkbox>span, .m-radio>span {
    top: -3px !important;
    /* height: 16px !important; */
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Additional Hour Request Approval</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Additional Hour Request Approval</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
            <div>
                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                        <i class="la la-plus m--hide"></i>
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__section m-nav__section--first m--hide">
                                            <span class="m-nav__section-text">Quick Actions</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                <span class="m-nav__link-text">Activity</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                <span class="m-nav__link-text">Messages</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                <span class="m-nav__link-text">FAQ</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                <span class="m-nav__link-text">Support</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit">
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-content ">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--tabs">
                     <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                 <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#pendingApprovalAhrRequest" role="tab">
                                        <i class="la la-refresh"></i> Pending</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#approvalAhrRecord" role="tab">
                                        <i class="la la-th-list"></i> Records</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="pendingApprovalAhrRequest" role="tabpanel">
                                <!-- <div class="col-md-12"> -->
                                    <!-- <div class="col-12 col-md-10 col-lg-9 col-xl-10"> -->
                                        <div class="row px-3">
                                            <div class="col-md-4 p-1">
                                                <div class="form-group m-form__group">
                                                    <label class="font-weight-bold">Account</label>
                                                    <select class="form-control m-select2 accountSelect" id="accountSelect" name="accountSelect"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-5 p-1">
                                                <div class="form-group m-form__group">
                                                    <label class="font-weight-bold">Employee</label>
                                                    <select class="form-control m-select2 " id="requestorSelect" name="requestorSelect"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 p-1">
                                                <div class="form-group m-form__group">
                                                    <label class="font-weight-bold">Approval Level</label>
                                                    <select class="custom-select form-control" id="approvalLevel" name="approvalLevel">
                                                        <option value="0">ALL</option>
                                                        <option value="1">Level 1</option>
                                                        <option value="2">Level 2</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 p-1">
                                                <div class="form-group m-form__group">
                                                    <!-- <label class="font-weight-bold">Search</label> -->
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="Search by AHR ID, Deadline, or Schedule Date" id="approvalPendingAhrSearch">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <span class="m-form__help font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data shown in the table below.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row quick_approval_btns mx-1 my-2" style="display:none;background: #3d3e4d;">
                                            <div class="col-12 py-3">
                                                <div class="pull-right">
                                                    <button type="submit" class="btn btn-outline-info m-btn m-btn--icon quick_appr_btn" data-approval="approve" data-appr="approver">
                                                        <span>
                                                            <i class="fa fa-thumbs-o-up"></i>&nbsp;Approve
                                                        </span>
                                                    </button>
                                                    <button type="submit" class="btn btn-outline-danger m-btn m-btn--icon quick_appr_btn" data-approval="disapprove" data-appr="approver">
                                                        <span>
                                                            <i class="fa fa-thumbs-o-down"></i>&nbsp;Dissapprove
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="approvalAhrPendingDatatable"></div>
                                            </div>
                                        </div>
                                        <div class="row quick_approval_btns mx-1 my-3" style="display:none;background: #3d3e4d;">
                                            <div class="col-12 py-3">
                                                <div class="pull-right">
                                                    <button type="submit" class="btn btn-outline-info m-btn m-btn--icon quick_appr_btn" data-approval="approve" data-appr="approver">
                                                        <span>
                                                            <i class="fa fa-thumbs-o-up"></i>&nbsp;Approve
                                                        </span>
                                                    </button>
                                                    <button type="submit" class="btn btn-outline-danger m-btn m-btn--icon quick_appr_btn" data-approval="disapprove" data-appr="approver">
                                                        <span>
                                                            <i class="fa fa-thumbs-o-down"></i>&nbsp;Dissapprove
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                <!-- </div> -->
                            </div>
                            <div class="tab-pane" id="approvalAhrRecord" role="tabpanel">         
                                <div class="row">
                                    <div class="col-12 col-md-2 col-lg-3 col-xl-2">
                                        <label for=""><b>Overall Count:</b></label>
                                        <div class="row">
                                            <div class="col-6 col-sm-3 col-md-12">
                                                <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;" data-toggle="m-tooltip" title="Records">
                                                    <div class="m-alert__icon mini_alert bg-secondary">
                                                        <span class="request m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                            <i class="fa fa-archive text-primary"></i>
                                                        </span>
                                                        <span style="border-left-color: #343a40 !important;"></span>
                                                    </div>
                                                    <div class="m-alert__text text-center mini_alert text-dark">
                                                        <strong class="font-size-20" id="recordCount">0</strong>
                                                        <br>
                                                        <!-- <span class="text-info" style="font-weight: 500;">Record</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-3 col-md-12">
                                                <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde !important;" data-toggle="m-tooltip" title="Approved AHR Requests">
                                                    <div class="m-alert__icon mini_alert bg-secondary">
                                                        <span class="approved m-alert--icon-solid m-alert__icon" style="padding: 7px 3px 7px 4px !important;">
                                                            <i class="fa fa-thumbs-o-up text-info"></i>
                                                        </span>
                                                        <span style="border-left-color: #343a40 !important;"></span>
                                                    </div>
                                                    <div class="m-alert__text text-center mini_alert text-dark">
                                                        <strong class="font-size-20" id="approvedCount">0</strong>
                                                        <br>
                                                        <!-- <span class="text-primary" style="font-weight: 500;">Approved</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-3 col-md-12">
                                                <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde !important;" data-toggle="m-tooltip" title="Disapproved AHR Requests">
                                                    <div class="m-alert__icon mini_alert bg-secondary">
                                                        <span class="dissapproved m-alert--icon-solid m-alert__icon" style="padding: 7px 3px 7px 4px !important;">
                                                            <i class="fa fa-thumbs-o-down text-danger"></i>
                                                        </span>
                                                        <span style="border-left-color: #343a40 !important;"></span>
                                                    </div>
                                                    <div class="m-alert__text text-center mini_alert text-dark">
                                                        <strong class="font-size-20" id="disapprovedCount">0</strong>
                                                        <br>
                                                        <!-- <span class="text-warning" style="font-weight: 500;">Disapproved</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6 col-sm-3 col-md-12">
                                                <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde !important;" data-toggle="m-tooltip" title="Missed to Approve AHR Requests">
                                                    <div class="m-alert__icon mini_alert bg-secondary">
                                                        <span class="missed m-alert--icon-solid m-alert__icon" style="padding: 7px 3px 7px 4px !important;">
                                                            <i class="fa fa-warning text-warning"></i>
                                                        </span>
                                                        <span style="border-left-color: #343a40 !important;"></span>
                                                    </div>
                                                    <div class="m-alert__text text-center mini_alert text-dark">
                                                        <strong class="font-size-20" id="dueCount">0</strong>
                                                        <br>
                                                        <!-- <span class="text-warning" style="font-weight: 500;">Missed</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-10 col-lg-9 col-xl-10">
                                        <div class="row">
                                            <!-- <div class="row"> -->
                                                <div class="col-sm-6 col-md-3 mb-3">
                                                    <select class="custom-select form-control" id="approvalRequestStatus" name="approvalRequestStatus" data-toggle="m-tooltip" title="Request Status">
                                                        <option value="0">ALL</option>
                                                        <option value="5">Approved</option>
                                                        <option value="6">Dissapproved</option>
                                                        <option value="12">Missed</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class='input-group pull-right' id='dateRangeApprovalRecord'>
                                                                <input type='text' class="form-control m-input" readonly placeholder="Select date range" id="approvalDateRange"/>
                                                                <div class="input-group-append" data-toggle="m-tooltip" title="Date Schedule">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-check-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-5 mb-3">
                                                    <select class="form-control m-select2 empSelect" id="empSelect2" name="empSelect2" placeholder="Select a Shift"></select>
                                                </div>
                                                <div class="col-sm-6 col-md-12">
                                                    <div class="form-group m-form__group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Search by AHR ID or Schedule Date" id="approvalRecordSearch">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="basic-addon2">
                                                                    <i class="fa fa-search"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data shown in the table below.</span>
                                                    </div>
                                                </div>
                                            <!-- </div> -->
                                        </div>
                                        <!-- <div class="col-md-12"> -->
                                            <div id="approvalRecordAhrDatatable"></div>
                                        <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODALS -->


<div class="modal fade" id="approvalAhrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="approvalAhrForm" method="post">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-pencil-square-o text-light mr-1" style="font-size: 26px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Additional Hour Request Approval</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                   
                </div>
                <div class="modal-body">
                    <div id="errNotifyApprovalAhr" class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: rgb(255, 184, 34) !important;">
                        <div class="m-alert__icon mini_alert bg-danger">
                            <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 3px 7px 4px !important;">
                                <i class="fa fa-warning text-light"></i>
                            </span>
                            <span style="border-left-color: #343a40 !important;"></span>
                        </div>
                        <div class="m-alert__text text-center mini_alert text-danger" id="errNotifyApprovalMssg"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="row">
                                <div class="col-12 col-sm-3 col-md-3 col-lg-3">
                                    <img id="empPicApproval" src="" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-9">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-sm-left" style="font-size: 19px;" id="employeeName"></div>
                                        <div class="col-md-12 text-center text-sm-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="employeeJob"></div>
                                        <div class="col-md-12 mb-1 text-center text-sm-left" style="font-size: 12px" id="employeeAccount"></div>
                                    </div>
                                </div>
                            </div>
                            <hr style="padding-top:1px;" class="bg-primary">
                            <div class="col-md-12">
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">AHR Number:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="ahrNo"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Filed:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="dateFiledAppr"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Filing Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="filingTypeAppr"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Overtime Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="ahrTypeAppr"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Rendered:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id ="dateRenderedAppr"></div>
                                </div>
                                <div class="form-group m-form__group row" id="shiftInfo">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="shiftAppr"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="shiftTypeAppr"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Reason:</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-12 ahr_quote pt-3 pb-1 mb-3">
                                    <blockquote>
                                    <i class="la la-quote-left"></i>
                                    <span class="mb-0" id="reasonAppr"></span>
                                    <i class="la la-quote-right"></i>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="col-md-12" id="shiftWord">
                                <div class="form-group pt-3" style="border-top: 1px dashed #ebedf2;">
                                    <label class="form-control-label font-weight-bold">Shift</label>
                                    <select class="form-control m-select2 select2" id="shiftWordUnrendered" name="shiftWord" placeholder="Select a Shift"></select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 borderLeft stack-border-left-only">
                            <div class="col-md-12">
                                <div id="renderedAhrApproval" style="border-bottom: 1px dashed #ebedf2;">
                                    <label for="recipient-name" class="form-control-label font-weight-bold">DTR DETAILS</label>
                                    <div id="renderedPreAfterApproval" class="">
                                        <table class="table table-bordered table-hover">
                                            <tbody class=" font-size-12">
                                                <tr>
                                                    <th scope="row" id="renderedDtrLogLabel"></th>
                                                    <td id="renderedDtrLogVal"></td>
                                                </tr>
                                                <tr id="renderBctAppr">
                                                    <th scope="row">BCT</th>
                                                    <td id="renderedBctVal"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="form-group pt-2"  style="border-top: 1px dashed #ebedf2;">
                                            <label for="" class="font-weight-bold" id="renderedActualDtrLabel"></label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="form-control-label">Date</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control m-input datePicker" readonly="" id="renderedActualDtrDate">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="form-control-label">Time</label>
                                                    <div class="input-group">
                                                        <input id="renderedActualDtrTime" type="text" class="form-control m-input" readonly="">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="la la-exclamation-circle"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="renderedWordApproval">
                                        <table class="table table-bordered table-hover">
                                            <tbody class=" font-size-12">
                                                <tr>
                                                    <th scope="row">CLOCK IN</th>
                                                    <td id="renderedWordInVal"></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">CLOCK OUT</th>
                                                    <td id="renderedWordOutVal"></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">TARDINESS</th>
                                                    <td id="renderedWordTardVal" class="text-danger font-weight-bold"></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">BREAK</th>
                                                    <td id="renderedWordBreakVal"> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="unrenderedAhrApproval" style="border-bottom: 1px dashed #ebedf2;">                                    
                                    <div class="form-group m-form__group row mb-0">
                                        <label for="" class="col-md-12 font-weight-bold">RENDER DETAILS</label>
                                    </div>
                                    <table class="table table-bordered table-hover" id="additionalDtrDetails">
                                        <tbody class=" font-size-12">
                                            <tr>
                                                <th scope="row" id="additionalDtrDetailsLabel"></th>
                                                <td id="additionalDtrDetailsVal"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="form-group m-form__group row">
                                        <label for="" class="col-md-12">FROM:</label>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Start Date</label>
                                                <div class="input-group date">
                                                    <input type="text" class="form-control m-input datePicker" readonly="" id="startDateAppr" style="font-size: 12px;">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Start Time</label>
                                                <div class="input-group timepicker">
                                                    <input type="text" class="form-control m-input unrenderedTimePicker" readonly="" id="startTimeAppr" style="font-size: 12px;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-exclamation-circle"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="" class="col-md-12">TO:</label>
                                        <h4 id="actualDtrDate"></h4>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="recipient-name" class="form-control-label">End Date</label>
                                                <div class="input-group date">
                                                    <input type="text" class="form-control m-input datePicker" readonly="" id="endDateAppr" style="font-size: 12px;">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="la la-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="recipient-name" class="form-control-label">End Time</label>
                                                <div class="input-group timepicker">
                                                    <input type="text" class="form-control m-input unrenderedTimePicker" readonly="" id="endTimeAppr" style="font-size: 12px;">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="la la-exclamation-circle"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group clearfix pt-2 pb-2" style="border-bottom: 1px dashed #ebedf2;">
                                    <label for="recipient-name" class="form-control-label font-weight-bold">HOURS RENDERED</label>
                                    <h3 id="renderedOt" class="font-size-39">00 hr, 00 min</h3>
                                    <span class="font-size-12 pull-right">overtime</span>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label font-weight-bold">Note</label>
                                    <textarea class="form-control m-input text-counter" id="note" name="note" rows="5"></textarea>
                                    <span class="character-remaining" style="font-size: 13px;font-weight: 400;"></span>
                                </div>
                            </div>
                        </div>               
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-info m-btn m-btn--icon" id="approveAhrBtn">
                        <span>
                            <i class="fa fa-thumbs-o-up"></i>&nbsp;Approve
                        </span>
                    </button>
                    <button type="submit" class="btn btn-outline-danger m-btn m-btn--icon" id="rejectAhrBtn">
                        <span>
                            <i class="fa fa-thumbs-o-down"></i>&nbsp;Dissapprove
                        </span>
                    </button>
                    <button type="button" class="btn btn-outline-dark m-btn m-btn--icon" data-dismiss="modal">
                        <span>
                            <i class="fa fa-close"></i>
                            <span fon>Close</span>
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="recordInfoAhrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="recordInfoAhrForm" novalidate="novalidate" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Additional Hour Request Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <div class="row">
                               <div class="col-12 col-sm-3 col-md-3 col-lg-3">
                                    <img id="empPicApproval" src="" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-12 col-sm-9 col-md-9 col-lg-9">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-sm-left" style="font-size: 19px;" id="empNameRecord"></div>
                                        <div class="col-md-12 text-center text-sm-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="empJobRecord"></div>
                                        <div class="col-md-12 mb-1 text-center text-sm-left" style="font-size: 12px" id="empAccountRecord"></div>
                                    </div>
                                </div>
                            </div>
                            <hr style="padding-top:1px;" class="bg-primary">
                            <div class="col-md-12 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">AHR Number:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="ahrNoRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Filed:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="dateFiledRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Filing Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7 text-capitalize" id="filingTypeRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Overtime Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7 .text-capitalize" id="ahrTypeRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Date Rendered:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="dateRenderedRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="shiftRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Shift Type:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="shiftTypeRecord"></div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="" class="col-6 col-sm-5 col-md-5 font-weight-bold">Status:</label>
                                    <div class="col-6 col-sm-7 col-md-7" id="approvalStatRecord"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 borderLeft stack-border-left-only">
                            <div class="col-md-12 mb-3">
                                <label>
                                    <b>REASON:</b>
                                </label>
                                <div class="col-md-12 ahr_quote pt-3 pb-1">
                                    <blockquote>
                                        <i class="fa fa-quote-left"></i>
                                        <span class="mb-0 text-capitalize" id="reasonRecord"></span>
                                        <i class="fa fa-quote-right"></i>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>
                                    <b>RENDER DETAILS:</b>
                                </label>
                                <table class="table table-bordered table-hover">
                                    <tbody class=" font-size-12">
                                        <tr>
                                            <th scope="row">START:</th>
                                            <td id="startOtRecord"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">END</th>
                                            <td id="endOtRecord"></td>
                                        </tr>
                                        <tr id="breakRecord">
                                            <th scope="row">BREAK</th>
                                            <td id="breakRecordVal"></td>
                                        </tr>
                                        <tr id="bctRecord">
                                            <th scope="row">BCT</th>
                                            <td id="bctRecordVal"></td>
                                        </tr>
                                        <tr id="tardRecord">
                                            <th scope="row">TARDINESS</th>
                                            <td>
                                                <div class="col-md-12" id="lateRecord"></div>
                                                <div class="col-md-12" id="utRecord"></div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <label>
                                    <b>COVERAGE:</b>
                                </label>
                                <div class="form-group">
                                    <h3 id="renderedOtRecord" class="font-size-39">00 hr, 00 min</h3>
                                    <span class="font-size-12 pull-right">overtime</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Close</button>
                </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="approversInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Approval Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
            <div id="approvalBody"></div>
        </div>          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <script src="<?php echo base_url();?>assets/src/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js" type="text/javascript"></script>
    <!-- <script src="<?php echo base_url();?>node_modules/moment/moment.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url();?>assets/src/custom/js/ahr_common.js"></script>
    <script src="<?php echo base_url();?>assets/src/custom/js/approval_ahr.js"></script>
    <script src="<?php echo base_url();?>assets/src/custom/js/ahr/quick_approval.js"></script>
