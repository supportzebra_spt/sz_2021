<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Additional Hour Request Checking</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__body">
                        <div class="row pt-4">
                            <div class="col-6 mb-3">
                                <label class="font-weight-bold">Sched Date</label>
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Schedule Date" id="check_date_range">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <label class="font-weight-bold">Employee</label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Type any keyword..." id="ahr_employee_search">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12" id='ahr_records' data-limit="0" style="max-height: 700px; height: 700px; position: relative; overflow-y: scroll; overflow-x: hidden;">
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/src/custom/js/ahr/ahr_checker.js"></script>