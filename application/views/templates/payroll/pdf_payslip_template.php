
<?php foreach ($employees as $key => $emp): ?>
		<?php
			$clothing = explode("(",$emp->clothing);
			$laundry = explode("(",$emp->laundry);
			$rice = explode("(",$emp->rice);
			$total = 0;
			$deduct = 0;
			$takehome = 0;
			$bir = explode("(",$emp->bir_details);
		?>
		<table style="width:100%;">
			<tr>
				<td>
					
						<table>
							<tr><td colspan=2 style="text-align:center;"><img src="assets/img/logo2.png" style="width:30%;"></td></tr>
							<tr><td colspan=2></td></tr>
							
							<tr><td colspan=2><b>PAY STATEMENT - EMPLOYEES COPY</b></td></tr>
							<tr><td><b>NAME</b></td><td><?php echo  $emp->fname." ".$emp->lname; ?></td></tr>
							<tr><td><b>POSITION</b></td><td><?php echo  $emp->pos_details; ?></td></tr>
							<tr><td><b>PERIOD COVERED:</b></td><td><?php echo  $emp->daterange; ?></td></tr>
							<tr><td><b>ATM:</b></td><td><?php echo  ($emp->isAtm==1) ? "ATM" : "NON-ATM"; ?></td></tr>
							
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><b>EARNINGS</b></td></tr>
							
							<tr><td><b>SALARY:</b>	</td><td><?php echo number_format($emp->quinsina,2,".",","); ?></td></tr>
							<tr><td><b>CLOTHING:</b>	</td><td><?php echo number_format($clothing[0],2,".",","); ?></td></tr>
							<tr><td><b>LAUNDRY:<b>	</td><td><?php echo number_format($laundry[0],2,".",","); ?></td></tr>
							<tr><td><b>RICE:	</b></td><td><?php echo number_format($rice[0],2,".",","); ?></td></tr>
							<tr><td><b>NIGHT DIFFERENTIAL:	</b></td><td><?php echo number_format($emp->nd,2,".",","); ?></td></tr>
							<tr><td><b>HAZARD PAY:	</b></td><td><?php echo number_format($emp->hp,2,".",","); ?></td></tr>
							<tr><td><b>HOLIDAY:	</b></td><td><?php echo number_format($emp->ho,2,".",","); ?></td></tr>
							
							<?php
								$total = $emp->quinsina + $clothing[0] + $laundry[0] + $rice[0] + $emp->nd + $emp->hp + $emp->ho;
								if($emp->ahr_amount>0){
							?>
								<tr><td><b>OVERTIME:</b></td><td><?php echo number_format($emp->ahr_amount,2,".",","); ?></td></tr>
							<?php
								$total+= $emp->ahr_amount;
								}
							?>
							<?php
								if(trim($emp->adjust_details)!="0"){
								 $adjusted = explode(",",$emp->adjust_details);
									for($i = 0; $i<count($adjusted); $i++){
										$ii = explode("|",$adjusted[$i]);
										$iii = explode("=",$ii[1]);

										if(trim($ii[0])!="Bonus"){
							?>
								<tr><td><b><?php echo strtoupper($iii[0]); ?><b></td><td><?php echo number_format(trim($iii[1]),2,".",","); ?></td></tr>
							<?php
											$total +=  $iii[1];

										}
									
									}
								} 
							?>
							<tr><td><b>TOTAL EARNING:	</b></td><td><?php echo number_format($total,2,".",","); ?></td></tr>
							
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><b>DEDUCTIONS</b></td></tr>
							
							<tr><td><b>WITHHOLDING TAX:	</b></td><td><?php echo number_format(trim($bir[0]),2,".",","); ?></td></tr>
							<tr><td><b>SSS PREMIUM:	</b></td><td><?php echo number_format($emp->sss_details,2,".",","); ?></td></tr>
							<tr><td><b>HDMF PREMIUM:	</b></td><td><?php echo number_format($emp->hdmf_details,2,".",","); ?></td></tr>
							<tr><td><b>PHILHEALTH:	</b></td><td><?php echo number_format($emp->phic_details,2,".",","); ?></td></tr>
							<?php
							$deduct = $bir[0] + $emp->sss_details + $emp->hdmf_details + $emp->phic_details;
							if(trim($emp->absentLate)!="0.00"){
							?>	
								<tr><td><b>ABSENT/UNDERTIME:	</b></td><td><?php echo number_format($emp->absentLate,2,".",","); ?></td></tr>
							<?php
							$deduct+=$emp->absentLate;
							}
							if(trim($emp->deduct_details)!="0"){
							 $deduct_details = explode(",",$emp->deduct_details);

								for($i = 0; $i<count($deduct_details); $i++){
										$ii = explode("=",$deduct_details[$i]);
							?>
								<tr><td><b><?php echo strtoupper(trim($ii[0])); ?></b></td><td><?php echo number_format(trim($ii[1]),2,".",","); ?></td></tr>
							<?php			 
									$deduct +=  $ii[1];
								}
							}
							
							if((trim($emp->loan_details)!=="")){
								if((trim($emp->loan_details)!=="0")){
									$loan_details = explode(",",$emp->loan_details);
									for($a = 0; $a<count($loan_details); $a++){
										$aa = explode("=",$loan_details[$a]);
							?>
								<tr><td><b><?php echo strtoupper(trim($aa[0])); ?></b></td><td><?php echo number_format(trim($aa[1]),2,".",","); ?></td></tr>
							<?php
										$deduct +=  $aa[1];
									}
									} 
								} 
							?>
							<?php
								$takehome = $total - $deduct;
							?>
							<tr><td><b>TOTAL DEDUCTION:	</b></td><td><?php echo number_format($deduct,2,".",","); ?></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td><b>TAKE HOME PAY:	</b></td><td><?php echo number_format($takehome,2,".",","); ?></td></tr>
						</table>
						
				</td>
				<td>
					<div style="padding: 20px;width:50%; border: 1px solid #d2d2d2;">
						<table class="tbl" style="">
							<tr><td colspan=2 style="text-align:center;"><img src="assets/img/logo2.png" style="width:30%;"></td></tr>
							<tr><td colspan=2></td></tr>
							
							<tr><td colspan=2><b>BONUS STATEMENT - EMPLOYEES COPY</b></td></tr>
							<tr><td><b>NAME</b></td><td><?php echo  $emp->fname." ".$emp->lname; ?></td></tr>
							<tr><td><b>POSITION</b></td><td><?php echo  $emp->pos_details; ?></td></tr>
							<tr><td><b>PERIOD COVERED:</b></td><td><?php echo  $emp->daterange; ?></td></tr>
							<tr><td><b>ATM:</b></td><td><?php echo  ($emp->isAtm==1) ? "ATM" : "NON-ATM"; ?></td></tr>
							
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							
							
							<?php
							$totalB=0;
								if(trim($emp->adjust_details)!="0"){
								 $adjusted = explode(",",$emp->adjust_details);
									for($i = 0; $i<count($adjusted); $i++){
										$ii = explode("|",$adjusted[$i]);
										$iii = explode("=",$ii[1]);

										if(trim($ii[0])=="Bonus"){
							?>
								<tr><td><b><?php echo strtoupper($iii[0]); ?></b></td><td><?php echo number_format(trim($iii[1]),2); ?></td></tr>
							<?php
											$totalB +=  $iii[1];

										}
									
									}
								} 
							?>
							<?php
							
								if(trim($emp->bonus_details)!="0"){
								 $bonus_details = explode(",",$emp->bonus_details);
									for($i = 0; $i<count($bonus_details); $i++){
										$ii = explode("=",$bonus_details[$i]);

										
							?>
								<tr><td><b><?php echo strtoupper($ii[0]); ?></b></td><td><?php echo number_format(trim($ii[1]),2); ?></td></tr>
							<?php
											$totalB +=  $ii[1];

									
									
									}
								} 
							?>
							<?php 
								if($totalB==0) {
							?>
							<tr><td colspan=2><b>NO DATA</b></td></tr>
							
							<?php
								}
							?>
							<tr><td><b>TAKE HOME PAY:	</b></td><td><?php echo number_format($totalB,2); ?></td></tr>

							
						</table>
						</div>
				</td>
			
			
			</tr>
		
		
		</table>

    <?php if ($key === COUNT($employees)): ?>

        <pagebreak />
    <?php endif; ?>
<?php endforeach; ?>
