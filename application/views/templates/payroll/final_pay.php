<link rel="stylesheet" href="<?php echo base_url("assets/css/cards-style.css"); ?>" type="text/css" /> </head>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<style>
.btn-group.buttons {
    background: #575962;
}
button.btn.moveall.btn-outline-secondary {
    margin: 0;
}
button.btn.removeall.btn-outline-secondary {
    margin: 0;
}
#tblDtrShow thead{
     background: #34bfa3;
    color: #fff;
    font-size: initial;
}
#tblDtrShow thead th {
    padding: 6px;
}
.bootstrap-duallistbox-container {
    width: 100%;
}
thead {
    background: #575962;
}
thead td {
	padding: 10px;
    color: #FFEB3B;
    font-weight: 300;
}
tbody td {
    padding: 9px;
    border: 1px solid #bdc0cc;
    color: #000;
}
tbody tr:hover {
    background: #fff7b7;
	font-weight: 500;
}
.h3, h3 {
	font-size: .98rem !important;
}
td.allowanceStyle {
    background: #c7f1c9;
    border: 1px solid #7ec181;
}
td.govDeductStyle {
    background: #a6d6fd;
    border: 1px solid #65b7f9;
}
td.homePayStyle {
    background: #ffd885;
    font-size: larger;
    font-weight: 600;
}
td.earnStyle,td.deductStyle{
	text-decoration-line: underline;
    cursor: pointer;
    text-decoration-style: dashed;
    text-decoration-color: #8c8c8c;
    text-decoration-skip-ink: none;
    color: #1f9cff;
    font-weight: 600;
}
tr.payIsZero td {
    text-decoration: line-through;
    text-decoration-color: red;
}
.pointer{
	cursor:pointer;
	text-decoration: underline;
    color: #2196F3;

}
.notmatch {
    background: #ffe38e;
}
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
  ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator"><?php echo strtoupper($site); ?> Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/emp/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > Base</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/general/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > General</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<?php
		date_default_timezone_set('Asia/Manila');
		$checker = date('Y-m-d', strtotime("+6 day", strtotime($date[1])));
			$flag = true;
		if(strtotime(date("Y-m-d")) >=strtotime($checker)){
			$flag =false;
		}
	?>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" id="mjmPilaLangISave"  data-pilalangisave="0">
			 <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <!--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Live DTR</a>
                                </li>-->
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i>Finalizing the Payroll Page</a>
                                </li>
								<!--<button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="right: 36px;position: absolute;">
                                    <i class="fa fa-filter"></i>
								</button>-->
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body pt-0" style="background: #e4e3e3;">

                <div class="tab-content">
                  
                   
                    <div class="tab-pane active" id="m_tabs_6_3" role="tabpanel">
					<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
									<select multiple="multiple" class="form-control m-input"  size="5"  name="duallistbox_record[]" id="employeeListRecord" data-emp="" >
									
									</select>
 								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
								<label></label>
									<button type="button" class="btn m-btn--pill m-btn--air btn-info btn-sm" id="btnShowLogs">Show</button>
									<?php if($flag) {?>
									
									<button type="button" class="btn m-btn--pill m-btn--air btn-success btn-sm" id="btnSavePayroll" style="display:none">Save</button>
									<?php }else{?>
										<button type="button" class="btn m-btn--pill m-btn--air btn-warning btn-sm" id="btnShowLogsHistory">Show Payslip</button>

									<?php } ?>
 								</div>
								
								
							</div>
								
						</div>
								<div style="overflow: scroll;text-align: center;border: 1px solid #bdc0cc;max-height: 1500px;">
								<div class="div_record_log_monitoring">
								</div>
								<?php $flag = 1; if($flag){ ?>
									<div class="row" id="div_record_log_monitoring" style="background: #fff;" >
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
									<?php }else{?>
									<div class="row" id="payslipRs" style="background: #fff;padding-left: 60px;padding-right: 60px;" >
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
									<?php } ?>
								</div>
                    </div>
                </div>  

               
 
            </div>
        </div>
		</div>
	</div>
<div class="modal fade" id="modalShow"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="">Add Adjustments</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <form class="m-form m-form--fit m-form--label-align-right" id="defaultForm">
		  <div class="modal-body">
				<div id="btnType"></div>
				<div class="result-output">

					
				</div>

		  </div>
		  <div class="modal-footer">
				<button type="" class="btn btn-danger m-btn" data-dismiss="modal">Close</button>
		  </div>
		  </form>
		</div>
		
	  </div>
	</div>
	<div class="modal fade" id="modalAdjShow"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-adjtype="">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="">Add Adjustments</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <form class="m-form m-form--fit m-form--label-align-right" id="defaultForm">
		  <div class="modal-body">
				
				<div class="m-portlet__body"> 
					<div class="form-group m-form__group">
						<label for="exampleSelect1"><?php // echo ($act=="add") ? "Addition" : "Deduction" ; ?> Adjustment:</label>
						<select class="form-control m-select2" id="adjustment" name="reqadjust"></select>
					</div>
					<div class="form-group m-form__group">
						<label for="exampleInputEmail1">Amount</label>
						<input type="text" class="form-control m-input" placeholder="Enter Peso value" id="reqamount" name="reqamount">
					</div>
				</div>

		  </div>
		  <div class="modal-footer">
				<span  class="btn btn-success m-btn" onClick="saveAdj()">Save</span>
				<button type="" class="btn btn-danger m-btn" data-dismiss="modal">Close</button>
		  </div>
		  </form>
		</div>
		
	  </div>
	</div>
	<div class="modal fade" id="modalDetailShow"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Add Adjustments</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <form class="m-form m-form--fit m-form--label-align-right" id="defaultForm">
		  <div class="modal-body">
		
				<div class="result-detail">

					
				</div>

		  </div>
		  <div class="modal-footer">
				<button type="" class="btn btn-danger m-btn" data-dismiss="modal">Close</button>
		  </div>
		  </form>
		</div>
		
	  </div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>
 
  function emp_list(){
		var employeeList = $("#employeeListRecord").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/loadEmployee'); ?>",
		data: {
			class_type : '<?php echo $type; ?>',
			empstat :  '<?php echo $empstat; ?>',
			site :  '<?php echo $site; ?>',
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			var str_emp = "";
			var empz= [];
				$.each(result.employee, function (x, item){
					if((item.payroll_id >0)){
					str_emp += '<option   value='+item.emp_id+' >'+item.lname+", "+item.fname+'</option>';
					}
					empz.push(item.emp_id);
				}); 
				 $("#employeeListRecord").data("emp",empz);
				 $("#employeeListRecord").html(str_emp);
				 $("#employeeListRecord").bootstrapDualListbox('refresh');
		}
	});
}

 function delAddAdjust(id){ 
 
 alert(id);
 } 
 function addAct(id,emp_id,coverage_id){ 
 	var act  = (id==1) ? "add" : "deduct";

 $.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/adjustmentOption'); ?>",
		data: {
				emp : emp_id,
				act :  act,
				coverage :  coverage_id,
			},
			cache: false,
			success: function (json) {
				var result = JSON.parse(json);
				var str_emp = "";
					str_emp = '<option disabled selected > -- </option>';

					$.each(result, function (x, item){
						str_emp += '<option   value='+item.id+' >'+item.options+'</option>';
					}); 
					 $("#adjustment").html(str_emp);
					 $("#reqamount").val(1);
					  $("#modalAdjShow").modal("show");
			}
		});

 }
  function getDetailz(txt,type){
	 
	 var data = (type==1) ? $("#"+txt).data("earn") : $("#"+txt).data("deduct");
	 var name = $("#"+txt).data("name");
	 if(type==1){
		$("#exampleModalLongTitle").html("Gross Salary Details of "+name);
	 }else{
		$("#exampleModalLongTitle").html("Deduction details of "+name);
	 }
	 $("#modalDetailShow").modal("show");
	 var total = 0;
	console.log(data);
	 var rs = data.split(",");
	 var tbl = "<table style='width: 100%;'>";
	 tbl += "<thead><tr><td>Description</td><td>Amount</td></tr></thead>";
	for(var i=0;i<rs.length;i++){
		var x = rs[i].split("=");
		if(x[1]>0){
			tbl += "<tr><td>"+x[0]+"</td><td>"+x[1]+"</td></tr>";
			total +=Number(x[1]);

		}
	}
	 tbl += "<tr style='background: #d6d6d6;font-weight: 900;font-size: initial;'><td>Total</td><td>"+total.toFixed(2)+"</td></tr>";
	 tbl += "</table>";

	 $(".result-detail").html(tbl);
 }
 
 function getEarnings(emp_id,coverage_id,payroll_id,type){  
	 
	// $("#modalShow").modal("show");
	 
	// $(".result-output").html(tbl);
	
	var link  = (type==1) ? "showAdjustAdd" : "showAdjustDeduct";
	var act  = (type==1) ? "Additional Adjustment" : "Deduction Adjustment";
	
		$.ajax({
		type: "POST",
		url: "<?php echo base_url('Payrollreport/'); ?>"+link,
		data: {
 			emp_id :  emp_id,
 			coverage_id :  coverage_id,
 			payroll_id :  payroll_id,
  		},
		cache: false,
		success: function (json) {
			// $("#btnType").html("<span class='btn btn-success' onClick=addAct('"+type+"','"+emp_id+"','"+coverage_id+"')> Add "+act+"</span>");
			var res = JSON.parse(json);
			var html = "<table style='width:100%'>";
			html += "<tr style='background: #009688;'><td>Adjustment</td><td>Amount</td></tr>";
			if(res.length>0){
				$(res).each(function( index	, val ){
				
				html += "<tr><td>"+val["adjname"]+"</td><td>"+val["value"]+"</td></tr>";
				// html += "<tr><td></td><td></td></tr>";
			});
			}else{
				html += "<tr style='text-align: center;'><td colspan=2>No Data</td></tr>";
			}
			
			html += "</table>";
		  $("#modalShow").modal("show");
		 
			 $(".result-output").html(html);

		} 
	}); 
 } 
 
 $(function(){ 
 		$('.m-select2').select2({
                placeholder: "Select your schedule.",
				width: '100%'
         });
$('#dateReq').daterangepicker({});
  
  $("#btnSavePayroll").click(function(){
			swal({
                title: 'Are you sure?',
                text: "Employees will be marked as final and payslips will be auto generated",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Save!'
            }).then(function(result) {
                if (result.value) {
					   var rowvalue = [];
					$("#tblShow  > tr").each(function(i, v) {
						if($(this).attr("class")=="notmatch"){ 
							rowvalue[i] = $('td', this).map(function() {
								if($(this).attr("id")!="notincluded"){
									return $(this).attr("id")+"---"+$(this).text();
								}
							}).get();
						}
					});
					var dataSet = JSON.stringify(rowvalue);
					var coverage_id = "<?php echo $pcover[0]->coverage_id; ?>";
					var numToSabe = $("#mjmPilaLangISave").data("pilalangisave");
					console.log(numToSabe);
				  	$.ajax({
						url: '<?php echo base_url() ?>Payrollreport/saveFinalPayroll',
						type: "POST",
						data: {
						dataArray: dataSet,
						coverage_id: coverage_id,
						site: '<?php echo $site; ?>',
						},
						success: function (result) {
							if(result==1){
								swal("Success","Payslips are already generated and available.", "success");
								$("#btnShowLogs").trigger("click");
							}else{
								swal("Opps!","Nothing was saved.", "error");
							}
						}
					});   
				}
            });
		
		

});	
  
  
   
$("#btnShowLogs").click(function(){
	console.log(<?php echo $flag; ?>);
	 
	 var emp = $("#employeeListRecord").val();
 	 $.ajax({
		type: "POST",
		url: "<?php echo base_url('Payrollreport/get_final_pay'); ?>",
		data: {
			emp : emp,
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
 			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			
			var txt="<table class='tblRs' style='margin-left: 2%;background: #fff;'>";
			txt+="<thead>";
					txt+="<tr>";
						txt+="<td rowspan=2 >Fullname</td>";
						txt+="<td rowspan=2>Quinsina</td>";
						txt+="<td rowspan=2>Total Hours</td>";
						txt+="<td colspan=3 style='color: #8BC34A;font-weight: 700;font-size: larger;'>Allowances</td>";
						txt+="<td rowspan=2>Bonuses</td>";
						txt+="<td rowspan=2>AHR</td>";
						txt+="<td rowspan=2>ND</td>";
						txt+="<td rowspan=2>HP</td>";
						txt+="<td rowspan=2>HO</td>";
						txt+="<td colspan=4 style='color: #2dc3d6;font-weight: 700;'>Government Deduction</td>";
						txt+="<td rowspan=2>Adjust<i class='la la-plus-circle' style='color: #4CAF50;'><i></td>";
						txt+="<td rowspan=2>Adjust<i class='la la-minus-circle' style='color: #ff958d;'><i></td>";
						txt+="<td rowspan=2>Earnings</td>";
						txt+="<td rowspan=2>Deduction</td>";
						txt+="<td rowspan=2>Takehome Pay</td>";

					txt+="</tr>";
					
					txt+="<tr>";
						txt+="<td>Rice</td>";
						txt+="<td>Clothing</td>";
						txt+="<td>Laundry</td>";
						txt+="<td>SSS</td>";
						txt+="<td>PHIC</td>";
						txt+="<td>HDMF</td>";
						txt+="<td>BIR</td>";
					txt+="</tr>"; 
			txt+="</thead><tbody id='tblShow'>";

			var numNoNeedSave = 0;
			$.each(result, function (x, item){ 
			var adjustPlus  = 0;
			var adjustMinus  = 0;
			var loan  = 0;
			var bonus  = 0;
			var earnings  = 0;
			var deduction  = 0;

			var txtEarn= new Array();
			var txtBonus= new Array();
			var txtAdjAdd= new Array();
			var txtAdjDeduct= new Array();
			var txtLoan= new Array();
			var txtDeduct= new Array();
			var txtAhr= new Array();
			var txtLeave= new Array();
						 
				txtEarn.push("Basic (Quinsina)="+item.quinsina);	
				txtEarn.push("Night Differential (ND)="+item.nd);	
				txtEarn.push("Hazard Pay (HP)="+item.hp);	
				txtEarn.push("Holiday Pay (HO)="+item.ho);
				txtEarn.push("Laundry  ="+item.laundry);
				txtEarn.push("Clothing  ="+item.clothing);
				txtEarn.push("Rice  ="+item.rice);
				txtEarn.push("Allowances  ="+item.bonus);
				txtEarn.push("Overtime (AHR)="+Number(item.ahr_amount));
				
				
				txtDeduct.push("SSS="+parseFloat(item.sss_details).toFixed(2));	
				txtDeduct.push("PHIC="+parseFloat(item.phic_details).toFixed(2));	
				txtDeduct.push("HDMF="+parseFloat(item.hdmf_details).toFixed(2));	
				txtDeduct.push("BIR="+ parseFloat(item.bir_details).toFixed(2));	
					txtDeduct.push("Late/Undertime/Absent ="+item.absentLate);	

						$.each(item.adjAdd, function (b, item_b){
							adjustPlus+=Number(item_b.value);
							
							txtEarn.push(item_b.description+"|"+item_b.additionname+"="+item_b.value);
							txtAdjAdd.push(item_b.description+"|"+item_b.additionname+"="+item_b.value);

						});
						$.each(item.adjMinus, function (c, item_c){
							adjustMinus += Number(item_c.value);
							// console.log(item_c.value);
							txtDeduct.push(item_c.deductionname+"="+item_c.value);
							txtAdjDeduct.push(item_c.deductionname+"="+item_c.value);

						});
						$.each(item.loan, function (f, item_f){
							loan += Number(item_f.adj_amount);
							// console.log(" LOAN "+item_f.adj_amount);
							txtDeduct.push(item_f.deductionname+"="+item_f.adj_amount);
							txtLoan.push(item_f.deductionname+"="+item_f.adj_amount);

						});
							var needSave = "";
						
						var earnings =  Number(item.quinsina) + Number(item.nd) + Number(item.hp) + Number(item.ho) + (Number(item.rice) + Number(item.clothing) + Number(item.laundry)) + Number(item.bonus) + Number(adjustPlus)+Number(item.ahr_amount);
						 earnings = parseFloat(earnings).toFixed(2);
						 
						 
						 
						var deduction = Number(item.sss_details) + Number(item.phic_details) + Number(item.hdmf_details) + Number(item.bir_details) + Number(adjustMinus) + Number(item.absentLate) + Number(loan);
							
							
							console.log(" contri_sss = "+Number(item.sss_details) +" contri_phic = "+ Number(item.phic_details) +" contri_hdmf = "+ Number(item.hdmf_details) +" contri_bir = "+ Number(item.bir_details) +" adjustMinus = "+  Number(adjustMinus) +" absentLate = "+ item.absentLate+" Loan = "+ loan) ;

							var finalpay = earnings - deduction;
							if(parseFloat(item.final).toFixed(2)!=parseFloat(finalpay).toFixed(2)){
									needSave="notmatch";
									numNoNeedSave +=1;
								}
 						txt+="<tr id='"+item.emp_id+"' data-name='"+item.lname+", "+item.fname+"' data-earn='"+txtEarn+"' data-deduct='"+txtDeduct+"' class='"+needSave+"'>";
							txt+="<td id='notincluded'>"+item.lname+", "+item.fname+"</td>";
							txt+="<td  id='quinsina'>"+item.quinsina+"</td>";
							txt+="<td  id='total_hours'>"+item.total_hours+"</td>";
			 				txt+="<td  id='rice'>"+item.rice+"</td>";
							txt+="<td  id='clothing'>"+item.clothing+"</td>";
							txt+="<td  id='laundry'>"+item.laundry+"</td>";
							txt+="<td  id='bonus'>"+item.bonus+"</td>";
							txt+="<td  id='ahr_amount'>"+item.ahr_amount+"</td>";
							txt+="<td  id='nd'>"+item.nd+"</td>";
							txt+="<td  id='hp'>"+item.hp+"</td>";
							txt+="<td  id='ho'>"+item.ho+"</td>";
							txt+="<td  id='sss_details'>"+item.sss_details+"</td>";
							txt+="<td  id='phic_details'>"+item.phic_details+"</td>";
							txt+="<td  id='hdmf_details'>"+item.hdmf_details+"</td>";
							txt+="<td  id='bir_details'>"+item.bir_details+"</td>";
							txt+="<td  id='adjust_add' class='pointer' onclick='getEarnings("+item.emp_id+","+item.coverage_id+","+item.payroll_id+",1)'>"+parseFloat(adjustPlus).toFixed(2)+"</td>";
							txt+="<td  id='adjust_minus' class='pointer' onclick='getEarnings("+item.emp_id+","+item.coverage_id+","+item.payroll_id+",0)'>"+parseFloat(adjustMinus).toFixed(2)+"</td>";
							txt+="<td  id='total_earning' class='pointer'  onclick='getDetailz("+item.emp_id+",1)' >"+parseFloat(earnings).toFixed(2)+"</td>";
							txt+="<td  id='total_deduction' class='pointer' onclick='getDetailz("+item.emp_id+",0)' >"+parseFloat(deduction).toFixed(2)+"</td>";
							txt+="<td  id='final'>"+parseFloat(finalpay).toFixed(2)+"</td>";
							txt+="<td hidden title='"+item.fullname+"' id='adjust_details'>"+((txtAdjAdd.length>0) ? txtAdjAdd : 0) +"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='deduct_details'>"+((txtAdjDeduct.length>0) ? txtAdjDeduct : 0 )+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='employer_hdmf'>"+parseFloat(item.employer_hdmf).toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='employer_phic'>"+parseFloat(item.employer_phic).toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='employer_sss'>"+parseFloat(item.employer_sss).toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_hp'>"+item.hour_hp+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_ho'>"+item.hour_ho+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_nd'>"+item.hour_nd+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='isDaily'>"+item.isDaily+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='isActive'>"+item.isActive+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hourly'>"+item.hourly+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='daily'>"+item.daily+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='monthly'>"+item.monthly+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='leave_id'>"+item.leave_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='coverage_id'>"+item.coverage_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='ahr_id'>"+item.ahr_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='ahr_hour'>"+item.ahr_hour+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='account'>"+item.account+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='absentLate_hours'>"+item.absentLate_hours+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='absentLate'>"+item.absentLate+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='ot_bct'>"+item.ot_bct+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='pagibig_id'>"+item.pagibig_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='philhealth_id'>"+item.philhealth_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='sss_id'>"+item.sss_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='tax_id'>"+item.tax_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='emp_type'>"+item.emp_type+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='bonus_details'>"+item.bonus_details+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='emp_promoteId'>"+item.emp_promoteId+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='pemp_adjust_id'>"+item.pemp_adjust_id+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='loan_details'>"+item.loan_details+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='payroll_id'>"+item.payroll_id+"</td>";

  						txt+="</tr>";
								 
					
				// txt+="<tr></tr>"; 
 
			 });
			 
			 $("#mjmPilaLangISave").data("pilalangisave",numNoNeedSave);
			txt+="</tbody></table>";
			if(emp.length>0){
				$("#div_record_log_monitoring").html(txt);
				$("#btnSavePayroll").css("display","block");
			}else{
				$("#btnSavePayroll").css("display","none");
				$("#div_record_log_monitoring").html("<h1 style='margin: 0 auto;padding: 5%;'>No Records Found</h1>");
			}
		}
	});
});

$('select[name="duallistbox_live[]"]').bootstrapDualListbox();
$('select[name="duallistbox_record[]"]').bootstrapDualListbox();
	$('#toggleFilter').on('click', function () {
		$('#filters').toggle(1000);
	});
emp_list();
 });
 </script>