<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}

</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <!--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Live DTR</a>
                                </li>-->
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i> Payroll Page</a>
                                </li>
								 <button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="margin-top: 7px;right: 50px;position: absolute;" data-toggle="modal" data-target="#myModalCoverage">
                                    <i class="fa fa-plus"></i>
								</button> 
                            </ul>
                        </div>
                    </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
					<div class="col-lg-6">
						<label class="col-form-label col-lg-3 col-sm-12">Year</label>
							<select class="form-control m-select2" id="year" name="param">
							<?php 
								for($i=2018;$i<=date("Y");$i++){
									echo "<option>".$i."</option>";
								}
							?>
							</select>
					</div>
					<div class="col-lg-6">						
						<label class="col-form-label col-lg-3 col-sm-12">Month</label>
							<select class="form-control m-select2" id="month" name="param">
								<option>All</option>
								<option>January</option>
								<option>February</option>
								<option>March</option>
								<option>April</option>
								<option>May</option>
								<option>June</option>
								<option>July</option>
								<option>August</option>
								<option>September</option>
								<option>October</option>
								<option>November</option>
								<option>December</option>
							</select>
						
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
					<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnShow">Show</button>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
						<div class="m_datatable_record"></div>
					</div>
				</div>
               
 
            </div>
        </div>
		</div>
	</div>
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-location-arrow"></i> Please choose which site</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="la la-remove"></span>
					</button>
				</div>
		  <div class="modal-body">
			  <div class="modal-site-div">
					
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
	<div id="myModalCoverage" class="modal fade" role="dialog">
	  <div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-location-arrow"></i> Payroll Period</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="la la-remove"></span>
					</button>
				</div>
		  <div class="modal-body">
			  <div class="form-group m-form__group row">
				<label class="col-form-label col-lg-4 col-sm-12">Year:</label>
				<div class="col-lg-4 col-md-9 col-sm-12">
					<input id="pcyear" type="text" class="form-control bootstrap-touchspin-vertical-btn" value="<?php echo date("Y"); ?>" name="demo1" placeholder="<?php echo date("Y"); ?>" type="text" readonly>
				</div>
			  </div>
			  <div class="form-group m-form__group row">
				<label class="col-form-label col-lg-4 col-sm-12">Month:</label>
				<div class="col-lg-7 col-md-9 col-sm-12">
					<select class="form-control m-select2" id="pcmonth"  >
								<option selected disabled>--</option>
								<option value="01">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
				</div>
			  </div>
			  <div class="form-group m-form__group row">
				<label class="col-form-label col-lg-4 col-sm-12">Period:</label>
				<div class="col-lg-7 col-md-9 col-sm-12">
					<select class="form-control m-select2" id="pcperiod">
								<option selected disabled>--</option>
								<option>1</option>
								<option>2</option>
					</select>
				</div>
			  </div>
			  <div class="form-group m-form__group row">
				<label class="col-form-label col-lg-4 col-sm-12">Coverage:</label>
				<div class="col-lg-7 col-md-9 col-sm-12">
					<input type="text" class="form-control m-input" id="pcDescription" readonly>
				</div>
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-accent" id="btnSave">Check</button>
		  </div>
		</div>

	  </div>
	</div>
 
	<script>
	function coverage(){
		var year = $("#year").val();
		var month = $("#month").val();
		
		 $('.m_datatable_record').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
						url: "<?php echo base_url(); ?>payroll/payrollcoverage/"+year+"/"+month,
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        }
                    }
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#searchRecord'),
            },
            columns: [{
                field: "Year",
                title: "Year",
                width: 100,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<td class = 'col-md-12'>" +  row.year + "</td>";
                    return html;
                }
            },{
                field: "Month",
                title: "Month",
                width: 150,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12'>" +  row.month + "</td>";
                    return html;
                }
            },{
                field: "Coverage",
                title: "Coverage",
                width: 200,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12'>" +  row.daterange + "</td>";
                    return html;
                }
            },{
                field: "Status",
                title: "Status",
                width: 100,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12' onclick='clickme()'>" +  row.status + "</td>";
                    return html;
                }
            },{
                field: "Action",
                title: "Action",
                width: 100,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                      var html = "<td class = 'col-md-12'><span class='btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air' onclick='showModal("+row.coverage_id+")'><i class='fa fa-search'></i></span></td>";
                    return html;
					// href='<?php echo base_url("/payroll/emp/"); ?>"+row.coverage_id+"'
                }
            },
			],
        });
 
	}
	function saveCoverage(pcyear,pcmonth,pcperiod,pcDescription){
	if(pcDescription!=""){
				var monthz = {
					"01":"January",
					"02":"February",
					"03":"March",
					"04":"April",
					"05":"May",
					"06":"June",
					"07":"July",
					"08":"August",
					"09":"September",
					"10":"October",
					"11":"November",
					"12":"December"	
				}
		var dataString = "pcyear="+pcyear+"&pcmonth="+monthz[pcmonth]+"&pcperiod="+pcperiod+"&pcDescription="+pcDescription;
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>/Payroll/pcInsert",
				data:  dataString,
				cache: false,
				success: function(html)
				{
				 if(html>0){
					$("#myModalCoverage").modal("hide");
					coverage();
				 }else{
					alert("NOT SAVED!");
				 }
				}
		  });
	}else{
		$("#btnSave").text("Save");
	}
	}
	function showModal(coverage_id){
 		html = '<div class="m-widget1__item">'+
					'<div class="row m-row--no-padding align-items-center">'+
						'<div class="col">'+
							'<a href="<?php echo base_url("/payroll/emp/cdo/"); ?>'+coverage_id+'"><div class="siteDiv one" style="width: 85%;">'+
							'<img src="<?php echo base_url("assets/cdo.jpg"); ?>" style="width: 100%;">'+
							'</div></a>'+
						'</div>'+
						'<div class="col m--align-right">'+
							'<a href="<?php echo base_url("/payroll/emp/cebu/"); ?>'+coverage_id+'"><div class="siteDiv one" style="width: 85%;">'+
							'<img src="<?php echo base_url("assets/cebu.jpg"); ?>" style="width: 100%;">'+
							'</div></a>'+
						'</div>'+
					'</div>'+
				'</div>';
		$(".modal-site-div").html(html);
		$("#myModal").modal();
		
	}
	function clickme(){
		alert("asasasa");
	}
	$(function(){
			$('.m-select2').select2({
					placeholder: "Select your schedule.",
					width: '100%'
			 });
		 coverage();
		 $('#pcyear').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            verticalbuttons: true,
            verticalupclass: 'la la-plus',
            verticaldownclass: 'la la-minus',
			min: -1000000000,
            max: 1000000000,
			
        });
		$("#btnShow").click(function(){
			 $('.m_datatable_record').mDatatable('destroy');
			coverage();
		});
		$("#btnSave").click(function(){
				var monthz = {
					"01":"January",
					"02":"February",
					"03":"March",
					"04":"April",
					"05":"May",
					"06":"June",
					"07":"July",
					"08":"August",
					"09":"September",
					"10":"October",
					"11":"November",
					"12":"December"	
				}
			var pcyear = $("#pcyear").val();
			var pcmonth = $("#pcmonth").val();
			var pcperiod = $("#pcperiod").val();
			var pcDescription = $("#pcDescription").val();
			var dataString = "pcyear="+pcyear+"&pcmonth="+monthz[pcmonth]+"&pcperiod="+pcperiod+"&pcDescription="+pcDescription;
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>/Payroll/pcCheck",
				data:  dataString,
				cache: false,
				success: function(html)
				{
				 
				   if(html==1){
					   alert("Already have a record!");
					   $("#pcDescription").val("");
				   }else if(html==0){
						var d = new Date(pcyear,parseInt(pcmonth),0); //this is last day of current month   
		 
					   if(pcperiod==1){
							$("#pcDescription").val(pcmonth+"/01/"+pcyear+" - "+pcmonth+"/"+15+"/"+pcyear);
					   }else{
							$("#pcDescription").val(pcmonth+"/"+16+"/"+pcyear+" - "+pcmonth+"/"+d.getDate()+"/"+pcyear);
					   }
 						
						saveCoverage(pcyear,pcmonth,pcperiod,pcDescription);
				   }else{
						alert("All Fields must be filled-out!");
						$("#pcDescription").val("");
				   }
				}
		  });
		  });
	});
 
 </script>