<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.txtUp{
	text-transform: uppercase;
}
.txtIndent{
	    text-indent: 20px;
}

</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
			
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
					<div class="col-lg-4">
						<label class="col-form-label col-lg-3 col-sm-12">Year</label>
							<select class="form-control m-select2" id="year" name="param">
								<option>2018</option>
								<option selected>2019</option>
								<option>2020</option>
							</select>
					</div>
					<div class="col-lg-4">						
						<label class="col-form-label col-lg-3 col-sm-12">Month</label>
							<select class="form-control m-select2" id="month" name="param"  onchange="coverage()">
								<option>All</option>
								<option>January</option>
								<option>February</option>
								<option>March</option>
								<option>April</option>
								<option>May</option>
								<option>June</option>
								<option>July</option>
								<option>August</option>
								<option>September</option>
								<option>October</option>
								<option>November</option>
								<option>December</option>
							</select>
						
					</div>
					<div class="col-lg-4">						
						<label class="col-form-label col-lg-3 col-sm-12">Coverage</label>
							<select class="form-control m-select2" id="coverage" name="param">
							</select>
						
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
					<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnShow">Show</button>
					</div>
				</div>
				 
				
            </div>
			
        </div>
		<div class="col-lg-12" id="payslipContainer" style='text-align: center;background: #fff;padding: 22px;font-size: x-large;' >
			<span>NO RECORDS FOUND</span>
		</div>
		 <div class="m-content">
		    <div class="row">
				<div class="col-lg-6" >
					<div id="payslip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">
					</div>
							
				</div>
				<div class="col-lg-6">
					<div  id="bonus_slip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">		
					</div>		
				</div>
			</div>
		</div>
			
		</div>
	</div>
 
 <script>
	function coverage(){
		var year = $("#year").val();
		var month = $("#month").val();
		 $.ajax({
						type: 'POST',
						url: "<?php echo base_url(); ?>payroll/payrollcoverage/"+year+"/"+month,
						success: function (json) {
							
							var result = JSON.parse(json);
							var str_emp = "";
							str_emp = '<option disabled selected > -- </option>';

							$.each(result, function (x, item){
								var date = (item.daterange).split("-");
								var date1 = moment(date[0]).format('ll');  
								var date2 = moment(date[1]).format('ll');  
								str_emp += '<option   value='+item.coverage_id+' >'+date1+ "  -  "+date2+'</option>';
									 
							}); 
							 $("#coverage").html(str_emp);
							
						}
		});
								
 
 
	}
	function showModal(coverage_id){
 		html = '<div class="m-widget1__item">'+
					'<div class="row m-row--no-padding align-items-center">'+
						'<div class="col">'+
							'<a href="<?php echo base_url("/payroll/emp/"); ?>'+coverage_id+'/cdo"><div class="siteDiv one" style="width: 85%;">'+
							'<img src="<?php echo base_url("assets/cdo.jpg"); ?>" style="width: 100%;">'+
							'</div></a>'+
						'</div>'+
						'<div class="col m--align-right">'+
							'<a href="<?php echo base_url("/payroll/emp/"); ?>'+coverage_id+'/cebu"><div class="siteDiv one" style="width: 85%;">'+
							'<img src="<?php echo base_url("assets/cebu.jpg"); ?>" style="width: 100%;">'+
							'</div></a>'+
						'</div>'+
					'</div>'+
				'</div>';
		$(".modal-site-div").html(html);
		$("#myModal").modal();
		
	}
	function clickme(){
		alert("asasasa");
	}
	$(function(){
			$('.m-select2').select2({
					placeholder: "Select your schedule.",
					width: '100%'
			 });
		 coverage();
		$("#btnShow").click(function(){
			var coverage = $("#coverage").val();
			 $.ajax({
						type: 'POST',
						url: "<?php echo base_url(); ?>payroll/employeePayslip",
						data: {
 							coverage :  coverage,
							emp_id :  '<?php echo $_SESSION["emp_id"]; ?>',
						},
						success: function (json) {
							 
							var result = JSON.parse(json);
							 var txt = "";
							 var txtB = "";
							if(result.payslip!=null){
							if(result.employee.isAtm=="1"){
								var atm = "ATM";
							}else if(result.employee.isAtm=="0"){
								var atm = "NON-ATM";
							}else{
								var atm = "HOLD";
							}
								var date = (result.payslip.daterange).split("-");
								var date1 = moment(date[0]).format('ll');  
								var date2 = moment(date[1]).format('ll');  
								var totalEarn = 0;
								var totalDeduct = 0;
								var ahr = 0;
							
								txt += '<div class="m-portlet__head">';
								txt += '<div class="m-portlet__head-caption">';
								txt += '<div class="m-portlet__head-title" style="padding: 10px;">';
								txt += '<h3 class="m-portlet__head-text text-center">';
								txt += '<img src="<?php echo base_url("assets/img/logo2.png"); ?>" style="width: 78%;">';
								txt += '</h3>';
								txt += '</div>';
								txt += '</div>';
								txt += '</div>';
								txt += '<div class="m-portlet__body" style="padding: 15px;">';
								txt += '<b>PAY STATEMENT - EMPLOYEES COPY</b><br>';
								txt += '<table style="width: 100%;">';
								txt += '<tr><td><b>NAME:</b></td><td>'+result.employee.fname+" "+result.employee.lname+'</td></tr>';
								txt += '<tr><td><b>POSITION:</b></td><td>'+result.payslip.pos_details+'</td></tr>';
								txt += '<tr><td><b>PERIOD COVERED:</b></td><td>'+date1+" to "+date2+'</td></tr>';
								txt += '<tr><td><b>ATM:</b></td><td>'+atm+'</td></tr>';
								txt += '</table>';
								txt += '<br>';
								
								txt += '<table style="width: 100%;">';
								txt += '<tr ><td  style="width: 70%;"><b><u>EARNINGS</u></b></td></tr>';
								txt += '<tr class="txtIndent"><td><b>SALARY:</b></td><td> &#8369;'+result.payslip.quinsina+'</td></tr>';
								var cloth = ((result.payslip.clothing).trim()).split("(");
								var laundry = ((result.payslip.laundry).trim()).split("(");
								var rice = ((result.payslip.rice).trim()).split("(");
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>CLOTHING:</b></td><td> &#8369;'+cloth[0]+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>LAUNDRY:</b></td><td> &#8369;'+laundry[0]+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>RICE:</b></td><td> &#8369;'+rice[0]+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>NIGHT DIFFERENTIAL:</b></td><td> &#8369;'+result.payslip.nd+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HAZARD PAY:</b></td><td> &#8369;'+result.payslip.hp+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HOLIDAY PAY:</b></td><td> &#8369;'+result.payslip.ho+'</td></tr>';
								
								if(Number(result.payslip.ahr_amount)>0){
									txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>OVERTIME ('+result.payslip.ahr_hour+' hr.):</b></td><td> &#8369;'+result.payslip.ahr_amount+'</td></tr>';
									 ahr = Number(result.payslip.ahr_amount);
								} 	
								
								totalEarn = Number(result.payslip.quinsina)+ Number(cloth[0])+ Number(laundry[0])+ Number(rice[0])+ Number(result.payslip.nd)+ Number(result.payslip.hp)+ Number(result.payslip.ho)+ ahr;
								if((result.payslip.adjust_details).trim()!=0){
									var adjAdd =  (result.payslip.adjust_details).split(",");
									for(var i=0;i<adjAdd.length;i++){
										var index = adjAdd[i].split("|");
										if(index[0].trim()!="Bonus"){
											var adjadd_details = index[1].split("=");
											txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjadd_details[0]+':</b></td><td> &#8369;'+adjadd_details[1]+'</td></tr>';
											totalEarn+=Number(adjadd_details[1]);
										}
									}
								}
								txt += '<tr class="txtIndent" style="background: #ffffcd;border-top: 1px solid;"><td  style="width: 70%;"><b>TOTAL EARNING:</b></td><td> &#8369;'+totalEarn.toFixed(2)+'</td></tr>';

								txt += '</table>';
								txt += '<br>';
								
								txt += '<table style="width: 100%;">';
								txt += '<tr ><td><b><u>DEDUCTIONS</u></b></td></tr>';
								var bir = ((result.payslip.bir_details).trim()).split("(");
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>WITHHOLDING TAX:</b></td><td> &#8369;'+bir[0]+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>SSS PREMIUM:</b></td><td> &#8369;'+result.payslip.sss_details+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HDMF PREMIUM:</b></td><td> &#8369;'+result.payslip.hdmf_details+'</td></tr>';
								txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>PHILHEALTH:</b></td><td> &#8369;'+result.payslip.phic_details+'</td></tr>';
								if(Number(result.payslip.absentLate)>0){
									txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>ABSENT/UNDERTIME ('+result.payslip.absentLate_hours+' hr.):</b></td><td> &#8369;'+result.payslip.absentLate+'</td></tr>';
								}
								totalDeduct = Number(bir[0])+ Number(result.payslip.sss_details)+ Number(result.payslip.hdmf_details)+ Number(result.payslip.phic_details)+ Number(result.payslip.absentLate);
								
								if((result.payslip.deduct_details).trim()!=0){
									var adjDedz =  (result.payslip.deduct_details).split(",");
									for(var i=0;i<adjDedz.length;i++){
										var adjDedz_details = adjDedz[i].split("=");
											txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjDedz_details[0]+':</b></td><td> &#8369;'+adjDedz_details[1]+'</td></tr>';
											totalDeduct+=Number(adjDedz_details[1]);
										
									}
								}
								txt += '<tr class="txtIndent" style="background: #eaffec;border-top: 1px solid;"><td  style="width: 70%;"><b>TOTAL DEDUCTION:</b></td><td> &#8369;'+totalDeduct.toFixed(2)+'</td></tr>';

								txt += '</table>';
								txt += '<br>';
								txt += '<table style="width: 100%;">';
								var takehome=  totalEarn-totalDeduct;
								txt += '<tr class="txtIndent" style="background: #d9fbff;border-top: 1px solid;font-weight: 600;font-size: initial;"><td style="width: 70%;"><b>TAKE HOME PAY	:</b></td><td> &#8369;'+takehome.toFixed(2)+'</td></tr>';

								txt += '</table>';
								

								txt += '</div>';
								$("#payslip").html(txt);
								
								txtB += '<div class="m-portlet__head">';
								txtB += '<div class="m-portlet__head-caption">';
								txtB += '<div class="m-portlet__head-title" style="padding: 10px;">';
								txtB += '<h3 class="m-portlet__head-text text-center">';
								txtB += '<img src="<?php echo base_url("assets/img/logo2.png"); ?>" style="width: 78%;">';
								txtB += '</h3>';
								txtB += '</div>';
								txtB += '</div>';
								txtB += '</div>';
								txtB += '<div class="m-portlet__body"  style="padding: 15px;">';
								txtB += '<b>BONUS STATEMENT - EMPLOYEES COPY</b><br>';
								txtB += '<table style="width: 100%;">';
								txtB += '<tr><td><b>NAME:</b></td><td>'+result.employee.fname+" "+result.employee.lname+'</td></tr>';
								txtB += '<tr><td><b>POSITION:</b></td><td>'+result.payslip.pos_details+'</td></tr>';
								txtB += '<tr><td><b>PERIOD COVERED:</b></td><td>'+date1+" to "+date2+'</td></tr>';
								txtB += '<tr><td><b>ATM:</b></td><td>'+atm+'</td></tr>';
								txtB += '</table>';
								txtB += '<br>';
								var totalBonus = 0;
								txtB += '<table style="width: 100%;">';
								if((result.payslip.adjust_details).trim()!=0){
									var adjAdd =  (result.payslip.adjust_details).split(",");
									for(var i=0;i<adjAdd.length;i++){
										var index = adjAdd[i].split("|");
										if(index[0].trim()=="Bonus"){
											var adjadd_details = index[1].split("=");
											txtB += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjadd_details[0]+':</b></td><td> &#8369;'+adjadd_details[1]+'</td></tr>';
											totalBonus+=Number(adjadd_details[1]);
										}
									}
								}
								if((result.payslip.bonus_details).trim()!=0){
									var adjAdd =  (result.payslip.bonus_details).split(",");
									for(var i=0;i<adjAdd.length;i++){
										
											var adjbonus_details = adjAdd[i].split("=");
											txtB += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjbonus_details[0]+':</b></td><td> &#8369;'+parseFloat(adjbonus_details[1]).toFixed(2)+'</td></tr>';
											totalBonus+=Number(adjbonus_details[1]);
										
									}
								}
								txtB += '</table>';
								txtB += '<br>';
								txtB += '<table style="width: 100%;">';
 								txtB += '<tr class="txtIndent" style="background: #d9fbff;border-top: 1px solid;font-weight: 600;font-size: initial;"><td style="width: 70%;"><b>TAKE HOME PAY	:</b></td><td> &#8369;'+totalBonus.toFixed(2)+'</td></tr>';

								txtB += '</table>';
								txtB += '</div>';
								
 								
 								$("#bonus_slip").html(txtB);
								$("#payslipContainer").hide();

							}else{
 								$("#payslipContainer").show();
								$("#payslip").html("");
								$("#bonus_slip").html("");
							}
						 
						 
							
						}
			});

		});
	});
 
 </script>