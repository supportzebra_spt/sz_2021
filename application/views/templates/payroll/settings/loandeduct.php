<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.m-widget14 {
    text-align: center;
	padding-bottom: 1.2rem;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}
.m-widget14__header {
    background: #555769;
}
.m-widget14__header {
    background: #555769;
}
h3.m-widget14__title {
    color: #fff !important;
    font-size: 1.0rem !important;
}
td {
    padding: 10px;
}
a#checkPayrollDeduct{
	cursor: pointer;
    color: #009688;
    border-bottom: 1px dashed #009688;
}
	
</style>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
 			
			
            <div class="m-portlet__body">
			<div class="row">
	 
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
							<i class="fa fa-line-chart"></i> Setting
						</h3>
					</div>
					
				</div>
				<a href='#' class='btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air' style='position:absolute;right:30px;top: 15px;' onclick='addEmpzAdjust()'><i class='fa fa-plus'></i></a>
			</div>
			<!--begin::Form-->
			<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;padding: 10px;">
 				<!---->
				
					<div class="m-portlet">
						<button id="btnCheck" hidden>Click mee!</button>
						
						<div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-5 mb-2">
                                                
                                            </div>
                                            <div class="col-md-2 mb-2">
                                                <select class="form-control m-select2" id="empStat" name="empStat" onchange="empStat()">
													<option value="all">ALL</option>
													<option value="yes">Active</option>
													<option value="no">In-Active</option>
												 </select>
                                            </div>
                                            <div class="col-md-5 mb-2">
                                                <div class="form-group m-form__group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="Search by lastname" id="ongoingSearchs">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data below.</span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="m_datatable_pending" style="width: 95%;margin: 0 auto;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
					</div>
				
		
			</div>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
	</div>
</div>
               
				
            </div>
 
		</div>
	</div>
	<div id="showDetails" class="modal fade"  role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true" data-empid="">
	  <div class="modal-dialog modal-lg" role="document">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
		   <h4 class="modal-title" id="headerTitle"></h4>
		   <div id="addBtn"></div>
 		   
		  </div>
		  <div class="modal-body">
			  <div id="addAdjustment" style="display:none;">
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label><b>Adjustment Name *</b></label>
							<div class="input-group date" >
								<select class="form-control m-select2" id="adjustname" name="adjustname"  tabindex="-1" aria-hidden="true"></select>
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-3">
							<label><b>Maximum Amount *</b></label>
							<div class="input-group">
								<input type="number" class="form-control m-input" id="ma" value="1" min=1 onkeyup="checkPayment('')" onchange="checkPayment('')" />
							</div>
						</div>
						<div class="col-lg-3">
							<label><b>Term(s) *</b></label>
							<div class="input-group">
								<input type="number" class="form-control m-input" id="mtp"  value="1"  min=1 onkeyup="checkPayment('')" onchange="checkPayment('')"/>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						
						<div class="col-lg-6">
							<label><b>Comment</b></label>
							<div class="input-group">
								<textarea class="form-control m-input" id="adjustComment" rows="2"  name="reqReason"></textarea>
							</div>
						</div>
						<div class="col-lg-3">
							<label><b>Adjustment Date *</b></label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input datePick" id="adjustDate"  placeholder="Select a Date" value="<?php echo date("Y-m-d"); ?>" readonly/>
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-3">
							<label><b>Deduction per Payroll</b></label>
							<div class="input-group date" >
								<span id="qdeduct"> P 0.00 </span>
							</div>
							<span class="m-form__help"></span>
						</div>
					</div>	
				 
						<br>
					<div class="input-group" >
						<button type="button" class="btn btn-warning" onclick="addAdjustAction(0,'')">Cancel</button> &nbsp;
						<button type="button" class="btn btn-success"	id="btnAddAdjust" onclick="addAdjustAction(1,'')">Save</button>
						<button type="button" class="btn btn-info" 		id="btnUpdate" onclick="addAdjustAction(2,'')" data-pempadjustid>Update</button>
					</div>
					 
			  </div>
			  <div id="bodyShowDetails" style="overflow: overlay;">
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
	<div id="showEmpzAdjust" class="modal fade"  role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true" data-empid="">
	  <div class="modal-dialog modal-lg" role="document">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
		   <h4 class="modal-title text-danger"><i class="fa fa-plus-circle text-success"></i> Add Automatic Deduction </h4>
		   <div id="addBtn"></div>
 		   
		  </div>
		  <div class="modal-body">
			  <div id="addAdjustment" >
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label><b>Employee *</b></label>
							<div class="input-group date" >
								<select class="form-control m-select2" id="empzAdd" name="empzAdd"  tabindex="-1"  aria-hidden="true"></select>
							</div>
							<span class="m-form__help"></span>
						</div>
						
						<div class="col-lg-3">
							<label><b>Maximum Amount *</b></label>
							<div class="input-group">
								<input type="number" class="form-control m-input" id="maAdd" value="1" min=1 onkeyup="checkPayment('Add')" onchange="checkPayment('Add')" />
							</div>
						</div>
						<div class="col-lg-3">
							<label><b>Term(s) *</b></label>
							<div class="input-group">
								<input type="number" class="form-control m-input" id="mtpAdd"  value="1"  min=1 onkeyup="checkPayment('Add')" onchange="checkPayment('Add')"/>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label><b>Adjustment Name *</b></label>
							<div class="input-group date" >
								<select class="form-control m-select2" id="adjustnameAdd" name="adjustnameAdd"   tabindex="-1" aria-hidden="true"></select>
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-3">
							<label><b>Adjustment Date *</b></label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input datePick" id="adjustDateAdd"  placeholder="Select a Date" value="<?php echo date("Y-m-d"); ?>" readonly/>
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-3">
							<label><b>Deduction per Payroll</b></label>
							<div class="input-group date" >
								<span id="qdeductAdd"> P 0.00 </span>
							</div>
							<span class="m-form__help"></span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						
						<div class="col-lg-12">
							<label><b>Comment</b></label>
							<div class="input-group">
								<textarea class="form-control m-input" id="adjustCommentAdd" rows="2"  name="reqReason"></textarea>
							</div>
						</div>
						
					</div>	
				 
						<br>
					<div class="input-group" >
						
					</div>
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-success" onclick="addAdjustAction(1,'Add')">Save</button>
		  </div>
		</div>

	  </div>
	</div>
  <script>
  
  function checkPayment(txt){
	  var ma = $("#ma"+txt).val();
	  var mtp = $("#mtp"+txt).val();
	  
	  if(ma=="" || ma==null){
		$("#ma"+txt).val(1);
	  }
	  if(mtp=="" || mtp==null){
		$("#mtp"+txt).val(1);
	  }
	  var month  = parseFloat($("#ma"+txt).val())/parseFloat($("#mtp"+txt).val());
	  var quinsina  = month;
	  $("#qdeduct"+txt).text("P "+parseFloat(quinsina).toFixed(2));
	  
  }
  function addAdjustAction(act,txt){
	  if(act==0){
			$("#addAdjustment").css("display","none");
			$("#bodyShowDetails").css("display","block");

	  }else{
 			var adjustname = $("#adjustname"+txt).val();
			var ma = $("#ma"+txt).val();
			var mtp = $("#mtp"+txt).val();
			var adjustDate = $("#adjustDate"+txt).val();
			var adjustComment = $("#adjustComment"+txt).val();
			var emp_id = (txt=='Add') ? $("#empzAdd").val() : $("#showDetails"+txt).data("empid");
			if(adjustname!=null){
				var pempadjustid  = (act==2) ? $("#btnUpdate").data("pempadjustid") : 0;
				 $.ajax({
					type: "POST",
					url: "<?php echo base_url('payroll_settings/addAdjustment/'); ?>"+act,
					data: {emp_id:emp_id,adjustname:adjustname,ma:ma,mtp:mtp,adjustDate:adjustDate,adjustComment:adjustComment,pempadjustid:pempadjustid},
					cache: false,
					success: function (output) {
						if(output>0){
							var typ = (act==2) ? "Updated" : "Added";
							toastr.success("Successfully "+typ+" Adjustment.");
							if(txt=='Add'){
								loadEmpLoan();
								$("#showEmpzAdjust").modal("hide");
							}else{
								viewLoans(emp_id);
							}
								
						}else{
							toastr.danger("ERROR! Please contact administrator.");

						}
					}
				 });  
				
				
			}else{
				alert("Fields with * are required.");
			}
			
	  }
	  
  }
  function addEmpzAdjust(){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll_settings/getListAdjustment'); ?>",
			data: {emp_id:1},
			cache: false,
			success: function (output) {
 			var rs = JSON.parse(output);
			var opt = "<option disabled selected> Please Select Adjustment </option>";
			var empz = "<option disabled selected> Please Select Employee </option>";
				$.each(rs.adjust, function (key, obj) {
 					opt += "<option value='" + obj.pdeduct_id + "' >" + obj.deductionname + "</option>";
				});
							
				$("#adjustnameAdd").html(opt);
				$("#adjustnameAdd").trigger("change");
				
				$.each(rs.emp, function (key, obj) {
 					empz += "<option value='" + obj.emp_id + "' >" + obj.lname+", "+obj.fname + "</option>";
				});
				$("#empzAdd").html(empz);
				$("#empzAdd").trigger("change");

			}
		 });  
		$("#showEmpzAdjust").modal("show");

  }
  function addAdjustment(emp_id){
			$("#btnAddAdjust").css("display","block");
			$("#btnUpdate").css("display","none");
			$("#ma").val(1);
			$("#mtp").val(1);
			$("#adjustComment").val("");
			$("#qdeduct").text("P0.00");
			
 	    $.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll_settings/getListAdjustment'); ?>",
			data: {emp_id:emp_id},
			cache: false,
			success: function (output) {
			$("#addAdjustment").css("display","block");
			$("#bodyShowDetails").css("display","none");
			var rs = JSON.parse(output);
			var opt = "<option disabled selected> Please Select Adjustment </option>";
				$.each(rs.adjust, function (key, obj) {
 					opt += "<option value='" + obj.pdeduct_id + "' >" + obj.deductionname + "</option>";
				});
			
				$("#adjustname").html(opt);
				$("#adjustname").trigger("change");
				

			}
		 });  
	  
  }
  function setStatus(pemp_adjust_id,ans,emp_id,count){
	  var answer = (ans.checked) ? 1:0;
 	  $.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll_settings/updateActiveness'); ?>",
			data: {pemp_adjust_id:pemp_adjust_id,answer:answer,count:count},
			cache: false,
			success: function (output) {
				if(output==1){
					toastr.success("Successfully updated the status.");
				}else if(output==2){
					toastr.warning("The deduction has already reached its limit.");
				}else{
					toastr.danger("ERROR! Please contact administrator.");
				}
				viewLoans(emp_id);
				loadEmpLoan();
			}
		 });
  }
  function updateEmpAdjust(pemp_adjust_id,ans,emp_id,count){
	  $.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll_settings/updateEmpAdjust'); ?>",
			data: {pemp_adjust_id:pemp_adjust_id,count:count},
			cache: false,
			success: function (output) {
				if(output==2){
					toastr.warning("Attempt to update record failed. Transaction is already running.");
				}else{
					$("#addAdjustment").css("display","block");
					$("#bodyShowDetails").css("display","none");
					var rs = JSON.parse(output);
						var opt = "<option value="+rs.pdeduct_id+" selected>"+rs.deductionname+"</option>";
						$("#adjustname").html(opt);
						$("#adjustname").trigger("change");
					$("#ma").val(rs.max_amount);
					$("#mtp").val(rs.count);
					$("#adjustDate").val(rs.adj_date);
					$("#adjustComment").val(rs.remarks);
					$("#qdeduct").text("P "+rs.adj_amount);
						$("#btnAddAdjust").css("display","none");
						$("#btnUpdate").css("display","block");
						$("#btnUpdate").data("pempadjustid",pemp_adjust_id);

 				}
			}
		 });
  }
  function deleteEmpAdjust(pemp_adjust_id,ans,emp_id,count){
	   $.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll_settings/deleteEmpAdjust'); ?>",
			data: {pemp_adjust_id:pemp_adjust_id,count:count},
			cache: false,
			success: function (output) {
				if(output==2){
					toastr.warning("Attempt to delete record failed. Transaction is already running.");
				}else{
					swal({
						title: 'Are you sure?',
						text: "The adjustment will be deleted.",
						type: 'warning',
						showCancelButton: true,
						confirmButtonText: 'Yes, Delete!'
					}).then(function(result) {
						if (result.value) {
							
							 $.ajax({
								url: '<?php echo base_url() ?>payroll_settings/deleteAdjust',
								type: "POST",
								data: {pemp_adjust_id: pemp_adjust_id},
								success: function (result) {
									if(result==1){
										swal("Success","You have successfully deleted the adjustment.", "success");
										viewLoans(emp_id);
									}else{
										swal("Error","There was an error deleting the adjustment.", "error");
									}
								}
							});  
						}
					});
				}
			}
		 });
	  
  }
  function viewLoans(emp_id){
	  		$("#addAdjustment").css("display","none");
			$("#bodyShowDetails").css("display","block");

		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll_settings/viewLoans'); ?>",
			data: {emp_id:emp_id},
			cache: false,
			success: function (output) {
				var rs = JSON.parse(output);
				$("#showDetails").data("empid",emp_id)
				$("#headerTitle").html("<i class='fa fa-minus-circle text-danger'> </i> "+rs.personal.fname+" "+rs.personal.lname+"'s Adjustment");
				$("#addBtn").html("<a href='#' class='btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air' style='position:absolute;right:30px;' onclick='addAdjustment("+emp_id+")'><i class='fa fa-plus'></i></a>");
 				var str = "<table style='width: 100%;'>";
				str += "<tr style='background: #505a6b;color: #fff;'> <td>Adjustment</td> <td>Maximum Amount</td> <td>Deduction per Payroll</td> <td>Total Months</td> <td>Active?</td> <td>Action</td> </tr>";
				if((rs.data).length>0){
					
					$.each(rs.data, function (index, value) {
						var check = (value.isActive==1) ? "checked" : "";
						str += "<tr>"; 
						str += "<td>"+value.deductionname+"</td>"; 
						str += "<td>"+value.max_amount+"</td>"; 
						str += "<td>"+value.adj_amount+"</td>"; 
						str += "<td><a id='checkPayrollDeduct' onclick='checkPayrollDeduct("+value.pemp_adjust_id+","+value.emp_id+")'>"+value.totalDeduct+"/"+value.count+"</a></td>"; 
						if(value.empIsActive=="yes"){
							str += "<td><span class='m-switch m-switch--outline m-switch--icon m-switch--success'><label><input type='checkbox' "+check+" name='' onchange='setStatus("+value.pemp_adjust_id+",this,"+emp_id+","+value.count+")'><span></span></label></span></td>"; 
							str += "<td><a href='#' class='btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air' onclick='updateEmpAdjust("+value.pemp_adjust_id+",this,"+emp_id+","+value.count+")'><i class='fa fa-pencil'></i></a> ";
							str +="<a href='#' class='btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air' onclick='deleteEmpAdjust("+value.pemp_adjust_id+",this,"+emp_id+","+value.count+")'><i class='fa fa-trash-o'></i></a></td>"; 
						}else{
							str += "<td><span class='m-switch m-switch--outline m-switch--icon m-switch--success'><label><i class='fa fa-check text-success'></i></label></span></td>"; 
							str += "<td><span class='m-switch m-switch--outline m-switch--icon m-switch--success'><label><i class='fa fa-check text-success'></i></label></span></td>"; 
 						}
						str += "</tr>"; 
					});
				}else{
				str += "<tr> <td colspan=7 style='text-align: center;' class='text-danger'> NO RECORDS FOUND</td> </tr>";

				}
				str += "</table>";
				
				$("#bodyShowDetails").html(str);	
				$("#showDetails").modal("show");
				
			}
		 });
   }
  function checkPayrollDeduct(pemp_adjust_id,emp_id){
	$.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll_settings/checkCoverage'); ?>",
			data: {pemp_adjust_id:pemp_adjust_id,emp_id:emp_id},
			cache: false,
			success: function (output) {
				var txt ="";
				var rs = JSON.parse(output);
				$.each(rs, function (index, value) {
					txt+="<p>"+(index+1)+".) "+value["daterange"]+"</p>";
				});
				 swal({
				  title: "Payroll Coverage!",
				  html: txt,
				});
				 console.log(rs);
			}
		 });
  }
  function empStat(){
	  
	  loadEmpLoan();
	  
  }
  function loadEmpLoan(){
		
 		 $('.m_datatable_pending').mDatatable('destroy');
		 var empStat = $("#empStat").val();
         var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/payroll_settings/loadEmpLoan/'+empStat,
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
						 map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    }
                },
				saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: 'dashed-table',
                scroll: false,
                footer: false,
                header: true
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#ongoingSearchs'),
            },			
            columns: [{
                field: "lname",
                title: "Fullname",
                width: 250,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(data) {
                     var html = "<div class = 'col-md-12'>" + data.fname+" "+data.lname + "</div>";
                    return html;
                }
            },{
                field: "pos_details",
                title: "Position",
                width: 300,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'>" +  data.pos_details+ " <br> "+ data.status+ "</div>";
                    return html;
                }
            },{
                field: "cnt_active",
                title: "Summary",
                width: 300,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'> <span class='text-info'> Active </span>: " +  data.cnt_active+ " <hr> <span class='text-danger'> Inactive </span> : "+ data.cnt_inactive+ "</div>";
                    return html;
                }
            },{
                field: "",
                title: "Action",
				selector: false,
                sortable: false,
				textAlign: 'center',
				overflow: 'invisible',
                width: 100,
                template: function (data) {
                     var html = '<span class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick=viewLoans('+data.emp_id+')><i class="fa fa-search" ></i></span>';
                    return html;
                }
            },
			],
        };

        $('.m_datatable_pending').mDatatable(options);
		}
	$(function(){
		loadEmpLoan();
		$("#btnCheck").click(function(){
				loadEmpLoan();
		});	
		$('.m-select2').select2({
			width: '100%'
		});	
		$('.datePick').datepicker({format: 'yyyy-mm-dd'});		
	});
  </script>