<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.txtUp{
	text-transform: uppercase;
}
.txtIndent{
	    text-indent: 20px;
}
div#payslipRs {
    background: #fff;
    border: 1px solid #a9a9a9;
    padding: 16px;
}
tr#paskoHeader td {
    border: 1px solid #868686;
    background: #4CAF50;
    color: #fff;
    padding: 8px;
}
tr#paskoHeader2 td {
    background: #f1e36e;
    color: #000;
    border: 1px solid #9E9E9E;
}
tr#paskoBody td {
    border: 1px solid #9E9E9E;
	text-align:center;
}
td.text-center.total {
    background: #fff6ab;
}
td.text-center.una {
    background: #cee9ff;
}
td.text-center.duha {
    background: #ffd1d1;
}
tbody tr:hover {
    background: #c3f58a;
}
</style>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">13th Month Pay Report</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("dashboard"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
					<li class="m-nav__separator"> > </li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("payroll/report"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Reports</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
			
            <div class="m-portlet__body">
			<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
							13th Month Pay
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit">
				 
	            <div class="form-group m-form__group row">
						<div class="col-lg-2">
							<label class="col-form-label col-lg-3 col-sm-12">Year</label>
								<select class="form-control m-select2" id="year" name="param">
									<?php 
										for($i=2018;$i<=date("Y");$i++){
											echo "<option>".$i."</option>";
										}
									?>
								</select>
						</div>
						<div class="col-lg-3">						
						<label class="col-form-label col-lg-3 col-sm-12">Type</label>
							<select class="form-control m-select2" id="empType" name="param">
									<option value="Admin" >SZ Team</option>
									<option value="Agent" selected>Ambassadors</option>
							</select>
						
						</div>
						<div class="col-lg-4">						
						<label class="col-form-label col-lg-3 col-sm-12">Category</label>
							<select class="form-control m-select2" id="category" name="param">
 									<option selected>Probationary</option>
									<option >Regular</option>

							</select>
						
						</div>
						<div class="col-lg-3">						
							<label class="col-form-label col-lg-3 col-sm-12">Site</label>
								<select class="form-control m-select2" id="site" name="param">
									<option>CDO</option>
									<option>CEBU</option>
								</select>
							
						</div>
				</div>
            
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
					<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnShow" ><i class="fa fa-file-excel-o"></i> Show</button>
					<button type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnExportExcel" hidden><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<button type="button" class="btn btn-danger m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnExportExcel2" hidden><i class="fa fa-file-excel-o"></i> Export Excel2</button>
					</div>
				</div>
				 <br>
				 <div id="paskoOutput" style="overflow: scroll;height: 100%;padding: 0px 33px 33px 33px;"></div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
 
	</div>

</div>
               
				
            </div>
			
        </div>
        <div hidden>
		<div class="col-lg-12" id="payslipContainer" style='text-align: center;background: #fff;padding: 22px;font-size: x-large;' >
			<span>NO RECORDS FOUND</span>
		</div>
		 <div class="m-content" id="payslipRs">
		    <div class="row">
				<div class="col-lg-6" >
					<div id="payslip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">
					</div>
							
				</div>
				<div class="col-lg-6">
					<div  id="bonus_slip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">		
					</div>		
				</div>
			</div>
		</div>
		</div>
			
		</div>
	</div>
<div id="showDetails" class="modal fade"  role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true" data-empid="">
	  <div class="modal-dialog modal-lg" role="document">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
		   <h4 class="modal-title" id="headerTitle"></h4>
		   <div id="addBtn"></div>
 		   
		  </div>
		  <div class="modal-body">
			 
			  <div id="bodyShowDetails" style="overflow: overlay;">
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>	
	
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>
		function currencyFormat(num) {
		  return '<span>&#8369;</span> ' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
		}
		function viewSalaries(emp_id,year,empType,category,site){
				 $.ajax({
				type: "POST",
				data: {
					year :  year,
					empType :  empType,
					category :  category,
					site :  site,
				},
				url: "<?php echo base_url(); ?>/payroll/paskoTEST/"+emp_id,
				cache: false,
				success: function(json)
				{ 
				
					
					var result = JSON.parse(json);
					var coverage = new Array();
					 
					var str_emp = "<table style='width: 100%;' id='paskoTable'>";
						str_emp+="<tr id='paskoHeader' style='text-align: center;'><td>Fullname</td><td>Year</td><td>Month</td><td>Payroll Coverage</td><td style='width:60px'>Total DTR Issue Deduction</td><td>Basic/Quinsina</td><td>TOTAL</td></td>";
					
 
					$.each(result.emp, function (x, item){
						var deduction = 0;
						var basic = 0;
						var total = 0;
						var pasko = 0;
						var ctr = 0;
						var emp_id;
						var fname = "";
						var lname = "";
						
						
						$.each(item, function (y, item2){
							ctr++;
							fname = item2.fname;
							lname = item2.lname;
						str_emp += "<tr id='paskoBody'>";
						str_emp += "<td>"+lname+" "+fname+"</td>";
						str_emp += "<td>"+item2.year+"</td>";
						str_emp += "<td>"+item2.month+"</td>";
						str_emp += "<td>"+item2.daterange+"</td>";
						str_emp += "<td>"+currencyFormat(parseFloat(item2.absentLate))+"</td>";
						str_emp += "<td>"+currencyFormat(parseFloat(item2.quinsina))+"</td>";
						str_emp += "<td>"+currencyFormat(parseFloat(item2.quinsina)-parseFloat(item2.absentLate))+"</td>";
						str_emp += "</tr>";
							basic+= parseFloat(item2.quinsina);
 							deduction+= parseFloat(item2.absentLate);
 							total+= (parseFloat(item2.quinsina)-parseFloat(item2.absentLate));
 							pasko+= (parseFloat(item2.quinsina)-parseFloat(item2.absentLate))/12;

						}); 
						str_emp += "<tr style='text-align: center; background: #ffc878; border: 1px solid #9e9e9e;'><td colspan=4></td><td>"+currencyFormat(deduction)+"</td><td>"+currencyFormat(basic)+"</td><td>"+currencyFormat(total)+"</td></tr>";
						 $("#headerTitle").html("<i class='fa fa-search'></i> "+fname+" "+lname+"'s 13th Month Pay details ");

						 str_emp += "<b>Total Months</b> : "+ctr/2+"/12<br>";
						str_emp += "<b>13th Month Pay</b> : "+currencyFormat(pasko)+"<br>";
						str_emp += "<b>1st Pay</b> : "+currencyFormat(pasko/2)+"<br>";
						str_emp += "<b>2nd Pay</b> : "+currencyFormat(pasko/2)+"<br>";
						str_emp += "<br>";
						
					}); 
						
						
					str_emp += "</table>";
					console.log(coverage);
					 $("#bodyShowDetails").html(str_emp);
					
					 $("#showDetails").modal("show");
					
				
				}
			}); 
		}
	$(function(){
		var emp_id = <?php echo $_SESSION["emp_id"]; ?>;
		
		$('.m-select2').select2({ width: '100%' });
 		$('select[name="duallistbox_record[]"]').bootstrapDualListbox({selectorMinimalHeight:250});
		$("#btnShow").click(function(){
			var year = $("#year").val();
			var empType = $("#empType").val();
			var category = $("#category").val();
			var site = $("#site").val();
			
			 $.ajax({
				type: 'POST',
				url: "<?php echo base_url(); ?>payroll/paskoTEST",
				data: {
					year :  year,
					empType :  empType,
					category :  category,
					site :  site,
				},
				success: function (json) { 
				
					var result = JSON.parse(json);
					var coverage = new Array();
					var str_emp = "<table style='width: 100%;' id='paskoTable'>";
					str_emp+="<tr id='paskoHeader' style='text-align: center;'><td>Fullname</td><td>Total Basic</td><td style='width: 118px;'>Total DTR Issue Deduction</td><td>Total</td><td>13th Month Pay</td><td>1st Pay<br> <small>(Dec. 5, "+year+")</small></td><td>2nd Pay<br> <small>(Dec. 20, "+year+")</small></td><td>View</td>";
					
						
					$.each(result.emp, function (x, item){
						var deduction = 0;
						var basic = 0;
						var total = 0;
						var pasko = 0;
						var emp_id;
						var fname = "";
						var lname = "";
						str_emp += "<tr id='paskoBody'>";
						
						$.each(item, function (y, item2){
							fname = item2.fname;
							lname = item2.lname;
 							emp_id =  item2.emp_id;
 							basic+= parseFloat(item2.quinsina);
 							deduction+= parseFloat(item2.absentLate);
 							total+= (parseFloat(item2.quinsina)-parseFloat(item2.absentLate));
 							pasko+= (parseFloat(item2.quinsina)-parseFloat(item2.absentLate))/12;
						}); 
						str_emp += "<td>"+lname+" "+fname+"</td>";
						str_emp += "<td class='text-center'>"+currencyFormat(basic) +"</td>";
						str_emp += "<td class='text-center'>"+currencyFormat(deduction) +"</td>";
						str_emp += "<td class='text-center'>"+currencyFormat(total) +"</td>";
						str_emp += "<td class='text-center total'> <b>"+currencyFormat(pasko) +"</b></td>";
						str_emp += "<td class='text-center una'>"+currencyFormat(pasko/2)+"</td>";
						str_emp += "<td class='text-center duha'>"+currencyFormat(pasko/2) +"</td>";
						str_emp += '<td class="text-center"><a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill" onclick=viewSalaries('+emp_id+','+year+',"'+empType+'","'+category+'","'+site+'")><i class="fa fa-search"></i></a></td>';
						str_emp += "</tr>";
					}); 
						
						
					str_emp += "</table>";
					console.log(coverage);
					 $("#paskoOutput").html(str_emp);
				}
			});  

		});
		$("#year").change(function(){
			
			var opt= '<option disabled selected > -- </option>';
				opt+='<option>January</option>';
				opt+='<option>February</option>';
				opt+='<option>March</option>';
				opt+='<option>April</option>';
				opt+='<option>May</option>';
				opt+='<option>June</option>';
				opt+='<option>July</option>';
				opt+='<option>August</option>';
				opt+='<option>September</option>';
				opt+='<option>October</option>';
				opt+='<option>November</option>';
				opt+='<option>December</option>';
			 $("#month").html(opt);
			 $("#month").change();			
		});
	
		 
		 $("#btnExportExcel").click(function(){
 			var site = $("#site").val();
 			var year = $("#year").val();
 			var month = $("#month").val();
			// alert(month.length);
			var str = month.join("-");
 			if(month.length>0){
			  window.open("<?php echo base_url(); ?>payroll/exportbirReport_excel/"+str+"/"+year+"/"+site,'_blank');
			}else{
				alert("Month(s) is required.")
			}
  			/* $.ajax({
				type: "POST",
				data: {month:month,year:year},
				url: "<?php echo base_url(); ?>/payroll/taxReport",
				cache: false,
				success: function(html)
				{ 
				}
			}); */
	  }); 
		 $("#btnExportExcel2").click(function(){
 			var site = $("#site").val();
 			var year = $("#year").val();
 			var month = $("#month").val();
			// alert(month.length);
			var str = month.join("-");
 			/* if(month.length>0){
			  window.open("<?php echo base_url(); ?>payroll/exportbirReport_excel/"+str+"/"+year+"/"+site,'_blank');
			}else{
				alert("Month(s) is required.")
			} */
  			 $.ajax({
				type: "POST",
				data: {month:str,year:year},
				url: "<?php echo base_url(); ?>/payroll/taxReport",
				cache: false,
				success: function(html)
				{ 
				}
			}); 
	  });
	  $("#empType").change(function(){
				var val = $(this).val();
				  var Agent = new Array("Probationary", "Regular");
				  if(emp_id==147 || emp_id==26){
					   var Admin = new Array("Trainee","Trainee-TSN","Probationary_and_Regular","Probationary_and_Regular-TSN","Confidential");
				  }else{
					  var Admin = new Array("Trainee","Probationary_and_Regular");
				  }
				 
				// $("#empzCat").html();
				var opt="";
					$.each(((val=="Admin") ? Admin : Agent),function(index,val){
						opt+="<option>"+val+"</option>";
						});
						$("#category").html(opt);
		  });
			
			
			
		
	});
 
 </script>