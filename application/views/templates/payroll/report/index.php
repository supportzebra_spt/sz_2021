<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.m-widget14 {
    text-align: center;
	padding-bottom: 1.2rem;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}
.m-widget14__header {
    background: #555769;
}
.m-widget14__header {
    background: #555769;
}
h3.m-widget14__title {
    color: #fff !important;
    font-size: 1.0rem !important;
}
</style>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
 			
			
            <div class="m-portlet__body">
			<div class="row">
	 
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
							<i class="fa fa-line-chart"></i> Special Reports
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;padding: 10px;">
 				<!---->
				
					<div class="m-portlet">
						<div class="m-portlet__body  m-portlet__body--no-padding">
							<div class="row m-row--no-padding m-row--col-separator-xl">			
											
									<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
										<div class="m-widget14">
											<div class="m-widget14__header m--margin-bottom-5">
												<h3 class="m-widget14__title">
													Bank Report              
												</h3> 
											</div>
											<a href="<?php echo base_url("payroll/bankreport"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/financial.png"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
											
										</div>
									<!--end:: Widgets/Daily Sales-->
									</div>		
									<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
										<div class="m-widget14">
											<div class="m-widget14__header m--margin-bottom-5">
												<h3 class="m-widget14__title">
													Bank Report <small class='text-success'>(new template)</small>        
												</h3> 
											</div>
											<a href="<?php echo base_url("payroll/bankreport2"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/financial.png"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
											
										</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title">
												Payroll Detailed Report (old)
											</h3>
											
										</div>
										<a href="<?php echo base_url("payroll/payrollreport"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/Payroll.png"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
										
										 
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title">
												Payroll Detailed Report v2        
											</h3>
											
										</div>
										<a href="<?php echo base_url("payrollreport/detail"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/Payroll.png"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
										
										 
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
									
								
							</div>
							<div class="row m-row--no-padding m-row--col-separator-xl">			
											
									<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title" style="font-size: 85% !important;">
												Government Mandatory Benefits Report             
											</h3> 
										</div>
										<a href="<?php echo base_url("payroll/government"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/gov_mandatory.jpg"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
										
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>		
									<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title" style="font-size: 80% !important;">
												Government Mandatory Benefits Report  v2            
											</h3> 
										</div>
										<a href="<?php echo base_url("payroll/governmentPremium"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/gov_mandatory.jpg"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
										
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-3">
								<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title">
												BIR Report              
											</h3>
										</div>
										
										 <a href="<?php echo base_url("payroll/birreport"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/bir.png"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
								
									<div class="col-xl-3">
								<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title">
												13th Month Pay              
											</h3>
										</div>
										
										 <a href="<?php echo base_url("payroll/pasko"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/bunos.jpg"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
									
								
							</div>
							<div class="row m-row--no-padding m-row--col-separator-xl">	
							<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title">
												Payslip Report             
											</h3>
											
										</div>
										<a href="<?php echo base_url("payroll/payslipreport"); ?>"><div class="siteDiv one" style="width: 100%;"><img src="<?php echo base_url("assets/img/payslips.png"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
										
										 
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
									
									<div class="col-xl-3">
													<!--begin:: Widgets/Daily Sales-->
									<div class="m-widget14">
										<div class="m-widget14__header m--margin-bottom-5">
											<h3 class="m-widget14__title">
												Payroll Summary Report              
											</h3>
										</div>
										
										 <a href="<?php echo base_url("payroll/summaryreport"); ?>"><div class="siteDiv one" style="width: 85%;"><img src="<?php echo base_url("assets/img/expense.jpg"); ?>" style="width: 190px;margin: 5% 14%;"></div></a>
									</div>
									<!--end:: Widgets/Daily Sales-->
									</div>
							</div>
						</div>
					</div>
				
				
				
				
				
				
				
				
				
				
				
				
 				<!---->
 	            
				 
			</div>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
	</div>
</div>
               
				
            </div>
			
     
		
			
		</div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>
 