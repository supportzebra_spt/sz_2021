<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.txtUp{
	text-transform: uppercase;
}
.txtIndent{
	    text-indent: 20px;
}
div#payslipRs {
    background: #fff;
    border: 1px solid #a9a9a9;
    padding: 16px;
}

</style>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">BIR</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("dashboard"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
					<li class="m-nav__separator"> > </li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("payroll/report"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Reports</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
			
            <div class="m-portlet__body">
			<div class="row">
	<div class="col-lg-12">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
							BIR Report
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit">
				 
	            <div class="form-group m-form__group row">
						<div class="col-lg-2">
							<label class="col-form-label col-lg-3 col-sm-12">Year</label>
								<select class="form-control m-select2" id="year" name="param">
									<?php 
										for($i=2018;$i<=date("Y");$i++){
											echo "<option>".$i."</option>";
										}
									?>
								</select>
						</div>
						<div class="col-lg-8">						
						<label class="col-form-label col-lg-3 col-sm-12">Month</label>
							<select class="form-control m-select2" id="month" name="param" multiple="multiple">
 								<option>January</option>
								<option>February</option>
								<option>March</option>
								<option>April</option>
								<option>May</option>
								<option>June</option>
								<option>July</option>
								<option>August</option>
								<option>September</option>
								<option>October</option>
								<option>November</option>
								<option>December</option>
							</select>
						
						</div>
						<div class="col-lg-2">						
							<label class="col-form-label col-lg-3 col-sm-12">Site</label>
								<select class="form-control m-select2" id="site" name="param">
									<option>ALL</option>
									<option>CDO</option>
									<option>CEBU</option>
								</select>
							
						</div>
				</div>
            
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
					<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnShow" hidden disabled><i class="fa fa-file-excel-o"></i> Show</button>
					<button type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnExportExcel" ><i class="fa fa-file-excel-o"></i> Export Excel</button>
					<button type="button" class="btn btn-danger m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnExportExcel2" hidden><i class="fa fa-file-excel-o"></i> Export Excel2</button>
					</div>
				</div>
				 <br>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
 
	</div>

</div>
               
				
            </div>
			
        </div>
        <div hidden>
		<div class="col-lg-12" id="payslipContainer" style='text-align: center;background: #fff;padding: 22px;font-size: x-large;' >
			<span>NO RECORDS FOUND</span>
		</div>
		 <div class="m-content" id="payslipRs">
		    <div class="row">
				<div class="col-lg-6" >
					<div id="payslip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">
					</div>
							
				</div>
				<div class="col-lg-6">
					<div  id="bonus_slip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">		
					</div>		
				</div>
			</div>
		</div>
		</div>
			
		</div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>
 
	$(function(){
		var emp_id = <?php echo $_SESSION["emp_id"]; ?>;
		
		$('.m-select2').select2({ width: '100%' });
 		$('select[name="duallistbox_record[]"]').bootstrapDualListbox({selectorMinimalHeight:250});

		$("#year").change(function(){
			
			var opt= '<option disabled selected > -- </option>';
				opt+='<option>January</option>';
				opt+='<option>February</option>';
				opt+='<option>March</option>';
				opt+='<option>April</option>';
				opt+='<option>May</option>';
				opt+='<option>June</option>';
				opt+='<option>July</option>';
				opt+='<option>August</option>';
				opt+='<option>September</option>';
				opt+='<option>October</option>';
				opt+='<option>November</option>';
				opt+='<option>December</option>';
			 $("#month").html(opt);
			 $("#month").change();			
		});
	
		 
		 $("#btnExportExcel").click(function(){
 			var site = $("#site").val();
 			var year = $("#year").val();
 			var month = $("#month").val();
			// alert(month.length);
			var str = month.join("-");
 			if(month.length>0){
			  window.open("<?php echo base_url(); ?>payroll/exportbirReport_excel/"+str+"/"+year+"/"+site,'_blank');
			}else{
				alert("Month(s) is required.")
			}
  			/* $.ajax({
				type: "POST",
				data: {month:month,year:year},
				url: "<?php echo base_url(); ?>/payroll/taxReport",
				cache: false,
				success: function(html)
				{ 
				}
			}); */
	  }); 
		 $("#btnExportExcel2").click(function(){
 			var site = $("#site").val();
 			var year = $("#year").val();
 			var month = $("#month").val();
			// alert(month.length);
			var str = month.join("-");
 			/* if(month.length>0){
			  window.open("<?php echo base_url(); ?>payroll/exportbirReport_excel/"+str+"/"+year+"/"+site,'_blank');
			}else{
				alert("Month(s) is required.")
			} */
  			 $.ajax({
				type: "POST",
				data: {month:str,year:year},
				url: "<?php echo base_url(); ?>/payroll/taxReport",
				cache: false,
				success: function(html)
				{ 
				}
			}); 
	  });
	  
		  
		$("#btnShow").click(function(){
			var coverage = $("#coverage").val();
			var emp = $("#employeeListRecord").val();
			
			/* $.ajax({
				type: 'POST',
				url: "<?php echo base_url(); ?>payroll/employeePayslip/bulk",
				data: {
					coverage :  coverage,
					emp_id :  emp,
				},
				success: function (json) { 
				}
			});  */ 

		});
	});
 
 </script>