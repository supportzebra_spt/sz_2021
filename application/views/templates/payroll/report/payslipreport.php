<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.txtUp{
	text-transform: uppercase;
}
.txtIndent{
	    text-indent: 20px;
}
div#payslipRs {
    background: #fff;
    border: 1px solid #a9a9a9;
    padding: 16px;
}

</style>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
			
            <div class="m-portlet__body">
			<div class="row">
	<div class="col-lg-4">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
							Coverage
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit">
				 
	            <div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label class="col-form-label col-lg-3 col-sm-12">Year</label>
								<select class="form-control m-select2" id="year" name="param">
									<?php 
										for($i=2018;$i<=date("Y");$i++){
											echo "<option>".$i."</option>";
										}
									?>
								</select>
						</div>
				</div>
                <div class="form-group m-form__group row">
				<div class="col-lg-12">						
						<label class="col-form-label col-lg-3 col-sm-12">Month</label>
							<select class="form-control m-select2" id="month" name="param">
								<option>All</option>
								<option>January</option>
								<option>February</option>
								<option>March</option>
								<option>April</option>
								<option>May</option>
								<option>June</option>
								<option>July</option>
								<option>August</option>
								<option>September</option>
								<option>October</option>
								<option>November</option>
								<option>December</option>
							</select>
						
					</div>
				</div>
                <div class="form-group m-form__group row">
					<div class="col-lg-12">						
						<label class="col-form-label col-lg-3 col-sm-12">Coverage</label>
							<select class="form-control m-select2" id="coverage" name="param">
							</select>
						
					</div>
				</div>
				
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
					<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnShow" disabled><i class="fa fa-file-excel-o"></i> Show</button>
					<button type="button" class="btn btn-danger m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnExportPDF" disabled><i class="fa fa-file-excel-o"></i> PDF</button>
					<button type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnExportExcel" disabled><i class="fa fa-file-excel-o"></i> Excel</button>
					</div>
				</div>
				 <br>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
 
	</div>
	<div class="col-lg-8">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
							Employee
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;padding: 10px;">
 				<select multiple="multiple" class="form-control m-input"  size="5"  name="duallistbox_record[]" id="employeeListRecord" ></select>
 	            
				 
			</div>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
	</div>
</div>
               
				
            </div>
			
        </div>
		<div class="col-lg-12" id="payslipContainer" style='text-align: center;background: #fff;padding: 22px;font-size: x-large;' >
			<span>NO RECORDS FOUND</span>
		</div>
		 <div class="m-content" id="payslipRs">
		    <div class="row">
				<div class="col-lg-6" >
					<div id="payslip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">
					</div>
							
				</div>
				<div class="col-lg-6">
					<div  id="bonus_slip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">		
					</div>		
				</div>
			</div>
		</div>
			
		</div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>
	function coverage(){
		
		var year = $("#year").val();
		var month = $("#month").val();
		 $.ajax({
						type: 'POST',
						url: "<?php echo base_url(); ?>payroll/payrollcoverage/"+year+"/"+month,
						success: function (json) {
							
							var result = JSON.parse(json);
							var str_emp = "";
							str_emp = '<option disabled selected > -- </option>';

							$.each(result, function (x, item){
								var date = (item.daterange).split("-");
								var date1 = moment(date[0]).format('ll');  
								var date2 = moment(date[1]).format('ll');  
								str_emp += '<option   value='+item.coverage_id+' >'+date1+ "  -  "+date2+'</option>';
									 
							}); 
							 $("#coverage").html(str_emp);
							$("#btnShow").attr("disabled",true);
							$("#btnExportPDF").attr("disabled",true);
							$("#btnExportExcel").attr("disabled",true);
						}
		});
								
 
 
	}
	$(function(){
		$('.m-select2').select2({ width: '100%' });
		coverage();
		$('select[name="duallistbox_record[]"]').bootstrapDualListbox({selectorMinimalHeight:250});

		$("#year").change(function(){
			var opt= '<option disabled selected > -- </option>';
				opt+='<option>January</option>';
				opt+='<option>February</option>';
				opt+='<option>March</option>';
				opt+='<option>April</option>';
				opt+='<option>May</option>';
				opt+='<option>June</option>';
				opt+='<option>July</option>';
				opt+='<option>August</option>';
				opt+='<option>September</option>';
				opt+='<option>October</option>';
				opt+='<option>November</option>';
				opt+='<option>December</option>';
			 $("#month").html(opt);
			 $("#month").change();			
		});
		$("#month").change(function(){
			coverage();	
		
		});
		$("#coverage").change(function(){
			$("#btnShow").attr("disabled",false);
			$("#btnExportPDF").attr("disabled",false);
			$("#btnExportExcel").attr("disabled",false);

			$.ajax({
				type: 'POST',
				url: "<?php echo base_url(); ?>payroll/loadPayrollEmployees",
				data: {
					coverage :  $(this).val(),
				},
				success: function (json) { 						 
					var result = JSON.parse(json);
					var str_emp = "";
					$("#employeeListRecord").html("");
					$.each(result, function (x, item){
						str_emp += '<option   value='+item.emp_id+'>'+item.lname+", "+item.fname+'</option>';
							 
					}); 
					 $("#employeeListRecord").html(str_emp);
					 $("#employeeListRecord").bootstrapDualListbox('refresh');
				}
			});


		});
		$("#btnExportPDF").click(function(){
		var emp_id = $("#employeeListRecord").val();
		var coverage = $("#coverage").val();
		var emp_id2 = emp_id.toString().replace(/,/g,"_");
			if(emp_id!=""){
				// window.location =  "<?php echo base_url(); ?>payroll/downloadPayslipPDF/"+coverage+"/"+emp_id2;
				window.open("<?php echo base_url(); ?>payroll/downloadPayslipPDF/"+coverage+"/"+emp_id2,'_blank');
			}else{
				alert("empty");
			}

		});
		$("#btnExportExcel").click(function(){
		var emp_id = $("#employeeListRecord").val();
		var coverage = $("#coverage").val();
		var emp_id2 = emp_id.toString().replace(/,/g,"_");
			if(emp_id!=""){
				window.location =  "<?php echo base_url(); ?>payroll/downloadPayslipExcel/"+coverage+"/"+emp_id2;
			}else{
				alert("empty");
			}

		});
		$("#btnShow").click(function(){
			var coverage = $("#coverage").val();
			var emp = $("#employeeListRecord").val();
			
			   $.ajax({
				type: 'POST',
				url: "<?php echo base_url(); ?>payroll/employeePayslip/bulk",
				data: {
					coverage :  coverage,
					emp_id :  emp,
				},
				success: function (json) { 						 
					
							 
							var result = JSON.parse(json);
							 var txt = "";
							 var txtB = "";
							if(result.payslip!=null){
								$.each(result.payslip, function (x, item){
									if(item.isAtm=="1"){
										var atm = "ATM";
									}else if(item.isAtm=="0"){
										var atm = "NON-ATM";
									}else{
										var atm = "HOLD";
									}
										var date = (item.daterange).split("-");
										var date1 = moment(date[0]).format('ll');  
										var date2 = moment(date[1]).format('ll');  
										var totalEarn = 0;
										var totalDeduct = 0;
										var ahr = 0;
									
		
										txt += '<div class="row" style="border-bottom: 2px solid black;padding-bottom: 38px;">';
										txt += '<div class="col-lg-6" ><div id="payslip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">';
										txt += '<div class="m-portlet__head">';
										txt += '<div class="m-portlet__head-caption">';
										txt += '<div class="m-portlet__head-title" style="padding: 10px;">';
										txt += '<h3 class="m-portlet__head-text text-center">';
										txt += '<img src="<?php echo base_url("assets/img/logo2.png"); ?>" style="width: 55%;">';
										txt += '</h3>';
										txt += '</div>';
										txt += '</div>';
										txt += '</div>';
										txt += '<div class="m-portlet__body" style="padding: 15px;">';
										txt += '<b>PAY STATEMENT - EMPLOYEES COPY</b><br>';
										txt += '<table style="width: 100%;">';
										txt += '<tr><td><b>NAME:</b></td><td>'+item.fname+" "+item.lname+'</td></tr>';
										txt += '<tr><td><b>POSITION:</b></td><td>'+item.pos_details+'</td></tr>';
										txt += '<tr><td><b>PERIOD COVERED:</b></td><td>'+date1+" to "+date2+'</td></tr>';
										txt += '<tr><td><b>ATM:</b></td><td>'+atm+'</td></tr>';
										txt += '</table>';
										txt += '<br>';
										
										txt += '<table style="width: 100%;">';
										txt += '<tr ><td  style="width: 70%;"><b><u>EARNINGS</u></b></td></tr>';
										txt += '<tr class="txtIndent"><td><b>SALARY:</b></td><td> &#8369;'+item.quinsina+'</td></tr>';
										var cloth = ((item.clothing).trim()).split("(");
										var laundry = ((item.laundry).trim()).split("(");
										var rice = ((item.rice).trim()).split("(");
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>CLOTHING:</b></td><td> &#8369;'+cloth[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>LAUNDRY:</b></td><td> &#8369;'+laundry[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>RICE:</b></td><td> &#8369;'+rice[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>NIGHT DIFFERENTIAL:</b></td><td> &#8369;'+item.nd+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HAZARD PAY:</b></td><td> &#8369;'+item.hp+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HOLIDAY PAY:</b></td><td> &#8369;'+item.ho+'</td></tr>';
										
										if(Number(item.ahr_amount)>0){
											txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>OVERTIME ('+item.ahr_hour+' hr.):</b></td><td> &#8369;'+item.ahr_amount+'</td></tr>';
											 ahr = Number(item.ahr_amount);
										} 	
										
										totalEarn = Number(item.quinsina)+ Number(cloth[0])+ Number(laundry[0])+ Number(rice[0])+ Number(item.nd)+ Number(item.hp)+ Number(item.ho)+ ahr;
										if((item.adjust_details).trim()!=0){
											var adjAdd =  (item.adjust_details).split(",");
											for(var i=0;i<adjAdd.length;i++){
												var index = adjAdd[i].split("|");
												if(index[0].trim()!="Bonus"){
													var adjadd_details = index[1].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjadd_details[0]+':</b></td><td> &#8369;'+adjadd_details[1]+'</td></tr>';
													totalEarn+=Number(adjadd_details[1]);
												}
											}
										}
										txt += '<tr class="txtIndent" style="background: #ffffcd;border-top: 1px solid;"><td  style="width: 70%;"><b>TOTAL EARNING:</b></td><td> &#8369;'+totalEarn.toFixed(2)+'</td></tr>';

										txt += '</table>';
										txt += '<br>';
										
										txt += '<table style="width: 100%;">';
										txt += '<tr ><td><b><u>DEDUCTIONS</u></b></td></tr>';
										var bir = ((item.bir_details).trim()).split("(");
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>WITHHOLDING TAX:</b></td><td> &#8369;'+bir[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>SSS PREMIUM:</b></td><td> &#8369;'+item.sss_details+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HDMF PREMIUM:</b></td><td> &#8369;'+item.hdmf_details+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>PHILHEALTH:</b></td><td> &#8369;'+item.phic_details+'</td></tr>';
										if(Number(item.absentLate)>0){
											txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>ABSENT/UNDERTIME ('+item.absentLate_hours+' hr.):</b></td><td> &#8369;'+item.absentLate+'</td></tr>';
										}
										totalDeduct = Number(bir[0])+ Number(item.sss_details)+ Number(item.hdmf_details)+ Number(item.phic_details)+ Number(item.absentLate);
										
										if((item.deduct_details).trim()!=0){
											var adjDedz =  (item.deduct_details).split(",");
											for(var i=0;i<adjDedz.length;i++){
												var adjDedz_details = adjDedz[i].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjDedz_details[0]+':</b></td><td> &#8369;'+adjDedz_details[1]+'</td></tr>';
													totalDeduct+=Number(adjDedz_details[1]);
												
											}
										}
										if((item.loan_details).trim()!=0 || (item.loan_details).trim()!="0"){
											var adjLoan =  (item.loan_details).split(",");
											for(var i=0;i<adjLoan.length;i++){
												
													var adjLoan_details = adjLoan[i].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjLoan_details[0]+':</b></td><td> &#8369;'+parseFloat(adjLoan_details[1]).toFixed(2)+'</td></tr>';
													totalDeduct+=Number(adjLoan_details[1]);
												
											}
										}
										txt += '<tr class="txtIndent" style="background: #eaffec;border-top: 1px solid;"><td  style="width: 70%;"><b>TOTAL DEDUCTION:</b></td><td> &#8369;'+totalDeduct.toFixed(2)+'</td></tr>';

										txt += '</table>';
										txt += '<br>';
										txt += '<table style="width: 100%;">';
										var takehome=  totalEarn-totalDeduct;
										txt += '<tr class="txtIndent" style="background: #d9fbff;border-top: 1px solid;font-weight: 600;font-size: initial;"><td style="width: 70%;"><b>Total Net :</b></td><td> &#8369;'+takehome.toFixed(2)+'</td></tr>';

										txt += '</table>';
										

										txt += '</div></div></div>';

		
		
										 txt += '<div class="col-lg-6"><div  id="bonus_slip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;"><div class="m-portlet__head">';
										txt += '<div class="m-portlet__head-caption">';
										txt += '<div class="m-portlet__head-title" style="padding: 10px;">';
										txt += '<h3 class="m-portlet__head-text text-center">';
										txt += '<img src="<?php echo base_url("assets/img/logo2.png"); ?>" style="width: 55%;">';
										txt += '</h3>';
										txt += '</div>';
										txt += '</div>';
										txt += '</div>';
										txt += '<div class="m-portlet__body"  style="padding: 15px;">';
										txt += '<b>BONUS STATEMENT - EMPLOYEES COPY</b><br>';
										txt += '<table style="width: 100%;">';
										txt += '<tr><td><b>NAME:</b></td><td>'+item.fname+" "+item.lname+'</td></tr>';
										txt += '<tr><td><b>POSITION:</b></td><td>'+item.pos_details+'</td></tr>';
										txt += '<tr><td><b>PERIOD COVERED:</b></td><td>'+date1+" to "+date2+'</td></tr>';
										txt += '<tr><td><b>ATM:</b></td><td>'+atm+'</td></tr>';
										txt += '</table>';
										txt += '<br>';
										var totalBonus = 0;
										txt += '<table style="width: 100%;">';
										if((item.adjust_details).trim()!=0){
											var adjAdd =  (item.adjust_details).split(",");
											for(var i=0;i<adjAdd.length;i++){
												var index = adjAdd[i].split("|");
												if(index[0].trim()=="Bonus"){
													var adjadd_details = index[1].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjadd_details[0]+':</b></td><td> &#8369;'+adjadd_details[1]+'</td></tr>';
													totalBonus+=Number(adjadd_details[1]);
												}
											}
										}
										if((item.bonus_details).trim()!=0){
											var adjAdd =  (item.bonus_details).split(",");
											for(var i=0;i<adjAdd.length;i++){
												
													var adjbonus_details = adjAdd[i].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjbonus_details[0]+':</b></td><td> &#8369;'+parseFloat(adjbonus_details[1]).toFixed(2)+'</td></tr>';
													totalBonus+=Number(adjbonus_details[1]);
												
											}
										}
										txt += '</table>';
										txt += '<br>';
										txt += '<table style="width: 100%;">';
										txt += '<tr class="txtIndent" style="background: #d9fbff;border-top: 1px solid;font-weight: 600;font-size: initial;"><td style="width: 70%;"><b>Total Bonus	:</b></td><td> &#8369;'+totalBonus.toFixed(2)+'</td></tr>';

										txt += '</table>';
										
										txt += '</div></div></div><br>';
										var totalFinalReceive = totalBonus + takehome;
										txt += '<div style="font-size: large;margin-top: 32px;margin-bottom: -17px;padding-left: 20px;background: #ffd969;width: 100%;font-weight: bold;margin-left: 14px;margin-right: 12px;">TakeHome Pay: <u>&#8369;'+totalFinalReceive.toFixed(2)+'</u></div>';
										txt += '</div><br><br>';
										
										totalFinalReceive=0;
									});
										$("#payslipRs").html(txt);
										$("#payslipRs").css("display","block");

										$("#payslipContainer").hide();
										// $("#bonus_slip").html(txtB);
										// $("#bonus_slip").css("display","block");

							}else{
 								$("#payslipContainer").show();
								$("#payslipRs").html("");
								// $("#bonus_slip").html("");
								// $("#bonus_slip").css("display","none");
								$("#payslipRs").css("display","none");
							}
						 
				}
			});  

		});
	});
 
 </script>