<link rel="stylesheet" href="<?php echo base_url("assets/css/cards-style.css"); ?>" type="text/css" /> </head>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<style>
.btn-group.buttons {
    background: #575962;
}
button.btn.moveall.btn-outline-secondary {
    margin: 0;
}
button.btn.removeall.btn-outline-secondary {
    margin: 0;
}
#tblDtrShow thead{
     background: #34bfa3;
    color: #fff;
    font-size: initial;
}
#tblDtrShow thead th {
    padding: 6px;
}
.bootstrap-duallistbox-container {
    width: 100%;
}
thead {
    background: #575962;
}
thead td {
	padding: 10px;
    color: #FFEB3B;
    font-weight: 300;
}
tbody td {
    padding: 9px;
    border: 1px solid #bdc0cc;
    color: #000;
}
tbody tr:hover {
    background: #fff7b7;
	font-weight: 500;
}
.h3, h3 {
	font-size: .98rem !important;
}
td.allowanceStyle {
    background: #c7f1c9;
    border: 1px solid #7ec181;
}
td.govDeductStyle {
    background: #a6d6fd;
    border: 1px solid #65b7f9;
}
td.homePayStyle {
    background: #ffd885;
    font-size: larger;
    font-weight: 600;
}
td.earnStyle,td.deductStyle{
	text-decoration-line: underline;
    cursor: pointer;
    text-decoration-style: dashed;
    text-decoration-color: #8c8c8c;
    text-decoration-skip-ink: none;
    color: #1f9cff;
    font-weight: 600;
}
tr.payIsZero td {
    text-decoration: line-through;
    text-decoration-color: red;
}
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
  ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator"><?php echo strtoupper($site); ?> Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/emp/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > Base</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/general/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > General</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<?php
		date_default_timezone_set('Asia/Manila');
		$checker = date('Y-m-d', strtotime("+6 day", strtotime($date[1])));
			$flag = true;
		if(strtotime(date("Y-m-d")) >=strtotime($checker)){
			$flag =false;
		}
	?>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			 <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <!--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Live DTR</a>
                                </li>-->
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i>Payroll Page</a>
                                </li>
								<a href="<?php echo  base_url("/payroll/finalpay/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link"><button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="right: 60px;position: absolute;">
                                 Review
								</button>
						</a>
						
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body pt-0" style="background: #e4e3e3;">

                <div class="tab-content">
                  
                   
                    <div class="tab-pane active" id="m_tabs_6_3" role="tabpanel">
					<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
									<select multiple="multiple" class="form-control m-input"  size="5"  name="duallistbox_record[]" id="employeeListRecord" data-emp="">
									
									</select>
 								</div>
								<div class="form-group m-form__group row" style="background: #e4e3e3;">
								<label></label>
										
									<?php if($flag) {?>
									<button type="button" class="btn m-btn--pill m-btn--air btn-info btn-sm" id="btnShowLogs">Show</button>
									<button type="button" class="btn m-btn--pill m-btn--air btn-success btn-sm" id="btnSavePayroll" style="display:none">Save</button>
									<?php }else{?>
										<button type="button" class="btn m-btn--pill m-btn--air btn-warning btn-sm" id="btnShowLogsHistory">Show Payslip</button>

									<?php }?>
									<?php if($_SESSION["emp_id"]==26) {?>
									<button type="button" class="btn m-btn--pill m-btn--air btn-primary btn-sm" id="btnLoanSync"  style="display:none">Loan</button>
									<button type="button" class="btn m-btn--pill m-btn--air btn-info btn-sm" id="btnShowLogs">Show Past</button>
										<?php }?>
 								</div>
								
								
							</div>
								
						</div>
								<div style="overflow: scroll;text-align: center;border: 1px solid #bdc0cc;max-height: 1500px;">
								<div class="div_record_log_monitoring">
								</div>
								<?php if($flag){ ?>
									<div class="row" id="div_record_log_monitoring" style="background: #fff;" >
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
									<?php }else{?>
									<div class="row" id="payslipRs" style="background: #fff;padding-left: 60px;padding-right: 60px;" >
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
							<?php if($_SESSION["emp_id"]==26) {?>
									<div class="row" id="div_record_log_monitoring" style="background: #fff;" >
										 <span style='font-size: x-large;margin: 0 auto;'><i class='socicon-stackoverflow'></i> No Records Found</span>
									</div>
										<?php }?>
									<?php } ?>
								</div>
                    </div>
                </div>  

               
 
            </div>
        </div>
		</div>
	</div>
<div class="modal fade" id="modalShow"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Add Adjustments</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <form class="m-form m-form--fit m-form--label-align-right" id="defaultForm">
		  <div class="modal-body">
		
				<div class="result-output">

					
				</div>

		  </div>
		  <div class="modal-footer">
				<button type="" class="btn btn-danger m-btn" data-dismiss="modal">Close</button>
		  </div>
		  </form>
		</div>
		
	  </div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>

 <script>
 
  function emp_list(){
		var employeeList = $("#employeeListRecord").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/loadEmployee'); ?>",
		data: {
			class_type : '<?php echo $type; ?>',
			empstat :  '<?php echo $empstat; ?>',
			site :  '<?php echo $site; ?>',
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			var str_emp = "";
			var empz= [];
				$.each(result.employee, function (x, item){
					var selected = (item.payroll_id >0) ? "selected=selected": "" ;
					str_emp += '<option   value='+item.emp_id+' '+selected+'>'+item.lname+", "+item.fname+'</option>';
					empz.push(item.emp_id);
				}); 
				 $("#employeeListRecord").data("emp",empz);
				 $("#employeeListRecord").html(str_emp);
				 $("#employeeListRecord").bootstrapDualListbox('refresh');
		}
	});
}
 
 function getEarnings(txt,type){
	 
	 var data = (type==1) ? $("#"+txt).data("earn") : $("#"+txt).data("deduct");
	 var name = $("#"+txt).data("name");
	 if(type==1){
		$("#exampleModalLongTitle").html("Gross Salary Details of "+name);
	 }else{
		$("#exampleModalLongTitle").html("Deduction details of "+name);
	 }
	 $("#modalShow").modal("show");
	 var total = 0;

	 var rs = data.split(",");
	 var tbl = "<table style='width: 100%;'>";
	 tbl += "<thead><tr><td>Description</td><td>Amount</td></tr></thead>";
	for(var i=0;i<rs.length;i++){
		var x = rs[i].split("=");
		if(x[1]>0){
			tbl += "<tr><td>"+x[0]+"</td><td>"+x[1]+"</td></tr>";
			total +=Number(x[1]);

		}
	}
	 tbl += "<tr style='background: #d6d6d6;font-weight: 900;font-size: initial;'><td>Total</td><td>"+total.toFixed(2)+"</td></tr>";
	 tbl += "</table>";

	 $(".result-output").html(tbl);
 }
 
 $(function(){ 
 		$('.m-select2').select2({
                placeholder: "Select your schedule.",
				width: '100%'
         });
$('#dateReq').daterangepicker({});
  
  
  
  
  $("#btnLoanSync").click(function(){
  		toastr.success("Syncing Automatic Deductions...");

	    var emp = $("#employeeListRecord").data("emp");
	    var emp_selected = $("#employeeListRecord").val();

	  $.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll_settings/syncLoans'); ?>",
		data: {
			emp : emp,
			emp_selected : emp_selected,
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		success: function (json) {
		
		}
	});
  });
  $("#btnSavePayroll").click(function(){
			swal({
                title: 'Are you sure?',
                text: "Employees will be marked as final and payslips will be auto generated",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Save!'
            }).then(function(result) {
                if (result.value) {
					   var rowvalue = [];
					$("#tblShow  > tr").each(function(i, v) {
						rowvalue[i] = $('td', this).map(function() {
						if($(this).attr("id")!="notincluded"){
							return $(this).attr("id")+"---"+$(this).text();
						}
						}).get()

					});
					var dataSet = JSON.stringify(rowvalue);
					var coverage_id = "<?php echo $pcover[0]->coverage_id; ?>";
					$.ajax({
						url: '<?php echo base_url() ?>payroll/savePayroll',
						type: "POST",
						data: {
						dataArray: dataSet,
						coverage_id: coverage_id,
						site: '<?php echo $site; ?>',
						},
						success: function (result) {
							if(result==1){
								swal("Success","Payslips are already generated and available.", "success");
							}else{
								swal("Error","There was an error saving payroll.", "error");
							}
						}
					});  
				}
            });
		
		

});	
  
  
  
  
  
  
  
 function loadLoanFunc(){
	 toastr.success("Syncing Automatic Deductions...");

	    var emp = $("#employeeListRecord").data("emp");
	    var emp_selected = $("#employeeListRecord").val();

	  $.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll_settings/syncLoans'); ?>",
		data: {
			emp : emp,
			emp_selected : emp_selected,
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		success: function (json) {
		
		}
	});
 }
  
  
$("#btnShowLogsHistory").click(function(){
	var emp = $("#employeeListRecord").val();
	var coverage =  '<?php echo $pcover[0]->coverage_id; ?>';
		// $.ajax({
			// url: '<?php echo base_url() ?>Payrollreport/payrollhistory',
			// type: "POST",
			// data: {
			// emp: emp,
			// coverage_id: coverage,
			// },
			// success: function (result) {
				
			// }
		// }); 
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url(); ?>payroll/employeePayslip/bulk",
				data: {
					coverage :  coverage,
					emp_id :  emp,
				},
				success: function (json) { 						 
					
							 
							var result = JSON.parse(json);
							 var txt = "";
							 var txtB = "";
							if(result.payslip!=null){
								$.each(result.payslip, function (x, item){
									if(item.isAtm=="1"){
										var atm = "ATM";
									}else if(item.isAtm=="0"){
										var atm = "NON-ATM";
									}else{
										var atm = "HOLD";
									}
										var date = (item.daterange).split("-");
										var date1 = moment(date[0]).format('ll');  
										var date2 = moment(date[1]).format('ll');  
										var totalEarn = 0;
										var totalDeduct = 0;
										var ahr = 0;
									
		
										txt += '<div class="row" style="border-bottom: 2px solid black;padding-bottom: 38px;">';
										txt += '<div class="col-lg-6" ><div id="payslip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;">';
										txt += '<div class="m-portlet__head">';
										txt += '<div class="m-portlet__head-caption">';
										txt += '<div class="m-portlet__head-title" style="padding: 10px;">';
										txt += '<h3 class="m-portlet__head-text text-center">';
										txt += '<img src="<?php echo base_url("assets/img/logo2.png"); ?>" style="width: 55%;">';
										txt += '</h3>';
										txt += '</div>';
										txt += '</div>';
										txt += '</div>';
										txt += '<div class="m-portlet__body" style="padding: 15px;">';
										txt += '<b>PAY STATEMENT - EMPLOYEES COPY</b><br>';
										txt += '<table style="width: 100%;text-align: left;">';
										txt += '<tr><td><b>NAME:</b></td><td>'+item.fname+" "+item.lname+'</td></tr>';
										txt += '<tr><td><b>POSITION:</b></td><td>'+item.pos_details+'</td></tr>';
										txt += '<tr><td><b>PERIOD COVERED:</b></td><td>'+date1+" to "+date2+'</td></tr>';
										txt += '<tr><td><b>ATM:</b></td><td>'+atm+'</td></tr>';
										txt += '</table>';
										txt += '<br>';
										
										txt += '<table style="width: 100%;text-align: left;">';
										txt += '<tr ><td  style="width: 70%;"><b><u>EARNINGS</u></b></td><td  style="width: 70%;"><b>AMOUNT</b></td></tr>';
										txt += '<tr class="txtIndent"><td><b>SALARY:</b></td><td> &#8369;'+item.quinsina+'</td></tr>';
										var cloth = ((item.clothing).trim()).split("(");
										var laundry = ((item.laundry).trim()).split("(");
										var rice = ((item.rice).trim()).split("(");
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>CLOTHING:</b></td><td> &#8369;'+cloth[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>LAUNDRY:</b></td><td> &#8369;'+laundry[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>RICE:</b></td><td> &#8369;'+rice[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>NIGHT DIFFERENTIAL:</b></td><td> &#8369;'+item.nd+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HAZARD PAY:</b></td><td> &#8369;'+item.hp+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HOLIDAY PAY:</b></td><td> &#8369;'+item.ho+'</td></tr>';
										
										if(Number(item.ahr_amount)>0){
											txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>OVERTIME ('+item.ahr_hour+' hr.):</b></td><td> &#8369;'+item.ahr_amount+'</td></tr>';
											 ahr = Number(item.ahr_amount);
										} 	
										
										totalEarn = Number(item.quinsina)+ Number(cloth[0])+ Number(laundry[0])+ Number(rice[0])+ Number(item.nd)+ Number(item.hp)+ Number(item.ho)+ ahr;
										if((item.adjust_details).trim()!=0){
											var adjAdd =  (item.adjust_details).split(",");
											for(var i=0;i<adjAdd.length;i++){
												var index = adjAdd[i].split("|");
												if(index[0].trim()!="Bonus"){
													var adjadd_details = index[1].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjadd_details[0]+':</b></td><td> &#8369;'+adjadd_details[1]+'</td></tr>';
													totalEarn+=Number(adjadd_details[1]);
												}
											}
										}
										txt += '<tr class="txtIndent" style="background: #ffffcd;border-top: 1px solid;"><td  style="width: 70%;"><b>TOTAL EARNING:</b></td><td> &#8369;'+totalEarn.toFixed(2)+'</td></tr>';

										txt += '</table>';
										txt += '<br>';
										
										txt += '<table style="width: 100%;text-align: left;">';
										txt += '<tr ><td><b><u>DEDUCTIONS</u></b></td><td><b>AMOUNT</b></td></tr>';
										var bir = ((item.bir_details).trim()).split("(");
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>WITHHOLDING TAX:</b></td><td> &#8369;'+bir[0]+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>SSS PREMIUM:</b></td><td> &#8369;'+item.sss_details+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>HDMF PREMIUM:</b></td><td> &#8369;'+item.hdmf_details+'</td></tr>';
										txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>PHILHEALTH:</b></td><td> &#8369;'+item.phic_details+'</td></tr>';
										if(Number(item.absentLate)>0){
											txt += '<tr class="txtIndent"><td  style="width: 70%;"><b>ABSENT/UNDERTIME ('+item.absentLate_hours+' hr.):</b></td><td> &#8369;'+item.absentLate+'</td></tr>';
										}
										totalDeduct = Number(bir[0])+ Number(item.sss_details)+ Number(item.hdmf_details)+ Number(item.phic_details)+ Number(item.absentLate);
										
										if((item.deduct_details).trim()!=0){
											var adjDedz =  (item.deduct_details).split(",");
											for(var i=0;i<adjDedz.length;i++){
												var adjDedz_details = adjDedz[i].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjDedz_details[0]+':</b></td><td> &#8369;'+adjDedz_details[1]+'</td></tr>';
													totalDeduct+=Number(adjDedz_details[1]);
												
											}
										}
										if((item.loan_details).trim()!=0 || (item.loan_details).trim()!="0"){
											var adjLoan =  (item.loan_details).split(",");
											for(var i=0;i<adjLoan.length;i++){
												
													var adjLoan_details = adjLoan[i].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjLoan_details[0]+':</b></td><td> &#8369;'+parseFloat(adjLoan_details[1]).toFixed(2)+'</td></tr>';
													totalDeduct+=Number(adjLoan_details[1]);
												
											}
										}
										txt += '<tr class="txtIndent" style="background: #eaffec;border-top: 1px solid;"><td  style="width: 70%;"><b>TOTAL DEDUCTION:</b></td><td> &#8369;'+totalDeduct.toFixed(2)+'</td></tr>';

										txt += '</table>';
										txt += '<br>';
										txt += '<table style="width: 100%;text-align: left;">';
										var takehome=  totalEarn-totalDeduct;
										txt += '<tr class="txtIndent" style="background: #d9fbff;border-top: 1px solid;font-weight: 600;font-size: initial;"><td style="width: 70%;"><b>Total Net :</b></td><td> &#8369;'+takehome.toFixed(2)+'</td></tr>';

										txt += '</table>';
										

										txt += '</div></div></div>';

		
		
										 txt += '<div class="col-lg-6"><div  id="bonus_slip" style="border: 1px solid #d2d2d2;background-image: url(<?php echo base_url("assets/watermark2.png"); ?>);background-size: cover;"><div class="m-portlet__head">';
										txt += '<div class="m-portlet__head-caption">';
										txt += '<div class="m-portlet__head-title" style="padding: 10px;">';
										txt += '<h3 class="m-portlet__head-text text-center">';
										txt += '<img src="<?php echo base_url("assets/img/logo2.png"); ?>" style="width: 55%;">';
										txt += '</h3>';
										txt += '</div>';
										txt += '</div>';
										txt += '</div>';
										txt += '<div class="m-portlet__body"  style="padding: 15px;">';
										txt += '<b>BONUS STATEMENT - EMPLOYEES COPY</b><br>';
										txt += '<table style="width: 100%;text-align: left;">';
										txt += '<tr><td><b>NAME:</b></td><td>'+item.fname+" "+item.lname+'</td></tr>';
										txt += '<tr><td><b>POSITION:</b></td><td>'+item.pos_details+'</td></tr>';
										txt += '<tr><td><b>PERIOD COVERED:</b></td><td>'+date1+" to "+date2+'</td></tr>';
										txt += '<tr><td><b>ATM:</b></td><td>'+atm+'</td></tr>';
										txt += '</table>';
										txt += '<br>';
										var totalBonus = 0;
										txt += '<table style="width: 100%;text-align: left;">';
										if((item.adjust_details).trim()!=0){
											var adjAdd =  (item.adjust_details).split(",");
											for(var i=0;i<adjAdd.length;i++){
												var index = adjAdd[i].split("|");
												if(index[0].trim()=="Bonus"){
													var adjadd_details = index[1].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjadd_details[0]+':</b></td><td> &#8369;'+adjadd_details[1]+'</td></tr>';
													totalBonus+=Number(adjadd_details[1]);
												}
											}
										}
										if((item.bonus_details).trim()!=0){
											var adjAdd =  (item.bonus_details).split(",");
											for(var i=0;i<adjAdd.length;i++){
												
													var adjbonus_details = adjAdd[i].split("=");
													txt += '<tr class="txtUp txtIndent"><td  style="width: 70%;"><b>'+adjbonus_details[0]+':</b></td><td> &#8369;'+parseFloat(adjbonus_details[1]).toFixed(2)+'</td></tr>';
													totalBonus+=Number(adjbonus_details[1]);
												
											}
										}
										txt += '</table>';
										txt += '<br>';
										txt += '<table style="width: 100%;text-align: left;">';
										txt += '<tr class="txtIndent" style="background: #d9fbff;border-top: 1px solid;font-weight: 600;font-size: initial;"><td style="width: 70%;"><b>Total Bonus	:</b></td><td> &#8369;'+totalBonus.toFixed(2)+'</td></tr>';

										txt += '</table>';
										
										txt += '</div></div></div><br>';
										var totalFinalReceive = totalBonus + takehome;
										txt += '<div style="font-size: large;margin-top: 32px;margin-bottom: -17px;padding-left: 20px;background: #ffd969;width: 100%;font-weight: bold;margin-left: 14px;margin-right: 12px;text-align: left;">TakeHome Pay of '+item.fname+' '+item.lname+': <u>&#8369;'+totalFinalReceive.toFixed(2)+'</u></div>';
										txt += '</div><br><br>';
										
										totalFinalReceive=0;
									});
										$("#payslipRs").html(txt);
										$("#payslipRs").css("display","block");

										$("#payslipContainer").hide();
										// $("#bonus_slip").html(txtB);
										// $("#bonus_slip").css("display","block");

							}else{
 								$("#payslipContainer").show();
								$("#payslipRs").html("");
								// $("#bonus_slip").html("");
								// $("#bonus_slip").css("display","none");
								$("#payslipRs").css("display","none");
							}
						 
				}
			});
});
$("#btnShowLogs").click(function(){
	console.log(<?php echo $flag; ?>);
	
		// $("#btnLoanSync").trigger("click");
 <?php if($flag && $_SESSION["emp_id"]!="26") {?>
			loadLoanFunc();
		<?php } ?>
	 var emp = $("#employeeListRecord").val();
 	 $.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/payroll_calculate'); ?>",
		data: {
			emp : emp,
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
			coverage_text :  '<?php echo $pcover[0]->daterange; ?>',
			empstat :  '<?php echo $empstat; ?>',
			class_type : '<?php echo $type; ?>',
			site :  '<?php echo $site; ?>',

 		},
		cache: false,
		beforeSend:function(data){
			$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
		},
		success: function (json) {
 			$("#div_record_log_monitoring").html("");
			var result = JSON.parse(json);
			
			var txt="<table class='tblRs' style='margin-left: 2%;background: #fff;'>";
			txt+="<thead>";
					txt+="<tr>";
						txt+="<td rowspan=2 >Fullname</td>";
						txt+="<td rowspan=2>Monthly</td>";
						txt+="<td rowspan=2>Quinsina</td>";
						txt+="<td rowspan=2>Daily</td>";
						txt+="<td rowspan=2>Hourly</td>";
						txt+="<td rowspan=2>Total Hours</td>";
						txt+="<td colspan=3 style='color: #8BC34A;font-weight: 700;font-size: larger;'>Allowances</td>";
						txt+="<td rowspan=2>Bonuses</td>";
						txt+="<td rowspan=2>AHR</td>";
						txt+="<td rowspan=2>ND</td>";
						txt+="<td rowspan=2>HP</td>";
						txt+="<td rowspan=2>HO</td>";
						txt+="<td colspan=4 style='color: #2dc3d6;font-weight: 700;'>Government Deduction</td>";
						txt+="<td rowspan=2>Adjust<i class='la la-plus-circle' style='color: #4CAF50;'><i></td>";
						txt+="<td rowspan=2>Adjust<i class='la la-minus-circle' style='color: #ff958d;'><i></td>";
						txt+="<td rowspan=2>Earnings</td>";
						txt+="<td rowspan=2>Deduction</td>";
						txt+="<td rowspan=2>Takehome Pay</td>";

					txt+="</tr>";
					
					txt+="<tr>";
						txt+="<td>Rice</td>";
						txt+="<td>Clothing</td>";
						txt+="<td>Laundry</td>";
						txt+="<td>SSS</td>";
						txt+="<td>PHIC</td>";
						txt+="<td>HDMF</td>";
						txt+="<td>BIR</td>";
					txt+="</tr>";
			txt+="</thead><tbody id='tblShow'>";

			
			$.each(result.payroll, function (x, item){
			var adjustPlus  = 0;
			var adjustMinus  = 0;
			var loan  = 0;
			var bonus  = 0;
			var earnings  = 0;
			var deduction  = 0;
			var ahr_cost  = 0;
			var rice  = 0;
			var laundry  = 0;
			var clothing  = 0;
			var monthly = parseFloat(item.Rate_monthly).toFixed(2);
			var quinsina = parseFloat(item.Rate_quinsina).toFixed(2);
			var daily = parseFloat(item.Rate_daily).toFixed(2);
			var hourly = parseFloat(item.Rate_hourly).toFixed(2);
			var ho =  parseFloat(hourly*item.holidaywork).toFixed(2);
			var nd = (item.night_diff_cost).toFixed(2);
			var hp = (item.hazard_pay_cost).toFixed(2);
			
			var txtEarn= new Array();
			var txtBonus= new Array();
			var txtAdjAdd= new Array();
			var txtAdjDeduct= new Array();
			var txtLoan= new Array();
			var txtDeduct= new Array();
			var txtAhr= new Array();
			var txtLeave= new Array();
			
				txtEarn.push("Basic (Quinsina)="+quinsina);	
				txtEarn.push("Night Differential (ND)="+nd);	
				txtEarn.push("Hazard Pay (HP)="+hp);	
				txtEarn.push("Holiday Pay (HO)="+ho);
				
				txtDeduct.push("SSS="+parseFloat(item.contri_sss).toFixed(2));	
				txtDeduct.push("PHIC="+parseFloat(item.contri_phic).toFixed(2));	
				txtDeduct.push("HDMF="+parseFloat(item.contri_hdmf).toFixed(2));	
				txtDeduct.push("BIR="+ parseFloat(item.contri_bir).toFixed(2));	
				
				if(item.logs_issue>0){
					txtDeduct.push("Late/Undertime/Absent ["+parseFloat(item.logs_issue).toFixed(2)+" hour(s)]="+parseFloat(Number(item.logs_issue*Number(hourly))).toFixed(2));	
				} 
				$.each(item.allowances, function (a, item_a){
					
					if(item_a.allowance_name=="Rice"){
						rice = item_a.value/2;
						txtEarn.push(item_a.allowance_name+"="+item_a.value/2);
					}
					if(item_a.allowance_name=="Clothing"){
						clothing = item_a.value/2; 
						txtEarn.push(item_a.allowance_name+"="+item_a.value/2);

					}
					if(item_a.allowance_name=="Laundry"){
						laundry = item_a.value/2;
						txtEarn.push(item_a.allowance_name+"="+item_a.value/2);
					}
				});
				$.each(item.adjAdd, function (b, item_b){
					adjustPlus+=Number(item_b.value);
					
					txtEarn.push(item_b.description+"|"+item_b.additionname+"="+item_b.value);
					txtAdjAdd.push(item_b.description+"|"+item_b.additionname+"="+item_b.value);

				});
				$.each(item.adjMinus, function (c, item_c){
					adjustMinus += Number(item_c.value);
					// console.log(item_c.value);
					txtDeduct.push(item_c.deductionname+"="+item_c.value);
					txtAdjDeduct.push(item_c.deductionname+"="+item_c.value);

				});
				
					$.each(item.loan, function (f, item_f){
						loan += Number(item_f.adj_amount);
						// console.log(" LOAN "+item_f.adj_amount);
						txtDeduct.push(item_f.deductionname+"="+item_f.adj_amount);
						txtLoan.push(item_f.deductionname+"="+item_f.adj_amount);

					});
				 
				$.each(item.bonus, function (d, item_d){
					bonus += Number(item_d.amount)/2;
					txtEarn.push(item_d.bonus_name+"="+Number(item_d.amount)/2);
					txtBonus.push(item_d.bonus_name+"="+Number(item_d.amount)/2);

				});
				for(var i=0;i<(item.ahr).length;i++){
					txtAhr.push(item.ahr[i]);
				}
				$.each(item.leave, function (e, item_e){
					txtLeave.push(item_e.requestLeaveDetails_ID);
				});bonus
			 
				console.log(item.fullname+" -> "+item.ahr_val+"*"+hourly);
				var ahr_amount = (item.ahr_val*hourly).toFixed(2);
				console.log(ahr_amount);
				var ahr_amount_hour = (item.ahr_val).toFixed(2);
				txtEarn.push("Overtime (AHR)="+Number(ahr_amount));
				var absentLate = Number(item.logs_issue*Number(hourly));
				var absentLate_hours = Number(item.logs_issue);
				var earnings =  Number(quinsina) + Number(nd) + Number(hp) + Number(ho) + (Number(rice) + Number(clothing) + Number(laundry)) + Number(bonus) + Number(adjustPlus)+Number(ahr_amount);
					var earnings = parseFloat(earnings).toFixed(2);
					
				var deduction = Number(item.contri_sss) + Number(item.contri_phic) + Number(item.contri_hdmf) + Number(item.contri_bir) + Number(adjustMinus) + absentLate + Number(loan);
			  console.log(" contri_sss = "+Number(item.contri_sss) +" contri_phic = "+ Number(item.contri_phic) +" contri_hdmf = "+ Number(item.contri_hdmf) +" contri_bir = "+ Number(item.contri_bir) +" adjustMinus = "+  Number(adjustMinus) +" absentLate = "+ absentLate) +" Loan = "+ loan;
					 deduction = parseFloat(deduction).toFixed(2);
  				var finalpay = earnings - deduction;
					 finalpay = (finalpay>0 && (item.total_shift_work!=0 || item.total_shift_work_OB !=0)) ? parseFloat(finalpay).toFixed(2) : "0.00";

			
						var ifZero = (finalpay=="0.00") ? "class='payIsZero'": "";
 						txt+="<tr id='"+item.emp_id+"' data-name='"+item.fullname+"' data-earn='"+txtEarn+"' data-deduct='"+txtDeduct+"' "+ifZero+" ><td id='notincluded'>"+item.fullname+"</td>";
 						txt+="<td title='"+item.fullname+"' id='monthly'>"+monthly+"</td>";
 						txt+="<td title='"+item.fullname+"' id='quinsina'>"+quinsina+"</td>";
 						txt+="<td title='"+item.fullname+"' id='daily'>"+daily+"</td>";
 						txt+="<td title='"+item.fullname+"' id='hourly'>"+hourly+"</td>";
 						txt+="<td title='"+item.fullname+"' id='total_hours'>"+item.total_shift_work+"</td>";
 						txt+="<td class='allowanceStyle' title='"+item.fullname+"' id='rice'>"+rice+"</td>";
 						txt+="<td class='allowanceStyle' title='"+item.fullname+"' id='clothing'>"+clothing+"</td>";
 						txt+="<td class='allowanceStyle' title='"+item.fullname+"' id='laundry'>"+laundry+"</td>";
 						txt+="<td title='"+item.fullname+"' id='bonus'>"+bonus+"</td>";
 						txt+="<td title='"+item.fullname+"' id='ahr_amount'>"+ahr_amount+"</td>";
 						txt+="<td title='"+item.fullname+"' id='nd'>"+nd+"</td>";
 						txt+="<td title='"+item.fullname+"' id='hp'>"+hp+"</td>";
 						txt+="<td title='"+item.fullname+"' id='ho'>"+ho+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='sss_details'>"+parseFloat(item.contri_sss).toFixed(2)+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='phic_details'>"+parseFloat(item.contri_phic).toFixed(2)+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='hdmf_details'>"+parseFloat(item.contri_hdmf).toFixed(2)+"</td>";
 						txt+="<td class='govDeductStyle' title='"+item.fullname+"' id='bir_details'>"+parseFloat(item.contri_bir).toFixed(2)+"</td>";
 						txt+="<td title='"+item.fullname+"' id='adjust_add'>"+parseFloat(adjustPlus).toFixed(2)+"</td>";
 						txt+="<td title='"+item.fullname+"' id='adjust_minus'>"+parseFloat(adjustMinus).toFixed(2)+"</td>";
 						txt+="<td class='earnStyle' onclick='getEarnings("+item.emp_id+",1)'  title='"+item.fullname+"' id='total_earning'>"+earnings+"</td>";
 						txt+="<td class='deductStyle' onclick='getEarnings("+item.emp_id+",0)'  title='"+item.fullname+"' id='total_deduction'>"+deduction+"</td>";
  						txt+="<td class='homePayStyle'  title='"+item.fullname+"' id='final'>"+finalpay+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='emp_type'>"+item.emp_type+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='emp_promoteid'>"+item.emp_promoteid+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='account'>"+item.account+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='adjust_details'>"+((txtAdjAdd.length>0) ? txtAdjAdd : 0) +"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='deduct_details'>"+((txtAdjDeduct.length>0) ? txtAdjDeduct : 0 )+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='absentLate'>"+absentLate.toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='bonus_details'>"+((txtBonus.length>0) ? txtBonus : 0 )+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='isDaily'>"+item.isDaily+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_hp'>"+item.hour_hp+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_ho'>"+item.hour_ho+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='hour_nd'>"+item.hour_nd+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='ahr_id'>"+txtAhr+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='leave_id'>"+txtLeave+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='absentLate_hours'>"+absentLate_hours.toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='ahr_hour'>"+ahr_amount_hour+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='employer_hdmf'>"+parseFloat(item.employer_hdmf).toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='employer_phic'>"+parseFloat(item.employer_phic).toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='employer_sss'>"+parseFloat(item.employer_sss).toFixed(2)+"</td>";
  						txt+="<td hidden title='"+item.fullname+"' id='loan_details'>"+((txtLoan.length>0) ? txtLoan : 0 )+"</td>";

						txt+="</tr>";
								 
					
				// txt+="<tr></tr>"; 
 
			 });
			txt+="</tbody></table>";
			if(emp.length>0){
				$("#div_record_log_monitoring").html(txt);
				$("#btnSavePayroll").css("display","block");
			}else{
				$("#btnSavePayroll").css("display","none");
				$("#div_record_log_monitoring").html("<h1 style='margin: 0 auto;padding: 5%;'>No Records Found</h1>");
			}
		}
	});
});

$('select[name="duallistbox_live[]"]').bootstrapDualListbox();
$('select[name="duallistbox_record[]"]').bootstrapDualListbox();
	$('#toggleFilter').on('click', function () {
		$('#filters').toggle(1000);
	});
emp_list();
 });
 </script>