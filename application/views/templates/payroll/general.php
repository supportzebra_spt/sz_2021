<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.h3, h3 {
	font-size: .98rem !important;
}
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
 ?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator"><?php echo strtoupper($site); ?> Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/emp/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > Base </span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
	 <div class="row">
				<div class="col-xl-4">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											(1) Attendance Review
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/attendance/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>">
							<div class="m-portlet__body" >
								<div class="m-widget4__info siteDiv one" style="text-align:center"  >
									<img src="<?php echo base_url()."assets/img/attendance.png"?>" style="width: 130px;"> 
								</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-4">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											(2) Adjustment (+)
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/adjustment/add/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>">
							<div class="m-portlet__body">
									<div class="m-widget4__info siteDiv one" style="text-align:center">
										<img src="<?php echo base_url()."assets/img/adj-add.png"?>" style="width: 130px;">
									</div>
							</div>
							</a>
					</div>
				</div>
				<div class="col-xl-4">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											(3) Adjustment (-)
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/adjustment/deduct/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>">
							<div class="m-portlet__body">
										<div class="m-widget4__info siteDiv one" style="text-align:center">
											<img src="<?php echo base_url()."assets/img/adj-minus.png"?>" style="width: 130px;">
										</div>
							</div>
							</a>
					</div>
				</div>
				
				
         </div>
         <div class="row" >
		 <div class="col-xl-2">
		 </div>
		 <div class="col-xl-4">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height " style="height: 100% !important;">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											(4) Payroll Calculator
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/calculator/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>">
							<div class="m-portlet__body" style="padding: 0;">
										<div class="m-widget4__info siteDiv one" style="text-align:center">
											<img src="<?php echo base_url()."assets/img/payroll.png"?>" style="width: 130px;">
										</div>
							</div>
							</a>
					</div>
		 </div>
		 <div class="col-xl-4">
						<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height " style="height: 100% !important;">
							<div style="background: #4e4e4e;text-align: center;padding: 5px;">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text" style="color:#fff !important;">
											(5) Final
										</h3>
									</div>
								</div>
								
							</div>
							<a href="<?php echo base_url("/payroll/finalpay/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>">
							<div class="m-portlet__body" style="padding: 0;">
										<div class="m-widget4__info siteDiv one" style="text-align:center">
											<img src="<?php echo base_url()."assets/img/payroll-final.jpg"?>" style="width: 170px;">
										</div>
							</div>
							</a>
					</div>
				</div> 
		 <div class="col-xl-2">
		 </div>
         </div>
		 
	</div>
	</div>
 <script>
 
	$(function(){
	
	});
 
 </script>