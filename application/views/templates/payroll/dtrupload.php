<style>
.bootstrap-duallistbox-container {
    width: 100%;
}
thead {
    background: #c5c5c5;
}
thead td {
	padding: 10px;
    color: #FFEB3B;
    font-weight: 300;
}
tbody td {
    padding: 9px;
    border: 1px solid #bdc0cc;
    color: #000;
}
tbody tr:hover {
    background: #fff7b7;
}
tbody tr {
    background: #fff;
}
.bootstrap-datetimepicker-widget.dropdown-menu.bottom , .bootstrap-datetimepicker-widget.dropdown-menu.top {
    width: 70% !important;
}
span.select2-dropdown.select2-dropdown--below {
    text-transform: capitalize;
}
</style>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">DTR Logs Upload</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <!--<li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> Live DTR</a>
                                </li>-->
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i> Flexible Schedule & Logs </a>
                                </li>
								<!--<button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="right: 36px;position: absolute;">
                                    <i class="fa fa-filter"></i>
								</button>-->
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body pt-0">
			<br>
                <div class="form-group m-form__group row">
					<div class="col-lg-4">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Date Range</b></label>
							<input type="text" class="form-control" id="m_daterangepicker_1" readonly placeholder="Select time" type="text" style="color: #00c5dc;background: #fff;    font-weight: 700;text-align: center;">

							<br>
							<button type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnUpload"><i class="fa fa-upload"></i> Upload</button>
							<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" onclick="showAddLogModal()"><i class="fa fa-plus"></i> Add Logs</button> 
							<button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnShow"><i class="fa fa-search"></i> Show</button>

					</div>
					<br>
					<div class="col-lg-8">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Employees</b></label>
						<select multiple="multiple" class="form-control m-input"  size="5"  name="duallistbox_record[]" id="employeeListRecord" style="text-transform: capitalize;"></select>
					</div>
				</div>
			
				 
				
            </div>
			
        </div>
		<div class="col-lg-12" id="tempContainer" style='text-align: center;background: #fff;padding: 22px;font-size: x-large;' >
			<span>NO RECORDS FOUND</span>
		</div>
		 <div class="m-content">
		    <div class="row">
				<div class="col-lg-12" >
					<div id="result_output">
					</div>
							
				</div>
			
			</div>
		</div>
			
		</div>
	</div>
	  
<div class="modal fade" id="modalShow"  role="dialog" aria-labelledby="exampleModalCenterTitle"  data-backdrop="static" data-keyboard="false" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Upload</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <div class="m-form m-form--fit m-form--label-align-right">
		  <div class="modal-body">
		<div class="tab-pane " id="m_tabs_6_3" role="tabpanel">
						<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
								<button type="button" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnDownload"><i class="fa fa-download"></i> Template</button>
								<button type="button" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnFileUpload"><i class="fa fa-upload"></i> Upload File</button>
								<button type="button" class="btn btn-warning m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" style="padding: 0.70rem 1rem;display:none" id="btnFileSave"><i class="fa fa-download"></i> Save</button>
							</div>
						</div>
						<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" id="divOutput" style="width: 100%;">
							<div id="divFileUpload" style="display: none;" data-id="">
								<form method="post" id="import_excel">
									<input type="file" name="excel_file" id="excel_file">
								</form>
								<br>
								<div id="result" style="overflow: scroll;width: 100%;"></div>
							</div>
								
							</div>
						</div>
                    </div>
				<div id="divOutput">

					
				</div>

		  </div>
		  <div class="modal-footer">
				<button type="" class="btn btn-danger m-btn" data-dismiss="modal">Close</button>
		  </div>
		  </div>
		</div>
		
	  </div>
	</div>	  
	<div class="modal fade" id="showAddLogModal"  role="dialog" aria-labelledby="exampleModalCenterTitle" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Manual Add Flexible Schedule & Logs</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <div class="m-form m-form--fit m-form--label-align-right">
		<form class="m-form m-form--fit m-form--label-align-right form-horizontal" id="defaultForm" method="post" >

		  <div class="modal-body">
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Employee:</label>
								<div class="input-group date" >
									<select class="form-control m-select2" id="emplist" name="emplist" style="text-transform: capitalize;"></select>
								</div>
						</div>
						<br>
						<div class="col-lg-12">
							<label>Schedule Date</label>
							<div class="input-group date" >
								<input type="text" class="form-control m-input" readonly placeholder="Select date" id="schedDates" name="schedDates" value="<?php echo date("m/d/Y")?>"/>
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-12">
							<label>Clock-In</label>
							<div class="input-group date" >
								<input type="text" class="form-control datetimepicker-input" placeholder="Select date" id="clockIn" name="clockIn" data-toggle="datetimepicker" data-target="#clockIn"/>
							</div>
							<span class="m-form__help"></span>
						</div>
						<div class="col-lg-12">
							<label>Clock-Out</label>
							<div class="input-group date" >
								<input type="text" class="form-control datetimepicker-input" placeholder="Select date" id="clockOut" name="clockOut" " data-toggle="datetimepicker" data-target="#clockOut"/>
							</div>
							<span class="m-form__help"></span>
						</div>
					</div>	

		  </div>
		  <div class="modal-footer">
				<button type="" class="btn btn-danger m-btn" data-dismiss="modal" onclick="resetForm()">Close</button>
  				<button type="submit" class="btn btn-success m-btn" id="btnSubmit">Submit</button>					
		  </div>
		  </form>
		  </div>
		</div>
		
	  </div>
	</div>
	
	<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>

 <script>
	 function emp_list_flexi_Sched(){
			var employeeList = $("#employeeListRecord").val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll/loadEmployeeFlexiSched'); ?>",
			cache: false,
			beforeSend:function(data){
				$("#div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
			},
			success: function (json) {
				$("#div_record_log_monitoring").html("");
				var result = JSON.parse(json);
				var str_emp = "";

					$.each(result, function (x, item){
						str_emp += '<option value='+item.emp_id+' >'+item.lname+", "+item.fname+'</option>';
							 
					}); 
					 $("#employeeListRecord").html(str_emp);
					 str_emp += '<option disabled selected></option>';
					 $("#emplist").html(str_emp);
					 $("#employeeListRecord").bootstrapDualListbox('refresh');
			}
		});
	}
	function showAddLogModal(){
		$("#showAddLogModal").modal();
		resetForm();
	}
	function resetForm(){
		$("#defaultForm").formValidation('resetForm', true);	
	}
	$(function(){
			emp_list_flexi_Sched();
			
                $('#clockIn').datetimepicker();
                $('#clockOut').datetimepicker();
			 $('#emplist').select2({
					placeholder: "Select an employee.",
					width: '100%'
			 });
			$('select[name="duallistbox_live[]"]').bootstrapDualListbox();
			$('select[name="duallistbox_record[]"]').bootstrapDualListbox();
		
		$("#excel_file").change(function(){
			$("#divFileUpload").data("id",0);
			$("#import_excel").submit();
 		}); 
	$('#import_excel').on('submit', function(event){  
           event.preventDefault();  

			var state = $("#divFileUpload").data("id");
			var linked = (state==0) ? 'payroll/uploadDTRFlex/'  : 'payroll/saveDTRFlex/' ;

			$.ajax({  
                url:"<?php echo base_url(); ?>"+linked,  
                method:"POST",  
                data:new FormData(this),  
                contentType:false,  
                processData:false,  
                success:function(data){
				if(state==0){
					if(data!="Invalid"){
							var result = JSON.parse(data);
							console.log(result);
							var str_output = "";
							str_output += '<table style="width: 900px;">';
							str_output += '<thead style="background: #3c3c3c;"><tr><td>'+result.header[0]+'</td><td>'+result.header[1]+'</td><td>'+result.header[2]+'</td><td>'+result.header[3]+'</td><td>'+result.header[4]+'</td><td>Total</td></tr></thead>';
							$.each(result.data, function (x, item){
								$.each(item, function (y, item2){
									
									var style = (item2[5]<=0) ? "style='background: #ffd1ce !important;'": "";
									str_output += '<tr '+style+'><td>'+item2[0]+'</td><td>'+item2[1]+'</td><td>'+item2[2]+'</td><td>'+item2[3]+'</td><td>'+item2[4]+'</td><td>'+item2[5]+'</td></tr>';

								}); 
							}); 
							str_output += '</table>';

							 $('#result').html(str_output);  
							$('#btnFileSave').css("display","inline-block");   
							$('#btnFileUpload').css("display","none");   

							
						 }else{
							$('#result').html("Invalid!");   
							$('#btnFileSave').css("display","none");   
							$('#btnFileUpload').css("display","inline-block");   
						 }
				}else{
 					 $("#modalShow").modal("hide");
					 $("#divFileUpload").slideToggle();
					 $("#excel_file").val('');
					 $("#result").html('');  
				}
					/* if(state==0){ //if show data
							if(data!="Invalid"){
							var result = JSON.parse(data);
							var str_output = "";
							str_output += '<table>';

							$.each(result, function (x, item){
									str_output += (x==0) ? '<thead><tr>' : '<tr>';
										$.each(item, function (y, item2){
											str_output += '<td>'+item2+'</td>';
										}); 
									str_output += (x==0) ? '</tr></thead>' :'</tr>';
							}); 
							str_output += '</table>';

							 $('#result').html(str_output);  
							$('#btnFileSave').css("display","inline-block");   
							$('#btnFileUpload').css("display","none");   

							
						 }else{
							$('#result').html("Invalid!");   
							$('#btnFileSave').css("display","none");   
							$('#btnFileUpload').css("display","inline-block");   
						 }  
					}else{ // if save data
						$('#result').html("Saving state..");   
					} */ 
                 }  
           }); 
  
		});
		$('#btnFileSave').on('click', function(){  
			$("#divFileUpload").data("id",1); // means save state
			$("#import_excel").submit();
		});
		
		
		 	$('#m_daterangepicker_1').daterangepicker({
					opens: 'right',
					
				}, function(start, end, label) {
					$("#noRecordShow").hide();
					  var date= start.format('MM-DD-YYYY') + 'to' + end.format('MM-DD-YYYY');
			});
		 
		$("#btnDownload").click(function(){ 
			var emp_id = $("#employeeListRecord").val();
				var date =  ($("#m_daterangepicker_1").val()).split(" - ");
					var date1 =moment(date[0]).format("YYYY-MM-DD");
					var date2 =moment(date[1]).format("YYYY-MM-DD");
				var emp_id2 = emp_id.toString().replace(/,/g,"_");
			if(emp_id!=""){
				window.location =  "<?php echo base_url(); ?>payroll/download_template_flexi/"+date1+"/"+date2+"/"+emp_id2;
			}else{
				alert("empty");
			}
		});
		$("#btnUpload").click(function(){
			var emp_id = $("#employeeListRecord").val();
			if(emp_id!=""){
				$("#modalShow").modal();
				$('#btnFileSave').css("display","none");   
				$('#btnFileUpload').css("display","inline-block");   
				$("#excel_file").val('');
				$("#result").html('');  
			}else{
				// alert("Please select atleast one employee to upload DTR Logs");
				swal('Error!', "Please select atleast one employee to upload DTR Logs!", 'error');
			}
		});
		$("#btnFileUpload").click(function(){
			$("#divFileUpload").slideToggle();
			$("#excel_file").val('');
			$("#result").html('');
			
			
		});
		$("#btnShow").click(function(){
			var emp_id = $("#employeeListRecord").val();
			var date =  ($("#m_daterangepicker_1").val()).split(" - ");
				var date1 =moment(date[0]).format("YYYY-MM-DD");
				var date2 =moment(date[1]).format("YYYY-MM-DD");
				console.log(emp_id);
			if(emp_id.length>0){
					$.ajax({
							type: "POST",
							url: "<?php echo base_url('payroll/showFlexiSched'); ?>",
							data: {
								emp_id : emp_id,
								date1 : date1,
								date2 : date2,
							},
							cache: false,
							success: function (json) {
									var result = JSON.parse(json);
									console.log(result);
									var str_output = "";
									if(json.length >2){
 									  str_output += '<table style="width: 100%;">';
									str_output +='<thead style="background: #676464;"><tr><td>Fullname</td><td>Date</td><td>Clock-In</td><td>Clock-Out</td></tr></thead>';
									$.each(result, function (x, item){
											
											str_output +='<tr>';
												$.each(item, function (y, item2){
													var cin = (item2[0]) ? item2[0].I : "--";
													var cout = (item2[1]) ? item2[1].O : "--";
													str_output += '<td>'+y+'</td><td>'+item2[0].date+'</td><td>'+cin+'</td><td>'+cout+'</td>';
													
												}); 
											str_output += '</tr>';
									}); 
									str_output += '</table>';
				
									 $('#result_output').html(str_output);
									 $('#tempContainer').hide();							 
									}else{
										$('#result_output').html("");
										$('#tempContainer').show();
									}
							}
					});		
			}else{
				$('#result_output').html("");
				$('#tempContainer').show();
			}
		});
	
		$('#schedDates').datepicker().on("changeDate", function (e) {
			var date = $(this).val();		
		});
		
		$('#defaultForm').formValidation({
            message: 'This value is not valid',
			live: 'enabled',
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'fa fa-refresh'
            },
            fields: {
				emplist: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'You need to select an employee.'
                        }
                    }
                },
				clockIn: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'This field is required.'
                        }
                    }
                },
				clockOut: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'This field is required.'
                        }
                    }
                },
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
			var emp = $("#emplist").val();
			var sched_date = $("#schedDates").val();
			var clockIn = $("#clockIn").val();
			var clockOut = $("#clockOut").val();
			
				$.ajax({
					type: "POST",
					url: "<?php echo base_url('payroll/insertManualFlexiLogs'); ?>",
					data: {
						emp : emp,
						sched_date : sched_date,
						clockIn : clockIn,
						clockOut : clockOut,
					},
					cache: false,
					success: function (json) { 
						if(json>0){
							$("#showAddLogModal").modal("hide");
							resetForm();
						}else{
							alert("Error!");
						}
					}
				});	
        });
	
	
	
	
	});
 
 </script>