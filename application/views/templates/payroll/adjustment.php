<link rel="stylesheet" href="<?php echo base_url("assets/css/cards-style.css"); ?>" type="text/css" /> </head>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<style>
.btn-group.buttons {
    background: #575962;
}
button.btn.moveall.btn-outline-secondary {
    margin: 0;
}
button.btn.removeall.btn-outline-secondary {
    margin: 0;
}
#tblDtrShow thead{
     background: #34bfa3;
    color: #fff;
    font-size: initial;
}
#tblDtrShow thead th {
    padding: 6px;
}
.bootstrap-duallistbox-container {
    width: 100%;
}
thead {
    background: #575962;
}
thead td {
	padding: 10px;
    color: #FFEB3B;
    font-weight: 300;
}
tbody td {
    padding: 9px;
    border: 1px solid #bdc0cc;
    color: #000;
}
tbody tr:hover {
    background: #fff7b7;
}
i.form-control-feedback.fa.fa-remove {
	display: none !important;
}
i.form-control-feedback.fa.fa-check {
	display: none !important;
}
button.btn.btn-primary.btn-sm.editable-submit {
    padding: 4px;
}
.editable-buttons {
    margin-top: -9px;
}
button.btn.btn-default.btn-sm.editable-cancel {
    padding: 4px;
}
.editable-error-block.help-block {
    position: absolute;
    margin-top: 19px;
    font-size: 11px;
}

.h3, h3 {
	font-size: .98rem !important;
}
td.class-even {
    background: #f6f6f7;
}
td.class-odd {
    background: #fff;
}
</style>
<?php
	$date = explode("-",$pcover[0]->daterange); 
	$data = date('F d', strtotime($date[0]))." - ".date('d , Y', strtotime($date[1]));
  ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/formValidation/dist/css/formValidation.min.css" />
    <link href="<?php echo base_url("assets/bootstrap4-editable/css/bootstrap-editable.css"); ?>" rel="stylesheet"/>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator"><?php echo strtoupper($site); ?> Payroll</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("/payroll/coverage"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Coverage</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/emp/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > Base</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="<?php echo  base_url("/payroll/general/".$type."/".$empstat."/".$site."/".$pcover[0]->coverage_id); ?>" class="m-nav__link">
							<span class="m-nav__link-text"> > General</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<?php echo " > ".$data;?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php
		$checker = date('Y-m-d', strtotime("+6 day", strtotime($date[1])));
		$flag = true;
		if(strtotime(date("Y-m-d")) >=strtotime($checker)){
			$flag =false;
		}
	?>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			 <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i> <?php echo ($act=="add") ? "Addition" : "Deduction" ; ?> - Manual Adjustment</a>
                                </li>
                                <li class="nav-item m-tabs__item" id="a">
                                    <a class="nav-link m-tabs__link " data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i> <?php echo ($act=="add") ? "Addition" : "Deduction" ; ?> - Batch Upload Adjustment</a>
                                </li>
								<!--<button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-right" id="toggleFilter" style="right: 36px;position: absolute;">
                                    <i class="fa fa-filter"></i>
								</button>-->
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body pt-0" style="background: #fff;">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_tabs_6_1" role="tabpanel">
						<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
							<?php if($flag){ ?>
								<a href="#" class="btn btn-accent m-btn btn-sm 	m-btn m-btn--icon m-btn--pill" style="padding: 0.70rem 1rem;" data-toggle="modal" data-target="#modalAdd" onclick="emp_list()">
									<span>
										<i class="fa fa-plus"></i>
										<span>Add</span>
									</span>
								</a>
								<?php } ?>
								<a href="#" class="btn btn-success m-btn btn-sm 	m-btn m-btn--icon m-btn--pill" style="padding: 0.70rem 1rem;" onclick="load_adjustment()">
									<span>
										<i class="fa fa-refresh"></i>
										<span>Reload</span>
									</span>
								</a>
							</div>
						
							
						</div>
						<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" id="divOutput" style="width: 100%;">
							
							</div>
						</div>
                    </div>
                    <div class="tab-pane " id="m_tabs_6_3" role="tabpanel">
					<div class="row">
					<?php if($flag){?>
							<div class="m-form m-form--fit m-form--label-align-right" style="width: 100%;">
								<a href="#" class="btn btn-accent m-btn btn-sm 	m-btn m-btn--icon m-btn--pill" id="btnTemplate" style="padding: 0.70rem 1rem;">
									<span>
										<i class="fa fa-download"></i>
										<span>Template</span>
									</span>
								</a>
								<a href="#" class="btn btn-success m-btn btn-sm m-btn m-btn--icon m-btn--pill" style="padding: 0.70rem 1rem;" id="btnFileUpload">
									<span>
										<i class="fa fa-upload"></i>
										<span>Upload File</span>
									</span>
								</a>
								<a href="#" class="btn btn-warning m-btn btn-sm m-btn m-btn--icon m-btn--pill" style="padding: 0.70rem 1rem;display:none" id="btnFileSave" >
									<span>
										<i class="fa fa-upload"></i>
										<span>Save</span>
									</span>
								</a>
							</div>
					<?php }else{ echo "<h4 class='text-success' style='margin: 0 auto;margin-top:10px;'><i class='fa fa-info-circle'></i> Payroll is already finalized and released!</h4>"; } ?>
							
						</div>
						<div class="row">
							<div class="m-form m-form--fit m-form--label-align-right" id="divOutput" style="width: 100%;">
							<div id="divFileUpload" style="display: none;" data-id="">
								<form method="post" id="export_excel">
									<input type="file" name="excel_file" id="excel_file">
								</form>
								<br>
								<div id="result" style="overflow: scroll;width: 100%;"></div>
							</div>
								
							</div>
						</div>
								
                    </div>
                </div>  

               
 
            </div>
        </div>
		</div>
	</div>
	<div class="modal fade" id="modalAdd"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Add Adjustments</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		   <form class="m-form m-form--fit m-form--label-align-right" id="defaultForm">
		  <div class="modal-body">
		
				<div class="m-portlet__body">
					
					
					
					<div class="form-group m-form__group">
						<label for="exampleSelect1">Employee Name:</label>
						<select class="form-control m-select2" id="emplist" name="reqemp"></select>
					</div>
					<div class="form-group m-form__group">
						<label for="exampleSelect1"><?php echo ($act=="add") ? "Addition" : "Deduction" ; ?> Adjustment:</label>
						<select class="form-control m-select2" id="adjustment" name="reqadjust"></select>
					</div>
					<div class="form-group m-form__group">
						<label for="exampleInputEmail1">Amount</label>
						<input type="text" class="form-control m-input" placeholder="Enter Peso value" id="reqamount" name="reqamount">
					</div>
				</div>
				
			
		  </div>
		  <div class="modal-footer">
				<button type="" class="btn btn-secondary m-btn" data-dismiss="modal" onclick="resetForm()">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
		  </div>
		  </form>
		</div>
		
	  </div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>
	<script src="<?php echo base_url(); ?>assets/src/custom/js/formValidation.min.js" ></script>
    <script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/formValidation/dist/js/formValidation.js"></script>
    <script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/formValidation/dist/js/framework/bootstrap.min.js"></script>
    <script src="<?php echo base_url("assets/bootstrap4-editable/js/bootstrap-editable.js"); ?>"></script>

 <script>
 function resetForm(){
	$("#defaultForm").formValidation('resetForm', true);	
}
 
 function load_adjustment(){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payroll/loadAdjustmentEmployee'); ?>",
			data: {
				class_type : '<?php echo $type; ?>',
				empstat :  '<?php echo $empstat; ?>',
				site :  '<?php echo $site; ?>',
				act :  '<?php echo $act; ?>',
				coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
			},
			cache: false,
			success: function (json) {
				var result = JSON.parse(json);
				var str_emp = "";
					
					txt = "<table style='width:100%'>";
						txt+="<thead>";
						txt+="<tr>";
						txt+="<td>Fullname</td>";
						txt+="<td>Adjustment</td>";
						txt+="<td>Amount</td>";
						txt+="</tr>";
						txt+="</thead>";
 					if(json!=0){
					var ctr = 0;
						$.each(result.employee, function (x, item){
							
							
							txt+='<tr><td rowspan='+item.cnt+'>'+item.lname+" , "+item.fname+'</td>';
							$.each(item.adjustment, function (y, adj){
								 var styl = (ctr % 2 == 0) ? "class-even" : "class-odd" ;
								txt += '<td class="'+styl+'">'+adj.options+'</td><td  class="'+styl+'"><a href="#" class="amount" data-type="text" data-pk="1" data-title="Enter Amount" data-id='+adj.id+' data-emp_id='+adj.emp_id+'>'+adj.value+'<a/></td></tr>';
								ctr++;
								
							}); 
							
						}); 
					}else{
						txt += '<tr><td colspan=3 style="text-align: center;font-size: x-large;font-weight:inherit;color: #3d3e4d;"> NO RECORDS FOUND</td></tr>';
					}
					txt += "</table>";
					 $("#divOutput").html(txt);
					 <?php if($flag){?>
					 $('.amount').editable({
							mode: 'popover',
							type: 'number',
							type: 'number',
							container: 'body',
							validate: function (value) {
								var regex = /(?<=^| )\d+(\.\d+)?(?=$| )|(?<=^| )\.\d+(?=$| )/;
								var parsedValue = parseFloat(value);
								// if (parsedValue.toFixed(2) == 0.00) {
									// return 'Invalid Amount. Amount must not be 0.';
								// }
								if (!regex.test(value)) {
									return 'Please input valid amount. e.g 12 or 12.00';
								}
							},
							
					}).on('save', function (e, params) {
 					 $.ajax({
							type: 'POST',
							url: "<?php echo base_url(); ?>payroll/updateAdjustment",
							data: {
								act  : '<?php echo $act; ?>',
								coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
								amount: params.newValue,
								id: $(this).data("id"),
								emp_id: $(this).data("emp_id"),
							},
							success: function (res) {
								if(res>0){
									toastr.success("Successfully updated record.");
								}else{
									toastr.error("Something went wrong. Record was not updated.");
								}
							}
						}); 
					});
					 <?php } ?>
			}
		});
	 
 }
 function emp_list(){
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/loadEmployee'); ?>",
		data: {
			class_type : '<?php echo $type; ?>',
			empstat :  '<?php echo $empstat; ?>',
			site :  '<?php echo $site; ?>',
			coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
 		},
		cache: false,
		success: function (json) {
			var result = JSON.parse(json);
			var str_emp = "";
				str_emp = '<option disabled selected > -- </option>';

				$.each(result.employee, function (x, item){
					str_emp += '<option   value='+item.emp_id+' >'+item.lname+", "+item.fname+'</option>';
						 
				}); 
				 $("#emplist").html(str_emp);
 		}
	});
}
 $(function(){ 
	
		
	$('.m-select2').select2({
			placeholder: "Select your schedule.",
			width: '100%'
	 });
	// emp_list();
	 load_adjustment();
	$("#excel_file").change(function(){
		$("#divFileUpload").data("id",0);
		$("#export_excel").submit();
		
	}); 
	
	$('#export_excel').on('submit', function(event){  
           event.preventDefault();  
		   	var act  =  '<?php echo $act; ?>';
			var coverage  =  '<?php echo $pcover[0]->coverage_id; ?>';

			var state = $("#divFileUpload").data("id");
			var linked = (state==0) ? 'payroll/uploadAdjustment/'+act+'/'+coverage  : 'payroll/saveAdjustment/'+act+'/'+coverage ;

           $.ajax({  
                url:"<?php echo base_url(); ?>"+linked,  
                method:"POST",  
                data:new FormData(this),  
                contentType:false,  
                processData:false,  
                success:function(data){
					if(state==0){ //if show data
							if(data!="Invalid"){
							var result = JSON.parse(data);
							var str_output = "";
							str_output += '<table>';

							$.each(result, function (x, item){
									str_output += (x==0) ? '<thead><tr>' : '<tr>';
										$.each(item, function (y, item2){
											str_output += '<td>'+item2+'</td>';
										}); 
									str_output += (x==0) ? '</tr></thead>' :'</tr>';
							}); 
							str_output += '</table>';

							 $('#result').html(str_output);  
							$('#btnFileSave').css("display","inline-block");   
							$('#btnFileUpload').css("display","none");   

							
						 }else{
							$('#result').html("Invalid!");   
							$('#btnFileSave').css("display","none");   
							$('#btnFileUpload').css("display","inline-block");   
						 }  
					}else{ // if save data
						$('#result').html("Saving state..");   
					} 
                 }  
           });  
      });
	  $('#btnFileSave').on('click', function(event){  
		$("#divFileUpload").data("id",1); // means save state
		$("#export_excel").submit();

           // $.ajax({  
                // url:"<?php echo base_url('payroll/saveAdjustment'); ?>",  
                // method:"POST",  
                // data:new FormData(this),  
                // contentType:false,  
                // processData:false,  
                // success:function(data){
					
                // }  
           // });  
      });  

	$("#emplist").change(function(){
 		$.ajax({
		type: "POST",
		url: "<?php echo base_url('payroll/adjustmentOption'); ?>",
		data: {
				emp : $(this).val(),
				act :  "<?php echo $act; ?>",
				coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
			},
			cache: false,
			success: function (json) {
				var result = JSON.parse(json);
				var str_emp = "";
					str_emp = '<option disabled selected > -- </option>';

					$.each(result, function (x, item){
						str_emp += '<option   value='+item.id+' >'+item.options+'</option>';
					}); 
					 $("#adjustment").html(str_emp);
			}
		});
	});
	
	 $('#defaultForm').formValidation({
            message: 'This value is not valid',
			live: 'enabled',
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-remove',
                validating: 'fa fa-refresh'
            },
            fields: { 
				reqemp: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'You need to select an employee.'
                        }
                    }
                },
				reqadjust: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'This field is required.'
                        }
                    }
                },
				reqamount: {
                    message: 'This is a required field.',
                    validators: {
                        notEmpty: {
                            message: 'This field is required.'
                        }
                    }
                },
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
				
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('payroll/addAdjustment'); ?>",
				data: {
						emp : $("#emplist").val(),
						adj : $("#adjustment").val(),
						amount : $("#reqamount").val(),
						act :  "<?php echo $act; ?>",
						coverage :  '<?php echo $pcover[0]->coverage_id; ?>',
					},
					cache: false,
					success: function (json) { 
					$("#modalAdd").modal("hide");
					resetForm();
					load_adjustment();
					}
			});
			
        });
		
		$("#btnFileUpload").click(function(){
			$("#divFileUpload").slideToggle();
			$("#excel_file").val('');
			$("#result").html('');
			
			
		});
		$("#btnTemplate").click(function(){
				var class_type = '<?php echo $type; ?>';
				var empstat  =  '<?php echo $empstat; ?>';
				var site  =  '<?php echo $site; ?>';
				var act  =  '<?php echo $act; ?>';
				var coverage  =  '<?php echo $pcover[0]->coverage_id; ?>';
					// window.open("<?php echo base_url(); ?>payroll/export_adjustment_template/"+class_type+"/"+empstat+"/"+site+"/"+act+"/"+coverage,"_blank");
				window.location =  "<?php echo base_url(); ?>payroll/export_adjustment_template/"+class_type+"/"+empstat+"/"+site+"/"+act+"/"+coverage;
 				
 
		});

 });
 </script>