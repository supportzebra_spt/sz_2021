<style>
	img {
		/*max-width: 100%;
		max-height: 100vh;
		height: auto;*/
		max-width: 50%;
		max-height: 50vh;
		height: auto;
	}
	.input_number::-webkit-outer-spin-button,
	.input_number::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	.input_number {
		-moz-appearance: textfield;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Kudos</h3>           
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Kudos/report1View" class="m-nav__link">
							<span class="m-nav__link-text">Report - Monthly</span>
						</a>
					</li>
				</ul>
			</div>

		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
			<div class="m-portlet__body">
				<div id="page-title">
					<div class="panel">
						<div class="panel-body">

							<div id="chart_1"></div>
                            <br>
                            <div class="form-group">
                                <div>
                                    <div class="clearfix row">
                                        <label for="" class="col-sm-3 control-label" style="font-size: 20px;">Date Range Picker</label>
                                        <div class="col-sm-3">
                                            <input type="text" name="fromDate" id="fromDate" placeholder="From date..." class="float-left form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="toDate" id="toDate" placeholder="To date..." class="float-left form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-3">
                                            <button class="btn btn-primary" id="datePicked" style="background-color: #3498db; border-color: #3498db; width: 100%;">Generate</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <h4 style="text-align: center;"><b>Generate Table for Overall Kudos Count</b></h4><br>
                                    <div align="center" style="display: none;">
                                        <div class="col-sm-2" align="center">
                                        </div>
                                        <div class="col-sm-3" align="center">
                                            <input type="text" name="fromDate5" id="fromDate5" placeholder="From date..." class="mrg10R form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-3" align="center">
                                            <input type="text" name="toDate5" id="toDate5" placeholder="To date..." class="form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-2" align="center">
                                            <button class="btn btn-primary" id="datePicked5" style="background-color: #3498db; border-color: #3498db; width: 100%;">Generate</button>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div id="showTable2" style="display: none;">
                                <button class="btn btn-success" id="download_report2" style="background-color: #3498db; border-color: #3498db;">Export</button><br>
                                <table class="table table-striped" id="table_report2" cellpadding="0" style="width: 100%; height: 100%; font-size: 15px;" >
                                    <thead>
                                        <tr>
                                            <td><b>Month and Year</b></td>
                                            <td><b>Kudos Count</b></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>                                   
                            </div>
                            
                            <div id="chart_3" style="display: none;"></div>
                            <div class="form-group" style="display: none;">
                                <div>
                                    <div class="clearfix row">
                                        <label for="" class="col-sm-3 control-label">Date Range Picker</label>
                                        <div class="col-sm-3">
                                            <input type="text" name="fromDate" id="fromDate3" placeholder="From date..." class="float-left form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="toDate" id="toDate3" placeholder="To date..." class="float-left form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                            <input class="btn btn-primary" type="button" name="datePicked" id="datePicked3" value="Generate" style="background-color: #3498db; border-color: #3498db;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <div>
                                    <h4 style="text-align: center;"><b>Generate Table for Overall Kudos Count by Ambassador</b></h4><br>
                                    <div align="center" style="display: none;">
                                        <div class="col-sm-2" align="center">
                                        </div>
                                        <div class="col-sm-3" align="center">
                                            <input type="text" name="fromDate6" id="fromDate6" placeholder="From date..." class="mrg10R form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-3" align="center">
                                            <input type="text" name="toDate6" id="toDate6" placeholder="To date..." class="form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-2" align="center">
                                            <button class="btn btn-primary" id="datePicked6" style="background-color: #3498db; border-color: #3498db; width: 100%;">Generate</button>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div id="showTable3" style="display: none;">
                                <button class="btn btn-success float-left" id="download_report3" style="background-color: #3498db; border-color: #3498db;">Download Excel File</button><br>
                                <table class="table table-striped" id="table_report3" cellpadding="0" style="width: 100%; font-size: 15px;" >
                                    <thead>
                                        <tr>
                                            <td><b>Ambassador</b></td>
                                            <td><b>Campaign</b></td>
                                            <td><b>Kudos Count</b></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                
                            </div>
                            <div id="chart_0" style="display: none;"></div>
                            <br>
                            <div class="form-group" style="display: none;">
                                <div>
                                    <div class="clearfix row">
                                        <div class="col-sm-4">
                                            <select class="form-control" id="addCampaign2" required="true" data-trigger="change">
                                                <option selected="selected" value="">Campaign</option>
                                                <?php 
                                                foreach($acc as $row)
                                                {
                                                    echo '<option value="'.$row->acc_id.'">'.$row->acc_name.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="fromDate" id="fromDate2" placeholder="From date..." class="float-left mrg10R form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" name="toDate" id="toDate2" placeholder="To date..." class="float-left form-control m-input m-input--solid datepick" required autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-primary" id="datePicked2" style="background-color: #3498db; border-color: #3498db;">Generate</button>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        chart = Highcharts.chart('chart_0', {
            chart: {
                inverted: true,
                polar: false,
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Kudos Count by Campaigns from <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
            },

            xAxis: {
                categories: [<?php foreach($campaign_kcount as $kc){ echo "'";echo $kc->campaign; echo "', "; }?>]
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                    },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php foreach($campaign_kcount as $kc){ echo $kc->kudos_count; echo ","; }?>],
            showInLegend: false,
        }],


    });
        Highcharts.setOptions(Highcharts.theme);
        var chart = Highcharts.chart('chart_1', {
            chart: {
                inverted: true,
                polar: false,
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Kudos Count from <?php date_default_timezone_set('Asia/Manila'); echo date("Y");?>'
            },

            xAxis: {
                type: 'category',
            },


            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                    },
                }
            },
            series: [{
                type: 'column',
                name: 'Kudos Count',
                colorByPoint: true,
                data: [

                <?php foreach($k_count as $kc){ ?>
                    {
                        name: '<?php echo ucwords($kc->month);?>',
                        y: <?php echo ucwords($kc->kudos_count);?>,
                        drilldown: '<?php echo $kc->month;?>'
                    },
                <?php }?>
                ]
            }],

            drilldown: {
                series: [
                <?php foreach($k_count as $k){ ?>
                    {
                        type: 'column',
                        name: '<?php echo ($k->month);?>',
                        id: '<?php echo ($k->month);?>',
                        data: [ <?php foreach ($drilldown2 as $d){
                            if($k->month==$d->month){?>
                                ['<?php echo $d->ambassador?>', <?php echo $d->kudos_count?>],
                            <?php }}?>
                            ]
                        },
                    <?php }?>
                    ]
                }


            });
        Highcharts.setOptions(Highcharts.theme);
        Highcharts.chart('chart_2', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Top 5 Ambassadors for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    },
                }
            },
            series: [{
                type: 'pie',
                name: 'Kudos Count',
                data: [

                <?php foreach($top as $t){ ?>
                    {
                        name: '<?php echo $t->ambassador;?>',
                        y: <?php echo $t->kudos_count;?>
                    },
                <?php }?>
                ]
            }]
        });
        Highcharts.setOptions(Highcharts.theme);
        Highcharts.chart('chart_3', {
            chart: {
                inverted: true,
                polar: false
            },
            title: {
                text: 'Kudos Count from <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
            },
            credits: {
                enabled: false
            },

            xAxis: {
                categories: [<?php foreach($all as $a){ echo "'";echo $a->ambassador; echo "', "; }?>]
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                    },
                //colors: ['#3399ff']
            }
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php foreach($all as $a){ echo $a->kudos_count; echo ","; }?>],
            showInLegend: false
        }]

    });
        Highcharts.setOptions(Highcharts.theme);
        Highcharts.chart('chart_4', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Top 5 Campaigns for <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Kudos Count',
                data: [

                <?php foreach($top_campaigns as $t){ ?>
                    {
                        name: '<?php echo ucwords($t->campaign);?>',
                        y: <?php echo ucwords($t->kudos_count);?>,
                        drilldown: '<?php echo $t->campaign;?>'
                    },
                <?php }?>
                ]
            }],
            drilldown: {
                series: [
                <?php foreach($top_campaigns as $t){ ?>
                    {
                        name: '<?php echo ucwords($t->campaign);?>',
                        id: '<?php echo ucwords($t->campaign);?>',
                        data: [ <?php foreach ($drilldown as $d){
                            if($t->campaign==$d->campaign){?>
                                ['<?php echo $d->ambassador?>', <?php echo $d->kudos_count?>],
                            <?php }}?>
                            ]
                        },
                    <?php }?>
                    ]
                }
            });
    });
</script>
<script>
    $(function(){
        $('.datepick').datepicker({format: 'yyyy-mm-dd'});
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Templatedesign/headerLeft",
          cache: false,
          success: function(html)
          {

            $('#headerLeft').html(html);
     //alert(html);
 }
}); 
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>index.php/Templatedesign/sideBar",
          cache: false,
          success: function(html)
          {

            $('#sidebar-menu').html(html);
   // alert(html);
}
});
    }); 
</script>
<script>

    $(function(){

        $("#datePicked").click(function(){
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();

            var arr_date1 = fromDate.split('-');
            var arr_date2 = toDate.split('-');

            var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var month1 = arr_date1[1];
            var month2 = arr_date2[1];
            var day1 = arr_date1[2];
            var day2 = arr_date2[2];

            var m1 = month1.replace(/^0+/, '');
            var m2 = month2.replace(/^0+/, '');
            var d1 = day1.replace(/^0+/, '');
            var d2 = day2.replace(/^0+/, '');

            var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
            var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate',
                type: 'POST',
                dataType: 'json',
                data:  dataString,
                success:function(data)
                {
                    var month = data.map(function (value) {
                        return value.month;
                    });
                    var kudos_count = data.map(function (value) {
                        return parseInt(value.kudos_count);
                    });

                    console.log(month);
                    console.log(kudos_count);

                    $("#datePicked5").trigger("click");

                    chart = Highcharts.chart('chart_1', {
                        chart: {
                            inverted: true,
                            polar: false,
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Kudos Count '+fromDate2+' to '+toDate2
                        },

                        xAxis: {
                            categories: month,
                        },

                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                },
                                
                            }
                        },

                        series: [{
                            type: 'column',
                            colorByPoint: true,
                            data: kudos_count,
                            showInLegend: false,
                        }],


                    });
                    Highcharts.setOptions(Highcharts.theme);
                }
            });
        });

        $("#datePicked2").click(function(){
            var fromDate = $("#fromDate2").val();
            var toDate = $("#toDate2").val();
            var campaign = $("#addCampaign2").val();
            var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();

            var arr_date1 = fromDate.split('-');
            var arr_date2 = toDate.split('-');

            var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var month1 = arr_date1[1];
            var month2 = arr_date2[1];
            var day1 = arr_date1[2];
            var day2 = arr_date2[2];

            var m1 = month1.replace(/^0+/, '');
            var m2 = month2.replace(/^0+/, '');
            var d1 = day1.replace(/^0+/, '');
            var d2 = day2.replace(/^0+/, '');

            var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
            var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

            var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign;


            $.ajax({
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate2',
                type: 'POST',
                dataType: 'json',
                data:  dataString,
                success:function(data)
                {
                    
                    var month = data.map(function (value) {
                        return value.month;
                    });
                    var kudos_count = data.map(function (value) {
                        return parseInt(value.kudos_count);
                    });
                    var c = data.map(function (value) {
                        return parseInt(value.campaign);
                    });

                    console.log(month);
                    console.log(kudos_count);
                    chart = Highcharts.chart('chart_0', {
                        chart: {
                            inverted: true,
                            polar: false,
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Kudos Count '+fromDate2+' to '+toDate2+' of '+campaign_name
                        },

                        xAxis: {
                            categories: month,
                        },

                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                },
                            }
                        },

                        series: [{
                            type: 'column',
                            colorByPoint: true,
                            data: kudos_count,
                            showInLegend: false,
                        }],


                    });
                    Highcharts.setOptions(Highcharts.theme);
                }
            });
        });

        $("#datePicked3").click(function(){
            var fromDate = $("#fromDate3").val();
            var toDate = $("#toDate3").val();

            var arr_date1 = fromDate.split('-');
            var arr_date2 = toDate.split('-');

            var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var month1 = arr_date1[1];
            var month2 = arr_date2[1];
            var day1 = arr_date1[2];
            var day2 = arr_date2[2];

            var m1 = month1.replace(/^0+/, '');
            var m2 = month2.replace(/^0+/, '');
            var d1 = day1.replace(/^0+/, '');
            var d2 = day2.replace(/^0+/, '');

            var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
            var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate3',
                type: 'POST',
                dataType: 'json',
                data:  dataString,
                success:function(data)
                {
                    var ambassador = data.map(function (value) {
                        return value.ambassador;
                    });
                    var kudos_count = data.map(function (value) {
                        return parseInt(value.kudos_count);
                    });
                    console.log(ambassador);
                    console.log(kudos_count);
                    $("#datePicked6").trigger("click");
                    chart = Highcharts.chart('chart_3', {
                        chart: {
                            inverted: true,
                            polar: false,
                        },
                        credits: {
                            enabled: false
                        },
                        title: {
                            text: 'Kudos Count '+fromDate2+' to '+toDate2
                        },

                        xAxis: {
                            categories: ambassador,
                        },

                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                },
                                    //colors: ['#3399ff']
                                }
                            },

                            series: [{
                                type: 'column',
                                colorByPoint: true,
                                data: kudos_count,
                                showInLegend: false,
                            }],


                        });
                    Highcharts.setOptions(Highcharts.theme);
                }
            });
        });

        $("#datePicked4").click(function(){
            var fromDate = $("#fromDate4").val();
            var toDate = $("#toDate4").val();
            var campaign = $("#addCampaign4").val();
            var campaign_name = $("#addCampaign4 option[value='"+campaign+"']").text();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign;

                //alert(dataString);

                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/Kudos/getDate4',
                    type: 'POST',
                    data:  dataString,
                    success:function(data)
                    {
                        $("#table_report1 tbody").html(data);
                        $("#showTable1").show();
                        console.log(data);
                        //alert(JSON.stringify(data));
                    }
                });
            });

        $("#datePicked5").click(function(){
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate5',
                type: 'POST',
                data:  dataString,
                success:function(data)
                {
                    $("#table_report2 tbody").html(data);
                    $("#showTable2").show();
                }
            });
        });

        $("#datePicked6").click(function(){
            var fromDate = $("#fromDate3").val();
            var toDate = $("#toDate3").val();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
                //url of the function
                url: '<?php echo base_url(); ?>index.php/Kudos/getDate6',
                type: 'POST',
                data:  dataString,
                success:function(data)
                {
                    $("#table_report3 tbody").html(data);
                    $("#showTable3").show();
                }
            });
        });

        $("#myButton1").click(function (e) {
            $("#table1").table2excel({
                name: 'report',
                filename: 'report'
            });
        });

        $("#download_report1").click(function (e) {
            $("#table_report1").table2excel({
                name: 'report',
                filename: 'report'
            });
        });

        $("#download_report2").click(function (e) {
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();

            var dataString = "fromDate="+fromDate+"&toDate="+toDate;

            $.ajax({
            //url of the function
            url: '<?php echo base_url(); ?>index.php/Kudos/excel_report1',
            type: 'POST',
            data:  dataString,
            beforeSend: function() {
                mApp.block('#portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg',
                    message: 'Downloading...'
                });
            },
            xhrFields: {
                responseType: 'blob'
            },
            success:function(data)
            {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'KudosCount.xlsx';
                a.click();
                window.URL.revokeObjectURL(url);
                mApp.unblock('#portlet');
            }
        });
        });

        $("#download_report3").click(function (e) {
            $("#table_report3").table2excel({
                name: 'report',
                filename: 'report'
            });
        });


        
    });
</script>
<a href="<?php echo base_url(); ?>/reports/KudosCount.xls" style="display: none;"><input type="button" id="kcount" value="Test" class="btn btn-success"></a>
<script type="text/javascript" src="<?php echo base_url();?>assets/assets-minified/admin-all-demo.js"></script>

