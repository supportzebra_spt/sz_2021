<style>
	
	.input_number::-webkit-outer-spin-button,
	.input_number::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	.input_number {
		-moz-appearance: textfield;
	}
	.blockqoute {
		/*font-family: 'Gelasio', serif;*/
		/*font-style: italic;*/
		font-size: 1.15em;
		max-width: 100%;
		margin: 10px 30px;
		padding: 25px;
		background-color: #f8fffe;
		border: 1px solid #b0d2cb;
		color: #0b4237;
		box-sizing: border-box;
		border-left-width: 10px;

	}

	.blockqoute::before {
		content: "\201c";
		font-size: 70px;
		color: #009578;
		display: block;
		margin-bottom: -50px;
		margin-top: -30px;
	}
	.blockqoute__text {
		/*font-style: italic;*/
		font-size: 12px;
		line-height: 1.5;
		margin: 0
	}

	.blockquote__text--credit {
		font-size: 12px;
		font-weight: bold;
		text-align: right;
		font-style: normal;
	}

	.blockquote__text--credit::before{
		content: "\2014\0020";
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Kudos</h3>           
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<!-- <li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Kudos/kudos_all" class="m-nav__link">
							<span class="m-nav__link-text">List of Kudos</span>
						</a>
					</li> -->
				</ul>
			</div>

		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
			<div class="m-portlet__body">
				<div class="m-section" id="datatable_div">
					<div class="m-form m-form--label-align-right m--margin-bottom-10">
						<div class="m--margin-bottom-30">
							<h5>Kudos | List of Kudos <br>
								<small class="text-muted">Displayed All Kudos Transaction</small>
							</h5>
						</div>
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-6">
										<div style="width: 100%; padding: 5px" class="m-input-icon m-input-icon--left m-input-icon--right">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search for..." id="searchField" autocomplete="off">
											<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
										</div>
									</div>
									<div class="col-md-6">
										<div style="width:100%" class="m-form__group m-form__group--inline">
											<div class="m-input-icon m-input-icon--left">
												<input id="searchDate" type="text" class="form-control m-input m-input--solid" readonly="" placeholder="Select time">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span><i class="la la-calendar"></i></span>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="#" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-info exportButton">
									<span>
										<span>Export File </span>
										<i class="fa fa-file-excel-o"></i>
									</span>
								</a>
								<div class="m-separator m-separator--dashed d-xl-none"></div>
							</div>
						</div>
					</div>
					<div id="offenseDatatable"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="view_kudos_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="max-width: 850px" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button style="float: right" class="btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<div style="margin: 0 30px">
					<div class="form-group m-form__group row">
						<div style="margin: 20px auto;">
							<h2 style="text-align: center!important;">Kudos</h2>
							<h5 style="text-align: center!important;" class="text-muted">Details of A Kudos Transaction</h5>
						</div>
					</div>
					<!-- <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="300"> -->
						<div class="row">
							<div class="col-5">
								<div id="details_1"></div>
							</div>
							<div class="col-7">
								<blockquote class="blockqoute">
									<div id="details_2"></div>
								</blockquote>
								
							</div>
						</div>
						<hr>
						<div class="form-group">
							<blockquote class="blockqoute">
								<div id="details_3"></div>
							</blockquote>

						</div>
						<hr>
						<div class="row">
							<div class="col-4">
								<div class="form-group">
									<div id="reward_type"></div>
								</div>
								<div class="form-group">
									<div id="proofreading"></div>
								</div>
								<div class="form-group">
									<div id="status"></div>
								</div>
							</div>
							<div class="col-8">
								<div class="text-center">
									<label for="message-text" class="m--font-bolder">Card Picture</label>
								</div>
								<div class="text-center">
									<div id="card"></div>
								</div>
							</div>
						</div>
					<!-- </div> -->
					<div style="bottom: 0;" class="form-group m-form__group row">
						<div style="margin: 10px auto;">
							<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="show_image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="max-width: 100%;" role="document">
		<div class="modal-content">
			<div class="modal-body" style="background-color: black;">
				<button style="float: right; margin-top: -20px; margin-right: -20px;" class="btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<div style="margin: 0 30px">
					<div id="image"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var offenseDatatable = function () {
		var offense = function (searchVal = "", start_date, end_date) {
			var options = {
				data: {
					type: 'remote',
					source: {
						read: {
							method: 'POST',
							url: "<?php echo base_url(); ?>Kudos/kudos_all_data",
							params: {
								query: {
									searchField: searchVal,
									start: start_date,
									end: end_date,
								},
							},
							map: function (raw) {
								var dataSet = raw;
								if (typeof raw.data !== 'undefined') {
									dataSet = raw.data;
								}
								dataCount = dataSet.length;
								return dataSet;
							}
						}
					},
					saveState: {
						cookie: false,
						webstorage: false
					},
					pageSize: 5,
					serverPaging: true,
					serverFiltering: true,
					serverSorting: true,
				},
				layout: {
					theme: 'default',
					class: '',
					scroll: true,
					footer: false,

				},
				sortable: true,
				pagination: true,
				toolbar: {
					placement: ['bottom'],
					items: {
						pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
            	input: $('#searchField'),
            },
            columns: [
            {
            	field: "ambassador",
            	title: "Name",
            	width: 200,
            	selector: false,
            	sortable: true,
            	textAlign: 'left',
            }, 
            {
            	field: 'campaign',
            	width: 200,
            	title: 'Campaign',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'left',
            },  
            {
            	field: 'kudos_type',
            	width: 40,
            	title: 'Type',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.kudos_type == 'Call'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">'+row.kudos_type+'</span>';
            		} else {
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">'+row.kudos_type+'</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'proofreading',
            	width: 80,
            	title: '<small class="m--font-boldest">Proofreading</small',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.proofreading == 'Pending'){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Pending</span>';
            		} else if(row.proofreading == 'Done'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Done</span>';
            		} else if(row.proofreading == 'In Progress'){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">In Progress</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'kudos_card',
            	width: 70,
            	title: 'Card',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.kudos_card == 'Pending'){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Pending</span>';
            		} else if(row.kudos_card == 'Done'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Done</span>';
            		} else if(row.kudos_card == 'In Progress'){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">In Progress</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'reward_status',
            	width: 80,
            	title: 'Reward',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.reward_status == 'Pending'){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Pending</span>';
            		} else if(row.reward_status == 'Requested'){
            			status = '<span class="m-badge m-badge--primary m-badge--wide m-badge--rounded">Requested</span>';
            		} else if(row.reward_status == 'Released'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Released</span>';
            		} else if(row.reward_status == 'Approved'){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">Approved</span>';
            		} else if(row.reward_status == 'On Hold'){
            			status = '<span class="m-badge m-badge--brand m-badge--wide m-badge--rounded">On Hold</span>';
            		} else if(row.reward_status == 'Reject'){
            			status = '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">Reject</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'added_on',
            	width: 150,
            	title: 'Date Added',
            	overflow: 'visible',
            	sortable: 'desc',
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		return row.date_given;
            	},
            },  
            {
            	field: "Actions",
            	width: 70,
            	title: "Actions",
            	textAlign: 'center',
            	sortable: false,
            	overflow: 'visible',
            	template: function (row, index, datatable) {
            		var view = "<a style='text-decoration: none;' href='#' data-kudos_id='"+row.kudos_id+"' data-reward_type='"+row.reward_type+"' data-kudos_type='"+row.kudos_type+"' data-client_name='"+row.client_name+"' data-client_phone='"+row.phone_number+"' data-comment='"+row.comment+"' data-client_email='"+row.client_email+"' data-file='"+row.file+"' data-date_given='"+row.date_given+"' data-fname='"+row.fname+"' data-lname='"+row.lname+"' data-campaign='"+row.campaign+"' data-status='"+row.status+"' data-pos_details='"+row.pos_details+"' data-comment='"+row.comment+"' data-revision='"+row.revision+"' data-revised_by='"+row.revised_by+"' data-date_revised='"+row.date_revised+"' data-proofreading='"+row.proofreading+"' data-reward_status='"+row.reward_status+"' data-kudos_card_pic='"+row.kudos_card_pic+"' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill view_modal' title='View '><span class='btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air'><i style='color:#00c5dc;' class='la la-search'></i></span></a>";
            		return "\
            		"+view+"\
            		";
            	}
            }
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
    	init: function (searchVal, start_date, end_date) {
    		offense(searchVal, start_date, end_date);
    	}
    };
}();

function initOffenses(){
	let search_date_val = $('#searchDate').val().split(' - ');
	$('#offenseDatatable').mDatatable('destroy');
	offenseDatatable.init('',  moment(search_date_val[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(search_date_val[1], 'MM/DD/YYYY').format('Y-MM-DD'));
}

$(function(){
	$('.m_selectpicker').selectpicker();
	initOffenses();
	getCurrentDateTime(function (date) {
		start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
		end =  moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
		$('#searchDate').daterangepicker({
			buttonClasses: 'm-btn btn',
			applyClass: 'btn-primary',
			cancelClass: 'btn-secondary',
			startDate: start,
			endDate: end,
			ranges: {
				'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
				'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
			}
		}, function(start, end, label) {
			$('#searchDate').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			initOffenses();
		});
	});
	$("#show_image").on("hidden.bs.modal", function(){
		$("#view_kudos_modal").modal('show');
	});
	// $('img').on("error", function() {
	// 	$(this).attr('src', '<?php echo base_url()?>assets/images/emoji/image-not-found.png');
	// });
})

$('#datatable_div').on('click','.view_modal',function(){
	$('#view_kudos_modal').modal('show');
	let kudos_id = $(this).data('kudos_id');
	console.log(kudos_id);
	$.ajax({
		url:'<?php echo base_url();?>Kudos/kudos_add_modal',
		type:"post",
		async: false,
		data: {kudos_id},
		dataType: 'json',
		success: function(data){
			let image_link = data.old_image_link;
			if(data.modal.is_new == 1){
				image_link = '<?php echo base_url()?>'
			}
			  var pic = imagePath + "" + data.modal.pic;
			let details_1 = "<div class='m-card-user__pic'><img style='max-width:60%; border: 1px solid' src='"+pic+"' onerror='noImageGeneral(this)' class='m--img-rounded m--marginless header-userpic6' style='border: 1px solid #afabab;'></div><br>"+
			"<h3>"+data.modal.ambassador+"</h3><p><span>"+data.modal.pos_details+" ("+data.modal.status+")</span></p><p><span class='m--font-boldest'>"+data.modal.campaign+"</span></p>";
			let details_2 = (data.modal.kudos_type == 'Email') ? "<p class='blockqoute__text'><i class='la la-calendar'></i> "+data.modal.date_given+"<br><i class='la la-envelope'></i> "+data.modal.client_email+"</p><br><br><a href='"+image_link+data.modal.file+"' class='cardpicture' data-lightbox="+image_link+data.modal.file+"><img style='max-width:100%;height:auto' src='"+image_link+data.modal.file+"' onerror='imgError(this);'></a><p> <footer class='blockquote__text--credit'> "+data.modal.client_name+"</footer></p>" : "<p class='blockqoute__text'><i class='la la-calendar'></i> "+data.modal.date_given+"<br><i class='la la-phone'></i> "+data.modal.phone_number+"<br><br>"+data.modal.comment+"</p><footer id='' class='blockquote__text--credit'>"+data.modal.client_name+"</footer>";
			let details_3 = (data.modal.revision != '') ? "<p class='blockqoute__text'>"+data.modal.revision+"</p><footer class='blockquote__text--credit'>Edited by "+data.modal.revised_by+" on "+data.modal.date_revised+"</footer>" : "<p class='blockqoute__text' style='text-align:center; font-style:normal;'><mark>NOT YET REVISED</mark</p>";
			let reward_type_display = (data.modal.reward_type == 'Credit') ? "<label for='message-text' class='m--font-bolder'>Reward Type: <br><span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>"+data.modal.reward_type+"</span></label>" : "<label for='message-text' class='m--font-bolder'>Reward Type: <br><span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>"+data.modal.reward_type+"</span></label>"
			let proofreading_display = (data.modal.proofreading == 'Pending') ? "<label for='message-text' class='m--font-bolder'>Proof Reading: <br><span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>"+data.modal.proofreading+"</span></label>" : (data.modal.proofreading == 'Done') ? "<label for='message-text' class='m--font-bolder'>Proof Reading: <br><span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>"+data.modal.proofreading+"</span></label>" : (data.modal.proofreading == 'In Progress') ? "<label for='message-text' class='m--font-bolder'>Proof Reading: <br><span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>"+data.modal.proofreading+"</span></label>" : "";
			let status_display = (data.modal.reward_status == 'Pending') ? "<label for='message-text' class='m--font-bolder'>Reward Status: <br><span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>"+data.modal.reward_status+"</span></label>" : (data.modal.reward_status == 'Requested') ? "<label for='message-text' class='m--font-bolder'>Reward Status: <br><span class='m-badge m-badge--primary m-badge--wide m-badge--rounded'>"+data.modal.reward_status+"</span></label>" : (data.modal.reward_status == 'Released') ? "<label for='message-text' class='m--font-bolder'>Reward Status: <br><span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>"+data.modal.reward_status+"</span></label>" : (data.modal.reward_status == 'Approved') ? "<label for='message-text' class='m--font-bolder'>Reward Status: <br><span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>"+data.modal.reward_status+"</span></label>" : (data.modal.reward_status == 'On Hold') ? "<label for='message-text' class='m--font-bolder'>Reward Status: <br><span class='m-badge m-badge--brand m-badge--wide m-badge--rounded'>"+data.modal.reward_status+"</span></label>" : (data.modal.reward_status == 'Reject') ? "<label for='message-text' class='m--font-bolder'>Reward Status: <br><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>"+data.modal.reward_status+"</span></label>" : "";
			let display_image = '';
			let image = image_link+data.modal.kudos_card_pic;
			if(data.modal.kudos_card_pic != ''){
				display_image = '<a href="'+image+'" class="cardpicture" data-lightbox="'+image+'"><img style="max-width:80%;height:auto" src="'+image+'" onerror="imgError(this);"></a>';
			} else {
				display_image = "<p class='blockqoute__text' style='text-align:center; font-style:normal;'><mark>NO KUDOS CARD UPLOADED YET</mark</p>";
			}
			// $('#kudos_type').html(data.modal.kudos_type);
			// $('#date_given').html(data.modal.date_given);
			$('#details_1').html(details_1);
			$('#details_2').html(details_2);
			$('#details_3').html(details_3);
			$('#reward_type').html(reward_type_display);
			$('#proofreading').html(proofreading_display);
			$('#status').html(status_display);
			$('#card').html(display_image);
		}
	});
});

function imgError(image) {
    image.onerror = "";
    image.src = "<?php echo base_url()?>assets/images/emoji/image-not-found.png";
    return true;
}

// $('#card,#details_2').on('click','.cardpicture',function(e) {
// 	e.preventDefault();
// 	let image = $(this).data('lightbox');
// 	$('#image').html('<img style="display:block; margin-left:auto; margin-right:auto; height:auto; width:100%;" src="'+image+'" onerror="imgError(this);">');
// 	$('#view_kudos_modal').modal('hide');
// 	$('#show_image').modal('show');
// });

$('.exportButton').click(function() {
	let search_date_val = $('#searchDate').val().split(' - ');
	let start           = moment(search_date_val[0], 'MM/DD/YYYY').format('Y-MM-DD');
	let end             = moment(search_date_val[1], 'MM/DD/YYYY').format('Y-MM-DD')
	let searchField     = $('#searchField').val();
	$.ajax({
		url: '<?php echo base_url(); ?>Kudos/downloadExcel_kudos',
		method: 'POST',
		data: {
			searchField,
			start,
			end,
		},
		beforeSend: function() {
			mApp.block('#portlet', {
				overlayColor: '#000000',
				type: 'loader',
				state: 'brand',
				size: 'lg',
				message: 'Downloading...'
			});
		},
		xhrFields: {
			responseType: 'blob'
		},
		success: function(data) {
			var a = document.createElement('a');
			var url = window.URL.createObjectURL(data);
			a.href = url;
			a.download = 'KudosList.xlsx';
			a.click();
			window.URL.revokeObjectURL(url);
			mApp.unblock('#portlet');
		},
		error: function(data) {
			$.notify({
				message: 'An error occured while exporting excel'
			}, {
				type: 'danger',
				timer: 1000
			});
			mApp.unblock('#portlet');
		}
	});
});

$('#searchDate').change(function(){
	initOffenses();
});

</script>
