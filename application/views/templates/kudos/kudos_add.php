<style>
	img {
		/*max-width: 100%;
		max-height: 100vh;
		height: auto;*/
		max-width: 75%;
		max-height: 75vh;
		height: auto;
	}
	.input_number::-webkit-outer-spin-button,
	.input_number::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	.input_number {
		-moz-appearance: textfield;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Kudos</h3>           
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<!-- <li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Kudos/kudos_add" class="m-nav__link">
							<span class="m-nav__link-text">Kudos Add</span>
						</a>
					</li> -->
				</ul>
			</div>

		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
			<div class="m-portlet__body">
				<div class="m-section" id="datatable_div">
					<div class="m-form m-form--label-align-right m--margin-bottom-10">
						<div class="m--margin-bottom-30">
							<h5>Kudos | Add <br>
								<small class="text-muted">Displayed All New Created Kudos</small>
							</h5>
						</div>
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-8">
										<div style="width: 100%; padding: 5px" class="m-input-icon m-input-icon--left m-input-icon--right">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search for..." id="searchField" autocomplete="off">
											<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="#" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info addNewButton">Create New Kudos 
									<i class="fa fa-plus"></i>
								</a>
								<div class="m-separator m-separator--dashed d-xl-none"></div>
							</div>
						</div>
					</div>
					<div id="offenseDatatable"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="create_kudos" method="post">
	<div class="modal fade" id="create_kudos_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="max-width: 700px" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button style="float: right" class="btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<div style="margin: 0 30px">
						<div class="form-group m-form__group row">
							<div style="margin: 20px auto;">
								<h2 style="text-align: center!important;">Kudos</h2>
								<h5 style="text-align: center!important;" class="text-muted">Create New Kudos</h5>
							</div>
						</div>
						
						
						<div class="row">
							<div class="col-6">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Accounts <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="create_accounts" id="create_accounts" title="Select your account here" data-dropup-auto="false" data-live-search="true" required>
									</select>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Ambassador <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="create_employee" id="create_employee" title="Select your ambassador here" data-dropup-auto="false" data-live-search="true" required>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label for="message-text" class="m--font-bolder">Preferred Reward <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" required name="create_reward" id="create_reward" title="Select your preferred reward here">
										<option value="Cash">Cash</option>
										<option value="Credit">Credit</option>
									</select>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label for="message-text" class="m--font-bolder">Kudos Type <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="create_type" id="create_type" title="Select the kudos type here" required>
										<option data-content="<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>Call</span>" value="Call">Call</option>
										<option data-content="<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>Email</span>" value="Email">Email</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Client's Name <span class="m--font-danger">*</span></label>
									<input type="text" class="form-control m-input m-input--solid" placeholder="Type the client's name here" autocomplete="off" name="create_name" id="create_name" required>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group m-form__group">
									<div id="client_type1"></div>
								</div>
							</div>
						</div>
						<div class="form-group m-form__group">
							<div id="client_type2"></div>
						</div>
						<div style="bottom: 0;" class="form-group m-form__group row">
							<div style="margin: 10px auto;">
								<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-info">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form id="update_kudos" method="post">
	<div class="modal fade" id="update_kudos_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="max-width: 700px" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button style="float: right" class="btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<div style="margin: 0 30px">
						<div class="form-group m-form__group row">
							<div style="margin: 30px auto;">
								<h2 style="text-align: center!important;">Kudos</h2>
								<h5 style="text-align: center!important;" class="text-muted">Update Existing Kudos</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Accounts <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="upacc_id" id="upacc_id" title="Select your account here">

									</select>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Ambassador <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="upemp_id" id="upemp_id" title="Select your ambassador here">

									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label for="message-text" class="m--font-bolder">Preferred Reward <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" required name="upreward_type" id="upreward_type" title="Select your preferred reward here">
										<option value="Cash">Cash</option>
										<option value="Credit">Credit</option>
									</select>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label for="message-text" class="m--font-bolder">Kudos Type <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="upkudos_type" id="upkudos_type" title="Select the kudos type here">
										<option data-content="<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>Call</span>" value="Call">Call</option>
										<option data-content="<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>Email</span>" value="Email">Email</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Client's Name <span class="m--font-danger">*</span></label>
									<input type="text" class="form-control m-input m-input--solid" placeholder="Type the client's name here" autocomplete="off" name="upclient_name" id="upclient_name" required>
									<input type="hidden" name="upkudos_id" id="upkudos_id" required>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group m-form__group">
									<div id="upclient_type1"></div>
								</div>
							</div>
						</div>
						<div class="form-group m-form__group">
							<div id="upclient_type2"></div>
							<div id="image_display"></div>
						</div>
						<div style="bottom: 0;" class="form-group m-form__group row">
							<div style="margin: 10px auto;">
								<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-info">Update</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	var offenseDatatable = function () {
		var offense = function (searchVal = "") {
			var options = {
				data: {
					type: 'remote',
					source: {
						read: {
							method: 'POST',
							url: "<?php echo base_url(); ?>Kudos/kudos_add_data",
							params: {
								query: {
									searchField: searchVal,
								},
							},
							map: function (raw) {
								var dataSet = raw;
								if (typeof raw.data !== 'undefined') {
									dataSet = raw.data;
								}
								dataCount = dataSet.length;
								return dataSet;
							}
						}
					},
					saveState: {
						cookie: false,
						webstorage: false
					},
					pageSize: 5,
					serverPaging: true,
					serverFiltering: true,
					serverSorting: true,
				},
				layout: {
					theme: 'default',
					class: '',
					scroll: true,
					footer: false

				},
				sortable: true,
				pagination: true,
				toolbar: {
					placement: ['bottom'],
					items: {
						pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
            	input: $('#searchField'),
            },
            columns: [
            {
            	field: "ambassador",
            	title: "Name",
            	width: 200,
            	selector: false,
            	sortable: true,
            	textAlign: 'left',
            }, 
            {
            	field: 'campaign',
            	width: 200,
            	title: 'Campaign',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'left',
            },  
            {
            	field: 'kudos_type',
            	width: 40,
            	title: 'Type',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.kudos_type == 'Call'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">'+row.kudos_type+'</span>';
            		} else {
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">'+row.kudos_type+'</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'proofreading',
            	width: 80,
            	title: '<small class="m--font-boldest">Proofreading</small',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.proofreading == 'Pending'){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Pending</span>';
            		} else if(row.proofreading == 'Done'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Done</span>';
            		} else if(row.proofreading == 'In Progress'){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">In Progress</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'kudos_card',
            	width: 80,
            	title: 'Card',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.kudos_card == 'Pending'){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Pending</span>';
            		} else if(row.kudos_card == 'Done'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Done</span>';
            		} else if(row.kudos_card == 'In Progress'){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">In Progress</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'reward_status',
            	width: 80,
            	title: 'Reward',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.reward_status == 'Pending'){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Pending</span>';
            		} else if(row.reward_status == 'Requested'){
            			status = '<span class="m-badge m-badge--primary m-badge--wide m-badge--rounded">Requested</span>';
            		} else if(row.reward_status == 'Released'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Released</span>';
            		} else if(row.reward_status == 'Approved'){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">Approved</span>';
            		} else if(row.reward_status == 'On Hold'){
            			status = '<span class="m-badge m-badge--brand m-badge--wide m-badge--rounded">On Hold</span>';
            		} else if(row.reward_status == 'Reject'){
            			status = '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">Reject</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'added_on',
            	width: 150,
            	title: 'Date Added',
            	overflow: 'visible',
            	sortable: 'desc',
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		return row.date_given;
            	},
            },  
            {
            	field: "Actions",
            	width: 70,
            	title: "Actions",
            	textAlign: 'center',
            	sortable: false,
            	overflow: 'visible',
            	template: function (row, index, datatable) {
            		var edit = "<a style='text-decoration: none;' href='#' data-kudos_id='"+row.kudos_id+"' data-acc_id='"+row.acc_id+"' data-emp_id='"+row.emp_id+"' data-reward_type='"+row.reward_type+"' data-kudos_type='"+row.kudos_type+"' data-client_name='"+row.client_name+"' data-client_phone='"+row.phone_number+"' data-comment='"+row.comment+"' data-client_email='"+row.client_email+"' data-file='"+row.file+"' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill update_modal' title='Update '><span class='btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air'><i style='color:#00c5dc;' class='la la-edit'></i></span></a>";
            		return "\
            		"+edit+"\
            		";
            	}
            }
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
    	init: function (searchVal) {
    		offense(searchVal);
    	}
    };
}();

function initOffenses(){
	$('#offenseDatatable').mDatatable('destroy');
	offenseDatatable.init();
}

$(function(){
	$('.m_selectpicker').selectpicker();
	account_option();
	initOffenses();
})

function account_option(){
	$.ajax({
		url:'<?php echo base_url();?>Kudos/Kudos_add_account',
		type:"post",
		async: false,
		dataType: 'json',
		success: function(data){
			$('#create_accounts').html(data);
			$('#create_accounts').selectpicker('refresh');
			$('#upacc_id').html(data);
			$('#upacc_id').selectpicker('refresh');
		}
	});
}

function ambassador_option(acc_id,display){
	$.ajax({
		url:'<?php echo base_url();?>Kudos/Kudos_add_ambassador',
		type:"post",
		async: false,
		data: {acc_id},
		dataType: 'json',
		success: function(data){
			$(display).html(data);
			$(display).selectpicker('refresh');
		}
	});
}

function type_option(modal_type,type){
	console.log(modal_type);
	console.log(type);
	let client_type1 = '';
	let client_type2 = '';
	let display1 = (modal_type == 'create') ? '#client_type1' : '#upclient_type1';
	let display2 = (modal_type == 'create') ? '#client_type2' : '#upclient_type2';
	let idname1 = (modal_type == 'create') ? '' : 'up';
	let idname2 = (modal_type == 'create') ? '' : 'up';
	if(type == 'Call'){
		client_type1 = "<label class='m--font-bolder'>Client's Phone Number <span class='m--font-danger'>*</span></label><input type='number' class='form-control m-input m-input--solid input_number' placeholder='Type the client's phone number here' autocomplete='off' name='"+idname1+"client_phone' id='"+idname1+"client_phone' required>";
		client_type2 = "<label class='m--font-bolder'>Comment <span class='m--font-danger'>*</span></label><textarea class='form-control m-input m-input--solid' placeholder='Type the comment here' name='"+idname2+"comment' id='"+idname2+"comment' required style='height: 70px;'> </textarea>";
	}else if(type == 'Email'){
		client_type1 = "<label class='m--font-bolder'>Client's Email Address <span class='m--font-danger'>*</span></label><input type='text' class='form-control m-input m-input--solid input_number' placeholder='Type the client's email address here' autocomplete='off' name='"+idname1+"client_email' id='"+idname1+"client_email' required>";
		client_type2 = "<label class='m--font-bolder'>Screenshot <span class='m--font-danger'>*</span></label><input type='file' class='form-control m-input m-input--solid' placeholder='Paste the screenshot here' autocomplete='off' name='"+idname2+"screenshot' id='"+idname2+"screenshot' required>";
	}else{
		client_type1 = '';
		client_type2 = '';
	}
	$(display1).html(client_type1);
	$(display2).html(client_type2);
}

$('.addNewButton').click(function() {
	$('#create_kudos_modal').modal('show');
});

$('#datatable_div').on('click','.update_modal',function(){
	$('#update_kudos_modal').modal('show');
	let kudos_id = $(this).data('kudos_id');
	$.ajax({
		url:'<?php echo base_url();?>Kudos/kudos_add_modal',
		type:"post",
		async: false,
		data: {kudos_id},
		dataType: 'json',
		success: function(data){
			let image_link = 'http://3.18.221.50/supportzebra/';
			if(data.modal.is_new == 1){
				image_link = '<?php echo base_url()?>'
			}
			$('#upemp_id').html(data.ambassador);
			$('#upemp_id').selectpicker('refresh');
			let image_display = (data.modal.kudos_type == 'Email') ? '<div class="input-group">'+
			'<label for=upimage">'+
			'<img src="'+image_link+data.modal.file+'"/>' : '';
			$('#image_display').html(image_display);
			type_option('update',data.modal.kudos_type);
			$('#upkudos_id').val(kudos_id);
			$('#upacc_id').selectpicker('val',data.modal.acc_id);
			$('#upemp_id').selectpicker('val',data.modal.emp_id);
			$('#upreward_type').selectpicker('val',data.modal.reward_type);
			$('#upkudos_type').selectpicker('val',data.modal.kudos_type);
			$('#upclient_name').val(data.modal.client_name);
			$('#upclient_phone').val(data.modal.phone_number);
			$('#upcomment').val(data.modal.comment);
			$('#upclient_email').val(data.modal.client_email);
			$('#upscreenshot').val(data.modal.file);
		}
	});
});

$('#create_type').change(function() {
	type_option('create',$(this).val());
})

$('#upkudos_type').change(function() {
	type_option('update',$(this).val());
})

$('#create_accounts').change(function(){
	ambassador_option($(this).val(),'#create_employee')
});

$('#upacc_id').change(function(){
	ambassador_option($(this).val(),'#upemp_id')
});


$('#create_kudos').formValidation({
	message: 'This value is not valid',
	excluded: ':disabled',
	feedbackIcons: {
		valid: 'glyphicon glyphicon-ok',
		invalid: 'glyphicon glyphicon-remove',
		validating: 'glyphicon glyphicon-refresh'
	},
}).on('success.form.fv', function (e, data) {
	e.preventDefault();
	let kudos_type = $('#create_type').val();
	let type = 'screenshot';
	$.ajax({
		url:'<?php echo base_url();?>Kudos/create_single',
		type:"post",
		dataType: 'json',
		data:new FormData(this),
		processData:false,
		contentType:false,
		cache:false,
		async:false,
		success: function(res){
			if(res.qry=='Failed'){
				$('#create_kudos_modal').modal('hide');
				$('#offenseDatatable').mDatatable('reload');
				swal(
					'Error Occured!',
					'Please try again later.',
					'error'
					)
			}else{
				if(kudos_type=='Email'){
					var kudosid = res.qry; 
					
					var data = new FormData();
					$.each($("#screenshot")[0].files,function(i,file){
						data.append("imagefile",file);
					});
					data.append("type",type);
					data.append("kudosid",kudosid);
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>Kudos/uploadscreenshot",
						data:data,
						contentType: false,
						cache: false,
						processData:false,
						success: function(res2)
						{
							if(res2.trim()=='Success'){
								$('#create_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Kudos Added',
									'Successfully uploaded and inserted kudos details.',
									'success'
									)
							}else{
								$('#create_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Error Occured!',
									'Please try again later.',
									'error'
									)
							}
						}
					});
				}else{
					$('#create_kudos_modal').modal('hide');
					$('#offenseDatatable').mDatatable('reload');
					$('#create_accounts').selectpicker('val', '');
					$('#create_employee').selectpicker('val', '');
					$('#create_reward').selectpicker('val', '');
					$('#create_type').selectpicker('val', '');
					$('#create_name').val('');
					$('#client_phone').val('');
					$('#comment').val('');
					$('#client_email').val('');
					$('#screenshot').val('');
					$('#client_type1').html('');
					$('#client_type2').html('');
					swal(
						'Kudos Added',
						'Successfully inserted kudos details.',
						'success'
						)
				}
				
				res.notif_id.forEach(function(value){
					systemNotification([value]);
					console.log(value);
				});
			}
		}
	});
});



$('#update_kudos').formValidation({
	message: 'This value is not valid',
	excluded: ':disabled',
	feedbackIcons: {
		valid: 'glyphicon glyphicon-ok',
		invalid: 'glyphicon glyphicon-remove',
		validating: 'glyphicon glyphicon-refresh'
	},
}).on('success.form.fv', function (e, data) {
	e.preventDefault();
	let kudosid = $('#upkudos_id').val();
	let type = 'screenshot';
	let kudos_type = $('#upkudos_type').val();
	$.ajax({
		url:'<?php echo base_url();?>Kudos/update_kudos_data',
		type:"post",
		data:new FormData(this),
		processData:false,
		contentType:false,
		cache:false,
		async:false,
		success: function(res){
			if(kudos_type=='Email'){
				var uploadpic = $("#upscreenshot").val();
				if(uploadpic!=''){
					var data = new FormData();
					$.each($("#upscreenshot")[0].files,function(i,file){
						data.append("imagefile",file);
					});
					data.append("type",type);
					data.append("kudosid",kudosid);
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>Kudos/uploadscreenshot",
						data:data,
						contentType: false,
						cache: false,
						processData:false,
						success: function(res2)
						{
							if(res2.trim()=='Success'){
								$('#update_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Kudos Uploaded!',
									'Successfully Updated Kudos Details.',
									'success'
									)
							}else{
								$('#update_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Error Occured!',
									'Please try again later.',
									'error'
									)
							}
						}
					});
				}else{
					$('#update_kudos_modal').modal('hide');
					$('#offenseDatatable').mDatatable('reload');
					swal(
						'Kudos Updated!',
						'Successfully Updated Kudos Details.',
						'success'
						)
				}
			}else{
				$('#update_kudos_modal').modal('hide');
				$('#offenseDatatable').mDatatable('reload');
				swal(
					'Kudos Updated!',
					'Successfully Updated Kudos Details.',
					'success'
					)
			}
		}
	});
});

</script>

