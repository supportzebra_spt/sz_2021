<style>
	img {
		/*max-width: 100%;
		max-height: 100vh;
		height: auto;*/
		max-width: 50%;
		max-height: 50vh;
		height: auto;
	}
	.input_number::-webkit-outer-spin-button,
	.input_number::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	.input_number {
		-moz-appearance: textfield;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Kudos</h3>           
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Kudos/report3View" class="m-nav__link">
							<span class="m-nav__link-text">Report - Campaign</span>
						</a>
					</li>
				</ul>
			</div>

		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
			<div class="m-portlet__body">
				<div id="chart_0"></div>
				<br>
				<div class="form-group">
					<div>
						<div class="clearfix row">
							<div class="col-sm-4">
								<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" id="addCampaign2" required="true" data-trigger="change" data-dropup-auto="false" data-live-search="true">
									<option selected="selected" value="">Campaign</option>
									<?php 
									foreach($acc as $row)
									{
										echo '<option value="'.$row->acc_id.'">'.$row->account.'</option>';
									}
									?>
								</select>
							</div>
							<div class="col-sm-3">
								<input type="text" name="fromDate" id="fromDate" placeholder="From date..." class="float-left mrg10R form-control m-input m-input--solid datepick" required autocomplete="off">
							</div>
							<div class="col-sm-3">
								<input type="text" name="toDate" id="toDate" placeholder="To date..." class="float-left form-control m-input m-input--solid datepick" required autocomplete="off">
							</div>
							<div class="col-sm-2">
								<button class="btn btn-primary" id="datePicked2" style="background-color: #3498db; border-color: #3498db;">Generate</button>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div>
						<h4 style="text-align: center;"><b>Generate Table for Kudos Count by Campaign</b></h4><br>
						<div class="clearfix row">
							<div class="col-sm-2">
								<button class="btn btn-primary" id="datePicked4" style="background-color: #3498db; border-color: #3498db; width: 100%; display: none;">Generate</button>
							</div>
						</div>
					</div>
				</div>
				<div id="showTable1" style="display: none;">
					<button class="btn btn-success float-left" id="download_report1" style="background-color: #3498db; border-color: #3498db;">Export</button><br>
					<table class="table table-striped" id="table_report1" cellpadding="0" style="width: 100%; height: 100%; font-size: 15px;" >
						<thead>
							<tr>
								<td><b>Month and Year</b></td>
								<td><b>Campaign</b></td>
								<td><b>Kudos Count</b></td>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
		$('.m_selectpicker').selectpicker();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/Templatedesign/headerLeft",
			cache: false,
			success: function(html)
			{
				$('#headerLeft').html(html);
			}
		}); 
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/Templatedesign/sideBar",
			cache: false,
			success: function(html)
			{
				$('#sidebar-menu').html(html);
			}
		});
	}); 
</script>
<script>
	$(function(){
		$('.datepick').datepicker({format: 'yyyy-mm-dd'});
		$("#datePicked2").click(function(){
			var fromDate = $("#fromDate").val();
			var toDate = $("#toDate").val();
			var campaign = $("#addCampaign2").val();
			var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();
			var arr_date1 = fromDate.split('-');
			var arr_date2 = toDate.split('-');
			var months   = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			var month1 = arr_date1[1];
			var month2 = arr_date2[1];
			var day1 = arr_date1[2];
			var day2 = arr_date2[2];
			var m1 = month1.replace(/^0+/, '');
			var m2 = month2.replace(/^0+/, '');
			var d1 = day1.replace(/^0+/, '');
			var d2 = day2.replace(/^0+/, '');
			var fromDate2 = months[m1] + ' ' + d1 + ', ' + arr_date1[0];
			var toDate2 = months[m2] + ' ' + d2 + ', ' + arr_date1[0];
			var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign;
			$.ajax({
				url: '<?php echo base_url(); ?>index.php/Kudos/getDate2',
				type: 'POST',
				dataType: 'json',
				data:  dataString,
				success:function(data)
				{
					var month = data.map(function (value) {
						return value.month;
					});
					var kudos_count = data.map(function (value) {
						return parseInt(value.kudos_count);
					});
					var c = data.map(function (value) {
						return parseInt(value.campaign);
					});
					console.log(month);
					console.log(kudos_count);
					$("#datePicked4").trigger("click");
					chart = Highcharts.chart('chart_0', {
						chart: {
							inverted: true,
							polar: false,
						},
						credits: {
							enabled: false
						},
						title: {
							text: 'Kudos Count '+fromDate2+' to '+toDate2+' of '+campaign_name
						},

						xAxis: {
							categories: month,
						},

						plotOptions: {
							series: {
								dataLabels: {
									enabled: true,
								},
							}
						},
						series: [{
							type: 'column',
							colorByPoint: true,
							data: kudos_count,
							showInLegend: false,
						}],
					});
					Highcharts.setOptions(Highcharts.theme);
				}
			});
		});
		$("#datePicked4").click(function(){
			var fromDate = $("#fromDate").val();
			var toDate = $("#toDate").val();
			var campaign = $("#addCampaign2").val();
			var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();
			var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign+"&campaign_name="+campaign_name;
			$.ajax({
				url: '<?php echo base_url(); ?>index.php/Kudos/getDate4',
				type: 'POST',
				data:  dataString,
				success:function(data)
				{
					$("#table_report1 tbody").html(data);
					$("#showTable1").show();
					console.log(data);
                    }
                });
		});

		$("#download_report1").click(function (e) {
			var fromDate = $("#fromDate").val();
			var toDate = $("#toDate").val();
			var campaign = $("#addCampaign2").val();
			var campaign_name = $("#addCampaign2 option[value='"+campaign+"']").text();
			var dataString = "fromDate="+fromDate+"&toDate="+toDate+"&campaign="+campaign+"&campaign_name="+campaign_name;
			$.ajax({
            url: '<?php echo base_url(); ?>index.php/Kudos/excel_report3',
            type: 'POST',
            data:  dataString,
            beforeSend: function() {
            	mApp.block('#portlet', {
            		overlayColor: '#000000',
            		type: 'loader',
            		state: 'brand',
            		size: 'lg',
            		message: 'Downloading...'
            	});
            },
            xhrFields: {
            	responseType: 'blob'
            },
            success:function(data)
            {
            	var a = document.createElement('a');
            	var url = window.URL.createObjectURL(data);
            	a.href = url;
            	a.download = 'KudosCountCampaign.xlsx';
            	a.click();
            	window.URL.revokeObjectURL(url);
            	mApp.unblock('#portlet');
            }
        });
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {

		chart = Highcharts.chart('chart_0', {
			chart: {
				inverted: true,
				polar: false,
			},
			credits: {
				enabled: false
			},
			title: {
				text: 'Kudos Count by Campaigns from <?php date_default_timezone_set('Asia/Manila'); echo date("F Y");?>'
			},

			xAxis: {
				type: 'category',
			},

			plotOptions: {
				series: {
					dataLabels: {
						enabled: true,
					},
                //colors: ['#3399ff']
            }
        },

        series: [{
        	type: 'column',
        	name: 'Kudos Count',
        	colorByPoint: true,
        	data: [

        	<?php foreach($campaign_kcount as $kc){ ?>
        		{
        			name: '<?php echo ucwords($kc->campaign);?>',
        			y: <?php echo ucwords($kc->kudos_count);?>,
        			drilldown: '<?php echo $kc->campaign;?>'
        		},
        	<?php }?>
        	]
        }],

        drilldown: {
        	series: [
        	<?php foreach($campaign_kcount as $k){ ?>
        		{
        			type: 'column',
        			name: '<?php echo ucwords($k->campaign);?>',
        			id: '<?php echo ucwords($k->campaign);?>',
        			data: [ <?php foreach ($drilldown3 as $d){
        				if($k->campaign==$d->campaign){?>
        					['<?php echo $d->ambassador?>', <?php echo $d->kudos_count?>],
        				<?php }}?>
        				]
        			},
        		<?php }?>
        		]
        	}
        });
	});
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>

