<style>
	img {
		/*max-width: 100%;
		max-height: 100vh;
		height: auto;*/
		max-width: 75%;
		max-height: 75vh;
		height: auto;
	}
	.input_number::-webkit-outer-spin-button,
	.input_number::-webkit-inner-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	.input_number {
		-moz-appearance: textfield;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Kudos</h3>           
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Kudos/kudos_notify" class="m-nav__link">
							<span class="m-nav__link-text">Notification</span>
						</a>
					</li>
				</ul>
			</div>

		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
			<div class="m-portlet__body">
				<div class="m-section" id="datatable_div">
					<div class="m-form m-form--label-align-right m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-8">
										<div style="width: 100%; padding: 5px" class="m-input-icon m-input-icon--left m-input-icon--right">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search for..." id="searchField" autocomplete="off">
											<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="#" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info addNewButton"><i class="fa fa-user-plus"></i> Add New User 
									
								</a>
								<div class="m-separator m-separator--dashed d-xl-none"></div>
							</div>
						</div>
					</div>
					<div id="offenseDatatable"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="create_kudos" method="post">
	<div class="modal fade" id="create_kudos_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="max-width: 600px" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button style="float: right" class="btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<div style="margin: 0 30px">
						<div class="form-group m-form__group row">
							<div style="margin: 20px auto;">
								<h2 style="text-align: center!important;">Kudos Notification</h2>
								<h5 style="text-align: center!important;" class="text-muted">Assign employee as user</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Role <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="role" id="role" title="Select a role here">
										<option data-content="<span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>Request</span>" value="Request">Requestor</option>
										<option data-content="<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>Approve</span>" value="Approve">Approver</option>
										<option data-content="<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>Release</span>" value="Release">Releaser</option>
									</select>
								</div>
							</div>
							<div class="col-8">
								<div class="form-group m-form__group">
									<label class="m--font-bolder">Department <span class="m--font-danger">*</span></label>
									<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="department" id="department" title="Select a department here" data-live-search="true">

									</select>
								</div>
							</div>
						</div>
						<div class="form-group m-form__group">
							<label class="m--font-bolder">Employee <span class="m--font-danger">*</span></label>
							<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="user" id="user" title="Select a user here">

							</select>
						</div>
						<div style="bottom: 0;" class="form-group m-form__group row">
							<div style="margin: 10px auto;">
								<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-info">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	var offenseDatatable = function () {
		var offense = function (searchVal = "") {
			var options = {
				data: {
					type: 'remote',
					source: {
						read: {
							method: 'POST',
							url: "<?php echo base_url(); ?>Kudos/kudos_notify_data",
							params: {
								query: {
									searchField: searchVal,
								},
							},
							map: function (raw) {
								var dataSet = raw;
								if (typeof raw.data !== 'undefined') {
									dataSet = raw.data;
								}
								dataCount = dataSet.length;
								return dataSet;
							}
						}
					},
					saveState: {
						cookie: false,
						webstorage: false
					},
					pageSize: 5,
					serverPaging: true,
					serverFiltering: true,
					serverSorting: true,
				},
				layout: {
					theme: 'default',
					class: '',
					scroll: true,
					footer: false

				},
				sortable: true,
				pagination: true,
				toolbar: {
					placement: ['bottom'],
					items: {
						pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
            	input: $('#searchField'),
            },
            columns: [
            {
            	field: "emp_id",
            	title: "Name",
            	width: 200,
            	selector: false,
            	sortable: true,
            	textAlign: 'left',
            	template: function (row, index, datatable) {
            		return row.lname+', '+row.fname;
            	},
            }, 
            {
            	field: 'role',
            	width: 90,
            	title: 'Role',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.role == 'Approve'){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Approver</span>';
            		} else if(row.role == 'Release'){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">Releaser</span>';
            		} else if(row.role == 'Request'){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Requestor</span>';
            		}
            		return status;
            	},
            },  
            {
            	field: 'dep_name',
            	width: 200,
            	title: 'Department',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		return row.dep_name+' - '+row.dep_details;
            	},
            },  
            {
            	field: 'pos_details',
            	width: 200,
            	title: 'Position',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            },  
            {
            	field: 'status',
            	width: 90,
            	title: 'Status',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            },  
            {
            	field: "isActive",
            	width: 70,
            	title: "Actions",
            	textAlign: 'center',
            	sortable: false,
            	overflow: 'visible',
            	template: function (row, index, datatable) {
            		var status = '';
            		if(row.isActive == 1){
            			status = '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">Deactivate</span>';
            		} else{
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">Activate</span>';
            		} 
            		return '<a style="text-decoration: none;" href="#" data-ksetting_id="'+row.ksetting_id+'" data-isActive="'+row.isActive+'" class="action">'+status+'</a>';
            	},
            }
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
    	init: function (searchVal) {
    		offense(searchVal);
    	}
    };
}();

function initOffenses(){
	$('#offenseDatatable').mDatatable('destroy');
	offenseDatatable.init();
}

$(function(){
	$('.m_selectpicker').selectpicker();
	department_option();
	initOffenses();
})

function department_option(){
	$.ajax({
		url:'<?php echo base_url();?>Kudos/notification_department',
		type:"post",
		async: false,
		dataType: 'json',
		success: function(data){
			$('#department').html(data);
			$('#department').selectpicker('refresh');
		}
	});
}

function ambassador_option(acc_id,display){
	$.ajax({
		url:'<?php echo base_url();?>Kudos/Kudos_add_ambassador',
		type:"post",
		async: false,
		data: {acc_id},
		dataType: 'json',
		success: function(data){
			$(display).html(data);
			$(display).selectpicker('refresh');
		}
	});
}

function type_option(modal_type,type){
	console.log(modal_type);
	console.log(type);
	let client_type1 = '';
	let client_type2 = '';
	let display1 = (modal_type == 'create') ? '#client_type1' : '#upclient_type1';
	let display2 = (modal_type == 'create') ? '#client_type2' : '#upclient_type2';
	let idname1 = (modal_type == 'create') ? '' : 'up';
	let idname2 = (modal_type == 'create') ? '' : 'up';
	if(type == 'Call'){
		client_type1 = "<label class='m--font-bolder'>Client's Phone Number <span class='m--font-danger'>*</span></label><input type='number' class='form-control m-input m-input--solid input_number' placeholder='Type the client's phone number here' autocomplete='off' name='"+idname1+"client_phone' id='"+idname1+"client_phone' required>";
		client_type2 = "<label class='m--font-bolder'>Comment <span class='m--font-danger'>*</span></label><textarea class='form-control m-input m-input--solid' placeholder='Type the comment here' name='"+idname2+"comment' id='"+idname2+"comment' required style='height: 70px;'> </textarea>";
	}else if(type == 'Email'){
		client_type1 = "<label class='m--font-bolder'>Client's Email Address <span class='m--font-danger'>*</span></label><input type='text' class='form-control m-input m-input--solid input_number' placeholder='Type the client's email address here' autocomplete='off' name='"+idname1+"client_email' id='"+idname1+"client_email' required>";
		client_type2 = "<label class='m--font-bolder'>Screenshot <span class='m--font-danger'>*</span></label><input type='file' class='form-control m-input m-input--solid' placeholder='Paste the screenshot here' autocomplete='off' name='"+idname2+"screenshot' id='"+idname2+"screenshot' required>";
	}else{
		client_type1 = '';
		client_type2 = '';
	}
	$(display1).html(client_type1);
	$(display2).html(client_type2);
}

$('.addNewButton').click(function() {
	$('#create_kudos_modal').modal('show');
});

$('.filter').click(function() {
	$('#modal_filter').modal('show');
});

$('#datatable_div').on('click','.update_modal',function(){
	$('#update_kudos_modal').modal('show');
	let kudos_id = $(this).data('kudos_id');
	$.ajax({
		url:'<?php echo base_url();?>Kudos/kudos_add_modal',
		type:"post",
		async: false,
		data: {kudos_id},
		dataType: 'json',
		success: function(data){
			$('#upemp_id').html(data.ambassador);
			$('#upemp_id').selectpicker('refresh');
			let image_display = (data.modal.kudos_type == 'Email') ? '<div class="input-group">'+
			'<label for=upimage">'+
			'<img src="<?php echo base_url()?>'+data.modal.file+'"/>' : '';
			$('#image_display').html(image_display);
			type_option('update',data.modal.kudos_type);
			$('#upkudos_id').val(kudos_id);
			$('#upacc_id').selectpicker('val',data.modal.acc_id);
			$('#upemp_id').selectpicker('val',data.modal.emp_id);
			$('#upreward_type').selectpicker('val',data.modal.reward_type);
			$('#upkudos_type').selectpicker('val',data.modal.kudos_type);
			$('#upclient_name').val(data.modal.client_name);
			$('#upclient_phone').val(data.modal.phone_number);
			$('#upcomment').val(data.modal.comment);
			$('#upclient_email').val(data.modal.client_email);
			$('#upscreenshot').val(data.modal.file);
		}
	});
	// let acc_id = $(this).data('acc_id');
	// let emp_id = $(this).data('emp_id');
	// let reward_type = $(this).data('reward_type');
	// let kudos_type = $(this).data('kudos_type');
	// let client_name = $(this).data('client_name');
	// let client_phone = $(this).data('client_phone');
	// let comment = $(this).data('comment');
	// let client_email = $(this).data('client_email');
	// let file = $(this).data('file');
	// console.log(file);
	
	// $.ajax({
	// 	url:'<?php echo base_url();?>Kudos/Kudos_add_ambassador',
	// 	type:"post",
	// 	async: false,
	// 	data: {acc_id},
	// 	dataType: 'json',
	// 	success: function(data){
	// 		$('#upemp_id').html(data);
	// 		$('#upemp_id').selectpicker('refresh');
	// 	}
	// });
	
});

$('#datatable_div').on('click','.action',function(){
	let isActive = ($(this).data('isActive') == 1) ? 'Activate' : 'Deactivate';
	let ksetting_id = $(this).data('ksetting_id');
	swal({
		title: 'Are you sure?',
		text: "You want to "+isActive+" this user?",
		type: 'info',
		showCancelButton: true,
		confirmButtonText: 'Yes, '+isActive+' it!'
	}).then(function(result) {
		if (result.value) {
			$.ajax({
				url:'<?php echo base_url();?>Kudos/update_isActive',
				type:"post",
				async: false,
				data: {isActive,ksetting_id},
				dataType: 'json',
				success: function(data){
					swal(
						'Success!',
						'Your file has been updated.',
						'success'
						)
					$('#offenseDatatable').mDatatable('reload');
				}
			});
			
		}
	});
});


$('#department').change(function(e) {
	e.preventDefault();
	let dep_id = $('#department').val();
	let role = $('#role').val();
	if(role == ''){
		alert('Please select a role first!');
		$('#department').selectpicker('val',"");
	}else {
		add_user(role,dep_id);
	}
})

$('#role').change(function(e) {
	e.preventDefault();
	let dep_id = $('#department').val();
	let role = $('#role').val();
	if(department != ''){
		add_user(role,dep_id);
	}
})

function add_user(role,dep_id){
	$.ajax({
		url:'<?php echo base_url();?>Kudos/notification_user',
		type:"post",
		async: false,
		data: {dep_id,role},
		dataType: 'json',
		success: function(data){
			$('#user').html(data);
			$('#user').selectpicker('refresh');
		}
	});
}


$('#create_kudos').formValidation({
	message: 'This value is not valid',
	excluded: ':disabled',
	feedbackIcons: {
		valid: 'glyphicon glyphicon-ok',
		invalid: 'glyphicon glyphicon-remove',
		validating: 'glyphicon glyphicon-refresh'
	},
}).on('success.form.fv', function (e, data) {
	e.preventDefault();
	let kudos_type = $('#create_type').val();
	let type = 'screenshot';
	$.ajax({
		url:'<?php echo base_url();?>Kudos/add_new_user',
		type:"post",
		data:new FormData(this),
		processData:false,
		contentType:false,
		cache:false,
		async:false,
		success: function(res){
			res = res.trim();
			if(res=='Failed'){
				$('#create_kudos_modal').modal('hide');
				$('#offenseDatatable').mDatatable('reload');
				swal(
					'Error Occured!',
					'Please try again later.',
					'error'
					)
			}else{
				if(kudos_type=='Email'){
					var kudosid = res;
					
					var data = new FormData();
					$.each($("#screenshot")[0].files,function(i,file){
						data.append("imagefile",file);
					});
					data.append("type",type);
					data.append("kudosid",kudosid);
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>Kudos/uploadscreenshot",
						data:data,
						contentType: false,
						cache: false,
						processData:false,
						success: function(res2)
						{
							if(res2.trim()=='Success'){
								$('#create_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Kudos Added',
									'Successfully uploaded and inserted kudos details.',
									'success'
									)
							}else{
								$('#create_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Error Occured!',
									'Please try again later.',
									'error'
									)
							}
						}
					});
				}else{
					$('#create_kudos_modal').modal('hide');
					$('#offenseDatatable').mDatatable('reload');
					swal(
						'Kudos Added',
						'Successfully inserted kudos details.',
						'success'
						)
				}

			}
		}
	});
});

$('#update_kudos').formValidation({
	message: 'This value is not valid',
	excluded: ':disabled',
	feedbackIcons: {
		valid: 'glyphicon glyphicon-ok',
		invalid: 'glyphicon glyphicon-remove',
		validating: 'glyphicon glyphicon-refresh'
	},
}).on('success.form.fv', function (e, data) {
	e.preventDefault();
	let kudosid = $('#upkudos_id').val();
	let type = 'screenshot';
	let kudos_type = $('#upkudos_type').val();
	$.ajax({
		url:'<?php echo base_url();?>Kudos/update_kudos_data',
		type:"post",
		data:new FormData(this),
		processData:false,
		contentType:false,
		cache:false,
		async:false,
		success: function(res){
			if(kudos_type=='Email'){
				var uploadpic = $("#upscreenshot").val();
				if(uploadpic!=''){
					var data = new FormData();
					$.each($("#upscreenshot")[0].files,function(i,file){
						data.append("imagefile",file);
					});
					data.append("type",type);
					data.append("kudosid",kudosid);
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>Kudos/uploadscreenshot",
						data:data,
						contentType: false,
						cache: false,
						processData:false,
						success: function(res2)
						{
							if(res2.trim()=='Success'){
								$('#update_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Kudos Uploaded!',
									'Successfully Updated Kudos Details.',
									'success'
									)
							}else{
								$('#update_kudos_modal').modal('hide');
								$('#offenseDatatable').mDatatable('reload');
								swal(
									'Error Occured!',
									'Please try again later.',
									'error'
									)
							}
						}
					});
				}
			}else{
				$('#update_kudos_modal').modal('hide');
				$('#offenseDatatable').mDatatable('reload');
				swal(
					'Kudos Updated!',
					'Successfully Updated Kudos Details.',
					'success'
					)
			}
		}
	});
});

</script>

</body>
</html>