<style type="text/css">
    pre.announcementPre {
        overflow-x: auto;
        white-space: pre-wrap;
        white-space: -moz-pre-wrap;
        white-space: -pre-wrap;
        white-space: -o-pre-wrap;
        word-wrap: break-word;
        font-family: Poppins;
        font-size: 14px;
    }

    .alert-announcement {
        border-left: none !important;
        border-top: none !important;
        border-bottom: none !important;
        border-right-style: solid !important;
        border-right-width: 5px !important;
        border-radius: 0 !important;
        color: unset;
    }
    .announcement-close{
        z-index:2000;
        font-size:30px;
        position:absolute;
        top:1px;
        right:16px;
    }
    .announcement-close:hover{
        text-shadow: 1px 1px 1px gray;
    }
    .announcement-close:focus{
        outline:none !Important;
    }
    #card-container td {
        font-size: 13px;
    }

    .note-toolbar-wrapper {
        height: unset !Important;
    }
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Announcement</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url() ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">My Announcement</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <div class="m-portlet m-portlet--rounded">
            <div class="m-portlet__head" style="background:#2c2e3eeb;color:white">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text" style="font-family:Poppins;color:white !important">
                            My Announcements
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--pill" id="btn-modal-insert" data-toggle="modal" data-target="#modal-announcement" data-type="insert" data-title="Create Announcement">
                                <i class="flaticon-plus"></i> Create Announcement
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row mb-3">

                    <div class="col-md-5">
                        <div class="input-group">
                            <input type="text" class="form-control m-input" placeholder="Search Announcement Subject" aria-describedby="basic-addon2" id="search-subject" name="search-subject">
                            <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Search Announcement Subject"><span class="input-group-text"><i class="la la-search"></i></span></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <select class="custom-select form-control" id="search-highlight" data-toggle="m-tooltip" title="" data-original-title="Hightlight Type">
                            <option value="">All</option>
                            <option value="important">Important</option>
                            <option value="info">Info</option>
                            <option value="priority">Priority</option>
                            <option value="urgent">Urgent</option>
                            <option value="warning">Warning</option>
                        </select>
                    </div>
                </div>
                <table class="table table-bordered table-hover table-striped m-table m-table--head-bg-success" id="card-container">
                    <thead>
                        <th>ID</th>
                        <th>Subject</th>
                        <th class="text-center">Highlight</th>
                        <th class="text-center">Start Date</th>
                        <th class="text-center">End Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Actions</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="row no-gutters">
                    <div class="col-md-9 col-12">
                        <ul id="pagination">
                        </ul>
                    </div>
                    <div class="col-md-1 text-right col-4">
                        <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                            <option>10</option>
                            <option>25</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                    <div class="col-md-2 text-right col-8">
                        <p style="font-family:Poppins;font-size:13px;text-align:right">Showing <span id="count"></span> of <span id="total"></span> records</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-announcement" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-announcement-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-12"><label class="m--font-bold" style="font-size:12px" for="">Subject:</label></div>
                            <div class="col-12"><input type="text" class="form-control m-input" id="announcement_title"></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12"><label class="m--font-bold" style="font-size:12px" for="">Message:</label></div>
                            <div class="col-12">
                                <div id="announcement_summernote"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-12"><label class="m--font-bold" style="font-size:12px" for="">Duration:</label></div>
                            <div class="col-12">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="announcement_daterange" style="font-size:12px">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12"><label class="m--font-bold" style="font-size:12px" for="">Highlight Type:</label></div>
                            <div class="col-12">
                                <select class="form-control m-bootstrap-select m_selectpicker" id="announcement_hightlight">
                                    <option value="important" style="color:#7BC043" data-content="<span class='m-badge m-badge--wide  text-capitalize m--font-bolder' style='background:#7BC043;color:white'>Important</span>" selected>Important</option>
                                    <option value="info" style="color:#0392CF" data-content="<span class='m-badge m-badge--wide  text-capitalize m--font-bolder' style='background:#0392CF;color:white'>Info</span>">Info</option>
                                    <option value="priority" style="color:#716ACA" data-content="<span class='m-badge m-badge--wide  text-capitalize m--font-bolder' style='background:#716ACA;color:white'>Priority</span>">Priority</option>
                                    <option value="urgent" style="color:#E6558B" data-content="<span class='m-badge m-badge--wide  text-capitalize m--font-bolder' style='background:#E6558B;color:white'>Urgent</span>">Urgent</option>
                                    <option value="warning" style="color:#F7AB42" data-content="<span class='m-badge m-badge--wide  text-capitalize m--font-bolder' style='background:#F7AB42;color:white'>Warning</span>">Warning</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12"><label class="m--font-bold" style="font-size:12px" for="">Announcer Type:</label></div>
                            <div class="col-12">
                                <select class="form-control m-bootstrap-select m_selectpicker" id="announcement_announcer">
                                    <option value="account" selected>Account</option>
                                    <option value="department">Department</option>
                                    <option value="individual">Individual</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12"><label class="m--font-bold" style="font-size:12px" for="">Group Display:</label></div>
                            <div class="col-12">
                                <select class="form-control m-bootstrap-select m_selectpicker" data-selected-text-format="count" data-count-selected-text="Selected Account ({0})" data-container="false" data-live-search="true" multiple data-actions-box="true" title="Account Lists" id="list_of_acc_id">

                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12"><label class="m--font-bold" style="font-size:12px" for="">Individual Display:</label></div>
                            <div class="col-12">
                                <select class="form-control m-bootstrap-select m_selectpicker" data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})" data-container="false" data-live-search="true" multiple data-actions-box="true" title="Employee Lists" id="list_of_emp_id">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success m-btn m-btn--icon" id="btn-insert" style="display:none">
                    <span>
                        <i class="la la-plus-circle"></i>
                        <span>Create Announcement</span>
                    </span>
                </button>
                <button type="button" class="btn btn-brand m-btn m-btn--icon" id="btn-update" style="display:none">
                    <span>
                        <i class="la la-edit"></i>
                        <span>Save Changes</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-preview-announcement" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div style="width: auto" class="modal-content">
            <div class="modal-body" style="padding: 7px !important;">

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!-- <div class="modal fade" id="modal-attachment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Announcement Attachments</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>

<script type="text/javascript">
    function tz_date(thedate, format = null) {
        var tz = moment.tz(new Date(thedate), "Asia/Manila");
        return (format === null) ? moment(tz) : moment(tz).format(format);
    }

    function uploadImageGeneral(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajax({
            url: baseUrl+"/Announcement/general_upload_photo",
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "POST",
            beforeSend: function(){
                mApp.block('.m-content', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'success',
                    message: 'Please wait...'
                });
            },
            success: function(url) {
                var image = $('<img style="max-width: 100% !important" class="myImg" src="'+url+'" />');
                $('#announcement_summernote').summernote("insertNode", image[0]);
                // details_update();
                mApp.unblock('.m-content');
            },
            error: function(data) {
                mApp.unblock('.m-content');
                // console.log(data);
            }
        });
    }

    function deleteImageGeneral(src) {
        $.ajax({
            data: {src : src},
            type: "POST",
            url: baseUrl+"/Announcement/delete_image",
            cache: false,
            success: function(response) {
                // console.log(response);
                // details_update();
            }
        });
    }

    function capitalizeTheFirstLetterOfEachWord(words) {
        var separateWord = words.toLowerCase().split(' ');
        for (var i = 0; i < separateWord.length; i++) {
            separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
            separateWord[i].substring(1);
        }
        return separateWord.join(' ');
    }

    function group_display(check = '') {
        $.ajax({
            type: 'post',
            url: '<?php echo base_url(); ?>Announcement/group_display',
            async: false,
            dataType: 'json',
            data: {check},
            success: function(data) {
                let html = '';
                data.forEach(function(value){
                    let select = (value.checked == 1) ? 'selected' : '';
                    html += '<option '+select+' value="'+value.acc_id+'">'+value.acc_name+'</option>'
                })
                $('#list_of_acc_id').html(html);
                $('#list_of_acc_id').selectpicker('refresh');
            }
        });
    }

    function individual_display(check = '') {
        $.ajax({
            type: 'post',
            url: '<?php echo base_url(); ?>Announcement/individual_display',
            async: false,
            dataType: 'json',
            data: {check},
            success: function(data) {
                let html = '';
                data.forEach(function(value){
                    let select = (value.checked == 1) ? 'selected' : '';
                    let lname = (value.lname) ? capitalizeTheFirstLetterOfEachWord(value.lname) : '';
                    let fname = (value.fname) ? capitalizeTheFirstLetterOfEachWord(value.fname) : '';
                    let mname = (value.mname) ? capitalizeTheFirstLetterOfEachWord(value.mname) : '';
                    html += '<option '+select+' value="'+value.emp_id+'">'+lname+', '+fname+' '+mname+'</option>'
                })
                $('#list_of_emp_id').html(html);
                $('#list_of_emp_id').selectpicker('refresh');
            }
        });
    }

    function getMyAnnouncements(limiter, perPage, callback) {
        var searchSubject = $("#search-subject").val();
        var searchHighlight = $("#search-highlight").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Announcement/get_my_announcements",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchSubject: searchSubject,
                searchHighlight: searchHighlight
            },
            beforeSend: function() {
                mApp.block('#content', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#total").html(res.total);
                $("#total").data('count', res.total);
                $("#card-container tbody").html('');

                $.each(res.announcements, function(key, ann) {
                    var string = '';
                    var compareDate = currentdate = tz_date(moment());
                    var startDate = moment(ann.startDate);
                    var endDate = moment(ann.endDate);
                    if (compareDate.isBetween(startDate, endDate, 'days', '[]')) { //inclusivity
                        var status = "table-success";
                    } else {
                        var status = "";
                    }
                    var highlightColor = (ann.highlightType == 'important') ? "#7BC043" : (ann.highlightType == 'info') ? "#0392CF" : (ann.highlightType == 'priority') ? "#716ACA" : (ann.highlightType == 'urgent') ? "#E6558B" : "#F7AB42";
                    var highlight = '<span class="m-badge m-badge--wide  text-capitalize m--font-bolder" style="background:' + highlightColor + ';color:white">' + ann.highlightType + '</span>';
                    var checked = (ann.isEnabled === "1") ? 'checked="checked"' : "";
                    string += '<tr class="' + status + '" data-id="' + ann.announcement_ID + '">\
                    <td class="m--font-bolder">' + ann.announcement_ID + '</td>\
                    <td class="m--font-bolder">' + ann.subject + '<br><small>By: <i>' + ann.announcer + '</i></small></td>\
                    <td class="m--font-bold text-center">' + highlight + '</td>\
                    <td class="text-center m--font-bolder">' + moment(ann.startDate).format('MMM DD, YYYY') + '</td>\
                    <td class="text-center m--font-bolder">' + moment(ann.endDate).format('MMM DD, YYYY') + '</td>\
                    <td class="text-center"><span class="m-switch m-switch--success m-switch--sm m-switch--icon"><label><input type="checkbox" ' + checked + ' class="table-switch" name="" data-updateid="' + ann.announcement_ID + '"><span></span></label></span></td>\
                    <td class="text-center">\
                    <a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="Update details" data-toggle="modal" data-target="#modal-announcement" data-type="update" data-updateid="' + ann.announcement_ID + '" data-title="Update Announcement"><i class="la la-edit" style="color:#F7AB42"></i></a>\
                    <a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air btn-delete-announcement" title="Delete Announcement"  data-deleteid="' + ann.announcement_ID + '"><i class="la la-trash" style="color:#E6558B"></i></a>\
                    <a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="Preview announcement" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modal-preview-announcement" data-previewid="' + ann.announcement_ID + '"><i class="la la-eye" style="color:#0392CF"></i></a>\
                    </td>\
                    </tr>';
                    //<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="Preview announcement" data-toggle="modal" data-target="#modal-preview" data-obj=\'' + JSON.stringify(ann) + '\'><i class="la la-eye"></i></a>\
                    //<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Attachments" data-toggle="modal" data-target="#modal-attachment" data-obj=\'' + JSON.stringify(ann) + '\'><i class="la la-paperclip"></i></a>\
                    $("#card-container tbody").append(string);
                    $("#card-container").find('a[data-updateid="' + ann.announcement_ID + '"]').data('obj', ann);
                    $("#card-container").find('a[data-previewid="' + ann.announcement_ID + '"]').data('obj', ann);
                });

                var count = $("#total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#count").html(1 + ' - ' + perPage);
                } else {
                    $("#count").html((limiter + 1) + ' - ' + result);
                }

                mApp.unblock('#content');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#content');
            }
        });
}
$(function() {
    $('.m_selectpicker').selectpicker();
    $("#perpage").change();
    var currentdate = tz_date(moment());
    var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
    var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
    $('#announcement_daterange').val(start + ' - ' + end);
    $('#announcement_daterange').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        opens: "left",
        minDate: currentdate,
        ranges: {
            'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
            'This Week': [moment(currentdate, 'YYYY-MM-DD').startOf('week'), moment(currentdate, 'YYYY-MM-DD').endOf('week')],
            'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate, 'YYYY-MM-DD').endOf('month')]
        }
    }, function(start, end, label) {
        $('#announcement_daterange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
    });
    var max = 500;
    $('#announcement_summernote').summernote({
        // dialogsInBody: true,
        dialogsFade: true,
        toolbar: 
        [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen', /*'codeview'*/]],
        ],
        height: 150,
        callbacks: {
            onImageUpload: function(image) {
                uploadImageGeneral(image[0]);
            },
            onMediaDelete : function(target) {
                deleteImageGeneral(target[0].src);
            },
            onChange: function(e) {
                setTimeout(function(){
                    // details_update();
                },200);
            }
        }
    });
        // $('#announcement_summernote').siblings('.note-editor').find('.note-view,.note-table,.note-fontname').hide();
        $('#announcement_summernote').siblings('.note-editor').find('button[data-original-title="Video"]').hide();
        // $('#announcement_summernote').siblings('.note-editor').find('button[data-original-title="Picture"]').hide();
        $("#announcement_hightlight").change();
        group_display();
        individual_display();
    });

$("#search-subject,#search-highlight").change(function() {
    $("#perpage").change();
});
$("#perpage").change(function() {
    var perPage = $("#perpage").val();
    getMyAnnouncements(0, perPage, function(total) {
        $("#pagination").pagination('destroy');
        $("#pagination").pagination({
                items: total, //default
                itemsOnPage: $("#perpage").val(),
                hrefTextPrefix: "#",
                cssStyle: 'light-theme',
                displayedPages: 10,
                onPageClick: function(pagenumber) {
                    var perPage = $("#perpage").val();
                    getMyAnnouncements((pagenumber * perPage) - perPage, perPage, function() {});
                }
            });
    });
});
$("#btn-previous").click(function() {
    $("#pagination").pagination('prevPage');
})
$("#btn-next").click(function() {
    $("#pagination").pagination('nextPage');
});
$("#modal-announcement").on("shown.bs.modal", function(e) {
    $("body").css('padding-right', '0px');
})
$("#modal-announcement").on("show.bs.modal", function(e) {
    
    if (e.namespace === 'bs.modal' && $(e.target).attr('id')=='modal-announcement') {
        $("#modal-announcement-title").html($(e.relatedTarget).data('title'));
        if ($(e.relatedTarget).data('type') == 'update') {
            var obj = $(e.relatedTarget).data('obj');
            console.log(obj);
                //-----------------------UPDATE AREA----------------------
                $("#announcement_summernote").summernote('code', obj.message);
                $("#announcement_title").val(obj.subject);
                $("#announcement_daterange").data('daterangepicker').setStartDate(moment(obj.startDate));
                $("#announcement_daterange").data('daterangepicker').setEndDate(moment(obj.endDate));
                $("#announcement_hightlight").val(obj.highlightType);
                $("#announcement_announcer").val(obj.announcerType);
                group_display(obj.list_of_acc_id);
                individual_display(obj.list_of_emp_id);
                //-----------------------UPDATE AREA----------------------
                $("#btn-update").data('announcement_id', obj.announcement_ID);
                $("#btn-update").show();
                $("#btn-insert").hide();
            } else {
                $("#btn-insert").show();
                $("#btn-update").hide();
            }
            $("#announcement_hightlight").change();
            $(".note-toolbar-wrapper").css('height', '')
        }
    });

$("#btn-insert").click(function() {
    var message = $("#announcement_summernote").summernote('code');
    var title = $("#announcement_title").val();
    var durationstart = $("#announcement_daterange").data('daterangepicker').startDate.format('YYYY-MM-DD');
    var durationend = $("#announcement_daterange").data('daterangepicker').endDate.format('YYYY-MM-DD');
    var highlight = $("#announcement_hightlight").val();
    var announcer = $("#announcement_announcer").val();
    var list_of_emp_id = $("#list_of_emp_id").val();
    var list_of_acc_id = $("#list_of_acc_id").val();
    if (message === '' || title === '' || highlight === '' || announcer === '') {
        swal("Missing Fields", "Fill out the form completely to proceed", "error");
    } else {
        swal({
            title: 'Are you sure?',
            html: "Continue creating announcement.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn btn-success m-btn m-btn--air",
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Announcement/create_new_announcement",
                    data: {
                        message,
                        title,
                        durationstart,
                        durationend,
                        highlight,
                        announcer,
                        list_of_emp_id: list_of_emp_id.toString(),
                        list_of_acc_id: list_of_acc_id.toString()
                    },
                    cache: false,
                    success: function(res) {
                        res = JSON.parse(res.trim());
                        if (res.status === "Success") {
                            swal("Success", "Successfully created announcement.", "success");
                            $("#perpage").change();
                        } else {
                            swal("Something's wrong", "An error occurred while processing your request.", "error");
                        }
                        $("#modal-announcement").modal('hide');
                        $("#announcement_summernote").summernote('code', '');
                        $("#announcement_title").val('');
                        $("#announcement_hightlight").val('');
                        $("#announcement_announcer").val('');
                    }
                });
            }
        });
    }

});
$("#btn-update").click(function() {
    var announcement_ID = $("#btn-update").data('announcement_id');
    var message = $("#announcement_summernote").summernote('code');
    var title = $("#announcement_title").val();
    var durationstart = $("#announcement_daterange").data('daterangepicker').startDate.format('YYYY-MM-DD');
    var durationend = $("#announcement_daterange").data('daterangepicker').endDate.format('YYYY-MM-DD');
    var highlight = $("#announcement_hightlight").val();
    var announcer = $("#announcement_announcer").val();
    var list_of_emp_id = $("#list_of_emp_id").val();
    var list_of_acc_id = $("#list_of_acc_id").val();
    if (message === '' || title === '' || highlight === '' || announcer === '') {
        swal("Missing Fields", "Fill out the form completely to proceed", "error");
    } else {
        swal({
            title: 'Are you sure?',
            html: "Continue updating announcement.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn btn-warning m-btn m-btn--air",
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Announcement/update_announcement",
                    data: {
                        announcement_ID,
                        message,
                        title,
                        durationstart,
                        durationend,
                        highlight,
                        announcer,
                        list_of_emp_id: list_of_emp_id.toString(),
                        list_of_acc_id: list_of_acc_id.toString()
                    },
                    cache: false,
                    success: function(res) {
                        res = JSON.parse(res.trim());
                        if (res.status === "Success") {
                            swal("Success", "Successfully updated announcement.", "success");
                            $("#perpage").change();
                        } else {
                            swal("Something's wrong", "An error occurred while processing your request.", "error");
                        }
                        $("#modal-announcement").modal('hide');
                        $("#announcement_summernote").summernote('code', '');
                        $("#announcement_title").val('');
                        $("#announcement_hightlight").val('');
                        $("#announcement_announcer").val('');
                    }
                });
            }
        });
    }
});
$("#announcement_hightlight").change(function() {
    var color = $(this).children(":selected").css("color");
    $("#announcement_hightlight").css('color', color);
})
$("#modal-preview-announcement").on('show.bs.modal', function(e) {
    if (e.namespace === 'bs.modal') {
        var ann = $(e.relatedTarget).data('obj');
        var highlightColor = (ann.highlightType == 'important') ? "#7BC043" : (ann.highlightType == 'info') ? "#0392CF" : (ann.highlightType == 'priority') ? "#716ACA" : (ann.highlightType == 'urgent') ? "#E6558B" : "#F7AB42";
        var highlightIcon = (ann.highlightType == 'important') ? "flaticon-confetti" : (ann.highlightType == 'info') ? "flaticon-information" : (ann.highlightType == 'priority') ? "flaticon-notes" : (ann.highlightType == 'urgent') ? "flaticon-alarm-1" : "flaticon-exclamation-1";

        var string = '<button type="button" class="close announcement-close" style="color:' + highlightColor + '" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><div class="m-0 m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-dismissible fade show alert-announcement" role="alert"  style="border-right-color:' + highlightColor + ' !important">' +
        '<div class="m-alert__icon"  style="background:' + highlightColor + '">' +
        '<i class="' + highlightIcon + '" style="color:white"></i><p class="mt-3 m--font-bolder text-capitalize" style="color:white;font-size:12px;width:55px">' + ann.highlightType + '</p>' +
        '<span style="border-left-color:' + highlightColor + '"></span>' +
        '</div>' +
        '<div class="m-alert__text">' +
        '<h5 style="color:' + highlightColor + ';font-size:16px;" class="m--font-boldest mb-1">' + ann.subject + '</h5><p class="mb-3 mt-1"><span style="color:gray;font-size:12px;" class="m--font-bolder">' + moment(ann.startDate).format('MMMM DD, YYYY') + '&nbsp; | &nbsp;By: &nbsp;' + ann.announcer + '</span></p> <pre class="announcementPre">' +
        ann.message + '</pre></div></div>'
        '</div>' +
        '</div>';
        $("#modal-preview-announcement").find('.modal-body').html(string);
    }
});
$("#card-container").on('click', '.btn-delete-announcement', function() {
    var announcement_ID = $(this).data('deleteid');
    swal({
        title: 'Are you sure?',
        html: "Continue deleting announcement.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: "btn btn-danger m-btn m-btn--air",
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Announcement/delete_announcement",
                data: {
                    announcement_ID: announcement_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === "Success") {
                        swal("Success", "Successfully deleted announcement.", "success");
                        $("#perpage").change();
                    } else {
                        swal("Something's wrong", "An error occurred while processing your request.", "error");
                    }
                }
            });
        }
    });
})
$("#card-container").on("change", ".table-switch", function() {
    var isEnabled = $(this).is(':checked');
    var announcement_ID = $(this).data('updateid');
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Announcement/change_announcement_status",
        data: {
            announcement_ID: announcement_ID,
            isEnabled: isEnabled
        },
        cache: false,
        success: function(res) {
            res = JSON.parse(res.trim());
            if (res.status === "Success") {
                $.notify({
                    message: 'Successfully changed status.'
                }, {
                    type: 'success',
                    timer: 1000
                });
            } else {
                $.notify({
                    message: 'An error occured while changing status.'
                }, {
                    type: 'danger',
                    timer: 1000
                });
            }
        }
    });
});
</script>