<style type="text/css">
    pre.announcementPre {
        overflow-x: auto;
        white-space: pre-wrap;
        white-space: -moz-pre-wrap;
        white-space: -pre-wrap;
        white-space: -o-pre-wrap;
        word-wrap: break-word;
        font-family: Poppins;
        font-size: 14px;
        margin-bottom: 0 !important;
    }

    #card-container td {
        font-size: 13px;
    }

    .note-toolbar-wrapper {
        height: unset !Important;
    }

    .alert-announcement {
        border-left: none !important;
        border-top: none !important;
        border-bottom: none !important;
        border-right-style: solid !important;
        border-right-width: 5px !important;
        border-radius: 0 !important;
        color: unset;
    }
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content ">
        <div class="row mb-4">
            <div class="col-md-5">
                <h4 class="pt-1">SupportZebra Announcements</h4>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-5">
                        <select class="custom-select form-control" id="search-highlight" data-toggle="m-tooltip" title="" data-original-title="Hightlight Type">
                            <option value="">All</option>
                            <option value="important">Important</option>
                            <option value="info">Info</option>
                            <option value="priority">Priority</option>
                            <option value="urgent">Urgent</option>
                            <option value="warning">Warning</option>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <div class="input-group">
                            <input type="text" class="form-control m-input" placeholder="Search Announcement Subject" aria-describedby="basic-addon2" id="search-subject" name="search-subject">
                            <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Search Announcement Subject"><span class="input-group-text"><i class="la la-search"></i></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row mb-3">
            <div class="col-md-12">
                <span class="text-left">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon" id="btn-modal-insert" data-toggle="modal" data-target="#modal-announcement" data-type="insert" data-title="Create Announcement">
                        <i class="flaticon-plus"></i> Previous
                    </a>
                </span>
                <span class="pull-right">
                    <a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon" id="btn-modal-insert" data-toggle="modal" data-target="#modal-announcement" data-type="insert" data-title="Create Announcement">
                        <i class="flaticon-plus"></i> Next
                    </a>
                </span>
            </div>
        </div> -->

        <div id="container-announcements"></div>
        <div class="row no-gutters">
            <div class="col-md-9 col-12">
                <ul id="pagination">
                </ul>
            </div>
            <div class="col-md-1 text-right col-4">
                <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                    <option>5</option>
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                </select>
            </div>
            <div class="col-md-2 text-right col-8">
                <p style="font-family:Poppins;font-size:13px;text-align:right">Showing <span id="count"></span> of <span id="total"></span> records</p>
            </div>
        </div>
    </div>
</div>
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script type="text/javascript">
    function tz_date(thedate, format = null) {
        var tz = moment.tz(new Date(thedate), "Asia/Manila");
        return (format === null) ? moment(tz) : moment(tz).format(format);
    }

    function getAllAnnouncements(limiter, perPage, callback) {
        var searchSubject = $("#search-subject").val();
        var searchHighlight = $("#search-highlight").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Announcement/get_all_announcements",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchSubject: searchSubject,
                searchHighlight: searchHighlight
            },
            beforeSend: function() {
                mApp.block('#content', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#total").html(res.total);
                $("#total").data('count', res.total);
                $("#container-announcements").html("");

                $.each(res.announcements, function(i, ann) {
                    var string = '';
                    var highlightColor = (ann.highlightType == 'important') ? "#7BC043" : (ann.highlightType == 'info') ? "#0392CF" : (ann.highlightType == 'priority') ? "#716ACA" : (ann.highlightType == 'urgent') ? "#E6558B" : "#F7AB42";
                    var highlightIcon = (ann.highlightType == 'important') ? "flaticon-confetti" : (ann.highlightType == 'info') ? "flaticon-information" : (ann.highlightType == 'priority') ? "flaticon-notes" : (ann.highlightType == 'urgent') ? "flaticon-alarm-1" : "flaticon-exclamation-1";

                    string +=
                        '<div style="background:white;" class="p-2 mb-3"><div class="m-0 m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-dismissible fade show alert-announcement" role="alert"  style="border-right-color:' + highlightColor + ' !important">' +
                        '<div class="m-alert__icon"  style="background:' + highlightColor + '">' +
                        '<i class="' + highlightIcon + '" style="color:white"></i><p class="mt-3 m--font-bolder text-capitalize" style="color:white;font-size:12px;width:55px">' + ann.highlightType + '</p>' +
                        '<span style="border-left-color:' + highlightColor + '"></span>' +
                        '</div>' +
                        '<div class="m-alert__text">' +
                        '<h5 style="color:' + highlightColor + ';font-size:16px;" class="m--font-boldest mb-1">' + ann.subject + '</h5><p class="mb-3 mt-1"><span style="color:gray;font-size:12px;" class="m--font-bolder">' + moment(ann.startDate).format('MMMM DD, YYYY') + '&nbsp; | &nbsp;By: &nbsp;' + ann.announcer + '</span></p> <pre class="announcementPre">' +
                        ann.message + '</pre></div></div>'
                    '</div>' +
                    '</div></div></div>';
                    //<p class="mb-0 mt-3">- <span style="color:' + highlightColor + '" class="m--font-bolder">' + ann.announcer + '</span></p>

                    $("#container-announcements").append(string);
                })
                if ($("#container-announcements").html() == "") {
                    $("#container-announcements").html('<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air m-alert--outline"><div class="m-alert__text"><strong>No available announcement!</strong></div></div>');
                }
                var count = $("#total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#count").html(1 + ' - ' + perPage);
                } else {
                    $("#count").html((limiter + 1) + ' - ' + result);
                }

                mApp.unblock('#content');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#content');
            }
        });
    }
    $(function() {
        $("#perpage").change();
        var currentdate = tz_date(moment());
        var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
        var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
        $('#announcement_daterange').val(start + ' - ' + end);
        $('#announcement_daterange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            minDate: currentdate,
            ranges: {
                'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
                'This Week': [moment(currentdate, 'YYYY-MM-DD').startOf('week'), moment(currentdate, 'YYYY-MM-DD').endOf('week')],
                'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate, 'YYYY-MM-DD').endOf('month')]
            }
        }, function(start, end, label) {
            $('#announcement_daterange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
        });

    });
    $("#search-subject,#search-highlight").change(function() {
        $("#perpage").change();
    });
    $("#perpage").change(function() {
        var perPage = $("#perpage").val();
        getAllAnnouncements(0, perPage, function(total) {
            $("#pagination").pagination('destroy');
            $("#pagination").pagination({
                items: total, //default
                itemsOnPage: $("#perpage").val(),
                hrefTextPrefix: "#",
                cssStyle: 'light-theme',
                displayedPages: 10,
                onPageClick: function(pagenumber) {
                    var perPage = $("#perpage").val();
                    getAllAnnouncements((pagenumber * perPage) - perPage, perPage, function() {});
                }
            });
        });
    });
    $("#btn-previous").click(function() {
        $("#pagination").pagination('prevPage');
    })
    $("#btn-next").click(function() {
        $("#pagination").pagination('nextPage');
    });
</script>