<link rel="stylesheet"
    href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.css">
<style type="text/css">
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
    background: unset !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
    background: #F0F0F2 !important;
    color: black !important;
}

.m-datatable__head {
    background: #e0e0e0;
}

.label_irequest {
    font-size: initial;
    font-weight: 500;
    color: darkblue;
}

.label2_irequest {
    color: slateblue;
    font-style: italic;
    font-size: small;
    font-weight: 500;
}

.fieldbackg {
    background: lavender;
}

.mrf_css1 {
    /* background: bisque;
    padding: 18px;
    color: orangered;
    text-align: center;
    font-weight: 700;
    border-bottom-style: dotted;
    border-right-color: #98d2d8; */
    padding: 18px;
    text-align: center;
    font-weight: 700;
    border-bottom-style: dotted;
}

.style_per-line {
    padding-top: 4px;
    padding-left: 3em;
    font-size: inherit;
}

.manpower_label_css {
    text-align: center;
    padding-top: 1em;
    /* background: #ebedf2;
     */
    background: #d5d9f5;
    color: #5650a0
}

.manpower_label_css2 {
    text-align: center;
    padding-top: 1em;
    color: #1b80ce;
}

.w_color_label {
    font-weight: 500;
    color: #5650a0;
}

.desc_display_css {
    border-style: solid;
    padding: 2em;
    background: aliceblue;
    border-color: #baccea;
}

.approved_color {
    background: linear-gradient(to left, #c6f3e9 0%, #65cab5 100%);
    color: #064034;
}

.pending_color {
    background: linear-gradient(to left, #ffe4c4 0%, #ff9966 100%);
    color: #8a3d3a;
}

.disapproved_color {
    background: linear-gradient(to left, #ffe6ea 0%, #f98a9d 100%);
    color: #822635;
}

.missed_color {
    background: linear-gradient(to left, #fbf0d9 0%, #ffb822 100%);
    color: #804100;
}

.for_row_c {
    padding-right: 1em;
    padding-left: 1em;
}

.pos_class_f {
    padding-top: 2em;
    padding-left: 3em;
    font-size: inherit;
}

.missed_recruitment_css {
    background: #ffd486;
    color: #865a5a;
}

.completed_recruitment_css {
    background: #92e4d3;
    color: #036954;
}

.mrf_css2 {
    padding: 18px;
    text-align: center;
    font-weight: 700;
}

.desc_display_css2 {
    border-style: solid;
    padding: 2em;
    background: #e8f3f1;
    border-color: #a4e2d5;
}

.cancelled_recruitment_css {
    background: #ecd2d6;
    color: #ca2b45;
}

.pending_recruitment_css {
    background: #aeebf3;
    color: #15598e;
}

.hiredClass {
    color: #107d66;
}

.span_CL {
    font-size: smaller;
    font-style: italic;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator"> iRequest</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span id="label_req" class="m-nav__link-text"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <div class="row" style="text-align:end">
            <div id="manpower_btndiv" class="col-lg-12 pb-4">
            </div>
        </div>
        <!-- START  -->
        <div class="m-portlet m-portlet--tabs active" id="">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul id="" class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li id="tab_mypendingireq" class="nav-item m-tabs__item">
                            <a id="" class="nav-link m-tabs__link active" data-toggle="tab" href="#my_pendingirequest"
                                role="tab">
                                <i class="fa fa-spinner"></i>Pending Request</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="tab_mymanpowerrequest" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#manpower_request" role="tab">
                                <i class="fa fa-file-o"></i>Request Records</a>
                        </li>

                        <?php if($access_page==3){?>
                        <li class="nav-item m-tabs__item">
                            <a id="tab_missedrequests" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#missed_requests" role="tab">
                                <i class="fa fa-exclamation-circle"></i>Missed Requests</a>
                        </li>
                        <?php };?>
                        <li class="nav-item m-tabs__item">
                            <a id="tab_manpowerRec" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#manpower_RecruitmentDisplay" role="tab">
                                <i class="fa fa-suitcase"></i>Recruitment Records</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="my_pendingirequest" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="my_pendingposition"
                                    data-toggle="m-tooltip" title="" data-original-title="Position">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="my_pendingdepartment"
                                    data-toggle="m-tooltip" title="" data-original-title="Requesting Team">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2" style="display:none">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="recordsall-date_mypendingireq"
                                        data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div id="hide_sel" class="col-md-6 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <select class="m-input form-control m-select font-12" id="my_pending_stats"
                                            data-toggle="m-tooltip" title="" data-original-title="Status">
                                            <option value="0"> All</option>
                                            <option value="2"> Pending</option>
                                            <option value="12"> Missed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="adjust_col" class="col-md-6 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by Request-ID"
                                            id="my_pending_id">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="irequest_pending_col">
                                <div id=""
                                    style="padding-top: 20px !important;border-top: 1px dashed #b1b2bf;margin-top: 25px !important;">
                                    <div class="m_datatable" id="irequest_mypendingDatatable">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="manpower_request" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="my_manreqposition"
                                    data-toggle="m-tooltip" title="" data-original-title="Position">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="my_manreqdepartment"
                                    data-toggle="m-tooltip" title="" data-original-title="Requesting Department">
                                </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="recordsall-date_mymanpowerireq"
                                        data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="my_manreqstatus"
                                    data-toggle="m-tooltip" title="" data-original-title="Status">
                                    <option disabled selected=''>-Select Manpower Request Status-</option>
                                    <option value='0'>All</option>
                                    <option value='5'>Approved</option>
                                    <option value='6'>Disapproved</option>
                                    <?php if($access_page==2){?>
                                    <option value='12'>Missed</option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by Request-ID"
                                            id="my_manreq_id">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="my_manpower_col">
                                <div class="m_datatable" id="irequest_mymanpowerreqDatatable">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Another TAB  -->
                    <!-- END  -->


                    <div class="tab-pane fade col-md-12" id="missed_requests" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="missed_position"
                                    data-toggle="m-tooltip" title="" data-original-title="Position">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="missed_department"
                                    data-toggle="m-tooltip" title="" data-original-title="Requesting Department">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="recordsall-date_missedireq"
                                        data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by Request-ID"
                                            id="missed_search">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="irequest_missed_col">
                                <div id=""
                                    style="padding-top: 20px !important;border-top: 1px dashed #b1b2bf;margin-top: 25px !important;">
                                    <div class="m_datatable" id="irequest_missedDatatable">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade col-md-12" id="manpower_RecruitmentDisplay" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12"
                                    id="manpowerRec_my_manreqposition" data-toggle="m-tooltip" title=""
                                    data-original-title="Position">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12"
                                    id="manpowerRec_my_manreqdepartment" data-toggle="m-tooltip" title=""
                                    data-original-title="Requesting Department">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control font-12" id="manpowerRec_my_manreqStatus"
                                    data-toggle="m-tooltip" title="" data-original-title="Position">
                                    <option value="0">All</option>
                                    <option value="2">Pending</option>
                                    <option value="3">Completed</option>
                                    <option value="7">Cancelled</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="recordsall-mymanpowerRec"
                                        data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by Request-ID"
                                            id="myrec_recruitment_id">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="irequest_ManpowerRec_col">
                                <div id=""
                                    style="padding-top: 20px !important;border-top: 1px dashed #b1b2bf;margin-top: 25px !important;">
                                    <div class="m_datatable" id="irequest_manpowerRecruitmentMonDatatable">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- END  -->
    </div>
</div>
<div class="modal fade" id="form_irequest" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="form_manpower" method="post">
            <div class="modal-content">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">iRequest Manpower</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row p-4" id="">
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleSelect1">Requesting Team</label>
                                <select class="form-control m-select2 m-input m-input--square" name="irequest_req_dept"
                                    id="irequest_req_dept">
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleSelect1">Position/ Title</label>
                                <select class="form-control m-select2 m-input m-input--square" name="irequest_pos_title"
                                    id="irequest_pos_title">
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleInputPassword1">Number of Days
                                    Needed</label><br>
                                <span class="span_CL">( Note that the standard number of days to fill this position
                                    after approval is <strong>30 DAYS</strong>. The number of days here will only serve
                                    as suggestion to when this manpower be likely filled after being approved. )</span>
                                <input type="number" class=" mt-3 form-control m-input fieldbackg"
                                    name="irequest_daysneeded" id="irequest_daysneeded" value="0" min="0"
                                    placeholder="">

                                <!-- <label class="label_irequest" for="exampleInputPassword1">Date Duration required</label>
                                <input type="text" class="form-control m-input fieldbackg" name="irequest_dateduration"
                                    id="irequest_dateduration" placeholder=""> -->
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="m-form__group form-group">
                                <label class="label_irequest">Purpose of Hiring </label>
                                <div id="irequest_purp_ofhiring" class="m-radio-list">
                                </div>
                            </div>
                        </div>
                        <div id="additional_field" class="col-12">
                        </div>
                        <div class="col-12 mt-1">
                            <div class="m-form__group form-group">
                                <label class="label_irequest">Hiring Type </label>
                                <div id="irequest_type_ofhiring" class="m-checkbox-list">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-1">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleInputPassword1">Manpower Needed</label>
                                <input type="number" class="form-control m-input fieldbackg" name="irequest_manpower"
                                    id="irequest_manpower" min="1" value="1" placeholder="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleInputPassword1">Educational
                                    Requirement</label>
                                <input type="text" class="form-control m-input fieldbackg"
                                    name="irequest_educationalreq" id="irequest_educationalreq" placeholder="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="m-form__group form-group">
                                <label class="label_irequest">Gender: </label>
                                <div id="irequest_gender" class="m-radio-list">
                                    <label class="m-radio m-radio--state-success">
                                        <input type="radio" name="irequest_gender" value="male"> Male
                                        <span></span>
                                    </label>
                                    <label class="m-radio m-radio--state-brand">
                                        <input type="radio" name="irequest_gender" value="female"> Female
                                        <span></span>
                                    </label>
                                    <label class="m-radio m-radio--state-primary">
                                        <input type="radio" name="irequest_gender" value="no preference"> No Preference
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleTextarea"> Brief Description of duties</label>
                                <textarea class="form-control m-input fieldbackg" name="irequest_descriptionduties"
                                    id="irequest_descriptionduties" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label2_irequest" for="exampleInputPassword1">*OR attach Job Description
                                    document ( Allowed file types/file extensions: .doc, .docx, .pdf, .txt, .xml, .xlsx,
                                    .xls, .xlsm, .ppt, .pptx, .csv, .xlxx) </label>
                                <br>
                                <div class="row">
                                    <div class="col-2">
                                        <label
                                            class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"
                                            id="add_evidences_button"><i class="fa fa-paperclip"></i> Attach File Here
                                            <input type="file" name="upload_attachment_jobdesc"
                                                id="upload_attachment_jobdesc" style="display: none;"
                                                onchange="getattachment_forIrequest()">
                                        </label>
                                    </div>
                                    <div class="col-10 col-md-9 pl-5 pl-md-0"
                                        style="font-size: 13px; font-weight: 400;margin:auto;text-align:-webkit-left"
                                        id="">
                                        <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-3 col-12"
                                            style="max-height: 50px; height: 100px; position: relative; overflow: visible;display:none"
                                            id="display_div_attachment_jobdesc">
                                            <ol id="fileList_jobdesc">
                                            </ol>
                                        </div>
                                        <div class="text-center">
                                            <label style="color:red" id="label_exceeded_jd"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleTextarea"> Preferred Qualifications/
                                    Experience</label>
                                <textarea class="form-control m-input fieldbackg" id="irequest_qualifications"
                                    name="irequest_qualifications" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit Request</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- DISPLAY MODAL  -->

<div class="modal fade" id="recruitment_displaydetails" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <!-- <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span> -->
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row for_row_c">
                    <div class="col-xl-8 manpower_label_css2"><label
                            style="font-size: x-large;font-weight: 400;">Manpower For Recruitment</label></div>
                    <div id="color_change_dis_recc" class="col-xl-4 mrf_css2 rounded">
                        <div class="row">
                            <div class="col-12" id="ireq_status_display_recc"></div>
                            <div class="col-12">
                                <label style="font-size: small;font-weight:500;"> <i class="fa fa-tags" style=""></i>
                                    IRequest
                                    No. </label>&nbsp;
                                <span id="ireq_id_display_recc" style="color: steelblue;font-weight: 800;"></span>
                            </div>
                        </div>
                    </div>
                    <!-- <div id="color_change_dis_recc" class="col-xl-4 mrf_css2 rounded">
                        <span id="ireq_status_display_recc"></span>
                    </div> -->
                    <div class="col-xl-12">
                        <div id=""
                            style="padding-top: 20px !important;border-top: 1px solid #d1d2e2;margin-top: 25px !important;">
                        </div>
                    </div>
                    <!-- <div class="col-xl-12" style="text-align: right;padding-right:2em;font-size:larger">
                        <label style="font-size: larger;font-weight:500"> <i class="fa fa-tags" style=""></i> IRequest
                            No. </label><span> IREQ -
                        </span><span id="ireq_id_display_recc" style="color: steelblue;font-weight: 800;"></span>
                    </div> -->

                    <div class="col-xl-12 pos_class_f">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Position :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_pos_display_recc"
                                    style="font-weight: 700;color: steelblue;text-transform: uppercase;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Requesting Department :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_reqdept_display_recc"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label">Number of Days Needed :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_dateduration_display_recc"
                                    style="color: #9816f4;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Purpose of Hiring :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_hpurpose_display_recc"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Type of Hiring :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_tHiring_display_recc"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Manpower Needed :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpower_display_recc" style="color: #249a9a;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Manpower Supplied :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpowersupplied_display_recc"
                                    style="color: #249a9a;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>
                    <div id="NumDaysFilled" class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Number of Days Filled :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpowerfilled_display" style="color: #fd7320;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label">Educational Requirement :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_edreq_display_recc"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label">Gender :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_gender_display_recc"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12" style="font-size: inherit;padding: 3em;">
                        <div class="row">
                            <div class="col-12">
                                <label class="w_color_label">Brief Description of Duties :</label>
                            </div>
                            <div class="col-12 desc_display_css2" style="">
                                <span id="ireq_bdesc_display_recc"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12" style="font-size: inherit;padding: 3em;padding-top:0em">
                        <div class="row">
                            <div class="col-12">
                                <label class="w_color_label">Preferred Qualifications / Experience :</label>
                            </div>
                            <div class="col-12 desc_display_css2">
                                <span id="ireq_prefqual_display_recc"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12" style="padding-right: 3em;padding-left: 3em;font-size: inherit;">
                        <div class="row" id="ireq_attachedfile_display_recc">
                        </div>
                    </div>

                    <div class="col-xl-12"
                        style="padding-right: 3em;padding-left: 3em;font-size: inherit;padding-top:2em">
                        <div class="row">
                            <div class="col-xl-4 col-sm-12">
                                <label class="w_color_label"> Approvers :</label>
                            </div>
                            <div id="moredetails_irq_recc" class="col-xl-8 col-sm-12" style="">
                                <!-- <button type="button" class="btn btn-link view_approvers_irqdisplay">Click to View Approver Details</button> -->
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 mt-4" style="padding-right: 3em;padding-left: 3em;font-size: inherit;">
                        <div class="row" id="">
                            <div class="col-xl-12 col-sm-12 mb-2">
                                <label class="w_color_label"> Hired Applicants :</label>
                            </div>
                            <div class="col-xl-12 col-sm-12">
                                <ul id="applicant_hiredNames" class="list-group">
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-4">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            <span>
                                <i class="fa fa-close"></i>
                                <span>Close</span>
                            </span>
                        </button>
                    </div>
                    <!-- <div id="btnfor_completed" class="col-8">

                    </div> -->
                </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="irequest_displaydetails" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <!-- <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span> -->
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row for_row_c">
                    <div class="col-xl-8 manpower_label_css"><label class="mt-2"
                            style="font-size: x-large;font-weight: 400;">Manpower Request</label></div>

                    <div id="color_change_dis" class="col-xl-4 mrf_css1">
                        <div class="row">
                            <div id="ireq_status_display" class="col-12"></div>
                            <div class="col-12">
                                <label style="font-size: small;font-weight:500;"> <i class="fa fa-tags" style=""></i>
                                    IRequest
                                    No. </label>&nbsp;<span id="ireq_id_display"
                                    style="color: steelblue;font-weight: 800;"></span>
                            </div>
                        </div>
                    </div>
                    <!-- 
                    <div id="color_change_dis" class="col-xl-4 mrf_css1">
                        <div class="row">
                            <div class="col-12"></div>
                            <div class="col-12"></div>
                        </div>
                        <span id="ireq_status_display"></span>
                    </div> -->

                    <div class="col-xl-12">
                        <div id=""
                            style="padding-top: 20px !important;border-top: 1px solid #d1d2e2;margin-top: 25px !important;">
                        </div>
                    </div>
                    <!-- <div class="col-xl-12" style="text-align: right;padding-right:2em;font-size:larger">
                        <label style="font-size: larger;font-weight:500"> <i class="fa fa-tags" style=""></i> IRequest
                            No. </label><span> IREQ -
                        </span><span id="ireq_id_display" style="color: steelblue;font-weight: 800;"></span>
                    </div> -->
                    <div class="col-xl-12 pos_class_f">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Position :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_pos_display"
                                    style="font-weight: 700;color: steelblue;text-transform: uppercase;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Requesting Department :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_reqdept_display"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Number of Days Needed:</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_dateduration_display" style="color: #9816f4;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Purpose of Hiring :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_hpurpose_display"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Type of Hiring :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_tHiring_display"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Manpower Needed :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpower_display" style="color: #249a9a;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label">Educational Requirement :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_edreq_display"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label">Gender :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_gender_display"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12" style="font-size: inherit;padding: 3em;">
                        <div class="row">
                            <div class="col-12">
                                <label class="w_color_label">Brief Description of Duties :</label>
                            </div>
                            <div class="col-12 desc_display_css" style="">
                                <span id="ireq_bdesc_display"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12" style="font-size: inherit;padding: 3em;padding-top:0em">
                        <div class="row">
                            <div class="col-12">
                                <label class="w_color_label">Preferred Qualifications / Experience :</label>
                            </div>
                            <div class="col-12 desc_display_css">
                                <span id="ireq_prefqual_display"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12" style="padding-right: 3em;padding-left: 3em;font-size: inherit;">
                        <div class="row" id="ireq_attachedfile_display">
                        </div>
                    </div>
                    <div class="col-xl-12"
                        style="padding-right: 3em;padding-left: 3em;font-size: inherit;padding-top:2em">
                        <div class="row">
                            <div class="col-xl-4 col-sm-12">
                                <label class="w_color_label"> Approvers :</label>
                            </div>
                            <div id="moredetails_irq" class="col-xl-7 col-sm-12" style="">
                                <!-- <button type="button" class="btn btn-link view_approvers_irqdisplay">Click to View Approver Details</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="approvdis_btn" class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="irequest_approverdetails" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">Approver Details</h5>
                <button data-id="0" id="dis_appr_bttn" type="button" class="close" data-dismiss="modal"
                    aria-label="Close closeModalBtn" style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id="approverlist_disp" class="modal-body p-4">
                <div>
                    <!-- end  -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal for confirmation  -->

<div class="modal fade" id="approval-disapproval_modal" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" style="padding:2em">
            <div id="" class="modal-body p-4">
                <h5 id="mess_apdis"></h5>
                <div class="row">
                    <div class="col-12" style="padding-bottom: 2em;padding-top: 2em">
                        <input type="text" class="form-control m-input input_customize" name="" id="approval_comment"
                            rows="1" placeholder="Type remarks here..." data-mentions-input="true"
                            data-fv-field="placeIncident">
                    </div>
                    <div class="col-12 mb-3" id="aftDIV" style="display:none">
                        <div class="m-checkbox-list">
                            <label class="m-checkbox m-checkbox--state-success">
                                <input type="checkbox" name="aftApproved" value="1"> Approved by AFT (Accounting and Finance Team).
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div id="apdis_btn" class="col-6">
                    </div>
                    <div class="col-5">
                        <button type="button" id="cancel_bttn_modal"
                            class="btn btn-danger m-btn m-btn--icon m-btn--wide" data-dismiss="modal"
                            aria-label="Close closeModalBtn">
                            <span><i class="fa fa-remove"></i><span> Cancel</span></span></button>
                    </div>
                </div>
                <div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js"
    type="text/javascript"></script>
<?php if($access_page==1){
?>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/myrequest_cha.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/myrequest_cha_manpower.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/myrequest_recruitment.js"></script>
<?php }else if($access_page==2){?>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_cha_myrequestapproval.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_cha_myrequestapproval_manpower.js">
</script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/allreqapproval_recruitment.js"></script>
<?php }else if($access_page==3){?>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_cha_allrequests.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_cha_allrequests_manpower.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_cha_missedrequests.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/allreq_recruitment.js"></script>
<?php }?>