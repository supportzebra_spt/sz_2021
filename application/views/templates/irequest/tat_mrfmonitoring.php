<link rel="stylesheet"
    href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.css">
<style type="text/css">
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
    background: unset !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
    background: #F0F0F2 !important;
    color: black !important;
}

.m-datatable__head {
    background: #e0e0e0;
}

.label_irequest {
    font-size: initial;
    font-weight: 500;
    color: darkblue;
}

.label2_irequest {
    color: slateblue;
    font-style: italic;
    font-size: small;
    font-weight: 500;
}

.fieldbackg {
    background: lavender;
}

.mrf_css1 {
    padding: 18px;
    text-align: center;
    font-weight: 700;
    border-bottom-style: dotted;
}

.mrf_css2 {
    padding: 18px;
    text-align: center;
    font-weight: 700;
}

.style_per-line {
    padding-top: 4px;
    padding-left: 3em;
    font-size: inherit;
}

.manpower_label_css {
    text-align: center;
    padding-top: 1em;
    /* background: #ebedf2;
     */
    background: #d5d9f5;
    color: #5650a0
}

.manpower_label_css2 {
    text-align: center;
    padding-top: 1em;
    color: #1b80ce;
}

.w_color_label {
    font-weight: 500;
    color: #5650a0;
}

.desc_display_css {
    border-style: solid;
    padding: 2em;
    background: aliceblue;
    border-color: #baccea;
}

.desc_display_css2 {
    border-style: solid;
    padding: 2em;
    background: #e8f3f1;
    border-color: #a4e2d5;
}

.approved_color {
    background: linear-gradient(to left, #c6f3e9 0%, #65cab5 100%);
    color: #064034;
}

.pending_color {
    background: linear-gradient(to left, #ffe4c4 0%, #ff9966 100%);
    color: #8a3d3a;
}

.disapproved_color {
    background: linear-gradient(to left, #ffe6ea 0%, #f98a9d 100%);
    color: #822635;
}

.for_row_c {
    padding-right: 1em;
    padding-left: 1em;
}

.pos_class_f {
    padding-top: 2em;
    padding-left: 3em;
    font-size: inherit;
}

.custom-editable {
    font-size: x-large;
    color: #1996f6 !important;
    font-style: normal !important;
    font-weight: 600;
    /* text-decoration: underline dotted #1D98F6; */
}

.underline_css_num {
    width: 20px;
    border-bottom: 2px solid #8b86c5;
    border-bottom-style: dotted;
}

.update_manpowerdes {
    padding-right: 3em;
    padding-left: 3em;
    font-size: inherit;
    padding-top: 2em
}

.supplied_manpowerdes {
    background: gainsboro;
    padding: 2em;
}

.required_manpowerdes {
    background: khaki;
    padding: 2em;
}

.missed_recruitment_css {
    background: #ffd486;
    color: #865a5a;
}

.cancelled_recruitment_css {
    background: #ecd2d6;
    color: #ca2b45;
}

.completed_recruitment_css {
    background: #92e4d3;
    color: #036954;
}

.pending_recruitment_css {
    background: #a9e7ef;
    color: #135f9a;
}

.s_label {
    font-size: smaller;
    font-family: ;
    font-style: italic;
    color: darkcyan;
}

.hiredClass {
    color: #107d66;
}

.back_Widg {
    height: 170px;
    background: #c6e2c6;
}

.container_Col6 {
    height: 36em !important;
}

.backg_request {
    background: #3d3e4d !important;
}

.backg_recruit {
    /* border: 4px solid #5f9ea066; */
    background: #f4f5fd !important;
}

.number_ColoR {
    font-size: x-large !important;
    color: white !important;
}

.number_ColoR_2 {
    font-size: x-large !important;
    color: #3d3e4d !important;
}

.icon_pending {
    font-size: unset;
    color: #ea9b4a !important;
}

.icon_approved {
    font-size: unset;
    color: #87ce87 !important;
}

.icon_disapproved {
    font-size: unset;
    color: #e09999 !important;
}

.icon_missed {
    font-size: unset;
    color: #dab669 !important;
}

.desc_pending {
    color: darkorange !important;
    font-weight: 600;
}

.desc_approved {
    color: darkseagreen !important;
    font-weight: 600;
}

.desc_missed {
    color: #f1d69e !important;
    font-weight: 600;
}

.desc_missed2 {
    color: #f7ba38 !important;
    font-weight: 600;
}

.desc_disapproved {
    color: #f15870 !important;
    font-weight: 600;
}

.body_Back {
    background: cadetblue;
}

/* chart  */

#container {
    height: 400px;
}

.total_manpowerDes {
    background: white;
    margin-right: 1.8em;
    margin-left: 1.8em;
}

.label3_tot {
    padding: 3em;
    font-weight: 600;
    color: #c6e2c6;
    text-align: center;
}

.label3_num {
    color: white;
    font-size: x-large;
    padding-top: 1.5em;
    font-weight: 600;
}

.label3_tot2 {
    padding: 3em;
    font-weight: 600;
    color: cadetblue;
    text-align: center;
}

.label3_num2 {
    color: #3d3e4d;
    font-size: x-large;
    padding-top: 1.5em;
    font-weight: 600;
}

.head_c {
    text-align: center;
    font-size: small;
    background: #e0e0e0;
    color: #505a6b;
    font-weight: bolder;
}
.ShadowClass{
    border: 4px solid #78c2caeb;
    box-shadow: 1px -2px 15px #638c63 !important;
}
.ShadowClass2{
    border: 4px solid #5f9ea066;
    box-shadow: 1px -2px 15px #638c63 !important;
}
.body_c {
    text-align: center;

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    font-size: 0.85rem;
}

.overdue_request {
    background: #ffd4db !important;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator"> iRequest- iRecruit</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Manpower - Recruitment</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <div class="row" style="text-align:end">
            <!-- button here before  -->
        </div>
        <!-- START  -->
        <div class="m-portlet m-portlet--tabs active" id="">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul id="" class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li id="tattab_dashboard" class="nav-item m-tabs__item">
                            <a id="dashBoardTab" class="nav-link m-tabs__link active" data-toggle="tab"
                                href="#tat_dashboard" role="tab">
                                <i class="fa fa-bar-chart"></i>Dashboard</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="tattab_pendingrecruitment" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#tat_pendingrecruitment" role="tab">
                                <i class="fa fa-spinner"></i>Pending Recruitment</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="tattab_completedrecruitment" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#tat_completedrecruitment" role="tab">
                                <i class="fa fa-file-o"></i>Recruitment Records</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="tattab_hiredApplicants" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#tat_hiredApplicants" role="tab">
                                <i class="fa fa-file-o"></i>Hired Applicants</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="tat_dashboard" role="tabpanel">
                        <div class="row">

                            <div class="col-md-12 mb-5" style="">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px;"
                                        placeholder="Select date range" id="irequest_dateduration"
                                        data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>

                            <div class="col-xl-6 container_Col6">
                                <!--begin:: Widgets/Activity-->
                                <div
                                    class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light  m-portlet--rounded-force">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title text-center">
                                                <h3 class="m-portlet__head-text m--font-light">
                                                    iRequest Records
                                                <span id="tot_irequest" class="badge badge-pill badge-info">0</span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div id="addBtn_request" class="m-portlet__head-tools">
                                        </div>
                                    </div>
                                    <div class="m-portlet__body body_Back">
                                        <div class="m-widget17">
                                            <div
                                                class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                                                <div class="m-widget17__chart back_Widg" style="">
                                                    <div class="chartjs-size-monitor"
                                                        style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                                        <div class="chartjs-size-monitor-expand"
                                                            style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                            <div
                                                                style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                                            </div>
                                                        </div>
                                                        <div class="chartjs-size-monitor-shrink"
                                                            style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                            <div
                                                                style="position:absolute;width:200%;height:200%;left:0; top:0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <canvas id="m_chart_activities" width="502" height="208"
                                                        class="chartjs-render-monitor"
                                                        style="display: block; width: 502px; height: 208px;"></canvas>
                                                </div>
                                            </div>
                                            <div class="m-widget17__stats">
                                                <div class="m-widget17__items m-widget17__items-col1">
                                                    <div class="m-widget17__item backg_request rounded" data-stat="2">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-spinner text-primary icon_pending fa-spin"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irequest_PendingNum"
                                                                    class="m-widget17__subtitle number_ColoR" style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_pending" style="">
                                                                    PENDING
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-widget17__item backg_request rounded" data-stat="5">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-thumbs-o-up text-primary icon_approved"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irequest_ApprovedNum"
                                                                    class="m-widget17__subtitle number_ColoR" style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_approved" style="">
                                                                    APPROVED
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-widget17__items m-widget17__items-col2">
                                                    <div class="m-widget17__item backg_request rounded" data-stat="12">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-exclamation-circle text-primary icon_missed m-animate-blink"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irequest_MissedNum"
                                                                    class="m-widget17__subtitle number_ColoR" style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_missed" style="">
                                                                    MISSED
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-widget17__item backg_request rounded" data-stat="6">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-thumbs-o-down text-primary icon_disapproved"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irequest_DisapprovedNum"
                                                                    class="m-widget17__subtitle number_ColoR" style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_disapproved"
                                                                    style="">
                                                                    DISAPPROVED
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="row total_manpowerDes" style="
                                                background: #3d3e4d;
                                            ">
                                            <div class="col-7 label3_tot" style="">
                                                <span class="">Total Records</span>
                                            </div>
                                            <div class="col-3 label3_num" style="">
                                                <span id="totalReqCount">0</span>
                                            </div>
                                            <div>
                                            </div>
                                        </div> -->

                                    </div>
                                </div>
                                <!--end:: Widgets/Activity-->
                            </div>

                            <!-- Second  -->

                            <div class="col-xl-6 container_Col6">
                                <!--begin:: Widgets/Activity-->
                                <div
                                    class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light  m-portlet--rounded-force">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text m--font-light">
                                                    Recruitment Records
                                                </h3>
                                            </div>
                                        </div>
                                        <div id="addBtn_request2" class="m-portlet__head-tools">
                                        </div>
                                    </div>
                                    <div class="m-portlet__body body_Back">
                                        <div class="m-widget17">
                                            <div
                                                class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">
                                                <div class="m-widget17__chart back_Widg" style="">
                                                    <div class="chartjs-size-monitor"
                                                        style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                                        <div class="chartjs-size-monitor-expand"
                                                            style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                            <div
                                                                style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
                                                            </div>
                                                        </div>
                                                        <div class="chartjs-size-monitor-shrink"
                                                            style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                                            <div
                                                                style="position:absolute;width:200%;height:200%;left:0; top:0">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <canvas id="m_chart_activities" width="502" height="208"
                                                        class="chartjs-render-monitor"
                                                        style="display: block; width: 502px; height: 208px;"></canvas>
                                                </div>
                                            </div>
                                            <div class="m-widget17__stats">
                                                <div class="m-widget17__items m-widget17__items-col1">
                                                    <div class="m-widget17__item backg_recruit rounded" data-stat="vacant">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-question text-primary icon_pending"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irecruit_vacantNum"
                                                                    class="m-widget17__subtitle number_ColoR_2"
                                                                    style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_pending" style="">
                                                                    VACANT
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-widget17__item backg_recruit rounded" data-stat="hired">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-users text-primary icon_approved"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irecruit_hiredNum"
                                                                    class="m-widget17__subtitle number_ColoR_2"
                                                                    style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_approved" style="">
                                                                    HIRED
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-widget17__items m-widget17__items-col2">
                                                    <div class="m-widget17__item backg_recruit rounded" data-stat="exceeded">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-exclamation-circle text-primary icon_missed"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irecruit_ExceededNum"
                                                                    class="m-widget17__subtitle number_ColoR_2"
                                                                    style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_missed2" style="">
                                                                    Exceeded 30 Days
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-widget17__item backg_recruit rounded" data-stat="cancelled">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="m-widget17__icon">
                                                                    <i
                                                                        class="fa fa-remove text-primary icon_disapproved"></i>
                                                                </span>
                                                            </div>
                                                            <div class="col-12" style="margin: 0px;">
                                                                <span id="irecruit_CancelledNum"
                                                                    class="m-widget17__subtitle number_ColoR_2"
                                                                    style="">
                                                                    0
                                                                </span>
                                                            </div>
                                                            <div class="col-12">
                                                                <span class="m-widget17__desc desc_disapproved"
                                                                    style="">
                                                                    CANCELLED
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="row total_manpowerDes" style="
                                                background: white;
                                            ">
                                                <div class="col-7 label3_tot2" style="">
                                                    <span class="">All Count </span>
                                                </div>
                                                <div class="col-3 label3_num2" style="">
                                                    <span id="totalRecCount">0</span>
                                                </div>
                                                <div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <!--end:: Widgets/Activity-->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="tat_pendingrecruitment" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="tat_manreqposition"
                                    data-toggle="m-tooltip" title="" data-original-title="Position">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="tat_manreqdepartment"
                                    data-toggle="m-tooltip" title="" data-original-title="Requesting Team">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2" style="display:none">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px;"
                                        placeholder="Select date range" id="recordsall-date_tat" data-toggle="m-tooltip"
                                        title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div id="tat_recruitment_div" class="col-md-12 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by Request-ID"
                                            id="tat_recruitment_id">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="irequest_recruitment_col">
                                <div id=""
                                    style="padding-top: 20px !important;border-top: 1px dashed #b1b2bf;margin-top: 25px !important;">
                                    <div class="m_datatable" id="irequest_pendingRecruitmentDatatable">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tat_completedrecruitment" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="tat_manreqposition_c"
                                    data-toggle="m-tooltip" title="" data-original-title="Position">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12"
                                    id="tat_manreqdepartment_c" data-toggle="m-tooltip" title=""
                                    data-original-title="Requesting Team">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="recordsall-date_tat_c"
                                        data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select font-12" id="tat_manreq_stat"
                                    data-toggle="m-tooltip" title="" data-original-title="Status">
                                    <option value="0"> All</option>
                                    <option value="3"> Completed</option>
                                    <option value="7"> Cancelled</option>
                                </select>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by Request-ID"
                                            id="tat_recruitment_id_c">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" id="irequest_recruitment_completed_col">
                                <div id=""
                                    style="padding-top: 20px !important;border-top: 1px dashed #b1b2bf;margin-top: 25px !important;">
                                    <div class="m_datatable" id="irequest_completedRecruitmentDatatable">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="tat_hiredApplicants" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12"
                                    id="tat_manreqposition_hired" data-toggle="m-tooltip" title=""
                                    data-original-title="Position">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control m-select2 font-12" id="tat_manreq_position"
                                    data-toggle="m-tooltip" title="" data-original-title="Requesting Team">
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="recordsall-date_tat_hired"
                                        data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by Request-ID"
                                            id="tat_recruitment_id_hired">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" id="irequest_recruitment_hired_col">
                            <div id=""
                                style="padding-top: 20px !important;border-top: 1px dashed #b1b2bf;margin-top: 25px !important;">
                                <div class="m_datatable" id="irequest_tathiredApplicantsDatatable">
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- END  -->
    </div>
</div>
<div class="modal fade" id="form_irequest" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="form_manpower" method="post">
            <div class="modal-content">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">iRequest Manpower</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row p-4" id="">
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleSelect1">Requesting Team</label>
                                <select class="form-control m-select2 m-input m-input--square" name="irequest_req_dept"
                                    id="irequest_req_dept">
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleSelect1">Position/ Title</label>
                                <select class="form-control m-select2 m-input m-input--square" name="irequest_pos_title"
                                    id="irequest_pos_title">
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleInputPassword1">Number of Days
                                    Needed:</label>
                                <input type="text" class="form-control m-input fieldbackg" name="irequest_dateduration"
                                    id="irequest_dateduration" placeholder="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="m-form__group form-group">
                                <label class="label_irequest">Purpose of Hiring: </label>
                                <div id="irequest_purp_ofhiring" class="m-radio-list">
                                </div>
                            </div>
                        </div>
                        <div id="additional_field" class="col-12">
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleInputPassword1">Manpower Required</label>
                                <input type="number" class="form-control m-input fieldbackg" name="irequest_manpower"
                                    id="irequest_manpower" placeholder="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleInputPassword1">Educational
                                    Requirement</label>
                                <input type="text" class="form-control m-input fieldbackg"
                                    name="irequest_educationalreq" id="irequest_educationalreq" placeholder="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="m-form__group form-group">
                                <label class="label_irequest">Gender: </label>
                                <div id="irequest_gender" class="m-radio-list">
                                    <label class="m-radio m-radio--state-success">
                                        <input type="radio" name="irequest_gender" value="male"> Male
                                        <span></span>
                                    </label>
                                    <label class="m-radio m-radio--state-brand">
                                        <input type="radio" name="irequest_gender" value="female"> Female
                                        <span></span>
                                    </label>
                                    <label class="m-radio m-radio--state-primary">
                                        <input type="radio" name="irequest_gender" value="no preference"> No
                                        Preference
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleTextarea"> Brief Description of
                                    duties</label>
                                <textarea class="form-control m-input fieldbackg" name="irequest_descriptionduties"
                                    id="irequest_descriptionduties" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label2_irequest" for="exampleInputPassword1">*OR attach Job
                                    Description
                                    document ( Allowed file types/file extensions: .doc, .docx, .pdf, .txt, .xml,
                                    .xlsx,
                                    .xls, .xlsm, .ppt, .pptx, .csv, .xlxx) </label>
                                <br>
                                <div class="row">
                                    <div class="col-2">
                                        <label
                                            class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"
                                            id="add_evidences_button"><i class="fa fa-paperclip"></i> Attach File
                                            Here
                                            <input type="file" name="upload_attachment_jobdesc"
                                                id="upload_attachment_jobdesc" style="display: none;"
                                                onchange="getattachment_forIrequest()">
                                        </label>
                                    </div>
                                    <div class="col-10 col-md-9 pl-5 pl-md-0"
                                        style="font-size: 13px; font-weight: 400;margin:auto;text-align:-webkit-left"
                                        id="">
                                        <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-3 col-12"
                                            style="max-height: 50px; height: 100px; position: relative; overflow: visible;display:none"
                                            id="display_div_attachment_jobdesc">
                                            <ol id="fileList_jobdesc">
                                            </ol>
                                        </div>
                                        <div class="text-center">
                                            <label style="color:red" id="label_exceeded_jd"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <label class="label_irequest" for="exampleTextarea"> Preferred Qualifications/
                                    Experience</label>
                                <textarea class="form-control m-input fieldbackg" id="irequest_qualifications"
                                    name="irequest_qualifications" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="m-form__group form-group">
                                <!-- <label>Checkbox States</label> -->
                                <div class="m-checkbox-list">
                                    <label class="m-checkbox m-checkbox--state-success">
                                        <input style="font-weight:500" name="irequest_aftapproved"
                                            id="irequest_aftapproved" type="checkbox" value="1"> Approved by
                                        Accounting
                                        and
                                        Finance Team
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit Request</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- DISPLAY MODAL  -->

<div class="modal fade" id="recruitment_displaydetails" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row for_row_c">
                    <div class="col-xl-8 manpower_label_css2"><label
                            style="font-size: x-large;font-weight: 400;">Manpower For Recruitment</label></div>
                    <div id="color_change_dis" class="col-xl-4 mrf_css2 rounded">
                        <div class="row">
                            <div class="col-12" id="ireq_status_display"></div>
                            <div class="col-12">
                                <label style="font-size: small;font-weight:500;"> <i class="fa fa-tags" style=""></i>
                                    IRequest
                                    No. </label>&nbsp;<span id="ireq_id_display"
                                    style="color: steelblue;font-weight: 800;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div id=""
                            style="padding-top: 20px !important;border-top: 1px solid #d1d2e2;margin-top: 25px !important;">
                        </div>
                    </div>
                    <div class="col-xl-12 pos_class_f">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Position :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_pos_display"
                                    style="font-weight: 700;color: steelblue;text-transform: uppercase;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Requesting Team :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_reqdept_display"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Number of Days Needed :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_dateduration_display" style="color: #9816f4;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Purpose of Hiring :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_hpurpose_display"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Type of Hiring :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_tHiring_display_recc"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Manpower Needed :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpower_display" style="color: #249a9a;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Manpower Supplied :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpowersupplied_display"
                                    style="color: #249a9a;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>
                    <div id="NumDaysFilled" class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Number of Days Filled :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpowerfilled_display" style="color: #fd7320;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>
                    <div id="manpowerToSupplyDIV" class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label"> Manpower To Supply:</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_manpowerRemaining_display"
                                    style="color: #249a9a;font-weight: 700;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label">Educational Requirement :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_edreq_display"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 style_per-line">
                        <div class="row">
                            <div class="col-4">
                                <label class="w_color_label">Gender :</label>
                            </div>
                            <div class="col-8">
                                <span id="ireq_gender_display"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12" style="font-size: inherit;padding: 3em;">
                        <div class="row">
                            <div class="col-12">
                                <label class="w_color_label">Brief Description of Duties :</label>
                            </div>
                            <div class="col-12 desc_display_css2" style="">
                                <span id="ireq_bdesc_display"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12" style="font-size: inherit;padding: 3em;padding-top:0em">
                        <div class="row">
                            <div class="col-12">
                                <label class="w_color_label">Preferred Qualifications / Experience :</label>
                            </div>
                            <div class="col-12 desc_display_css2">
                                <span id="ireq_prefqual_display"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12" style="padding-right: 3em;padding-left: 3em;font-size: inherit;">
                        <div class="row" id="ireq_attachedfile_display">
                        </div>
                    </div>
                    <div class="col-xl-12"
                        style="padding-right: 3em;padding-left: 3em;font-size: inherit;padding-top:2em">
                        <div class="row">
                            <div class="col-xl-4 col-sm-12">
                                <label class="w_color_label"> Approvers :</label>
                            </div>
                            <div id="moredetails_irq_recc" class="col-xl-8 col-sm-12" style="">
                                <!-- <button type="button" class="btn btn-link view_approvers_irqdisplay">Click to View Approver Details</button> -->
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 mt-4" style="padding-right: 3em;padding-left: 3em;font-size: inherit;">
                        <div class="row" id="">
                            <div class="col-xl-12 col-sm-12 mb-2">
                                <label class="w_color_label"> Hired Applicants :</label>
                            </div>
                            <div class="col-xl-12 col-sm-12">
                                <ul id="applicant_hiredNames" class="list-group">
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- start here  -->
                    <div id="sel_ApplicantDIV" class="col-xl-12 mt-5"
                        style="font-size: inherit;padding: 3em;padding-top:0em">
                        <div class="row">
                            <div class="col-xl-12">
                                <label class="w_color_label">Select Applicants</label>
                                <span class="s_label">(Select Applicants employed for this position.)</span>
                            </div>
                            <div class="col-xl-7 col-sm-12">
                                <div class="form-group">
                                    <select class="form-control m-bootstrap-select mt-1" id="applicant_nAmes"
                                        name="applicant_nAmes" data-live-search='true' data-actions-box='true' multiple>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-5 col-sm-12" id="buttonToSaveApplicants" style="text-align: center;">
                            </div>
                            <div class="col-12 mt-4 text-center" id="addNewApplicant" style="">
                            </div>
                        </div>
                    </div>
                    <!-- THIS IS ADDING SUPPLY  -->

                    <!-- <div id="update_manpowerdes" class="col-xl-12 update_manpowerdes" style="">
                        <div class="row">
                            <div class="col-xl-6 col-sm-12 required_manpowerdes" style="">
                                <div class="row">
                                    <div class="col-5" style="font-size: medium;">Manpower Needed</div>
                                    <div class="col-5">
                                        <input type="number" class="form-control m-input input_customize"
                                            name="update_req" id="update_req">
                                    </div>
                                    <div class="col-2" id="btn-check_ned">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-sm-12 supplied_manpowerdes" style="">
                                <div class="row">
                                    <div class="col-5" style="font-size: medium;">Manpower Supplied</div>
                                    <div class="col-5">
                                        <input type="number" class="form-control m-input input_customize"
                                            name="update_sup" id="update_sup">
                                    </div>
                                    <div class="col-2" id="btn-check_sup">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-4">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            <span>
                                <i class="fa fa-close"></i>
                                <span>Close</span>
                            </span>
                        </button>
                    </div>
                    <div id="btnfor_completed" class="col-8">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="irequest_approverdetails" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">Approver Details</h5>
                <button data-id="0" id="dis_appr_bttn" type="button" class="close" data-dismiss="modal"
                    aria-label="Close closeModalBtn" style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id="approverlist_disp" class="modal-body p-4">
                <div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="details_ForCountNumber" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="" method="post">
            <div class="modal-content">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel"> Manpower Requests Records</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row p-4" id="">
                        <div class="col-xl-12 mb-4">
                            <h4 id="records_laBel_mp"></h4>
                        </div>
                        <div class="col-md-12" id="manpowerDetails_col">
                            <div id=""
                                style="padding-top: 20px !important;border-top: 1px dashed #b1b2bf;margin-top: -14px !important;">
                                <div class="m_datatable" id="manpowerDetails_Datatable">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit Request</button>
                </div> -->
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="details_ForCountNumber_recruit" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="" method="post">
            <div class="modal-content">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel"> Recruitment Records</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12 mb-4"
                                style="border-bottom: 1px dashed #b1b2bf;margin-top: 4px !important;">
                                <h4 id="records_laBel"></h4>
                            </div>
                            <div class="col-xl-12">
                                <table id="emptyAfter" class="table table-striped">
                                    <thead id="detailsForRequest_head" class="head_c">
                                    </thead>
                                    <tbody id="detailsForRequest" class="body_c">
                                    </tbody>
                                </table>
                                <div style="text-align: center;display:none;margin-bottom: 45px;" class="row allNorecords">
                                    <div class="col-12" style="/*! background: aliceblue; */">
                                        No Records!
                                    </div>
                                </div>
                            </div>

                            <!-- for Vacant record  -->
                            <div class="col-xl-12 mb-4 otherVacanttable"
                                style="border-bottom: 1px dashed #b1b2bf;margin-top: 4px !important;">
                                <h4 id="records_laBel2"></h4>
                            </div>
                            <div class="col-xl-12 otherVacanttable" style="display:none">
                                <table id="emptyAfter2" class="table table-striped">
                                    <thead id="detailsForRequest_head_vacant" class="head_c">
                                    </thead>
                                    <tbody id="detailsForRequest_body_vacant" class="body_c">
                                    </tbody>
                                </table>
                                <div style="text-align: center;display:none;margin-bottom: 45px;" class="row vacantNorecords">
                                    <div class="col-12" style="/*! background: aliceblue; */">
                                        No Records!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit Request</button>
                </div> -->
            </div>
        </form>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js"
    type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_irecruit_manpower_cha.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_irecruit_manpower_Completed_cha.js">
</script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_TAT_hired.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_TAT_dashboard.js"></script>