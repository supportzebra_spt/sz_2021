<link rel="stylesheet"
    href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/lightbox2/dist/css/lightbox.css" />
<style type="text/css">
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
    background: unset !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
    background: #F0F0F2 !important;
    color: black !important;
}

.m-datatable__head {
    background: #e0e0e0;
}

.setting_label_ir {
    font-size: x-large;
}

.set_deadline_approval {
    font-size: large;
}

.custom-editable {
    font-size: x-large;
    color: #1996f6 !important;
    font-style: normal !important;
    font-weight: 600;
    /* text-decoration: underline dotted #1D98F6; */
}

.approver_dtcss {
    border-style: dashed !important;
    border-color: #cbb3dc !important;
    padding: 1em !important;
}

.form_approver_ir {
    padding: 1em;
}
.underline_css_num{
    width: 20px;
    border-bottom: 2px solid #8b86c5;
    border-bottom-style: dotted;
}
.apdetname{
    font-size: large;
    font-weight: 500;
    color: lightseagreen;
}
.wlabel_ireq{
    font-weight: 500;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator"> iRequest Control Panel</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <div class="row">
            <div class="col-xl-12">
                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <label class="setting_label_ir"> iRequest Approvers Setting</label>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">
                                <div class="row">
                                    <div class="col-12" style="padding-bottom: 3em;padding-top: 2em;">
                                        <button id="add_approver_irq" type="button"
                                            class="btn btn-success m-btn m-btn--icon m-btn--wide">
                                            <span>
                                                <i class="fa fa-plus"></i>
                                                <span>Add Approver</span>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-12 set_deadline_approval">
                                        <label>Set Deadline for Approval :</label>
                                        <div class="row" style="padding-bottom:2em;padding-left:2em">
                                            <div class="col-2 underline_css_num">
                                                <a href="#" id="popover_ireq_deadline" 
                                                    class="custom-editable custom-task-duedate displayof_deadline"
                                                    data-original-title="" data-value="null" title=""></a>
                                            </div>
                                            <div class="col-6" style="font-size: x-large;">
                                                <span> Days</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-8" id="ireqapprover_col">
                                <div class="row">
                                    <div class="col-6 pb-4 pt-4">
                                        <select name="" class="m-input form-control m-select font-12" id="filter_emptype"
                                        data-toggle="m-tooltip" title="" data-original-title="Approval Type">
                                            <!-- <option value="">All</option> -->
                                            <option value="admin" selected>Admin</option>
                                            <option value="agent">Agent</option>
                                        </select>
                                    </div>
                                    <div class="col-12 m_datatable approver_dtcss" id="irequest_approversdatatable">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="irequest_addapprover" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    data-backdrop="static">
    <div class="modal-dialog modal-m" role="document">
        <div class="modal-content">
            <form id="form_addir_approver" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Approver</h5>
                    <button onclick="" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class='row'>
                    <div class='col-xl-12 form_approver_ir'>
                            <div class="form-group">
                                <label>Approval For</label>
                                <select name="ir_addapptype" id="ir_addapptype"
                                    class="m-input form-control m-select font-12" data-toggle="m-tooltip" title=""
                                    data-original-title="Level of Approval" placeholder="- Select Approval Type -">
                                    <option disabled selected=''> - Select Approval - </option>
                                    <option value="admin"> Admin </option>
                                    <option value="agent"> Agent </option>
                                </select>
                            </div>
                        </div>
                        <div class='col-xl-12 form_approver_ir'>
                            <div class="form-group">
                                <label>Employees</label>
                                <select name="cp_employees" class="m-input form-control m-select2 font-12"
                                    id="cp_employees" data-toggle="m-tooltip" title="" data-original-title="Employees">
                                </select>
                            </div>
                        </div>
                        <div class='col-xl-12 form_approver_ir'>
                            <div class="form-group">
                                <label>Level</label>
                                <select name="ir_addlevel" id="ir_addlevel"
                                    class="m-input form-control m-select2 font-12" data-toggle="m-tooltip" title=""
                                    data-original-title="Level of Approval"
                                    placeholder=" - Select Level of Approval - ">

                                    <!-- <option disabled selected=''> - Select Level of Approval - </option>
                                    <option value="1"> 1 </option>
                                    <option value="2"> 2 </option>
                                    <option value="3"> 3 </option> -->

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-accent">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="irequest_updateapprover" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <!-- <form id="form_addir_approver" method="post"> -->
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Approver Details</h5>
                <button onclick="" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-xl-12 form_approver_ir'>
                        <div class="form-group">
                            <label class="wlabel_ireq">Approver Name:</label><br>
                            <span class="approver_det_name apdetname"></span>
                        </div>
                    </div>
                    <div class='col-xl-12 form_approver_ir'>
                        <div class="form-group">
                            <label class="wlabel_ireq">Level:</label>
                            <select name="ir_addlevel_update" id="ir_addlevel_update"
                                class="m-input form-control m-select font-12" data-toggle="m-tooltip" title=""
                                data-original-title="Level of Approval" placeholder=" - Select Level of Approval - ">
                            </select>
                        </div>
                    </div>
                    <!-- <div class='col-xl-12 form_approver_ir'>
                        <div class="form-group">
                            <label class="wlabel_ireq">Approval For:</label>
                            <select name="ir_addapptype_update" id="ir_addapptype_update"
                                class="m-input form-control m-select font-12" data-toggle="m-tooltip" title=""
                                data-original-title="Level of Approval" placeholder="- Select Approval Type -">
                            </select>
                        </div>
                    </div> -->
                </div>
            </div>
            <div id="btn_approvalmodalfooter" class="modal-footer">
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!--end::Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js"
    type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/irequest/irequest_cha_controlpanel.js"></script>