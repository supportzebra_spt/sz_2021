<style>
    .m-checkbox>span, .m-radio>span {
        top: -3px !important;
        /* height: 16px !important; */
    }
    .view_sched_details{
        cursor: pointer;
        color: #0d5ff3;
    }
    .view_sched_details:hover{
        text-decoration: underline;
        color: #720ddc;
    }
    .m-portlet.m-portlet--creative .m-portlet__head .m-portlet__head-caption .m-portlet__head-label {
        top: -1rem !important;
    }
    .m-portlet.m-portlet--creative {
        padding-top: 0rem !important;
        margin-top: 1rem !important;
    }
    .m-demo .m-demo__preview {
        border: 3px solid #bdc3d4;
    }
    .editableStyle {
        border-bottom: dashed 1px #0088cc;
        cursor: pointer;
    }
    .li_nav{
        background: #575962;
    }
    .li_nav_text{
        color: white !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">TITO Monitoring</h3>
			</div>
		</div>
	</div>
	<div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-demo pull-right" id="tito_monitoring_nav" data-code-preview="true" data-code-html="true" data-code-js="false" style="margin-top: -5rem;">
                    <div class="m-demo__preview" style="padding:0px !important;background: #282a3c;">
                        <ul class="dms m-nav m-nav--inline" style="padding: 0;">
                            <li class="m-nav__item active px-3 py-2">
                                <a href="#" class="titoMainNav m-nav__link active" data-navcontent="tito_monitoring">
                                    <i class="m-nav__link-icon fa fa-tasks"></i>
                                    <span class="m-nav__link-text">TITO Monitoring</span>
                                </a>
                            </li>
                            <!-- <li class="m-nav__item px-3 py-2">
                                <a href="#" class="titoMainNav m-nav__link" data-navcontent="tito_report">
                                    <i class="m-nav__link-icon fa fa-pie-chart"></i>
                                    <span class="m-nav__link-text">TITO Report</span>
                                </a>
                            </li>  -->
                            <li class="m-nav__item px-3 py-2">
                                <a href="#" class="titoMainNav m-nav__link" data-navcontent="tito_record">
                                    <i class="m-nav__link-icon fa fa-pie-chart"></i>
                                    <span class="m-nav__link-text">TITO Record</span>
                                </a>
                            </li> 
                            <li class="m-nav__item px-3 py-2" id="access_nav" style="display:none;">
                                <a href="#" class="titoMainNav m-nav__link" data-navcontent="tito_monitoring_access">
                                    <i class="m-nav__link-icon fa fa-user"></i>
                                    <span class="m-nav__link-text">Monitoring Access</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="tito_monitoring" class="row monitoring_content">
            <div class="col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__body">
                        <div class="row pt-4">
                            <div class="col-6 mb-3">
                                <label class="font-weight-bold">Class</label>
                                <select class="custom-select form-control" id="pending_monitoring_class" name="pending_monitoring_class">
                                    <option value="All">ALL</option>
                                    <option value="Agent">Ambassador</option>
                                    <option value="Admin">SZ Family</option>
                                </select>
                            </div>
                            <div class="col-6 mb-3">
                                <label class="font-weight-bold">Account</label>
                                <select class="form-control m-select2" id="pending_monitoring_acc" name="pending_monitoring_acc">
                                <!-- <option value="0">--</option> -->
                                </select>
                            </div>
                            <div class="col-4">
                                <label class="font-weight-bold">Request Type</label>
                                <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                    <label class="m-checkbox m-checkbox--state-success">
                                        <input type="checkbox" class="tito_req_type" data-type="tito"> TITO
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--state-success">
                                        <input type="checkbox" class="tito_req_type" data-type="tito_ahr"> TITO with AHR
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-3">
                                <label class="font-weight-bold">Status</label>
                                <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                    <label class="m-checkbox m-checkbox--state-info">
                                        <input type="checkbox" class="tito_req_status" data-stat="2"> Pending
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--state-warning">
                                        <input type="checkbox" class="tito_req_status" data-stat="12"> Missed
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                </div>
                            </div>
                        </div>			 
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">Tito Request Lists</h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                    All
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 38px;"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-share"></i>
                                                            <span class="m-nav__link-text">Activity</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                            <span class="m-nav__link-text">Messages</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-info"></i>
                                                            <span class="m-nav__link-text">FAQ</span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="#" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                            <span class="m-nav__link-text">Support</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-6 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Employee</label>
                                    <div class="col-10">
                                        <select class="form-control m-select2" id="pending_monitoring_emp_list" name="pending_monitoring_emp_list">
                                        <!-- <option value="0">--</option> -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Search</label>
                                    <div class="col-10">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Type any keyword..." id="tito_pending_monitoring_search">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12" id="tito_pending_monitoring_div">
                                <div id="tito_pending_monitoring_datatable"></div>
                            </div>
                        </div>			 
                    </div>
                </div>
            </div>
        </div>        
        <div id="tito_record" class="row monitoring_content" style="display:none;">
            <div class="col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__body">
                        <div class="row pt-4">
                            <div class="col-3 mb-3">
                                <label class="font-weight-bold">Class</label>
                                <select class="custom-select form-control" id="record_monitoring_class" name="record_monitoring_class">
                                    <option value="All">ALL</option>
                                    <option value="Agent">Ambassador</option>
                                    <option value="Admin">SZ Family</option>
                                </select>
                            </div>
                            <div class="col-4 mb-3">
                                <label class="font-weight-bold">Sched Date</label>
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Schedule Date" id="tito_monitoring_record_date_range">
                                    <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Filed">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5 mb-3">
                                <label class="font-weight-bold">Account</label>
                                <select class="form-control m-select2" id="record_monitoring_acc" name="record_monitoring_acc">
                                </select>
                            </div>
                            <div class="col-3">
                                <label class="font-weight-bold">Request Type</label>
                                <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                    <label class="m-checkbox m-checkbox--state-primary">
                                        <input type="checkbox" class="tito_rec_type" data-type="tito"> TITO
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--state-primary">
                                        <input type="checkbox" class="tito_rec_type" data-type="tito_ahr"> TITO with AHR
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="font-weight-bold">Status</label>
                                <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                    <label class="m-checkbox m-checkbox--state-success">
                                        <input type="checkbox" class="tito_rec_status" data-stat="5"> Approved
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--state-danger">
                                        <input type="checkbox" class="tito_rec_status" data-stat="6"> Disapproved
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-5 mb-3">
                                <label class="font-weight-bold">Employee</label>
                                <select class="form-control m-select2" id="record_monitoring_emp" name="record_monitoring_emp">
                                </select>
                            </div>
                        </div>			 
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">Tito Monitoring Records</h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <!-- <div class="col-6 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Employee</label>
                                    <div class="col-10">
                                        <select class="form-control m-select2" id="record_monitoring_emp_list" name="record_monitoring_emp_list">
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-12 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-1 col-form-label">Search</label>
                                    <div class="col-11">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Type any keyword..." id="tito_record_monitoring_search">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12" id="tito_record_monitoring_div">
                                <div id="tito_record_monitoring_datatable"></div>
                            </div>
                        </div>			 
                    </div>
                </div>
            </div>
        </div>
        <!-- <div id="tito_report" class="row monitoring_content" style="display:none;">
            <div class="col-4">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__body px-4">
                        <div class="row pt-2">
                            <div class="col-12 mb-3">
                                <label class="font-weight-bold">Class</label>
                                <select class="custom-select form-control" id="report_record_class" name="report_record_class">
                                    <option value="0">ALL</option>
                                    <option value="2">Pending</option>
                                    <option value="12">Missed</option>
                                </select>
                            </div>
                            <div class="col-12 mb-3">
                                <label class="font-weight-bold">Date Range</label>
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Schedule Date" id="tito_report_date_range">
                                    <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Filed">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <label class="font-weight-bold">Request Type</label>
                                <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                    <label class="m-checkbox m-checkbox--state-success">
                                        <input type="checkbox" class="tito_rec_type" data-type="tito"> TITO
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--state-success">
                                        <input type="checkbox" class="tito_rec_type" data-type="tito_ahr"> TITO with AHR
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-12 mb-3">
                                <label class="font-weight-bold">Status</label>
                                <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                    <label class="m-checkbox m-checkbox--state-success">
                                        <input type="checkbox" class="tito_rec_status" data-stat="5"> Approved
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--state-danger">
                                        <input type="checkbox" class="tito_rec_status" data-stat="6"> Disapproved
                                        <span style="margin-top: 0.3rem;"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__body">
                    </div>
                </div>
            </div> 
            <div class="col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__body px-4">
                        <div class="row pt-3">
                            <div class="col-6 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Employee</label>
                                    <div class="col-10">
                                        <select class="form-control m-select2" id="report_emp_list" name="report_emp_list">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Search</label>
                                    <div class="col-10">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Type any keyword..." id="report_record_search" name="report_record_search">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span>
                                                    <i class="la la-search"></i>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div id="tito_monitoring_access" class="row monitoring_content" style="display:none;">
        </div>
	</div>
</div>
<div class="modal fade" id="access_type_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span>
                    <i class="flaticon flaticon-clock text-light mr-1" style="font-size: 26px;"></i>
                </span>
                <h5 class="modal-title mt-2" id="exampleModalLabel">Access Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body py-3 px-1">
                <div class="col-12">
                    <div style="margin-top:1rem; margin-bottom:1rem;">
                        <div class="row rowDtrPrescriptive">
                            <div class="col-12">
                                <div class="col">
                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                        <input type="radio" name="monitoringAccessType" value="limited">
                                        Limited Access
                                        <span style="margin-top: 0.4rem !important;"></span>
                                    </label>
                                </div>
                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                    Access is limited to direct subordinates and down.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;">
                    </div>
                    <div style="margin-top:1rem; margin-bottom:1rem;">
                        <div class="row rowAccessType">
                            <div class="col-12">
                                <div class="col">
                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px; font-weight: 400;">
                                        <input type="radio" name="monitoringAccessType" value="full">
                                        Full Access
                                        <span style="margin-top: 0.4rem !important;"></span>
                                    </label>
                                </div>
                                <div class="col comment" style="font-size: 13px; padding-left: 3.3rem !important;">
                                    Can view all records
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="save_access">Save</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approversInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Approval Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
                    <div id="approvalBody"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tito_request_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="tito_request_form" method="post">
                <div class="modal-header">
                    <span>
                        <i class="flaticon flaticon-clock text-light mr-1" style="font-size: 26px;"></i>
                    </span>
                    <h5 class="modal-title mt-2" id="exampleModalLabel">Time In Time Out Request Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body py-2">
                    <div class="row py-1" style="margin-left: -25px;margin-right: -25px;padding-left: 0.2rem;padding-right: 0.2rem;">
                        <span class="ml-3" style="font-size: 1.2rem;font-weight: 500;color: #5d4747;"><i class="fa fa-calendar-o mr-2" style="font-size: 1.1rem;"></i>Schedule</span>
                        <div class="col">
                            <hr style="margin-top: 0.8rem !important;border-top: 1px dashed rgba(0,0,0,.1)!important;">
                        </div>
                    </div>
                    <div class="row px-2">
                        <div class="col-12 mb-2 hidden_elements" id="with_ahr_err" style="display:none;">
                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show mb-0" role="alert" style="border-color: rgb(244 81 108) !important;">
                                <div class="m-alert__icon mini_alert bg-danger">
                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 0px 0px 0px 0px !important;">
                                        <i class="fa fa-warning text-light"></i>
                                    </span>
                                    <span style="border-left-color: #343a40 !important;"></span>
                                </div>
                                <div class="m-alert__text mini_alert text-danger"><b>Cannot file TITO.</b> This schedule has an attached AHR. <span style="color: black;" id="remove_ahr_link" style="display;none">Remove AHR?</span> </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="font-weight-bold" style="font-size: 0.9rem;">Date</label>
                            <div class="form-group m-form__group">
                                <div class='input-group pull-right'>
                                <input type='text' class="form-control m-input datepicker  no-edit" readonly placeholder="Select date" id="tito_sched_date" name="tito_sched_date"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="font-weight-bold" style="font-size: 0.9rem;">Shift</label>
                            <div class="form-group m-form__group">
                            <select class="form-control m-select2  no-edit" id="tito_shift" name="tito_shift" >
                                    <!-- <option value="0">--</option> -->
                                </select>
                            </div>
                        </div>
                        <div class="col-12 hidden_elements" id="sched_details" style="display:none;">
                            <label class="view_sched_details" style="font-size: 0.9rem;" id="view_sched_details">View Sched Details</label>
                        </div>
                    </div>
                    <div class="row py-1" style="margin-left: -25px;margin-right: -25px;padding-left: 0.2rem;padding-right: 0.2rem;">
                        <span class="ml-3" style="font-size: 1.2rem;font-weight: 500;color: #5d4747;"><i class="fa fa-clock-o mr-2" style="font-size: 1.1rem;"></i>Logs</span>
                        <div class="col">
                            <hr style="margin-top: 0.8rem !important;border-top: 1px dashed rgba(0,0,0,.1)!important;">
                        </div>
                    </div>
                    <div class="row px-2">
                        <div class="col-7">
                            <div class="row">
                            <div class="col-12 text-center" style="font-size: 0.9rem;"><span class="font-weight-bold">Clock In </span>
								<span class="text-danger hidden_elements" id="titoLate" style="display:none;">- Late</span></div>
                                <div class="col-12 pb-3">
                                    <div class="row">
                                        <div class="col-7">
                                            <label style="font-size:0.8rem">Date</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input datepicker tito_input_fields  no-edit" readonly id="tito_start_date" placeholder="Clock-In Date" name="tito_start_date" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <label style="font-size:0.8rem">Time</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input timepicker tito_input_fields  no-edit" readonly id="tito_start_time" name="tito_start_time" />
                                                    <div class="input-group-append" d>
                                                        <span class="input-group-text">
                                                            <i class="la la-clock-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-center" style="font-size: 0.9rem;"><span class="font-weight-bold">Clock Out </span>
								<span class="text-danger hidden_elements" id="titoUndertime" style="display:none;">- Undertime</span></div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-7">
                                            <label style="font-size:0.8rem">Date</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input datepicker tito_input_fields  no-edit" readonly id="tito_end_date" placeholder="Clock-Out Date" name="tito_end_date" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <label style="font-size:0.8rem">Time</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input timepicker tito_input_fields  no-edit" readonly id="tito_end_time" name="tito_end_time" />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-clock-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-2">
                                        <h3 id="renderedOt" class="font-size-12"><i class="fa fa-hourglass-3 mr-2"></i>Total Time of Work</h3>
                                        <div class="text-center" style="font-size: 1.6rem;" id="tito_total_time" data-origtotal="0"  data-origdatestart=""  data-origdateend=""   data-newdatestart=""  data-newdateend=""     data-newtotal="0" data-acctimeid="0"  data-otpre="0"  data-otafter="0"   data-otword="0"   data-titoid="0" >00 hr, 00 min</div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group mb-0">
                                        <h3 class="font-size-12"><i class="fa fa-tags mr-2"></i>Additional Hour</h3>
                                        <div class="row px-5">
                                            <!--<div class="col-12" style="border: 1px dashed;" >
                                                <div class="row py-2">
                                                    <div class="col-12 text-center">
                                                        <i style="font-size: 40px;" class="flaticon-notes"></i>
                                                    </div>
                                                    <div class="col-12 text-center" style="font-size: 0.9rem;">
                                                        No Excess time duration for AHR
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="" id="ahrOptions">
                                                <table class="table hidden_elements">
                                                    <tbody class=" font-size-12">
                                                        <tr id="ot_row_pre">
                                                            <th scope="row" style="padding: 0.5rem;">Pre Shift</th>
                                                            <td id="ot_time_pre" class="ahr_form" data-ottype="pre" style="padding: 0.5rem;">00 hr, 00 min</td>
                                                            <td style="padding: 0.5rem;">
                                                                <label class="m-checkbox mjm-Checkbox">
                                                                <input type="checkbox"  id="checkPre" data-otstartdatetime="" data-otenddatetime="" data-ahrid="" disabled>
                                                                    <span></span>
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr id="ot_row_after">
                                                            <th scope="row" style="padding: 0.5rem;">After Shift</th>
                                                            <td id="ot_time_after" class="ahr_form" data-ottype="after" style="padding: 0.5rem;">00 hr, 00 min</td>
                                                            <td style="padding: 0.5rem;">
                                                                <label class="m-checkbox mjm-Checkbox">
                                                                <input type="checkbox"  id="checkAfter" data-otstartdatetime="" data-otenddatetime="" data-ahrid=""  disabled><span></span>
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr id="ot_row_word">
                                                            <th scope="row" style="padding: 0.5rem;">WORD</th>
                                                            <td id="ot_time_word" style="padding: 0.5rem;" >00 hr, 00 min</td>
                                                            <td style="padding: 0.5rem;">
                                                                <label class="m-checkbox mjm-Checkbox">
                                                                    <input type="checkbox" id="checkWord" data-otstartdatetime="" data-otenddatetime="" data-ahrid=""  disabled><span></span>
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> <!--- -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3 hidden_elements"  id="ahrNotif" style="display:none">
                            <div id="info_notif_ahr" class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style="border-color: rgb(54, 163, 247) !important;">
                                <div class="m-alert__icon mini_alert bg-info">
                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 0px 0px 0px 0px !important;">
                                        <i class="fa fa-info-circle text-light"></i>
                                    </span>
                                    <span style="border-left-color: #343a40 !important;"></span>
                                </div>
                                <div class="m-alert__text text-center mini_alert text-primary"><b class="mr-2">With AHR Filing:</b><span id="info_ahr_types">Pre-shift, After-shift, Work on Rest day</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row py-1" style="margin-left: -25px;margin-right: -25px;padding-left: 0.2rem;padding-right: 0.2rem;">
                        <span class="ml-3" style="font-size: 1.2rem;font-weight: 500;color: #5d4747;"><i class="fa fa-pencil-square-o mr-2" style="font-size: 1.1rem;"></i>Reason for Filing</span>
                        <div class="col">
                            <hr style="margin-top: 0.8rem !important;border-top: 1px dashed rgba(0,0,0,.1)!important;">
                        </div>
                    </div>
                    <div class="row px-2">
                        <div class="col-12">
                        <div class="form-group m-form__group hidden_elements" style="display:none">
                                <textarea class="form-control m-input text-counter tito_input_fields no-edit" data-approverid="" id="tito_reason" name="tito_reason" rows="5" placeholder="Input your reason here..." maxlength="100"></textarea>
                                <span class="tito_char_remaining" style="font-size: 13px;font-weight: 400;">100 characters remaining</span>
                            </div>
                        </div>
                        <div class="col-12 ahr_quote pt-3 pb-1">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                <span class="mb-0 text-capitalize" id="reasonRecord"></span>
                                <i class="fa fa-quote-right"></i>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger hideOnView"  id="btnDisApprove"  onclick="titoRequestAction(6)">Disapprove</button>
                    <button type="button" class="btn btn-success hideOnView" id="btnApprove" onclick="titoRequestAction(5)">Approve</button>
                </div>
            </form>
        </div>
    </div> 
</div>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/tito_general.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/tito_monitoring_access.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/tito_monitoring.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/tito_monitoring_pending.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/tito_monitoring_record.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/mjm_tito_request.js" type="text/javascript"></script> 





