<style>
    .m-checkbox>span, .m-radio>span {
        top: -3px !important;
        /* height: 16px !important; */
    }
    .view_sched_details{
        cursor: pointer;
        color: #0d5ff3;
    }
    .view_sched_details:hover{
        text-decoration: underline;
        color: #720ddc;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto"> 
				<h3 class="m-subheader__title m-subheader__title--separator">My Time-in & Time-out Request <sup>v2</sup> </h3><span class="m-badge m-badge--success m-badge--wide m-badge--rounded">new</span>
				
				<!-- <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">My Time-in & Time-out Request</span>
						</a>
					</li>
				</ul> -->
			</div>
		</div>
	</div>
	<div class="m-content">
    <div class="m-portlet m-portlet--tabs m-portlet--success m-portlet--head-solid-bg m-portlet--head-sm">
			<div class="m-portlet__head" style="background: #2c2e3eeb;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text" style="color: #e2e0e0 !important;">
						TITO Requests
						</h3>
					</div>			
				</div>
				<div class="m-portlet__head-tools">
					<ul class="nav nav-tabs m-tabs m-tabs-line  m-tabs-line--right" id="tito_request_tabs" role="tablist">
                        <li class="tito_request_nav nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#pending_tito" role="tab">
                                <i class="fa fa-refresh"></i> Pending</a>
                        </li>
                        <li class="tito_request_nav nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#record_tito" role="tab">
                                <i class="la la-th-list"></i> Records</a>
                        </li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="tab-content">
                    <div class="tab-pane active" id="pending_tito" role="tabpanel">
                        <div class="row">
                            <div class="col-2 mb-3">
                                <select class="custom-select form-control" id="requestStatus" name="requestStatus">
                                    <option value="0">ALL</option>
                                    <option value="2">Pending</option>
                                    <option value="12">Missed</option>
                                </select>
                            </div>
                            <div class="col-8">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Type any keyword..." id="pending_tito_search">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-2">
                                <button id="file_tito_request" class="btn btn-outline-success m-btn m-btn--icon m-btn--outline-2x pull-right">
                                    <span>
										<i class="fa fa-file-o"></i>
										<span>Request</span>
									</span>
                                </button>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <label class="col-form-label col-2">
                                        <b>Request Type:</b>
                                    </label>
                                    <div class="col-8 col-10">
                                        <div class="input-group-append">
                                            <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                                <label class="m-checkbox m-checkbox--state-success">
                                                    <input type="checkbox" class="tito_req_type" data-type="tito"> TITO
                                                    <span style="margin-top: 0.3rem;"></span>
                                                </label>
                                                <label class="m-checkbox m-checkbox--state-success">
                                                    <input type="checkbox" class="tito_req_type" data-type="tito_ahr"> TITO with AHR
                                                    <span style="margin-top: 0.3rem;"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" id="tito_request_div">
                                <div id="tito_request_datatable"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="record_tito" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="row">
                                            <label class="col-form-label col-4 col-sm-4 col-md-4 col-lg-3 col-xl-4">
                                                <b>REQUEST STATUS</b>
                                            </label>
                                            <div class="col-8 col-sm-8 col-md-8 col-lg-9 col-xl-8">
                                                <div class="input-group-append">
                                                    <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                                        <label class="m-checkbox m-checkbox--state-success">
                                                            <input type="checkbox" class="tito_rec_status" data-stat="5"> Approved
                                                            <span style="margin-top: 0.3rem;"></span>
                                                        </label>
                                                        <label class="m-checkbox m-checkbox--state-danger">
                                                            <input type="checkbox" class="tito_rec_status" data-stat="6"> Disapproved
                                                            <span style="margin-top: 0.3rem;"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="row">
                                            <label class="col-form-label col-sm-3 col-md-4 col-lg-3 col-xl-3">
                                                <b>SCHED DATE</b>
                                            </label>
                                            <div class="col-sm-9 col-md-8 col-lg-9 col-xl-9">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="input-group pull-right" id="dateRangePersonalRecord">
                                                            <input type="text" class="form-control m-input" readonly="" placeholder="Schedule Date" id="tito_record_date_range">
                                                            <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Filed">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="row">
                                            <label class="col-form-label col-4 col-sm-4 col-md-4 col-lg-3 col-xl-4">
                                                <b>REQUEST TYPE</b>
                                            </label>
                                            <div class="col-8 col-sm-8 col-md-8 col-lg-9 col-xl-8">
                                                <div class="input-group-append">
                                                    <div class="m-checkbox-inline" style="margin-top: 0.6rem !important;">
                                                        <label class="m-checkbox m-checkbox--state-success">
                                                            <input type="checkbox" class="tito_rec_type" data-type="tito"> TITO
                                                            <span style="margin-top: 0.3rem;"></span>
                                                        </label>
                                                        <label class="m-checkbox m-checkbox--state-success">
                                                            <input type="checkbox" class="tito_rec_type" data-type="tito_ahr"> TITO with AHR
                                                            <span style="margin-top: 0.3rem;"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="row">
                                            <label class="col-form-label col-sm-3 col-md-4 col-lg-3 col-xl-3">
                                                <b>SEARCH ID</b>
                                            </label>
                                            <div class="col-sm-9 col-md-8 col-lg-9 col-xl-9">
                                                <div class="form-group m-form__group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="TITO ID" id="titoRecordSearch">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data below.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: -8px;">
                                <hr style="padding-top:1px;" class="bg-metal">
                            </div>
                            <!-- <div class="col-md-12 mb-1">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by AHR ID, Schedule, Date Filed" id="personalAhrRecordSearch">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data below.</span>
                                </div>
                            </div> -->
                            <div class="col-12" id="tito_record_div">
                                <div id="tito_record_datatable"></div>
                            </div>
                        </div>
                    </div> 
				</div>
			</div>
		</div>	
	</div>
</div>
<!-- MODALS -->
<div class="modal fade" id="tito_request_modal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="tito_request_form" method="post">
                <div class="modal-header">
                    <span>
                        <i class="flaticon flaticon-clock text-light mr-1" style="font-size: 26px;"></i>
                    </span>
                    <h5 class="modal-title mt-2" id="exampleModalLabel">Time In Time Out Request Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body py-2">
                    <div class="row py-1" style="margin-left: -25px;margin-right: -25px;padding-left: 0.2rem;padding-right: 0.2rem;">
                        <span class="ml-3" style="font-size: 1.2rem;font-weight: 500;color: #5d4747;"><i class="fa fa-calendar-o mr-2" style="font-size: 1.1rem;"></i>Schedule</span>
                        <div class="col">
                            <hr style="margin-top: 0.8rem !important;border-top: 1px dashed rgba(0,0,0,.1)!important;">
                        </div>
                    </div>
                    <div class="row px-2">
                        <div class="col-12 mb-2 hidden_elements" id="with_ahr_err" style="display:none;">
                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show mb-0" role="alert" style="border-color: rgb(244 81 108) !important;">
                                <div class="m-alert__icon mini_alert bg-danger">
                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 0px 0px 0px 0px !important;">
                                        <i class="fa fa-warning text-light"></i>
                                    </span>
                                    <span style="border-left-color: #343a40 !important;"></span>
                                </div>
                                <div class="m-alert__text mini_alert text-danger"><b>Cannot file TITO.</b> This schedule has an attached AHR. <span style="color: black;" id="remove_ahr_link" style="display;none">Remove AHR?</span> </div>
                            </div>
                        </div>
                        <div class="col-12 mb-2 hidden_elements" id="with_tito_err" style="display:none;">
                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show mb-0" role="alert" style="border-color: rgb(244 81 108) !important;">
                                <div class="m-alert__icon mini_alert bg-danger">
                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 0px 0px 0px 0px !important;">
                                        <i class="fa fa-warning text-light"></i>
                                    </span>
                                    <span style="border-left-color: #343a40 !important;"></span>
                                </div>
                                <div class="m-alert__text mini_alert text-danger"><b>Cannot file TITO.</b> You have already Filed a TITO request for this shift.</span> </div>
                            </div>
                        </div>
                        <div class="col-12 mb-2 hidden_elements" id="max_tito_err" style="display:none;">
                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show mb-0" role="alert" style="border-color: rgb(244 81 108) !important;">
                                <div class="m-alert__icon mini_alert bg-danger">
                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 0px 0px 0px 0px !important;">
                                        <i class="fa fa-warning text-light"></i>
                                    </span>
                                    <span style="border-left-color: #343a40 !important;"></span>
                                </div>
                                <div class="m-alert__text mini_alert text-danger"><b>Cannot file TITO.</b> You are only allowed to file a Maximum of 2 TITO request for this schedule date.</span> </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="font-weight-bold" style="font-size: 0.9rem;">Date</label>
                            <div class="form-group m-form__group">
                                <div class='input-group pull-right'>
                                    <input type='text' class="form-control m-input datepicker" readonly placeholder="Select date" id="tito_sched_date" name="tito_sched_date"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="font-weight-bold" style="font-size: 0.9rem;">Shift</label>
                            <div class="form-group m-form__group">
                                <select class="form-control m-select2" id="tito_shift" name="tito_shift" disabled="disabled">
                                    <!-- <option value="0">--</option> -->
                                </select>
                            </div>
                        </div>
                        <div class="col-12 hidden_elements" id="sched_details" style="display:none;">
                            <label class="view_sched_details" style="font-size: 0.9rem;" id="view_sched_details">View Sched Details</label>
                        </div>
                    </div>
                    <div class="row py-1" style="margin-left: -25px;margin-right: -25px;padding-left: 0.2rem;padding-right: 0.2rem;">
                        <span class="ml-3" style="font-size: 1.2rem;font-weight: 500;color: #5d4747;"><i class="fa fa-clock-o mr-2" style="font-size: 1.1rem;"></i>Logs</span>
                        <div class="col">
                            <hr style="margin-top: 0.8rem !important;border-top: 1px dashed rgba(0,0,0,.1)!important;">
                        </div>
                    </div>
                    <div class="row px-2">
                        <div class="col-7">
                            <div class="row">
                            <div class="col-12 text-center" style="font-size: 0.9rem;"><span class="font-weight-bold">Clock In </span>
								<span class="text-danger hidden_elements" id="titoLate" style="display:none;">- Late</span></div>
                                <div class="col-12 pb-3">
                                    <div class="row">
                                        <div class="col-7">
                                            <label style="font-size:0.8rem">Date</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input datepicker tito_input_fields" readonly id="tito_start_date" placeholder="Clock-In Date" name="tito_start_date" disabled="disabled" data-date=""/>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <label style="font-size:0.8rem">Time</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input timepicker tito_input_fields" readonly id="tito_start_time" name="tito_start_time" disabled="disabled"/>
                                                    <div class="input-group-append" d>
                                                        <span class="input-group-text">
                                                            <i class="la la-clock-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-center" style="font-size: 0.9rem;"><span class="font-weight-bold">Clock Out </span>
								<span class="text-danger hidden_elements" id="titoUndertime" style="display:none;">- Undertime</span></div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-7">
                                            <label style="font-size:0.8rem">Date</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input datepicker tito_input_fields" readonly id="tito_end_date" placeholder="Clock-Out Date" name="tito_end_date" disabled="disabled" data-date=""/>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <label style="font-size:0.8rem">Time</label>
                                            <div class="form-group m-form__group">
                                                <div class='input-group pull-right' id='dateRangePersonalRecord'>
                                                    <input type='text' class="form-control m-input timepicker tito_input_fields" readonly id="tito_end_time" name="tito_end_time" disabled="disabled"/>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-clock-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-2">
                                        <h3 id="renderedOt" class="font-size-12"><i class="fa fa-hourglass-3 mr-2"></i>Total Time of Work</h3>
                                        <div class="text-center" style="font-size: 1.6rem;" id="tito_total_time" data-origtotal="0"  data-origdatestart=""  data-origdateend=""   data-newdatestart=""  data-newdateend=""     data-newtotal="0" data-acctimeid="0"  data-otpre="0"  data-otafter="0"   data-otword="0" >00 hr, 00 min</div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group mb-0">
                                        <h3 class="font-size-12"><i class="fa fa-tags mr-2"></i>Additional Hour</h3>
                                        <div class="row px-5">
                                            <!--<div class="col-12" style="border: 1px dashed;" >
                                                <div class="row py-2">
                                                    <div class="col-12 text-center">
                                                        <i style="font-size: 40px;" class="flaticon-notes"></i>
                                                    </div>
                                                    <div class="col-12 text-center" style="font-size: 0.9rem;">
                                                        No Excess time duration for AHR
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="" id="ahrOptions">
                                                <table class="table ">
                                                    <tbody class=" font-size-12">
                                                        <tr id="ot_row_pre">
                                                            <th scope="row" style="padding: 0.5rem;">Pre Shift</th>
                                                            <td id="ot_time_pre" class="ahr_form" data-ottype="pre" style="padding: 0.5rem;">00 hr, 00 min</td>
                                                            <td style="padding: 0.5rem;">
                                                                <label class="m-checkbox mjm-Checkbox">
                                                                <input type="checkbox"  id="checkPre" data-otstartdatetime="" data-otenddatetime="" disabled>
                                                                    <span></span>
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr id="ot_row_after">
                                                            <th scope="row" style="padding: 0.5rem;">After Shift</th>
                                                            <td id="ot_time_after" class="ahr_form" data-ottype="after" style="padding: 0.5rem;">00 hr, 00 min</td>
                                                            <td style="padding: 0.5rem;">
                                                                <label class="m-checkbox  mjm-Checkbox">
                                                                <input type="checkbox"  id="checkAfter" data-otstartdatetime="" data-otenddatetime="" disabled><span></span>
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr id="ot_row_word">
                                                            <th scope="row" style="padding: 0.5rem;">WORD</th>
                                                            <td id="ot_time_word" style="padding: 0.5rem;" >00 hr, 00 min</td>
                                                            <td style="padding: 0.5rem;">
                                                                <label class="m-checkbox  mjm-Checkbox">
                                                                <input type="checkbox" id="checkWord" data-otstartdatetime="" data-otenddatetime="" disabled><span></span>
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> <!--- -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-3 hidden_elements"  id="ahrNotif" style="display:none">
                            <div id="info_notif_ahr" class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style="border-color: rgb(54, 163, 247) !important;">
                                <div class="m-alert__icon mini_alert bg-info">
                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 0px 0px 0px 0px !important;">
                                        <i class="fa fa-info-circle text-light"></i>
                                    </span>
                                    <span style="border-left-color: #343a40 !important;"></span>
                                </div>
                                <div class="m-alert__text text-center mini_alert text-primary"><b class="mr-2">With AHR Filing:</b><span id="info_ahr_types">Pre-shift, After-shift, Work on Rest day</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row py-1" style="margin-left: -25px;margin-right: -25px;padding-left: 0.2rem;padding-right: 0.2rem;">
                        <span class="ml-3" style="font-size: 1.2rem;font-weight: 500;color: #5d4747;"><i class="fa fa-pencil-square-o mr-2" style="font-size: 1.1rem;"></i>Reason for Filing</span>
                        <div class="col">
                            <hr style="margin-top: 0.8rem !important;border-top: 1px dashed rgba(0,0,0,.1)!important;">
                        </div>
                    </div>
                    <!--<div class="row px-2">
                        <div class="col-12">
                            <div class="form-group m-form__group">
                                <textarea class="form-control m-input text-counter tito_input_fields" id="tito_reason" name="tito_reason" rows="5" placeholder="Input your reason here..." maxlength="100" disabled="disabled"></textarea>
                                <span class="tito_char_remaining" style="font-size: 13px;font-weight: 400;">100 characters remaining</span>
                            </div>
                        </div>
                    </div>-->
					 <div class="row px-2">
                        <div class="col-12">
                        <div class="form-group m-form__group hidden_elements" style="display:none" id="divReason1">
                                <textarea class="form-control m-input text-counter tito_input_fields no-edit" data-approverid="" id="tito_reason" name="tito_reason" rows="5" placeholder="Input your reason here..." maxlength="100"></textarea>
                                <span class="tito_char_remaining" style="font-size: 13px;font-weight: 400;">100 characters remaining</span>
                            </div>
                        </div>
                        <div class="col-12 ahr_quote pt-3 pb-1"  id="divReason2">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                <span class="mb-0 text-capitalize" id="reasonRecord"></span>
                                <i class="fa fa-quote-right"></i>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="requestAhrBtn">File Request</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="tito_sched_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <form id="ahr_request_form" method="post">
                <div class="modal-header">
                    <span>
                        <i class="flaticon flaticon-clock text-light mr-1" style="font-size: 26px;"></i>
                    </span>
                    <h5 class="modal-title mt-2" id="exampleModalLabel">Schedule Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body py-3 px-1" id="sched_log_details">
                    <div class="col-12">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="" style="background:#d8d8d8;">
                                    <th class="align-middle p-2" colspan="3" style="font-size: 0.7rem;">SCHED ID: 20980978988</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 1rem;" class="text-center">
                                <tr>
                                    <!-- <th class="align-middle" scope="row" rowspan="2" style="padding: 0.5rem;">10:00 AM - 7:00 PM </br> <div style="font-size: 0.8rem;" class="text-primary">(Locked by AHR)</div></th> -->
                                    <td style="padding: 0.5rem;">IN</td>
                                    <td style="padding: 0.5rem;">2020-01-22 9:30 AM</td>
                                </tr>
                                <tr>
                                    <td style="padding: 0.5rem;">OUT</td>
                                    <td style="padding: 0.5rem;">2020-01-22 8:30 PM</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="tito_ahr_locked_details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <span>
                    <i class="flaticon flaticon-clock text-light mr-1" style="font-size: 26px;"></i>
                </span>
                <h5 class="modal-title mt-2" id="exampleModalLabel">Schedule Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body py-3 px-1" id="attach_ahr_details">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">close</button>
                <button type="submit" class="btn btn-primary" id="deleteAhrBtn">Delete AHR</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approversInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Approval Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
                    <div id="approvalBody"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>assets/src/custom/js/tito/tito_general.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/tito_request.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/src/custom/js/tito/mjm_tito_request.js" type="text/javascript"></script>





