<style>
    img {
        max-width: 100%;
        max-height: 100vh;
        height: auto;
    }
</style>
<?php 
date_default_timezone_set('Asia/Manila'); 
$current_time = time();
$time = $current_time - (5 * 60);
$time -= $time % (60 * 5);
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Support Tracker</h3>           
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
            <div class="m-portlet__body">
                <div class="m-section" id="datatable_div">
                    <div class="m-form m-form--label-align-right m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">
                                    <div class="col-md-8">
                                        <div style="width: 100%; padding: 5px" class="m-input-icon m-input-icon--left m-input-icon--right">
                                            <input type="text" class="form-control m-input m-input--solid" placeholder="Search for..." id="searchField" autocomplete="off">
                                            <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
                                            <span class="m-input-icon__icon m-input-icon__icon--right filter" style="cursor: pointer;"  data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Show search option"><span><i class="la la-filter"></i></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                <a href="#" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info addNewButton">Add New 
                                    <i class="fa fa-plus"></i>
                                </a>
                                <a href="#" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-info exportButton">
                                    <span>
                                        <span>Export File </span>
                                        <i class="fa fa-file-excel-o"></i>
                                    </span>
                                </a>
                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                            </div>
                        </div>
                    </div>
                    <div id="offenseDatatable"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<form id="addModal" method="post">
    <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="max-width: 700px" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <div style="margin: 0 30px">
                        <div class="form-group m-form__group row">
                            <div style="margin: 30px auto;">
                                <h2 style="text-align: center!important;">Support Tracker</h2>
                                <h5 style="text-align: center!important;" class="text-muted">Create New Support Tracker</h5>
                            </div>
                        </div>
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="300">
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Title <span class="m--font-danger">*</span></label>
                                <input type="text" class="form-control m-input m-input--solid" placeholder="Type the title here" autocomplete="off" name="title" id="title" required>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Description <span class="m--font-danger">*</span></label>
                                <textarea class="form-control m-input m-input--solid" placeholder="Type the details here" name="description" id="description" required style="height: 100px;"></textarea>
                            </div>
                            <label class="m--font-bolder">Data Upload <span class="m--font-danger">*</span></label>
                            <div class="form-group m-form__group row">
                                <div class="col-3">
                                    <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" required id="select_type">
                                        <option value="1">Link</option>
                                        <option value="2">Image</option>
                                    </select>
                                </div>
                                <div class="col-9">
                                    <div class="input-group" id="type_display">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Paste the link here" name="link" id="link">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="la la-link glyphicon-th"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Status <span class="m--font-danger">*</span></label>
                                        <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" required name="status" id="status">
                                            <option value="">All Status</option>
                                            <option data-content="<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>Easy</span>" value="1">Easy</option>
                                            <option data-content="<span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>Normal</span>" value="2">Normal</option>
                                            <option data-content="<span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>Hard</span>" value="3">Hard</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Time Reported <span class="m--font-danger">*</span></label>
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter the time reported here" autocomplete="off" required name="time_report" id="time_report">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Time Acknowledged <span class="m--font-danger">*</span> <span class="btn p-1" data-toggle="m-popover" title="" data-content="Automatically paste same time to 'Time Confirmed'"><i class="fa fa-question-circle-o"></i></span></label>
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter the time acknowledged here" autocomplete="off" required name="time_acknowledge" id="time_acknowledge">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Time Confirmed <span class="m--font-danger">*</span></label>
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter the time confirmed here" autocomplete="off" required name="time_confirm" id="time_confirm">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Fixed By <span class="m--font-danger">*</span> <span class="btn p-1" data-toggle="m-popover" title="" data-content="Automatically paste same data to 'Acknowledged By' and 'Confirmed By'"><i class="fa fa-question-circle-o"></i></span></label>
                                <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="fixed" id="fixed">
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Acknowledged By <span class="m--font-danger">*</span></label>
                                <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="acknowledged" id="acknowledged">
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Confirmed By <span class="m--font-danger">*</span></label>
                                <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="confirmed" id="confirmed">
                                </select>
                            </div>
                        </div>
                        <div style="bottom: 0;" class="form-group m-form__group row">
                            <div style="margin: 10px auto;">
                                <button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-info">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide" data-scrollbar-shown="true" data-scrollable="true" data-max-height="350" style="max-height: 350px; height: 350px; position: relative; overflow: visible;">
                    <div id="mCSB_2" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0" style="max-height: none;">
                        <div id="mCSB_2_container" class="mCSB_container" style="position: relative; left: 0px;" dir="ltr">
                            <div id="modal_body1" class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide" data-scrollbar-shown="true" data-scrollable="true"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<form id="updatemodal" method="post">
    <div class="modal fade" id="faqsModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="max-width: 700px" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <div style="margin: 0 30px">
                        <div class="form-group m-form__group row">
                            <div style="margin: 30px auto;">
                                <h2 style="text-align: center!important;">Support Tracker</h2>
                                <h5 style="text-align: center!important;" class="text-muted">Update Existing Support Tracker</h5>
                            </div>
                        </div>
                        <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="300">
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Title <span class="m--font-danger">*</span></label>
                                <input readonly type="hidden" class="form-control m-input" name="upsupport_tracker_id" id="upsupport_tracker_id">
                                <input type="text" class="form-control m-input m-input--solid" placeholder="Type the title here" autocomplete="off" name="uptitle" id="uptitle" required>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Description <span class="m--font-danger">*</span></label>
                                <textarea class="form-control m-input m-input--solid" placeholder="Type the details here" name="updescription" id="updescription" required style="height: 100px;"></textarea>
                            </div>
                            <label class="m--font-bolder">Data Upload <span class="m--font-danger">*</span></label>
                            <div class="form-group m-form__group row">
                                <div class="col-3">
                                    <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" required id="upselect_type">
                                        <option value="1">Link</option>
                                        <option value="2">Image</option>
                                    </select>
                                </div>
                                <div class="col-9">
                                    <div class="input-group" id="uptype_display">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Status <span class="m--font-danger">*</span></label>
                                        <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" required name="upstatus" id="upstatus">
                                            <option value="">All Status</option>
                                            <option data-content="<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>Easy</span>" value="1">Easy</option>
                                            <option data-content="<span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>Normal</span>" value="2">Normal</option>
                                            <option data-content="<span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>Hard</span>" value="3">Hard</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Time Reported <span class="m--font-danger">*</span></label>
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter the time reported here" autocomplete="off" required name="uptime_report" id="uptime_report">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Time Acknowledged <span class="m--font-danger">*</span> </label>
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter the time acknowledged here" autocomplete="off" required name="uptime_acknowledge" id="uptime_acknowledge">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="message-text" class="m--font-bolder">Time Confirmed <span class="m--font-danger">*</span></label>
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter the time confirmed here" autocomplete="off" required name="uptime_confirm" id="uptime_confirm">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Fixed By <span class="m--font-danger">*</span></label>
                                <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="upfixed" id="upfixed">
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Acknowledged By <span class="m--font-danger">*</span></label>
                                <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="upacknowledged" id="upacknowledged">
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="m--font-bolder">Confirmed By <span class="m--font-danger">*</span></label>
                                <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="upconfirmed" id="upconfirmed">
                                </select>
                            </div>
                        </div>
                        <div style="bottom: 0;" class="form-group m-form__group row">
                            <div style="margin: 10px auto;">
                                <button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-info">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="modal fade" id="modal_filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="max-width: 600px" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div style="margin: 0 50px">
                    <div class="form-group m-form__group row">
                        <div style="margin: 50px auto;">
                            <h2 style="text-align: center!important;">Support Tracker</h2>
                            <h5 style="text-align: center!important;" class="text-muted">Search Option Filter</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="recipient-name" class="m--font-bolder">Date</label>
                                <input id="searchDate" type="text" class="form-control m-input m-input--solid" readonly="" placeholder="Select time">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="recipient-name" class="m--font-bolder">Status </label>
                                <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" id="searchStatus">
                                    <option value="">All Status</option>
                                    <option data-content="<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>Easy</span>" value="1">Easy</option>
                                    <option data-content="<span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>Normal</span>" value="2">Normal</option>
                                    <option data-content="<span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>Hard</span>" value="3">Hard</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label class="m--font-bolder">Acknowledged By </label>
                        <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" id="filter_acknowledge">

                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label class="m--font-bolder">Fixed By </label>
                        <select class="form-control m-bootstrap-select m_selectpicker m-input--solid" id="filter_fixed">

                        </select>
                    </div>
                    <div style="bottom: 0;" class="form-group m-form__group row">
                        <div style="margin: 10px auto;">
                            <button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
                            <!-- <button type="submit" class="btn btn-info">Send</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.m_selectpicker').selectpicker();
    $('#time_report, #time_confirm, #time_acknowledge, #uptime_in, #uptime_out').datetimepicker({
        todayHighlight: true,
        autoclose: true,
        pickerPosition: 'bottom-left',
        format: 'yyyy/mm/dd HH:ii P',
        showMeridian: true,
        todayBtn: true,
    });

    $('#time_acknowledge').change(function() {
        $('#time_confirm').val($('#time_acknowledge').val());
    })

    $('#fixed').change(function() {
        $('#acknowledged').selectpicker('val', $('#fixed').val());
        $('#confirmed').selectpicker('val', $('#fixed').val());
    })

    $('#select_type').change(function() {
        var id = $(this).val();
        var html = '';
        if(id == 1){
            html = 
            '<div class="input-group">'+
            '<input type="text" class="form-control m-input m-input--solid" placeholder="Paste the link here" name="link" id="link">'+
            '<div class="input-group-append">'+
            '<span class="input-group-text"><i class="la la-link glyphicon-th"></i></span>'+
            '</div>'+
            '</div>';
        } 
        if(id == 2) {
            html = 
            '<div class="input-group">'+
            '<input type="file" name="image" id="image">'+
            '</div>';
        }
        $('#type_display').html(html);
    })

    $('#upselect_type').change(function() {
        var id = $(this).val();
        var html = '';
        var select = '';
        var link = $(this).data('link');
        var image = $(this).data('image');
        var srcimg = '<?php echo base_url()?>uploads/support_tracker/'+image+'';
        if(link == null){
            link = '';
        }
        if(image == null){
            image = '';
            srcimg = '';
        }
        if(id == 1){
            // alert('1');
            // alert($(this).data('link'));
            select = 1;
            html = 
            '<div class="input-group">'+
            '<input type="text" class="form-control m-input m-input--solid" placeholder="Link here" value="'+link+'" name="uplink" id="uplink">'+
            '<div class="input-group-append">'+
            '<span class="input-group-text" style="cursor: pointer;"  data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Redirect to this link"><a target="_blank" href="'+link+'" style="text-decoration: none;"><i class="la la-link glyphicon-th"></i></a></span>'+
            '</div>'+
            '</div>';
        } else {
            select = 2;
            // alert($(this).data('image'));
            html = 
            '<div class="input-group">'+
            '<label for=upimage">'+
            '<img src="'+srcimg+'"/>'+
            '</label>'+
            '<input type="file" value="'+image+'" name="upimage" id="upimage">'+
            '</div>';
            // alert('2');
        }
        $('#upselect_type').selectpicker('val', select);
        $('#uptype_display').html(html);
    })

    var offenseDatatable = function () {
        var offense = function (searchVal = "", start_date, end_date) {
            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: "<?php echo base_url(); ?>Support_tracker/support_tracker_view",
                            params: {
                                query: {
                                    searchField: searchVal,
                                    searchStatus: $('#searchStatus').val(),
                                    start: start_date,
                                    end: end_date,
                                    acknowledged: $('#filter_acknowledge').val(),
                                    fixed: $('#filter_fixed').val()
                                },
                            },
                            map: function (raw) {
                                console.log(raw);
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                dataCount = dataSet.length;
                                return dataSet;
                            }
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    theme: 'default',
                    class: '',
                    scroll: true,
                    footer: false

                },
                sortable: true,
                pagination: true,
                toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],

                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
                input: $('#searchField'),
            },
            columns: [
            {
                field: "support_tracker_id",
                title: "#",
                width: 50,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    index = index+1;
                    return index;
                },
            }, 
            {
                field: 'title',
                width: 150,
                title: 'Title',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
            },  
            {
                field: 'description',
                width: 300,
                title: 'Description',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
            },  
            {
                field: 'alname',
                width: 150,
                title: 'Acknowledge by',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    let html = row.afname+' '+row.alname;
                    return html;
                },
            },  
            {
                field: 'flname',
                width: 150,
                title: 'Fixed by',
                overflow: 'visible',
                sortable: true,
                textAlign: 'left',
                template: function (row, index, datatable) {
                    let html = row.ffname+' '+row.flname;
                    return html;
                },
            },  
            {
                field: 'status',
                width: 70,
                title: 'Status',
                overflow: 'visible',
                sortable: true,
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var status = '';
                    if(row.status == 1){
                        status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Easy</span>';
                    } else if(row.status == 2){
                        status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Normal</span>';
                    } else {
                        status = '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">Hard</span>';
                    }
                    return status;
                },
            },  
            {
                field: 'time_report',
                width: 120,
                title: 'Date Report',
                overflow: 'visible',
                sortable: 'asc',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    var time = moment(row.time_report).format('LL');
                    return time;
                },
            },  
            {
                field: "Actions",
                width: 110,
                title: "Actions",
                textAlign: 'center',
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    var display_actions = '';
                    var get_id = '<?php echo $this->session->emp_id; ?>' 
                    // var view = "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill view_modal' title='View ' data-support_tracker_id='"+row.support_tracker_id+"' data-title='"+row.title+"' data-description='"+row.description+"' data-alname='"+row.alname+"' data-afname='"+row.afname+"' data-amname='"+row.amname+"' data-flname='"+row.flname+"' data-ffname='"+row.ffname+"' data-fmname='"+row.fmname+"' data-clname='"+row.clname+"' data-cfname='"+row.cfname+"' data-cmname='"+row.cmname+"' data-image='"+row.image+"' data-link='"+row.link+"' data-time_report='"+row.time_report+"' data-duration='"+row.duration+"' data-time_acknowledge='"+row.time_acknowledge+"' data-time_confirm='"+row.time_confirm+"' data-status='"+row.status+"'><i class='la la-eye'></i></a>";
                    var edit = "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill update_modal' title='Update ' data-support_tracker_id='"+row.support_tracker_id+"' data-fixed='"+row.fixed+"' data-acknowledged='"+row.acknowledged+"' data-confirmed='"+row.confirmed+"' data-title='"+row.title+"' data-description='"+row.description+"' data-alname='"+row.alname+"' data-afname='"+row.afname+"' data-amname='"+row.amname+"' data-flname='"+row.flname+"' data-ffname='"+row.ffname+"' data-fmname='"+row.fmname+"' data-clname='"+row.clname+"' data-cfname='"+row.cfname+"' data-cmname='"+row.cmname+"' data-image='"+row.image+"' data-link='"+row.link+"' data-time_report='"+row.time_report+"' data-duration='"+row.duration+"' data-time_acknowledge='"+row.time_acknowledge+"' data-time_confirm='"+row.time_confirm+"' data-status='"+row.status+"'><i class='la la-edit'></i></a>";
                    var deleteb = "<a href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete_modal' title='Delete ' data-support_tracker_id='"+row.support_tracker_id+"'><i class='la la-trash'></i></a>";
                    if(get_id == 26 || get_id == 114){
                        display_actions = "\
                        "+edit+"\
                        "+deleteb+"\
                        "
                    }
                    return "\
                    "+display_actions+"\
                    ";
                }
            }
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);

    };
    return {
        init: function (searchVal, start_date, end_date) {
            offense(searchVal, start_date, end_date);
        }
    };
}();

function initOffenses(){
    let search_date_val = $('#searchDate').val().split(' - ');
    $('#offenseDatatable').mDatatable('destroy');
    offenseDatatable.init('',  moment(search_date_val[0], 'MM/DD/YYYY').format('Y-MM-DD'), moment(search_date_val[1], 'MM/DD/YYYY').format('Y-MM-DD'));
}

$(function(){
    spt_list();
    $('.m_selectpicker').selectpicker();
    getCurrentDateTime(function (date) {
        start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
        end =  moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
        $('#searchDate').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
                'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
                'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
                'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
                'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function(start, end, label) {
            $('#searchDate').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            initOffenses();
        });
    });
    initOffenses();
})

function spt_list(){
    $.ajax({
        type: 'post',
        url: '<?php echo base_url(); ?>Support_tracker/spt_list',
        async: false,
        dataType: 'json',
        success: function(data) {
            let html = '<option selected value="">Nothing Selected</option>';
            data.forEach(function(value){
                html += '<option value="'+value.emp_id+'">'+value.lname+', '+value.fname+' '+value.mname+'</option>'
            })
            $('#filter_acknowledge').html(html);
            $('#filter_acknowledge').selectpicker('refresh');
            $('#filter_fixed').html(html);
            $('#filter_fixed').selectpicker('refresh');
            $('#fixed').html(html);
            $('#fixed').selectpicker('refresh');
            $('#acknowledged').html(html);
            $('#acknowledged').selectpicker('refresh');
            $('#confirmed').html(html);
            $('#confirmed').selectpicker('refresh');
            $('#upfixed').html(html);
            $('#upfixed').selectpicker('refresh');
            $('#upacknowledged').html(html);
            $('#upacknowledged').selectpicker('refresh');
            $('#upconfirmed').html(html);
            $('#upconfirmed').selectpicker('refresh');
        }
    });
}

$('#searchStatus,#searchDate,#filter_acknowledge,#filter_fixed').change(function(){
    initOffenses();
});

$('.addNewButton').click(function() {
    $('#fixed').selectpicker('val', '');
    $('#acknowledged').selectpicker('val', '');
    $('#confirmed').selectpicker('val', '');
    $('#status').selectpicker('val', '');
    $('#m_modal_1').modal('show');
});
$('.filter').click(function() {
    $('#modal_filter').modal('show');
});

$('#addModal').on('submit',function(e) {
    e.preventDefault(); 
    $.ajax({
        url:'<?php echo base_url();?>Support_tracker/support_tracker_add',
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success: function(data){
            $('#title').val("");
            $('#description').val("");
            $('#link').val("");
            $('#image').val("");
            $('#time_report').val("");
            $('#time_confirm').val("");
            $('#time_acknowledge').val("");
            $('#fixed').selectpicker('val', '');
            $('#acknowledged').selectpicker('val', '');
            $('#confirmed').selectpicker('val', '');
            $('#m_modal_1').modal('hide');
            swal('Success!', 'Your file has been saved.', 'success');
            $('#offenseDatatable').mDatatable('reload');
        }
    });
});

$('#datatable_div').on('click','.view_modal',function(){
    $('#faqsModal1').modal('show');
    var support_tracker_id = $(this).data('support_tracker_id');
    var title = $(this).data('title');
    var description = $(this).data('description');
    var alname = $(this).data('alname');
    var afname = $(this).data('afname');
    var amname = $(this).data('amname');
    var flname = $(this).data('flname');
    var ffname = $(this).data('ffname');
    var fmname = $(this).data('fmname');
    var clname = $(this).data('clname');
    var link = $(this).data('link');
    var cfname = $(this).data('cfname');
    var cmname = $(this).data('cmname');
    var image = $(this).data('image');
    var time_report = $(this).data('time_report');
    var time_acknowledge = $(this).data('time_acknowledge');
    var time_confirm = $(this).data('time_confirm');
    var duration = $(this).data('duration');
    var type = '';
    if(link != null){
        type = '<p><a href="'+link+'" target="_blank">'+link+'</a><br></p>';
    } else {
        type = '<a href="<?php echo base_url()?>uploads/support_tracker/'+image+'"><img src="<?php echo base_url()?>uploads/support_tracker/'+image+'" style="width: 597.984px;"><br></a>';
    }
    var status = $(this).data('status');
    var status_display = ''
    if(status == 1){
        status_display = 'Easy';
    } else if(status == 2){
        status_display = 'Normal';
    } else {
        status_display = 'Hard';
    }
    var html = 
    '<hr>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Title</label>'+
    '<div class="col-10">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+title+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-tag glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Description</label>'+
    '<div class="col-10">'+
    '<textarea readonly class="form-control" disabled="disabled" style="height: 100px;">'+description+'</textarea>'+
    '</div>'+
    '</div>'+
    '<hr>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Type</label>'+
    '<div class="col-10">'+
    type+
    '</div>'+
    '</div>'+
    '<hr>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" style="float:right;" class="col-2 col-form-label">Fixed By</label>'+
    '<div class="col-10">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+flname+', '+ffname+' '+fmname+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-user glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Acknowledged</label>'+
    '<div class="col-10">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+alname+', '+afname+' '+amname+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-user glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Confirmed</label>'+
    '<div class="col-10">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+clname+', '+cfname+' '+cmname+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-user glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<hr>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Status</label>'+
    '<div class="col-10">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+status_display+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-navicon glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<hr>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Time Report</label>'+
    '<div class="col-4">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+time_report+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-clock-o glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<label style="font-size:12px" for="example-text-input" class="col-2 col-form-label">Time Acknowledge</label>'+
    '<div class="col-4">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+time_acknowledge+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-clock-o glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="form-group m-form__group row">'+
    '<label for="example-text-input" class="col-2 col-form-label">Time Confirm</label>'+
    '<div class="col-4">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+time_confirm+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-clock-o glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<label for="example-text-input" class="col-2 col-form-label">Duration</label>'+
    '<div class="col-4">'+
    '<div class="input-group">'+
    '<input type="text" class="form-control m-input" disabled="disabled" value="'+duration+'">'+
    '<div class="input-group-append">'+
    '<span class="input-group-text"><i class="la la-clock-o glyphicon-th"></i></span>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>';

    $('#modal_body1').html(html);
});

$('#datatable_div').on('click','.update_modal',function(){
    $('#faqsModal2').modal('show');
    var support_tracker_id = $(this).data('support_tracker_id');
    var fixed = $(this).data('fixed');
    var acknowledged = $(this).data('acknowledged');
    var confirmed = $(this).data('confirmed');
    var title = $(this).data('title');
    var description = $(this).data('description');
    var alname = $(this).data('alname');
    var afname = $(this).data('afname');
    var amname = $(this).data('amname');
    var flname = $(this).data('flname');
    var ffname = $(this).data('ffname');
    var fmname = $(this).data('fmname');
    var clname = $(this).data('clname');
    var cfname = $(this).data('cfname');
    var cmname = $(this).data('cmname');
    var image = $(this).data('image');
    var link = $(this).data('link');
    var time_report = $(this).data('time_report');
    var time_acknowledge = $(this).data('time_acknowledge');
    var time_confirm = $(this).data('time_confirm');
    var status = $(this).data('status');
    $('#uplink').val(link);
    $('#upselect_type').data('image',image);
    $('#upselect_type').data('link',link);
    var select = '';
    if(link != null){
        select = 1;
        html = 
        '<div class="input-group">'+
        '<input type="text" class="form-control m-input m-input--solid" placeholder="Link here" value="'+link+'" name="uplink" id="uplink">'+
        '<div class="input-group-append">'+
        '<span class="input-group-text" style="cursor: pointer;" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Redirect to this link"><a href="'+link+'" target="_blank" style="text-decoration: none;"><i class="la la-link glyphicon-th"></i></span>'+
        '</div>'+
        '</div>';
    } else {
        select = 2;
        html = 
        '<div class="input-group">'+
        '<label for=upimage">'+
        '<img src="<?php echo base_url()?>uploads/support_tracker/'+image+'"/>'+
        '</label>'+
        '<input type="file" name="upimage" id="upimage">'+
        '</div>';
    }
    console.log(title,description);
    $('#upselect_type').selectpicker('val', select);
    $('#uptype_display').html(html);
    $('#uptitle').val(title);
    $('#upsupport_tracker_id').val(support_tracker_id);
    $('#upfixed').selectpicker('val',fixed);
    $('#upacknowledged').selectpicker('val',acknowledged);
    $('#upconfirmed').selectpicker('val',confirmed);
    $('#updescription').val(description);
    $('#uptime_report').val(time_report);
    $('#uptime_acknowledge').val(time_acknowledge);
    $('#uptime_confirm').val(time_confirm);
    $('#upstatus').selectpicker('val',status);
    
});

$('#updatemodal').on('submit', function(e) {
    e.preventDefault(); 
    $.ajax({
        url:'<?php echo base_url();?>Support_tracker/support_tracker_update',
        type:"post",
        data:new FormData(this),
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success: function(data) {
            swal({
                title: 'Success!',
                text: 'Your file has been updated!',
                type: 'success',
                confirmButtonText: 'Ok',
                allowEscapeKey: 'false',
                allowOutsideClick: 'false',
            })
            $('#uptitle').val("");
            $('#upsupport_tracker_id').val("");
            $('#upfixed').val("");
            $('#upacknowledged').val("");
            $('#upconfirmed').val("");
            $('#updescription').val("");
            $('#upalname').val("");
            $('#upfname').val("");
            $('#upmname').val("");
            $('#upflname').val("");
            $('#upffname').val("");
            $('#upfmname').val("");
            $('#upclname').val("");
            $('#upcfname').val("");
            $('#upcmname').val("");
            $('#uptime_confirm').val("");
            $('#uptime_acknowledge').val("");
            $('#uptime_confirm').val("");
            $('#uplink').val("");
            $('#upimage').val("");
            $('#faqsModal2').modal('hide');
            $('#offenseDatatable').mDatatable('reload');
        }
    });
});

$('#datatable_div').on('click', '.delete_modal', function(e) {
    var support_tracker_id = $(this).data('support_tracker_id');
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        allowEscapeKey: 'false',
        allowOutsideClick: 'false',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Support_tracker/support_tracker_delete",
                dataType: "JSON",
                data: {
                    support_tracker_id
                },
                success: function(data) {
                        // faqsGeneralView();
                        $('#offenseDatatable').mDatatable('reload');
                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                            )
                    }
                });
            return false;
        }
    });

});

$('.exportButton').click(function() {
    let search_date_val = $('#searchDate').val().split(' - ');
    let start           = moment(search_date_val[0], 'MM/DD/YYYY').format('Y-MM-DD');
    let end             = moment(search_date_val[1], 'MM/DD/YYYY').format('Y-MM-DD')
    let searchField     = $('#searchField').val();
    let searchStatus    = $('#searchStatus').val();
    let acknowledged    = $('#filter_acknowledge').val();
    let fixed           = $('#filter_fixed').val();
    $.ajax({
        url: '<?php echo base_url(); ?>Support_tracker/downloadExcel_support_tracker',
        method: 'POST',
        data: {
            searchField,
            searchStatus,
            start,
            end,
            acknowledged,
            fixed
        },
        beforeSend: function() {
            mApp.block('#portlet', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg',
                message: 'Downloading...'
            });
        },
        xhrFields: {
            responseType: 'blob'
        },
        success: function(data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'Support_tracker.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
            mApp.unblock('#portlet');
        },
        error: function(data) {
            $.notify({
                message: 'An error occured while exporting excel'
            }, {
                type: 'danger',
                timer: 1000
            });
            mApp.unblock('#portlet');
        }
    });
});

</script>

</body>
</html>