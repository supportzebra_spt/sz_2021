<style type="text/css">
.m-datatable__cell {
    text-transform: capitalize
}

.bg-custom-gray {
    background: #4B4C54 !important;
}

.portlet-header-report {
    background: #4B4C54 !important;
    color: white !important;
    height: 50px !important;
}

.font-white {
    color: white !important;
}

.m-content ul.m-subheader__breadcrumbs li,
.m-content ul.m-subheader__breadcrumbs li a {
    padding: 0 !important;
}

.m-content ul.m-subheader__breadcrumbs li a span:hover {
    background: #ddd !important;
    color: black !important;
}

.m-content ul.m-subheader__breadcrumbs li a span {
    color: #fff !important;
    padding: 10px 20px !important;
}

.m-content ul.m-subheader__breadcrumbs {
    background: #4B4C54 !important;
    padding: 2px;
}

.my-nav-active {
    background: #eee !important;
}

li.my-nav-active a span {
    color: black;
}

.m-content hr {
    border: 2.5px solid #eee !important;
}

/*DATERANGEPICKER*/
.daterangepicker {
    font-size: 13px !important;
}

.daterangepicker table td {
    width: 28px !important;
    height: 28px !important;
}

.daterangepicker_input input {
    padding-top: 5px !important;
    padding-bottom: 5px !important;
    line-height: 1 !important;
}

.m-content .m-stack__item {
    padding: 0 !important;
    background: 0 !important;
    border: 0 !important;
}

.table-sticky-container {
    overflow: auto;
    max-height: 500px;
}

.table-sticky-container table thead tr th {
    position: sticky;
    top: 0;
    min-width: 200px;
    max-width: 200px;
    background: #f9f9f9;
    font-size: 12px;
    border-top: 0;
}

.table-sticky-container table tbody tr th {
    position: sticky;
    left: 0;
    background: #f1f1f1;
}

.table-sticky-container table thead tr th:first-child {
    left: 0;
    z-index: 1;
    min-width: 250px;
    max-width: 250px;
    background: #f1f1f1;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">System Settings</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Settings/user_access_rights'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">User Access Rights</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins"
                                    style="color:white !important;font-size:16px">
                                    User Access Rights
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview" style="padding:10px">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Link Type</label>
                                                <select class="form-control m-input" id="search-link-type">
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Class</label>
                                                <select class="form-control m-input" id="search-class">
                                                    <option value="">All</option>
                                                    <option value="Admin">SZ Team</option>
                                                    <option value="Agent">Ambassador</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <label for="">Search</label>
                                                <input type="text" class="form-control m-input"
                                                    placeholder="Search first name or last name..." id="search-name" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-demo__preview" style="padding:10px">
                                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show"
                                    role="alert" id="date-alert">
                                    <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                    <div class="m-alert__text"><strong>No Records Found! </strong> <br>Sorry, no records
                                        were found. Please adjust your search criteria and try again.
                                    </div>
                                </div>

                                <div class="table-sticky-container">
                                    <table class="table m-table table-hover table-sm" id="table-access">

                                    </table>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-stack m-stack--ver m-stack--general m-stack--demo"
                                    style="height:0 !important">
                                    <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                        <ul id="pagination">
                                        </ul>
                                    </div>
                                    <div class="m-stack__item m-stack__item--right m-stack__item--middle"
                                        style="width:8%">
                                        <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                                            <option>5</option>
                                            <option>10</option>
                                            <option>20</option>
                                            <option>50</option>
                                            <option>100</option>
                                        </select>
                                    </div>
                                    <div class="m-stack__item m-stack__item--middle m-stack__item--right"
                                        style="width:20%;font-size:12px">Showing <span id="count"></span> of <span
                                            id="total"></span> records</div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="modal-auto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Auto Set User Access</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 id="auto-name"></h4>
                <hr>
                <button class="btn btn-success" id="btn-admin">Admin Supervisor</button>
                <button class="btn btn-brand" id="btn-tl">Team Leader</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script type="text/javascript">
function getMenuTabs(callback) {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Settings/getMenuTabs",
        cache: false,
        success: function(result) {
            result = JSON.parse(result.trim());
            callback(result);
        }
    });
}

function getMenuItems(callback) {
    var searchLinkType = $("#search-link-type").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Settings/getMenuItems",
        data: {
            searchLinkType: searchLinkType,
            searchSystemType: 1
        },
        cache: false,
        success: function(result) {
            result = JSON.parse(result.trim());
            callback(result);
        }
    });
}

function getUserAccess(limiter, perPage, callback) {
    var searchClass = $("#search-class").val();
    var searchName = $("#search-name").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Settings/getUserAccess",
        data: {
            limiter: limiter,
            perPage: perPage,
            searchClass: searchClass,
            searchName: searchName
        },
        beforeSend: function() {
            mApp.block('#m_blockui_2_portlet', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg'
            });
        },
        cache: false,
        success: function(res) {
            res = JSON.parse(res.trim());

            $("#total").html(res.total);
            $("#total").data('count', res.total);

            getMenuItems(function(menu_items) {
                //                        console.log(menu_items);
                $("#table-access").html("");
                var string = '';
                string += '<thead><tr>';
                string +=
                    '<th><span class="m--font-bold" style="font-size:12px">Last Name, First Name</span><br><span class="m--font-boldest">Names</span></th>';
                $.each(menu_items, function(i, item) {
                    string += '<th class="text-center"><span class="m--font-bold">' + item
                        .item_title + '</span><br><span class="m--font-boldest">' + item
                        .item_link + '</span></th>';
                });
                string += '</tr></thead><tbody>';
                $.each(res.data, function(i, item) {
                    string += '<tr>';
                    string += '<th><a href="#" class="m-link btn-auto" data-uid="' + item
                        .uid + '" data-name="' + item.lname +
                        ', ' + item.fname + '" >' + item.lname +
                        ', ' + item.fname + '</a></th>';
                    $.each(menu_items, function(key, menu) {
                        var isOkay = false;
                        $.each(item.data, function(x, obj) {
                            if (obj.menu_item_id === menu.menu_item_id) {
                                isOkay = true;
                            }
                        });
                        if (isOkay === true) {
                            string +=
                                '<td class="text-center"><input type="checkbox" name="checkbox-access" checked="checked" data-uid="' +
                                item.uid + '" data-menuitemid="' + menu
                                .menu_item_id + '"></td>';
                        } else {
                            string +=
                                '<td class="text-center"><input type="checkbox" name="checkbox-access" data-uid="' +
                                item.uid + '" data-menuitemid="' + menu
                                .menu_item_id + '"></td>';
                        }
                    });
                    string += '</tr>';
                });
                string += '</tbody>';
                if (string === '') {
                    $("#date-alert").show();
                    $("#table-access").hide();
                } else {
                    $("#date-alert").hide();
                    $("#table-access").show();
                }
                $("#table-access").html(string);
            });

            var count = $("#total").data('count');
            var result = parseInt(limiter) + parseInt(perPage);
            if (count === 0) {
                $("#count").html(0 + ' - ' + 0);
            } else if (count <= result) {
                $("#count").html((limiter + 1) + ' - ' + count);
            } else if (limiter === 0) {
                $("#count").html(1 + ' - ' + perPage);
            } else {
                $("#count").html((limiter + 1) + ' - ' + result);
            }
            mApp.unblock('#m_blockui_2_portlet');
            callback(res.total);
        },
        error: function() {
            swal("Error", "Please contact admin", "error");
            mApp.unblock('#m_blockui_2_portlet');
        }
    });
}
$(function() {
    getMenuTabs(function(res) {
        $("#search-link-type").html("<option value=''>All</option>");
        $.each(res.tabs, function(i, item) {
            $("#search-link-type").append("<option value='" + item.tab_id + "'>" + item
                .tab_name + "</option>");
        });
    });
    $("#perpage").change(function() {
        var perPage = $("#perpage").val();
        getUserAccess(0, perPage, function(total) {
            $("#pagination").pagination('destroy');
            $("#pagination").pagination({
                items: total, //default
                itemsOnPage: $("#perpage").val(),
                hrefTextPrefix: "#",
                cssStyle: 'light-theme',
                onPageClick: function(pagenumber) {
                    var perPage = $("#perpage").val();
                    getUserAccess((pagenumber * perPage) - perPage, perPage);
                }
            });
        });
    });

    $("#search-class,#search-link-type,#search-name").change(function() {
        $("#perpage").change();
    });
    $("#perpage").change();

    var autoSetAccess = function(uid, type) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Settings/setUserAccess_supervisor",
            data: {
                uid: uid,
                type: type
            },
            cache: false,
            success: function(res) {
                var res = res.trim();
                if (res == 1) {
                    $.notify({
                        message: 'Successfully automated user access.'
                    }, {
                        type: 'success',
                        timer: 1000
                    });
                } else {
                    $.notify({
                        message: 'An error occurred.'
                    }, {
                        type: 'danger',
                        timer: 1000
                    });
                }
                $("#modal-auto").modal('hide');
                $("#perpage").change();
            }
        });
    }
    $("#btn-tl").click(function() {
        autoSetAccess($(this).data('uid'), "tl");
    })
    $("#btn-admin").click(function() {
        autoSetAccess($(this).data('uid'), "admin");
    })
    $("#table-access").on("click", ".btn-auto", function() {
        var that = this;
        var uid = $(that).data('uid');
        var name = $(that).data('name');
        $("#btn-tl,#btn-admin").data('uid', uid);
        $("#auto-name").html(name);
        $("#modal-auto").modal('show');
    })
    $("#table-access").on('click', '[name="checkbox-access"]', function() {
        var menu_item_id = $(this).data('menuitemid');
        var uid = $(this).data('uid');
        if ($(this).is(':checked')) {
            var status = true; //add to db
        } else {
            var status = false; //remove to db
        }
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Settings/setUserAccess",
            data: {
                menu_item_id: menu_item_id,
                uid: uid,
                status: status
            },
            cache: false,
            success: function(res) {
                var res = JSON.parse(res.trim());
                if (res.status === 1) {
                    $.notify({
                        message: 'Successfully changed user access.'
                    }, {
                        type: 'success',
                        timer: 1000
                    });
                } else {
                    $.notify({
                        message: 'No Change.'
                    }, {
                        type: 'danger',
                        timer: 1000
                    });
                }
            }
        });
    });

});
</script>