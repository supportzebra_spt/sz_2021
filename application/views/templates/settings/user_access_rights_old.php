<style type="text/css">
    .myscrolling table {
        table-layout: inherit;
        *margin-left: -100px;/*ie7*/
    }
    .myscrolling td, .myscrolling th {
        vertical-align: top;
        padding: 10px;
        min-width: 100px;
        white-space: nowrap;
        border-left:1px solid #ccc;
        border-right:1px solid #ccc;
    }
    .myscrolling  th {
        position: absolute;
        *position: relative; /*ie7*/
        left: 0;
        width: 250px; 
        white-space: nowrap;
        text-overflow: ellipsis;    
        overflow: hidden;
    }
    .myscrolling td{
        font-size:12px
    }
    .myouter {
        position: relative
    }
    .myinner {
        overflow-x: auto;
        overflow-y: visible;
        margin-left: 250px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">System Settings</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Settings/user_access_rights'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">User Access Rights</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_blockui_2_portlet"> 
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">Leave Credits 
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--                        <div class="row text-left">
                                                    <div class="col-md-3">
                                                        <div class="form-group m-form__group">
                                                            <select class="form-control m-input m-input--air" id="search-tab">
                                                            </select>
                        
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="input-group m-form__group">
                                                            <input type="text" class="form-control m-input m-input--air" placeholder="Search Menu Items..." id="search-custom">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-secondary" type="button" id="btn-search"><i class="la la-search m--font-brand"></i></button>
                                                            </div>			      	
                                                        </div>
                                                    </div>
                                                </div>  -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="myscrolling myouter">
                                    <div class="myinner">
                                        <table class="table table-striped table-hover table-condensed" id="table-access">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">

        function getMenuItems(callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Settings/getMenuItems",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    callback(result);
                }
            });
        }
        function getUserAccess() {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Settings/getUserAccess",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    getMenuItems(function (menu_items) {
//                        console.log(menu_items);
                        $("#table-access").html("");
                        var string = '';
                        string += '<tr>';
                            string += '<th><span class="m--font-bold" style="font-size:12px">First and Last</span><br><span class="m--font-boldest">Names</span></th>';
                        $.each(menu_items, function (i, item) {
                            string += '<td><span class="m--font-bold">' + item.item_title + '</span><br><span class="m--font-boldest">' + item.item_link + '</span></td>';
                        });
                        string += '</tr>';
                        $.each(result, function (i, item) {
                            string += '<tr>';
                            string += '<th>' + item.lname + ', ' + item.fname + '</th>';

                            $.each(menu_items, function (key, menu) {

                                var isOkay = false;
                                $.each(item.data, function (x, obj) {
                                    if (obj.menu_item_id === menu.menu_item_id) {
                                        isOkay = true;
                                    }
                                });
                                if (isOkay === true) {
                                    string += '<td><input type="checkbox" name="checkbox-access" checked="checked" data-uid="' + item.uid + '" data-menuitemid="' + menu.menu_item_id + '"></td>';
                                } else {
                                    string += '<td><input type="checkbox" name="checkbox-access" data-uid="' + item.uid + '" data-menuitemid="' + menu.menu_item_id + '"></td>';
                                }


                            });

                            string += '</tr>';
                        });

                        $("#table-access").html(string);
                    });
                }
            });
        }
        $(function () {
//            getMenuTabs(function (json) {
//                $("#search-tab").html("");
//                var str = "<option value=''>All</option>";
//                $.each(json.tabs, function (key, obj) {
//                    str += "<option value='" + obj.tab_id + "'>" + obj.tab_name + "</option>";
//                });
//                $("#search-tab").html(str);
//            });
            getUserAccess();
            $("#table-access").on('click', '[name="checkbox-access"]', function () {
                var menu_item_id = $(this).data('menuitemid');
                var uid = $(this).data('uid');
                if ($(this).is(':checked')) {
                    var status = true;//add to db
                } else {
                    var status = false;//remove to db
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Settings/setUserAccess",
                    data: {
                        menu_item_id: menu_item_id,
                        uid: uid,
                        status: status
                    },
                    cache: false,
                    success: function (res) {
                        var res = JSON.parse(res.trim());
                        if (res.status === 1) {
                            $.notify({
                                message: 'Successfully changed user access.'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                        } else {
                            $.notify({
                                message: 'No Change.'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }
                    }
                });
            });
        });
</script>