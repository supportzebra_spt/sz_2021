<style type="text/css">
    label{
        font-weight:401;
        font-size:14px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">System Settings</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Settings/tab_menu'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Tab Menu</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#tabModal">
                        <span>
                            <i class="fa fa-th-list"></i>
                            <span>Tab Menu</span>
                        </span>
                    </button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-info m-btn m-btn--icon" data-toggle="modal" data-target="#linkModal" data-type='insert'>
                        <span>
                            <i class="fa fa-plus-circle"></i>
                            <span>New Link</span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered m-portlet--rounded">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-link"></i>
                                </span>
                                <h3 class="m-portlet__head-text" style='color:white !important'>
                                    Tab Menu and Links
                                </h3>
                            </div>			
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="" id="search-link-type" class='form-control m-input'></select> 
                                </div>
                                <div class="col-md-6">
                                    <select name="" id="search-system-type" class='form-control m-input'>
                                        <option value="">All</option>
                                        <option value="1">SZ</option>
                                        <option value="0">Supportzebra</option>
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <table class="table table-bordered table-hover table-sm" id="table-menu">
                            <thead>
                            <th></th>
                            <th>Tab Menu</th>
                            <th>Title</th>
                            <th>Link</th>
                            <th class='text-center'>System Type</th>
                            <th class='text-center'>Show in Sidebar ?</th>
                            <th class='text-center'>Action</th>
                            </thead>
                            <tbody></tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="linkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tab Links</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Tab Menu:</label>
                        <select name="" id="link-tabmenu" class='form-control m-input'></select> <br />
                    </div>
                    <div class="col-md-12">
                        <label for="">Title:</label>
                        <input type="text" class="form-control m-input" id="link-title"/><br />
                    </div>
                    <div class="col-md-12">
                        <label for="">Link:</label>
                        <input type="text" class="form-control m-input" id="link-link"/><br />
                    </div>
                    <div class="col-md-6">
                        <label for="">System Type:</label>
                        <select name="" id="link-systemtype" class='form-control m-input'>
                            <option value="1">SZ</option>
                            <option value="2">SZ v2</option>
                            <option value="0">Supportzebra</option>
                        </select> <br />
                    </div>
                    <div class="col-md-6">
                        <label for="">Show In Sidebar:</label>
                        <select name="" id="link-showsidebar" class='form-control m-input'>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select> <br />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id='btn-createlink'>Create Link</button>
                <button type="button" class="btn btn-warning" id='btn-updatelink'>Update Link</button>
            </div>
        </div>
    </div>
</div> 
<div class="modal fade" id="tabModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tab Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm  table-hover">

                    <tr>
                        <td style="width:45%"><input type="text" class="form-control form-control-sm m-input input-tab" /></td>
                        <td style="width:45%"><input type="text" class="form-control form-control-sm m-input input-icon" /></td>
                        <td style="width:10%"><button class="btn btn-sm btn-outline-success" id="btn-createtabmenu"><span><i class="fa fa-plus-circle"></i><span> Create</span></span></button></td>
                    </tr>
                </table>
                <table class='table table-sm table-hover' id='table-modalTab'>
                    <thead style="background:#716aca;color:white">
                    <th class="text-center" style="width:45%">Tab Menu Name</th>
                    <th class="text-center" style="width:45%">Icon</th>
                    <th class="text-right" style="width:10%">Action</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">

        function getMenuTabs(callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Settings/getMenuTabs",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    callback(result);
                }
            });
        }
        function getMenuItems() {
            var searchLinkType = $("#search-link-type").val();
            var searchSystemType = $("#search-system-type").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Settings/getMenuItems",
                data: {
                    searchLinkType: searchLinkType,
                    searchSystemType: searchSystemType
                },
                cache: false,
                success: function (result) {
                    var menu_items = JSON.parse(result.trim());
                    $("#table-menu tbody").html("");
                    var string = "";
                    $.each(menu_items, function (i, item) {
                        var systemtype = (item.isNewTemplate === '1') ? "sz" : (item.isNewTemplate === '2') ? "sz v2" : "supportzebra";
                        var systemcolor = (item.isNewTemplate === '1') ? "m--font-info" :(item.isNewTemplate === '2') ? "m--font-primary" :  "m--font-success";
                        var showsidebar = (item.isSidebar === '1') ? "Yes" : "No";
                        var sidecolor = (item.isSidebar === '1') ? "m--font-info" : "m--font-danger";
                        string += '<tr style="font-size:13px;font-weight:400">'
                                + '<td><small><i class="' + item.icon + '"></i></small></td>'
                                + '<td>' + item.tab_name + '</td>'
                                + '<td>' + item.item_title + '</td>'
                                + '<td>' + item.item_link + '</td>'
                                + '<td class="' + systemcolor + ' m--font-bolder text-center">' + systemtype + '</td>'
                                + '<td class="' + sidecolor + ' text-center">' + showsidebar + '</td>'
                                + '<td class="text-center">'
                                + ' <a href="#" class="m-link m--font-bold" title="Edit" data-toggle="modal" data-target="#linkModal" data-type="update" data-menuitemid="' + item.menu_item_id + '"><i class="fa fa-edit"></i></a>'
                                + ' <a href="#" class="m-link m--font-bold m--font-danger btn-deletelink ml-3" title="Delete" data-menuitemid="' + item.menu_item_id + '"><i class="fa fa-trash"></i></a>'
                                + '  </td>'
                                + '</tr>';
                    });
                    $("#table-menu tbody").html(string);
                }
            });
        }
        function setMenuTabs() {
            getMenuTabs(function (res) {
                $("#search-link-type").html("<option value=''>All</option>");
                $("#link-tabmenu").html("");
                $.each(res.tabs, function (i, item) {
                    $("#search-link-type").append("<option value='" + item.tab_id + "'>" + item.tab_name + "</option>");
                    $("#link-tabmenu").append("<option value='" + item.tab_id + "'>" + item.tab_name + "</option>");
                });

            });
        }
        $(function () {
            setMenuTabs();
            getMenuItems();
        });
        $("#search-link-type,#search-system-type").change(function () {
            getMenuItems();
        });
        $("#linkModal").on('hide.bs.modal', function (e) {
            $("#link-title").val('');
            $("#link-link").val('');
        });
        $("#linkModal").on('show.bs.modal', function (e) {
            var type = $(e.relatedTarget).data('type'); //insert,update
            if (type === 'insert') {
                $("#btn-createlink").show();
                $("#btn-updatelink").hide();
            } else {
                var menu_item_id = $(e.relatedTarget).data('menuitemid');
                $("#btn-updatelink").data('menuitemid', menu_item_id);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Settings/getMenuItem_single",
                    data: {
                        menu_item_id: menu_item_id
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        var menu_item = res.menu_item;
                        $("#link-title").val(menu_item.item_title);
                        $("#link-link").val(menu_item.item_link);
                        $("#link-tabmenu").val(menu_item.tab_id);
                        $("#link-systemtype").val(menu_item.isNewTemplate);
                        $("#link-showsidebar").val(menu_item.isSidebar);
                    }
                });

                $("#btn-createlink").hide();
                $("#btn-updatelink").show();
            }
        });
        $("#btn-createlink").click(function () {
            var that = this;
            var item_title = $("#link-title").val();
            var item_link = $("#link-link").val();
            var tab_id = $("#link-tabmenu").val();
            var isNewTemplate = $("#link-systemtype").val();
            var isSidebar = $("#link-showsidebar").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Settings/createMenuLink",
                data: {
                    item_title: item_title,
                    item_link: item_link,
                    tab_id: tab_id,
                    isNewTemplate: isNewTemplate,
                    isSidebar: isSidebar
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        $.notify({
                            message: 'Succesfully created the menu link.'
                        }, {
                            type: 'success',
                            timer: 1000
                        });
                        setMenuTabs();
                    } else {
                        $.notify({
                            message: 'An error occured while creating the menu link.'
                        }, {
                            type: 'danger',
                            timer: 1000
                        });
                    }
                    getMenuItems();
                    $("#link-title").val('');
                    $("#link-link").val('');
                    $("#linkModal").modal('hide');
                }
            });
        });
        $("#btn-updatelink").click(function () {
            var that = this;
            var menu_item_id = $(that).data('menuitemid');
            var item_title = $("#link-title").val();
            var item_link = $("#link-link").val();
            var tab_id = $("#link-tabmenu").val();
            var isNewTemplate = $("#link-systemtype").val();
            var isSidebar = $("#link-showsidebar").val();
            if ($.trim(item_title) === '' || $.trim(item_link) === '') {
                $.notify({
                    message: 'Title and link cannot be empty.'
                }, {
                    type: 'warning',
                    timer: 1000,
                    z_index: 99999
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Settings/updateMenuLink",
                    data: {
                        menu_item_id: menu_item_id,
                        item_title: item_title,
                        item_link: item_link,
                        tab_id: tab_id,
                        isNewTemplate: isNewTemplate,
                        isSidebar: isSidebar
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $.notify({
                                message: 'Succesfully updated the menu link.'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                            // setMenuTabs();
                        } else {
                            $.notify({
                                message: 'An error occured while updating the menu link.'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }
                        getMenuItems();
                        $("#linkModal").modal('hide');
                    }
                });
            }
        });
        $("#tabModal").on('show.bs.modal', function () {
            getMenuTabs(function (res) {
                var string = "";
                $("#table-modalTab tbody").html("");
                $.each(res.tabs, function (i, item) {
                    string += '<tr>'
                            + '<td><input type="text" class="form-control form-control-sm m-input input-tab" value="' + item.tab_name + '" /></td>'
                            + '<td><input type="text" class="form-control form-control-sm m-input input-icon" value="' + item.icon + '" /></td>'
                            + '<td class="text-right"><button class="btn btn-sm btn-outline-brand btn-updatetabmenu" data-originalvalue="' + item.tab_name + '" data-originalvalueicon="' + item.icon + '" data-tabid="' + item.tab_id + '"><span><i class="fa fa-pencil"></i><span> </span></span></button></td>'
                            + '</tr>';
                });
                $("#table-modalTab tbody").html(string);
            });
        });
        $("#btn-createtabmenu").click(function () {
            var that = this;
            var tab_name = $(that).closest('tr').find('input.input-tab').val();
            var icon = $(that).closest('tr').find('input.input-icon').val();
            if ($.trim(tab_name) === '' || $.trim(icon) === '') {
                $.notify({
                    message: 'Tab menu and icon cannot be empty.'
                }, {
                    type: 'warning',
                    timer: 1000,
                    z_index: 99999
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Settings/createTabMenu",
                    data: {
                        tab_name: tab_name,
                        icon: icon
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $.notify({
                                message: 'Succesfully created the tab menu.'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                            setMenuTabs();
                        } else {
                            $.notify({
                                message: 'An error occured while creating the tab menu.'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }
                        $(that).closest('tr').find('input').val('');
                        $("#tabModal").modal('hide');
                    }
                });
            }
        });
        $("#table-modalTab").on("click", ".btn-updatetabmenu", function () {
            var that = this;
            var tab_id = $(that).data('tabid');
            var originalvalue = $(that).data('originalvalue');
            var originalvalueicon = $(that).data('originalvalueicon');
            var tab = $(that).closest('tr').find('input.input-tab').val();
            var icon = $(that).closest('tr').find('input.input-icon').val();
            if ($.trim(tab) === '' || $.trim(icon) === '') {
                $.notify({
                    message: 'Tab menu and icon cannot be empty.'
                }, {
                    type: 'warning',
                    timer: 1000,
                    z_index: 99999
                });
                $(that).closest('tr').find('input.input-tab').val(originalvalue);
                $(that).closest('tr').find('input.input-icon').val(originalvalueicon);
            } else if (originalvalue === tab && originalvalueicon === icon) {
                $.notify({
                    message: 'No changes were made.'
                }, {
                    type: 'warning',
                    timer: 1000,
                    z_index: 99999
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Settings/updateTabMenu",
                    data: {
                        tab_id: tab_id,
                        tab_name: tab,
                        icon: icon
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $.notify({
                                message: 'Succesfully updated the tab menu.'
                            }, {
                                type: 'success',
                                timer: 1000,
                                z_index: 99999
                            });
                            $(that).data('originalvalue', tab);
                            $(that).data('originalvalueicon', icon);
                            setMenuTabs();
                        } else {
                            $.notify({
                                message: 'An error occured while updating the tab menu.'
                            }, {
                                type: 'danger',
                                timer: 1000,
                                z_index: 99999
                            });
                        }

                    }
                });
            }

        });
        $("#table-menu").on("click", ".btn-deletelink", function () {
            var that = this;
            var menu_item_id = $(that).data('menuitemid');
            swal({
                title: 'Are you sure?',
                text: "Menu link will be removed!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: "btn btn-danger m-btn m-btn--air",
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, remove it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Settings/deleteMenuLink",
                        data: {
                            menu_item_id: menu_item_id
                        },
                        cache: false,
                        success: function (res) {
                            res = JSON.parse(res.trim());
                            if (res.status === 'Success') {
                                $.notify({
                                    message: 'Succesfully delete the menu link.'
                                }, {
                                    type: 'success',
                                    timer: 1000,
                                    z_index: 99999
                                });
                                getMenuItems();
                            } else {
                                $.notify({
                                    message: 'An error occured while deleting the menu link.'
                                }, {
                                    type: 'danger',
                                    timer: 1000,
                                    z_index: 99999
                                });
                            }

                        }
                    });
                }
            });
        });
</script>