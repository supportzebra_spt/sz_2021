<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Meszenge<a href="#" class="resetData" style="text-decoration: none; color: inherit;">r</a></h3>           
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Meszenger/control_panel" class="m-nav__link">
							<span class="m-nav__link-text">Control Panel</span>
						</a>
					</li>
				</ul>
			</div>
			<div>
				<a href="<?php echo base_url()?>Meszenger/group_chat" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info addNewButton">Group Chat Option
					<i class="fa fa-gears"></i>
				</a>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
			<div class="m-portlet__body">
				<div class="m-section" id="datatable_div">
					<div class="m-form m-form--label-align-right m--margin-bottom-10">
						<div class="row align-items-cente m--margin-bottom-10">
							<div class="col-xl-6 order-2 order-xl-1">
								<h5>Control Panel <br>
									<small class="text-muted">Display All User's Contact Information</small>
								</h5>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-xl-7 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-12">
										<div style="width: 100%; padding: 5px" class="m-input-icon m-input-icon--left m-input-icon--right">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search for Employee Name and Accounts..." id="searchField" autocomplete="off">
											<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-8">
										<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" required name="searchStatus" id="searchStatus">
											<option value="">All Status</option>
											<option data-content="<span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>Hide Meszenger</span>" value="0">Hide Meszenger</option>
											<option data-content="<span class='m-badge m-badge--info m-badge--wide m-badge--rounded'>Default User Contact</span>" value="1">Default User Contact</option>
											<option data-content="<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>Basic Contact</span>" value="2">Basic Contact</option>
											<option data-content="<span class='m-badge m-badge--primary m-badge--wide m-badge--rounded'>Group Contact</span>" value="3">Group Contact</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="offenseDatatable"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var offenseDatatable = function () {
		var offense = function (searchVal = "") {
			var options = {
				data: {
					type: 'remote',
					source: {
						read: {
							method: 'POST',
							url: "<?php echo base_url(); ?>Meszenger/control_panel_data",
							params: {
								query: {
									searchField: searchVal,
									searchStatus: $('#searchStatus').val(),
								},
							},
							map: function (raw) {
								var dataSet = raw;
								if (typeof raw.data !== 'undefined') {
									dataSet = raw.data;
								}
								dataCount = dataSet.length;
								return dataSet;
							}
						}
					},
					saveState: {
						cookie: false,
						webstorage: false
					},
					pageSize: 5,
					serverPaging: true,
					serverFiltering: true,
					serverSorting: true,
				},
				layout: {
					theme: 'default',
					class: '',
					// scroll: true,
					maxHeight: 500,
					footer: false

				},
				sortable: true,
				pagination: true,
				toolbar: {
					placement: ['bottom'],
					items: {
						pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
            	input: $('#searchField'),
            },
            columns: [
            {
            	field: "lname",
            	title: "Employee",
            	width: 250,
            	selector: false,
            	sortable: 'asc',
            	textAlign: 'left',
            	template: function (row, index, datatable) {
            		let check = (row.flag==1) ? 'checked' : '';
            		return capitalizeTheFirstLetterOfEachWord(row.lname)+', '+capitalizeTheFirstLetterOfEachWord(row.fname)+' '+capitalizeTheFirstLetterOfEachWord(row.mname);
            	}
            }, 
            {
            	field: 'acc_name',
            	width: 250,
            	title: 'Account',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            },  
            {
            	field: 'status',
            	width: 150,
            	title: 'Status',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            		var display = '';
            		var status = '';
            		if(row.status == 0){
            			status = '<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Hide Meszenger</span>';
            		} else if(row.status == 1){
            			status = '<span class="m-badge m-badge--info m-badge--wide m-badge--rounded">Default User Contact</span>';
            		} else if(row.status == 2){
            			status = '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Basic Contact</span>';
            		} else if(row.status == 3){
            			status = '<span class="m-badge m-badge--primary m-badge--wide m-badge--rounded">Group Contact</span>';
            		}

            		if(row.status != 0){
            			display += '<a class="dropdown-item change_status" data-id="'+row.meszenger_status_ID+'" data-status="0" href="#"><span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">Hide Meszenger</span></a>';
            		}
            		if(row.status != 1){
            			display += '<a class="dropdown-item change_status" data-id="'+row.meszenger_status_ID+'" data-status="1" href="#"><span class="m-badge m-badge--info m-badge--wide m-badge--rounded">Default User Contact</span></a>';
            		}
            		if(row.status != 2){
            			display += '<a class="dropdown-item change_status" data-id="'+row.meszenger_status_ID+'" data-status="2" href="#"><span class="m-badge m-badge--success m-badge--wide m-badge--rounded">Basic Contact</span></a>';
            		}
            		if(row.status != 3){
            			display += '<a class="dropdown-item change_status" data-id="'+row.meszenger_status_ID+'" data-status="3" href="#"><span class="m-badge m-badge--primary m-badge--wide m-badge--rounded">Group Contact</span></a>';
            		}

            		return '\
            		<div class="dropdown ' + dropup + '">\
            		<a style="text-decoration: none;" href="#" class="" data-toggle="dropdown">\
            		'+status+'\
            		</a>\
            		<div class="dropdown-menu dropdown-menu-right">\
            		'+display+'\
            		</div>\
            		</div>\
            		';
            	},
            },  
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
    	init: function (searchVal) {
    		offense(searchVal);
    	}
    };
}();

$('#searchStatus').change(function(){
	initOffenses();
});

function initOffenses(){
	$('#offenseDatatable').mDatatable('destroy');
	offenseDatatable.init();
}

function capitalizeTheFirstLetterOfEachWord(words) {
	var separateWord = words.toLowerCase().split(' ');
	for (var i = 0; i < separateWord.length; i++) {
		separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
		separateWord[i].substring(1);
	}
	return separateWord.join(' ');
}

$(function(){
	$('.m_selectpicker').selectpicker();
	initOffenses();
	select_option();
})

$('.resetData').click(function(){
	swal({
		title: 'Are you sure?',
		text: "You want to reset the data!",
		type: 'warning',
		input: 'text',
		allowEscapeKey: 'false',
		allowOutsideClick: 'false',
		showCancelButton: true,
		confirmButtonText: 'Yes, reset it!',
	}).then(function(result) {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>Meszenger/resetData",
				dataType: "JSON",
				success: function(data) {
					$('#offenseDatatable').mDatatable('reload');
					swal(
						'Success!',
						'Data has been reset!',
						'success'
						)
				}
			});
			return false;
		}
	});
	swal({
			title: 'Are you sure?',
			text: "You want to reset the data!",
			type: 'warning',
			input: 'text',
			allowEscapeKey: 'false',
			allowOutsideClick: 'false',
			showCancelButton: true,
			confirmButtonText: 'Yes, reset it!',
			inputValidator: (value) => {
				return new Promise((resolve) => {
					if (value.trim() !== '') {
						resolve()
					} else {
						resolve('Oops! Reason is Required')
					}
				})
			}
		}).then(function(result) {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo base_url()?>Meszenger/resetData",
					dataType: "JSON",
					data: {
						passtext: result.value,
					},
					success: function(data) {
						// $('#offenseDatatable').mDatatable('reload');
						swal({
							title: 'Success!',
							text: 'Data has been reset!',
							type: 'success',
							confirmButtonText: 'Ok',
							allowEscapeKey: 'false',
							allowOutsideClick: 'false',
						}).then(function(result) {
							if (result.value) {
								location.reload();
							}
						});
					},
					error: function(data) {
						swal('Error!', 'There is an error occur', 'error');
					}
				});
			} 
		});
})

$('#datatable_div').on('click', '.change_status', function(e) {
	let id = $(this).data('id');
	let status = $(this).data('status');
	swal({
		title: 'Are you sure?',
		text: "User status on Meszenger will be update!",
		type: 'info',
		allowEscapeKey: 'false',
		allowOutsideClick: 'false',
		showCancelButton: true,
		confirmButtonText: 'Yes, update it!',
	}).then(function(result) {
		if (result.value) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>Meszenger/change_status",
				dataType: "JSON",
				data: {
					id,status
				},
				success: function(data) {
					$('#offenseDatatable').mDatatable('reload');
					swal(
						'Updated!',
						'User status has been updated!',
						'success'
						)
				}
			});
			return false;
		}
	});
});



</script>
