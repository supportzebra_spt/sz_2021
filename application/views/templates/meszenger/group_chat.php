<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Meszenger</h3>           
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Meszenger/control_panel" class="m-nav__link">
							<span class="m-nav__link-text">Control Panel</span>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Meszenger/group_chat" class="m-nav__link">
							<span class="m-nav__link-text">Group Chat</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile" style="border-radius: 10px !important">
			<div class="m-portlet__body">
				<div class="m-section" id="datatable_div">
					<div class="m-form m-form--label-align-right m--margin-bottom-10">
						<div class="row align-items-cente m--margin-bottom-10">
							<div class="col-xl-6 order-2 order-xl-1">
								<h5>Group Chat <br>
									<small class="text-muted">Customize Group Chat MeSZenger</small>
								</h5>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-10">
										<div style="width: 100%; padding: 5px" class="m-input-icon m-input-icon--left m-input-icon--right">
											<input type="text" class="form-control m-input m-input--solid" placeholder="Search for Group Name..." id="searchField" autocomplete="off">
											<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="#" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-info addNewButton">Create New Group 
									<i class="fa fa-users"></i>
								</a>
								<div class="m-separator m-separator--dashed d-xl-none"></div>
							</div>
						</div>
					</div>
					<div id="offenseDatatable"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="submit_updateNewGroup" method="post">
	<div class="modal fade" id="modal_updateNewGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="max-width: 500px" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button style="float: right" class="btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<div style="margin: 0 20px">
						<div class="form-group m-form__group row">
							<div style="margin: 20px auto;">
								<h2 style="text-align: center!important;">Group Chat</h2>
								<h5 style="text-align: center!important;" class="text-muted">Update An Existing Meszenger Group Chat</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-9">
								<label class="m--font-bolder">Group Name <span class="m--font-danger">*</span></label>
								<input type="text" class="form-control m-input m-input--solid" placeholder="Type the group name here" autocomplete="off" name="update_name" id="update_name" required>
								<input type="hidden" name="update_id" id="update_id" required>
							</div>
							<div class="col-3">
								<label class="m--font-bolder">Flag <span class="m--font-danger">*</span></label>
								<div id="update_flag">
									
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="m--font-bolder">Accounts <span class="m--font-danger">*</span></label>
							<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="update_accounts" id="update_accounts" title="Select your account here" data-dropup-auto="false" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Accounts ({0})">
							</select>
						</div>
						<div class="form-group">
							<label class="m--font-bolder">Employee <span class="m--font-danger">*</span></label>
							<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="update_employee" id="update_employee" title="Select your account here" data-dropup-auto="false" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})">
							</select>
						</div>
						<div style="bottom: 0;" class="form-group m-form__group row">
							<div style="margin: 10px auto;">
								<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-info">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<form id="submit_addNewGroup" method="post">
	<div class="modal fade" id="modal_addNewGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog" style="max-width: 500px" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button style="float: right" class="btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
					<div style="margin: 0 20px">
						<div class="form-group m-form__group row">
							<div style="margin: 20px auto;">
								<h2 style="text-align: center!important;">Group Chat</h2>
								<h5 style="text-align: center!important;" class="text-muted">Create A Meszenger Group Chat</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-9">
								<label class="m--font-bolder">Group Name <span class="m--font-danger">*</span></label>
								<input type="text" class="form-control m-input m-input--solid" placeholder="Type the group name here" autocomplete="off" name="created_name" id="created_name" required>
							</div>
							<div class="col-3">
								<label class="m--font-bolder">Flag <span class="m--font-danger">*</span></label>
								<div>
									<span class="m-switch m-switch--outline m-switch--info">
										<label>
											<input class="get_value" type="checkbox" name="">
											<span>
											</span>
										</label>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="m--font-bolder">Accounts <span class="m--font-danger">*</span></label>
							<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="create_accounts" id="create_accounts" title="Select your account here" data-dropup-auto="false" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Accounts ({0})">
							</select>
						</div>
						<div class="form-group">
							<label class="m--font-bolder">Employee <span class="m--font-danger">*</span></label>
							<select class="form-control m-bootstrap-select m_selectpicker m-input--solid" name="create_employee" id="create_employee" title="Select your account here" data-dropup-auto="false" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})">
							</select>
						</div>
						<div style="bottom: 0;" class="form-group m-form__group row">
							<div style="margin: 10px auto;">
								<button type="button" class="btn btn-metal" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-info">Save</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	var offenseDatatable = function () {
		var offense = function (searchVal = "") {
			var options = {
				data: {
					type: 'remote',
					source: {
						read: {
							method: 'POST',
							url: "<?php echo base_url(); ?>Meszenger/group_chat_data",
							params: {
								query: {
									searchField: searchVal,
								},
							},
							map: function (raw) {
								var dataSet = raw;
								if (typeof raw.data !== 'undefined') {
									dataSet = raw.data;
								}
								dataCount = dataSet.length;
								return dataSet;
							}
						}
					},
					saveState: {
						cookie: false,
						webstorage: false
					},
					pageSize: 5,
					serverPaging: true,
					serverFiltering: true,
					serverSorting: true,
				},
				layout: {
					theme: 'default',
					class: '',
					scroll: true,
					footer: false

				},
				sortable: true,
				pagination: true,
				toolbar: {
					placement: ['bottom'],
					items: {
						pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                    },
                }
            },
            search: {
            	input: $('#searchField'),
            },
            columns: [
            {
            	field: "meszenger_group_name",
            	title: "Group Name",
            	width: 250,
            	selector: false,
            	sortable: 'asc',
            	textAlign: 'left',
            }, 
            {
            	field: 'flag',
            	width: 50,
            	title: 'Flag',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		let check = (row.flag==1) ? 'checked' : '';
            		return '<span class="m-switch m-switch--outline m-switch--info"><label><input type="checkbox" id="flag_status" value="'+row.flag+'" '+check+' data-id="'+row.meszenger_group_ID+'" name=""><span></span></label></span>';
            	}
            },  
            {
            	field: 'lname',
            	width: 250,
            	title: 'Created By',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'left',
            	template: function (row, index, datatable) {
            		return row.lname+', '+row.fname+' '+row.mname;
            	},
            },  
            {
            	field: 'createdOn',
            	width: 175,
            	title: 'Date Created',
            	overflow: 'visible',
            	sortable: true,
            	textAlign: 'center',
            	template: function (row, index, datatable) {
            		return moment(row.createdOn).format('LLL');
            	},
            },  
            {
            	field: "Actions",
            	width: 70,
            	title: "Actions",
            	textAlign: 'center',
            	sortable: false,
            	overflow: 'visible',
            	template: function (row, index, datatable) {
            		var edit = "<a style='text-decoration: none;' href='#' class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill update_modal' title='Update ' data-id='"+row.meszenger_group_ID+"'><span class='btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air'><i style='color:#00c5dc;' class='la la-pencil'></i></span></a>";
            		return "\
            		"+edit+"\
            		";
            	}
            }
            ],
        };
        var datatable = $('#offenseDatatable').mDatatable(options);
    };
    return {
    	init: function (searchVal) {
    		offense(searchVal);
    	}
    };
}();

function initOffenses(){
	$('#offenseDatatable').mDatatable('destroy');
	offenseDatatable.init();
}

function select_option(emp_id = null, acc_id = null){
	console.log(emp_id);
	console.log(acc_id);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>Meszenger/select_option",
		dataType: "JSON",
		data:{emp_id,acc_id},
		success: function(data) {
			if(emp_id != null || acc_id != null){
				$('#update_accounts').html(data.account);
				$('#update_accounts').selectpicker('refresh');
				$('#update_employee').html(data.employee);
				$('#update_employee').selectpicker('refresh');
			}else{
				$('#create_accounts').html(data.account);
				$('#create_accounts').selectpicker('refresh');
				$('#create_employee').html(data.employee);
				$('#create_employee').selectpicker('refresh');
			}
		}
	});
}

$(function(){
	$('.m_selectpicker').selectpicker();
	initOffenses();
	select_option();
})

$('.addNewButton').click(function (){
	$('#modal_addNewGroup').modal('show');
})

$('#submit_addNewGroup').on('submit',function(e) {
	e.preventDefault();
	let name = $('#created_name').val();
	let employee = $('#create_employee').val();
	let accounts = $('#create_accounts').val();
	let flag =  ($('.get_value').is(":checked")) ? 1 : 0;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>Meszenger/create_group_chat",
		dataType: "JSON",
		data: {
			name,
			employee: employee.toString(),
			accounts: accounts.toString(),
			flag
		},
		success: function(data) {
			console.log(data);
			$('#created_name').val("");
			$('#create_employee').val("");
			$('#create_accounts').val("");
			$('.get_value').val("");
			$('#modal_addNewGroup').modal('hide');
			swal('Success!', 'Your file has been saved.', 'success');
			$('#offenseDatatable').mDatatable('reload');
		},
		error: function(data) {
			swal('Error!', 'There is an error occur', 'error');
		}
	});
});

$('#datatable_div').on('click','.update_modal',function(){
	$('#modal_updateNewGroup').modal('show');
	let id = $(this).data('id');
	$.ajax({
		url:'<?php echo base_url();?>Meszenger/get_meszenger_data',
		type:"post",
		data: {id},
		dataType: 'json',
		success: function(data){
			console.log(data.group_acc_id.split(','));
			$('#update_id').val(id);
			$('#update_name').val(data.meszenger_group_name);
			select_option(data.group_emp_id, data.group_acc_id);
			let check = (data.flag == 1) ? 'checked' : '';
			$('#update_flag').html(
				'<span class="m-switch m-switch--outline m-switch--info"><label><input '+check+' class="update_flag" type="checkbox" name=""><span></span></label></span>'
			);
		}
	});
});

$('#submit_updateNewGroup').on('submit',function(e) {
	e.preventDefault();
	let id = $('#update_id').val();
	let name = $('#update_name').val();
	let employee = $('#update_employee').val();
	let accounts = $('#update_accounts').val();
	let flag =  ($('.update_flag').is(":checked")) ? 1 : 0;
	$.ajax({
		type: "POST",
		url: "<?php echo base_url()?>Meszenger/update_group_chat",
		dataType: "JSON",
		data: {
			id,
			name,
			employee: employee.toString(),
			accounts: accounts.toString(),
			flag
		},
		success: function(data) {
			console.log(data);
			$('#update_name').val("");
			$('#update_employee').val("");
			$('#update_accounts').val("");
			$('.update_flag').val("");
			$('#modal_updateNewGroup').modal('hide');
			swal('Success!', 'Your file has been saved.', 'success');
			$('#offenseDatatable').mDatatable('reload');
		},
		error: function(data) {
			swal('Error!', 'There is an error occur', 'error');
		}
	});
});

$('#datatable_div').on('click','#flag_status',function(){
	let id = $(this).data('id');
	console.log(id);
	let flag = '';
	if($(this).is(":checked")){
		flag = '1';
		title = 'Successfully set to flag';
		statement = 'This group was successfully flag!';
	} else {
		flag = '0';
		title = 'Successfully set to unflag';
		statement = 'This group is not viewable anymore!';
	}
	$.ajax({
		type : "POST",
		url  : "<?php echo base_url() ?>Meszenger/flag_status",
		dataType : "JSON",
		data : {
			flag,id},
		success: function(data){
			swal({
				title: title,
				text: statement,
				type: 'success',
				confirmButtonText: 'Ok',
				allowEscapeKey: 'false',
				allowOutsideClick: 'false',
			}).then(function(result) {
				$('#offenseDatatable').mDatatable('reload');
			});
		}
	});
});



</script>
