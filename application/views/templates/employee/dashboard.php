<style>
    .highcharts-exporting-group,.highcharts-credits{
        display:none
    }

    .highcharts-title{
        font-family:'Poppins' !important;
    }

    .btn-green{
        background: #4CAF50;
        color:white !important;
    }
    .btn-green:hover{
        background:#2a862e !important;
    }
    .font-green{
        color: #4CAF50 !important;
    }
    .btn-outline-green{
        color: #4CAF50 !important;
        background:white !important;
        border-color:#4CAF50 !important;
    }
    .btn-outline-green:hover{
        background: #4CAF50 !important;
        color:white !important;
        border-color:#4CAF50 !important;
    }
    .btn-outline-orange{
        color: #333 !important;
        background:white !important;
        border-color:#ffb822 !important;
    }
    .btn-outline-orange:hover{
        background:#ffb822 !important;
        color:black !important;
        border-color:#ffb822 !important;
    }
    .dashboard-title{
        font-weight:401;
        text-align:center;
        font-size:14px;
        margin-top:6px;
        margin-bottom:5px;
    }
    .dashboard-title > i{
        color: #4CAF50 !important;
        margin: 0 5px;
    }
    .m-widget4 .m-widget4__item{
        padding:12px 5px !important
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <div class="m-content">
        <div class="row">
            <div class="col-md-4">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">DASHBOARD: <br /><small>Employee Management System</small></h3>   
            </div>
            <div class="col-md-8 text-right" style="margin-top:13px">
              <div class="btn-group m-btn-group flex-wrap " role="group" aria-label="...">
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-green" onclick="location.href = '<?php echo base_url(); ?>Employee/dashboard'"><span><i class="fa fa-dashboard"></i><span>Dashboard</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/profiles'"><span><i class="fa fa-list-alt"></i><span>Profiles</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/birthday_list'"><span><i class="fa fa-birthday-cake"></i><span>Birthdays</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/control_panel'"><span><i class="fa fa-gear"></i><span>Control Panel</span></span></button>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <!--first row-->
            <div class="col-12 col-sm-6  col-md-4 col-lg-4 col-xl-4">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">	
                        <div class="m-scrollable" data-scrollable="true" data-max-height="290">
                            <p class="dashboard-title"><i class="fa fa-male"></i> Male to Female Ratios <i class="fa fa-female"></i></p>
                            <div id="container-gender" style="height: 250px;margin: 0 auto"></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">	
                        <div class="m-scrollable" data-scrollable="true" data-max-height="290">
                            <p class="dashboard-title"><i class="fa fa-calendar"></i> Employee by Age Group</p>
                            <div id="container-age" style="height: 250px;margin: 0 auto"></div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">	
                        <div class="m-scrollable" data-scrollable="true" data-max-height="290">
                            <p class="dashboard-title"><i class="fa fa-briefcase"></i>  Employee by Years of Work</p>
                            <div id="container-work" style="height: 250px;margin: 0 auto"></div>
                        </div>
                    </div>

                </div>
            </div>
            <!--end first row-->
            <!--second row-->

            <div class="col-12 col-sm-6  col-md-6 col-lg-6 col-xl-6">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">	
                        <div class="m-scrollable" data-scrollable="true" data-max-height="290">
                            <p class="dashboard-title"><i class="fa fa-birthday-cake"></i> Upcoming Birthdays</p>
                            <hr style='margin:10px 0'/>
                            <div id="container-birthday"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6  col-md-6 col-lg-6 col-xl-6">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">	
                        <div class="m-scrollable" data-scrollable="true" data-max-height="290">
                            <p class="dashboard-title"><i class="fa fa-gift"></i> Upcoming Anniversary</p>
                            <hr style='margin: 10px 0'/>
                            <div id="container-anniversary"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end second row-->
            <!--third row-->
            <div class="col-12 col-sm-12  col-md-12 col-lg-12 col-xl-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">	
                        <div class="m-scrollable" data-scrollable="true" data-max-height="350">
                            <p class="dashboard-title"><i class="fa fa-headphones"></i>  Head Count Per Account (Ambassador)</p>
                            <div id="container-account" style="height: 310px;margin: 0 auto"></div>
                        </div>
                    </div>

                </div>
            </div>

            <!--end third row-->
            <!--frouth row-->
            <div class="col-12 col-sm-6  col-md-6 col-lg-6 col-xl-6">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">
                        <div class="m-scrollable" data-scrollable="true" data-max-height="290">
                            <p class="dashboard-title"><i class="fa fa-sitemap"></i>  Head Count Per Department (SZ Team)</p>
                            <div id="container-department" style="height: 250px;margin: 0 auto"></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12 col-sm-6  col-md-6 col-lg-6 col-xl-6">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--fit  ">
                    <div class="m-portlet__body px-3 py-1" style="background:white">	
                        <div class="m-scrollable" data-scrollable="true" data-max-height="290">
                            <p class="dashboard-title"><i class="fa fa-map-marker"></i>  Head Count Per Location</p>
                            <div id="container-site" style="height: 250px;margin: 0 auto"></div>
                        </div>
                    </div>

                </div>
            </div>

            <!--end fourth row-->
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/js/lazyload.min.js" ></script>
<script>
                            var initialize = function () {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>Employee/getDashboardDetails",
                                    cache: false,
                                    success: function (res) {
                                        res = JSON.parse(res.trim());
                                        var gender = res.gender;
                                        var age = res.age;
                                        var department = res.department;
                                        var account = res.account;
                                        var site = res.site;
                                        var site_details = res.site_details;
                                        var birthday = res.birthday;
                                        var work = res.work;
                                        var anniversary = res.anniversary;

                                        if (res.pending === '0') {
                                            $("#btn-pendingupdate").hide();
                                        } else {
                                            $("#btn-pendingupdate").show();
                                            $("#btn-pendingupdate span").html(res.pending);
                                        }

                                        var series_gender = [];
                                        $.each(gender, function (x, item) {
                                            series_gender.push({name: item.gender, y: parseInt(item.count)});
                                        });
                                        var series_department = [];
                                        $.each(department, function (x, item) {
                                            series_department.push({name: item.dep_name, y: parseInt(item.count)});
                                        });
                                        var series_account = [];
                                        $.each(account, function (x, item) {
                                            series_account.push({name: item.acc_name, y: parseInt(item.count)});
                                        });

                                        var series_site_admin = [];
                                        var series_site_agent = [];
                                        var options_site = [];
                                        $.each(site, function (x, item) {
                                            options_site.push(item.code);
                                            $.each(site_details, function (y, subitem) {
                                                if (item.code === subitem.code) {
                                                    if (subitem.class === 'Admin') {
                                                        series_site_admin.push(parseInt(subitem.count));
                                                    } else {
                                                        series_site_agent.push(parseInt(subitem.count));
                                                    }
                                                }

                                            });
                                        });
                                        $("#container-birthday").html("");
                                        var str_birthday = '';
                                        var modulo_cnt = 0;
                                        $.each(birthday, function (x, item) {
                                            if (modulo_cnt % 2 === 0) {
                                                str_birthday += '<div class="row no-gutters">';
                                            }
                                            var fname = item.fname.split(" ");
                                            str_birthday += '<div class="col-md-6"><div class="m-widget4">'
                                                    + '<div class="m-widget4__item">'
                                                    + '<div class="m-widget4__img m-widget4__img--logo">'
                                                    + '<img src="<?php echo base_url(); ?>' + item.pic + '" class="lazy-img" alt=""> '
                                                    + '</div>'
                                                    + '<div class="m-widget4__info">'
                                                    + '<span class="m-widget4__title" style="font-size:12px">'
                                                    + item.lname + ', ' + fname[0]
                                                    + '</span><br> '
                                                    + '<span class="m-widget4__sub m--font-bold font-green">'
                                                    + moment(item.birthday).format('MMM DD')
                                                    + '</span>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>';
                                            if (modulo_cnt % 2 !== 0) {
                                                str_birthday += '</div>';
                                            }
                                            modulo_cnt++;
                                        });
                                        $("#container-birthday").html(str_birthday);

                                        $("#container-anniversary").html("");
                                        var str_anniversary = '';
                                        modulo_cnt = 0;
                                        $.each(anniversary, function (x, item) {
                                            if (modulo_cnt % 2 === 0) {
                                                str_anniversary += '<div class="row no-gutters">';
                                            }
                                            var fname = item.fname.split(" ");
                                            str_anniversary += '<div class="col-md-6"><div class="m-widget4">'
                                                    + '<div class="m-widget4__item">'
                                                    + '<div class="m-widget4__img m-widget4__img--logo">'
                                                    + '<img src="<?php echo base_url(); ?>' + item.pic + '" class="lazy-img" alt=""> '
                                                    + '</div>'
                                                    + '<div class="m-widget4__info">'
                                                    + '<span class="m-widget4__title" style="font-size:12px">'
                                                    + item.lname + ', ' + fname[0]
                                                    + '</span><br> '
                                                    + '<span class="m-widget4__sub m--font-bold font-green">'
                                                    + moment(item.dateFrom).format('MMM DD')
                                                    + '</span>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>'
                                                    + '</div>';
                                            if (modulo_cnt % 2 !== 0) {
                                                str_anniversary += '</div>';
                                            }
                                            modulo_cnt++;
                                        });
                                        $("#container-anniversary").html(str_anniversary);
                                        new LazyLoad({
                                            elements_selector: ".lazy-img"
                                        });
                                        $('.lazy-img').on('error', function () {
                                            $(this).attr('onerror', null);
                                            $(this).attr('src', '<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>');
                                        });
                                        //CHART - GENDER
                                        // Build the chart
                                        Highcharts.chart('container-gender', {
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false,
                                                type: 'pie'
                                            },
                                            title: {
                                                text: null
                                            },
                                            tooltip: {
                                                pointFormat: '{series.name} total: <b>{point.y}</b>'
                                            },
                                            plotOptions: {
                                                pie: {
                                                    allowPointSelect: true,
                                                    cursor: 'pointer',
                                                    dataLabels: {
                                                        enabled: false
                                                    },
                                                    showInLegend: true
                                                }
                                            },
                                            series: [{
                                                    name: 'Gender',
                                                    colorByPoint: true,
                                                    data: series_gender
                                                }]
                                        });
                                        //CHART - AGE

                                        // Build the chart
                                        Highcharts.chart('container-age', {
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false,
                                                type: 'pie'
                                            },
                                            title: {
                                                text: null
                                            },
                                            tooltip: {
                                                pointFormat: '{series.name} total: <b>{point.y}</b>'
                                            },
                                            plotOptions: {
                                                pie: {
                                                    allowPointSelect: true,
                                                    cursor: 'pointer',
                                                    dataLabels: {
                                                        enabled: false
                                                    },
                                                    showInLegend: true
                                                }
                                            },
                                            series: [{
                                                    name: 'Age',
                                                    colorByPoint: true,
                                                    data: [{
                                                            name: '<18',
                                                            y: parseInt(age[0])
                                                        }, {
                                                            name: '18-25',
                                                            y: parseInt(age[1])
                                                        }, {
                                                            name: '26-35',
                                                            y: parseInt(age[2])
                                                        }, {
                                                            name: '36-45',
                                                            y: parseInt(age[3])
                                                        }, {
                                                            name: '45<',
                                                            y: parseInt(age[4])
                                                        }]
                                                }]
                                        });
                                        //CHART - YEARS OF WORK

                                        // Build the chart
                                        Highcharts.chart('container-work', {
                                            chart: {
                                                plotBackgroundColor: null,
                                                plotBorderWidth: null,
                                                plotShadow: false,
                                                type: 'pie'
                                            },
                                            title: {
                                                text: null
                                            },
                                            tooltip: {
                                                pointFormat: '{series.name}: <b>{point.y}</b>'
                                            },
                                            plotOptions: {
                                                pie: {
                                                    allowPointSelect: true,
                                                    cursor: 'pointer',
                                                    dataLabels: {
                                                        enabled: false
                                                    },
                                                    showInLegend: true
                                                }
                                            },
                                            series: [{
                                                    name: 'Work',
                                                    colorByPoint: true,
                                                    data: [{
                                                            name: '0-1',
                                                            y: parseInt(work[0])
                                                        }, {
                                                            name: '2-3',
                                                            y: parseInt(work[1])
                                                        }, {
                                                            name: '4-5',
                                                            y: parseInt(work[2])
                                                        }, {
                                                            name: '6-7',
                                                            y: parseInt(work[3])
                                                        }, {
                                                            name: '8-9',
                                                            y: parseInt(work[4])
                                                        }, {
                                                            name: '10<=',
                                                            y: parseInt(work[5])
                                                        }]
                                                }]
                                        });
                                        //CHART - HEAD COUNT PER DEPT

                                        // Build the chart
                                        Highcharts.chart('container-department', {
                                            chart: {
                                                type: 'column'
                                            },
                                            title: null,
                                            xAxis: {
                                                type: 'category'
                                            },
                                            yAxis: {
                                                title: {
                                                    text: null
                                                }

                                            },
                                            legend: {
                                                enabled: false
                                            },
                                            plotOptions: {
                                                series: {
                                                    borderWidth: 0,
                                                    dataLabels: {
                                                        enabled: true,
                                                        format: '{point.y}'
                                                    }
                                                }
                                            },

                                            tooltip: {
                                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                                            },

                                            "series": [
                                                {
                                                    "name": "Departments",
                                                    "colorByPoint": true,
                                                    "data": series_department
                                                }
                                            ]
                                        });
                                        //CHART - HEAD COUNT PER DEPT

                                        // Build the chart
                                        Highcharts.chart('container-account', {
                                            chart: {
                                                type: 'column'
                                            },
                                            title: null,
                                            xAxis: {
                                                type: 'category'
                                            },
                                            yAxis: {
                                                title: {
                                                    text: null
                                                }

                                            },
                                            legend: {
                                                enabled: false
                                            },
                                            plotOptions: {
                                                series: {
                                                    borderWidth: 0,
                                                    dataLabels: {
                                                        enabled: true,
                                                        format: '{point.y}'
                                                    }
                                                }
                                            },

                                            tooltip: {
                                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
                                            },

                                            "series": [
                                                {
                                                    "name": "Accounts",
                                                    "colorByPoint": true,
                                                    "data": series_account
                                                }
                                            ]
                                        });
                                        //CHART - HEAD COUNT PER SITE

                                        // Build the chart

                                        Highcharts.chart('container-site', {
                                            chart: {
                                                type: 'bar'
                                            },
                                            title: {
                                                text: null
                                            },
                                            xAxis: {
                                                categories: options_site
                                            },
                                            yAxis: {
                                                min: 0,
                                                title: {
                                                    text: null
                                                }
                                            },
                                            legend: {
                                                enabled: false
                                            },
                                            plotOptions: {
                                                series: {
                                                    stacking: 'normal'
                                                }
                                            },
                                            series: [{
                                                    name: 'Admin',
                                                    data: series_site_admin
                                                }, {
                                                    name: 'Ambassadors',
                                                    data: series_site_agent
                                                }]
                                        });
                                    }

                                });
                            };
                            $(function () {

                                initialize();

                            });
</script>