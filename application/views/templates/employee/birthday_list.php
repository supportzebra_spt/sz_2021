<style>

    .btn-green{
        background: #4CAF50;
        color:white !important;
    }
    .btn-green:hover{
        background:#2a862e !important;
    }
    .font-green{
        color: #4CAF50 !important;
    }
    .btn-outline-green{
        color: #4CAF50 !important;
        background:white !important;
        border-color:#4CAF50 !important;
    }
    .btn-outline-green:hover{
        background: #4CAF50 !important;
        color:white !important;
        border-color:#4CAF50 !important;
    }
    .fa-unsorted{
        color:#aaa;
    }
    #table-birthdates th a{
        margin-left:4px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <div class="m-content">
        <div class="row">
            <div class="col-md-4">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">BIRTHDAYS: <br /><small>Employee Management System</small></h3>   
            </div>
            <div class="col-md-8 text-right" style="margin-top:13px">
              <div class="btn-group m-btn-group flex-wrap " role="group" aria-label="...">
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/dashboard'"><span><i class="fa fa-dashboard"></i><span>Dashboard</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/profiles'"><span><i class="fa fa-list-alt"></i><span>Profiles</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-green" onclick="location.href = '<?php echo base_url(); ?>Employee/birthday_list'"><span><i class="fa fa-birthday-cake"></i><span>Birthdays</span></span></button>
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/control_panel'"><span><i class="fa fa-gear"></i><span>Control Panel</span></span></button>
                </div>
            </div>
        </div>
        <br />
        <div class="card">
            <div class="card-body" style="background: #eee">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 mb-2">
                                        <label for="" class="m--font-bolder font-green">Search:</label>
                                        <input type="text" class="form-control form-control-sm m-input" id="input-search"/>
                                    </div>
                                    <div class="col-md-12 mb-2">
                                        <hr />
                                        <label for="" class="m--font-bolder font-green">Date Range:</label>
                                        <div class="m-radio-list">
                                            <label class="m-radio  m-radio--info">
                                                <input type="radio" name="radio-date" value="today" checked="checked"> Today
                                                <span></span>
                                            </label>
                                            <label class="m-radio  m-radio--info">
                                                <input type="radio" name="radio-date" value="week">This Week
                                                <span></span>
                                            </label>
                                            <label class="m-radio  m-radio--info">
                                                <input type="radio" name="radio-date" value="year">Whole Year
                                                <span></span>
                                            </label>
                                            <label class="m-radio  m-radio--info">
                                                <input type="radio" name="radio-date" value="month"> 
                                                <select id="select-month" class="form-control form-control-sm m-input" disabled>

                                                </select> 
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-2">
                                        <hr />
                                        <label for="" class="m--font-bolder font-green">Class Type:</label>
                                        <div class="m-radio-list">
                                            <label class="m-radio m-radio--brand">
                                                <input type="radio" name="radio-class" value="all"  checked="checked"> All
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--brand">
                                                <input type="radio" name="radio-class" value="Admin"> SZ Team
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--brand">
                                                <input type="radio" name="radio-class" value="Agent"> Ambassador
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="birthday-alert" style="display:none">
                            <div class="m-alert__icon">
                                <i class="flaticon-exclamation-1"></i>
                                <span></span>
                            </div>
                            <div class="m-alert__text">
                                <strong>No Birthdays!</strong> Please adjust your search criteria and try again.		
                            </div>			  	
                        </div>
                        <div style="padding:8px;background:white;" class="myheader"  data-scrollable="true" data-max-height="444" >
                            <table class="table table-bordered table-sm m-0" style="background:white;display:none" id="table-birthdates" >
                                <thead style="background:#4CAF50;color:white">
                                <th>Last Name <a href="" class="anchor-class"><i class="fa fa-unsorted"></i></a></th>
                                <th>First Name  <a href="" class="anchor-class"><i class="fa fa-unsorted"></i></a></th>
                                <th>Birth Month and Day  <a href="" class="anchor-class"><i class="fa fa-unsorted"></i></a></th>
                                <th>Year  <a href="" class="anchor-class"><i class="fa fa-unsorted"></i></a></th>
                                <th>Age  <a href="" class="anchor-class"><i class="fa fa-unsorted"></i></a></th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/js/lazyload.min.js" ></script>
<script type="text/javascript">
                            var getBirthdayList = function () {
                                var searchname = $("#input-search").val();
                                var classtype = $("[name='radio-class']:checked").val();
                                var date = $("[name='radio-date']:checked").val();
                                var startdate = '';
                                var enddate = '';
                                if (date === 'today') {
                                    startdate = moment();
                                    enddate = moment();
                                } else if (date === 'week') {
                                    startdate = moment().startOf('week');
                                    enddate = moment().endOf('week');
                                } else if (date === 'year') {
                                    startdate = moment().startOf('year');
                                    enddate = moment().endOf('year');
                                } else if (date === 'month') {
                                    var month = $("#select-month").val();
                                    startdate = moment().month(month).startOf('month');
                                    enddate = moment().month(month).endOf('month');
                                } else {
                                    alert("WRONG");
                                }

                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>Employee/getBirthdayList",
                                    data: {
                                        searchname: searchname,
                                        classtype: classtype,
                                        startdate: moment(startdate).format('YYYY-MM-DD'),
                                        enddate: moment(enddate).format('YYYY-MM-DD')
                                    },
                                    cache: false,
                                    success: function (res) {
                                        res = JSON.parse(res.trim());
                                        var birthdates = res.birthdays;
                                        $("#table-birthdates tbody").html("");

                                        var str_birthday = '';
                                        $.each(birthdates, function (i, birthday) {
                                            var background = '#fff';
                                            var color = '#333';
                                            if (moment().format('MMM DD') === moment(birthday.birthday).format('MMM DD')) {
                                                background = '#4CAF50';
                                                color = '#fff';
                                            }
                                            str_birthday += '<tr style="background:' + background + ';">'
                                                    + '<td style="color:' + color + '">' + birthday.lname + '</td>'
                                                    + '<td style="color:' + color + '">' + birthday.fname + '</td>'
                                                    + '<td class="date" data-date="' + birthday.birthday + '" style="color:' + color + '" class="m--font-brand m--font-bold">' + moment(birthday.birthday).format('MMMM DD') + '</td>'
                                                    + '<td class="num" style="color:' + color + '">' + moment(birthday.birthday).format('YYYY') + '</td>'
                                                    + '<td class="num" style="color:' + color + '">' + birthday.age + '</td>'
                                                    + '</tr>';
                                        });
                                        if (str_birthday === '') {
                                            $("#birthday-alert").show();
                                            $("#table-birthdates").hide();
                                            $(".myheader").hide();
                                        } else {
                                            $("#birthday-alert").hide();
                                            $("#table-birthdates").show();
                                            $(".myheader").show();
                                            $("#table-birthdates tbody").html(str_birthday);
                                            $('.anchor-class').eq(2).closest('th').removeClass('selected');
                                            $('.anchor-class').eq(2).click();
                                        }


                                    }
                                });
                            };
                            $(function () {
                                var months = moment.months();
                                $("#select-month").html('');
                                $.each(months, function (i, month) {
                                    if (moment().format('MMMM') === month) {
                                        $("#select-month").append('<option value="' + month + '" selected>' + month + '</option>');
                                    } else {
                                        $("#select-month").append('<option value="' + month + '">' + month + '</option>');
                                    }

                                });
                                getBirthdayList();
                                $("[name='radio-class']").change(function () {
                                    getBirthdayList();
                                });
                                $("[name='radio-date']").change(function () {
                                    var date = $("[name='radio-date']:checked").val();
                                    if (date === 'month') {
                                        $("#select-month").prop('disabled', false);
                                        getBirthdayList();
                                    } else {
                                        $("#select-month").prop('disabled', true);
                                        $("#select-month").val(moment().format('MMMM'));
                                        getBirthdayList();
                                    }
                                });
                                $("#input-search").on('keyup', function () {
                                    getBirthdayList();
                                });
                                $("#select-month").change(function () {
                                    getBirthdayList();
                                });
                                function OrderBy(a, b, num, date) {
                                    if (date)
                                        return a - b;
                                    if (num)
                                        return a - b;
                                    if (a < b)
                                        return -1;
                                    if (a > b)
                                        return 1;
                                    return 0;
                                }
                                $(".anchor-class").click(function (e) {
                                    e.preventDefault();
                                    var that = this;
                                    $(".anchor-class").find('i').removeClass().addClass('fa fa-unsorted').css('color', '#eee');
                                    var $th = $(that).closest('th');
                                    $th.toggleClass('selected');
                                    var isSelected = $th.hasClass('selected');
                                    var column = $th.index();
                                    var $table = $th.closest('table');
                                    var isNum = $table.find('tbody > tr').children('td').eq(column).hasClass('num');
                                    var isDate = $table.find('tbody > tr').children('td').eq(column).hasClass('date');
                                    var rows = $table.find('tbody > tr').get();
                                    rows.sort(function (rowA, rowB) {
                                        if (isDate) {
                                            var keyA = moment(moment($(rowA).children('td').eq(column).data('date')).format('MM DD'), 'MM DD').valueOf();
                                            var keyB = moment(moment($(rowB).children('td').eq(column).data('date')).format('MM DD'), 'MM DD').valueOf();
                                        } else {
                                            var keyA = $(rowA).children('td').eq(column).text().toUpperCase();
                                            var keyB = $(rowB).children('td').eq(column).text().toUpperCase();
                                        }

                                        if (isSelected) {
                                            $(that).find('i').removeClass().addClass('fa fa-caret-up').css('color', '#333');
                                            return OrderBy(keyA, keyB, isNum, isDate);
                                        } else {

                                            $(that).find('i').removeClass().addClass('fa fa-caret-down').css('color', '#333');
                                            return OrderBy(keyB, keyA, isNum, isDate);
                                        }
                                    });
                                    $.each(rows, function (index, row) {
                                        $table.children('tbody').append(row);
                                    });
                                    return false;

                                });

                            });
</script>