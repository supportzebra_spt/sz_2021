
<?php foreach ($employees as $key => $emp): ?>
    <div style="background-image:url(assets/images/img/logo_header.png);background-size:100%;background-repeat:no-repeat;height:100%;">
        <div style="padding: 96px 96px 48px 96px;"> <!-- 1 inch and 1/2 inch-->
            <h2 style="text-transform: capitalize"><b><?php echo $emp->fname . ' ' . $emp->mname . ' ' . $emp->lname . ' ' . $emp->nameExt; ?></b></h2>
            <table style="width:100%">
                <tr>
                    <td style="width:120px"><h5 class="w3-text-blue-gray"><b>&bull; About Me</b></h5></td>;
                    <td><hr/></td>
                </tr>
            </table>
            <table style="width: 100%;" cellpadding="10">
                <tr> 
                    <td style="width:120px !Important;">
                        <img src="<?php echo  $emp->pic; ?>" class="w3-round" alt="" style="width: 100px;border:2px solid #607D8B"/>
                    </td>
                    <td  style="font-size:12px;" valign="top">
                        <p style="text-transform: capitalize">Gender: <b class="w3-text-blue-gray"><?php echo $emp->gender; ?></b></p><br />
                        <p>Birthday: <b class="w3-text-blue-gray"><?php echo date_format(date_create($emp->birthday), "F d, Y"); ?></b></p><br />
                        <p>Age: <b class="w3-text-blue-gray"><?php echo date_diff(date_create($emp->birthday), date_create('now'))->y; ?> years old</b></p><br />
                        <p style="text-transform: capitalize">Religion: <b class="w3-text-blue-gray"><?php echo $emp->religion; ?></b></p>
                    </td>
                    <td  style="font-size:12px;" valign="top">
                        <p style="text-transform: capitalize">Civil Status: <b class="w3-text-blue-gray"><?php echo $emp->civilStatus; ?></b></p><br />
                        <p>Mobile No: <b class="w3-text-blue-gray"><?php echo $emp->cell; ?></b></p><br />
                        <?php if ($emp->tel !== '' || $emp->tel !== NULL): ?> <p>Alternate No: <b class="w3-text-blue-gray"><?php echo $emp->tel; ?></b></p><br /><?php endif; ?>
                        <p>Email: <b class="w3-text-blue-gray"><?php echo $emp->email; ?></b></p>
                    </td>
                </tr>
            </table>
            <table style="width:100%">
                <tr>
                    <td style="width:110px"><h5 class="w3-text-blue-gray"><b>&bull; Address</b></h5></td>;
                    <td><hr/></td>
                </tr>
            </table>
            <table  style="width: 100%;" cellpadding="7">
                <tr>
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>Present Address: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top" colspan="3">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo implode(', ', explode('|', $emp->presentAddress)); ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>Permanent Address: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top" colspan="3">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo implode(', ', explode('|', $emp->permanentAddress)); ?></b></p>
                    </td>
                </tr>

            </table>
            <table style="width:100%">
                <tr>
                    <td style="width:125px"><h5 class="w3-text-blue-gray"><b>&bull; Education</b></h5></td>;
                    <td><hr/></td>
                </tr>
            </table>
            <table  style="width: 100%;" cellpadding="7">
                <tr> 
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>Educ. Attainment: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top" colspan="2">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->educationalAttainment; ?></b></p>
                    </td>
                </tr>
                <?php if ($emp->course !== '' || $emp->course !== NULL): ?>
                    <tr> 
                        <td style="font-size:12px;width:130px" valign="top">
                            <p>College Course: 
                            </p>
                        </td>
                        <td style="font-size:12px;" valign="top" colspan="2">
                            <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->course; ?></b></p>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr> 
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>Last School: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->lastSchool; ?></b></p>
                    </td>
                    <td style="width:120px;font-size:12px" valign="top"> <p>Year Left: <b class="w3-text-blue-gray"><?php echo $emp->lastSchoolYear; ?></b></p></td>
                </tr>
                <tr> 
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>High School: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->highSchool; ?></b></p>
                    </td>
                    <td style="width:120px;font-size:12px" valign="top"> <p>Year Left: <b class="w3-text-blue-gray"><?php echo $emp->highSchoolYear; ?></b></p></td>
                </tr>

            </table>

            <table style="width:100%">
                <tr>
                    <td style="width:235px"><h5 class="w3-text-blue-gray"><b>&bull; Government Numbers</b></h5></td>;
                    <td><hr/></td>
                </tr>
            </table>

            <table  style="width: 100%;" cellpadding="7">
                <tr>
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>Pag-ibig: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top" colspan="3">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->pagibig; ?></b></p>
                    </td>
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>Philhealth: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top" colspan="3">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->philhealth; ?></b></p>
                    </td>
                </tr>
                <tr> 
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>Social Security: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top" colspan="3">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->sss; ?></b></p>
                    </td>
                    <td style="font-size:12px;width:130px" valign="top">
                        <p>BIR/TIN: 
                        </p>
                    </td>
                    <td style="font-size:12px;" valign="top" colspan="3">
                        <p style="text-transform: capitalize"><b class="w3-text-blue-gray"><?php echo $emp->bir; ?></b></p>
                    </td>
                </tr>

            </table>
            <table style="width:100%">
                <tr>
                    <td style="width:150px"><h5 class="w3-text-blue-gray"><b>&bull; Work Details</b></h5></td>;
                    <td><hr/></td>
                </tr>
            </table>



            <table  style="width: 100%;" >
                <tr> 
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Supervisor Name: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo $emp->supervisor_name; ?></b></p>
                    </td>
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Supervisor Email: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo $emp->supervisor_email ?></b></p><br />
                    </td>
                </tr>
                <tr> 
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Account: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo $emp->acc_name; ?></b></p>
                    </td>
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Department: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo $emp->dep_details; ?></b></p>
                    </td>

                </tr>
                <tr> 
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Position: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo $emp->pos_details; ?></b></p>
                    </td>
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Status: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo $emp->status; ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Location / Site: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo $emp->code; ?></b></p>
                    </td>
                    <td style="font-size:12px;width:50%;padding:8px 3px 0px 8px" valign="top">
                        <p style="text-transform: capitalize">Start Date: <br/><hr style="margin:3px 0;color:white"/><b class="w3-text-blue-gray" ><?php echo date_format(date_create($emp->dateFrom), "M d, Y"); ?></b></p>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <?php if ($key === COUNT($employees)): ?>
        <pagebreak />
    <?php endif; ?>
<?php endforeach; ?>
