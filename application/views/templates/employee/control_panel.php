<style type="text/css">
	.bg-green{
		background: #4CAF50 !important;
	}
	.btn-green{
		background: #4CAF50;
		color:white !important;
	}
	.btn-green:hover{
		background:#2a862e !important;
	} 
	.btn-outline-green{
		color: #4CAF50 !important;
		background:white !important;
		border-color:#4CAF50 !important;
	}
	.btn-outline-green:hover{
		background: #4CAF50 !important;
		color:white !important;
		border-color:#4CAF50 !important;
	}
	.font-green{
		color: #4CAF50 !important;
	}
	.border-light-green{
		border-color: #4CAF507d !important;
	}
	.card-shadow{
		box-shadow:  0 1px 15px 1px rgba(69,65,78,.08)
	}
	#bootstrap-duallistbox-nonselected-list_from option,#bootstrap-duallistbox-selected-list_from option {
		font-size:13px !important;
	}  
	.d-xl-inline{
		margin-right:3px;
	}

	.nav.nav-pills .nav-link{
		font-size:13px !important;
	}
	.font-10{
		font-size:10px !important;
	}
	.font-11{
		font-size:11px !important;
	}
	.font-12{
		font-size:12px !important;
	}
	.font-13{
		font-size:13px !important;
	}
	.font-16{
		font-size:16px !important;
	}
	#table-approval td{
		font-size:13px;
	}
	.m-accordion__item{
		border:none;
	}
	.m-accordion__item-mode{
		margin-left:5px;
		font-size:15px !important; 
		color: white !important;
	}
	.m-accordion__item-icon i{
		font-size:18px !important;  
	}
	.m-accordion__item-title{
		font-size:15px !important;  
	}
	.m-accordion__item-head{
		background:#4CAF50 !important;  
		color:white !important;  
		font-weight:bolder;
	} 
	.m-accordion__item-head:hover{
		background: #2a862e !important;  
	}
	.accordion-approve,.accordion-reject,.accordion-approve-insertfamily{
		width: 25px !important;
		height: 25px !important;
	}


	.daterangepicker .daterangepicker_input .input-mini {
		display:none;
	}
	.m-accordion .m-accordion__item .m-accordion__item-head{
		padding:10px 20px !Important;
	}
	.nav-pills .nav-link{
		background:#eee;
	}
	.nav-pills .nav-link.active{
		background:#555 !important;
		color:white !important;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="content">

	<div class="m-content">
		<div class="row">
			<div class="col-md-4">
				<h3 class="m-subheader__title m-subheader__title--separator font-poppins">CONTROL PANEL: <br /><small>Employee Management System</small></h3>   
			</div>
			<div class="col-md-8 text-right" style="margin-top:13px">
				<div class="btn-group m-btn-group flex-wrap " role="group" aria-label="...">
					<button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/dashboard'"><span><i class="fa fa-dashboard"></i><span>Dashboard</span></span></button>
					<button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/profiles'"><span><i class="fa fa-list-alt"></i><span>Profiles</span></span></button>
					<button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/birthday_list'"><span><i class="fa fa-birthday-cake"></i><span>Birthdays</span></span></button>
					<button type="button" class="m-btn btn m-btn m-btn--icon btn-green" onclick="location.href = '<?php echo base_url(); ?>Employee/control_panel'"><span><i class="fa fa-gear"></i><span>Control Panel</span></span></button>
				</div>
			</div>
		</div>

		<br />
		<div class="card card-shadow" >
			<div class="card-body" id="card-container">
				<ul class="nav nav-tabs mb-0" role="tablist" >
					<li class="nav-item">
						<a class="nav-link  active show" data-toggle="tab" href="#tab-approval"><span class="fa fa-thumbs-o-up d-lg-none d-xl-inline font-green"></span> Request Approval</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-userupdate"><span class="fa fa-pencil d-lg-none d-xl-inline font-green"></span> User Update </a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-resolution"><span class="fa fa-puzzle-piece d-lg-none d-xl-inline font-green"></span> Resolution Center</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-download"><span class="fa fa-download d-lg-none d-xl-inline font-green"></span> File Download</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-requestApproval" id="customApprovalAssignment"><span class="fa fa-gear d-lg-none d-xl-inline font-green"></span> Request Approval Settings</a>
					</li>
				</ul>
				<br />
				<div class="tab-content"> 
					<div class="tab-pane active" id="tab-approval" role="tabpanel">
						<div class="card">
							<div class="card-header m--font-bolder font-green font-16">
								REQUEST APPROVALS
							</div>
							<div class="card-body">
								<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand  fade show mt-2" role="alert" style="display:none" id="accordion-approval-alert">
									<div class="m-alert__icon">
										<i class="flaticon-exclamation-1"></i>
										<span></span>
									</div>
									<div class="m-alert__text">
										<strong>No Record!</strong> You do not have employee profile update request.    
									</div>                  
								</div>
								<div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-approval" role="tablist">                      

								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="tab-userupdate" role="tabpanel">
						<div class="card">
							<div class="card-header m--font-bolder font-green font-16">
								<div class="row">
									<div class="col-md-12">USER UPDATES</div>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-12">
										<div class="m-form__group form-group">
											<select name="from" id="dualListBox-userupdate" size="8" multiple="multiple"></select>
										</div>
									</div>
								</div>
								<button type="button" class="btn btn-green" id="btn-saveEnableUserUpdate">Save Changes</button>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab-resolution" role="tabpanel">
						<div class="card">
							<div class="card-header m--font-bolder font-green font-16">
								<div class="row">
									<div class="col-md-12">RESOLUTION CENTER</div>
								</div>
							</div>
							<div class="card-body">
								<table class="table table-bordered table-striped table-hover table-sm" id="table-resolution">
									<thead>
										<th>Field Name</th>
										<th>Importance</th>
										<th>Total Missing Data</th>
										<th></th>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>

					</div>
					<div class="tab-pane" id="tab-download" role="tabpanel">
						<div class="card">
							<div class="card-header m--font-bolder font-green font-16">
								<div class="row">
									<div class="col-md-12">FILE DOWNLOAD</div>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-9">
										<div class="alert alert-danger" role="alert" id="myalert" style="display:none">
											<strong>Wait!</strong> Chosen names exceeded the maximum limit of 20. Remove a total of <span>0</span>
										</div>
										<div class="m-form__group form-group">
											<select name="from" id="dualListBox-exports" size="20" multiple="multiple" ></select>
										</div>
									</div>
									<div class="col-md-3">
										<label for="">Employee Filter:</label>
										<select name="" class="m-input form-control font-12 form-control-sm" id="search-active">
											<option value="">All</option>
											<option value="yes" selected="">Active</option>
											<option value="No">Inactive</option>
										</select>
										<hr />
										<div class="card">
											<div class="card-body">
												<div class="row">
													<div class="col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12 my-2">
														<button class="btn btn-success m-btn m-btn--icon btn-block" id="btn-excel">
															<span>
																<i class="fa fa-file-excel-o"></i>
																<span>Excel</span>
															</span>
														</button>
													</div>
													<div class="col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12 my-2">
														<button class="btn btn-info m-btn m-btn--icon btn-block" id="btn-pdf" disabled>
															<span>
																<i class="fa fa-file-pdf-o"></i>
																<span>Printable</span>
															</span>
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="tab-pane" id="tab-requestApproval" role="tabpanel">
						<div class="card">
							<div class="card-header m--font-bolder font-green font-16">
								REQUEST APPROVAL SETTINGS
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-5">
										<div class="form-group m-form__group row">
											<div class="col-12">
												<select class="form-control m-select2 select2" id="empApprovers" name="empApprovers" data-toggle="m-tooltip" data-toggle="m-tooltip" title="Approver Name"></select>
											</div>
										</div>
									</div>
									<div class="col-2">
										<div class="form-group m-form__group row">
											<div class="col-12">
												<select class="form-control m-select2 select2" id="changeType" name="changeType" data-toggle="m-tooltip" data-toggle="m-tooltip" title="Change Type">
													<option value="0">ALL</option>
													<option value="1">Change</option>
													<option value="2">Exempt</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-5">
										<div class="form-group m-form__group row">
											<div class="col-12">
												<select class="form-control m-select2 select2" id="customAccount" name="customAccount" data-toggle="m-tooltip" data-toggle="m-tooltip" title="Account"></select>
											</div>
										</div>
									</div>
									<div class="col-12">
										<div class="btn-group m-btn-group float-right" role="group" aria-label="...">
											<button type="button" class="btn btn-brand m-btn m-btn--icon" data-toggle="modal" data-target="#assignModal" id="btn-assignChanges" data-from="changes">
												<span>
													<i class="fa fa-plus"></i>
													<span>Assign Change</span>
												</span>
											</button>
											<button type="button" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#assignModal" id="btn-assignExemption" data-from="exemption">
												<span>
													<i class="fa fa-plus"></i>
													<span>Assign Exemption</span>
												</span>
											</button>
										</div>
									</div>
									<div class="col-12 py-3">
										<div id="reassignmentDatatable"></div>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<!-- INSERT DATATABLE -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="viewNamesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">List of Missing Data : <span class='m--font-bolder m--font-warning' id="name-field"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body"  data-scrollbar-shown="true" data-scrollable="true" data-max-height="500">
				<table class="table table-bordered table-hover table-sm" id="table-missing">
					<thead>
						<th>Name</th>

					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>  
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="assignTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger alert-dismissible fade show" role="alert" id="errorAlertAssignment" style="display:none;">
                </div>
				<div class="form-group m-form__group row">
					<label class="col-form-label  col-md-3 col-sm-12" for="assignApprover">Approver:</label>
					<div class=" col-md-9 col-sm-12">
						<select class="form-control" id="assignApprover"></select>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-md-3 col-sm-12" for="assignApprover">Accounts:</label>
					<div class="col-md-9 col-sm-12">
						<select class="form-control" id="assignAccounts"></select>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-md-3 col-sm-12" for="assignApprover">Level:</label>
					<div class="col-md-9 col-sm-12">
						<div class="m-bootstrap-touchspin-brand">
                            <input type="text" class="form-control" value="0" type="number" id="assignLevel">
                        </div>
					</div>
				</div>
				<div id="assignEmployeeContainer">
					<h5>Assign To:</h5>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-md-3 col-sm-12" for="assignApprover">Employee:</label>
						<div class="col-md-9 col-sm-12">
							<select class="form-control" id="assignEmployee"></select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-brand" id="btn-submitAssignChanges">Assign Change</button>
				<button type="submit" class="btn btn-success" id="btn-submitAssignExemption">Assign Exemption</button>
			</div>
		</div>
	</div>
</div>  
<link href="<?php echo base_url(); ?>assets/src/custom/css/bootstrap-duallistbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.bootstrap-duallistbox.min.js" ></script>
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/employee/employee.js">
</script>
<script type="text/javascript">
	function tz_date(thedate, format = null) {
		var tz = moment.tz(new Date(thedate), "Asia/Manila");
		return (format === null) ? moment(tz) : moment(tz).format(format);
	}
	
	var getActiveEmployees = function (callback) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>Employee/get_all_active_employees",
			cache: false,
			success: function (res) {
				res = JSON.parse(res.trim());assignEmployee
				$("#assignApprover").html('<option>--</option>');
				$.each(res.activeEmployees,(index, item)=>{
					$("#assignApprover").append("<option value="+item.emp_id+">"+item.lname+", "+item.fname+"</option>");
				});
				$("#assignEmployee").html('<option>--</option>');
				$.each(res.activeEmployees,(index, item)=>{
					$("#assignEmployee").append("<option value="+item.emp_id+">"+item.lname+", "+item.fname+"</option>");
				});
				callback();
			}
		});
	};
	var getEmployees = function () {
		var searchactive = $("#search-active").val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>Employee/getEmployeeForExports",
			data: {
				searchactive: searchactive
			},
			cache: false,
			success: function (res) {
				res = JSON.parse(res.trim());
				$("#dualListBox-exports").html("");
				var str = "";
				$.each(res.employees, function (key, obj) {
					str += '<option value="' + obj.emp_id + '" class="text-capitalize">' + obj.lname + ' ' + obj.fname + '</option>';
				});
				$("#dualListBox-exports").html(str);
				$('#dualListBox-exports').bootstrapDualListbox('refresh');
			}
		});
	};
	var getEmployeesForResolution = function () {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>Employee/getEmployeesForResolution",
			cache: false,
			success: function (res) {
				res = JSON.parse(res.trim());
				$("#table-resolution tbody").html("");
				var str = "";
				var names = res.names;
				var important = res.important;
				$.each(res.result, function (key, obj) {
					var currentName = (typeof names[key] === 'undefined') ? key : names[key];
					var importance = '<span class="m--font-info m--font-bolder">Low</span>';
					if (jQuery.inArray(key, important) !== -1) {
						importance = '<span class="m--font-danger m--font-bolder">High</span>';
					}
					str += '<tr>'
					+ '<td>' + currentName + '</td>'
					+ '<td>' + importance + '</td>'
					+ '<td class="m--font-bolder font-green">' + obj.count + '</td>'
					+ '<td><a href="#" class="m-link m-link--state m-link--brand" data-toggle="modal" data-target="#viewNamesModal" data-name="' + currentName + '" data-field="' + key + '" data-emp_ids = "' + obj.emp_ids + '">View <i class="fa fa-caret-right"></i></a></td>'
					+ '</tr>';
				});
				$("#table-resolution tbody").html(str);
			}
		});
	};
	function get_all_accounts(callback) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>Employee/get_all_accounts",
			cache: false,
			success: function(result) {
				result = JSON.parse(result.trim());
				callback(result);
			}
		});
	}
	$(function () {
		var dualListBox_exports = $('#dualListBox-exports').bootstrapDualListbox({
			nonSelectedListLabel: '<span class="m--font-bolder m--font-danger">Non-selected:</span>',
			selectedListLabel: '<span class="m--font-bolder font-green">Selected Employee:</span>',
			selectorMinimalHeight: 150
		});
		var dualListContainer = dualListBox_exports.bootstrapDualListbox('getContainer');
		dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
		dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
		dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
		dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');

		var dualListBox_userupdate = $('#dualListBox-userupdate').bootstrapDualListbox({
			nonSelectedListLabel: '<span class="m--font-bolder m--font-danger">Disabled User:</span>',
			selectedListLabel: '<span class="m--font-bolder font-green">Enabled User:</span>',
			selectorMinimalHeight: 150
		});
		var dualListContainer2 = dualListBox_userupdate.bootstrapDualListbox('getContainer');
		dualListContainer2.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
		dualListContainer2.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
		dualListContainer2.find('.move i').removeClass().addClass('fa fa-arrow-right');
		dualListContainer2.find('.remove i').removeClass().addClass('fa fa-arrow-left');

		getActiveEmployees(()=>{
			// $("#assignEmployee").select2();
			// $("#assignApprover").select2();
			// $("#assignAccounts").select2();
			// $(".select2-container").css('width','100%');
		});
		get_all_accounts(res=>{
			$("#assignAccounts").html('<option>--</option>');
			$.each(res.accounts,(index, item)=>{
				$("#assignAccounts").append("<option value="+item.acc_id+">"+item.acc_name+"</option>");
			})
		})
		$("#assignApprover").change(()=>{
			let emp_id = $("#assignApprover").find(":selected").val();
			$("#assignEmployee").find('option').prop('disabled',false).css('color','#575962');
			$("#assignEmployee").find('option[value="'+emp_id+'"]').prop('disabled',true).css('color','#ccc');
		});
		$("#assignEmployee").change(()=>{
			let emp_id = $("#assignEmployee").find(":selected").val();
			$("#assignApprover").find('option').prop('disabled',false).css('color','#575962');
			$("#assignApprover").find('option[value="'+emp_id+'"]').prop('disabled',true).css('color','#ccc');
		});
		$("#assignModal").on('hide.bs.modal',(e)=>{
			$("#assignApprover").find('option').prop('disabled',false).css('color','#575962');
			$("#assignEmployee").find('option').prop('disabled',false).css('color','#575962');
			$("#assignEmployee,#assignApprover,#assignAccounts").each((i,elem)=>{
				$(elem).val($(elem).find('option:first').val());
			});
			$("#errorAlertAssignment").hide();
			$('#btn-submitAssignExemption').prop('disabled', false);
			$('#btn-submitAssignChanges').prop('disabled', false);
			$("#assignLevel").val('');
		});
		$("#assignModal").on('show.bs.modal',(e)=>{
			let from = $(e.relatedTarget).data('from');
			$("#btn-submitAssignChanges").hide();
			$("#btn-submitAssignExemption").hide();

			if(from=='changes'){
				$("#assignEmployeeContainer").show();
				$("#assignTitle").html('Assign Approval Change');
				$("#btn-submitAssignChanges").show();
			}else{
				$("#assignEmployeeContainer").hide();
				$("#assignTitle").html('Assign Approval Exemption');
				$("#btn-submitAssignExemption").show();
			}
			$("#assignLevel").TouchSpin({
				buttondown_class: 'btn btn-secondary',
				buttonup_class: 'btn btn-secondary',
				min: 1,
				max: 2,
				stepinterval: 50,
				maxboostedstep: 10000000,
				prefix: 'Level'
			});
			$("#assignLevel").trigger("touchspin.updatesettings", {
				max: 2
			});
			$("#assignLevel").val(parseInt(1));
		})
		$('[href="#tab-approval"]').on('click', function (e) {
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>Employee/getEmployeeRequest",
				cache: false,
				success: function (res) {
					res = JSON.parse(res.trim());
					var names = res.names;
					var names_family = res.namesFamily;
					$("#div-accordion-approval").html("");
					var str = "";
					$.each(res.requests, function (key, obj) {

						str += '<div class="m-accordion__item">';
						str += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + obj.emp_id + '" aria-expanded="  false">';
						str += '<span class="m-accordion__item-icon"><i class="fa fa-user-circle"></i></span>';
						str += '<span class="m-accordion__item-title">' + obj.lname + ', ' + obj.fname + '</span>';
						str += '<span class="m-accordion__item-mode"></span>';
						str += '</div>';
						str += '<div class="m-accordion__item-body collapse" id="accordion-item-' + obj.emp_id + '" role="tabpanel" data-parent="#div-accordion-approval"> ';
						str += '<div class="m-accordion__item-content">';

						if (obj.normal !== undefined) {
							str += '<div class="main-div-normal"><h5>Normal Updates</h5><hr class="border-light-green" style="border-width:3px"/><div style="overflow:auto;max-height:300px;" class="mb-4">';
							str += '<table class="table m-table table-sm">';
							str += '<thead style="background:#444;color:white"><th class="text-center">Date Requested</th><th>Field</th><th>Original Value</th><th>New Value</th><th class="text-center">Actions</th></thead><tbody>';
							$.each(obj.normal, function (i, item) {
								var orig = (item.originalValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.originalValue;
								var newValue = (item.newValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.newValue;
								var currentName = (typeof names[item.field] === 'undefined') ? item.field : names[item.field];
								str += '<tr class="font-12">';
								str += '<td class="text-center">' + moment(obj.requestDate).format('lll') + '</td>';
								str += '<td class="m--font-bold">' + currentName + '</td>';
								str += '<td>' + orig + '</td>';
								str += '<td>' + newValue + '</td>';
								str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
								str += '</tr>';
							});
							str += '</tbody></table></div></div>';
						}

						if (obj.family !== undefined) {
							str += '<div class="main-div-family"><h5>Family Changes</h5><hr class="border-light-green" style="border-width:3px"/><div style="overflow:auto;max-height:300px;">';
							str += '<table class="table m-table table-sm">';
							str += '<thead style="background:#444;color:white"><th class="text-center">Date Requested</th><th class="text-center">Type</th><th>Field</th><th>Original Value</th><th>New Value</th><th class="text-center">Actions</th></thead><tbody>';

							$.each(obj.family, function (x, arraycount) {
								var counter = 1;
								$.each(arraycount, function (i, item) {
									var orig = (item.originalValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.originalValue;
									var newValue = (item.newValue === '') ? '<i style="color:#aaa"><small>Empty</small></i>' : item.newValue;
									var background = (parseInt(item.familyInstance) % 2 === 0) ? "#fff" : "#eee";
									var styleColor = (item.style === 'update') ? 'm-badge--warning' : (item.style === 'delete') ? 'm-badge--danger' : 'm-badge--success';
									var currentName = (typeof names_family[item.field] === 'undefined') ? item.field : names_family[item.field];
									str += '<tr class="font-12" style="background:' + background + ' !important" data-familyinstance="' + item.familyInstance + '">';
									str += '<td class="text-center">' + moment(obj.requestDate).format('lll') + '</td>';
									str += '<td class="text-center"><span class="m-badge ' + styleColor + ' m-badge--wide m-badge--rounded font-10 m--font-bold">' + item.style + '</span></td>';
									str += '<td class="m--font-bold">' + currentName + '</td>';
									str += '<td>' + orig + '</td>';
									str += '<td>' + newValue + '</td>';
									if (item.style === 'delete' && counter === 1) {
										str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
									} else if (item.style === 'delete' && counter !== 1) {
										str += '<td></td>';
									} else if (item.style === 'insert' && counter === 1) {
										str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve-insertfamily" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
									} else if (item.style === 'insert' && counter !== 1) {
										str += '<td><button class="accordion-approve-insertfamily-temp m--hide" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"></button></td>';
									} else {
										str += '<td class="text-center"><button class="btn btn-info m-btn m-btn--icon btn-sm m-btn--icon-only accordion-approve" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-check"></i></button> <button class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only accordion-reject" data-style="' + item.style + '" data-type="' + item.type + '" data-emprequestid="' + item.employeeRequest_ID + '" data-emprequestdetailsid="' + item.employeeRequestDetails_ID + '"><i class="fa fa-times"></i></button></td>';
									}
									counter++;
									str += '</tr>';
								});
							});
							str += '</tbody></table></div></div>';
						}

						str += '</div></div></div>';
					});
if (str === "") {
	$("#accordion-approval-alert").show();
} else {
	$("#accordion-approval-alert").hide();
}
$("#div-accordion-approval").html(str);
}
});
});

$("#tab-approval").on('click', '.accordion-approve', function () {
	var that = this;
	var employeeRequest_ID = $(that).data('emprequestid');
	var employeeRequestDetails_ID = $(that).data('emprequestdetailsid');
	var style = $(that).data('style');
	var type = $(that).data('type');
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>Employee/approveEmployeeRequest",
		data: {
			employeeRequest_ID: employeeRequest_ID,
			employeeRequestDetails_ID: employeeRequestDetails_ID,
			style: style,
			type: type
		},
		cache: false,
		success: function (res) {
			res = JSON.parse(res.trim());
			if (res.status === "Success") {
				$.notify({
					message: 'Successfully approved employee request.'
				}, {
					type: 'success',
					timer: 1000
				});
				var divstatus = res.remainingrequest;
				if (divstatus === 'empty') {
					$(that).closest('.m-accordion__item').remove();
				} else if (divstatus === 'withoutnormal') {
					$(that).closest('.main-div-normal').remove();
				} else if (divstatus === 'withoutfamily') {
					$(that).closest('.main-div-family').remove();
				}

				$(that).closest('tr').remove();
				if ($("#div-accordion-approval").html() === '') {
					$("#accordion-approval-alert").show();
				} else {
					$("#accordion-approval-alert").hide();
				}
			} else {
				$.notify({
					message: 'An error occured while processing your approval'
				}, {
					type: 'danger',
					timer: 1000
				});
			}
		}

	});
});
$("#tab-approval").on('click', '.accordion-reject', function () {
	var that = this;
	var employeeRequest_ID = $(that).data('emprequestid');
	var employeeRequestDetails_ID = $(that).data('emprequestdetailsid');
	var style = $(that).data('style');
	var type = $(that).data('type');
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>Employee/rejectEmployeeRequest",
		data: {
			employeeRequest_ID: employeeRequest_ID,
			employeeRequestDetails_ID: employeeRequestDetails_ID,
			style: style,
			type: type
		},
		cache: false,
		success: function (res) {
			res = JSON.parse(res.trim());
			if (res.status === "Success") {
				$.notify({
					message: 'Successfully rejected employee request.'
				}, {
					type: 'success',
					timer: 1000
				});
				var divstatus = res.remainingrequest;
				if (divstatus === 'empty') {
					$(that).closest('.m-accordion__item').remove();
				} else if (divstatus === 'withoutnormal') {
					$(that).closest('.main-div-normal').remove();
				} else if (divstatus === 'withoutfamily') {
					$(that).closest('.main-div-family').remove();
				}

				$(that).closest('tr').remove();
				if ($("#div-accordion-approval").html() === '') {
					$("#accordion-approval-alert").show();
				} else {
					$("#accordion-approval-alert").hide();
				}
			} else {
				$.notify({
					message: 'An error occured while processing your approval'
				}, {
					type: 'danger',
					timer: 1000
				});
			}
		}

	});
});
$("#tab-approval").on('click', '.accordion-approve-insertfamily', function () {
	var that = this;
	var employeeRequest_ID = $(that).data('emprequestid');
	var employeeRequestDetails_ID = $(that).data('emprequestdetailsid');
	var tr = $(that).closest('tr');
	var family_instance = tr.data('familyinstance');
	var employeeRequestDetails_IDs = [employeeRequestDetails_ID];
	$.each(tr.siblings(), function (i, item) {
		var fam_instance = $(item).data('familyinstance');
		if (fam_instance === family_instance) {
			employeeRequestDetails_IDs.push($(item).find('.accordion-approve-insertfamily-temp').data('emprequestdetailsid'));
		}
	});
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>Employee/approveEmployeeRequest_familyInsert",
		data: {
			employeeRequestDetails_IDs: employeeRequestDetails_IDs,
			employeeRequest_ID: employeeRequest_ID
		},
		cache: false,
		success: function (res) {
			res = JSON.parse(res.trim());
			if (res.status === "Success") {
				$.notify({
					message: 'Successfully approved employee request.'
				}, {
					type: 'success',
					timer: 1000
				});
				var divstatus = res.remainingrequest;
				if (divstatus === 'empty') {
					$(that).closest('.m-accordion__item').remove();
				} else if (divstatus === 'withoutnormal') {
					$(that).closest('.main-div-normal').remove();
				} else if (divstatus === 'withoutfamily') {
					$(that).closest('.main-div-family').remove();
				}
				tr.remove();
				$.each(tr.siblings(), function (i, item) {
					var fam_instance = $(item).data('familyinstance');
					if (fam_instance === family_instance) {
						$(item).remove();
					}
				});
				if ($("#div-accordion-approval").html() === '') {
					$("#accordion-approval-alert").show();
				} else {
					$("#accordion-approval-alert").hide();
				}
			} else {
				$.notify({
					message: 'An error occured while processing your approval'
				}, {
					type: 'danger',
					timer: 1000
				});
			}
		}

	});
});
$('[href="#tab-userupdate"]').on('click', function (e) {
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>Employee/get_all_active_employees",
		cache: false,
		success: function (res) {
			res = JSON.parse(res.trim());
			$("#dualListBox-userupdate").html("");
			var str = "";
			$.each(res.activeEmployees, function (key, obj) {
				if (obj.allowUserUpdate === '1') {
					str += '<option value="' + obj.emp_id + '" selected>' + obj.lname + ' ' + obj.fname + '</option>';
				} else {
					str += '<option value="' + obj.emp_id + '">' + obj.lname + ' ' + obj.fname + '</option>';
				}

			});
			$("#dualListBox-userupdate").html(str);
			$('#dualListBox-userupdate').bootstrapDualListbox('refresh');
		}
	});
});
$("#btn-saveEnableUserUpdate").click(function () {
	var users = $("#dualListBox-userupdate").val();
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>Employee/setEnableUserUpdate",
		data: {
			users: users
		},
		cache: false,
		success: function (res) {
			res = JSON.parse(res.trim());
			$('#enableUserUpdateModal').modal('hide');
			if (res.status === 'Success') {
				swal("Good job!", "Successfully saved changes!", "success");
			} else {
				swal("Failed!", "Something happened while processing your request!", "error");
			}
		}

	});
});

$('[href="#tab-download"]').click(function () {
	getEmployees();
});
$('[href="#tab-resolution"]').click(function () {
	getEmployeesForResolution();
});
$('#dualListBox-exports').on('change', function (e) {
                                    // var value = $(this).val();
                                    // if (value.length > 20) {
                                    //     $("#myalert").show();
                                    //     $("#myalert span").html(value.length - 20);
                                    //     $("#btn-excel,#btn-pdf").prop('disabled', true);
                                    // } else {
                                    //     $("#myalert").hide();
                                    //     $("#btn-excel,#btn-pdf").prop('disabled', false);
                                    // }
                                    $("#btn-excel").prop('disabled', false);
                                });
$("#search-active").change(function () {
	getEmployees();
});

$("#btn-excel").click(function () {
	var users = $("#dualListBox-exports").val();
	if (users.length === 0) {
		swal("WAIT...", "Select employee first to download file", "warning");
	} else {

		var emp_ids = users.join('-');
		$.ajax({
			url: '<?php echo base_url(); ?>Employee/downloadExcel',
			method: 'POST',
			data: {
				emp_ids: emp_ids
			},
			beforeSend: function () {
				mApp.block('#card-container', {
					overlayColor: '#000000',
					type: 'loader',
					state: 'brand',
					size: 'lg',
					message: 'Downloading...'
				});
			},
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = 'SZ_EMSExport.xlsx';
				a.click();
				window.URL.revokeObjectURL(url);
				mApp.unblock('#card-container');
			}
		});

	}
});
$("#btn-pdf").click(function () {
	var users = $("#dualListBox-exports").val();
	if (users.length === 0) {
		swal("WAIT...", "Select employee first to download file", "warning");
	} else {

		var emp_ids = users.join('-');
		var base64 = btoa(emp_ids);
		window.open("<?php echo base_url() ?>Employee/downloadPDF/" + base64, '_blank');

	}
});

$("#viewNamesModal").on('show.bs.modal', function (e) {
	var emp_ids = $(e.relatedTarget).data('emp_ids');
	var field = $(e.relatedTarget).data('field');
	var name = $(e.relatedTarget).data('name');
	$("#name-field").html(name);
	$.ajax({
		type: "POST",
		url: "<?php echo base_url(); ?>Employee/getMissingList",
		data: {
			emp_ids: emp_ids
		},
		cache: false,
		success: function (res) {
			res = JSON.parse(res.trim());
			$("#table-missing tbody").html("");
			var str = "";
			$.each(res.missing, function (key, obj) {
				var selectInput = '<input type="" />';
				str += ' <tr>'
				+ '<td class="text-capitalize font-13">' + obj.lname + ', ' + obj.fname + '</td>'
//                                                        + '<td>' + selectInput + '</td>'
//                                                        + '<td><button class="btn btn-sm btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-field="'+field+'" data-emp_id="'+obj.emp_id+'"><i class="fa fa-check"></i></button></td>'
+ '</tr>';
});

			$("#table-missing tbody").html(str);
		}

	});

});
$('[href="#tab-approval"]').click();

});

</script>   
