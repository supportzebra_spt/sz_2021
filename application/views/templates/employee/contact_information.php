<style type="text/css">
body {
    padding-right: 0 !important;
}

.form-control {
    font-size: 13px;
}

th {
    font-size: 13px;
}

td {
    font-size: 13px;
}

label {
    font-size: 13px;
}
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h2 class="m-portlet__head-text">
                                    Employee Contact Information
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-12 col-md-2">
                                <label for="" class="m--font-bolder">CLASS:</label>
                                <select class="form-control m-input" id="search-class">
                                    <option value="">All</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-4">
                                <label for="" class="m--font-bolder">ACCOUNTS:</label>
                                <select class="form-control m-input" id="search-accounts">
                                    <option value=''>All</option>
                                    <?php foreach($accounts as $acc):
										echo "<option value='$acc->acc_id' data-class='$acc->acc_description'>$acc->acc_name</option>";
									 endforeach;?>
                                </select>
                            </div>
                            <div class="col-12 col-md-3">
                                <label for="" class="m--font-bolder">EMPLOYEE SEARCH: </label>
                                <div class="input-group">
                                    <input type="text" class="form-control m-input" placeholder="Search Name"
                                        id="search-name">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-3">
                                <br>
                                <button
                                    class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-block"
                                    id="btn-excel">
                                    <span>
                                        <i class="fa flaticon-download"></i>
                                        <span>Download Excel</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show"
                                    role="alert" id="date-alert" style="display:none">
                                    <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                    <div class="m-alert__text"><strong>No Record Found! </strong> <br>Please adjust your
                                        search criteria and try again.
                                    </div>
                                </div>

                                <table class="table m-table m-table--head-bg-brand table-sm" id="table-contacts">
                                    <thead>
                                        <th>ID<br>Number</th>
                                        <th>Employee<br>Name</th>
                                        <th>Account<br>Name</th>
                                        <th>Supervisor<br>I</th>
                                        <th>Supervisor<br>II</th>
                                        <th>Phone<br>Number</th>
                                        <th>Email<br>Address</th>
                                        <th>Home<br>Address</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body px-5">
                                <div class="row">
                                    <div class="col-md-7 col-sm-12 p-2">
                                        <ul id="pagination" style="font-size:12px">
                                        </ul>
                                    </div>
                                    <div class="col-md-2 col-sm-4 p-2">
                                        <select class="form-control m-input form-control-sm m-input--air" id="perpage"
                                            style="font-size:13px">
                                            <option>10</option>
                                            <option>30</option>
                                            <option>60</option>
                                            <option>100</option>
                                            <option>250</option>
                                            <option>500</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-8 p-2 text-right" style="font-size:13px">
                                        Showing <span id="count"></span> of <span id="total"></span> records
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script type="text/javascript">
function getContactsList(limiter, perPage, callback) {
    var searchclass = $("#search-class").val();
    var searchaccounts = $("#search-accounts").val();
    var searchname = $("#search-name").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Employee/getContactsList",
        data: {
            limiter: limiter,
            perPage: perPage,
            searchclass: searchclass,
            searchaccounts: searchaccounts,
            searchname: searchname,
        },
        beforeSend: function() {
            mApp.block('#portlet', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg'
            });
        },
        cache: false,
        success: function(res) {
            res = JSON.parse(res.trim());

            $("#total").html(res.total);
            $("#total").data('count', res.total);

            $("#table-contacts tbody").html("");
            var string = '';
            $.each(res.employees, (index, obj) => {
                string += `<tr>\
                        <td style="width:50px;">${obj.id_num || '-'}</td>\
                        <td style="width:150px;" class="m--font-bold m--font-transform-c">${obj.lname}, ${obj.fname}</td>\
                        <td>${obj.acc_name || '-'}</td>\
                        <td>${obj.directSupervisor || '-'}</td>\
                        <td>${obj.nextSupervisor || '-'}</td>\
                        <td>${obj.cell || '-'}</td>\
                        <td style="max-width:1px;width:150px"><div style="overflow-wrap:break-word;">${obj.email || '-'}</div></td>\
                        <td style="width:250px">${obj.presentAddress.replace(/\|/g, ', ') || '-'}</td>\
                        </tr>`;
            });
            if (string === '') {
                $("#date-alert").show();
                $("#table-contacts").hide();
            } else {
                $("#date-alert").hide();
                $("#table-contacts").show();
            }
            $("#table-contacts tbody").html(string);
            var count = $("#total").data('count');
            var result = parseInt(limiter) + parseInt(perPage);
            if (count === 0) {
                $("#count").html(0 + ' - ' + 0);
            } else if (count <= result) {
                $("#count").html((limiter + 1) + ' - ' + count);
            } else if (limiter === 0) {
                $("#count").html(1 + ' - ' + perPage);
            } else {
                $("#count").html((limiter + 1) + ' - ' + result);
            }
            mApp.unblock('#portlet');
            callback(res.total);
        },
        error: function() {
            swal("Error", "Please contact admin", "error");
            mApp.unblock('#portlet');
        }
    });
}

$(function() {
    $("#perpage").change(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        var perPage = $("#perpage").val();
        getContactsList(0, perPage, function(total) {
            $("#pagination").pagination('destroy');
            $("#pagination").pagination({
                items: total, //default
                itemsOnPage: $("#perpage").val(),
                hrefTextPrefix: "#",
                cssStyle: 'light-theme',
                onPageClick: function(pagenumber) {
                    var perPage = $("#perpage").val();
                    getContactsList((pagenumber * perPage) - perPage, perPage);
                }
            });
        });
    });


    $("#perpage").change();

    $("#search-class,#search-name,#search-accounts").change(function() {
        $("#perpage").change();
    });

    $("#search-class").change(function() {
        var theclass = $(this).val();
        $("#search-accounts").children('option').css('display', '');
        $("#search-accounts").val('').change();
        if (theclass !== '') {
            $("#search-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css(
                'display', 'none');
        }

    });

    $("#btn-excel").click(function() {
        var searchclass = $("#search-class").val();
        var searchaccounts = $("#search-accounts").val();
        var searchname = $("#search-name").val();
        $.ajax({
            url: '<?php echo base_url(); ?>Employee/downloadContactsExcel',
            method: 'POST',
            data: {
                searchclass: searchclass,
                searchaccounts: searchaccounts,
                searchname: searchname,
            },
            beforeSend: function() {
                mApp.block('#portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg',
                    message: 'Downloading...'
                });
            },
            xhrFields: {
                responseType: 'blob'
            },
            success: function(data) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'SZ_EMSContactExport.xlsx';
                a.click();
                window.URL.revokeObjectURL(url);
                mApp.unblock('#portlet');
            },
            error: function(data) {
                $.notify({
                    message: 'An error occured while exporting excel'
                }, {
                    type: 'danger',
                    timer: 1000
                });
                mApp.unblock('#portlet');
            }
        });
    });

});
</script>