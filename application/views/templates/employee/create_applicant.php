<style type="text/css">
    .btn-outline-green {
        color: #4CAF50 !important;
        background: white !important;
        border-color: #4CAF50 !important;
    }

    .btn-outline-green:hover {
        background: #4CAF50 !important;
        color: white !important;
        border-color: #4CAF50 !important;
    }

    .bg-green {
        background: #4CAF50 !important;
    }

    .btn-green {
        background: #4CAF50;
        color: white !important;
    }

    .btn-green:hover {
        background: #2a862e !important;
    }

    .font-green {
        color: #4CAF50 !important;
    }

    .font-white {
        color: white !Important;
    }

    .border-light-green {
        border-color: #4CAF507d !important;
    }

    .border-green {
        border-color: #4CAF50 !important;
    }

    .body {
        padding-right: 0 !important;
    }

    .swal2-title {
        font-family: 'Poppins' !important;
    }

    .font-poppins {
        font-family: 'Poppins' !important;
    }

    .card-shadow {
        box-shadow: 0 1px 8px rgba(0, 0, 0, 0.15);
    }

    .font-16 {
        font-size: 16px !important;
    }

    .font-14 {
        font-size: 14px !important;
    }

    .font-12 {
        font-size: 12px !important;
    }


    .bg-gray {
        /* background: #32323D; */
        background: #2c2e3eeb;
    }

    .form-control-feedback {
        color: #f4516c !important;
        margin-left: 15px;
    }

    #createApplicantForm label {
        font-size: 13px;
        font-weight: 402;
        margin-left: 15px;
        margin-top: 8px;
    }

    #createApplicantForm h5 {
        margin-top: 25px;
        font-size: 16px;
        text-align: center;
    }

    #createApplicantForm h5:first-child {
        margin-top: 5px !important;
        text-align: center;
    }

    #createApplicantForm .form-control {
        margin-bottom: 7px;
    }

    #createApplicantForm section {
        overflow-y: scroll;
        overflow-x: hidden;
        width: 100% !important;
        height: 100% !important;
    }

    #createApplicantForm .content {
        min-height: 30em !important;
        margin: 0;
        margin-left: 2%;
        margin-bottom: 2%;
        width: 67%;
    }

    .wizard.vertical>.actions {
        margin: 0 1% !important;
    }

    .wizard>.actions a,
    .wizard>.actions a:hover,
    .wizard>.actions a:active {
        background: #4caf50 !important;
    }

    .wizard>.actions .disabled a,
    .wizard>.actions .disabled a:hover,
    .wizard>.actions .disabled a:active {
        background: #eee !important;
        color: #aaa !important;
    }

    .wizard>.steps .current a,
    .wizard>.steps .current a:hover,
    .wizard>.steps .current a:active {
        background: #4caf50 !important;
    }

    .wizard>.steps .done a,
    .wizard>.steps .done a:hover,
    .wizard>.steps .done a:active {
        background: #a0e0a2 !important;
        color: white !important;
    }

    .tt-hint,
    .tt-input {
        border-radius: 20px;
    }

    .wizard .m-input {
        border: 1px solid #ccc !important;
        background: white !important
    }

    #modal-position .form-control-feedback {
        font-size: 11px !important;
        margin-left: 0 !important;
    }

    .form-control-feedback {
        font-weight: 500;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="content">
    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Create Applicant:
                    <br /><small>Employee Management System</small>
                </h3>
            </div>
            <div class="col-md-6 text-right" style="margin-top:13px">
                <div class="btn-group m-btn-group flex-wrap " role="group" aria-label="...">
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green"
                        onclick="location.href = '<?php echo base_url(); ?>Employee/applicant'"><span><i
                                class="fa   fa-th-list"></i><span>Applicants List</span></span></button>
                    <!-- <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/manual_tap'"><span><i
                            class="fa   fa-hand-pointer-o"></i><span>Manual TAP</span></span></button> -->
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green"
                        onclick="location.href = '<?php echo base_url(); ?>Employee/file_download'"><span><i
                                class="fa   fa-download"></i><span>File Download</span></span></button>

                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-green"
                        onclick="location.href = '<?php echo base_url(); ?>Employee/create_applicant'"><span><i
                                class="fa fa-user-plus"></i><span>Create Applicant</span></span></button>
                </div>
            </div>
        </div>
        <br>
        <div class="card card-shadow">
            <div class="card-header font-16 m--font-bolder font-white bg-gray">CREATE APPLICANT</div>
            <div class="card-body" style="padding: 20px;">
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                    id="createApplicantForm" style="display:none">
                    <!-- <div> -->
                    <h3>Basic Information</h3>
                    <section>
                        <div class="px-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Last Name:</label>
                                    <input type="text" name="lname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter last name">
                                </div>
                                <div class="col-md-6">
                                    <label>First Name:</label>
                                    <input type="text" name="fname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter first name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Middle Name:</label>
                                    <input type="text" name="mname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter middle name">
                                </div>
                                <div class="col-md-6">
                                    <label>Name Extension:</label>
                                    <select name="nameExt" class="form-control m-input m-input--air m-input--pill">
                                        <option value="">--</option>
                                        <option value="jr">jr</option>
                                        <option value="sr">Sr</option>
                                        <option value="i">I</option>
                                        <option value="ii">II</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Birthdate:</label>
                                    <input type="text" name="birthday"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter birthdate" readonly>
                                </div>
                                <div class="col-md-6">
                                    <label>Religion:</label>
                                    <select name="religion" class="form-control m-input m-input--air m-input--pill">
                                        <option value="">--</option>
                                        <option value="roman catholic">Roman Catholic</option>
                                        <option value="muslim">Muslim</option>
                                        <option value="protestant">Protestant</option>
                                        <option value="iglesia ni cristo">Iglesia Ni Cristo</option>
                                        <option value="el shaddai">El Shaddai</option>
                                        <option value="jehovahs witnesses">Jehovahs Witnesses</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Gender:</label>
                                    <select name="gender" class="form-control m-input m-input--air m-input--pill">
                                        <option value="">--</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Civil Status:</label>
                                    <select name="civilStatus" class="form-control m-input m-input--air m-input--pill">
                                        <option value="">--</option>
                                        <option value="single">Single</option>
                                        <option value="married">Married</option>
                                        <option value="legally separated">Legally Separated</option>
                                        <option value="widower">Widower</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>Address and Contact Info</h3>
                    <section>

                        <div class="px-3">
                            <label>Present Address:</label>
                            <input type="text" name="presentAddress"
                                class="form-control m-input m-input--air m-input--pill"
                                placeholder="Enter present address">

                            <label>Permanent Address:</label>
                            <input type="text" name="permanentAddress"
                                class="form-control m-input m-input--air m-input--pill"
                                placeholder="Enter permanent address">

                            <div class="row">
                                <div class="col-md-6">
                                    <label>Mobile Number:</label>
                                    <input type="text" name="cell"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter mobile number">
                                </div>
                                <div class="col-md-6">
                                    <label>Telephone Number:</label>
                                    <input type="text" name="tel"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter telephone number">
                                </div>
                            </div>
                        </div>
                    </section>
                    <h3>Application Details</h3>
                    <section>
                        <div class="px-3">
                            <label>Position Applied:</label>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="m-typeahead has-warning">
                                        <input class="form-control m-input" id="new_position" name="pos_name"
                                            type="text" placeholder="Search for a position">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn" id="new_position_btn" data-toggle="modal"
                                        data-target="#modal-position" style="background: #4caf50;color:white"
                                        disabled>Create
                                        Position</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-8">
                                    <label>Source:</label>
                                    <input type="text" name="source"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter source">

                                </div>
                                <div class="col-md-4">
                                    <label>CC Experience:</label>
                                    <select name="ccaExp" class="form-control m-input m-input--air m-input--pill">
                                        <option value="">--</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>

                    <h3>Previous Employment</h3>
                    <section>
                        <div class="px-3">
                            <label>Latest Employer:</label>
                            <input type="text" name="latestEmployer"
                                class="form-control m-input m-input--air m-input--pill"
                                placeholder="Enter latest employer">

                            <label>Last Position Held:</label>
                            <input type="text" name="lastPositionHeld"
                                class="form-control m-input m-input--air m-input--pill"
                                placeholder="Enter last position held">

                            <div class="row">
                                <div class="col-md-7">
                                    <label>Inclusive Dates:</label>
                                    <input type="text" name="inclusiveDates"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter inclusive dates" readonly>
                                </div>
                                <div class="col-md-5">
                                    <label>Contact Number:</label>
                                    <input type="text" name="lastEmployerContact"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter contact number">
                                </div>
                            </div>

                            <label>Address:</label>
                            <input type="text" name="lastEmployerAddress"
                                class="form-control m-input m-input--air m-input--pill" placeholder="Enter address">
                        </div>
                    </section>
                    <h3>Character Reference</h3>
                    <section>
                        <h5>Character Reference 1:</h5>
                        <div class="px-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Last Name:</label>
                                    <input type="text" name="cr1-lastname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter last name">
                                </div>
                                <div class="col-md-6">
                                    <label>First Name:</label>
                                    <input type="text" name="cr1-firstname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter first name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Middle Name:</label>
                                    <input type="text" name="cr1-middlename"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter middle name">
                                </div>
                                <div class="col-md-6">
                                    <label>Contact Number:</label>
                                    <input type="text" name="cr1-contactnumber"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter contact number">
                                </div>
                            </div>
                            <label>Occupation:</label>
                            <input type="text" name="cr1-occupation"
                                class="form-control m-input m-input--air m-input--pill" placeholder="Enter occupation">
                        </div>
                        <h5>Character Reference 2:</h5>
                        <div class="px-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Last Name:</label>
                                    <input type="text" name="cr2-lastname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter last name">
                                </div>
                                <div class="col-md-6">
                                    <label>First Name:</label>
                                    <input type="text" name="cr2-firstname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter first name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Middle Name:</label>
                                    <input type="text" name="cr2-middlename"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter middle name">
                                </div>
                                <div class="col-md-6">
                                    <label>Contact Number:</label>
                                    <input type="text" name="cr2-contactnumber"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter contact number">
                                </div>
                            </div>
                            <label>Occupation:</label>
                            <input type="text" name="cr2-occupation"
                                class="form-control m-input m-input--air m-input--pill" placeholder="Enter occupation">
                        </div>
                        <h5>Character Reference 3:</h5>
                        <div class="px-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Last Name:</label>
                                    <input type="text" name="cr3-lastname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter last name">
                                </div>
                                <div class="col-md-6">
                                    <label>First Name:</label>
                                    <input type="text" name="cr3-firstname"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter first name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Middle Name:</label>
                                    <input type="text" name="cr3-middlename"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter middle name">
                                </div>
                                <div class="col-md-6">
                                    <label>Contact Number:</label>
                                    <input type="text" name="cr3-contactnumber"
                                        class="form-control m-input m-input--air m-input--pill"
                                        placeholder="Enter contact number">
                                </div>
                            </div>
                            <label>Occupation:</label>
                            <input type="text" name="cr3-occupation"
                                class="form-control m-input m-input--air m-input--pill" placeholder="Enter occupation">
                        </div>
                    </section>
                    <!-- </div> -->
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-position" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background:#2c2e3eeb">
                <h5 class="modal-title" id="exampleModalLongTitle">CREATE POSITION FORM</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                    id="createPositionForm">
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="">Code:</label>
                            <input type="text" class="form-control m-input" id="input-code" name="code" />
                        </div>
                        <div class="col-md-8">
                            <label for="">Position Name:</label>
                            <input type="text" class="form-control m-input" id="input-positionname"
                                name="positionName" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="">Is Hiring:</label>
                            <select class="form-control m-input" id="input-ishiring" name="isHiring">
                                <option value="">--</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label for="">Department:</label>
                            <select class="form-control m-input" id="input-department" name="department">
                                <option value="">--</option>
                                <?php foreach ($departments as $dep):?>
                                <option
                                    value="<?php echo $dep->dep_id?>">
                                    <?php echo $dep->dep_details?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label for="">Class:</label>
                            <select class="form-control m-input" id="input-class" name="class">
                                <option value="">--</option>
                                <option value="Agent">Ambassador</option>
                                <option value="Admin">SZ Team</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="">Site:</label>
                            <select class="form-control m-input" id="input-site" name="site">
                                <option value="">--</option>
                                <?php foreach ($sites as $site):?>
                                <option
                                    value="<?php echo $site->site_ID?>">
                                    <?php echo $site->code?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn" style="background: #4caf50;color:white" id="btn-insert">Create
                    Position</button>
            </div>
        </div>
    </div>
</div>
<link
    href="<?php echo base_url(); ?>assets/src/custom/css/jquery.steps.css"
    rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.steps.min.js">
</script>
<script>
    $.fn.steps.setStep = function(step) {
        var currentIndex = $(this).steps('getCurrentIndex');
        for (var i = 0; i < Math.abs(step - currentIndex); i++) {
            if (step > currentIndex) {
                $(this).steps('next');
            } else {
                $(this).steps('previous');
            }
        }
    };
    $(function() {

        var createApplicantForm = $("#createApplicantForm");
        var createPositionForm = $("#createPositionForm");

        createApplicantForm.steps({
            headerTag: "h3",
            bodyTag: "section",
            stepsOrientation: "vertical",
            transitionEffect: "slideLeft",
            onStepChanging: function(event, currentIndex, newIndex) {
                if (currentIndex > newIndex) {
                    return true;
                }
                createApplicantForm.validate().settings.ignore = ":disabled,:hidden";
                return createApplicantForm.valid();
            },
            onFinishing: function(event, currentIndex) {
                createApplicantForm.validate().settings.ignore = ":disabled";
                return createApplicantForm.valid();
            },
            onFinished: function(event, currentIndex) {
                var data = {};
                createApplicantForm.find('.form-control').each(function(i, elem) {
                    let name = $(elem).attr('name');
                    if (typeof name !== 'undefined' && name !== false && name !==
                        'pos_name') {
                        data['\"' + name + '\"'] = $(elem).val();
                    }
                });
                data["pos_id"] = $("#new_position").data('pos_id');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Employee/save_create_applicant",
                    data: data,
                    cache: false,
                    success: function(res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $.notify({
                                message: 'Successfully created applicant'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                        } else {
                            $.notify({
                                message: 'An error occured while processing your actions'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }
                        createApplicantForm.resetForm();
                        createApplicantForm.steps("setStep", 0);
                        createApplicantForm.find('.done').addClass('disabled')
                            .removeClass('done');
                    }
                });
            }
        });
        var positionValidator = createPositionForm.validate({
            rules: {
                code: "required",
                positionName: "required",
                isHiring: "required",
                department: "required",
                class: "required",
                site: "required",
            }
        });
        var validator = createApplicantForm.validate({
            rules: {
                lname: "required",
                fname: "required",
                mname: "required",
                birthday: "required",
                gender: "required",
                civilStatus: "required",
                presentAddress: "required",
                permanentAddress: "required",
                cell: {
                    number: true,
                    required: true,
                    maxlength: 15
                },
                tel: {
                    number: true,
                    maxlength: 15
                },
                pos_name: {
                    required: true,
                    positionExist: true
                },
                source: "required",
                ccaExp: "required",
                lastEmployerContact: {
                    number: true,
                    maxlength: 15
                },
                'cr1-lastname': "required",
                'cr1-firstname': "required",
                'cr1-middlename': "required",
                'cr1-contactnumber': {
                    required: true,
                    number: true,
                    maxlength: 15
                },
                'cr1-occupation': "required"
            }
        });

        $.validator.addMethod('positionExist', function(value, element) {
            return ($("#new_position").data('pos_id') != null);
        }, 'Position does not exist');

        $("[name='birthday']").datepicker({
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            },
            format: 'yyyy-mm-dd'
        });
        $("[name='inclusiveDates']").daterangepicker({
            opens: "left",
            drops: "down",
            autoApply: false,
            clearBtn: true
        })
        // , function(start, end, label) {
        //     $("[name='inclusiveDates']").val(start.format('YYYY/MM/DD') + ' - ' + end.format('YYYY/MM/DD'));
        // });
        createApplicantForm.resetForm();
        createApplicantForm.show();


        $('#new_position').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            limit: 20,
            async: true,
            source: function(query, processSync, processAsync) {
                return $.ajax({
                    url: "<?php echo base_url()?>/Employee/getFilteredPositions_v2",
                    type: 'GET',
                    data: {
                        searchText: query
                    },
                    dataType: 'json',
                    success: function(json) {
                        // in this example, json is simply an array of strings
                        return processAsync(json.map(d => d.pos_details));
                    }
                });
            },
        });

        $('#new_position').bind('typeahead:change', function() {
            console.log('CHECKER');
            $("#new_position").data('pos_id', null);
            $.ajax({
                url: "<?php echo base_url()?>/Employee/checkExactPosition",
                type: 'GET',
                data: {
                    searchText: $('#new_position').typeahead('val')
                },
                dataType: 'json',
                success: function(json) {
                    if (json.status == 'Exist') {
                        $("#new_position").data('pos_id', json.pos_id);
                        $("#new_position_btn").prop('disabled', true);
                    } else {
                        $("#new_position").data('pos_id', null);
                        $("#new_position_btn").prop('disabled', false);
                    }
                    validator.element("#new_position");

                }
            });
        });

        $("#modal-position").on('show.bs.modal', () => {
            let position_name = $('#new_position').typeahead('val');
            $("#input-positionname").val(position_name);
        })
        $("#modal-position").on('hide.bs.modal', () => {
            createPositionForm.resetForm();
        })
        $("#btn-insert").click(function() {
            if (positionValidator.form()) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Payroll_settings/savePosition",
                    data: {
                        code: $("#input-code").val(),
                        positionname: $("#input-positionname").val(),
                        ishiring: $("#input-ishiring").val(),
                        department: $("#input-department").val(),
                        class: $("#input-class").val(),
                        site_ID: $("#input-site").val()
                    },
                    dataType: 'json',
                    success: function(json) {
                        if (json.status == 'Success') {
                            $("#new_position").data('pos_id', json.pos_id);
                            $("#new_position_btn").prop('disabled', true);
                            $.notify({
                                message: 'Successfully Created Position'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                        } else {
                            $("#new_position").data('pos_id', null);
                            $("#new_position_btn").prop('disabled', false);
                            $.notify({
                                message: 'An error occured while creating the position'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }
                        $("#modal-position").modal('hide')
                        validator.element("#new_position");

                    }
                });
            }
        })
    })
</script>