<style type="text/css">
    .btn-outline-green {
        color: #4CAF50 !important;
        background: white !important;
        border-color: #4CAF50 !important;
    }

    .btn-outline-green:hover {
        background: #4CAF50 !important;
        color: white !important;
        border-color: #4CAF50 !important;
    }

    .bg-green {
        background: #4CAF50 !important;
    }

    .btn-green {
        background: #4CAF50;
        color: white !important;
    }

    .btn-green:hover {
        background: #2a862e !important;
    }

    .font-green {
        color: #4CAF50 !important;
    }

    .font-white {
        color: white !Important;
    }

    .border-light-green {
        border-color: #4CAF507d !important;
    }

    .border-green {
        border-color: #4CAF50 !important;
    }

    .body {
        padding-right: 0 !important;
    }

    .swal2-title {
        font-family: 'Poppins' !important;
    }

    .font-poppins {
        font-family: 'Poppins' !important;
    }

    .m-widget4 .m-widget4__item {
        padding: 0 !important;
    }

    td.disabled {
        background: #eee !Important;
    }

    .two-line-ellipsis {
        display: inline-block;
        border: 0px;
        height: 18px;
        overflow: hidden;
        color: #777;
    }

    .one-line-ellipsis {
        line-height: 8px;
        height: 16px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        /*width: 80%;*/
        text-align: center !important;
    }

    .scaler:hover {
        box-shadow: 2px 2px 7px #777;
        z-index: 2;
        -webkit-transition: all 100ms ease-in;
        -webkit-transform: scale(1.01);
        -ms-transition: all 100ms ease-in;
        -ms-transform: scale(1.01);
        -moz-transition: all 100ms ease-in;
        -moz-transform: scale(1.01);
        transition: all 100ms ease-in;
        transform: scale(1.01);
        cursor: pointer;
    }

    .m-content .m-stack__item {
        padding: 0 !important;
        background: 0 !important;
        border: 0 !important;
    }

    .card-shadow {
        box-shadow: 0 1px 8px rgba(0, 0, 0, 0.15);
    }

    .text-line-height-1 {
        line-height: 1;
    }

    .font-10 {
        font-size: 10px !Important;
    }

    .font-11 {
        font-size: 11px !Important;
    }

    .font-12 {
        font-size: 12px !Important;
    }

    .font-13 {
        font-size: 13px !Important;
    }

    .font-14 {
        font-size: 14px !Important;
    }

    .font-15 {
        font-size: 15px !Important;
    }

    .font-16 {
        font-size: 16px !Important;
    }

    #profiles-directory p {
        line-height: 1.3 !Important;
    }

    .fa {
        text-align: center !important;
    }

    .m-quick-sidebar .m-quick-sidebar__close:hover {
        color: #4CAF50 !important;
    }

    .nav.nav-pills .nav-link {
        padding: 6px 5px !important;
        color: #aaa;
        border-bottom: 1px solid #444;
        border-radius: 0;
    }

    .nav-pills .nav-link.active {
        background-color: #4CAF50 !important;
    }

    .row-value>div:first-child:not(.static) {
        font-size: 11px;
        font-weight: bolder;
    }

    .row-value>div:nth-child(2):not(.static) {
        font-size: 11px;
        color: #1D98F6;
        font-weight: 600;
        text-decoration: underline dotted #1D98F6;
    }

    .row-value>.static {
        font-size: 11px;
        color: #333;
        font-weight: 600;
        text-decoration: none !important;
    }

    .the-legend {
        border-style: none;
        border-width: 0;
        font-size: 12px;
        line-height: 20px;
        margin-bottom: 0;
        width: auto;
        padding: 0 15px;
        border: 1px solid #4CAF507d;
        background: white;
        color: #4CAF50 !important;
        font-weight: 600;
    }

    .the-fieldset {
        margin-bottom: 15px;
        border: 1px solid #4CAF507d;
        padding: 10px 15px;
    }

    .form-control {
        font-size: 12px;
    }

    .popover {
        width: 100% !important;
    }

    .popover select option {
        text-transform: capitalize;
    }

    a[id*='tbl_applicant'] {
        text-transform: capitalize;
    }

    .notcapital {
        text-transform: none !Important;
    }


    label.cabinet {
        display: block;
        cursor: pointer;
    }

    label.cabinet input.file {
        position: relative;
        height: 100%;
        width: auto;
        opacity: 0;
        -moz-opacity: 0;
        filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
        margin-top: -30px;
    }

    #upload-demo {
        width: 100%;
        height: 100%;
    }

    li span.current {
        background: #333 !important;
    }

    figure figcaption {
        position: absolute;
        bottom: 0px;
        color: #fff;
        width: 100%;
        text-shadow: 0 0 10px #000;
        /*background: rgb(0,0,0,0.5);*/
        background: #333;
        margin: 5px 0px 0px -10px;
        text-align: center;
        padding: 5px 0;
    }

    @media (max-width:1220px) and (min-width:932px) {
        figure img {
            width: 80% !important;
            height: 80% !important;
        }
    }

    .daterangepicker td,
    .daterangepicker th {
        width: 30px !important;
        height: 30px !important;
        font-size: 13px !important;
    }

    #table-dtr tbody tr td {
        vertical-align: middle !important;
        text-align: center !Important;
        font-size: 12px;
    }

    .m-quick-sidebar .mCSB_scrollTools {
        right: 0 !Important;
    }

    .select2-selection__choice {
        font-size: 12px !important;
        font-weight: 400 !Important;
    }

    .select2-container,
    .m-select2,
    .select2-search__field,
    .select2-search {
        width: 100% !important;
    }

    .select2-results__option {
        font-size: 12px !important;
    }

    .m-popover.m-popover--skin-dark.popover {
        background: #333 !important;
    }

    .form-control[readonly] {
        background-color: #e9ecef !important;
    }

    .d-xl-inline {
        margin-right: 3px;
    }

    #m_quick_sidebar table td:first-child {
        font-size: 12px;
        width: 30%;
        font-weight: 400;
    }

    #m_quick_sidebar table td:nth-child(2) {
        font-size: 13px;
        font-weight: 402;
    }

    #div-characterReference label {
        font-size: 12px;
        font-weight: 400;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="content">

    <div class="m-content">
        <div id="main-directory">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="m-subheader__title m-subheader__title--separator font-poppins">APPLICANTS LIST:
                        <br /><small>Employee Management System</small></h3>
                </div>
                <div class="col-md-6 text-right" style="margin-top:13px">
                    <div class="btn-group m-btn-group flex-wrap " role="group" aria-label="...">
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-green" onclick="location.href = '<?php echo base_url(); ?>Employee/applicant'"><span><i class="fa 	fa-th-list"></i><span>Applicants List</span></span></button>
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/file_download'"><span><i class="fa 	fa-download"></i><span>File Download</span></span></button>
                        <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/create_applicant'"><span><i class="fa fa-user-plus"></i><span>Create Applicant</span></span></button>
                    </div>
                </div>
            </div>

            <br />
            <div class="card card-shadow mb-3">
                <div class="card-body p-0">
                    <div class="m-input-icon m-input-icon--right">
                        <input type="text" class="form-control m-input font-12" placeholder="Search Name..." id="search-name" />
                        <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                    </div>
                </div>
            </div>
            <div class="card card-shadow">
                <div class="card-footer" style="padding: 15px 20px 0px 20px;background:white">
                    <div class="row" id="card-container"></div>
                </div>
            </div>
            <br />
            <div class="card card-shadow px-2 ">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-7 col-sm-12 p-2">
                            <ul id="pagination">
                            </ul>
                        </div>
                        <div class="col-md-2 col-sm-4 p-2">
                            <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                                <option>12</option>
                                <option>30</option>
                                <option>60</option>
                                <option>100</option>
                                <option>250</option>
                                <option>500</option>
                                <option>1000</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-8 p-2 text-right">
                            Showing <span id="count"></span> of <span id="total"></span> records
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="single-profile" style="display:none">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="card card-shadow ">
                        <div class="card-body p-0" style="background:#333;">
                            <div class="row no-gutters">
                                <div class="col-12 col-sm-6  col-md-12">
                                    <div class="p-2">
                                        <img src="" class="gambar img-responsiv " id="item-img-output" style="background:white;width:100% !important;object-fit:cover;" />

                                    </div>
                                </div>

                            </div>
                            <div class="col-12 col-sm-6 col-md-12 p-2">
                                <ul class="nav nav-pills m-0" role="tablist">
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link  active show" data-toggle="tab" href="#tab-personal"><span class="la la-user d-lg-none d-xl-inline"></span> Personal <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-application"><span class="la la-clipboard d-lg-none d-xl-inline"></span> Application <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                    <li class="nav-item ml-0" style="width:100%">
                                        <a class="nav-link" data-toggle="tab" href="#tab-evaluation"><span class="la la-list-alt d-lg-none d-xl-inline"></span> Evaluation <span class="pull-right fa fa-chevron-right font-10 mt-1 d-lg-none d-xl-inline"></i></span></a>
                                    </li>
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-lg-9  col-md-8">
                    <div class="row">

                        <div class="col-lg-9 col-md-8">
                            <p style="font-size:28px;color: #2a862e;"><span id="image-caption-fname" class="m--font-boldest text-capitalize"></span> <span id="image-caption-lname" class="m--font-boldest text-capitalize"></span> <span id="image-caption-active"></span></p>
                        </div>
                        <div class="col-lg-3 col-md-4 text-md-right">
                            <button class="btn btn-outline-metal m-btn m-btn--icon" id="btn-backtodirectory" style="color:#333">
                                <span>
                                    <i class="fa fa-chevron-left"></i>
                                    <span>Employee List</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="tab-personal" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    BASIC INFORMATION</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">FULL NAME</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Last Name:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-lname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-lg-2 py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">First Name:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-fname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Middle Name:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-mname" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Extension:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-nameExt" data-type='select' data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">BIRTHDAY</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Birthdate:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-birthday" data-type='date' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Age:</div>
                                                    <div class="col-8  static" id="personal-age"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">OTHER BASIC INFO</legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Religion:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-religion" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Gender:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-gender" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">

                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Civil Status:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-civilStatus" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <br />
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    ADDRESS AND CONTACT INFORMATION</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">ADDRESSES</legend>

                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Present:
                                                    </div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-presentAddress" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4 col-sm-2 col-md-4 col-lg-4 col-xl-2">Permanent:
                                                    </div>
                                                    <div class="col-8 col-sm-10 col-md-8 col-lg-8 col-xl-10"><a href="#" id="tbl_applicant-permanentAddress" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">CONTACT INFORMATION
                                        </legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row  row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Telephone:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-tel" data-type="number" data-required="no"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Mobile:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-cell" data-type="number" data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-application" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    APPLICATION DETAILS</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">EMPLOYMENT HISTORY
                                        </legend>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">CC Experience:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-ccaExp" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Source:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-source" data-type='textarea' data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Latest Employer:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-latestEmployer" data-type='textarea' data-required="no"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Last Position Held:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-lastPositionHeld" data-required="no"></a></div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Inclusive Dates:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-inclusiveDates" data-type='daterange' data-required="no"></a></div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Contact Number:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-lastEmployerContact" data-type="number" data-required="no"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Address:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-lastEmployerAddress" data-type='textarea' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row ">
                                            <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-6">
                                                <div class="row row-value py-2 py-md-2 py-lg-2">
                                                    <div class="col-4">Position Applied:</div>
                                                    <div class="col-8"><a href="#" id="tbl_applicant-pos_id" data-type='select' data-required="yes"></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <br />
                            <div class="card card-shadow" id="div-characterReference">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    CHARACTER REFERENCES
                                </div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">CHARACTER REFERENCES 1:
                                        </legend>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Last Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr1-lname" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">First Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr1-fname" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Middle Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr1-mname" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Mobile No:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr1-mobilenumber" />
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Occupation:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr1-occupation" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="" style="color:white;">Submit:</label>
                                                <button class="btn btn-brand m-btn m-btn--icon btn-sm" id="btn-cr1" data-char="cr1">
                                                    <span>
                                                        <i class="fa fa-check"></i>
                                                        <span>Save</span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>


                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">CHARACTER REFERENCES 2:
                                        </legend>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Last Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr2-lname" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">First Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr2-fname" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Middle Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr2-mname" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Mobile No:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr2-mobilenumber" />
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Occupation:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr2-occupation" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="" style="color:white;">Submit:</label>
                                                <button class="btn btn-brand m-btn m-btn--icon btn-sm" id="btn-cr2" data-char="cr2">
                                                    <span>
                                                        <i class="fa fa-check"></i>
                                                        <span>Save</span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>


                                    </fieldset>
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">CHARACTER REFERENCES 3:
                                        </legend>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Last Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr3-lname" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">First Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr3-fname" />
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Middle Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr3-mname" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Mobile No:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr3-mobilenumber" />
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Occupation:</label>
                                                <input type="text" class="form-control form-control-sm m-input" id="cr3-occupation" />
                                            </div>
                                            <div class="col-md-2">
                                                <label for="" style="color:white;">Submit:</label>
                                                <button class="btn btn-brand m-btn m-btn--icon btn-sm" id="btn-cr3" data-char="cr3">
                                                    <span>
                                                        <i class="fa fa-check"></i>
                                                        <span>Save</span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>


                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-evaluation" role="tabpanel">
                            <div class="card card-shadow">
                                <div class="card-header m--font-boldest px-3 py-2" style="color:white;background:#333">
                                    EVALUATION</div>
                                <div class="card-footer p-3">
                                    <fieldset class="the-fieldset bg-white for_user_access_basic">
                                        <legend class="the-legend  m--font-bold m--font-brand">EVALUATE: PASS OR FAIL
                                        </legend>
                                        <div class="row px-5 py-2">
                                            <div class="col-md-6">
                                                <button class="btn btn-outline-info m-btn m-btn--icon btn-block" data-toggle="modal" data-target="#passModal">
                                                    <span>
                                                        <i class="fa fa-thumbs-o-up"></i>
                                                        <span>Pass</span>
                                                    </span>
                                                </button>
                                            </div>
                                            <div class="col-md-6">
                                                <button class="btn btn-outline-danger m-btn m-btn--icon btn-block btn-applicantFail">
                                                    <span>
                                                        <i class="fa fa-thumbs-o-down"></i>
                                                        <span>Fail</span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">Pass Employee: Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="" class="m--font-bold">Account:</label>
                        <select name="" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="pass-account"></select>
                        <br />
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="" class="m--font-bold">Position:</label>
                        <select name="" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="pass-position"></select>
                        <br />
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="" class="m--font-bold">Supervisor:</label>
                        <select name="" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="pass-supervisor"></select>
                        <br />
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="" class="m--font-bold">Status:</label>
                        <select name="" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" id="pass-status"></select>
                        <br />
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="" class="m--font-bold">Start Date:</label>
                        <input name="" class="form-control m-input" id="pass-startdate" readonly />
                        <br />
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" data-dismiss="modal" id="btn-applicantPass">Save and
                    Confirm</button>
            </div>
        </div>
    </div>
</div>
<!-- begin::Quick Sidebar -->
<button id="m_quick_sidebar_toggle" class="m--hide"></button>
<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--skin-light p-0" style="overflow:hidden">
    <div class="m-quick-sidebar__content">
        <span id="m_quick_sidebar_close" class="m-quick-sidebar__close" style="z-index:2 !important;color:#666;margin: 15px;"><i class="fa fa-times" style="font-size:40px;"></i></span>
        <div id="profiles-directory">
            <div class="row">
                <div class="col-md-12">
                    <div style="background:#333;border-bottom:8px solid #4CAF50;padding:20px">
                        <div class="m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width:35%">
                                <img id="profile-single-image" data-src="" style="border-radius:50%;height:6rem;width:6rem !important;object-fit:cover;border:3px solid white;background:white" alt="NO IMAGE">
                            </div>
                            <div class="m-stack__item m-stack__item--left m-stack__item--middle p-0" style="width:65%">
                                <p id="profile-single-name" class="font-white m--font-bolder mb-0 text-capitalize" style="font-size:24px"></p>
                            </div>
                        </div>
                    </div>

                    <div class="px-5 py-2" class="m-scrollable" data-scrollable="true" data-max-height="540" data-scrollbar-shown="true">

                        <div class="row my-2">
                            <div class="col-md-6">
                                <button class="btn btn-outline-info m-btn m-btn--icon btn-block" data-toggle="modal" data-target="#passModal">
                                    <span>
                                        <i class="fa fa-thumbs-o-up"></i>
                                        <span>Pass</span>
                                    </span>
                                </button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-outline-danger m-btn m-btn--icon btn-block btn-applicantFail">
                                    <span>
                                        <i class="fa fa-thumbs-o-down"></i>
                                        <span>Fail</span>
                                    </span>
                                </button>
                            </div>
                        </div>

                        <hr class="my-3 border-light-green" />
                        <div class="my-2 text-center">
                            <span class="font-12 m--font-bold">APPLIED POSITION:</span>
                            <p class=" m--font-boldest font-15"> <span id="profile-single-position"></span> </p>
                        </div>
                        <table class="table table-bordered table-sm">
                            <tr>
                                <td>BIRTHDAY</td>
                                <td><span id="profile-single-birthday"></span></td>
                            </tr>
                            <tr>
                                <td>GENDER</td>
                                <td class="text-capitalize"><span id="profile-single-gender"></span></td>
                            </tr>
                            <tr>
                                <td>CIVIL STATUS</td>
                                <td class="text-capitalize"><span id="profile-single-civilStatus"></span></td>
                            </tr>
                            <tr>
                                <td>RELIGION</td>
                                <td class="text-capitalize"><span id="profile-single-religion"></span></td>
                            </tr>
                            <tr>
                                <td>MOBILE NO</td>
                                <td class="text-capitalize"><span id="profile-single-cell"></span></td>
                            </tr>
                            <tr>
                                <td>TEL NO</td>
                                <td class="text-capitalize"><span id="profile-single-tel"></span></td>
                            </tr>
                            <tr>
                                <td>PRESENT ADDRESS</td>
                                <td class="text-capitalize"><span id="profile-single-presentAddress" class="font-12"></span></td>
                            </tr>
                            <tr>
                                <td>PERMANENT ADDRESS</td>
                                <td class="text-capitalize"><span id="profile-single-permanentAddress" class="font-12"></span></td>
                            </tr>
                        </table>
                        <hr class=" border-light-green" />

                        <a href="#" class="m-link m-link--state font-green m--font-bolder text-center ml-2" id="btn-full-quicksidebar"><i class="fa fa-arrow-circle-right" style="font-size:20px"></i>
                            Update <span id="profile-single-firstname" class="text-capitalize"></span> Profile</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-fail-applicant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">Fail Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h4 class="text-center"><i class="fa fa-exclamation-triangle m--font-danger" style="font-size:70px"></i></h4>
                <h3 class="text-center mb-3">Are you sure ?</h3>
                <p class="text-center mb-3">The applicant will be failed.</p>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <label for="" class="m--font-bold">Please enter Reason for Failing: <small>(optional)</small></label>
                        <input name="" class="form-control m-input" id="input-failReason" />
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="btn-modal-fail-proceed">Yes, fail this applicant</button>
            </div>
        </div>
    </div>
</div>
<!-- end::Quick Sidebar -->
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/lazyload.min.js"></script>
<script type="text/javascript">
    function tz_date(thedate, format = null) {
        var tz = moment.tz(new Date(thedate), "Asia/Manila");
        return (format === null) ? moment(tz) : moment(tz).format(format);
    }

    function get_all_active_employees(callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/get_all_active_employees",
            cache: false,
            success: function(result) {
                result = JSON.parse(result.trim());
                callback(result);
            }
        });
    };

    function get_all_emp_stat(callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/get_all_emp_stat",
            cache: false,
            success: function(result) {
                result = JSON.parse(result.trim());
                callback(result);
            }
        });
    }

    function get_all_accounts(callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/get_all_accounts",
            cache: false,
            success: function(result) {
                result = JSON.parse(result.trim());
                callback(result);
            }
        });
    }

    function get_all_hiring_positions(callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/get_all_hiring_positions",
            cache: false,
            success: function(result) {
                result = JSON.parse(result.trim());
                callback(result);
            }
        });
    }

    function heightChanger() {
        var height = 0;
        var $cards = $("#card-container").find(".inner-card");
        $($cards).each(function() {
            var tempheight = $(this).outerHeight(true);
            if (height < tempheight) {
                height = tempheight;
            }
        });
        $($cards).each(function() {
            $(this).outerHeight(height);
        });
    }

    function getEmployeeDirectory(limiter, perPage, callback) {
        var searchname = $("#search-name").val();
        $.each($("#single-profile").data(), function(i) {
            $("#single-profile").removeAttr("data-" + i);
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/getEmployeeApplicants",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchname: searchname
            },
            beforeSend: function() {
                mApp.block('#content', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#total").html(res.total);
                $("#total").data('count', res.total);
                $("#card-container").html("");
                var string = "";
                $.each(res.employees, function(key, emp) {
                    if (emp.pic === null) {
                        var pic = null;
                        var img = null;
                    } else {
                        var pic = emp.pic.split(".");
                        var img = pic[0] + "_thumbnail.jpg?" + new Date().getTime();
                    }
                    string += '<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-3 px-2">';
                    string +=
                        '<div class="card scaler" style="background:#4CAF50;border:1px solid #549857" data-apid="' +
                        emp.apid + '" data-isactive="' + emp.isActive + '">';
                    string += '<div class="card-body" style="padding: 5px 10px;">';
                    string += '<div class="m-widget4">';
                    string += '<div class="m-widget4__item inner-card">';
                    string += '<div class="m-widget4__img m-widget4__img--pic">';
                    string += '<img class="employee-pic" data-src="<?php echo base_url(); ?>' + img +
                        '" style="background:white;height:3.8rem;width:3.8rem !important;object-fit:cover;border:2px solid #d9e8ea85" alt="NO IMAGE">';
                    string += '</div>';
                    string += '<div class="m-widget4__info text-capitalize" style="line-height:1.2">';
                    string +=
                        '<span class="m-widget4__title font-13 text-capitalize" style="color:#ffffff !important;">' +
                        emp.lname + ', ' + emp.fname + '';
                    string += '</span><br />';
                    string += '<span class="m-widget4__sub font-11" style="color:#ffffff !important">' +
                        emp.pos_details + '</span>';
                    string += '</div>';
                    string += '</div>';
                    string += '</div>';
                    string += '</div>';
                    string += '</div>';
                    string += '</div>';
                });
                $("#card-container").html(string);
                var myLazyLoad = new LazyLoad({
                    elements_selector: ".employee-pic"
                });
                $('.employee-pic').on('error', function() {
                    $(this).attr('onerror', null);
                    $(this).attr('src', "<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>");
                    // $(this).attr('data-src', "<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>");
                    heightChanger();
                });
                var count = $("#total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#count").html(1 + ' - ' + perPage);
                } else {
                    $("#count").html((limiter + 1) + ' - ' + result);
                }

                mApp.unblock('#content');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#content');
            }
        });
    }

    var customPopover = function(elements) {
        $.each(elements, function(key, elemx) {
            var elem = $(elemx);
            var id = elem.attr('id');
            var title = elem.data('title') ? elem.data('title') : '';
            var required = elem.data('required');
            var type = elem.data('type') ? elem.data('type') : 'text';
            var save = (elem.data('save') === 'insert') ? 'insert' : 'update';
            var isEmpty = elem.data('isempty');
            var value = (isEmpty === true) ? "" : elem.text();
            var data = elem.data('data') ? elem.data('data') : [];
            var notifString =
                '<div id="notif-required" class="m--font-danger m--font-boldest mt-1" style="display:none;">Required*</div><div id="notif-number" class="m--font-danger m--font-boldest mt-1" style="display:none;">Not a valid number*</div>';
            var buttons =
                '<button title="Save Changes" class="btn btn-outline-info btn-sm py-1 px-1 popover-approve" type="button" data-required="' +
                required +
                '"><i class="fa fa-check" style="width:20px;font-size:15px;line-height: 1;"></i></button> <button title="Cancel Changes" class="btn btn-outline-danger btn-sm  py-1 px-1 popover-reject" type="button"><i class="fa  fa-times"  style="width:20px;font-size:15px;line-height: 1;"></i></button>';
            var contentText = '<input type="text" name="' + id + '" value="' + value + '"  data-value="' +
                value + '" class="form-control form-control-sm m-input"/>';
            var contentPassword = '<input type="password" name="' + id + '" value="' + value +
                '"  data-value="' + value + '" class="form-control form-control-sm m-input"/>';
            var contentEmail = '<input type="email" name="' + id + '" value="' + value + '"  data-value="' +
                value + '" class="form-control form-control-sm m-input"/>';
            var contentTextarea = '<textarea class="form-control m-input" name="' + id + '"  data-value="' +
                value + '" rows="3">' + value + '</textarea>';
            var contentDate = '<input type="text"  name="' + id + '"  data-value="' + value +
                '" class="form-control form-control-sm  custom-datepicker" readonly="" ' + value + '>';
            var contentDateRange = '<input type="text"  name="' + id + '"  data-value="' + value +
                '" class="form-control form-control-sm  custom-daterangepicker" readonly="" ' + value + '>';
            var contentNumber = '<input type="text" name="' + id + '" value="' + value + '"  data-value="' +
                value + '" class="form-control form-control-sm m-input"/>';
            var clearfield =
                '<button title="Clear Input" class="btn btn-outline-warning btn-sm py-1 px-1 popover-clearfield" type="button" style="margin-right:3px"><i class="fa fa-repeat" style="width:20px;font-size:15px;line-height: 1;"></i></button>';
            if (isEmpty) {
                elem.html('Empty');
                elem.addClass('m--font-danger font-italic');
            }
            switch (type) {
                case 'text':
                    var content = contentText;
                    break;
                case 'number':
                    var content = contentNumber;
                    break;
                case 'password':
                    var content = contentPassword;
                    break;
                case 'email':
                    var content = contentEmail;
                    break;
                case 'date':
                    var content = contentDate;
                    break;
                case 'daterange':
                    var content = contentDateRange;
                    break;
                case 'select':
                    var contentSelect = '<select name="' + id +
                        '" class="form-control form-control-sm m-input">';
                    contentSelect += '<option value="">--</option>';
                    $.each(data, function(key, obj) {

                        if (obj.value.toString().toLowerCase() === value.toString().toLowerCase()) {
                            contentSelect += '<option value="' + obj.value + '" selected>' + obj.text +
                                '</option>';
                            elem.html(obj.text);
                        } else {
                            contentSelect += '<option value="' + obj.value + '">' + obj.text +
                                '</option>';
                        }

                    });
                    contentSelect += '</select>';
                    var content = contentSelect;
                    break;
                case 'textarea':
                    var content = contentTextarea;
                    break;
            }
            elem.popover({
                trigger: 'click',
                placement: 'bottom',
                title: title,
                html: true,
                template: '\
                <div class="m-popover m-popover--skin-dark popover" role="tooltip">\
                <div class="arrow"></div>\
                <h3 class="popover-header"></h3>\
                <div class="popover-body p-2"></div>\
                </div>',
                content: '<div class="row no-gutters" data-save="' + save + '" data-type="' + type +
                    '"><div class="col-md-8 col-xs-7">' + content +
                    '</div><div class="col-md-4 col-xs-5 text-right">' + clearfield + buttons +
                    '</div></div>' + notifString
            }).on('click', function(e) {
                e.preventDefault();
                $('[data-original-title]').not(elem).popover('hide');

            }).on('inserted.bs.popover', function() {

                var popover_elem = $(this).data("bs.popover").tip;
                if (type === 'date') {
                    var input_elem = $(popover_elem).find('.custom-datepicker');
                    input_elem.datepicker({
                        orientation: "bottom left",
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        },
                        format: 'yyyy-mm-dd'
                    });

                    if (!isEmpty) {
                        input_elem.datepicker('update', new Date(value));
                    }

                }
                if (type === 'daterange') {
                    var input_elem = $(popover_elem).find('.custom-daterangepicker');
                    input_elem.daterangepicker({
                        opens: "left",
                        drops: "down",
                        autoApply: false,
                        clearBtn: true
                    }, function(start, end, label) {
                        input_elem.val(start.format('YYYY/MM/DD') + ' - ' + end.format(
                            'YYYY/MM/DD'));
                    });
                    if (!isEmpty) {
                        var splitted = value.split(" - ");
                        input_elem.data('daterangepicker').setStartDate(new Date(splitted[0]));
                        input_elem.data('daterangepicker').setEndDate(new Date(splitted[1]));
                    }
                } else if (type === 'select') {
                    var input_elem = $(popover_elem).find('select');
                    var realvalue = input_elem.val();
                    input_elem.data('value', realvalue);
                }
            }).on("show.bs.popover", function() {
                var el = $(this).data("bs.popover").tip;
                $(el).css("max-width", "300px");
            });
        });
    };
    var mQuickSidebar = function() {
        var topbarAside = $('#m_quick_sidebar');
        var topbarAsideClose = $('#m_quick_sidebar_close');
        var topbarAsideToggle = $('#m_quick_sidebar_toggle');
        var initOffcanvas = function() {
            topbarAside.mOffcanvas({
                class: 'm-quick-sidebar',
                overlay: true,
                close: topbarAsideClose,
                toggle: topbarAsideToggle
            });
        };
        return {
            init: function() {
                if (topbarAside.length === 0) {
                    return;
                }
                initOffcanvas();
            }
        };
    }();
    $(function() {
        mQuickSidebar.init();
        $('.m_selectpicker').selectpicker();

        //---------------------------------EMPLOYEE UPDATE INITIALIZATIONS------------------------------------------------------------------------------
        var startCount = 100;
        var year = parseInt(moment().year());
        var select_previousYears = [];
        for (var x = startCount; x >= 0; x--) {
            select_previousYears.push({
                value: year.toString(),
                text: year.toString()
            });
            year = year - 1;
        }
        var select_positions = [];
        get_all_hiring_positions(function(positionArr) {
            
            $.each(positionArr.positions, function(key, pos) {
                select_positions.push({
                    value: pos.pos_id,
                    text: pos.pos_details
                });
            });
        });

        
        
 

        var select_gender = [{
                value: 'male',
                text: 'Male'
            },
            {
                value: 'female',
                text: 'Female'
            }
        ];
        var select_yesno = [{
                value: '1',
                text: 'Yes'
            },
            {
                value: '0',
                text: 'No'
            }
        ];
        var select_civil = [{
                value: 'single',
                text: 'Single'
            },
            {
                value: 'married',
                text: 'Married'
            },
            {
                value: 'legally separated',
                text: 'Legally Separated'
            },
            {
                value: 'widower',
                text: 'Widower'
            }
        ];
        var select_extension = [{
                value: 'jr',
                text: 'Jr'
            },
            {
                value: 'sr',
                text: 'Sr'
            },
            {
                value: 'i',
                text: 'I'
            },
            {
                value: 'ii',
                text: 'II'
            }
        ];
        var select_bloodtype = [{
                value: 'a',
                text: 'A'
            },
            {
                value: 'b',
                text: 'B'
            },
            {
                value: 'o',
                text: 'O'
            },
            {
                value: 'ab',
                text: 'AB'
            }
        ];
        var select_religion = [{
                value: 'roman catholic',
                text: 'Roman Catholic'
            },
            {
                value: 'muslim',
                text: 'Muslim'
            },
            {
                value: 'protestant',
                text: 'Protestant'
            },
            {
                value: 'iglesia ni cristo',
                text: 'Iglesia Ni Cristo'
            },
            {
                value: 'el shaddai',
                text: 'El Shaddai'
            },
            {
                value: 'jehovahs witnesses',
                text: 'Jehovahs Witnesses'
            }
        ];
        //---------------------------------------------------EMPLOYEE UPDATE INITIALIZATIONS--------------------------------------------------------




        $("#perpage").change(function() {
            var perPage = $("#perpage").val();
            getEmployeeDirectory(0, perPage, function(total) {
                $("#pagination").pagination('destroy');
                $("#pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    displayedPages: 10,
                    onPageClick: function(pagenumber) {
                        var perPage = $("#perpage").val();
                        getEmployeeDirectory((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });
        $("#perpage").change();
        $("#search-name").change(function() {
            $("#perpage").change();
        });
        $("#card-container").on('click', '.scaler', function() {
            $('#m_quick_sidebar').data('apid', $(this).data('apid'));
            $('#m_quick_sidebar').data('isactive', $(this).data('isactive'));
            $("#m_quick_sidebar_toggle").click();
            var apid = $('#m_quick_sidebar').data('apid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Employee/profilePreviewApplicants",
                beforeSend: function() {
                    mApp.block($('#m_quick_sidebar'));
                },
                data: {
                    apid: apid
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    //USER ACCESS ------------------------------------------------------------------------------------------------------------------
                    var profile = res.profile;
                    var address = profile.presentAddress;
                    var addressArr = (address === null) ? null : jQuery.grep(address.split(
                        " = "), function(n) {
                        return (n);
                    });
                    var addressArr = (addressArr === null) ? null : jQuery.grep(address.split(
                        "|"), function(n) {
                        return (n);
                    });
                    var addressString = (addressArr === null) ? "" : addressArr.join(", ");
                    var address2 = profile.permanentAddress;
                    var addressArr2 = (address2 === null) ? null : jQuery.grep(address2.split(
                        " = "), function(n) {
                        return (n);
                    });
                    var addressArr2 = (addressArr2 === null) ? null : jQuery.grep(address2
                        .split("|"),
                        function(n) {
                            return (n);
                        });
                    var addressString2 = (addressArr2 === null) ? "" : addressArr2.join(", ");
                    var nameExt = (profile.nameExt === null) ? "" : profile.nameExt;
                    $("#profile-single-image").attr('src', "<?php echo base_url(); ?>" + profile.pic + "?" + new Date().getTime());
                    $("#profile-single-name").html(profile.fname + " <span style='color:#9ef5a2'>" + profile.mname + "</span> " + profile.lname + ' ' + nameExt);
                    $("#profile-single-position").html((profile.pos_details === null || profile.pos_details === '') ? "-" : profile.pos_details);
                    $("#profile-single-presentAddress").html((addressString === null || addressString === '') ? "-" : addressString);
                    $("#profile-single-permanentAddress").html((addressString2 === null || addressString2 === '') ? "-" : addressString2);
                    $("#profile-single-cell").html((profile.cell === null || profile.cell === '') ? "-" : profile.cell);
                    $("#profile-single-tel").html((profile.tel === null || profile.tel === '') ? "-" : profile.tel);
                    $("#profile-single-birthday").html((profile.birthday === null || profile.birthday === '') ? "-" : moment(profile.birthday).format('MMMM DD, YYYY'));
                    $("#profile-single-gender").html((profile.gender === null || profile.gender === '') ? "-" : profile.gender);
                    $("#profile-single-civilStatus").html((profile.civilStatus === null || profile.civilStatus === '') ? "-" : profile.civilStatus);
                    $("#profile-single-religion").html((profile.religion === null || profile.religion === '') ? "-" : profile.religion);
                    var withApostrophe = profile.fname.split(' ')[0];
                    if (withApostrophe.substr(-1) === 's') {
                        withApostrophe += "'";
                    } else {
                        withApostrophe += "'s";
                    }
                    $("#profile-single-firstname").html(withApostrophe);
                    $('#profile-single-image').on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', "<?php echo base_url('assets/images/img/sz.png '); ?>");
                        heightChanger();
                    });
                    $(".quickbar-reference").click(function() {
                        $('#m_quick_sidebar').data('emp_id', $(this).data(
                            'referenceid'));
                        $('#m_quick_sidebar').data('isactive', false);
                        $("#btn-full-quicksidebar").click();
                    });
                    mApp.unblock($('#m_quick_sidebar'));
                }
            });
        });
        //ISSUE WITH DATERANGEPICKER
        // $('html').on('click', function (e) {
        //      if (typeof $(e.target).data('original-title') === 'undefined' && $(e.target).closest('.popover').length === 0) {
        //          $('[data-original-title]').popover('hide');
        //      }
        //  });
        $("#btn-full-quicksidebar").click(function(e) {
            e.preventDefault();
            var top = (window.pageYOffset || $(document).scrollTop) - ($(document).clientTop || 0);
            $("#main-directory").data('scroll', top);
            $("#main-directory").hide();
            $("#single-profile").show(500);
            $(".m-subheader").hide();
            $("#m_quick_sidebar_close").click();

            //USER ACCESS -------------------------------------------------------------------------------------------------------------

            $(".alert_for_user_access").remove();
            //------------------------INSERT CODES FOR DISPLAY HERE-------------------------------------------------------------
            $('[href="#tab-personal"]').click();
            var apid = $('#m_quick_sidebar').data('apid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Employee/employeeDetailsApplicants",
                beforeSend: function() {
                    mApp.block($('#content'));
                },
                data: {
                    apid: apid
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    var employee = res.employee;
                    $("#image-caption-lname").html(employee.lname);
                    $("#image-caption-fname").html(employee.fname);
                    $("#tbl_applicant-lname").html(employee.lname);
                    $("#tbl_applicant-mname").html(employee.mname);
                    $("#tbl_applicant-fname").html(employee.fname);
                    $("#tbl_applicant-nameExt").html(employee.nameExt);
                    $("#tbl_applicant-nickName").html(employee.nickName);
                    $("#tbl_applicant-birthday").html(employee.birthday);
                    $("#tbl_applicant-birthplace").html(employee.birthplace);
                    $("#tbl_applicant-gender").html(employee.gender);
                    $("#tbl_applicant-religion").html(employee.religion);
                    $("#tbl_applicant-civilStatus").html(employee.civilStatus);
                    $("#tbl_applicant-bloodType").html(employee.bloodType);
                    $("#tbl_applicant-tel").html(employee.tel);
                    $("#tbl_applicant-cell").html(employee.cell);
                    $("#tbl_applicant-permanentAddress").html(employee.permanentAddress);
                    $("#tbl_applicant-presentAddress").html(employee.presentAddress);


                    $("#tbl_applicant-latestEmployer").html(employee.latestEmployer);
                    $("#tbl_applicant-lastPositionHeld").html(employee.lastPositionHeld);
                    $("#tbl_applicant-inclusiveDates").html(employee.inclusiveDates);
                    $("#tbl_applicant-lastEmployerAddress").html(employee.lastEmployerAddress);
                    $("#tbl_applicant-lastEmployerContact").html(employee.lastEmployerContact);
                    if (employee.birthday === '') {
                        $("#personal-age").html("No Birthday");
                    } else {
                        var bday = tz_date(moment()).diff(employee.birthday, 'years');
                        $("#personal-age").html(bday + " years old");
                    }

                    //CHARREF----------------------------------------------------

                    for (var charx = 1; charx <= 3; charx++) {
                        var charref = employee['cr' + charx];
                        var splitted = charref.split('|');
                        var lname = "";
                        var fname = "";
                        var mname = "";
                        var mobile = "";
                        var occupation = "";
                        if (splitted.length > 0) {
                            var names = splitted[0].split('$');
                            if (names.length > 0) {
                                lname = names[2];
                                fname = names[0];
                                mname = names[1];
                            }
                            mobile = splitted[1];
                            occupation = splitted[2];
                        }
                        $("#cr" + charx + "-lname").val(lname);
                        $("#cr" + charx + "-fname").val(fname);
                        $("#cr" + charx + "-mname").val(mname);
                        $("#cr" + charx + "-mobilenumber").val(mobile);
                        $("#cr" + charx + "-occupation").val(occupation);
                        $("#btn-cr" + charx).data('original_value', charref);
                    }
                    //END OF CHARREF----------------------------------------------------

                    $("#tbl_applicant-source").html(employee.source);
                    $("#tbl_applicant-ccaExp").html(employee.ccaExp);
                    $("#tbl_applicant-pos_id").html(employee.pos_id);
                    $("a[id^='tbl_applicant']").each(function(id, elem) {

                        var val = $(elem).html();
                        if (val === '') {
                            $(elem).data('isempty', true);
                        }
                    });
                    //--------------------------POPOVER INITIALIZATIONS----------------------------------------------------


                    $("#tbl_applicant-gender").data('data', select_gender);
                    $("#tbl_applicant-civilStatus").data('data', select_civil);
                    $("#tbl_applicant-nameExt").data('data', select_extension);
                    $("#tbl_applicant-bloodType").data('data', select_bloodtype);
                    $("#tbl_applicant-religion").data('data', select_religion);
                    $("#tbl_applicant-ccaExp").data('data', select_yesno);
                    $("#tbl_applicant-pos_id").data('data', select_positions);
                    //------------------------INSERT CODES FOR XEDITABLE HERE ----------------------------------------------------------
                    var element_ids = [
                        "#tbl_applicant-lname",
                        "#tbl_applicant-fname",
                        "#tbl_applicant-mname",
                        "#tbl_applicant-nickName",
                        "#tbl_applicant-nameExt",
                        "#tbl_applicant-birthday",
                        "#tbl_applicant-birthplace",
                        "#tbl_applicant-religion",
                        "#tbl_applicant-bloodType",
                        "#tbl_applicant-gender",
                        "#tbl_applicant-civilStatus",
                        "#tbl_applicant-presentAddress",
                        "#tbl_applicant-permanentAddress",
                        "#tbl_applicant-tel",
                        "#tbl_applicant-cell",
                        "#tbl_applicant-source",
                        "#tbl_applicant-ccaExp",
                        "#tbl_applicant-pos_id",
                        "#tbl_applicant-latestEmployer",
                        "#tbl_applicant-lastPositionHeld",
                        "#tbl_applicant-inclusiveDates",
                        "#tbl_applicant-lastEmployerAddress",
                        "#tbl_applicant-lastEmployerContact"
                    ];

                    $("#image-caption-active").removeClass();
                    $("#image-caption-active").addClass(
                        "m-badge m-badge--success m-badge--wide m-badge--rounded");
                    $("#image-caption-active").html("active");
                    customPopover(element_ids);
                    $.each(element_ids, function(x, elem) {
                        $(elem).css({
                            "color": "",
                            "font-style": ""
                        });
                        $(elem).parent().css({
                            "text-decoration": ""
                        });
                    });
                    // Start upload preview image
                    $("#item-img-output").attr("src", "<?php echo base_url(); ?>" + employee.pic + "?" + new Date().getTime());
                    $('#item-img-output').on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', "<?php echo base_url('assets/images/img/sz.png'); ?>");
                    });
                    mApp.unblock($('#content'));
                    //---------------------------------VIEW DTR LOGS------------------------------------------------------------------------------------

                }

            });
        });
        // End upload preview image
        $("#btn-backtodirectory").click(function(e) {
            e.preventDefault();
            $(".m-subheader").show();
            $("#main-directory").show(500, function() {
                $(document).scrollTop($(this).data('scroll'));
            });
            $("#single-profile").hide();
            $('[data-original-title]').popover('dispose');
        });
        $("html").on('click', '.popover-reject', function() {
            $(this).closest('.popover').popover('hide');
        });
        $("html").on('click', '.popover-clearfield', function() {
            $(this).closest('.popover').find('.form-control').val('');
        });
        $("html").on('click', '.popover-approve', function() {
            var that = this;
            var elem = $(that).closest('.popover').find('.form-control');
            var type = $(that).closest('.popover').find('.row').data('type');
            var original_value = $(elem).data('value');
            var value = ($(elem).val() === null) ? null : $(elem).val().trim();
            var element_name = $(elem).attr('name');
            var temp_name = element_name.split("-");
            var table = temp_name[0];
            var name = temp_name[1];
            var required = $(that).data('required');
            var isOkay = 'no';
            if (required === 'yes') {
                if (value === '' || value === null) {
                    $("#notif-required").show();
                    isOkay = 'no';
                } else {
                    $("#notif-required").hide();
                    if (type === 'number') {
                        if ($.isNumeric(value.trim())) {
                            $("#notif-number").hide();
                            isOkay = 'yes';
                        } else {
                            $("#notif-number").show();
                            isOkay = 'no';
                        }
                    } else {
                        $("#notif-number").hide();
                        isOkay = 'yes';
                    }
                }
            } else if (value !== '' && required !== 'yes') {
                $("#notif-required").hide();
                if (type === 'number') {
                    if ($.isNumeric(value.trim())) {
                        $("#notif-number").hide();
                        isOkay = 'yes';
                    } else {
                        $("#notif-number").show();
                        isOkay = 'no';
                    }
                } else {
                    $("#notif-number").hide();
                    isOkay = 'yes';
                }
            } else {
                isOkay = 'yes';
            }

            if (isOkay === 'yes') {
                var apid = $('#m_quick_sidebar').data('apid');
                if (original_value.toString().toLowerCase() === value.toString().toLowerCase()) {
                    $(that).closest('.popover').popover('hide');
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Employee/updateApplicantProfile",
                        data: {
                            name: name,
                            value: value.toString().toLowerCase(),
                            table: table,
                            apid: apid,
                            original_value: original_value
                        },
                        cache: false,
                        success: function(res) {
                            res = JSON.parse(res.trim());
                            $(that).closest('.popover').popover('hide');
                            if (res.status === 'Success') {
                                $(that).closest('.popover').popover('dispose');
                                $.notify({
                                    message: 'Successfully Updated'
                                }, {
                                    type: 'success',
                                    timer: 1000
                                });

                                var employee = res.employee;
                                if (element_name === 'tbl_applicant-lname' || element_name ===
                                    'tbl_applicant-fname') {
                                    $("#image-caption-fname").html(employee.fname);
                                    $("#image-caption-lname").html(employee.lname);
                                    $(".scaler[data-apid=" + apid + "]").find(
                                        'span.m-widget4__title').html(employee.lname +
                                        ', ' + employee.fname);
                                } else if (element_name === 'tbl_applicant-birthday') {
                                    if (value === '') {
                                        $("#personal-age").html("No Birthday");
                                    } else {
                                        var bday = moment().diff(value, 'years');
                                        $("#personal-age").html(bday + " years old");
                                    }
                                } else if (element_name === 'tbl_applicant-pos_id') {
                                    $(".scaler[data-apid=" + apid + "]").find(
                                        'span.m-widget4__sub').html(employee.pos_details);
                                }
                                if (value === '') {
                                    $("#" + element_name).html("Empty");
                                    $("#" + element_name).addClass(
                                        'm--font-danger font-italic');
                                    $("#" + element_name).data('isempty', true);
                                    customPopover(["#" + element_name]);
                                } else {
                                    $("#" + element_name).html(value);
                                    $("#" + element_name).removeClass(
                                        'm--font-danger font-italic');
                                    $("#" + element_name).data('isempty', false);
                                    customPopover(["#" + element_name]);
                                }
                            } else {
                                $.notify({
                                    message: 'An error occured while processing your request'
                                }, {
                                    type: 'danger',
                                    timer: 1000
                                });
                            }
                        }
                    });
                }
            }
        });
    });
    $("#btn-cr1,#btn-cr2,#btn-cr3").click(function() {
        var that = this;
        var apid = $('#m_quick_sidebar').data('apid');
        var name = $(that).data('char'); //cr1,cr2,cr3
        var original_value = $(that).data('original_value');
        var lname = $("#" + name + "-lname").val();
        var fname = $("#" + name + "-fname").val();
        var mname = $("#" + name + "-mname").val();
        var mobilenumber = $("#" + name + "-mobilenumber").val();
        var occupation = $("#" + name + "-occupation").val();
        var value = lname + '$' + fname + '$' + mname + '|' + mobilenumber + '|' + occupation;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/updateApplicantProfile",
            data: {
                name: name,
                value: value,
                apid: apid,
                original_value: original_value
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                if (res.status === 'Success') {
                    $.notify({
                        message: 'Successfully Updated'
                    }, {
                        type: 'success',
                        timer: 1000
                    });

                } else {
                    $.notify({
                        message: 'An error occured while processing your request'
                    }, {
                        type: 'danger',
                        timer: 1000
                    });
                    //CHARREF----------------------------------------------------

                    var charref = original_value;
                    var splitted = charref.split('|');
                    var lname = "";
                    var fname = "";
                    var mname = "";
                    var mobile = "";
                    var occupation = "";
                    if (splitted.length > 0) {
                        var names = splitted[0].split('$');
                        if (names.length > 0) {
                            lname = names[0];
                            fname = names[2];
                            mname = names[1];
                        }
                        mobile = splitted[1];
                        occupation = splitted[2];
                    }
                    $("#cr" + name + "-lname").val(lname);
                    $("#cr" + name + "-fname").val(fname);
                    $("#cr" + name + "-mname").val(mname);
                    $("#cr" + name + "-mobilenumber").val(mobile);
                    $("#cr" + name + "-occupation").val(occupation);

                    //END OF CHARREF----------------------------------------------------
                }
            }
        });
    });
    $("#passModal").on('show.bs.modal', function(e) {
        if (e.namespace === 'bs.modal') {
            $("#pass-startdate").datepicker({
                orientation: "top right",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                },
                format: 'yyyy-mm-dd'
            });
            get_all_emp_stat(function(res) {
                $("#pass-status").html("");
                var string = '<option value="">--</option>';
                $.each(res.emp_stat, function(i, item) {
                    string += '<option value="' + item.empstat_id + '" style="text-transform:capitalize">' + item.status +
                        '</option>';
                });
                $("#pass-status").html(string);
                $('#pass-status').selectpicker('refresh');
            });
            get_all_accounts(function(res) {
                $("#pass-account").html("");
                var string = '<option value="">--</option>';
                $.each(res.accounts, function(i, item) {
                    string += '<option value="' + item.acc_id + '" style="text-transform:capitalize">' + item.acc_name + '</option>';
                });
                $("#pass-account").html(string);
                $('#pass-account').selectpicker('refresh');
            });
            get_all_active_employees(function(res) {
                $("#pass-supervisor").html("");
                var string = '<option value="">--</option>';
                $.each(res.activeEmployees, function(i, item) {
                    string += '<option value="' + item.emp_id + '" style="text-transform:capitalize">' + item.lname + ", " +
                        item.fname + '</option>';
                });
                $("#pass-supervisor").html(string);
                $('#pass-supervisor').selectpicker('refresh');
            })
        }
    });
    $("#pass-account").change(function() {
        var that = this;
        var acc_id = $(that).val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/get_all_position_per_account",
            data: {
                acc_id: acc_id
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#pass-position").html("");
                var string = '<option value="">--</option>';
                $.each(res.positions, function(i, item) {
                    string += '<option value="' + item.pos_id + '" style="text-transform:capitalize">' + item.pos_details +
                        '</option>';
                });
                $("#pass-position").html(string);
                $('#pass-position').selectpicker('refresh');
            }
        });
    });
    $("#btn-applicantPass").click(function() {
        var apid = $('#m_quick_sidebar').data('apid');
        var acc_id = $("#pass-account").val();
        var pos_id = $("#pass-position").val();
        var supervisor_id = $("#pass-supervisor").val();
        var empstat_id = $("#pass-status").val();
        var startdate = $("#pass-startdate").val();
        if (acc_id === '' || pos_id === '' || empstat_id === '' || startdate === '' || supervisor_id === '') {
            $.notify({
                message: 'Form is not complete.'
            }, {
                type: 'warning',
                timer: 1000,
                z_index: 99999
            });
        } else {
            swal({
                title: 'Are you sure?',
                html: "The <b>applicant</b> will be <b>employed</b>.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: "btn btn-info m-btn m-btn--air",
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, pass the applicant!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Employee/passApplicant",
                        data: {
                            apid: apid,
                            acc_id: acc_id,
                            pos_id: pos_id,
                            empstat_id: empstat_id,
                            startdate: startdate,
                            supervisor_id:supervisor_id
                        },
                        cache: false,
                        success: function(res) {
                            res = JSON.parse(res.trim());
                            if (res.status === "Success") {
                                $("#btn-backtodirectory").click();
                                $(".m-quick-sidebar__close").click();
                                swal("Successfully employed the applicant",
                                    "Username: <span class='m--font-brand'>" + res.username + "</span><br>Password: <span class='m--font-brand'>" + res.password + "</span>",
                                    "success");
                                $("#perpage").change();
                            } else {
                                if (!res.withrate) {
                                    swal("No Rate !!", "Position Status has no rate set.",
                                        "error");
                                } else {
                                    $.notify({
                                        message: 'An error occured while processing your action'
                                    }, {
                                        type: 'danger',
                                        timer: 1000
                                    });
                                }

                            }
                        }
                    });
                }
            });
        }
    });
    $("#btn-modal-fail-proceed").click(function() {
        var apid = $("#btn-modal-fail-proceed").data('apid');
        var failReason = $("#input-failReason").val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/failApplicant",
            data: {
                apid: apid,
                failReason: failReason
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                if (res.status === "Success") {
                    $("#btn-backtodirectory").click();
                    $(".m-quick-sidebar__close").click();
                    $.notify({
                        message: 'Successfully failed the applicant.'
                    }, {
                        type: 'success',
                        timer: 1000
                    });
                    $("#perpage").change();
                } else {
                    $.notify({
                        message: 'An error occured while processing your action'
                    }, {
                        type: 'danger',
                        timer: 1000
                    });
                }
                $("#modal-fail-applicant").modal('hide');
            }
        });
    })
    $(".btn-applicantFail").click(function() {
        var apid = $('#m_quick_sidebar').data('apid');
        $("#modal-fail-applicant").modal('show');
        $("#btn-modal-fail-proceed").data('apid', apid);
    });
</script>