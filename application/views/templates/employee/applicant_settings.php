<style type="text/css">
    .btn-outline-green {
        color: #4CAF50 !important;
        background: white !important;
        border-color: #4CAF50 !important;
    }

    .btn-outline-green:hover {
        background: #4CAF50 !important;
        color: white !important;
        border-color: #4CAF50 !important;
    }

    .bg-green {
        background: #4CAF50 !important;
    }

    .btn-green {
        background: #4CAF50;
        color: white !important;
    }

    .btn-green:hover {
        background: #2a862e !important;
    }

    .font-green {
        color: #4CAF50 !important;
    }

    .font-white {
        color: white !Important;
    }

    .border-light-green {
        border-color: #4CAF507d !important;
    }

    .border-green {
        border-color: #4CAF50 !important;
    }

    .body {
        padding-right: 0 !important;
    }

    .swal2-title {
        font-family: 'Poppins' !important;
    }

    .font-poppins {
        font-family: 'Poppins' !important;
    }

    .card-shadow {
        box-shadow: 0 1px 8px rgba(0, 0, 0, 0.15);
    }

    #bootstrap-duallistbox-nonselected-list_from option,
    #bootstrap-duallistbox-selected-list_from option {
        font-size: 13px !important;
    }

    .font-16 {
        font-size: 16px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="content">

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Applicant Settings:
                    <br /><small>Employee Management System</small></h3>
            </div>
            <div class="col-md-6 text-right" style="margin-top:13px">
                <div class="btn-group m-btn-group flex-wrap " role="group" aria-label="...">
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/applicant'"><span><i class="fa 	fa-th-list"></i><span>Applicants List</span></span></button>
                    <!-- <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Employee/manual_tap'"><span><i class="fa 	fa-hand-pointer-o"></i><span>Manual TAP</span></span></button> -->
                    <button type="button" class="m-btn btn m-btn m-btn--icon btn-green" onclick="location.href = '<?php echo base_url(); ?>Employee/applicant_settings'"><span><i class="fa 	fa-gear"></i><span>Applicant Settings</span></span></button>
                </div>
            </div>
        </div>
        <br>
        <div class="card card-shadow" id="card-container">
            <div class="card-body">
                <ul class="nav nav-tabs mb-0" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link   active show" data-toggle="tab" href="#tab-download"><span class="fa fa-download d-lg-none d-xl-inline font-green"></span> File Download</a>
                    </li>
                </ul>
                <br />
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-download" role="tabpanel">
                        <div class="card">
                            <div class="card-header m--font-bolder font-green font-16">
                                <div class="row">
                                    <div class="col-md-12">FILE DOWNLOAD</div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="alert-maximum" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-warning alert-dismissible fade show" style="display:none" role="alert">
                                    <div class="m-alert__icon">
                                        <i class="flaticon-exclamation-1"></i>
                                        <span></span>
                                    </div>
                                    <div class="m-alert__text">
                                        <strong>Warning!</strong> Downloading excel with more than 3000 number of applicants may generate an error due to very large data processing.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                    </div>
                                </div>
                                <div id="alert-error" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" style="display:none" role="alert">
                                    <div class="m-alert__icon">
                                        <i class="flaticon-exclamation-1"></i>
                                        <span></span>
                                    </div>
                                    <div class="m-alert__text">
                                        <strong>Error!</strong> An error occurred while generating excel. Please try again with less than 3000 number of applicants.
                                    </div>
                                    <div class="m-alert__close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div style="border:1px solid #ddd" class="p-3">
                                            <label for="" class="m--font-bolder" style="font-size:12px;">Date Added Duration:</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="filter-daterange" style="font-size:12px">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <hr>
                                            <button class="btn btn-success m-btn m-btn--icon btn-block" id="btn-excel">
                                                <span>
                                                    <i class="fa fa-file-excel-o"></i>
                                                    <span>Download</span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="m-form__group form-group">
                                            <select name="from" id="dualListBox-exports" size="10" multiple="multiple"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/bootstrap-duallistbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.bootstrap-duallistbox.min.js"></script>
<script>
    function tz_date(thedate, format = null) {
        var tz = moment.tz(new Date(thedate), "Asia/Manila");
        return (format === null) ? moment(tz) : moment(tz).format(format);
    }
    var getApplicants = function() {
        var startDate = $('#filter-daterange').data('daterangepicker').startDate.format('YYYY-MM-DD');
        var endDate = $('#filter-daterange').data('daterangepicker').endDate.format('YYYY-MM-DD');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Employee/getApplicantForExports",
            data: {
                startDate: startDate,
                endDate: endDate
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#dualListBox-exports").html("");
                var str = "";
                $.each(res.applicants, function(key, obj) {
                    str += '<option value="' + obj.apid + '" class="text-capitalize">' + obj.lname.toLowerCase() + ', ' + obj.fname.toLowerCase() + '</option>';
                });
                $("#dualListBox-exports").html(str);
                $('#dualListBox-exports').bootstrapDualListbox('refresh');
            }
        });
    };
    $(function() {
        var dualListBox_exports = $('#dualListBox-exports').bootstrapDualListbox({
            nonSelectedListLabel: '<span class="m--font-bolder m--font-danger">Non-selected:</span>',
            selectedListLabel: '<span class="m--font-bolder font-green">Selected Employee:</span>',
            selectorMinimalHeight: 150
        });
        var dualListContainer = dualListBox_exports.bootstrapDualListbox('getContainer');
        dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
        dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
        dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
        dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');

        var currentdate = tz_date(moment());
        var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
        var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
        $('#filter-daterange').val(start + ' - ' + end);
        $('#filter-daterange').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "right",
            maxDate: currentdate,
            minDate: moment("2016-01-01"),
            ranges: {
                'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate, 'YYYY-MM-DD').endOf('month')],
                'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, "month").startOf('month'), moment(currentdate, 'YYYY-MM-DD').subtract(1, "month").endOf('month')],
                'This Year': [moment(currentdate, 'YYYY-MM-DD').startOf('year'), moment(currentdate, 'YYYY-MM-DD').endOf('year')],
                'Last Year': [moment(currentdate, 'YYYY-MM-DD').subtract(1, "year").startOf('year'), moment(currentdate, 'YYYY-MM-DD').subtract(1, "year").endOf('year')]
            }
        }, function(start, end, label) {
            $('#filter-daterange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            getApplicants();
        });
        getApplicants();
    })
    $('#dualListBox-exports').on('change', function(e) {
        var value = $(this).val();
        if (value.length > 3000) {
            $("#alert-maximum").show();
        } else {
            $("#alert-maximum").hide();
        }
    });
    $("#btn-excel").click(function() {
        var users = $("#dualListBox-exports").val();
        if (users.length === 0) {
            swal("WAIT...", "Select employee first to download file", "warning");
        } else {

            var apids = users.join('-');
            $.ajax({
                url: '<?php echo base_url(); ?>Employee/downloadApplicantExcel',
                method: 'POST',
                data: {
                    apids: apids
                },
                beforeSend: function() {
                    mApp.block('#card-container', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg',
                        message: 'Downloading...'
                    });
                },
                xhrFields: {
                    responseType: 'blob'
                },
                success: function(data) {
                    var a = document.createElement('a');
                    var url = window.URL.createObjectURL(data);
                    a.href = url;
                    a.download = 'SZ_EMSApplicantExport.xlsx';
                    a.click();
                    window.URL.revokeObjectURL(url);
                    mApp.unblock('#card-container');
                },
                error: function(data) {
                    console.log(data);
                    $("#alert-error").show();
                    $("#alert-maximum").hide();
                    mApp.unblock('#card-container');
                }
            });

        }
    });
</script>