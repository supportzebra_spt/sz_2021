<style>
	.custom-txt-center {
		margin: 0 auto;
	}

	.pCelebNames .Male {
		font-size: 1.35em;
		color: #00aabd;
	}

	.pCelebNames .Female {
		font-size: 1.35em;
		color: #f4516c;
	}

	.pCelebNames {
		font-size: smaller;
	}

	.tooltip-inner {
		text-align: left;
		color: #fff !important;
		font-size: 1em !important;
	}

	.highcharts-exporting-group,
	.highcharts-credits {
		display: none
	}

	.m-portlet__head {
		height: 50px !important;
	}

	.m-portlet__body {
		background: white !important;
	}

	.highcharts-title {
		font-family: 'Poppins' !important;
	}

	.bs-popover-auto[x-placement^=right] .arrow::after,
	.bs-popover-right .arrow::after {
		border-right-color: #282A3C !important;
	}

	.bs-popover-auto[x-placement^=left] .arrow::after,
	.bs-popover-left .arrow::after {
		border-left-color: #282A3C !important;
	}

	.popover-header {
		background-color: rgb(52, 55, 78) !important;
		border-bottom: 0px !important;
		color: white;
		max-width: 100% !important;
	}

	.popover-body {
		background-color: #282A3C !important;
		max-width: 100% !important;
	}

	.popover-body a {
		color: #a9aabf;
	}

	.popover-body a:hover {
		color: white;
		text-decoration: none;
	}

	.toast.toast-success {
		width: 400px !important;
	}

	.m-portlet__head-text {
		font-family: Poppins !Important;
	}

	pre.announcementPre {
		overflow-x: auto;
		white-space: pre-wrap;
		white-space: -moz-pre-wrap;
		white-space: -pre-wrap;
		white-space: -o-pre-wrap;
		word-wrap: break-word;
		font-family: Poppins;
		font-size: 14px;
	}

	.alert-announcement {
		border-left: none !important;
		border-top: none !important;
		border-bottom: none !important;
		border-right-style: solid !important;
		border-right-width: 5px !important;
		border-radius: 0 !important;
		color: unset;
	}

	.announcement-close {
		z-index: 2000;
		font-size: 30px;
		position: absolute;
		top: 1px;
		right: 16px;
	}

	.announcement-close:hover {
		text-shadow: 1px 1px 1px gray;
	}

	.announcement-close:focus {
		outline: none !Important;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="row">
			<!-- IMPORTANT -->

			<!-- Alert Icons & Action Buttons -->
			<!----Start Block of Announcement----->
			<div class="col-lg-12" style="display:none">
				<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" style="" role="alert">
					<div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
						<i class="fa fa-info-circle" style="font-size: 40px;color: white;"></i>
						<span></span>
					</div>
					<div class="m-alert__text" style="padding-bottom: 0px;">
						<h2>Announcement!!!</h2>
						<p>We will be having a System Maintenance this <b>February 24, 2018,
								Sunday </b>. With this, the system will be temporarily down and
							inaccessible from <b>1 PM - 10 PM </b>. For those who will be
							affected, we advise to file a TITO request later after the System
							will be up again.</p>
						<p>Thank you very much.</p>

						<blockquote class="blockquote pull-right" style="width: 280px;">
							<img src="<?php echo base_url("assets/sz-dance.gif"); ?>" style="width:15%;float:left">
							<footer class="blockquote-footer" style="margin-top: 10px;"><cite title="Announcement From">Software Programming Team</cite>
							</footer>
						</blockquote>

						<!-- ANOUNCEMENT HERE -->
					</div>
				</div>
			</div>
			<!----End Block of Announcement----->
			<div class="col-lg-12" id="approversDiv" style="display:none">
				<div class="alert alert-brand alert-dismissible fade show   m-alert m-alert--square m-alert--air" role="alert" id="pendingAppr">

					You currently have <strong id="pendingApprPopoverVal"></strong>
					pending request <span id="apprPendingWordForm"></span>.
					<button type="button" id="pendingApprPopover" class="btn btn-sm m-btn m-btn--icon m-btn--pill m-btn--wide" style="background:white;position: absolute;right: 10px;top: 8px;padding:4px 15px;color:#4153e6"><span><i class="fa fa-eye"></i><span>View Details</span></span></button>
				</div>
				<div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--square m-alert--air" role="alert" id="deadTodayAppr">
					You have <strong id="deadTodayApprPopoverVal"></strong> request <span id="apprDeadWordForm">approvals</span> that will be due by 11:59 PM
					today.
					<button type="button" id="deadTodayApprPopover" class="btn btn-sm m-btn m-btn--icon m-btn--pill m-btn--wide" style="background:white;position: absolute;right: 10px;top: 8px;padding:4px 15px;color:#F4516C"><span><i class="fa fa-eye"></i><span>View Details</span></span></button>
				</div>
				<!-- 	<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-warning fade show" style="color: #b77714;" role="alert" id="pendingAppr">
				<div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
					<i class="fa fa-info-circle" style="font-size: 40px;color: white;"></i>
					<span></span>
				</div>
				<div class="m-alert__text">
					You currently have <strong id="pendingApprPopoverVal"></strong>
					pending request <span id="apprPendingWordForm"></span>.
				</div>
				<div class="m-alert__actions" style="padding: 1.05rem 1.75rem 1.05rem 1.25rem !important;">
					<button type="button" id="pendingApprPopover" class="btn btn-sm m-btn--pill btn-outline-warning m-btn m-btn--custom m-btn--outline-2x">View
					Details</button>
				</div>
			</div> -->
				<!-- 	<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" style="color: #b77714;" role="alert" id="deadTodayAppr">
				<div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
					<i class="fa fa-exclamation-circle" style="font-size: 40px;color: white;"></i>
					<span></span>
				</div>
				<div class="m-alert__text">
					You have <strong id="deadTodayApprPopoverVal"></strong> request <span id="apprDeadWordForm">approvals</span> that will be due by 11:59 PM
					today.
				</div>
				<div class="m-alert__actions" style="padding: 1.05rem 1.75rem 1.05rem 1.25rem !important;">
					<button type="button" id="deadTodayApprPopover" class="btn btn-sm m-btn--pill btn-outline-danger m-btn m-btn--custom m-btn--outline-2x">View
					Details</button>
				</div>
			</div> -->

			</div>
			<div class="col-12 pb-2 mt-2">
				<div id="container-announcements" class="mb-4"></div>
			</div>
			<!-- END OF IMPORTANT -->
			<!---- START OF FIRST DIV ----->
			<div class="col-lg-4">
				<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
					<div class="m-portlet__head" style="background:#36A3F7">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title custom-txt-center">
								<span class="m-portlet__head-icon" style="color:white !important">
									<i class="fa    fa-birthday-cake"></i>
								</span>
								<h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;">
									Birthday Celebrants Today!
								</h5>
							</div>
						</div>
					</div>
					<div class="m-portlet__body" style="padding:15px;background:white">
						<div class="m-form__section m-form__section--first">
							<div class="m-scrollable" data-scrollable="true" data-max-height="225" id="divBday">
								<div class="m-widget4">
									<?php
									if (isset($celebrants) && count($celebrants) > 0) {
										foreach ($celebrants as $row => $val) {
											$bday = date_format(date_create($val["birthday"]), "F d, Y");
											?>
									<!--<p class="pCelebNames"> <i class="la la-info-circle <?php echo $val["gender"]; ?>" data-skin="dark" data-html="true" data-toggle="m-tooltip" data-placement="right" data-original-title="<div style='padding:10px 5px;!important'><span style='color:#ccc'>Fullname :</span><br /> <?php echo $val["fname"] . ' ' . $val["lname"]; ?> <br><br> <span style='color:#ccc'>Position :</span><br /> <small><?php echo $val["job"][0]->pos_details; ?></small> <br><br> <span style='color:#ccc'>Account :</span><br /> <small><?php echo $val["acc_name"]; ?></small> <br><br> <span style='color:#ccc'>Birthday :</span><br /> <small><?php echo $bday; ?></small></div>"></i><span class="m-topbar__userpic"> <img src="<?php echo base_url("/assets/images/" . $val["pic"]); ?>" class="m--img-rounded m--marginless m--img-centered header-userpic<?php echo $val["emp_id"] ?>" onerror="noImage(<?php echo $val["emp_id"]; ?>)"  alt=""></span> &nbsp;&nbsp;<?php echo $val["fname"] . ' ' . $val["lname"]; ?> </p>-->


									<div class="m-widget4__item pCelebNames">
										<div class="m-widget4__img m-widget4__img--logo">
										
											<img src="<?php echo base_url($val["pic"]); ?>" class="header-userpic<?php echo $val["emp_id"] ?>" alt="" onerror="noImage(<?php echo $val["emp_id"]; ?>)" style="width: 40px;">
										</div>
										<div class="m-widget4__info">
											<span class="m-widget4__title">
												<?php echo $val["fname"] . ' ' . $val["lname"]; ?>
											</span>
											</span>
										</div>
										<span class="m-widget4__ext">
											<span class="m-widget4__number">
												<i class="la la-info-circle <?php echo $val["gender"]; ?>" data-skin="dark" data-html="true" data-toggle="m-tooltip" data-placement="right" data-original-title="<div style='padding:10px 5px;!important'><span style='color:#ccc'>Fullname :</span><br /> <?php echo $val["fname"] . ' ' . $val["lname"]; ?> <br><br> <span style='color:#ccc'>Position :</span><br /> <small><?php echo $val["job"][0]->pos_details; ?></small> <br><br> <span style='color:#ccc'>Account :</span><br /> <small><?php echo $val["acc_name"]; ?></small> <br><br> <span style='color:#ccc'>Birthday :</span><br /> <small><?php echo $bday; ?></small></div>"></i>
											</span>
										</span>
									</div>
									<?php
										}
									} else {
										?>
									<!--<span style="font-size: x-large;margin: 0 auto;"><i class="fa fa-gears"></i> No Celebrants Today!</span>-->
									<div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A">
										<strong><i class="fa fa-asterisk"></i></strong> No
										Celebrants Today!
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
					<div class="m-portlet__head" style="background:#34BFA3">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title custom-txt-center">
								<span class="m-portlet__head-icon" style="color:white !important">
									<i class="fa fa-clock-o"></i>
								</span>
								<h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;">
									My Daily Time Record
								</h5>
							</div>
						</div>
					</div>
					<div class="m-portlet__body" style="padding:15px;background:white">
						<div class="m-form__section m-form__section--first">
							<div class="m-scrollable" data-scrollable="true" data-max-height="225">
								<div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A">
									<strong><i class="fa fa-gears"></i></strong> In Progress..
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!---- END OF FIRST DIV ----->

			<!---- START OF 2nd DIV ----->
			<div class="col-lg-4">
				<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit ">
					<div class="m-portlet__head" style="background:#666">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title custom-txt-center">
								<span class="m-portlet__head-icon" style="color:white !important">
									<i class="fa fa-hand-pointer-o"></i>
								</span>
								<h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;">
									My Requests
								</h5>
							</div>
						</div>
					</div>
					<div class="m-portlet__body" style="padding:15px;background:white">
						<div class="m-form__section m-form__section--first">
							<div class="m-scrollable" data-scrollable="true" data-max-height="225">
								<div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A">
									<strong><i class="fa fa-gears"></i></strong> In Progress..
								</div>

							</div>
						</div>
					</div>

				</div>
				<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
					<div class="m-portlet__head" style="background:#F4516C">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title custom-txt-center" data-toggle="m-tooltip" data-html="true" title="" data-placement="right" data-skin="dark" data-original-title="<small>Info with asterisk (*) are important fields that need to be updated.</small>">
								<span class="m-portlet__head-icon" style="color:white !important">
									<i class="fa fa-exclamation-circle"></i>
								</span>
								<h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;">
									Missing Information
								</h5>
							</div>
						</div>
					</div>
					<div class="m-portlet__body" style="padding:15px;background:white">
						<div class="m-form__section m-form__section--first">
							<div class="m-scrollable" data-scrollable="true" data-max-height="225">
								<table id="table-resolution" class="table table-bordered table-sm" style="display:none">
									<tbody></tbody>
								</table>

								<div class="alert m-alert m-alert--air m-alert--outline m-alert--outline-2x" role="alert" style="border:2px solid #7B7E8A;" id="alert-resolution">
									<strong><i class="fa fa-asterisk"></i></strong> No Missing
									Information
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!---- END OF 2nd DIV ----->
			<!---- START OF 3rd DIV ----->
			<div class="col-lg-4">
				<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
					<div class="m-portlet__head" style="background:#716ACA">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title custom-txt-center">
								<span class="m-portlet__head-icon" style="color:white !important">
									<i class="fa fa-user-circle-o"></i>
								</span>
								<h5 class="m-portlet__head-text" style="color:white !important;font-size:16px;">
									My Leave Details
								</h5>
							</div>
						</div>

					</div>
					<div class="m-portlet__body p-0" style="background:white">
						<div class="m-form__section m-form__section--first">
							<div class="m-scrollable" data-scrollable="true" data-max-height="550">
								<div id="div-nextLeave" class="card" style="border:0;background:#efeeff">
									<div class="card-body" style="padding: 1rem;border-style: dashed;border-width: 1px;border-color: #716aca;">
										<p class="mb-1 font-10 text-center m--font-bold">
											<u>UPCOMING LEAVE</u></p>
										<h4 class="text-center" id="text-date"></h4>
										<p class="text-center m--font-bolder" id="text-leavetype"></p>

									</div>
								</div>
								<div class="py-0 px-3">
									<div id="highchart-credits" style="height:200px"></div>
									<h5 class="text-center" style="font-size:18px"><?php echo date('Y'); ?> Leave Credits</h5>
									<table class="table m-table m-table--head-separator-primary table-sm text-center" style="font-size:12px;" id="table-credits">
										<thead class="thead-inverse">
											<tr>
												<th class="text-left">Leave Name</th>
												<th>Total</th>
												<th>Taken</th>
												<th>Remaining</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									<button type="button" class="btn btn-brand m-btn m-btn--custom m-btn--icon btn-block m--hide"><span class="pull-left">View More</span><i class="fa fa-arrow-right pull-right" style="margin-top: 8px;"></i></button>

									<hr />
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>
			<!---- END OF 3rd DIV ----->
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-preview-announcement" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body p-2 m-0">

			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/src/confetti/js/randomColor.js"></script>
<script src="<?php echo base_url(); ?>assets/src/confetti/js/confettiKit.js"></script>

<script>
	function ok() {
		new confettiKit({
			confettiCount: 100,
			angle: 90,
			startVelocity: 50,
			elements: {
				'confetti': {
					direction: 'down',
					rotation: true,
				},
				'star': {
					count: 10,
					direction: 'down',
					rotation: true,
				},
				'ribbon': {
					count: 5,
					direction: 'down',
					rotation: true,
				},
				'custom': [{
					count: 3,
					width: 50,
					textSize: 20,
					content: 'http://www.clker.com/cliparts/0/6/9/c/1194986736244974413balloon-red-aj.svg.thumb.png',
					contentType: 'image',
					direction: 'up',
					rotation: false,
				}]
			},

			position: 'topLeftRight',
		});
		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": true,
			"progressBar": false,
			"positionClass": "toast-top-center",
			"preventDuplicates": true,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "900",
			"timeOut": "5000",
			"extendedTimeOut": "11000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

		toastr.success("Care to Greet them?", "Happy Birthday to All Celebrant(s) Today.");

	}
</script>
<?php if (isset($celebrants)) {
	if (count($celebrants) > 0) {
		?>
<script>
	ok();
</script>
<?php }
} ?>
<script>
	$(function() {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('Announcement/dashboard_announcements'); ?>",
			cache: false,
			success: function(res) {
				res = res.trim();
				res = JSON.parse(res);
				$("#container-announcements").html("");
				$.each(res.announcements, function(i, ann) {
					var string = "";
					var highlightColor = (ann.highlightType == 'important') ? "#7BC043" : (ann.highlightType == 'info') ? "#0392CF" : (ann.highlightType == 'priority') ? "#716ACA" : (ann.highlightType == 'urgent') ? "#E6558B" : "#F7AB42";
					var highlightIcon = (ann.highlightType == 'important') ? "flaticon-confetti" : (ann.highlightType == 'info') ? "flaticon-information" : (ann.highlightType == 'priority') ? "flaticon-notes" : (ann.highlightType == 'urgent') ? "flaticon-alarm-1" : "flaticon-exclamation-1";

					string =
						'<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-dismissible fade show alert-announcement" role="alert"  style="box-shadow: 2px 3px 10px 1px #888787 !important;border-right-color:' + highlightColor + ' !important">' +
						'<div class="m-alert__icon py-0"  style="background:' + highlightColor + '">' +
						'<i class="' + highlightIcon + '" style="color:white"></i><p class="mt-3 m--font-bolder text-capitalize mb-0" style="color:white;font-size:12px;width:55px">' + ann.highlightType + '</p>' +
						'<span style="border-left-color:' + highlightColor + '"></span>' +
						'</div>' +
						'<div class="m-alert__text">' +
						'<h5 style="color:' + highlightColor + ';font-size:16px;" class="m--font-boldest mb-1">' + ann.subject + '</h5><p class="mb-0 mt-1"><span style="color:gray;font-size:12px;" class="m--font-bolder">' + moment(ann.startDate).format('MMMM DD, YYYY') + '&nbsp; | &nbsp;By: &nbsp;' + ann.announcer + '</span></p></div>' +
						'<div class="m-alert__actions" style="width: 150px;">' +
						'<button type="button" class="btn btn-sm m-btn m-btn--icon m-btn--pill m-btn--wide" style="background:' + highlightColor + ';color:white" data-toggle="modal" data-target="#modal-preview-announcement" data-backdrop="static" data-keyboard="false" data-previewid="' + ann.announcement_ID + '"><span><i class="fa fa-eye"></i><span>View Details</span></span></button>' +
						'</div>' +
						'</div>' +
						'</div>';
					$("#container-announcements").append(string);
					$("#container-announcements").find('button[data-previewid="' + ann.announcement_ID + '"]').data('obj', ann);
				})

				if ($("#container-announcements").html() === '') {
					$("#container-announcements").show();
					$("#container-announcements").hide();
				} else {
					$("#container-announcements").hide();
					$("#container-announcements").show();
				}
			}
		});
		$("#modal-preview-announcement").on('show.bs.modal', function(e) {
			if (e.namespace === 'bs.modal') {
				var ann = $(e.relatedTarget).data('obj');
				var highlightColor = (ann.highlightType == 'important') ? "#7BC043" : (ann.highlightType == 'info') ? "#0392CF" : (ann.highlightType == 'priority') ? "#716ACA" : (ann.highlightType == 'urgent') ? "#E6558B" : "#F7AB42";
				var highlightIcon = (ann.highlightType == 'important') ? "flaticon-confetti" : (ann.highlightType == 'info') ? "flaticon-information" : (ann.highlightType == 'priority') ? "flaticon-notes" : (ann.highlightType == 'urgent') ? "flaticon-alarm-1" : "flaticon-exclamation-1";

				var string = '<button type="button" class="close announcement-close" style="color:' + highlightColor + '" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><div class="m-0 m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-dismissible fade show alert-announcement" role="alert"  style="border-right-color:' + highlightColor + ' !important">' +
					'<div class="m-alert__icon"  style="background:' + highlightColor + '">' +
					'<i class="' + highlightIcon + '" style="color:white"></i><p class="mt-3 m--font-bolder text-capitalize" style="color:white;font-size:12px;width:55px">' + ann.highlightType + '</p>' +
					'<span style="border-left-color:' + highlightColor + '"></span>' +
					'</div>' +
					'<div class="m-alert__text">' +
					'<h5 style="color:' + highlightColor + ';font-size:16px;" class="m--font-boldest mb-1">' + ann.subject + '</h5><p class="mb-3 mt-1"><span style="color:gray;font-size:12px;" class="m--font-bolder">' + moment(ann.startDate).format('MMMM DD, YYYY') + '&nbsp; | &nbsp;By: &nbsp;' + ann.announcer + '</span></p> <pre class="announcementPre">' +
					ann.message + '</pre></div></div>'
				'</div>' +
				'</div>';
				$("#modal-preview-announcement").find('.modal-body').html(string);
			}
		});
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('Employee/getEmployeesForResolution/') . $this->session->uid; ?>",
			cache: false,
			success: function(res) {
				res = res.trim();
				res = JSON.parse(res);
				$("#table-resolution tbody").html("");
				var str = "";
				var names = res.names;
				var important = res.important;
				$.each(res.result, function(key, obj) {
					var currentName = (typeof names[key] ===
						'undefined') ? key : names[key];
					var importance = '';
					if (jQuery.inArray(key, important) !== -1) {
						importance =
							'<span class="m--font-danger m--font-bolder">*</span>';
					}
					str += '<tr>' +
						'<td style="font-size:13px" class="m--font-bold">' +
						currentName + ' ' + importance + '</td>' +
						'</tr>';
				});
				$("#table-resolution tbody").html(str);
				if (str === '') {
					$("#alert-resolution").show();
					$("#table-resolution").hide();
				} else {
					$("#alert-resolution").hide();
					$("#table-resolution").show();
				}
			}
		});
		//GET LEAVE CREDITS---------------------------------------------------------------------------
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>index.php/Dashboard/get_leave_credits",
			cache: false,
			success: function(res) {
				res = res.trim();
				res = JSON.parse(res);
				var nextLeave = res.nextLeave;
				if (nextLeave.length === 0) {
					$("#div-nextLeave").hide();
				} else {
					var moment_date = moment(nextLeave[0].date);

					if (moment_date.isSame(moment(), 'day')) {
						var formatted = "Today";
					} else if (moment_date.isSame(moment().add(1, 'day'))) {
						var formatted = "Tomorrow";
					} else {
						var formatted = moment_date.format('MMMM DD, YYYY');
					}
					$("#text-date").html(formatted);
					$("#text-leavetype").html(nextLeave[0].leaveType +
						' Leave');
					$("#div-nextLeave").show();
				}

				var leave_credits = res.leave_credits;
				var series = [];
				$.each(leave_credits, function(i, item) {
					var value = item.remaining;
					if (value === null) {
						value = 0;
					}
					series.push({
						name: item.leaveType,
						y: parseFloat(value)
					});
				});
				Highcharts.chart('highchart-credits', {
					chart: {
						type: 'pie',
						options3d: {
							enabled: true,
							alpha: 45
						}
					},
					title: null,
					subtitle: null,
					plotOptions: {
						pie: {
							innerSize: 65,
							depth: 40,
							dataLabels: {
								distance: 5
							}
						}
					},
					colors: ["#ffc855", "#e6558b", "#4f74ee"],
					series: [{
						name: 'Remaining Credits',
						data: series
					}]
				});
				$("#table-credits tbody").html("");
				var string = "";
				$.each(leave_credits, function(i, item) {
					var itemcredits = (item.credits === null) ?
						'No Data' : item.credits;
					var itemtaken = (item.taken === null) ? 'No Data' :
						item.taken;
					var itemremaining = (item.remaining === null) ?
						'No Data' : item.remaining;
					string += "<tr>";
					string += "<td class='text-left'>" + item
						.leaveType + " Leave</td>";
					string += "<td>" + itemcredits + "</td>";
					string += "<td>" + itemtaken + "</td>";
					string += "<td>" + itemremaining + "</td>";
					string += "</tr>";
				});
				$("#table-credits tbody").html(string);
			}
		});
		//END OF LEAVE CREDITS---------------------------------------------------------------------------

		//  GET PENDING APPROVALS ------------------------------------------------------------------------ 
		// var showApprovalNotice = 0;           
		getPendingAppr(function(pendingNoticeCount) {
			getTodayDeadAppr(function(deadNoticeCount) {
				if ((pendingNoticeCount > 0) || (deadNoticeCount > 0)) {
					$('#approversDiv').show();
					var title = "Approval Request Notice";
					var link = baseUrl + "/dashboard";
					var icon = baseUrl + "/assets/images/img/happy.gif";
					var appr = "Approval";
					if (pendingNoticeCount > 1) {
						appr = "Approvals";
					}
					if (pendingNoticeCount == deadNoticeCount) {
						var body = "You currently have " +
							pendingNoticeCount + " Pending " + appr +
							" which will be due by 11:59 PM today.";
					} else {
						if ((pendingNoticeCount > 0) && (deadNoticeCount ==
								0)) {
							var body = "You currently have " +
								pendingNoticeCount + " Pending " + appr +
								" as of the moment.";
						} else {
							var body = "You currently have " +
								pendingNoticeCount + " Pending " + appr +
								". " + deadNoticeCount +
								" out of it will by due by 11:59 PM today.";
						}
					}
					showDeskopNotif(title, body, icon, link);
				} else {
					$('#approversDiv').hide();
				}
			})
		})



		// END OF PENDING APPROVALS ---------------------------------------------------------------------
	});
</script>