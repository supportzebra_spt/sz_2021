<style>
.carousel-item img{
    background:white;
    height:240px;
    object-fit: contain;
}
.custom-txt-center {
    /* margin: 0 auto; */
}

.font-11 {
    font-size: 11px;
}

.pCelebNames .Male {
    font-size: 1.35em;
    color: #bee3f5;
}

.pCelebNames .Female {
    font-size: 1.35em;
    color: #f4c2c2;
}

.pCelebNames {
    font-size: smaller;
}

.tooltip-inner {
    text-align: left;
    color: #fff !important;
    font-size: 1em !important;
}

.highcharts-exporting-group,
.highcharts-credits {
    display: none
}

.m-portlet__head {
    height: 50px !important;
}

.m-portlet__body {
    background: white !important;
}

.highcharts-title {
    font-family: 'Poppins' !important;
}

.bs-popover-auto[x-placement^=right] .arrow::after,
.bs-popover-right .arrow::after {
    border-right-color: #282A3C !important;
}

.bs-popover-auto[x-placement^=left] .arrow::after,
.bs-popover-left .arrow::after {
    border-left-color: #282A3C !important;
}

.popover-header {
    background-color: rgb(52, 55, 78) !important;
    border-bottom: 0px !important;
    color: white;
    max-width: 100% !important;
}

.popover-body {
    background-color: #282A3C !important;
    max-width: 100% !important;
}

.popover-body a {
    color: #a9aabf;
}

.popover-body a:hover {
    color: white;
    text-decoration: none;
}

.toast.toast-success {
    width: 400px !important;
}

.m-portlet__head-text {
    font-family: Poppins !Important;
}

pre.announcementPre {
    overflow-x: auto;
    white-space: pre-wrap;
    white-space: -moz-pre-wrap;
    white-space: -pre-wrap;
    white-space: -o-pre-wrap;
    word-wrap: break-word;
    font-family: Poppins;
    font-size: 14px;
}

.alert-announcement {
    border-left: none !important;
    border-top: none !important;
    border-bottom: none !important;
    border-right-style: solid !important;
    border-right-width: 5px !important;
    border-radius: 0 !important;
    color: unset;
}

.announcement-close {
    z-index: 2000;
    font-size: 30px;
    position: absolute;
    top: 1px;
    right: 16px;
}

.announcement-close:hover {
    text-shadow: 1px 1px 1px gray;
}

.announcement-close:focus {
    outline: none !Important;
}

.m-portlet__head {
    padding: 10px 16px !important;
}

.lighbox-img img:hover {
    border: 2px solid #36A3F7;
    border-radius: 100%;

}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <!-- IMPORTANT -->

            <!-- Alert Icons & Action Buttons -->
            <!----Start Block of Announcement----->
            <div class="col-lg-12" style="display:none">
                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show"
                    style="" role="alert">
                    <div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
                        <i class="fa fa-info-circle" style="font-size: 40px;color: white;"></i>
                        <span></span>
                    </div>
                    <div class="m-alert__text" style="padding-bottom: 0px;">
                        <h2>Announcement!!!</h2>
                        <p>We will be having a System Maintenance this <b>February 24, 2018,
                                Sunday </b>. With this, the system will be temporarily down and
                            inaccessible from <b>1 PM - 10 PM </b>. For those who will be
                            affected, we advise to file a TITO request later after the System
                            will be up again.</p>
                        <p>Thank you very much.</p>

                        <blockquote class="blockquote pull-right" style="width: 280px;">
                            <img src="<?php echo base_url("assets/sz-dance.gif"); ?>" style="width:15%;float:left">
                            <footer class="blockquote-footer" style="margin-top: 10px;"><cite
                                    title="Announcement From">Software Programming Team</cite>
                            </footer>
                        </blockquote>

                        <!-- ANOUNCEMENT HERE -->
                    </div>
                </div>
            </div>
            <!----End Block of Announcement----->
            <div class="col-lg-12" id="approversDiv" style="display:none">
                <div class="alert alert-brand alert-dismissible fade show   m-alert m-alert--square m-alert--air"
                    role="alert" id="pendingAppr">

                    You currently have <strong id="pendingApprPopoverVal"></strong>
                    pending request <span id="apprPendingWordForm"></span>.
                    <button type="button" id="pendingApprPopover"
                        class="btn btn-sm m-btn m-btn--icon m-btn--pill m-btn--wide"
                        style="background:white;position: absolute;right: 10px;top: 8px;padding:4px 15px;color:#4153e6"><span><i
                                class="fa fa-eye"></i><span>View Details</span></span></button>
                </div>
                <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--square m-alert--air"
                    role="alert" id="deadTodayAppr">
                    You have <strong id="deadTodayApprPopoverVal"></strong> request <span
                        id="apprDeadWordForm">approvals</span> that will be due by 11:59 PM
                    today.
                    <button type="button" id="deadTodayApprPopover"
                        class="btn btn-sm m-btn m-btn--icon m-btn--pill m-btn--wide"
                        style="background:white;position: absolute;right: 10px;top: 8px;padding:4px 15px;color:#F4516C"><span><i
                                class="fa fa-eye"></i><span>View Details</span></span></button>
                </div>
                <!-- 	<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-warning fade show" style="color: #b77714;" role="alert" id="pendingAppr">
				<div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
					<i class="fa fa-info-circle" style="font-size: 40px;color: white;"></i>
					<span></span>
				</div>
				<div class="m-alert__text">
					You currently have <strong id="pendingApprPopoverVal"></strong>
					pending request <span id="apprPendingWordForm"></span>.
				</div>
				<div class="m-alert__actions" style="padding: 1.05rem 1.75rem 1.05rem 1.25rem !important;">
					<button type="button" id="pendingApprPopover" class="btn btn-sm m-btn--pill btn-outline-warning m-btn m-btn--custom m-btn--outline-2x">View
					Details</button>
				</div>
			</div> -->
                <!-- 	<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" style="color: #b77714;" role="alert" id="deadTodayAppr">
				<div class="m-alert__icon" style="padding: 0.45rem 0.75rem;">
					<i class="fa fa-exclamation-circle" style="font-size: 40px;color: white;"></i>
					<span></span>
				</div>
				<div class="m-alert__text">
					You have <strong id="deadTodayApprPopoverVal"></strong> request <span id="apprDeadWordForm">approvals</span> that will be due by 11:59 PM
					today.
				</div>
				<div class="m-alert__actions" style="padding: 1.05rem 1.75rem 1.05rem 1.25rem !important;">
					<button type="button" id="deadTodayApprPopover" class="btn btn-sm m-btn--pill btn-outline-danger m-btn m-btn--custom m-btn--outline-2x">View
					Details</button>
				</div>
			</div> -->

            </div>
            <div class="col-12 pb-2 mt-2">
                <div id="container-announcements" class="mb-4"></div>
            </div>
            <!-- END OF IMPORTANT -->
            <!---- START OF FIRST DIV ----->

            <div class="col-xl-4 col-md-6 col-sm-6 col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
                    <div class="m-portlet__head" style="background:#36A3F7">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-birthday-cake"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:14px;">
                                    Birthday Celebrants Today!
                                </h5>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <span
                                class="m-badge m--font-info m--font-boldest  bg-white"><?php echo (isset($celebrants)) ? count($celebrants) : 0; ?></span>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding:15px;background:white">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="225" id="divBday">
                                <div class="m-widget4">
                                    <?php
if (isset($celebrants) && count($celebrants) > 0) {
    foreach ($celebrants as $row => $val) {
        $bday = date_format(date_create($val["birthday"]), "F d, Y");
        ?>
                                    <!--<p class="pCelebNames"> <i class="la la-info-circle <?php echo $val["gender"]; ?>" data-skin="dark" data-html="true" data-toggle="m-tooltip" data-placement="right" data-original-title="<div style='padding:10px 5px;!important'><span style='color:#ccc'>Fullname :</span><br /> <?php echo $val["fname"] . ' ' . $val["lname"]; ?> <br><br> <span style='color:#ccc'>Position :</span><br /> <small><?php echo $val["job"][0]->pos_details; ?></small> <br><br> <span style='color:#ccc'>Account :</span><br /> <small><?php echo $val["acc_name"]; ?></small> <br><br> <span style='color:#ccc'>Birthday :</span><br /> <small><?php echo $bday; ?></small></div>"></i><span class="m-topbar__userpic"> <img src="<?php echo base_url("/assets/images/" . $val["pic"]); ?>" class="m--img-rounded m--marginless m--img-centered header-userpic<?php echo $val["emp_id"] ?>" onerror="noImage(<?php echo $val["emp_id"]; ?>)"  alt=""></span> &nbsp;&nbsp;<?php echo $val["fname"] . ' ' . $val["lname"]; ?> </p>-->


                                    <div class="m-widget4__item pCelebNames py-2">
                                        <div class="m-widget4__img m-widget4__img--logo">
                                            <a class="lighbox-img" href="<?php echo base_url($val["pic"]); ?>"
                                                data-lightbox="roadtrip"
                                                data-title="<?php echo $val["fname"] . ' ' . $val["lname"]; ?>">
                                                <img src="<?php echo base_url($val["pic"]); ?>"
                                                    onerror="this.onerror=null;this.src='<?php echo base_url('assets/images/img/sz_thumbnail.png'); ?>';$(this).parent().attr('href','<?php echo base_url('assets/images/img/sz.png'); ?>')"
                                                    style="width: 40px;">
                                            </a>

                                        </div>
                                        <div class="m-widget4__info">
                                            <p class="m-widget4__title mb-0" style="font-size:15px">
                                                <?php echo $val["lname"]; ?>
                                            </p>
                                            <p class="m-widget4__sub mb-0" style="font-size:12px;font-weight:400">
                                                <?php echo $val["fname"]; ?>
                                            </p>
                                        </div>
                                        <span class="m-widget4__ext">
                                            <span class="m-widget4__number">
                                                <i class="fa fa-info-circle <?php echo ucfirst($val["gender"]); ?>"
                                                    data-skin="dark" data-html="true" data-toggle="m-tooltip"
                                                    data-placement="right"
                                                    data-original-title="<div style='padding:10px 5px;!important'><span style='color:#ccc'>Fullname :</span><br /> <?php echo $val["fname"] . ' ' . $val["lname"]; ?> <br><br> <span style='color:#ccc'>Position :</span><br /> <small><?php echo $val["job"][0]->pos_details; ?></small> <br><br> <span style='color:#ccc'>Account :</span><br /> <small><?php echo $val["acc_name"]; ?></small></div>"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <?php
}
} else {
    ?>
                                    <div class="m-alert m-alert--icon alert" role="alert"
                                        style="background: #FFF2D0;color: #856927;">
                                        <div class="m-alert__icon" style="padding:0px 15px">
                                            <i class="fa fa-asterisk" style="font-size:16px"></i>
                                        </div>
                                        <div class="m-alert__text pr-3"
                                            style="padding:10px 0;font-size:12px;font-weight:400">
                                            <strong>No birthday celebrants for today</strong>
                                        </div>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
                    <div class="m-portlet__head" style="background:#F4516C">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center" data-toggle="m-tooltip"
                                data-html="true" title="" data-placement="right" data-skin="dark"
                                data-original-title="<small>Info with asterisk (*) are important fields that need to be updated.</small>">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-warning"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:14px;">
                                    Missing Information
                                </h5>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <span class="m-badge m--font-danger m--font-boldest bg-white"
                                id="table-resolution-count"></span>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding:15px;background:white">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="225">
                                <table id="table-resolution" class="table table-bordered table-sm" style="display:none">
                                    <tbody></tbody>
                                </table>
                                <div class="m-alert m-alert--icon alert" role="alert"
                                    style="background: #FFF2D0;color: #856927;display:none" id="alert-resolution">
                                    <div class="m-alert__icon" style="padding:0px 15px">
                                        <i class="fa fa-asterisk" style="font-size:16px"></i>
                                    </div>
                                    <div class="m-alert__text pr-3"
                                        style="padding:10px 0;font-size:12px;font-weight:400">
                                        <strong>No missing information</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!---- END OF FIRST DIV ----->

            <!---- START OF 2nd DIV ----->
            <div class="col-xl-4 col-md-6 col-sm-6 col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit ">
                    <div class="m-portlet__head" style="background:#666">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-pencil-square-o"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:14px;">
                                    Request Notifications
                                </h5>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <span class="m-badge m--font-dark m--font-boldest bg-white"
                                id="dash-myrequest-total"></span>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding:5px 15px;background:white">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="245">
                                <div class="m-alert m-alert--icon alert" role="alert"
                                    style="background: #FFF2D0;color: #856927;display:none;margin-top:10px"
                                    id="alert-myrequest">
                                    <div class="m-alert__icon" style="padding:0px 15px">
                                        <i class="fa fa-asterisk" style="font-size:16px"></i>
                                    </div>
                                    <div class="m-alert__text pr-3"
                                        style="padding:10px 0;font-size:12px;font-weight:400">
                                        <strong>No request notifications</strong>
                                    </div>
                                </div>
                                <div id="approvalChart" style="height:245px"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height  m-portlet--fit  ">
                    <div class="m-portlet__head" style="background:#34BFA3">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-clock-o"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:14px;">
                                    My Schedules
                                </h5>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body" style="padding:10px 15px;background:white">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="230">

                                <div class="mb-2" id="div-schedules-today" style="display:none">
                                    <p class="m--font-bolder mb-2"
                                        style="width: 100%;  border-bottom: 1px solid #ccc;line-height: 0.1em;margin: 10px 0 0px;color:gray">
                                        <span
                                            style="background:#fff;padding:0px;padding-right:10px;font-size:12px"><span>Today</span><span
                                                id="today-date"></span></span></p>

                                    <div class="m-widget4"></div>
                                </div>
                                <div class="mb-2" id="div-schedules-tomorrow" style="display:none">
                                    <p class="m--font-bolder mb-2"
                                        style="width: 100%;  border-bottom: 1px solid #ccc;line-height: 0.1em;margin: 10px 0 0px;color:gray">
                                        <span
                                            style="background:#fff;padding:0px;padding-right:10px;font-size:12px"><span>Tomorrow</span><span
                                                id="tomorrow-date"></span></span></p>

                                    <div class="m-widget4"></div>
                                </div>
                                <div class="mb-2" id="div-leaveschedules" style="display:none">
                                    <p class="m--font-bolder mb-2"
                                        style="width: 100%;  border-bottom: 1px solid #ccc;line-height: 0.1em;margin: 10px 0 0px;color:gray">
                                        <span
                                            style="background:#fff;padding:0px;padding-right:10px;font-size:12px">Upcoming
                                            Leave</span></p>

                                    <div class="m-widget4">
                                        <!--begin::Widget 14 Item-->
                                        <div class="m-widget4__item pb-0 pt-2">
                                            <div class="m-widget4__ext">
                                                <span class="m-widget4__icon">
                                                    <i class="flaticon-event-calendar-symbol m--font-success"
                                                        style="font-size:35px;"></i>
                                                </span>
                                            </div>
                                            <div class="m-widget4__info">
                                                <p class="m-widget4__title mb-0" id="text-date"
                                                    style="font-size:18px;font-weight:403;">
                                                </p>
                                                <p class="m-widget4__sub mb-0" id="text-leavetype"
                                                    style="font-size:12px;font-weight:401">
                                                </p>
                                            </div>
                                        </div>
                                        <!--end::Widget 14 Item-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
            <!---- END OF 2nd DIV ----->
            <!---- START OF 3rd DIV ----->
            <div class="col-xl-4 col-12">
                <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                    <div class="m-portlet__head" style="background:#716ACA">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title custom-txt-center">
                                <span class="m-portlet__head-icon" style="color:white !important">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <h5 class="m-portlet__head-text" style="color:white !important;font-size:14px;">
                                    My Leave Details
                                </h5>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__body p-0" style="background:white">
                        <div class="m-form__section m-form__section--first">
                            <div class="m-scrollable" data-scrollable="true" data-max-height="600">

                                <div class="py-0 px-3">
                                    <div id="highchart-credits" style="height:180px"></div>
                                    <h5 class="text-center" style="font-size:18px"><?php echo date('Y'); ?> Leave
                                        Credits</h5>
                                    <table class="table m-table m-table--head-separator-primary table-sm text-center"
                                        style="font-size:12px;" id="table-credits">
                                        <thead class="thead-inverse">
                                            <tr>
                                                <th class="text-left">Leave Name</th>
                                                <th>Total</th>
                                                <th>Taken</th>
                                                <th>Remaining</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <button type="button"
                                        class="btn btn-brand m-btn m-btn--custom m-btn--icon btn-block m--hide"><span
                                            class="pull-left">View More</span><i class="fa fa-arrow-right pull-right"
                                            style="margin-top: 8px;"></i></button>

                                    <hr />
                                    <a href="<?php echo base_url('Company/home')?>">
                                     <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                      
                                      <div class="carousel-inner">
                                        <div class="carousel-item active">
                                           <img src="<?php echo base_url('assets/images/img/corevalues/Helix.png') ?>"  class="d-block w-100" style="background: black">
                                        </div>
                                        <div class="carousel-item ">
                                           <img src="<?php echo base_url('assets/images/img/corevalues/mission.jpg') ?>" class="d-block w-100">
                                        </div>
                                        <div class="carousel-item ">
                                           <img src="<?php echo base_url('assets/images/img/corevalues/VISION.jpg') ?>" class="d-block w-100">
                                        </div>
                                        <div class="carousel-item ">
                                            <img src="<?php echo base_url('assets/images/img/corevalues/Core.jpg') ?>"  class="d-block w-100">
                                        </div>
                                        <div class="carousel-item">
                                           <img src="<?php echo base_url('assets/images/img/corevalues/Family.jpg') ?>"  class="d-block w-100">
                                        </div>
                                        <div class="carousel-item">
                                           <img src="<?php echo base_url('assets/images/img/corevalues/Wow.jpg') ?>"  class="d-block w-100">
                                        </div>
                                        <div class="carousel-item">
                                           <img src="<?php echo base_url('assets/images/img/corevalues/Integrity.jpg') ?>"  class="d-block w-100">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="<?php echo base_url('assets/images/img/corevalues/Positivity.jpg') ?>"  class="d-block w-100">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="<?php echo base_url('assets/images/img/corevalues/Generosity.jpg') ?>"  class="d-block w-100">
                                        </div>
                                      </div>
                                    </div>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!---- END OF 3rd DIV ----->
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-preview-announcement" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body p-2 m-0">

            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/src/confetti/js/randomColor.js"></script>
<script src="<?php echo base_url(); ?>assets/src/confetti/js/confettiKit.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/lightbox2/dist/css/lightbox.css" />
<script src="<?php echo base_url(); ?>node_modules/lightbox2/dist/js/lightbox.js"></script>
<script>
function ok() {
    new confettiKit({
        confettiCount: 100,
        angle: 90,
        startVelocity: 50,
        elements: {
            'confetti': {
                direction: 'down',
                rotation: true,
            },
            'star': {
                count: 10,
                direction: 'down',
                rotation: true,
            },
            'ribbon': {
                count: 5,
                direction: 'down',
                rotation: true,
            },
            'custom': [{
                count: 3,
                width: 50,
                textSize: 20,
                content: 'http://www.clker.com/cliparts/0/6/9/c/1194986736244974413balloon-red-aj.svg.thumb.png',
                contentType: 'image',
                direction: 'up',
                rotation: false,
            }]
        },

        position: 'topLeftRight',
    });
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "900",
        "timeOut": "5000",
        "extendedTimeOut": "11000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    toastr.success("Care to Greet them?", "Happy Birthday to All Celebrant(s) Today.");

}
</script>
<?php if (isset($celebrants)) {
    if (count($celebrants) > 0) {
        ?>
<script>
ok();
</script>
<?php }
}?>
<script>
$(function() {
     $('#myCarousel').carousel({
      interval: 5000
    });
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Announcement/dashboard_announcements'); ?>",
        cache: false,
        success: function(res) {
            res = res.trim();
            res = JSON.parse(res);
            $("#container-announcements").html("");
            $.each(res.announcements, function(i, ann) {
                var string = "";
                var highlightColor = (ann.highlightType == 'important') ? "#7BC043" : (ann
                        .highlightType == 'info') ? "#0392CF" : (ann.highlightType ==
                        'priority') ? "#716ACA" : (ann.highlightType == 'urgent') ?
                    "#E6558B" : "#F7AB42";
                var highlightIcon = (ann.highlightType == 'important') ?
                    "flaticon-confetti" : (ann.highlightType == 'info') ?
                    "flaticon-information" : (ann.highlightType == 'priority') ?
                    "flaticon-notes" : (ann.highlightType == 'urgent') ?
                    "flaticon-alarm-1" : "flaticon-exclamation-1";

                string =
                    '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-dismissible fade show alert-announcement" role="alert"  style="box-shadow: 0 1px 10px 1px #ccc !important;border-right-color:' +
                    highlightColor + ' !important;">' +
                    '<div class="m-alert__icon py-0"  style="background:' + highlightColor +
                    '">' +
                    '<i class="' + highlightIcon +
                    '" style="color:white"></i><p class="mt-3 m--font-bolder text-capitalize mb-0" style="color:white;font-size:12px;width:55px">' +
                    ann.highlightType + '</p>' +
                    '<span style="border-left-color:' + highlightColor + '"></span>' +
                    '</div>' +
                    '<div class="m-alert__text">' +
                    '<h5 style="color:' + highlightColor +
                    ';font-size:16px;" class="m--font-boldest mb-1">' + ann.subject +
                    '</h5><p class="mb-0 mt-1"><span style="color:gray;font-size:12px;" class="m--font-bolder">' +
                    moment(ann.startDate).format('MMMM DD, YYYY') +
                    '&nbsp; | &nbsp;By: &nbsp;' + ann.announcer + '</span></p></div>' +
                    '<div class="m-alert__actions" style="width: 150px;">' +
                    '<button type="button" class="btn btn-sm m-btn m-btn--icon m-btn--pill m-btn--wide" style="background:' +
                    highlightColor +
                    ';color:white" data-toggle="modal" data-target="#modal-preview-announcement" data-backdrop="static" data-keyboard="false" data-previewid="' +
                    ann.announcement_ID +
                    '"><span><i class="fa fa-eye"></i><span>View Details</span></span></button>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                $("#container-announcements").append(string);
                $("#container-announcements").find('button[data-previewid="' + ann
                    .announcement_ID + '"]').data('obj', ann);
            })

            if ($("#container-announcements").html() === '') {
                $("#container-announcements").show();
                $("#container-announcements").hide();
            } else {
                $("#container-announcements").hide();
                $("#container-announcements").show();
            }
        }
    });
    $("#modal-preview-announcement").on('show.bs.modal', function(e) {
        if (e.namespace === 'bs.modal') {
            var ann = $(e.relatedTarget).data('obj');
            var highlightColor = (ann.highlightType == 'important') ? "#7BC043" : (ann.highlightType ==
                'info') ? "#0392CF" : (ann.highlightType == 'priority') ? "#716ACA" : (ann
                .highlightType == 'urgent') ? "#E6558B" : "#F7AB42";
            var highlightIcon = (ann.highlightType == 'important') ? "flaticon-confetti" : (ann
                    .highlightType == 'info') ? "flaticon-information" : (ann.highlightType ==
                    'priority') ? "flaticon-notes" : (ann.highlightType == 'urgent') ?
                "flaticon-alarm-1" : "flaticon-exclamation-1";

            var string = '<button type="button" class="close announcement-close" style="color:' +
                highlightColor +
                '" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><div class="m-0 m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-dismissible fade show alert-announcement" role="alert"  style="border-right-color:' +
                highlightColor + ' !important">' +
                '<div class="m-alert__icon"  style="background:' + highlightColor + '">' +
                '<i class="' + highlightIcon +
                '" style="color:white"></i><p class="mt-3 m--font-bolder text-capitalize" style="color:white;font-size:12px;width:55px">' +
                ann.highlightType + '</p>' +
                '<span style="border-left-color:' + highlightColor + '"></span>' +
                '</div>' +
                '<div class="m-alert__text">' +
                '<h5 style="color:' + highlightColor +
                ';font-size:16px;" class="m--font-boldest mb-1">' + ann.subject +
                '</h5><p class="mb-3 mt-1"><span style="color:gray;font-size:12px;" class="m--font-bolder">' +
                moment(ann.startDate).format('MMMM DD, YYYY') + '&nbsp; | &nbsp;By: &nbsp;' + ann
                .announcer + '</span></p> <pre class="announcementPre">' +
                ann.message + '</pre></div></div>'
            '</div>' +
            '</div>';
            $("#modal-preview-announcement").find('.modal-body').html(string);
        }
    });
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Employee/getEmployeesForResolution/') . $this->session->uid; ?>",
        cache: false,
        success: function(res) {
            res = res.trim();
            res = JSON.parse(res);
            $("#table-resolution tbody").html("");
            var str = "";
            var names = res.names;
            var important = res.important;
            $.each(res.result, function(key, obj) {
                var currentName = (typeof names[key] ===
                    'undefined') ? key : names[key];
                var importance = '';
                if (jQuery.inArray(key, important) !== -1) {
                    importance =
                        '<span class="m--font-danger m--font-bolder">*</span>';
                }
                str += '<tr>' +
                    '<td style="font-size:13px" class="m--font-bold">' +
                    currentName + ' ' + importance + '</td>' +
                    '</tr>';
            });
            $("#table-resolution-count").html(res.total);
            $("#table-resolution tbody").html(str);
            if (str === '') {
                $("#alert-resolution").show();
                $("#table-resolution").hide();
            } else {
                $("#alert-resolution").hide();
                $("#table-resolution").show();
            }
        }
    });
    //GET LEAVE CREDITS---------------------------------------------------------------------------
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>index.php/Dashboard/get_leave_credits",
        cache: false,
        success: function(res) {
            res = res.trim();
            res = JSON.parse(res);
            var nextLeave = res.nextLeave;

            //FOR THE SCHEDULES DISPLAY
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('Dashboard/get_current_schedule') ?>",
                cache: false,
                success: function(res2) {
                    res2 = res2.trim();
                    res2 = JSON.parse(res2);
                    $("#div-schedules-today div").html("");
                    $("#div-schedules-tomorrow div").html("");
                    $("#today-date").html(', ' + moment().format('ll'));
                    $("#tomorrow-date").html(', ' + moment().add(1, 'day').format(
                        'll'));
                    var today = formatScheduleDisplay(res2.today);
                    var tomorrow = formatScheduleDisplay(res2.tomorrow);
                    $("#div-schedules-today div").html(today);
                    $("#div-schedules-tomorrow div").html(tomorrow);
                    $("#div-schedules-today").show();
                    $("#div-schedules-tomorrow").show();
                    if (nextLeave.length === 0) {
                        $("#div-leaveschedules").hide();
                    } else {
                        var moment_date = moment(nextLeave[0].date);
                        var formatted = moment_date.format('MMMM DD, YYYY');
                        $("#text-date").html(formatted);
                        $("#text-leavetype").html(nextLeave[0].leaveType + ' Leave');
                        $("#div-leaveschedules").show();
                    }
                }
            });

            var leave_credits = res.leave_credits;
            var series = [];
            $.each(leave_credits, function(i, item) {
                var value = item.remaining;
                if (value === null) {
                    value = 0;
                }
                series.push({
                    name: item.leaveType,
                    y: parseFloat(value)
                });
            });
            Highcharts.chart('highchart-credits', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45
                    }
                },
                title: null,
                subtitle: null,
                plotOptions: {
                    pie: {
                        innerSize: 65,
                        depth: 40,
                        dataLabels: {
                            distance: 5
                        }
                    }
                },
                colors: ["#ffc855", "#e6558b", "#4f74ee"],
                series: [{
                    name: 'Remaining Credits',
                    data: series
                }]
            });
            $("#table-credits tbody").html("");
            var string = "";
            $.each(leave_credits, function(i, item) {
                var itemcredits = (item.credits === null) ?
                    'No Data' : item.credits;
                var itemtaken = (item.taken === null) ? 'No Data' :
                    item.taken;
                var itemremaining = (item.remaining === null) ?
                    'No Data' : item.remaining;
                string += "<tr>";
                string += "<td class='text-left'>" + item
                    .leaveType + " Leave</td>";
                string += "<td>" + itemcredits + "</td>";
                string += "<td>" + itemtaken + "</td>";
                string += "<td>" + itemremaining + "</td>";
                string += "</tr>";
            });
            $("#table-credits tbody").html(string);
        }
    });

    //FOR MY DAILY TIME RECORD
    function formatScheduleDisplay(data) {
        var string = "";
        if (data.length == 0) {
            string += '<div class="m-alert m-alert--icon alert mt-3" role="alert" style="background: #FFF2D0;color: #856927;">\
							<div class="m-alert__icon" style="padding:0px 15px">\
								<i class="fa fa-asterisk" style="font-size:16px"></i>\
							</div>\
							<div class="m-alert__text pr-3" style="padding:10px 0;font-size:12px;font-weight:400">\
							  	<strong>No Schedule Set</strong>\
							</div>\
						</div>';
        } else {
            $.each(data, function(i, item) {
                string += '<div class="m-widget4__item py-1">\
                        <div class="m-widget4__ext">\
                            <span class="m-widget4__icon">\
                                <i class="flaticon-stopwatch" style="font-size:35px;color:' + item.style + '"></i>\
                            </span>\
                        </div>\
                        <div class="m-widget4__info">';
                if (item.schedtype_id === '3' || item.schedtype_id === '8') { //LEave-WP and Leave-WOP
                    string += '<p class="mb-0" style="font-size:12px;font-weight:400">' + item.type + '</p>\
                        <h4 class="mb-0" style="font-size:18px">' + item.leaveType + ' Leave</h4>';
                } else if (item.time_start !== null) {
                    var timestart = moment('2020-01-01' + ' ' + item.time_start, 'YYYY-MM-DD h:mm:ss A')
                        .format("hh:mma");
                    var timeend = moment('2020-01-01' + ' ' + item.time_end, 'YYYY-MM-DD h:mm:ss A')
                        .format("hh:mma");
                    string += '<div class="row">\
                                <div class="col-6">\
                                    <p class="mb-0" style="font-size:12px;font-weight:400">Shift In</p>\
                                    <h4 class="mb-0" style="font-size:18px;">' + timestart + '</h4>\
                                </div>\
                                <div class="col-6">\
                                    <p class="mb-0" style="font-size:12px;font-weight:400">Shift Out</p>\
                                    <h4 class="mb-0" style="font-size:18px;">' + timeend + '</h4>\
                                </div>\
                            </div>';
                } else {
                    string += '<p class="mb-0" style="font-size:12px;font-weight:400">Schedule</p>\
                        <h4 class="mb-0" style="font-size:18px;">' + item.type + '</h4>';
                }
                string += '</div></div>';
            });
        }

        return string;
    }

    //END OF LEAVE CREDITS---------------------------------------------------------------------------

    //=========CHART VERSION OF THE NOTIFICATIONS FOR APPROVAL=============
    $.when(fetchGetData('/general/get_pending_appr_counts'), fetchGetData('/general/get_approval_links'),
        fetchGetData('/general/get_deadline_today_appr_counts'), fetchGetData(
            '/general/get_my_pending_request_count')).then(
        function(pendingApprCount, apprLinks, todayDeadApprCount, personalApprCount) {
            var pendingApprCountObj = jQuery.parseJSON(pendingApprCount[0].trim());
            var todayDeadApprCountObj = jQuery.parseJSON(todayDeadApprCount[0].trim());
            var personalApprCountObj = jQuery.parseJSON(personalApprCount[0].trim());
            var apprLinksObj = jQuery.parseJSON(apprLinks[0].trim());

            var totalPendingAppr = parseInt(pendingApprCountObj.ahr_appr_pending) + parseInt(
                pendingApprCountObj
                .dtr_appr_pending) + parseInt(pendingApprCountObj.leave_appr_pending) + parseInt(
                pendingApprCountObj.leave_retract_appr_pending);
            var todayDeadAppr = parseInt(todayDeadApprCountObj.ahr_appr_today) + parseInt(
                todayDeadApprCountObj
                .dtr_appr_today) + parseInt(todayDeadApprCountObj.leave_appr_today) + parseInt(
                todayDeadApprCountObj.leave_retract_appr_today);
            var personalAppr = parseInt(personalApprCountObj.ahr) + parseInt(personalApprCountObj
                .tito) + parseInt(personalApprCountObj.leave) + parseInt(
                personalApprCountObj.leave_retract);

            var pending_total = totalPendingAppr;
            var deadline_total = todayDeadAppr;
            var personal_total = personalAppr; //kulang

            $("#dash-myrequest-total").html(pending_total + deadline_total + personal_total);

            var ahr_url = baseUrl + '/' + apprLinksObj.ahr_appr_link;
            var leave_url = baseUrl + '/' + apprLinksObj.leave_appr_link;
            var leave_retraction_url = baseUrl + '/' + apprLinksObj.leave_retract_appr_link;
            var tito_url = baseUrl + '/' + apprLinksObj.dtr_appr_link;

            var ahr_url_2 = baseUrl + '/additional_hour/request';
            var leave_retraction_url_2 = baseUrl + '/Leave/retraction';
            var leave_url_2 = baseUrl + '/leave/request';
            var tito_url_2 = baseUrl + '/tito/request';

            var pending_ahr = pendingApprCountObj.ahr_appr_pending;
            var pending_leave = pendingApprCountObj.leave_appr_pending;
            var pending_leave_retraction = pendingApprCountObj.leave_retract_appr_pending;
            var pending_tito = pendingApprCountObj.dtr_appr_pending;

            var deadline_ahr = todayDeadApprCountObj.ahr_appr_today;
            var deadline_leave = todayDeadApprCountObj.leave_appr_today;
            var deadline_leave_retraction = todayDeadApprCountObj.leave_retract_appr_today;
            var deadline_tito = todayDeadApprCountObj.dtr_appr_today;

            var personal_ahr = personalApprCountObj.ahr;
            var personal_leave = personalApprCountObj.leave;
            var personal_leave_retraction = personalApprCountObj.leave_retract;
            var personal_tito = personalApprCountObj.tito;

            var seriesdata = [];
            if (pending_total !== 0) {
                seriesdata.push({
                    name: 'Pending',
                    y: pending_total,
                    drilldown: "Pending"
                });
            }
            if (deadline_total !== 0) {
                seriesdata.push({
                    name: 'Deadline',
                    y: deadline_total,
                    drilldown: "Deadline"
                });
            }
            if (personal_total !== 0) {
                seriesdata.push({
                    name: 'Personal',
                    y: personal_total,
                    drilldown: "Personal"
                });
            }
            // color_ahr = '#138FFF';
            // color_leave = '#A16FDB';
            // color_retraction = '#7C5295';
            // color_tito = '#F59E0A';
            color_ahr = '#25A2A6';
            color_leave = '#A4D05F';
            color_retraction = '#FDAD54';
            color_tito = '#FF7061';
            if (seriesdata.length == 0) {
                $("#alert-myrequest").show();
                $("#approvalChart").hide();
            } else if (seriesdata.length == 1) {

                var colors = ["#ffc855", "#e6558b", "#4f74ee"];
                var series_pending = {
                    name: "Pending",
                    color: colors[0],
                    data: [{
                            color: color_ahr,
                            name: "AHR",
                            y: pending_ahr,
                            url: ahr_url
                        },
                        {
                            color: color_leave,
                            name: "Leave",
                            y: pending_leave,
                            url: leave_url
                        },
                        {
                            color: color_retraction,
                            name: "Retraction",
                            y: pending_leave_retraction,
                            url: leave_retraction_url
                        },
                        {
                            color: color_tito,
                            name: "TITO",
                            y: pending_tito,
                            url: tito_url
                        }
                    ]
                };
                var series_deadline = {
                    name: "Deadline",
                    color: colors[2],
                    data: [{
                            color: color_ahr,
                            name: "AHR",
                            y: deadline_ahr,
                            url: ahr_url
                        },
                        {
                            color: color_leave,
                            name: "Leave",
                            y: deadline_leave,
                            url: leave_url
                        },
                        {
                            color: color_retraction,
                            name: "Retraction",
                            y: deadline_leave_retraction,
                            url: leave_retraction_url
                        },
                        {
                            color: color_tito,
                            name: "TITO",
                            y: deadline_tito,
                            url: tito_url
                        }
                    ]
                };
                var series_personal = {
                    name: "Personal",
                    color: colors[1],
                    data: [{
                            color: color_ahr,
                            name: "AHR",
                            y: personal_ahr,
                            url: ahr_url_2
                        },
                        {
                            color: color_leave,
                            name: "Leave",
                            y: personal_leave,
                            url: leave_url_2
                        },
                        {
                            color: color_retraction,
                            name: "Retraction",
                            y: personal_leave_retraction,
                            url: leave_retraction_url_2
                        },
                        {
                            color: color_tito,
                            name: "TITO",
                            y: personal_tito,
                            url: tito_url_2
                        }
                    ]
                };
                var final_series = (seriesdata[0].name === 'Pending') ? series_pending : (seriesdata[0]
                    .name === 'Deadline') ? series_deadline : series_personal;
                var title_series = (seriesdata[0].name === 'Pending') ? "Pending Requests" : (seriesdata[0]
                    .name === 'Deadline') ? "Deadline Requests" : "Personal Requests";
                Highcharts.chart('approvalChart', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: title_series,
                        style: {
                            fontSize: '15px',
                            fontWeight: '400',
                            color: 'gray'
                        }
                    },
                    yAxis: {
                        labels: {
                            enabled: false
                        },
                        title: null
                    },
                    xAxis: {
                        type: 'category'
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                inside: true,
                                enabled: true
                            },
                            pointWidth: 50,
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function() {
                                        window.open(this.options.url, '_blank');
                                    }
                                }
                            }
                        }
                    },
                    tooltip: {
                        enabled: false
                    },
                    series: [final_series]
                });
            } else {
                var defaultTitle = 'All Requests';
                var drilldownTitle = ' Requests'
                var chart = Highcharts.chart('approvalChart', {
                    chart: {
                        type: 'column',
                        events: {
                            drilldown: function(e) {
                                chart.setTitle({
                                    text: e.point.name + drilldownTitle
                                });
                            },
                            drillup: function(e) {
                                chart.setTitle({
                                    text: defaultTitle
                                });
                            }
                        }
                    },
                    title: {
                        text: "All Requests",
                        style: {
                            fontSize: '15px',
                            fontWeight: '400'
                        }
                    },
                    yAxis: {
                        labels: {
                            enabled: false
                        },
                        title: null
                    },
                    xAxis: {
                        type: 'category'
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                inside: true,
                                enabled: true
                            },
                            pointWidth: 50,
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function() {
                                        window.open(this.options.url, '_blank');
                                    }
                                }
                            }
                        }
                    },
                    tooltip: {
                        enabled: false
                    },
                    colors: ["#ffd376", "#728ff1", "#eb76a2"],
                    series: [{
                        name: "Requests",
                        colorByPoint: true,
                        data: seriesdata
                    }],
                    drilldown: {
                        series: [{
                            name: "Pending",
                            id: "Pending",
                            data: [{
                                    color: color_ahr,
                                    name: "AHR",
                                    y: pending_ahr,
                                    url: ahr_url
                                },
                                {
                                    color: color_leave,
                                    name: "Leave",
                                    y: pending_leave,
                                    url: leave_url
                                },
                                {
                                    color: color_retraction,
                                    name: "Retraction",
                                    y: pending_leave_retraction,
                                    url: leave_retraction_url
                                },
                                {
                                    color: color_tito,
                                    name: "TITO",
                                    y: pending_tito,
                                    url: tito_url
                                },
                            ]
                        }, {
                            name: "Deadline",
                            id: "Deadline",
                            data: [{
                                    color: color_ahr,
                                    name: "AHR",
                                    y: deadline_ahr,
                                    url: ahr_url
                                },
                                {
                                    color: color_leave,
                                    name: "Leave",
                                    y: deadline_leave,
                                    url: leave_url
                                },
                                {
                                    color: color_retraction,
                                    name: "Retraction",
                                    y: deadline_leave_retraction,
                                    url: leave_retraction_url
                                },
                                {
                                    color: color_tito,
                                    name: "TITO",
                                    y: deadline_tito,
                                    url: tito_url
                                },
                            ]
                        }, {
                            name: "Personal",
                            id: "Personal",
                            data: [{
                                    color: color_ahr,
                                    name: "AHR",
                                    y: personal_ahr,
                                    url: ahr_url_2
                                },
                                {
                                    color: color_leave,
                                    name: "Leave",
                                    y: personal_leave,
                                    url: leave_url_2
                                },
                                {
                                    color: color_retraction,
                                    name: "Retraction",
                                    y: personal_leave_retraction,
                                    url: leave_retraction_url_2
                                },
                                {
                                    color: color_tito,
                                    name: "TITO",
                                    y: personal_tito,
                                    url: tito_url_2
                                },
                            ]
                        }]
                    }
                });
            }

        });


});
</script>