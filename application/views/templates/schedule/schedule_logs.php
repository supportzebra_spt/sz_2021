<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .m-datatable__cell{
        text-transform:  capitalize
    }
    .bg-custom-gray{
        background:#4B4C54 !important;
    }
    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
    .font-white{
        color:white !important;
    }
    .m-content ul.m-subheader__breadcrumbs li,.m-content ul.m-subheader__breadcrumbs li a{
        padding:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li a span:hover{
        background: #ddd !important;         
        color:black !important; 
    } 

    .m-content ul.m-subheader__breadcrumbs li a span{
        color:#fff !important;  
        padding:10px 20px !important; 
    } 
    .m-content ul.m-subheader__breadcrumbs{
        background:#4B4C54 !important; 
        padding:2px;
    }
    .my-nav-active{
        background: #eee !important;    
    } 
    li.my-nav-active a span{      
        color:black; 
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }

    /*DATERANGEPICKER*/
    .daterangepicker {
        font-size:13px !important;
    }
    .daterangepicker table td{
        width:28px !important;
        height:28px !important;
    }
    .daterangepicker_input input{
        padding-top:5px !important;
        padding-bottom:5px !important;
        line-height:1 !important;
    }

    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }    
    .blockOverlay{
        opacity:0.15 !important;
        background-color:#746DCB !important;
    }
    .form-control{
        font-size:12px !Important;
    }

</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Schedule Module</h3>          
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Schedule/schedule_logs'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Schedule Logs</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12"> 
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tabs m-portlet--success m-portlet--head-solid-bg m-portlet--head-sm" id="portlet">
                    <div class="m-portlet__head" style="background:#4B4C54;border-color:#4B4C54">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text" style="color:white !Important">
                                    Schedule - Logs
                                </h3>
                            </div>          
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#"class="m-portlet__nav-link btn btn-info m-btn--air m-btn--icon" onclick="$('#perpage').change();"> <i class="flaticon-refresh"></i> Reload

                                    </a>
                                </li> 
                                <li class="m-portlet__nav-item">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input type="text" class="form-control m-input" placeholder="Search First or Last Name" id="search-name"/>
                                        <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                                    </div>

                                </li>   
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">

                            <div class="col-md-3">
                                <label for="" class="m--font-bolder">DATE RANGE</label>
                                <br />
                                <div class="m-input-icon m-input-icon--left" id="search-dates">
                                    <input type="text" class="form-control m-input" placeholder="Date Range" readonly style="background: #fff;">
                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="" class="m--font-bolder">SCHEDULE TYPES</label>
                                <br />
                                <select class="form-control m-input" id="search-schedtypes" >
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="" class="m--font-bolder">CLASS</label>
                                <br />
                                <select class="form-control m-input" id="search-class">
                                    <option value="">All</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="" class="m--font-bolder">ACCOUNTS</label>
                                <br />
                                <select class="form-control m-input" id="search-accounts" >
                                </select>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert" style="display:none">
                                    <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                    <div class="m-alert__text"><strong>No Record Found! </strong> <br>Please adjust your search criteria and try again.
                                    </div>
                                </div>
                                <table class="table m-table table-hover table-sm table-bordered" id="table-access" style="font-size:13px">
                                    <thead style="background: #555;color: white;">
                                    <th>Name</th>
                                    <th class="text-center">Schedule Date</th>
                                    <th class="text-center" colspan="2">Schedule Shift</th>
                                    <th class="text-center">Remarks</th>
                                    <th class="text-center">Clock In</th>
                                    <th class="text-center">Clock Out</th>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">
                                    <div class="m-stack__item m-stack__item--left m-stack__item--middle" >
                                        <ul id="pagination">
                                        </ul>
                                    </div>
                                    <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:8%">
                                        <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                                            <option>10</option>
                                            <option>20</option>
                                            <option>50</option>
                                            <option>100</option>
                                            <option>250</option>
                                            <option>500</option>
                                        </select>
                                    </div>
                                    <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="count"></span> of <span id="total"></span> records</div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <!--end::Portlet-->
            </div>
        </div>

    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">

                                            function get_all_accounts(callback) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Schedule/getUniqueEmpAcc_ids",
                                                    cache: false,
                                                    success: function (result) {
                                                        result = JSON.parse(result.trim());
                                                        callback(result);
                                                    }
                                                });
                                            }
                                            function get_all_schedtypes(callback) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Schedule/get_all_schedule_types",
                                                    cache: false,
                                                    success: function (result) {
                                                        result = JSON.parse(result.trim());
                                                        callback(result);
                                                    }
                                                });
                                            }
                                            function getScheduleLogs(limiter, perPage, callback) {

                                                var datestart = $("#search-dates").data('daterangepicker').startDate;
                                                var dateend = $("#search-dates").data('daterangepicker').endDate;
                                                var searchclass = $("#search-class").val();
                                                var searchaccounts = $("#search-accounts").val();
                                                var searchname = $("#search-name").val();
                                                var searchschedtypes = $("#search-schedtypes").val();
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>Schedule/get_schedule_logs",
                                                    data: {
                                                        limiter: limiter,
                                                        perPage: perPage,
                                                        datestart: tz_date(moment(datestart), 'YYYY-MM-DD'),
                                                        dateend: tz_date(moment(dateend), 'YYYY-MM-DD'),
                                                        searchclass: searchclass,
                                                        searchaccounts: searchaccounts,
                                                        searchname: searchname,
                                                        searchschedtypes: searchschedtypes
                                                    },
                                                    beforeSend: function () {
                                                        mApp.block('#portlet', {
                                                            overlayColor: '#000000',
                                                            type: 'loader',
                                                            state: 'brand',
                                                            size: 'lg'
                                                        });
                                                    },
                                                    cache: false,
                                                    success: function (res) {
                                                        res = JSON.parse(res.trim());

                                                        $("#total").html(res.total);
                                                        $("#total").data('count', res.total);

                                                        $("#table-access tbody").html("");
                                                        var string = '';
                                                        $.each(res.data, function (i, item) {
                                                            var currentEmployee = item.emp_id;
                                                            var logs = item.logs;
                                                            var remarksString = "";
                                                            if (item.remarks=='ahr'){
                                                            	if(item.isLocked=='2'){
                                                            		remarksString =	"Pending AHR";
                                                            	}else if(item.isLocked=='14'){
																	remarksString =	"Completed AHR";
                                                            	}
                                                            }else if(item.remarks=='TITO'){
																remarksString =	"TITO";
                                                            }else if(item.remarks=='Excel'||item.remarks=='Manual'||item.remarks=='DMS'){
                                                            	remarksString = item.remarks;
                                                            }else if(item.remarks=="Request-Full"){
                                                            	remarksString == "Full Leave";
                                                            }else if(item.remarks=="Request-Half"){
                                                            	remarksString == "Half Leave";
                                                            }

                                                            if (item.time_start === null) {
                                                                var shift = "";
                                                            } else {
                                                                var shift = item.time_start + " - " + item.time_end;
                                                            }
                                                            if (shift === '') {
                                                                string += '<tr style="background:#ecf7fb" data-schedid="' + item.sched_id + '">';
                                                            } else {
                                                                string += '<tr data-schedid="' + item.sched_id + '">';
                                                            }

                                                            string += '<td style="font-weight:400">' + item.lname + ', ' + item.fname + '</td>';
                                                            string += '<td class="text-center">' + tz_date(moment(item.sched_date), 'MMM DD, YYYY') + '</td>';
                                                            if (shift === '') {
                                                                string += '<td class="text-center" colspan="2" style="color:' + item.style + ';font-weight:400">' + item.type + '</td>';
                                                            } else {
                                                                string += '<td class="text-center" style="color:' + item.style + ';font-weight:400">' + item.type + '</td>';
                                                                string += '<td class="text-center" style="color:' + item.style + ';font-weight:400">' + shift + '</td>';
                                                            }

                                                            string += '<td class="text-center">' + remarksString + '</td>';
                                                            var allowedSchedTypes = ['1', '4', '5'];
                                                            var clock_in_options = "";
                                                            var clock_out_options = "";
                                                            var disabled_select = (item.isLocked=='2'||item.isLocked=='14')? 'disabled': '';
                                                            var disabled_style = (item.isLocked=='2'||item.isLocked=='14')? 'background-color:#ddd;border-color:#ccc': '';
                                                            console.log(item.isLocked,disabled_select,disabled_style);
                                                            if (jQuery.inArray(item.schedtype_id, allowedSchedTypes) > -1) {// found
                                                                clock_in_options = '<select name="selectlogs" id="" class="form-control m-input form-control-sm" style="'+disabled_style+'" data-clocktype="clockin" '+disabled_select+'><option value="">--</option>';
                                                                $.each(item.available_clock_in, function (key, obj) {
                                                                    if (item.clock_in_id === obj.dtr_id) {
                                                                        clock_in_options += '<option value="' + obj.dtr_id + '" selected class="original_dtrid">' + tz_date(obj.log, "MMM DD, YYYY hh:mm A") + '</option>';
                                                                    } else {
                                                                        clock_in_options += '<option value="' + obj.dtr_id + '">' + tz_date(obj.log, "MMM DD, YYYY hh:mm A") + '</option>';
                                                                    }
                                                                });
                                                                clock_in_options += '</select>';

                                                                clock_out_options = '<select name="selectlogs" id="" class="form-control m-input form-control-sm" style="'+disabled_style+'"  data-clocktype="clockout" '+disabled_select+'><option value="">--</option>';
                                                                $.each(item.available_clock_out, function (key, obj) {
                                                                    if (item.clock_out_id === obj.dtr_id) {
                                                                        clock_out_options += '<option value="' + obj.dtr_id + '" selected class="original_dtrid">' + tz_date(obj.log, "MMM DD, YYYY hh:mm A") + '</option>';
                                                                    } else {
                                                                        clock_out_options += '<option value="' + obj.dtr_id + '">' + tz_date(obj.log, "MMM DD, YYYY hh:mm A") + '</option>';
                                                                    }
                                                                });
                                                                clock_in_options += '</select>';
                                                            }
                                                            string += '<td class="text-center">' + clock_in_options + '</td>';
                                                            string += '<td class="text-center">' + clock_out_options + '</td>';
                                                            string += '</tr>';

                                                            if (currentEmployee != '' && res.data.hasOwnProperty(i + 1)) {
                                                                if (currentEmployee !== res.data[i + 1].emp_id) {
                                                                    string += '<tr style="background:#D5DFD6"><td colspan=7></td></tr>';
                                                                }
                                                            }

                                                        });
                                                        if (string === '') {
                                                            $("#date-alert").show();
                                                            $("#table-access").hide();
                                                        } else {
                                                            $("#date-alert").hide();
                                                            $("#table-access").show();
                                                        }
                                                        $("#table-access tbody").html(string);

                                                        var count = $("#total").data('count');
                                                        var result = parseInt(limiter) + parseInt(perPage);
                                                        if (count === 0) {
                                                            $("#count").html(0 + ' - ' + 0);
                                                        } else if (count <= result) {
                                                            $("#count").html((limiter + 1) + ' - ' + count);
                                                        } else if (limiter === 0) {
                                                            $("#count").html(1 + ' - ' + perPage);
                                                        } else {
                                                            $("#count").html((limiter + 1) + ' - ' + result);
                                                        }
                                                        mApp.unblock('#portlet');
                                                        callback(res.total);
                                                    }, error: function () {
                                                        swal("Error", "Please contact admin", "error");
                                                        mApp.unblock('#portlet');
                                                    }
                                                });
                                            }
                                            function tz_date(thedate, format = null) {
                                                // var tz = moment.tz(new Date(thedate), "Asia/Manila");
                                                var tz = thedate;
                                                return (format === null) ? moment(tz) : moment(tz).format(format);
                                            }
                                            $(function () {
                                                $('#search-dates .form-control').val(tz_date(moment(), 'MMM DD, YYYY') + ' - ' + tz_date(moment(), 'MMM DD, YYYY'));
                                                $('#search-dates').daterangepicker({
                                                    startDate: tz_date(moment()),
                                                    endDate: tz_date(moment()),
                                                    maxDate: tz_date(moment()),
                                                    opens: 'center',
                                                    drops: 'down',
                                                    autoUpdateInput: true
                                                },
                                                        function (start, end, label) {
                                                            $('#search-dates .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));

                                                            $("#perpage").change();
                                                        });

                                                get_all_accounts(function (res) {
                                                    $("#search-accounts").html("<option value=''>All</option>");
                                                    $.each(res.accounts, function (i, item) {
                                                        $("#search-accounts").append("<option value='" + item.acc_id + "' data-class='" + item.acc_description + "'>" + item.acc_name + "</option>");
                                                    });
                                                });
                                                get_all_schedtypes(function (res) {
                                                    $("#search-schedtypes").html("<option value=''>All</option>");
                                                    $.each(res.schedtypes, function (i, item) {
                                                        $("#search-schedtypes").append("<option value='" + item.schedtype_id + "'>" + item.type + "</option>");
                                                    });
                                                });
                                                $("#perpage").change(function () {
                                                    var perPage = $("#perpage").val();
                                                    getScheduleLogs(0, perPage, function (total) {
                                                        $("#pagination").pagination('destroy');
                                                        $("#pagination").pagination({
                                                            items: total, //default
                                                            itemsOnPage: $("#perpage").val(),
                                                            hrefTextPrefix: "#",
                                                            cssStyle: 'light-theme',
                                                            onPageClick: function (pagenumber) {
                                                                var perPage = $("#perpage").val();
                                                                getScheduleLogs((pagenumber * perPage) - perPage, perPage);
                                                            }
                                                        });
                                                    });
                                                });

                                                $("#search-class,#search-leave-types,#search-name,#search-schedtypes,#search-accounts").change(function () {
                                                    $("#perpage").change();
                                                });
                                                $("#perpage").change();
                                                $("#search-class").change(function () {
                                                    var theclass = $(this).val();
                                                    $("#search-accounts").children('option').css('display', '');
                                                    $("#search-accounts").val('').change();
                                                    if (theclass !== '') {
                                                        $("#search-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
                                                    }

                                                });
                                                $("#table-access tbody").on("change", "select[name='selectlogs']", function () {
                                                    var that = this;
                                                    var clocktype = $(that).data('clocktype');
                                                    var dtr_id = $(that).val();
                                                    var element = $(that).find("option[class='original_dtrid']");
                                                    if ($(element)[0]) {
                                                        var original_dtr_id = $(element).attr('value');
                                                    } else {
                                                        var original_dtr_id = '';
                                                    }
                                                    var sched_id = $(that).closest('tr').data('schedid');

                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url(); ?>Schedule/assign_schedule_log_v2",
                                                        data: {
                                                            clocktype: clocktype,
                                                            dtr_id: dtr_id,
                                                            sched_id: sched_id,
                                                            original_dtr_id: original_dtr_id
                                                        },
                                                        cache: false,
                                                        beforeSend: function () {
                                                            mApp.block('#portlet', {
                                                                overlayColor: '#000000',
                                                                type: 'loader',
                                                                state: 'brand',
                                                                size: 'lg'
                                                            });
                                                        },
                                                        success: function (result) {
                                                            var res = JSON.parse(result.trim());
                                                            if (res.status === 'Success') {
//                            alert(res.new_dtr_id);
                                                                if (res.new_dtr_id == 0) {
//                                alert("ZERO");
                                                                    $(that).find('option').removeClass();
                                                                    $(that).val('');
                                                                    $(that).find('option[value=""]').prop('selected', true);
                                                                } else {
//                                alert("WITH NUMBER");
                                                                    $(that).find('option').removeClass();
                                                                    $(that).find('option[value="' + res.new_dtr_id + '"]').prop('selected', true);
                                                                    $(that).find('option[value="' + res.new_dtr_id + '"]').addClass("original_dtrid");
                                                                }
                                                                $.notify({
                                                                    message: 'Successfully tagged schedule logs.'
                                                                }, {
                                                                    type: 'success',
                                                                    timer: 1000
                                                                });
                                                            } else if (res.status === 'Warning') {
                                                                $(that).val(original_dtr_id);
                                                                $.notify({
                                                                    message: 'Log is already tagged to another schedule.'
                                                                }, {
                                                                    type: 'warning',
                                                                    timer: 1000
                                                                });
                                                            } else if (res.status === 'No Change') {
                                                                $(that).val(original_dtr_id);
                                                                $.notify({
                                                                    message: 'No change : Same log was tagged to the schedule.'
                                                                }, {
                                                                    type: 'info',
                                                                    timer: 1000
                                                                });
                                                            } else {
                                                                $(that).val(original_dtr_id);
                                                                $.notify({
                                                                    message: 'An error has occurred.'
                                                                }, {
                                                                    type: 'danger',
                                                                    timer: 1000
                                                                });
                                                            }
                                                            mApp.unblock('#portlet');
                                                        }
                                                    });
                                                });
                                            });
</script>