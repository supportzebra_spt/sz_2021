<style type="text/css">
body {
    padding-right: 0 !important;
}

.form-control {
    font-size: 11px;
}

label {
    font-size: 13px;
}

p {
    margin-bottom: 0 !important;
}

#table-intraday td {
    border: 1px solid #ddd;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body py-3 px-4 bg-danger" style="color:white">
                        <h2 id="intraday-totalabsentlate">0</h2>
                        <p>Absent/Late</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body py-3 px-4 bg-success" style="color:white">
                        <h2 id="intraday-totalpresent">0</h2>
                        <p>Present</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body py-3 px-4 bg-info" style="color:white">
                        <h2 id="intraday-totalscheduled">0</h2>
                        <p>Total Scheduled</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body py-3 px-4 bg-primary" style="color:white">
                        <h2 id="intraday-totalpercentage">0.00%</h2>
                        <p>Attendance %</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-statistics"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Intraday Attendance Report
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <a href="#" class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x"
                                data-toggle="modal" data-target="#modal-filter">
                                <span>
                                    <i class="la la-filter"></i>
                                    <span>Filters</span>
                                </span>
                            </a>
                            <a href="#" class="btn btn-outline-success m-btn m-btn--icon m-btn--outline-2x"
                                id="btn-excel">
                                <span>
                                    <i class="la la-download"></i>
                                    <span>Export</span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="m-portlet__body pt-2">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Start DateTime: <span class='m--font-bolder' id="intraday-starttime"></span></p>
                                <p class="mb-2">End DateTime: <span class='m--font-bolder' id="intraday-endtime"></span>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p style="text-align:right">Generated On: <br><span class='m--font-bolder'
                                        id="intraday-datetime"></span></p>
                            </div>
                        </div>
                        <table class="table m-table m-table--head-bg-brand table-sm table-bordered" id="table-intraday">
                            <thead>
                                <tr>
                                    <th>Account</th>
                                    <th>Team Leader</th>
                                    <th>AM</th>
                                    <th>Absent/Late</th>
                                    <th>Present</th>
                                    <th>Total Scheduled</th>
                                    <th>Attendance %</th>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Report Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">START DATE:</label>
                        <input type="text" class="form-control m-input" placeholder="Select date" id="search-date"
                            style="font-size:14px">
                    </div>
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">END DATE:</label>
                        <input type="text" class="form-control m-input" readonly disabled placeholder="Select date" id="helper-enddate"
                            style="font-size:14px">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">START TIME:</label>
                        <input type="text" class="form-control" id="search-starttime" placeholder="Select time"
                            style="font-size:14px">
                    </div>
                    <div class="col-md-6">
                        <label for="" class="m--font-bolder">END TIME:</label>
                        <input type="text" class="form-control" id="search-endtime" placeholder="Select time"
                            style="font-size:14px">
                    </div>
                </div>
                <hr>
                <div class="row mb-3">
                    <div class="col-12">
                        <label for="" class="m--font-bolder">ACCOUNTS:</label>
                        <select class="form-control m-bootstrap-select m_selectpicker" multiple id="search-accounts">
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-8 col-12">
                        <label for="" class="m--font-bolder">TEAM LEADERS:</label>
                        <select class="form-control m-bootstrap-select m_selectpicker" id="search-teamleads">
                        </select>
                    </div>
                    <div class="col-md-4 col-12">
                        <label for="" class="m--font-bolder">AMs:</label>
                        <select class="form-control m-bootstrap-select m_selectpicker" id="search-managers">
                        </select>
                    </div>
                </div>

                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-refresh">Update Report</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
<script type="text/javascript">
function getIntradayRecord(isDateChanged = true) {
    var searchstartdate = $("#search-date").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var searchenddate = $("#helper-enddate").data('datepicker').getFormattedDate('yyyy-mm-dd');
    var searchstarttime = $("#search-starttime").data("timepicker").getTime();
    var searchendtime = $("#search-endtime").data("timepicker").getTime();
    var searchaccounts = $("#search-accounts").val();
    var searchteamleads = $("#search-teamleads").val();
    var searchmanagers = $("#search-managers").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Schedule/get_intraday_record",
        data: {
          	searchstartdate: searchstartdate,
            searchenddate:searchenddate,
            searchstarttime: searchstarttime,
            searchendtime: searchendtime,
            searchaccounts: searchaccounts,
            searchteamleads: searchteamleads,
            searchmanagers: searchmanagers
        },
        beforeSend: function() {
            mApp.block('#portlet', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg'
            });
        },
        cache: false,
        success: function(res) {
            res = JSON.parse(res.trim());
            $("#intraday-datetime").text(res.currentDate + ' ' + res.currentTime);
            $("#intraday-totalabsentlate").text(res.total_absentlate);
            $("#intraday-totalpresent").text(res.total_present);
            $("#intraday-totalscheduled").text(res.total_scheduled);
            $("#intraday-totalpercentage").text(res.total_percentage);
            $("#intraday-starttime").text(res.searchStartTime);
            $("#intraday-endtime").text(res.searchEndTime);

            if (isDateChanged) {
                let search_accounts = ``;
                let search_teamleads = `<option value="">All</option>`;
                let search_managers = `<option value="">All</option>`;

                $.each(res.all_accounts, (key, acc) => {
                    search_accounts += `<option value="${acc.acc_id}">${acc.acc_name}</option>`;
                })
                $.each(res.all_teamleads, (key, tl) => {
                    search_teamleads += `<option value="${tl.emp_id}">${tl.name}</option>`;
                })
                $.each(res.all_managers, (key, am) => {
                    search_managers += `<option value="${am.emp_id}">${am.name}</option>`;
                })
                $("#search-accounts").html(search_accounts);
                $("#search-teamleads").html(search_teamleads);
                $("#search-managers").html(search_managers);

                $('#search-teamleads,#search-managers,#search-accounts').selectpicker('refresh');
            }

            $("#table-intraday tbody").html("");
            var string = '';
            $.each(res.data, (index, obj) => {
                if (obj.type == 'custom') {
                    string += `<tr>
                        <td style="border-bottom:none !important;font-weight:402">${obj.acc_name}</td>
                        <td style="border:none !important; background:#eee"></td><td style="border:none !important;background:#eee"></td>
                        <td style="border:none !important; background:#eee;font-weight:402">${obj.custom_absentlate}</td>
                        <td style="border:none !important; background:#eee;font-weight:402">${obj.custom_present}</td>
                        <td style="border:none !important; background:#eee;font-weight:402">${obj.custom_scheduled}</td>
                        <td style="border-left:none !important;background:#eee;font-weight:402">${obj.custom_percentage}</td>
                    </tr>`;
                    $.each(obj.customdata, (key, custom) => {
                        //same =======================================
                        let subStr = ``;
                        $.each(custom.supervisorArray, (key, supObj) => {
                            string += `<tr>`;
                            string +=
                                `<td style="text-align:right;font-style:italic;border-top:none !important;border-bottom:none !important">${custom.acc_name}</td>`;
                            string += `<td>${supObj.directSupervisor}</td>`;
                            string += `<td>${supObj.nextSupervisor}</td>`;
                            string +=
                                `<td>${supObj.absentlate}</td><td>${supObj.present}</td><td>${supObj.totalscheduled}</td><td>${supObj.percentage}</td>`;
                            string += `</tr>`;
                        });
                        //endsame =======================================
                    })
                } else {
                    //same =======================================
                    let subStr = ``;
                    $.each(obj.supervisorArray, (key, supObj) => {
                        string += `<tr>`;
                        if (key == 0) {
                            string +=
                                `<td rowspan="${Object.values(obj.supervisorArray).length}">${obj.acc_name}</td>`;
                        }
                        string += `<td>${supObj.directSupervisor}</td>`;
                        string += `<td>${supObj.nextSupervisor}</td>`;
                        string +=
                            `<td>${supObj.absentlate}</td><td>${supObj.present}</td><td>${supObj.totalscheduled}</td><td>${supObj.percentage}</td>`;
                        string += `</tr>`;
                    });
                    //endsame =======================================
                }

            });

            $("#table-intraday tbody").html(string);
            $("#modal-filter").modal('hide');
            mApp.unblock('#portlet');
        },
        error: function() {
            $("#modal-filter").modal('hide');
            swal("Error", "Please contact admin", "error");
            mApp.unblock('#portlet');
        }
    });
}

$(function() {
    $('#search-date,#helper-enddate').datepicker({
        todayHighlight: true,
        orientation: "bottom left",
        autoclose: true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        },
        format: "MM dd, yyyy"
    })
    $('#search-date').datepicker('setDate', new Date());
    $('#helper-enddate').datepicker('setDate', new Date(Date.now() + ( 3600 * 1000 * 24)));


    $('#search-starttime').timepicker({
        defaultTime: '12:00 PM',
        minuteStep: 1,
        showSeconds: false,
        showMeridian: true
    });

    $('#search-endtime').timepicker({
        defaultTime: '11:59 AM',
        minuteStep: 1,
        showSeconds: false,
        showMeridian: true
    });

    $('#search-teamleads,#search-managers,#search-accounts').selectpicker();

    $("#search-date,#search-starttime,#search-endtime").change(function() {
        $("#btn-refresh").data('isDateChanged', true);
        var searchdate = $("#search-date").data('datepicker').getFormattedDate('yyyy-mm-dd');    
        var searchstarttime = $("#search-starttime").data("timepicker").getTime();
        var searchendtime = $("#search-endtime").data("timepicker").getTime();
        var momentstart = moment(searchdate+" "+searchstarttime);
        var momentend = moment(searchdate+" "+searchendtime);
        var finalEndDate = momentend;
        if(momentstart>momentend){
        	finalEndDate = momentend.add(1,'day');
        }
        $("#helper-enddate").datepicker('setDate', finalEndDate.toDate());

    });
    $("#search-teamleads,#search-managers,#search-accounts").change(function() {
        $("#btn-refresh").data('isDateChanged', false);
    });

    $("#modal-filter").on("show.bs.modal", () => {
        $("#btn-refresh").data('isDateChanged', false);
    })

    $("#btn-refresh").click(() => {
        $("#modal-filter").modal('hide');
        getIntradayRecord($("#btn-refresh").data('isDateChanged'));
    });

    getIntradayRecord();

    $("#btn-excel").click(function() {

        var searchstartdate = $("#search-date").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var searchenddate = $("#helper-enddate").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var searchstarttime = $("#search-starttime").data("timepicker").getTime();
        var searchendtime = $("#search-endtime").data("timepicker").getTime();
        var searchaccounts = $("#search-accounts").val();
        var searchteamleads = $("#search-teamleads").val();
        var searchmanagers = $("#search-managers").val();
        $.ajax({
            url: '<?php echo base_url(); ?>Schedule/downloadExcel_intraday',
            method: 'POST',
            data: {
                searchstartdate: searchstartdate,
                searchenddate:searchenddate,
                searchstarttime: searchstarttime,
                searchendtime: searchendtime,
                searchaccounts: searchaccounts,
                searchteamleads: searchteamleads,
                searchmanagers: searchmanagers
            },
            beforeSend: function() {
                mApp.block('#portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg',
                    message: 'Downloading...'
                });
            },
            xhrFields: {
                responseType: 'blob'
            },
            success: function(data) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'SZ_IntradayAttendanceReport.xlsx';
                a.click();
                window.URL.revokeObjectURL(url);
                mApp.unblock('#portlet');
            },
            error: function(data) {
                $.notify({
                    message: 'An error occured while exporting excel'
                }, {
                    type: 'danger',
                    timer: 1000
                });
                mApp.unblock('#portlet');
            }
        });
    });
});
</script>