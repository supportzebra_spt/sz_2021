<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .blockOverlay{
        opacity:0.15 !important;
        background-color:#746DCB !important;
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }
    .m-content select{
        font-size: 13px;
    }
    .move,.moveall,.remove,.removeall{
        padding:5px !important;
    }

    .table-sticky-container{
        overflow: auto;
        max-height:600px;
    }
    .table-sticky-container table thead tr th {
        position: sticky;
        top: 0;
        min-width:200px;
        max-width:200px;
        background:#666;
        color:white;
        font-size:12px;
        border-top:0;
        padding:5px !important;
        text-align:center
    }

    .table-sticky-container table tbody tr th {
        position: sticky;
        left: 0;
        font-size:12px;
        padding:  5px !important;
        vertical-align: middle !important;
        text-align:left;
        background:whitesmoke;
        border-color:white;
    }
    .table-sticky-container table tbody tr td {
        font-size:12px;
        padding: 5px !important;
        vertical-align: middle !important;
        text-align:center
    }
    .table-sticky-container table thead tr th:first-child {
        left: 0 ;
        z-index: 1;
        min-width:250px;
        max-width:250px;
        text-align:left;
        background:#666;
        color:white;
        padding-left:5px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Schedule Module</h3>          
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Schedule/excel'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Schedule Batch</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12"> 
                <!--begin::Portlet-->

                <div class="m-portlet m-portlet--tabs m-portlet--success m-portlet--head-solid-bg m-portlet--head-sm" id="portlet">
                    <div class="m-portlet__head" style="background:#4B4C54;border-color:#4B4C54">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text" style="color:white !Important">
                                    Schedule Batch
                                </h3>
                            </div>          
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line  m-tabs-line--right" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-download" role="tab" aria-selected="true">
                                        <i class="la la-download"></i> Download Excel
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-upload" role="tab" aria-selected="false">
                                        <i class="la la-upload"></i> Upload Excel
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="tab-download" role="tabpanel">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="m-form__group form-group">
                                            <label for="" class="m--font-bolder">DATE RANGE</label>
                                            <br />
                                            <div class="m-input-icon m-input-icon--left" id="search-dates">
                                                <input type="text" class="form-control m-input" placeholder="Date Range" readonly style="background: #fff;">
                                                <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="" class="m--font-bolder">CLASS</label>
                                        <br />
                                        <select name="" class="m-input form-control" id="search-class">
                                            <option value="">All Class</option>
                                            <option value="Agent">Ambassador</option>
                                            <option value="Admin">SZ Team</option>
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <label for="" class="m--font-bolder">ACCOUNTS</label>
                                        <br />
                                        <select name="" class="m-input form-control" id="search-accounts">
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <p><b>Note:</b> For the uploading of schedule in excel format, select the schedule values from the cell dropdown options. Other values which are not specified in the cell dropdown option will be disregarded during uploading of the excel file.</p>   
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-form__group form-group">
                                            <label for="" class="m--font-bolder">EMPLOYEES</label>
                                            <br />
                                            <select name="from" class="form-control m-input" id="search-employees" size="8" multiple="multiple">
                                            </select>
                                        </div>   
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-brand btn-block m-btn m-btn--icon" id="btn-download">
                                            <span>
                                                <i class="la la-download"></i>
                                                <span>Download Excel</span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-upload" role="tabpanel">
                                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                    <div class="m-stack__item m-stack__item--center m-stack__item--middle pl-0" style="background:none;border:none">
                                        <div id="drag-and-drop-zone" class="dm-uploader text-center p-5" style="border:0.4rem dashed #A5A5C7;height:100%">

                                            <h3 class="mb-4 mt-4 text-muted">Drag &amp; drop excel file here</h3>
                                            <hr />
                                            <div class="btn btn-brand mb-4">
                                                <span>Open the file browser</span>
                                                <input type="file" title='Click to add Files' />
                                            </div>
                                        </div><!-- /uploader -->

                                    </div>
                                    <div class="m-stack__item m-stack__item--left pr-0" style="background:none;border:none">

                                        <div class="card">
                                            <div class="card-header">
                                                Excel Reader Status
                                            </div>
                                            <div class="card-body p-0">
                                                <div class="p-2" style="background:white;min-height:90px;border-bottom:1px solid gainsboro;height:100%">
                                                    <ul class="list-unstyled  d-flex flex-column col" id="files" style="overflow-y: auto;word-break: break-word;margin-bottom: 0;min-height:82px">
                                                        <li class="media">
                                                            <div class="media-body mb-1">
                                                                <p class="mb-2">
                                                                    <strong>None</strong><br> Status: <span class="text-muted">Waiting</span>
                                                                </p>
                                                                <div class="progress mb-2">
                                                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
                                                                         role="progressbar"
                                                                         style="width: 0%" 
                                                                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                                    </div>
                                                                </div>
                                                                <hr class="mt-1 mb-1" />
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div style="background:whitesmoke;font-size:11px;overflow-y: scroll;height: 115px;">
                                                    <ul class="list-group list-group-flush" id="debug" style="word-break: break-word;">
                                                    </ul>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header m--font-bolder">
                                                Excel Preview
                                            </div>
                                            <div class="card-body p-1">
                                                <div class="table-sticky-container">
                                                    <table class="table m-table table-hover table-sm mb-0" id="table-preview" >
                                                        <thead></thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="card-footer p-1">
                                                <button class="btn btn-brand m-btn m-btn--icon btn-block"  id="btn-upload" style="display:none">
                                                    <span>
                                                        <i class="la la-upload"></i>
                                                        <span>Upload Excel</span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
<!-- File item template -->
<script type="text/html" id="files-template">
    <li class="media">
        <div class="media-body mb-1">
            <p class="mb-2">
                <strong>%%filename%%</strong><br> Status: <span class="text-muted">Waiting</span>
            </p>
            <div class="progress mb-2">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
                     role="progressbar"
                     style="width: 0%" 
                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
            <hr class="mt-1 mb-1" />
        </div>
    </li>
</script>
<!-- Debug item template -->
<script type="text/html" id="debug-template">
    <li class="list-group-item text-%%color%%" style="background:rgba(0,0,0,.03);font-size:11px;border:1px solid rgba(0,0,0,.03)"><i class="fa fa-circle" style="font-size: 11px;margin-right:5px"></i> %%message%%</li>
</script>
<link href="<?php echo base_url(); ?>assets/src/custom/css/jquery.dm-uploader.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.dm-uploader.min.js" ></script>

<link href="<?php echo base_url(); ?>assets/src/custom/css/bootstrap-duallistbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.bootstrap-duallistbox.min.js" ></script>
<script type="text/javascript">
        function ui_add_log(message, color) {
            color = (typeof color === 'undefined' ? 'muted' : color);
            var template = $('#debug-template').text();
            template = template.replace('%%message%%', message);
            template = template.replace('%%color%%', color);
            $('#debug').find('li.empty').fadeOut(); // remove the 'no messages yet'
            $('#debug').prepend(template);
        }
// Creates a new file and add it to our list
        function ui_multi_add_file(id, file) {
            var template = $('#files-template').text();
            template = template.replace('%%filename%%', file.name);
            template = $(template);
            template.prop('id', 'uploaderFile' + id);
            template.data('file-id', id);
            $('#files').find('li.empty').fadeOut(); // remove the 'no files yet'
            $('#files').find('li:not(.empty)').remove();
            $('#files').prepend(template);
        }

// Changes the status messages on our list
        function ui_multi_update_file_status(id, status, message) {
            $('#uploaderFile' + id).find('span').html(message).prop('class', 'status text-' + status);
        }

// Updates a file progress, depending on the parameters it may animate it or change the color.
        function ui_multi_update_file_progress(id, percent, color, active) {
            color = (typeof color === 'undefined' ? false : color);
            active = (typeof active === 'undefined' ? true : active);
            var bar = $('#uploaderFile' + id).find('div.progress-bar');
            bar.width(percent + '%').attr('aria-valuenow', percent);
            bar.toggleClass('progress-bar-striped progress-bar-animated', active);
            if (percent === 0) {
                bar.html('');
            } else {
                bar.html(percent + '%');
            }

            if (color !== false) {
                bar.removeClass('bg-success bg-info bg-warning bg-danger');
                bar.addClass('bg-' + color);
            }
        }
</script>
<script type="text/javascript">
        function get_all_accounts(callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Schedule/getUniqueEmpAcc_ids",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    callback(result);
                }
            });
        }
        function tz_date(thedate, format = null) {
            var tz = moment.tz(new Date(thedate), "Asia/Manila");
            return (format === null) ? moment(tz) : moment(tz).format(format);
        }
        function getDownloadEmployees() {
            var searchclass = $("#search-class").val();
            var searchaccounts = $("#search-accounts").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Schedule/getDownloadEmployees",
                data: {
                    searchclass: searchclass,
                    searchaccounts: searchaccounts
                },
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    $("#search-employees").html("");
                    var string = "";
                    $.each(result.employees, function (key, employee) {
                        string += '<option value="' + employee.emp_id + '">' + employee.lname + ', ' + employee.fname + '</option>'
                    });
                    $("#search-employees").html(string);
                    $('#search-employees').bootstrapDualListbox('refresh');
                }
            });
        }
        function excel_preview() {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Schedule/excel_preview",
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        var data = res.data;
                        $("#table-preview thead").html("");
                        var string_thead = "";
                        var string_tbody = "";
                        $.each(data, function (key, item) {
                            var count = item.length;
                            if (key === 0) {
                                string_thead += '<tr>';
                                $.each(item, function (index, value) {
                                    string_thead += '<th>' + value + '</th>';
                                });
                                string_thead += '</tr>';
                            } else {
                                string_tbody += '<tr>';
                                for (var x = 0; x < count; x++) {
                                    if (x === 0) {
                                        string_tbody += '<th>' + item[x] + '</th>';
                                    } else {
                                        var shift = String(item[x]).split(" - ");
                                        var warning = String(item[x]).split("! ");
                                        if (shift.length > 1) {
                                            var date = moment().format('YYYY-MM-DD');
                                            var sched_start = moment(date + ' ' + shift[0], 'YYYY-MM-DD h:mm:ss A').format("hh:mma");
                                            var sched_end = moment(date + ' ' + shift[1], 'YYYY-MM-DD h:mm:ss A').format("hh:mma");
                                            string_tbody += '<td class="m--font-dark m--font-bold">' + sched_start + ' - ' + sched_end + '</td>';
                                        } else {
                                            if (item[x] === null) {
                                                string_tbody += '<td class="m--font-metal">No Schedule</td>';
                                            } else {
                                                if (warning.length > 1) {
                                                    string_tbody += '<td class="m--font-danger m--font-bolder">' + warning.reverse()[0] + '</td>';
                                                } else {
                                                    string_tbody += '<td class="m--font-dark m--font-bold">' + item[x] + '</td>';
                                                }

                                            }
                                        }
                                    }
                                }
                                string_tbody += '</tr>';
                            }
                        });
                        $("#table-preview thead").html(string_thead);
                        $("#table-preview tbody").html(string_tbody);
                        $("#btn-upload").show();
                    } else {
                        ui_add_log('Empty template', 'warning');
                    }

                }
            });
        }
        $(function () {

            $('#search-dates .form-control').val(tz_date(moment(), 'MMM DD, YYYY') + ' - ' + tz_date(moment(), 'MMM DD, YYYY'));
            $('#search-dates').daterangepicker({
                startDate: tz_date(moment()),
                endDate: tz_date(moment()),
                opens: 'center',
                drops: 'down',
                autoUpdateInput: true
            },
                    function (start, end, label) {
                        $('#search-dates .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));
                    });
            var dualListBox = $('#search-employees').bootstrapDualListbox({
                selectorMinimalHeight: 130
            });
            var dualListContainer = dualListBox.bootstrapDualListbox('getContainer');
            dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
            dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
            dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
            dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');

            $("#btn-download").click(function () {
                var datestart = $("#search-dates").data('daterangepicker').startDate;
                var dateend = $("#search-dates").data('daterangepicker').endDate;
                var start_str = tz_date(moment(datestart)).valueOf();
                var end_str = tz_date(moment(dateend)).valueOf();
                var employeelist = $("#search-employees").val();
                var emplist = employeelist.toString().split(",");
                var employee_str = emplist.join('-');
                if (employee_str === '') {
                    swal("Warning", "No Employee selected", 'warning');
                } else {
                    window.location.href = "<?php echo base_url(); ?>Schedule/downloadExcel/" + start_str + "/" + end_str + "/" + employee_str;
                }
            });
            $('#input-upload input').change(function () {
                $('#input-upload p').text(this.files.length + " file(s) selected");
            });

            getDownloadEmployees();
            get_all_accounts(function (res) {
                $("#search-accounts").html("<option value=''>All</option>");
                $.each(res.accounts, function (i, item) {
                    $("#search-accounts").append("<option value='" + item.acc_id + "' data-class='" + item.acc_description + "'>" + item.acc_name + "</option>");
                });
            });
            $("#search-class").change(function () {
                var theclass = $(this).val();
                $("#search-accounts").children('option').css('display', '');
                $("#search-accounts").val('').change();
                if (theclass !== '') {
                    $("#search-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
                }
                getDownloadEmployees();
            });
            $("#search-accounts").change(function () {
                getDownloadEmployees();
            });
            //-----------------------------------------------------------------------------

            $('#drag-and-drop-zone').dmUploader({//
                url: '<?php echo base_url('Schedule/excel_upload') ?>',
                maxFileSize: 9000000, // 3 Megs 
                multiple: false,
                allowedTypes: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                extFilter: ["xlsx", "xls", "xlsm"],
                onDragEnter: function () {
                    // Happens when dragging something over the DnD area
                    this.addClass('active');
                },
                onDragLeave: function () {
                    // Happens when dragging something OUT of the DnD area
                    this.removeClass('active');
                },
                onComplete: function () {
                    // All files in the queue are processed (success or error)
                    ui_add_log('End of Excel Reader');
                },
                onNewFile: function (id, file) {
                    // When a new file is added using the file selector or the DnD area
                    ui_multi_add_file(id, file);
                },
                onBeforeUpload: function (id) {
                    // about tho start uploading a file
                    ui_add_log('Start of Excel Reader');
                    ui_multi_update_file_status(id, 'reading', 'Reading...');
                    ui_multi_update_file_progress(id, 0, '', true);
                },
                onUploadCanceled: function (id) {
                    // Happens when a file is directly canceled by the user.
                    ui_multi_update_file_status(id, 'warning', 'Canceled by User');
                    ui_multi_update_file_progress(id, 0, 'warning', false);
                },
                onUploadProgress: function (id, percent) {
                    // Updating file progress
                    ui_multi_update_file_progress(id, percent);
                },
                onUploadSuccess: function (id, data) {
                    // A file was successfully uploaded
                    ui_add_log('Done reading the template', 'success');
                    ui_multi_update_file_status(id, 'success', 'Reading Complete');
                    ui_multi_update_file_progress(id, 100, 'success', false);
                    excel_preview();
                },
                onUploadError: function (id, xhr, status, message) {
                    ui_multi_update_file_status(id, 'danger', message);
                    console.log(message);
                    ui_multi_update_file_progress(id, 0, 'danger', false);
                    ui_add_log('Schedule Template Mismatch', 'danger');
                },
                onFileSizeError: function (file) {
                    ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
                },
                onFileTypeError: function (file) {
                    ui_add_log('File \'' + file.name + '\' cannot be added: must be an excel (EXCEL 2007+)', 'danger');
                },
                onFileExtError: function (file) {
                    ui_add_log('File \'' + file.name + '\' cannot be added: must be an excel (extension error)', 'danger');
                }
            });

            $('#btn-upload').click(function () {
                swal({
                    title: 'Are you sure to upload?',
                    text: "This will update the existing schedules",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, Upload!'
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>Schedule/excel_submit",
                            cache: false,
                            beforeSend: function () {
                                mApp.block('body', {
                                    overlayColor: '#000000',
                                    type: 'loader',
                                    state: 'brand',
                                    size: 'lg'
                                });
                            },
                            success: function (result) {
                                result = JSON.parse(result.trim());
                                if (result.status === 'Success') {
                                    swal(
                                            'Uploaded!',
                                            'Your file has been successfully uploaded.',
                                            'success'
                                            );
                                } else {
                                    swal(
                                            'Error!',
                                            'An error occurred while uploading schedule.',
                                            'danger'
                                            );
                                }
                                $("#table-preview thead").html("");
                                $("#table-preview tbody").html("");
                                $("#btn-upload").hide();

                                mApp.unblock('body');
                            }
                        });

                    }
                });
            });

        });

</script>

