<style type="text/css">
    body {
        padding-right: 0 !important;
    }

    .form-control {
        font-size: 12px;
    }

    #table-schedulebreaks tr:hover td {
        background: #dfdfdf;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Schedule Module</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Schedule/settings'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Settings</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head" style="background:#4B4C54;">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text" style="color:white !important">
                            Settings <small style="color:#eee !important;font-family:Poppins">Shift List and Schedule Breaks</small>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link btn btn-light m-btn m-btn--air m-btn--icon m-btn--pill" data-toggle="modal" data-target="#shiftModal">
                                <i class="la la-plus-circle"></i> Create New Shift
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-md-3">
                        <select class="form-control m-input m-input--square" id="break-class">
                            <option value="">All</option>
                            <option value="Admin">SZ Team</option>
                            <option value="Agent">Ambassador</option>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select class="form-control m-input m-input--square" id="break-accounts"></select>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control m-input m-input--square" id="break-search" placeholder="Search Shift Start or End">
                    </div>
                </div>
                <hr>
                <table class="table table-sm table-striped table-hover" id="table-schedulebreaks">
                    <thead class="thead-inverse">
                        <tr style="background:#4B4C54;color:white">
                            <th>#</th>
                            <th>Shift Start</th>
                            <th>Shift End</th>
                            <th class="text-center">Total</th>
                            <th class="text-center">First Break</th>
                            <th class="text-center">Lunch Break</th>
                            <th class="text-center">Last Break</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="breakModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change shift break</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="breakModal-title" class="text-center m--font-bold"></p>
                <h4 id="breakModal-shift" class="text-center"></h4>
                <hr>
                <div class="row text-center">
                    <div class="col-md-4">
                        <label for="" class="m--font-bolder">First Break:</label>
                        <select name="" id="breakModal-firstb" class="form-control"></select>
                    </div>
                    <div class="col-md-4">
                        <label for="" class="m--font-bolder">Lunch</label>
                        <select name="" id="breakModal-lunchb" class="form-control"></select>
                    </div>
                    <div class="col-md-4">
                        <label for="" class="m--font-bolder">Last Break</label>
                        <select name="" id="breakModal-lastb" class="form-control"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-warning m--font-bold" id="breakModal-save">Save Changes</button>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="shiftModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create New Shift</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-header text-center m--font-bolder" style="font-size:14px;background:#505a6b;color:white">SHIFT TIME</div>
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-md-6">
                                <label for="" class="m--font-bolder"> Start Time:</label>
                                <input type="text" class="form-control text-center" style="font-size:14px" id="timepicker-start" readonly="" placeholder="Select start time">
                            </div>
                            <div class="col-md-6">
                                <label for="" class="m--font-bolder"> End Time:</label>
                                <input type="text" class="form-control text-center" style="font-size:14px" id="timepicker-end" readonly="" placeholder="Select end time">
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header text-center m--font-bolder" style="font-size:14px;background:#505a6b;color:white">DEFAULT SHIFT BREAK</div>
                    <div class="card-body">
                        <div class="row text-center">
                            <div class="col-md-4">
                                <label for="" class="m--font-bolder">First Break:</label>
                                <select name="" id="shiftModal-firstb" class="form-control"></select>
                            </div>
                            <div class="col-md-4">
                                <label for="" class="m--font-bolder">Lunch</label>
                                <select name="" id="shiftModal-lunchb" class="form-control"></select>
                            </div>
                            <div class="col-md-4">
                                <label for="" class="m--font-bolder">Last Break</label>
                                <select name="" id="shiftModal-lastb" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-brand m--font-bold" id="breakShift-create">Create Shift</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function get_available_break_times(callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Schedule/get_available_break_times",
            cache: false,
            success: function(result) {
                result = JSON.parse(result.trim());
                callback(result);
            }
        });
    }
    var getScheduleBreaks = function() {
        var acc_id = $("#break-accounts").val();
        var search_shift = $("#break-search").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Schedule/getScheduleBreaks",
            data: {
                acc_id: acc_id,
                search_shift: search_shift
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#table-schedulebreaks tbody").html('');
                var dummydate = moment().format("YYYY-MM-DD");
                var dummydate2 = moment().add(1, 'day').format("YYYY-MM-DD");
                var cnt = 1;
                $.each(res.shifts, function(i, shift) {

                    var orig_start = moment(dummydate + " " + shift.time_start, "YYYY-MM-DD h:mm:ss A");
                    var orig_end = moment(dummydate + " " + shift.time_end, "YYYY-MM-DD h:mm:ss A");
                    var orig_end2 = moment(dummydate2 + " " + shift.time_end, "YYYY-MM-DD h:mm:ss A");
                    var format_start = orig_start.format("hh:mm a");
                    var format_end = orig_end.format("hh:mm a");
                    var difference = moment(orig_end).valueOf() - moment(orig_start).valueOf();
                    if (orig_end - orig_start < 0) {
                        difference = moment(orig_end2).valueOf() - moment(orig_start).valueOf();
                    }
                    difference = difference / (60 * 60 * 1000);
                    var hrs = parseInt(Number(difference));
                    var min = Math.round((Number(difference) - hrs) * 60);

                    if (min === 0) {
                        var ext = (hrs === 1) ? "hour" : "hrs";
                        var disp = hrs + " " + ext;
                    } else if (hrs === 0) {
                        var ext = (min === 1) ? "min" : "mins";
                        var disp = min + " " + ext;
                    } else {
                        var ext = (hrs === 1) ? "hour" : "hrs";
                        var ext2 = (min === 1) ? "min" : "mins";
                        var disp = hrs + " " + ext + ' & ' + min + " " + ext2;
                    }

                    var string = '<tr>\
                    <td>' + cnt + '</td>\
                    <td class="m--font-bolder" style="letter-spacing: 1px;">' + format_start + '</td>\
                    <td class="m--font-bolder" style="letter-spacing: 1px;">' + format_end + '</td>\
                    <td class="text-center m--font-bold">' + disp + '</td>\
                    <td class="text-center m--font-bolder">' + shift.min_firstb + '</td>\
                    <td class="text-center m--font-bolder">' + shift.min_lunchb + '</td>\
                    <td class="text-center m--font-bolder">' + shift.min_lastb + '</td>\
                    <td class="text-center">\
                    <button class="btn btn-outline-success btn-sm m-btn m-btn--icon m-btn--custom" data-start="' + format_start + '" data-end="' + format_end + '"  data-acctimeid="' + shift.acc_time_id + '" style="padding:2px 10px" data-toggle="modal" data-target="#breakModal">\
                    <i class="la la-pencil"></i> Edit\
                    </button></td>\
                    </tr>';
                    $("#table-schedulebreaks tbody").append(string);
                    cnt++;
                });
            }
        });
    }
    var format_breaktime = function(obj) {
        var finalBreak = "";
        var string = "";
        if (obj.hour == '0' && obj.min == '0') {
            finalBreak = "0 mins";
        } else if (obj.hour == '0' && obj.min != '0') {
            var ext = (obj.min === '1') ? ' min' : ' mins';
            finalBreak = obj.min + ext;
        } else if (obj.hour != '0' && obj.min == '0') {
            var ext = (obj.hour === '1') ? ' hour' : ' hrs';
            finalBreak = obj.hour + ext;
        } else {
            var ext = (obj.min === '1') ? ' min' : ' mins';
            var ext2 = (obj.hour === '1') ? ' hour' : ' hrs';
            finalBreak = obj.hour + " " + ext + " & " + obj.min + ' ' + ext;
        }
        string = '<option value="' + obj.bta_id + '">' + finalBreak + '</option>';
        return string;
    }
    $(function() {
        $('#timepicker-start,#timepicker-end').timepicker({
            defaultTime: '12:00 AM'
        });
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>Schedule/getUniqueEmpAcc_ids/1",
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#break-accounts").html("<option value=''>--</option>");
                $.each(res.accounts, function(i, acc) {
                    $("#break-accounts").append('<option value="' + acc.acc_id + '" data-class="' + acc.acc_description + '">' + acc.acc_name + '</option>');
                });
                //CONTINUATION
                get_available_break_times(function(available) {
                    var firstb = '<option value="">--</option>';
                    var lunchb = '<option value="">--</option>';
                    var lastb = '<option value="">--</option>';
                    $.each(available.firstb, function(i, obj) {
                        firstb += format_breaktime(obj);
                    });
                    $.each(available.lunchb, function(i, obj) {
                        lunchb += format_breaktime(obj);
                    });
                    $.each(available.lastb, function(i, obj) {
                        lastb += format_breaktime(obj);
                    });
                    $("#breakModal-firstb").html(firstb);
                    $("#breakModal-lunchb").html(lunchb);
                    $("#breakModal-lastb").html(lastb);
                    $("#shiftModal-firstb").html(firstb);
                    $("#shiftModal-lunchb").html(lunchb);
                    $("#shiftModal-lastb").html(lastb);
                });
            }
        });
    })

    $("#break-class").change(function() {
        var theclass = $(this).val();
        $("#break-accounts").children('option').css('display', '');
        $("#break-accounts").val('').change();
        if (theclass !== '') {
            $("#break-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
        }
    });
    $("#break-search").on('keypress',function() {
        getScheduleBreaks();
    })
    $("#break-accounts").change(function() {
        if ($(this).val() !== '') {
            $("#breakModal-title").html($(this).find(":selected").text());
            getScheduleBreaks();
        } else {
            $("#table-schedulebreaks tbody").html("");
        }
    })
    $("#breakModal").on('show.bs.modal', function(e) {
        if (e.namespace === 'bs.modal') {
            var acc_time_id = $(e.relatedTarget).data('acctimeid');
            var start = $(e.relatedTarget).data('start');
            var end = $(e.relatedTarget).data('end');
            $("#breakModal-save").data('acctimeid', acc_time_id);
            $("#breakModal-shift").html(start + " " + end);
            $("#breakModal-firstb").val('');
            $("#breakModal-lunchb").val('');
            $("#breakModal-lastb").val('');
            $("#breakModal-firstb").data('orig_value', '');
            $("#breakModal-lunchb").data('orig_value', '');
            $("#breakModal-lastb").data('orig_value', '');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Schedule/get_used_shift_breaks",
                data: {
                    acc_time_id: acc_time_id
                },
                cache: false,
                success: function(json) {
                    json = JSON.parse(json.trim());
                    if (json.firstb !== null) {
                        $("#breakModal-firstb").val(json.firstb.bta_id);
                        $("#breakModal-firstb").data('orig_value', json.firstb.bta_id);
                    }
                    if (json.lunchb !== null) {
                        $("#breakModal-lunchb").val(json.lunchb.bta_id);
                        $("#breakModal-lunchb").data('orig_value', json.lunchb.bta_id);
                    }
                    if (json.lastb !== null) {
                        $("#breakModal-lastb").val(json.lastb.bta_id);
                        $("#breakModal-lastb").data('orig_value', json.lastb.bta_id);
                    }
                }
            });
        }
    });
    $("#breakModal-save").click(function() {
        var acc_time_id = $(this).data('acctimeid');
        var firstb_val = $("#breakModal-firstb").val();
        var lunchb_val = $("#breakModal-lunchb").val();
        var lastb_val = $("#breakModal-lastb").val();
        var firstb_orig = $("#breakModal-firstb").data('orig_value');
        var lunchb_orig = $("#breakModal-lunchb").data('orig_value');
        var lastb_orig = $("#breakModal-lastb").data('orig_value');
        console.log(firstb_val + '!==' + firstb_orig + '||' + lunchb_val + '!==' + lunchb_orig + '||' + lastb_val + '!==' + lastb_orig);
        if (firstb_val !== firstb_orig || lunchb_val !== lunchb_orig || lastb_val !== lastb_orig) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Schedule/update_shift_break",
                data: {
                    firstb: firstb_val,
                    lunchb: lunchb_val,
                    lastb: lastb_val,
                    acc_time_id: acc_time_id
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        $.notify({
                            message: 'Successfully change account shift break'
                        }, {
                            type: 'success',
                            timer: 1000,
                            z_index: 2000
                        });
                    } else {
                        $.notify({
                            message: 'An error occured while processing your actions'
                        }, {
                            type: 'danger',
                            timer: 1000,
                            z_index: 2000
                        });
                    }
                    $("#break-accounts").change();
                }
            });
            $("#breakModal").modal("hide");
        } else {
            $.notify({
                message: 'No changes found'
            }, {
                type: 'info',
                timer: 1000,
                z_index: 2000
            });
        }

    });

    function check_time_exists(time_start, time_end, callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Schedule/check_time_exists",
            data: {
                time_start: time_start,
                time_end: time_end
            },
            cache: false,
            success: function(result) {
                result = JSON.parse(result.trim());
                callback(result);
            }
        });
    }
    $("#breakShift-create").click(function() {
        var time_start = $("#timepicker-start").val();
        var time_end = $("#timepicker-end").val();
        var firstb_val = $("#shiftModal-firstb").val();
        var lunchb_val = $("#shiftModal-lunchb").val();
        var lastb_val = $("#shiftModal-lastb").val();
        if (time_start === time_end) {
            $.notify({
                message: 'Cannot create a 24 hrs shift...'
            }, {
                type: 'warning',
                timer: 1000,
                z_index: 2000
            });
        } else {
            check_time_exists(time_start, time_end, function(res) {
                if (parseInt(res.count) > 0) {
                    $.notify({
                        message: 'Shift already exists...'
                    }, {
                        type: 'warning',
                        timer: 1000,
                        z_index: 2000
                    });
                } else {
                    var ts = time_start.split(" ");
                    var te = time_end.split(" ");
                    var datestart = moment().format('L');

                    var st = moment(new Date(datestart + " " + time_start));
                    var en = moment(new Date(datestart + " " + time_end));
                    if (en - st < 0) {
                        var dateend = moment().add(1, 'days').format('L'); //Next Day
                    } else {
                        var dateend = moment().format('L'); //Same Day
                    }

                    var datestart = new Date(datestart + " " + time_start);
                    var dateend = new Date(dateend + " " + time_end);
                    var difference = moment(dateend).valueOf() - moment(datestart).valueOf();
                    difference = difference / (60 * 60 * 1000);
                    var hrs = parseInt(Number(difference));
                    var min = Math.round((Number(difference) - hrs) * 60);
                    if (min === 0) {
                        var ext = (hrs === 1) ? "hour" : "hrs";
                        var disp = hrs + " " + ext;
                    } else if (hrs === 0) {
                        var ext = (min === 1) ? "min" : "mins";
                        var disp = min + " " + ext;
                    } else {
                        var ext = (hrs === 1) ? "hour" : "hrs";
                        var ext2 = (min === 1) ? "min" : "mins";
                        var disp = hrs + " " + ext + ' & ' + min + " " + ext2;
                    }

                    var withBreak = "";
                    if (firstb_val === '' && lunchb_val === '' && lastb_val === '') {
                        withBreak = "with no default breaks";
                    }
                    swal({
                        title: 'Are you sure to create shift ?',
                        html: "Shift covers <b>" + disp + "</b> of work " + withBreak,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, Create!'
                    }).then(function(result) {
                        if (result.value) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>index.php/Schedule/create_shift",
                                data: {
                                    firstb: firstb_val,
                                    lunchb: lunchb_val,
                                    lastb: lastb_val,
                                    time_start: time_start,
                                    time_end: time_end
                                },
                                cache: false,
                                success: function(res) {
                                    res = JSON.parse(res.trim());
                                    if (res.status === 'Success') {
                                        $.notify({
                                            message: 'Successfully created shift'
                                        }, {
                                            type: 'success',
                                            timer: 1000,
                                            z_index: 2000
                                        });
                                    } else {
                                        $.notify({
                                            message: 'An error occured while processing your actions'
                                        }, {
                                            type: 'danger',
                                            timer: 1000,
                                            z_index: 2000
                                        });
                                    }

                                    $("#break-accounts").change();
                                }
                            });
                            $("#shiftModal").modal("hide");
                        }
                    });
                }
            });


}

});
</script>