<style type="text/css">

    body{
        padding-right:0 !important;
    }
    .font-white{
        color:white !important;
    }
    .blockOverlay{
        opacity:0.15 !important;
        background-color:#746DCB !important;
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }
    .employee-pic{
        width:45px !important
    }
    .custom-badge{
        /*border-radius:0 !Important;*/
        font-size:11px;
        padding: 1px 5px !important;
        font-weight:500;
        display:block !important;
        margin:3px !important;
        text-overflow: ellipsis;
    }
    #table-schedule { border: none; border-collapse: collapse; }
    #table-schedule td { border-left: 1px solid #DBD9DB;padding:10px 5px }
    #table-schedule td:first-child { border-left: none;padding:0.75rem }
    #table-schedule th { border-left: 1px solid #bfbfbf;}
    #table-schedule th:first-child { border-left: none; }

    span[id*="weekday"] {
        font-size:13px !important;
        color:#fff
    }
    #table-schedule tbody td:not(:first-child):hover{
        background:#f3efcd;
        cursor: pointer;
    }
    .element-disable td{
        pointer-events: none;
    }
    .select2-container{
        width:100% !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: 0.9;
        font-size:13px;
        color:black;
    }
    .select2-container--default .select2-results>.select2-results__options{
        font-size:12px;
    }
    .tooltip-inner {
        color: #ddd !important;
        font-size:13px !important;
    }
    .m-table.m-table--border-metal th{
        border: none;
    }
    .m-table.m-table--border-metal, .m-table.m-table--border-metal td {
        border-color: #eaeaea;
    }
    .table-hover tbody tr:hover {
        background-color: #e4e4e4 !important;
    }
    .select-leavetype,#select-leavetype-new{
        margin-top:4px;
    }
    .form-control{
        font-size: 12px !important;
        line-height: 1.5 !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Schedule Module</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Schedule'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Schedule Manual</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tabs m-portlet--success m-portlet--head-solid-bg m-portlet--head-sm" id="portlet">
                    <div class="m-portlet__head" style="background:#4B4C54;border-color:#4B4C54">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text" style="color:white !Important">
                                    Schedule Manual
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <a href="#"class="btn btn-outline-dark" id="btn-prev" style="padding: 10px 20px;font-size: 13px;background:white">
                                <i class="la la-caret-left"></i>
                                Prev
                            </a>
                            <a href="#"class="btn btn-outline-dark" id="btn-next" style="padding: 10px 20px;font-size: 13px;background:white">
                                Next
                                <i class="la la-caret-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="padding: 15px">
                        <div class="row" style="padding: 5px 16px;">

                            <div class="col-xl-3 col-lg-6 col-md-4 col-sm-6 col-12">
                                <label for="" class="m--font-bolder mt-2">DATE RANGE</label>
                                <br />
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control" id="search-dates" readonly="" placeholder="Select date">
                                    <span class="m-input-icon__icon m-input-icon__icon--right" style="font-size: 12px"><span><i class="fa flaticon-calendar"></i></span></span>
                                </div>

                            </div>
                            <div class="col-xl-2 col-lg-6 col-md-4 col-sm-6 col-12"> 
                                <label for="" class="m--font-bolder mt-2">CLASS</label>
                                <br />
                                <select name="" class="m-input form-control" id="search-class">
                                    <option value="">All Class</option>
                                    <option value="Agent">Ambassador</option>
                                    <option value="Admin">SZ Team</option>
                                </select>
                            </div>
                            <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-12">
                                <label for="" class="m--font-bolder mt-2">ACCOUNTS</label>
                                <br />
                                <select name="" class="m-input form-control" id="search-accounts">
                                </select>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-md-4 col-sm-6 col-12">
                                <label for="" class="m--font-bolder mt-2">NAME SEARCH</label>
                                <br />
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input" placeholder="Search Name..." id="search-name"/>
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-center" style="color:#1F4E7B;font-weight:bold" id="weekstring"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="width:100%;overflow-x: auto">
                                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" style="display:none" role="alert" id="schedule-alert">
                                        <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                        <div class="m-alert__text"><strong>No Record Found! </strong> <br> Please adjust your search criteria and try again.
                                        </div>
                                    </div>
                                    <table class="table m-table  m-table--head-separator-metal table-hover" id="table-schedule" style="display:none">
                                        <thead class="thead-inverse">
                                            <tr style="background: #4b4c54;text-align:center;font-size:12px;font-weight:600 !important;color: #fff;">
                                                <th style="vertical-align:middle"><p style="font-size:15px;margin-bottom:0">Fullname</p></th>
                                                <th style="min-width:50px"><span id="weekday0"></span> <span style="color:#9a9a9a;font-size:11px"> Sunday </span></th>
                                                <th style="min-width:50px"><span id="weekday1"></span> <span style="color:#9a9a9a;font-size:11px"> Monday </span></th>
                                                <th style="min-width:50px"><span id="weekday2"></span> <span style="color:#9a9a9a;font-size:11px"> Tuesday </span></th>
                                                <th style="min-width:50px"><span id="weekday3"></span> <span style="color:#9a9a9a;font-size:11px"> Wednesday </span></th>
                                                <th style="min-width:50px"><span id="weekday4"></span> <span style="color:#9a9a9a;font-size:11px"> Thursday </span></th>
                                                <th style="min-width:50px"><span id="weekday5"></span> <span style="color:#9a9a9a;font-size:11px"> Friday </span></th>
                                                <th style="min-width:50px"><span id="weekday6"></span> <span style="color:#9a9a9a;font-size:11px"> Saturday </span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="schedule-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Employee Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-stack m-stack--ver m-stack--general">
                    <div class="m-stack__item m-stack__item--middle" style='border:1px solid #eaeaea;width:60%'>
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img"  id="employee-pic" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="employee-name" style="font-size:20px;color:black">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="employee-job" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-stack__item m-stack__item--center m-stack__item--middle" style='border:1px solid #eaeaea;border-left:none'>
                        <span style="font-size:20px;" class=" m--font-bolder" id='schedule-date'></span><br />
                        <span style="font-size:13px;" class=" m--font-metal">Schedule Date</span>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div style="width:100%;overflow-x: auto">
                            <table class="table m-table table-bordered m-table--border-metal" id="table-schedule-date">
                                <thead style='background:#505A6B;color:white' class='text-center'>
                                <th style="width:8px">#</th>
                                <th style="width:50%" colspan="2">Schedule Shift</th>
                                <th>Uploaded via</th>
                                <th>Uploaded by</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/js/lazyload.min.js" ></script>
<!--end::Modal-->
<script type="text/javascript">
        function get_all_accounts(callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Schedule/getUniqueEmpAcc_ids",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    callback(result);
                }
            });
        }
        function tz_date(thedate, format = null) {
            var tz = moment.tz(new Date(thedate), "Asia/Manila");
            return (format === null) ? moment(tz) : moment(tz).format(format);
        }
        function getSchedStartEnd(date, time_start, time_end, callback) {
            var start_minutes = moment(date + ' ' + time_start, 'YYYY-MM-DD h:mm:ss A').format("mm");
            var end_minutes = moment(date + ' ' + time_end, 'YYYY-MM-DD h:mm:ss A').format("mm");
            if (start_minutes === '00') {
                var sched_start = moment(date + ' ' + time_start, 'YYYY-MM-DD h:mm:ss A').format("ha");
            } else {
                var sched_start = moment(date + ' ' + time_start, 'YYYY-MM-DD h:mm:ss A').format("h:mma");
            }
            if (end_minutes === '00') {
                var sched_end = moment(date + ' ' + time_end, 'YYYY-MM-DD h:mm:ss A').format("ha");
            } else {
                var sched_end = moment(date + ' ' + time_end, 'YYYY-MM-DD h:mm:ss A').format("h:mma");
            }
            callback(sched_start, sched_end);
        }
        var enumerateDaysBetweenDates = function (startDate, endDate) {
            var now = startDate, dates = [];
            while (now.isSameOrBefore(endDate)) {
                dates.push(now.format('YYYY-MM-DD'));
                now.add(1, 'days');
            }
            return dates;
        };

        function getEmployeeSchedules(date_list) {
            var startDateStr = date_list[0];
            var endDateStr = date_list[date_list.length - 1];
            var searchname = $("#search-name").val();
            var searchclass = $("#search-class").val();
            var searchaccounts = $("#search-accounts").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Schedule/get_employee_schedules",
                data: {
                    startDate: startDateStr,
                    endDate: endDateStr,
                    searchname: searchname,
                    searchclass: searchclass,
                    searchaccounts: searchaccounts
                },
                beforeSend: function () {
                    mApp.block('#portlet', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    $("#table-schedule tbody").html("");
                    var string = "";
                    $.each(result.data, function (key, item) {
                        string += '<tr>';
                        string += '<td style="vertical-align:middle;"><div class="m-widget4"><div class="m-widget4__item">';
                        string += '<div class="m-widget4__img m-widget4__img--logo"><img data-src="<?php echo base_url(); ?>' + item.details.pic + '" class="employee-pic" alt=""></div>';
                        string += '<div class="m-widget4__info"><span class="m-widget4__title">' + item.details.lname + '</span><br> <span class="m-widget4__sub">' + item.details.fname + '</span></div>';
                        string += '</div></div></td>';
                        $.each(date_list, function (index, date) {
                            var isOkay = false;
                            $.each(item.schedule, function (x, sched) {
                                if (date === x) {
                                    isOkay = true;
                                    if (sched.length === 1) {
                                        var sched0 = sched[0];
                                        if (sched0.time_start === null) {
                                            if (sched0.schedtype_id === '3' || sched0.schedtype_id === '8') {
                                                string += '<td data-date="' + date + '" title="' + moment(date).format('ddd MMM D, YYYY') + '" data-empid="' + item.details.emp_id + '" style="vertical-align:middle;text-align:center" data-toggle="modal" data-target="#schedule-modal"><p  data-sched_id="' + sched0.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + sched0.style + ';color:white;font-size:10px">' + sched0.leaveType + '' + sched0.type.replace('Leave', '') + '</p></td>';
                                            } else {
                                                string += '<td data-date="' + date + '" title="' + moment(date).format('ddd MMM D, YYYY') + '" data-empid="' + item.details.emp_id + '" style="vertical-align:middle;text-align:center" data-toggle="modal" data-target="#schedule-modal"><p  data-sched_id="' + sched0.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + sched0.style + ';color:white">' + sched0.type + '</p></td>';
                                            }


                                        } else {
                                            getSchedStartEnd(date, sched0.time_start, sched0.time_end, function (sched_start, sched_end) {
                                                string += '<td data-date="' + date + '" title="' + moment(date).format('ddd MMM D, YYYY') + '" data-empid="' + item.details.emp_id + '" style="vertical-align:middle;text-align:center" data-toggle="modal" data-target="#schedule-modal"><p  data-sched_id="' + sched0.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + sched0.style + ';color:white">' + sched_start + ' - ' + sched_end + '</p></td>';
                                            });
                                        }
                                    } else {
                                        var substring = '<td data-date="' + date + '" title="' + moment(date).format('ddd MMM D, YYYY') + '" data-empid="' + item.details.emp_id + '" style="vertical-align:middle;text-align:center" data-toggle="modal" data-target="#schedule-modal">';
                                        var x = 1;
                                        $.each(sched, function (y, subsched) {
                                            if (subsched.time_start === null) {
                                                if (subsched.schedtype_id === '3' || subsched.schedtype_id === '8') {
                                                    substring += '<p  data-sched_id="' + subsched.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + subsched.style + ';color:white;font-size:10px">' + subsched.leaveType + '' + subsched.type.replace('Leave', '') + '</p>';
                                                } else {
                                                    substring += '<p  data-sched_id="' + subsched.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + subsched.style + ';color:white">' + subsched.type + '</p>';
                                                }

                                            } else {
                                                getSchedStartEnd(date, subsched.time_start, subsched.time_end, function (sched_start, sched_end) {
                                                    substring += '<p  data-sched_id="' + subsched.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + subsched.style + ';color:white">' + sched_start + ' - ' + sched_end + '</p>';
                                                });
                                            }
//                                            if (x !== sched.length) {
//                                                substring += '<br />';
//                                            }
                                            x++;
                                        });
                                        substring += '</td>';
//                                        string += '<td style="vertical-align:middle;text-align:center"><span class="m-badge m-badge--wide custom-badge">Multiple Schedule</span></td>';
                                        string += substring;
                                    }
                                }
                            });
                            if (isOkay === false) {
                                string += '<td data-date="' + date + '" title="' + moment(date).format('ddd MMM D, YYYY') + '" data-empid="' + item.details.emp_id + '" style="vertical-align:middle;text-align:center;color:#ccc" data-toggle="modal" data-target="#schedule-modal">No Schedule</td>';
//                                string += '<td style="vertical-align:middle;text-align:center"><a href="#" class="btn btn-outline-metal m-btn m-btn--icon m-btn--icon-only m-btn--pill btn-sm m-btn--air"><i class="la la-plus"></i></a></td>';
                            }
                        });

                        string += '</tr>';
                    });
                    if (string === '') {
                        $("#schedule-alert").show();
                        $("#table-schedule").hide();
                    } else {
                        $("#schedule-alert").hide();
                        $("#table-schedule").show();
                    }
                    $("#table-schedule tbody").html(string);

                    var myLazyLoad = new LazyLoad({
                        elements_selector: ".employee-pic"
                    });
                    $('.employee-pic').on('error', function () {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                        $(this).attr('data-src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                    mApp.unblock('#portlet');
                    mApp.initTooltips();
                }
            });
        }
        function saveupdate_checker(schedtype_id, withSchedules, time_id, isOkay, fullhalf_remarks, remarks, leaveType_ID, callback) {
            if (schedtype_id === '') {
                isOkay = false;
                $.notify({
                    message: 'Schedule type is required'
                }, {
                    type: 'warning',
                    timer: 1000,
                    z_index: 2000
                });
            } else {
                if (jQuery.inArray(parseInt(schedtype_id), withSchedules) !== -1) {//exists
                    if (time_id === '') {
                        isOkay = false;
                        $.notify({
                            message: 'Shift is required'
                        }, {
                            type: 'warning',
                            timer: 1000,
                            z_index: 2000
                        });
                    }
                } else {
                    if (time_id !== '') {
                        isOkay = false;
                        $.notify({
                            message: 'Shift should be empty'
                        }, {
                            type: 'warning',
                            timer: 1000,
                            z_index: 2000
                        });
                    }
                }
                if (schedtype_id === '3' || schedtype_id === '8') {
                    if (fullhalf_remarks === undefined) {
                        isOkay = false;
                        $.notify({
                            message: 'Full or Half leave selection is required'
                        }, {
                            type: 'warning',
                            timer: 1000,
                            z_index: 2000
                        });
                    } else if (leaveType_ID === '') {
                        isOkay = false;
                        $.notify({
                            message: 'Leave type is required'
                        }, {
                            type: 'warning',
                            timer: 1000,
                            z_index: 2000
                        });
                    } else {
                        remarks = fullhalf_remarks;
                    }
                }
            }
            callback(isOkay, remarks);
        }
        $(function () {
            var restrictedSchedTypes = [4,5,3,8];//change this if word lang sa
            // var restrictedSchedTypes = [4,5];
            var withSchedules = [1, 4, 5];
            var previousdateinput = "";

            get_all_accounts(function (res) {
                $("#search-accounts").html("<option value=''>All</option>");
                $.each(res.accounts, function (i, item) {
                    $("#search-accounts").append("<option value='" + item.acc_id + "' data-class='" + item.acc_description + "'>" + item.acc_name + "</option>");
                });
            });
            $("#search-class").change(function () {
                var theclass = $(this).val();
                $("#search-accounts").children('option').css('display', '');
                $("#search-accounts").val('').change();
                if (theclass !== '') {
                    $("#search-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
                }
                $('#search-dates').datepicker().trigger('changeDate');
            });
            $("#search-accounts").change(function () {
                $('#search-dates').datepicker().trigger('changeDate');
            });
            $('#search-dates').datepicker({
                setDate: tz_date(moment($(this).val())),
                autoclose: true,
                todayHighlight: true,
                orientation: "bottom left",
                format: 'yyyy-mm-dd',
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }).on('changeDate', function (e) {

                var date = tz_date(moment($('#search-dates').data('datepicker').dates[0]), 'YYYY-MM-DD');
                var startDate = moment(date).startOf('week');
                var endDate = moment(date).endOf('week');
                $(this).val(moment(startDate).format('MMM DD, YYYY') + " - " + moment(endDate).format('MMM DD, YYYY'));
                previousdateinput = moment(startDate).format('MMM DD, YYYY') + " - " + moment(endDate).format('MMM DD, YYYY');
                $("#weekstring").html("Week of " + moment(startDate).format('MMM. D') + " - " + moment(endDate).format('MMM. D, YYYY'));
                var date_list = enumerateDaysBetweenDates(startDate, endDate);
                $.each(date_list, function (key, item) {
                    $("#weekday" + key).html(tz_date(moment(item), 'MMM DD') + "<br />");
                    $.each($('.datepicker-days td'), function (x, elem) {
                        if (moment($(elem).data('date')).format('YYYY-MM-DD') === moment(item).format('YYYY-MM-DD')) {
                            $(elem).addClass('active');
                        }
                    });
                });
                getEmployeeSchedules(date_list);
            }).on('hide', function (e) {
                $(this).val(previousdateinput);
            });
//            $('#search-dates').datepicker('setDates', tz_date(moment(), 'YYYY-MM-DD'));
            $('#search-dates').datepicker().trigger('changeDate');
            $("#search-name").change(function () {
                $('#search-dates').datepicker().trigger('changeDate');
            });
            $("#btn-prev").on('click', function () {
                var date = tz_date(moment($('#search-dates').data('datepicker').dates[0]), 'YYYY-MM-DD');
                var startDate = moment(date).startOf('week');
                $('#search-dates').datepicker('setDate', tz_date(startDate.subtract(1, 'week'), 'YYYY-MM-DD'));
                $('#search-dates').val(previousdateinput);
            });
            $("#btn-next").on('click', function () {
                var date = tz_date(moment($('#search-dates').data('datepicker').dates[0]), 'YYYY-MM-DD');
                var startDate = moment(date).startOf('week');
                $('#search-dates').datepicker('setDate', tz_date(startDate.add(1, 'week'), 'YYYY-MM-DD'));
                $('#search-dates').val(previousdateinput);
            });
            $("#schedule-modal").on('show.bs.modal', function (e) {
                $("#table-schedule-date tbody").html("");
            });
            $("#schedule-modal").on('show.bs.modal', function (e) {
                var sched_date = $(e.relatedTarget).data('date');
                var emp_id = $(e.relatedTarget).data('empid');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Schedule/getDateSchedule",
                    data: {
                        sched_date: sched_date,
                        emp_id: emp_id
                    },
                    beforeSend: function () {
                        mApp.block('#portlet', {
                            overlayColor: '#000000',
                            type: 'loader',
                            state: 'brand',
                            size: 'lg'
                        });
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        var details = res.details;
                        var schedules = res.schedules;
                        $("#schedule-date").html(tz_date(moment(sched_date), "MMMM DD, YYYY"));
                        $("#schedule-date").data('date', sched_date);
                        $("#employee-name").html(details.fname + " " + details.lname);
                        $("#employee-pic").attr('src', '<?php echo base_url(); ?>' + details.pic);
                        $("#employee-job").html(details.job.position + " <small>(" + details.job.status + ")</small>");

                        var schedules_string = "";
                        var cnt = 0;
                        $.each(schedules, function (x, sched) {
                            cnt++;
                            if (sched.isLocked === '2') {
                                schedules_string += '<tr style="background:#ccc;" class="element-disable">';
                            } else if (sched.isLocked === '14') {
                                schedules_string += '<tr style="background:#ccc;" class="element-disable">';
                            } else {
                                if (jQuery.inArray(parseInt(sched.schedtype_id), restrictedSchedTypes) !== -1) {//exists
                                    schedules_string += '<tr style="background:#ccc;" class="element-disable">';
                                } else {
                                    schedules_string += '<tr>';
                                }
                            }
                            schedules_string += '<td style="vertical-align:middle;"  class="text-center"><b>' + cnt + '</b>.</td>';
                            var options_schedtype = "";
                            var options_shift = "<option value=''>--</option>";
                            var options_leaveTypes = "<option value=''>--</option>";
                            $.each(res.available_schedtypes, function (key, schedtype) {
                                if (jQuery.inArray(parseInt(schedtype.schedtype_id), restrictedSchedTypes) !== -1 && sched.schedtype_id === schedtype.schedtype_id) {
                                    options_schedtype += '<option value="' + schedtype.schedtype_id + '" disabled selected>' + schedtype.type + '</option>';
                                } else if (jQuery.inArray(parseInt(schedtype.schedtype_id), restrictedSchedTypes) !== -1) {
                                    options_schedtype += '<option value="' + schedtype.schedtype_id + '" disabled>' + schedtype.type + '</option>';
                                } else if (sched.schedtype_id === schedtype.schedtype_id) {
                                    options_schedtype += '<option value="' + schedtype.schedtype_id + '" selected>' + schedtype.type + '</option>';
                                } else {
                                    options_schedtype += '<option value="' + schedtype.schedtype_id + '">' + schedtype.type + '</option>';
                                }
                            });
                            $.each(res.available_shifts, function (key, shift) {
                                var sched_start = moment(sched_date + ' ' + shift.time_start, 'YYYY-MM-DD h:mm:ss A').format("hh:mma");
                                var sched_end = moment(sched_date + ' ' + shift.time_end, 'YYYY-MM-DD h:mm:ss A').format("hh:mma");
                                if (sched.time_id === shift.time_id) {
                                    options_shift += '<option value="' + shift.time_id + '" selected>' + sched_start + ' - ' + sched_end + '</option>';
                                } else {
                                    options_shift += '<option value="' + shift.time_id + '">' + sched_start + ' - ' + sched_end + '</option>';
                                }

                            });
                            $.each(res.available_leavetypes, function (key, leave) {
                                if (sched.leavenote === leave.leaveType_ID) {
                                    options_leaveTypes += '<option value="' + leave.leaveType_ID + '" selected>' + leave.leaveType + '</option>';
                                } else {
                                    options_leaveTypes += '<option value="' + leave.leaveType_ID + '">' + leave.leaveType + '</option>';
                                }
                            });

                            schedules_string += '<td style="vertical-align:middle;width:25%">';
                            schedules_string += '<select class="select-schedtype form-control m-select2">' + options_schedtype + '</select>';
                            var fullcheck = (sched.remarks.indexOf('Full') >= 0) ? "checked" : "";
                            var halfcheck = (sched.remarks.indexOf('Half') >= 0) ? "checked" : "";
                            var fullhalf_view = (fullcheck === '' && halfcheck === '') ? "display:none" : "";
                            schedules_string += '<div class="m-radio-inline fullhalf-parent" style="margin-top:5px;' + fullhalf_view + '" ><label class="m-radio"><input type="radio" name="fullhalf" value="Manual-Full" ' + fullcheck + '> Full<span></span></label><label class="m-radio"><input type="radio" name="fullhalf" value="Manual-Half" ' + halfcheck + '> Half<span></span></label></label></div>';

                            schedules_string += '</td>';
                            schedules_string += '<td style="vertical-align:middle;width:25%">';
                            if (jQuery.inArray(parseInt(sched.schedtype_id), withSchedules) !== -1) {
                                schedules_string += '<select class="select-shift form-control m-select2">' + options_shift + '</select>';
                            } else {
                                schedules_string += '<select class="select-shift form-control m-select2" disabled>' + options_shift + '</select>';
                            }

                            if (sched.schedtype_id === '3' || sched.schedtype_id === '8') {
                                schedules_string += '<select class="select-leavetype form-control form-control-sm">' + options_leaveTypes + '</select>';
                            } else {
                                schedules_string += '<select class="select-leavetype form-control form-control-sm" style="display:none">' + options_leaveTypes + '</select>';
                            }


                            schedules_string += '</td>';

                            if (sched.remarks.indexOf('Manual') >= 0) {
                                var remarks_icon = '<i class="fa fa-hand-o-up m--font-info"></i>';
                            } else if (sched.remarks === 'Excel') {
                                var remarks_icon = '<i class="fa fa-file-excel-o m--font-success"></i>';
                            } else {//request
                                var remarks_icon = '<i class="fa fa-user-plus m--font-brand"></i>';
                            }
                            var lock_status = "";
                            if (sched.isLocked === '2') {
                                lock_status = "<br /> <small class='m--font-boldest' style='color:'>Pending</small>";
                            } else if (sched.isLocked === '14') {
                                lock_status = "<br /> <small class='m--font-danger m--font-boldest'>Locked</small>";
                            }
                            schedules_string += '<td style="vertical-align:middle;font-size:11px;" class="text-center">' + remarks_icon + '<br>' + sched.remarks + ' ' + lock_status + '</td>';
                            schedules_string += '<td style="vertical-align:middle;font-size:12px;" class="text-center" data-skin="dark" data-toggle="m-tooltip" title="" data-original-title="' + moment(sched.updated_on).format('MMM DD, YYYY hh:mm a') + '">' + sched.last_updater + '</td>';
                            schedules_string += '<td style="vertical-align:middle;" class="text-center"><a href="#" data-toggle="m-tooltip" title="" data-skin="dark" data-sched_id="' + sched.sched_id + '" data-original-title="Save Update Changes" class="btn btn-sm btn-outline-success m-btn m-btn--icon m-btn--icon-only btn-update-schedule" style="width: 25px;height: 25px;margin-right:2px"><i class="fa fa-check"></i></a><a href="#"  data-sched_id="' + sched.sched_id + '" style="width: 25px;height: 25px;" data-toggle="m-tooltip" title="" data-skin="dark" data-original-title="Delete Schedule" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only  btn-delete-schedule"><i class="fa fa-close"></i></a></td>';
                            schedules_string += '</tr>';

                        });

                        //----------------------------FOR EMPTY SCHEDULES-----------------------------------------------------------------------------------------------------

                        var forAddSchedule = '<tr><td colspan=6 class="text-right"><a href="#" class="btn btn-outline-success btn-sm m-btn m-btn--icon" data-acc_id="' + details.acc_id + '" data-emp_id="' + details.emp_id + '" id="btn-create-schedule"><i class="fa fa-plus"></i> Create New</a></td></tr>';
                        if (schedules_string === '') {
                            $("#table-schedule-date tbody").html(forAddSchedule);
                        } else {
                            schedules_string = schedules_string + forAddSchedule;
                            $("#table-schedule-date tbody").html(schedules_string);
                            $('.select-shift,.select-schedtype').select2({dropdownParent: $('#schedule-modal')});
                        }
                        //disable select2 of locked schedules
                        $("#table-schedule-date tbody").find("tr.element-disable").find(".m-select2").prop('disabled', true);

                        $("#employee-pic").on('error', function () {
                            $(this).attr('onerror', null);
                            $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                        });
                        $(".btn-delete-schedule").click(function () {
                            var that = this;
                            var sched_id = $(that).data('sched_id');
                            swal({
                                title: 'Are you sure?',
                                text: "The schedule will be deleted!",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Yes, continue!'
                            }).then(function (result) {
                                if (result.value) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>Schedule/deleteSingleSchedule",
                                        data: {
                                            sched_id: sched_id
                                        },
                                        beforeSend: function () {
                                            mApp.block('#portlet', {
                                                overlayColor: '#000000',
                                                type: 'loader',
                                                state: 'brand',
                                                size: 'lg'
                                            });
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = JSON.parse(res.trim());
                                            if (res.status === 'Success') {
                                                var p = $("#table-schedule").find("p[data-sched_id='" + sched_id + "']");
                                                var td = $(p).closest('td');
                                                $(p).remove();
                                                var tr_content = $(td).text();
                                                if (tr_content === '') {
                                                    $(td).html("No Schedule");
                                                    $(td).css('color', '#ccc');
                                                }
                                                $.notify({
                                                    message: 'Successfully Deleted the Schedule.'
                                                }, {
                                                    type: 'success',
                                                    timer: 1000,
                                                    z_index: 2000
                                                });
                                            } else {
                                                $.notify({
                                                    message: 'An error occurred while processing your request.'
                                                }, {
                                                    type: 'danger',
                                                    timer: 1000,
                                                    z_index: 2000
                                                });
                                            }

                                            $("#schedule-modal").modal("hide");
                                            mApp.unblock('#portlet');
                                        }
                                    });
                                }
                            });

                        });

                        $(".btn-update-schedule").click(function () {
                            var that = this;
                            var sched_id = $(that).data('sched_id');
                            var tr = $(that).closest('tr');
                            var schedtype_id = $(tr).find(".select-schedtype").val();
                            var leaveType_ID = $(tr).find(".select-leavetype").val();
                            var time_id = $(tr).find(".select-shift").val();
                            var fullhalf_remarks = $(tr).find("input[name=fullhalf]:checked").val();
                            var remarks = "Manual";
                            var isOkay = true;
                            saveupdate_checker(schedtype_id, withSchedules, time_id, isOkay, fullhalf_remarks, remarks, leaveType_ID, function (isOkay, remarks) {
                                if (isOkay === true) {
                                    swal({
                                        title: 'Are you sure?',
                                        text: "The schedule set will be saved!",
                                        type: 'warning',
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes, continue!'
                                    }).then(function (result) {
                                        if (result.value) {
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>Schedule/updateSingleSchedule",
                                                data: {
                                                    sched_id: sched_id,
                                                    schedtype_id: schedtype_id,
                                                    time_id: time_id,
                                                    leaveType_ID: leaveType_ID,
                                                    remarks: remarks
                                                },
                                                beforeSend: function () {
                                                    mApp.block('#portlet', {
                                                        overlayColor: '#000000',
                                                        type: 'loader',
                                                        state: 'brand',
                                                        size: 'lg'
                                                    });
                                                },
                                                cache: false,
                                                success: function (res) {
                                                    res = JSON.parse(res.trim());
                                                    var schedule = res.schedule;
                                                    if (res.status === 'Success') {
                                                        var maintable_tr = $("#table-schedule").find("td[data-date='" + schedule.sched_date + "'][data-empid='" + schedule.emp_id + "']");
                                                        var td = $(maintable_tr).find("p[data-sched_id=" + schedule.sched_id + "]");
                                                        if (schedule.time_start === null) {
                                                            if (schedule.schedtype_id === '3' || schedule.schedtype_id === '8') {
                                                                $(td).replaceWith('<p data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white;font-size:10px">' + schedule.leaveType + '' + schedule.type.replace('Leave', '') + '</p>');
                                                            } else {
                                                                $(td).replaceWith('<p data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white">' + schedule.type + '</p>');
                                                            }

                                                        } else {
                                                            getSchedStartEnd(schedule.sched_date, schedule.time_start, schedule.time_end, function (sched_start, sched_end) {
                                                                $(td).replaceWith('<p  data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white">' + sched_start + ' - ' + sched_end + '</p>');
                                                            });
                                                        }

                                                        $.notify({
                                                            message: 'Successfully Updated a Schedule.'
                                                        }, {
                                                            type: 'success',
                                                            timer: 1000,
                                                            z_index: 2000
                                                        });
                                                    } else {
                                                        $.notify({
                                                            message: 'An error occurred while processing your request.'
                                                        }, {
                                                            type: 'danger',
                                                            timer: 1000,
                                                            z_index: 2000
                                                        });
                                                    }
                                                    $("#schedule-modal").modal("hide");
                                                    mApp.unblock('#portlet');
                                                }
                                            });
                                        }
                                    });

                                }
                            });

                        });

                        $("#btn-create-schedule").click(function () {
                            var that = this;
                            $(that).closest('tr').hide();
                            var acc_id = $(that).data('acc_id');
                            var employee_id = $(that).data('emp_id');
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>Schedule/getSchedTypeAndShift/" + acc_id,
                                beforeSend: function () {
                                    mApp.block('#portlet', {
                                        overlayColor: '#000000',
                                        type: 'loader',
                                        state: 'brand',
                                        size: 'lg'
                                    });
                                },
                                cache: false,
                                success: function (res) {
                                    res = JSON.parse(res.trim());
                                    var options_schedtype = "";
                                    var options_shift = "<option value=''>--</option>";
                                    var options_leaveTypes = "<option value=''>--</option>";
                                    $.each(res.available_schedtypes, function (key, schedtype) {
                                        if (jQuery.inArray(parseInt(schedtype.schedtype_id), restrictedSchedTypes) !== -1) {
                                            options_schedtype += '<option value="' + schedtype.schedtype_id + '" disabled>' + schedtype.type + '</option>';
                                        } else {
                                            options_schedtype += '<option value="' + schedtype.schedtype_id + '">' + schedtype.type + '</option>';
                                        }

                                    });
                                    $.each(res.available_shifts, function (key, shift) {
                                        var sched_start = moment(sched_date + ' ' + shift.time_start, 'YYYY-MM-DD h:mm:ss A').format("hh:mma");
                                        var sched_end = moment(sched_date + ' ' + shift.time_end, 'YYYY-MM-DD h:mm:ss A').format("hh:mma");
                                        options_shift += '<option value="' + shift.time_id + '">' + sched_start + ' - ' + sched_end + '</option>';
                                    });
                                    $.each(res.available_leavetypes, function (key, leave) {
                                        options_leaveTypes += '<option value="' + leave.leaveType_ID + '">' + leave.leaveType + '</option>';
                                    });

                                    var tr = $(that).closest('tr');
                                    var string = '';
                                    string += '<td></td>';
                                    string += '<td style="vertical-align:middle;width:25%"><select id="select-schedtype-new" class="form-control m-select2"><option value="">--</option>' + options_schedtype + '</select>';
                                    string += '<div class="m-radio-inline"  style="margin-top:5px;display:none" id="fullhalf-new-parent"><label class="m-radio"><input type="radio" name="fullhalf-new" value="Manual-Full"> Full<span></span></label><label class="m-radio"><input type="radio" name="fullhalf-new" value="Manual-Half"> Half<span></span></label></label></div>';
                                    string += '</td>';
                                    string += '<td style="vertical-align:middle;width:25%"><select id="select-shift-new" class="form-control m-select2" disabled>' + options_shift + '</select>';
                                    string += '<select class="form-control form-control-sm" id="select-leavetype-new"  style="display:none">' + options_leaveTypes + '</select>';
                                    string += '</td>';
                                    string += '<td colspan=3 class="text-center"><a href="#" class="btn btn-success btn-sm m-btn m-btn--icon btn-block" id="btn-insert-schedule"><span><i class="la la-calendar-check-o"></i><span>Create Schedule</span></span></a></td>';


                                    $(tr).html(string).fadeIn('slow');
                                    $(tr).css('background', '#F8F8FF');
                                    $('#select-shift-new,#select-schedtype-new').select2({dropdownParent: $('#schedule-modal')});
                                    mApp.unblock('#portlet');


                                    $("#btn-insert-schedule").click(function () {
                                        var schedtype_id = $("#select-schedtype-new").val();
                                        var time_id = $("#select-shift-new").val();
                                        var leaveType_ID = $("#select-leavetype-new").val();
                                        var fullhalf_remarks = $('input[name=fullhalf-new]:checked').val();
                                        var sched_date = $("#schedule-date").data('date');
                                        var emp_id = employee_id;
                                        var remarks = "Manual";
                                        var isOkay = true;
                                        saveupdate_checker(schedtype_id, withSchedules, time_id, isOkay, fullhalf_remarks, remarks, leaveType_ID, function (isOkay, remarks) {
                                            if (isOkay === true) {
                                                swal({
                                                    title: 'Are you sure?',
                                                    text: "The schedule set will be saved!",
                                                    type: 'warning',
                                                    showCancelButton: true,
                                                    confirmButtonText: 'Yes, continue!'
                                                }).then(function (result) {
                                                    if (result.value) {
                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo base_url(); ?>Schedule/insertSingleSchedule",
                                                            data: {
                                                                schedtype_id: schedtype_id,
                                                                time_id: time_id,
                                                                sched_date: sched_date,
                                                                emp_id: emp_id,
                                                                remarks: remarks,
                                                                leaveType_ID: leaveType_ID
                                                            },
                                                            beforeSend: function () {
                                                                mApp.block('#portlet', {
                                                                    overlayColor: '#000000',
                                                                    type: 'loader',
                                                                    state: 'brand',
                                                                    size: 'lg'
                                                                });
                                                            },
                                                            cache: false,
                                                            success: function (res) {
                                                                res = JSON.parse(res.trim());
                                                                var schedule = res.schedule;
                                                                if (res.status === 'Success') {
                                                                    var maintable_tr = $("#table-schedule").find("td[data-date='" + schedule.sched_date + "'][data-empid='" + schedule.emp_id + "']");
                                                                    var text = $(maintable_tr).text();
                                                                    if (text === 'No Schedule') {
                                                                        if (schedule.time_start === null) {
                                                                            if (schedule.schedtype_id === '3' || schedule.schedtype_id === '8') {

                                                                                $(maintable_tr).html('<p data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white;font-size:10px">' + schedule.leaveType + '' + schedule.type.replace('Leave', '') + '</p>');
                                                                            } else {
                                                                                $(maintable_tr).html('<p data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white">' + schedule.type + '</p>');
                                                                            }

                                                                        } else {
                                                                            getSchedStartEnd(schedule.sched_date, schedule.time_start, schedule.time_end, function (sched_start, sched_end) {
                                                                                $(maintable_tr).html('<p  data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white">' + sched_start + ' - ' + sched_end + '</p>');
                                                                            });
                                                                        }
                                                                    } else {
                                                                        if (schedule.time_start === null) {
                                                                            if (schedule.schedtype_id === '3' || schedule.schedtype_id === '8') {
                                                                                $(maintable_tr).append('<p data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white;font-size:10px">' + schedule.leaveType + '' + schedule.type.replace('Leave', '') + '</p>');
                                                                            } else {
                                                                                $(maintable_tr).append('<p data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white">' + schedule.type + '</p>');
                                                                            }

                                                                        } else {
                                                                            getSchedStartEnd(schedule.sched_date, schedule.time_start, schedule.time_end, function (sched_start, sched_end) {
                                                                                $(maintable_tr).append('<p  data-sched_id="' + schedule.sched_id + '" class="m-badge m-badge--wide custom-badge" style="background:' + schedule.style + ';color:white">' + sched_start + ' - ' + sched_end + '</p>');
                                                                            });
                                                                        }
                                                                    }
                                                                    $.notify({
                                                                        message: 'Successfully Added a Schedule.'
                                                                    }, {
                                                                        type: 'success',
                                                                        timer: 1000,
                                                                        z_index: 2000
                                                                    });
                                                                } else {
                                                                    $.notify({
                                                                        message: 'An error occurred while processing your request.'
                                                                    }, {
                                                                        type: 'danger',
                                                                        timer: 1000,
                                                                        z_index: 2000
                                                                    });
                                                                }
                                                                $("#schedule-modal").modal("hide");
                                                                mApp.unblock('#portlet');
                                                            }
                                                        });
                                                    }
                                                });

                                            }
                                        });
                                    });
                                    $("#select-schedtype-new").on('change', function () {
                                        var that = this;
                                        var tr = $(that).closest('tr');
                                        var schedtype_id = $(that).val();
                                        if (jQuery.inArray(parseInt(schedtype_id), withSchedules) !== -1) {
                                            $(tr).find('#select-shift-new').prop('disabled', false);
                                        } else {
                                            $(tr).find('#select-shift-new').prop('disabled', true);
                                            $(tr).find('#select-shift-new').val('').change();
                                        }
                                        if (schedtype_id === '3' || schedtype_id === '8') {
                                            $("#fullhalf-new-parent").show(500);
                                            $(tr).find('#select-leavetype-new').show(500);
                                        } else {
                                            $("#fullhalf-new-parent").hide(500);
                                            $(tr).find('#select-leavetype-new').hide(500);
                                        }
                                    });
                                }
                            });

                        });
                        mApp.unblock('#portlet');
                        mApp.initTooltips();
                    }
                });
            });

            $("#table-schedule-date").on('change', '.select-schedtype', function () {
                var that = this;
                var tr = $(that).closest('tr');
                var schedtype_id = $(that).val();
                if (jQuery.inArray(parseInt(schedtype_id), withSchedules) !== -1) {
                    $(tr).find('.select-shift').prop('disabled', false);
                } else {
                    $(tr).find('.select-shift').prop('disabled', true);
                    $(tr).find('.select-shift').val('').change();
                }
                if (schedtype_id === '3' || schedtype_id === '8') {
                    $(tr).find('.fullhalf-parent').show(500);
                    $(tr).find('.select-leavetype').show(500);
                } else {
                    $(tr).find('.fullhalf-parent').hide(500);
                    $(tr).find('.select-leavetype').hide(500);
                }
            });
            $("#btn-view-create").click(function () {
                if ($(this).data('stat') === 'open') {
                    $("#view-create").hide(500);
                    $(this).data('stat', 'close');
                } else {
                    $("#view-create").show(500);
                    $(this).data('stat', 'open');
                }
            });
        });
</script>
