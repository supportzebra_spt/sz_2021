<!DOCTYPE html>

<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8" />

    <title>SupportZebra | Login</title>
    <meta name="description" content="Latest updates and statistic charts">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!--begin::Web font -->
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->

    <!--begin::Base Styles -->



    <link href="<?php echo base_url(); ?>assets/src/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/src/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/img/favicon.ico" />
  
    <style>
        .form-control {
            color: #000000 !important;
            opacity: 0.8;
            background: #fbfbfb !important;
            box-shadow: 0 0 0 1px rgba(0, 0, 0, .05), 0 2px 3px 0 rgba(0, 0, 0, .1) !important;
        }
		body{
			background: linear-gradient(0deg, rgb(255, 255, 255) 1%, rgb(104, 145, 158) 35%, rgb(46, 122, 148) 100%) !important;
		}
		 canvas {
		   margin-top: -50%;
			height: 100%;
			width: 100%;
			z-index:1;
			position:fixed;
		 }
		@media (max-width: 991px){
		#papaZ {
			display:none;
		}
		.m-form__section.m-form__section--first {
			width: 100% !important;
			margin-left: 0 !important;
		}
		.col-lg-7 {
			left: 0 !important;
		}
		#szLogo {
			width: 100% !important;
		}
		}
    </style>
	<script type="text/javascript" src="<?php echo base_url('assets/christmas/js/ThreeCanvas.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/christmas/js/Snow.js') ?>"></script>
	<script>
	 	var SCREEN_WIDTH = window.innerWidth;
			var SCREEN_HEIGHT = window.innerHeight;

			var container;

			var particle;

			var camera;
			var scene;
			var renderer;

			var mouseX = 0;
			var mouseY = 0;

			var windowHalfX = window.innerWidth / 2;
			var windowHalfY = window.innerHeight / 2;
			
			var particles = []; 
			var particleImage = new Image();//THREE.ImageUtils.loadTexture( "img/ParticleSmoke.png" );
 			particleImage.src = "<?php echo base_url('assets/christmas/img/ParticleSmoke.png') ?>"; 

		
		
			function init() {

				container = document.createElement('div');
				document.body.appendChild(container);

				camera = new THREE.PerspectiveCamera( 75, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000 );
				camera.position.z = 1000;

				scene = new THREE.Scene();
				scene.add(camera);
					
				renderer = new THREE.CanvasRenderer();
				renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
				var material = new THREE.ParticleBasicMaterial( { map: new THREE.Texture(particleImage) } );
					
				for (var i = 0; i < 500; i++) {

					particle = new Particle3D( material);
					particle.position.x = Math.random() * 2000 - 1000;
					particle.position.y = Math.random() * 2000 - 1000;
					particle.position.z = Math.random() * 2000 - 1000;
					particle.scale.x = particle.scale.y =  1;
					scene.add( particle );
					
					particles.push(particle); 
				}

				container.appendChild( renderer.domElement );

	
				document.addEventListener( 'mousemove', onDocumentMouseMove, false );
				document.addEventListener( 'touchstart', onDocumentTouchStart, false );
				document.addEventListener( 'touchmove', onDocumentTouchMove, false );
				
				setInterval( loop, 1000 / 60 );
				
			}
			
			function onDocumentMouseMove( event ) {

				mouseX = event.clientX - windowHalfX;
				mouseY = event.clientY - windowHalfY;
			}

			function onDocumentTouchStart( event ) {

				if ( event.touches.length == 1 ) {

					event.preventDefault();

					mouseX = event.touches[ 0 ].pageX - windowHalfX;
					mouseY = event.touches[ 0 ].pageY - windowHalfY;
				}
			}

			function onDocumentTouchMove( event ) {

				if ( event.touches.length == 1 ) {

					event.preventDefault();

					mouseX = event.touches[ 0 ].pageX - windowHalfX;
					mouseY = event.touches[ 0 ].pageY - windowHalfY;
				}
			}

			//

			function loop() {

			for(var i = 0; i<particles.length; i++)
				{

					var particle = particles[i]; 
					particle.updatePhysics(); 
	
					with(particle.position)
					{
						if(y<-1000) y+=2000; 
						if(x>1000) x-=2000; 
						else if(x<-1000) x+=2000; 
						if(z>1000) z-=2000; 
						else if(z<-1000) z+=2000; 
					}				
				}
			
				camera.position.x += ( mouseX - camera.position.x ) * 0.05;
				camera.position.y += ( - mouseY - camera.position.y ) * 0.05;
				camera.lookAt(scene.position); 

				renderer.render( scene, camera );

				
			}

		</script>
</head>
<!-- end::Head -->


<!-- end::Body -->

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default" style="cursor:  url(<?php echo base_url('assets/images/img/cursor-christmx.png') ?>), zoom-in;background-position: center;" onload="init()">
<div class="m-content" style="height: 100%;">
			<div class="m-portlet__head" id="szLogo" style="text-align: center;width: 50%;margin: 70px auto;display:none;">
						 <img src="<?php echo base_url("assets/images/img/sz2.png"); ?>" style="margin-right: -36%;">
			</div>
<div class="row" style="width: 73%;z-index: 8;position: absolute;margin-left: 17%;margin-top: 16%;">

 
	<div class="col-lg-5" id="papaZ">
		<!--begin::Portlet-->
		<div class="m-portlet"> 
			
			<!--begin::Form-->
			<form class="m-form">
				<div class="">	
					<div class="m-form__section m-form__section--first">
						<div class="">
                               <img src="<?php echo base_url("assets/images/img/christmx-circle.png"); ?>" style="width: 465px;z-index: 1;position: absolute;right: -50px;top: -156px;">
                       </div>
						
		            </div>
	            </div>
	           
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

		<!--begin::Portlet-->
		
	</div><div class="col-lg-7" style="top: -40px;left: -10%;">
		<!--begin::Portlet-->
		<div class="m-portlet"  style="border-top-right-radius: 25px;width: 99%;box-shadow: 7px 7px #b1b1b129;">
			<div class="m-portlet__head" style="background-image:url(<?php echo base_url("assets/images/img/head-christmx.png"); ?>);background-size: 100%;border-top-right-radius: 20px;background-repeat: no-repeat;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
							
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form">
				<div class="m-portlet__body" style="background-image: url(<?php echo base_url('assets/images/img/login-bg-christmx.png') ?>);background-color: #bebebe;background-size: cover;background-repeat: no-repeat;background-position-y: -57px; ">	
					<div class="m-form__section m-form__section--first" style="width: 70%;margin-left: 25%;">
							<div id="showError" style="display:none">
								<div class="alert alert-danger alert-dismissible fade show" role="alert" style="font-size:small;    margin-top: -20px;">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									</button>
									<strong>Oh snap!</strong> Username and Password mismatched!
								</div>
							</div>
						    <div class="form-group m-form__group m-input-icon m-input-icon--left m-input-icon--right">
                                <input class="form-control m-input" type="text" placeholder="Username" name="username" autocomplete="off" style="border-bottom: 2px solid #7b7b7b;">
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la 	la-user"></i></span></span>
                            </div>
                            <div class="form-group m-form__group m-input-icon m-input-icon--left m-input-icon--right">
                                <input class="form-control m-input " type="password" placeholder="Password" name="password" style="border-bottom: 2px solid #7b7b7b;">
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-unlock"></i></span></span>

                            </div>
                            <div class="m-login__form-action text-right">
                                <button id="m_login_signin_submit" class="btn btn-warning m-btn m-btn--pill m-btn--custom " style="background: #1b5471;border-color: #174258;color: #fff;">Sign In</button>
                            </div>
                        
		            </div>
	            </div>
	            
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

		<!--begin::Portlet-->
		
	</div>
	</div>
	</div>

  

    <!--begin::Base Scripts -->
    <script src="<?php echo base_url(); ?>assets/src/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/src/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Base Scripts -->


</body>
<script type="text/javascript">
 
    $(function () {

        $('#m_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '<?php echo base_url('login/log_in'); ?>',
                type: 'post',
                success: function (response) {
                    response = JSON.parse(response.trim());

                    if (response.status === 'Success') {
                        console.log("HERE");
                        window.location.href = '<?php echo base_url('dashboard') ?>';
						$("#showError").css("display","none");
                    } else {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr(
                            'disabled', false);
						$("#showError").css("display","block");
                    }

                }
            });
        });
    })
</script>

</html>