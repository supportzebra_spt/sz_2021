<!DOCTYPE html>

<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8" />

    <title>SupportZebra | Login</title>
    <meta name="description" content="Latest updates and statistic charts">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!--begin::Web font -->
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->

    <!--begin::Base Styles -->



    <link href="<?php echo base_url(); ?>assets/src/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/src/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/img/favicon.ico" />
  
    <style>
        .form-control {
            color: #000000 !important;
            opacity: 0.8;
            background: #fbfbfb !important;
            box-shadow: 0 0 0 1px rgba(0, 0, 0, .05), 0 2px 3px 0 rgba(0, 0, 0, .1) !important;
        }
		input.form-control.m-input {
			border: 1px solid #0d8d9a;
		}
		@media (max-width: 991px){
		#papaZ {
			display:none;
		}
		.m-form__section.m-form__section--first {
			width: 100% !important;
			margin-left: 0 !important;
		}
		.col-lg-7 {
			left: 0 !important;
		}
		#szLogo {
			width: 100% !important;
		}
		#mask {
			width: 100% !important;
			position: absolute !important;
			left: -83px;
		}
		}
    </style>
</head>
<!-- end::Head -->


<!-- end::Body -->

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default" style="background-image:url('<?php echo base_url("assets/images/img/fl.jpg"); ?>');background-color: #fff;background-size: contain;background-repeat: no-repeat;   background-position: center;">
<div class="m-content">
			<div class="m-portlet__head" id="szLogo" style="text-align: center;width: 50%;margin: 70px auto;">
						 <img src="<?php echo base_url("assets/images/img/sz2.png"); ?>" style="margin-right: -36%;">
			</div>
<div class="row" style="width: 73%;margin: 0 auto;margin-top: 60px;">

 
	<div class="col-lg-5" id="papaZ">
		<!--begin::Portlet-->
		<div class="m-portlet" style="background: transparent;box-shadow: none !important;"> 
			
			<!--begin::Form-->
			<form class="m-form"> 
				<div class="">	
					<div class="m-form__section m-form__section--first">
						<div class="">
                               <img src="<?php echo base_url("assets/images/img/bg-virus.png"); ?>" style="width: 600px;z-index: 1;position: absolute;right: -98px;top: -117px;">
                       </div>
						
		            </div>
	            </div>
	           
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

		<!--begin::Portlet-->
		
	</div>
	
	<img src="<?php echo base_url("assets/images/img/mask2.png"); ?>" style="width: 55%;margin-left: 18%;margin-top: -4%;position: absolute;" id="mask">
	<div class="col-lg-7" style="top: -40px;left: -10%;">
		<!--begin::Portlet-->
		<div class="m-portlet"  style="background: transparent;box-shadow: none !important;">
			
			<!--begin::Form-->
			<form class="m-form">
				<div class="m-portlet__body" style="background-color: transparent; background-size: cover;background-repeat: no-repeat;background-position: center;">	
					<div class="m-form__section m-form__section--first" style="width: 70%;margin-left: 18%;margin-top: 20%;">
							<div id="showError" style="display:none">
								<div class="alert alert-danger alert-dismissible fade show" role="alert" style="font-size:small;    margin-top: -20px;">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									</button>
									<strong>Oh snap!</strong> Username and Password mismatched!
								</div>
							</div>
						    <div class="form-group m-form__group m-input-icon m-input-icon--left m-input-icon--right">
                                <input class="form-control m-input" type="text" placeholder="Username" name="username" autocomplete="off">
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la 	la-user"></i></span></span>
                            </div>
                            <div class="form-group m-form__group m-input-icon m-input-icon--left m-input-icon--right">
                                <input class="form-control m-input " type="password" placeholder="Password" name="password">
								<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-unlock"></i></span></span>

                            </div>
                            <div class="m-login__form-action" style="text-align: center;">
                                <button id="m_login_signin_submit" class="btn btn-info m-btn m-btn--pill m-btn--custom ">Sign In</button>
                            </div>
                        
		            </div>
	            </div>
	            
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->

		<!--begin::Portlet-->
		
	</div>
	</div>
	</div>

  

    <!--begin::Base Scripts -->
    <script src="<?php echo base_url(); ?>assets/src/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/src/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Base Scripts -->


</body>
<script type="text/javascript">
 
    $(function () {

        $('#m_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '<?php echo base_url('login/log_in'); ?>',
                type: 'post',
                success: function (response) {
                    response = JSON.parse(response.trim());

                    if (response.status === 'Success') {
                        console.log("HERE");
                        window.location.href = '<?php echo base_url('dashboard') ?>';
						$("#showError").css("display","none");
                    } else {
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr(
                            'disabled', false);
						$("#showError").css("display","block");
                    }

                }
            });
        });
    })
</script>

</html>