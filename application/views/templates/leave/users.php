

<link href="<?php echo base_url(); ?>assets/src/custom/css/bootstrap-duallistbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.bootstrap-duallistbox.min.js" ></script>
<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .m-datatable__cell{
        text-transform:  capitalize
    }
    .bg-custom-gray{
        background:#4B4C54 !important;
    }

    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
    .font-white{
        color:white !important;
    }
    .m-content ul.m-subheader__breadcrumbs li,.m-content ul.m-subheader__breadcrumbs li a{
        padding:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li a span:hover{
        background: #ddd !important;         
        color:black !important; 
    } 

    .m-content ul.m-subheader__breadcrumbs li a span{
        color:#fff !important;  
        padding:10px 20px !important; 
    } 
    .m-content ul.m-subheader__breadcrumbs{
        background:#4B4C54 !important; 
        padding:2px;
    }
    .my-nav-active{
        background: #eee !important;    
    } 
    li.my-nav-active a span{      
        color:black; 
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/users'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Users</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline" style="box-shadow:0px 1px 15px 1px rgba(69, 65, 78, 0.08);">
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-list m--font-primary"></i> Summary</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/monitoring'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-dashboard m--font-info"></i> Monitoring</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/types'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-table m--font-success"></i> Types</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/rules'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-asterisk m--font-brand"></i> Rules</span>
                </a>
            </li>
            <li class="m-nav__item my-nav-active">
                <a href="<?php echo base_url('Leave/users'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text" style="color:#555 !important"><i class="fa fa-users m--font-danger"></i> Users</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/credits'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-ticket m--font-warning"></i> Credits</span>
                </a>
            </li>
        </ul>
        <br />
        <br />
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins" style="color:white !important;font-size:16px">
                                   Leave Users
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">

                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-12">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span><i class="la la-search"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->

                        <!--begin: Datatable -->
                        <div class="m_datatable" id="ajax_data"></div>
                        <!--end: Datatable -->
                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>
        <!--begin::Modal-->
        <div class="modal fade" id="updateUsersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form onsubmit="return false;" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="form_leave" novalidate="novalidate" method="POST">

                        <div class="modal-header">
                            <h5 class="modal-title" id="leave_name">Leave Users</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="padding:0 25px">
                            <!--begin::Form-->

                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-12">
                                        <label>Validation Type:</label>
                                        <div class="m-radio-inline">
                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="validationType" value="all"> All Employees
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="validationType" value="general"> General
                                                <span></span>
                                            </label>

                                            <label class="m-radio m-radio--solid">
                                                <input type="radio" name="validationType" value="specific"> Specific
                                                <span></span>
                                            </label>
                                        </div>
                                        <span class="m-form__help">Please select validation type</span>
                                    </div>
                                </div>
                                <div style="max-height:300px;width:100%;overflow-y:scroll;overflow-x:hidden">
                                    <div id="div-general" style="display:none">
                                        <div class="m-form__group form-group">
                                            <label for="">Gender:</label>
                                            <div class="m-checkbox-inline">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="gender" value="male"> Male 
                                                    <span></span>
                                                </label>
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="gender" value="female"> Female 
                                                    <span></span>
                                                </label>
                                            </div>
                                            <span class="m-form__help">Choose user allowed gender</span>
                                        </div>


                                        <div class="m-form__group form-group">
                                            <label for="">Civil Status:</label>
                                            <div class="m-checkbox-inline">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="civilStatus" value="single"> Single 
                                                    <span></span>
                                                </label>
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="civilStatus" value="married"> Married 
                                                    <span></span>
                                                </label>

                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="civilStatus" value="divorced"> Divorced 
                                                    <span></span>
                                                </label>
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="civilStatus" value="separated"> Separated 
                                                    <span></span>
                                                </label>

                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="civilStatus" value="widowed"> Widowed 
                                                    <span></span>
                                                </label>
                                            </div>
                                            <span class="m-form__help">Choose user allowed civil status</span>
                                        </div>

                                        <div class="m-form__group form-group">
                                            <label for="">Class:</label>
                                            <div class="m-checkbox-inline">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="class" value="Admin"> SZ Team 
                                                    <span></span>
                                                </label>
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="class" value="Agent"> Ambassador 
                                                    <span></span>
                                                </label>
                                            </div>
                                            <span class="m-form__help">Choose employee class</span>
                                        </div>

                                        <div class="m-form__group form-group">
                                            <label for="">Position Status:</label>
                                            <div class="m-checkbox-inline" id="positionStatusDiv">

                                            </div>
                                            <span class="m-form__help">Choose user allowed position status</span>
                                        </div>
                                    </div>

                                    <div id="div-specific" style="display:none">
                                        <div class="m-form__group form-group">
                                            <label for="">List of Employees:</label>
                                            <select name="from" id="dualListBox" size="8" multiple="multiple">
                                            </select>
                                            <span class="m-form__help">Choose specific employee allowed to use the leave</span>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <!--end::Form-->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submit_form">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
        function updateModalTrigger(leave_id) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_leave_user_validation",
                data: {
                    leave_id: leave_id
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    var data = res.data;
                    console.log(data.validationType);

                    if (data.validationType === 'general') {
                        var general = data.general;
                        $.each(general.gender.split(','), function (i, item) {
                            console.log(item);
                            if (item !== '') {
                                $("[name=gender][value=" + item + "]").prop('checked', true);
                            }
                        });

                        $.each(general.civilStatus.split(','), function (i, item) {
                            console.log(item);
                            if (item !== '') {
                                $("[name=civilStatus][value=" + item + "]").prop('checked', true);
                            }
                        });

                        $.each(general.positionType.split(','), function (i, item) {
                            console.log(item);
                            if (item !== '') {
                                $("[name=class][value=" + item + "]").prop('checked', true);
                            }
                        });

                        $.each(general.positionStatus.split(','), function (i, item) {
                            console.log(item);
                            if (item !== '') {
                                $("[name=positionStatus][value=" + item + "]").prop('checked', true);
                            }
                        });
                    } else if (data.validationType === 'specific') {
                        $.each(data.specific, function (i, item) {
                            $("#dualListBox option[value=" + item.uid + "]").prop("selected", "selected");
                        });
                        $('#dualListBox').bootstrapDualListbox('refresh');
                    }

                    $("#leave_name").html(data.leave_name);
                    $('input:radio[name="validationType"]').filter('[value="' + data.validationType + '"]').attr('checked', true).change();
                    $("#submit_form").data('leave_id', leave_id);
                    $("#updateUsersModal").modal();
                }
            });
        }
        function datatable_init() {

            var datatable = $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'POST',
                            url: 'get_leave_lists',
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                // layout definition
                layout: {
                    theme: 'default', // datatable theme
                    class: '', // custom wrapper class
                    scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                    //                    height: 550, // datatable's body's fixed height
                    footer: false // display/hide footer
                },
                // column sorting
                sortable: true,
                pagination: true,
                toolbar: {
                    // toolbar placement can be at top or bottom or both top and bottom repeated
                    placement: ['bottom'],
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [5, 10, 20, 30, 50, 100]
                        }
                    }
                },
                search: {
                    input: $('#input-search')
                },
                // columns definition
                columns: [
                    {
                        field: 'leave_name',
                        title: 'Leave Name',
                        sortable: true,
                        width: 200
                    }, {
                        field: 'description',
                        title: 'Description',
                        sortable: true,
                        width: 300
                    }, {
                        field: 'validationType',
                        title: 'Validation Type',
                        sortable: true,
                        width: 140
                    }, {
                        field: 'Actions',
                        width: 80,
                        title: 'Actions',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
                            //                            row = {leave_name,leave_id}
                            return '\
                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"  onclick="updateModalTrigger(' + row.leave_id + ')">\
                                                        <i class="flaticon-edit-1"></i>\
                                                </a>\
                                        ';
                        }
                    }]
            });
            return datatable;
        }

        function formReset() {
            $("[name=gender]").prop('checked', false);
            $("[name=civilStatus]").prop('checked', false);
            $("[name=class]").prop('checked', false);
            $("[name=positionStatus]").prop('checked', false);
            $("#dualListBox option").prop("selected", false);
            $('#dualListBox').bootstrapDualListbox('refresh');
            $('[name=validationType]').attr('checked', false);
        }

        $(function () {
            var datatable = [];
            $.when(fetchGetData('/Leave/users_init')).then(function (json) {
                var json = JSON.parse($.trim(json));
                $("#positionStatusDiv").html("");

                var str = "";
                $.each(json.statuses, function (key, obj) {
                    str += '<label class="m-checkbox"><input type="checkbox" name="positionStatus" value="' + obj.empstat_id + '"> ' + obj.status + ' <span></span> </label>';
                });
                $("#positionStatusDiv").html(str);

                $("#dualListBox").html("");
                var str = "";
                $.each(json.users, function (key, obj) {
                    str += '<option value="' + obj.uid + '">' + obj.lname + ' ' + obj.fname + '</option>';
                });
                $("#dualListBox").html(str);
                datatable = datatable_init();
                var dualListBox = $('#dualListBox').bootstrapDualListbox({
                    nonSelectedListLabel: 'Non-selected',
                    selectedListLabel: 'Selected',
                    selectorMinimalHeight: 150
                });
                var dualListContainer = dualListBox.bootstrapDualListbox('getContainer');
                dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
                dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
                dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
                dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');

            });
            $("[name=validationType]").change(function () {
                var value = $(this).val();
//                console.log(value);
                $("#submit_form").data('validationtype', value);
                if (value === 'general') {
                    $("#div-general").show(500);
                    $("#div-specific").hide();
                } else if (value === 'specific') {
                    $("#div-general").hide();
                    $("#div-specific").show(500);
                } else {
                    $("#div-specific").hide(500);
                    $("#div-general").hide(500);
                }
            });
            $("#submit_form").click(function () {
                var validationType = $(this).data('validationtype');
                var data = [];
                if (validationType === 'general') {
                    var gender = [];
                    $("[name=gender]:checked").each(function () {
                        gender.push($(this).val());
                    });
                    var civilStatus = [];
                    $("[name=civilStatus]:checked").each(function () {
                        civilStatus.push($(this).val());
                    });
                    var classes = [];
                    $("[name=class]:checked").each(function () {
                        classes.push($(this).val());
                    });
                    var positionStatus = [];
                    $("[name=positionStatus]:checked").each(function () {
                        positionStatus.push($(this).val());
                    });
                    data = {gender: gender.toString(), civilStatus: civilStatus.toString(), classes: classes.toString(), positionStatus: positionStatus.toString()};

                } else if (validationType === 'specific') {
                    var users = $("#dualListBox").val();
                    data = {users: users};
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/update_leave_validation",
                    data: {
                        leave_id: $(this).data('leave_id'),
                        validationType: validationType,
                        data: data
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        $('#updateUsersModal').modal('hide');
                        if (res.status === 'Success') {
                            datatable.reload();
                            swal("Good job!", "Successfully updated leave validation!", "success");
                        } else {
                            swal("Failed!", "Something happened while updating leave validation!", "error");
                        }

                    }
                });
            });
            $('#updateUsersModal').on('hide.bs.modal', function () {
//                console.log("RESET");
                formReset();
            });
        });
</script>