<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
    .font-white{
        color:white !important;
    }
    .for-checkbox{
        width:10px !important;
        padding:0px;
    }
    select {
        text-align: center;
        text-align-last: center;
        /* webkit*/
    }
    .light-theme a, .light-theme span{
        padding:1.5px 8px !important;
    }
    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li,.m-content ul.m-subheader__breadcrumbs li a{
        padding:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li a span:hover{
        background: #ddd !important;         
        color:black !important; 
    } 

    .m-content ul.m-subheader__breadcrumbs li a span{
        color:#fff !important;  
        padding:10px 20px !important; 
    } 
    .m-content ul.m-subheader__breadcrumbs{
        background:#4B4C54 !important; 
        padding:2px;
    }
    .my-nav-active{
        background: #eee !important;    
    } 
    li.my-nav-active a span{      
        color:black; 
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }

    .m-accordion__item-icon i{
        font-size:18px !important;  
    }
    font-size:15px !important;  
    }
    .m-accordion__item-head{
        background:#fff !important;  
        color:#333 !important;  
    } 
    .m-accordion__item-head:hover{
        background:#ddd !important;  
    }
    .m-accordion__item{
        border:1px solid #b2b3c9 !important;
        color:black;
    }
    .m-table thead th{
        background:#545454;
        color:white;
    }
    .m-table th{
        border: 1px solid #545454 !important;
        text-align:center;
    }
    .m-table tr td{
        border: 1px solid #9B9C9E !important;
        text-align:center;
    }
    .input-credit{
        border:1px solid #888 !important;
    }
    #search-class,#search-status{
        padding: 0 !important;
    }
    .blockOverlay{
        opacity:0.15 !important;
        background-color:#746DCB !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/credits'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Credits</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline" style="box-shadow:0px 1px 15px 1px rgba(69, 65, 78, 0.08);">
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-list m--font-primary"></i> Summary</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/monitoring'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-dashboard m--font-info"></i> Monitoring</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/types'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-table m--font-success"></i> Types</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/rules'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-asterisk m--font-brand"></i> Rules</span>
                </a>
            </li>
            <li class="m-nav__item">
                <a href="<?php echo base_url('Leave/users'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text"><i class="fa fa-users m--font-danger"></i> Users</span>
                </a>
            </li>
            <li class="m-nav__item my-nav-active">
                <a href="<?php echo base_url('Leave/credits'); ?>" class="m-nav__link">
                    <span class="m-nav__link-text" style="color:#555 !important"><i class="fa fa-ticket m--font-warning"></i> Credits</span>
                </a>
            </li>
        </ul>
        <br />
        <br />
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_blockui_2_portlet"> 
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">Leave Credits 
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row text-left">
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <select class="form-control m-input m-input--air" id="search-class">
                                        <option value="">All</option>
                                        <option value="Admin">SZ Team</option>
                                        <option value="Agent">Ambassador</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group m-form__group">
                                    <select class="form-control m-input m-input--air" id="search-status">
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group m-form__group">
                                    <input type="text" class="form-control m-input m-input--air" placeholder="Search Position..." id="search-custom">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="button" id="btn-search"><i class="la la-search m--font-brand"></i></button>
                                    </div>			      	
                                </div>
                            </div>
                            <div class="col-md-3">
                                <a href="#" class="btn btn-outline-brand m-btn m-btn--icon" data-expand="open" id="btn-expand">
                                    <i class="fa fa-expand"></i> <small>Expand All</small>
                                </a>
                            </div>
                        </div>    
                        <div class="m-accordion m-accordion--bordered m-accordion--toggle-arrow" id="accordion-credits" role="tablist">   
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-7">
                                <ul id="pagination">
                                </ul>
                            </div>
                            <div class="col-md-2 text-right">
                                <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                                    <option>5</option>
                                    <option>10</option>
                                    <option>15</option>
                                    <option>20</option>
                                    <option>50</option>
                                    <option>100</option>
                                    <option>200</option>
                                </select></div>
                            <div class="col-md-3 text-right" >
                                <p style="font-size:13px;margin-top:3px;" class="m--font-bold">Showing <span class="m--font-bolder" id="count"></span> of <span class="m--font-bolder" id="total"></span> records</p>
                            </div>
                        </div>

                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">
        function getLeaveTypesList(callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/credits_init",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    callback(result);
                }
            });
        }
        function getPositionStatus(limiter, perPage, callback) {
            var searchstatus = $("#search-status").val();
            var searchcustom = $("#search-custom").val();
            var searchclass = $("#search-class").val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_position_status_list",
                data: {
                    limiter: limiter,
                    perPage: perPage,
                    searchstatus: searchstatus,
                    searchcustom: searchcustom,
                    searchclass: searchclass
                },
                beforeSend: function () {
                    mApp.block('#m_blockui_2_portlet', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    $("#total").html(res.total);
                    $("#total").data('count', res.total);
                    var finalstr = "";
                    var accordionstr = "";
                    getLeaveTypesList(function (json) {
                        $.each(res.data, function (key, obj) {
                            accordionstr += '<div class="m-accordion__item">';
                            accordionstr += '<div class="m-accordion__item-head collapsed" style="padding:10px 20px" role="tab" id="accordion-credits-head' + obj.posempstat_id + '" data-toggle="collapse" href="#accordion-credits-body' + obj.posempstat_id + '" aria-expanded="false">';
                            accordionstr += '<span class="m-accordion__item-icon"><i class="fa 	fa-id-badge"></i></span>';
                            accordionstr += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-8"><small>' + obj.pos_details + '</small> </div> <div class="col-md-2 text-right"><small>' + obj.status + '</small>&nbsp;&nbsp;&nbsp;</div><div class="col-md-2 text-right"><small> ' + obj.class + '</small>&nbsp;&nbsp;&nbsp;</div></div></span>';
                            accordionstr += '<span class="m-accordion__item-mode"></span></div>';
                            accordionstr += '<div class="m-accordion__item-body collapse" id="accordion-credits-body' + obj.posempstat_id + '" role="tabpanel" aria-labelledby="accordion-credits-head" data-parent="#accordion-credits"> ';
                            accordionstr += '<div class="m-accordion__item-content">';
                            accordionstr += '<div style="max-width:100%;overflow:auto"><table class="table-bordered  m-table  text-center">';
                            accordionstr += '<thead><tr>';
                            $.each(json.leave_list, function (key, objx) {
                                accordionstr += "<th data-leavetypeid='" + objx.leaveType_ID + "' style='font-size:13px'>" + objx.leaveType + " <small>Leave</small></th>";
                            });
                            accordionstr += '</tr></thead>';
                            accordionstr += '<tbody>';
                            accordionstr += '<tr>';

                            $.each(json.leave_list, function (key2, obj2) {
                                $.each(obj.credits, function (i, item) {
                                    if (obj2.leaveType_ID === item.leaveType_ID) {
                                        accordionstr += '<td class="text-center" style="padding:5px"><input data-posempstatid="' + obj.posempstat_id + '"  data-leavetypeid="' + obj2.leaveType_ID + '" type="text" class="input-credit form-control m-input form-control-sm"  maxlength="4" min="0" max="999" value="' + item.credit + '"/></td>';
                                    }
                                });
                            });
                            accordionstr += '</tr></tbody>';
                            accordionstr += '</table>';
                            accordionstr += '</div>';
                            accordionstr += '</div></div></div>';
                        });
                        $("#accordion-credits").html(accordionstr);
                        var count = $("#total").data('count');
                        var result = parseInt(limiter) + parseInt(perPage);
                        if (count === 0) {
                            $("#count").html(0 + ' - ' + 0);
                        } else if (count <= result) {
                            $("#count").html((limiter + 1) + ' - ' + count);
                        } else if (limiter === 0) {
                            $("#count").html(1 + ' - ' + perPage);
                        } else {
                            $("#count").html((limiter + 1) + ' - ' + result);
                        }
//
                        mApp.unblock('#m_blockui_2_portlet');
                    });
                    callback(res.total);
                },
                error: function () {
                    swal("Error", "Please contact admin", "error");
                    mApp.unblock('#m_blockui_2_portlet');
                }
            });
        }
        $(function () {
            //dynamic emp status
            getLeaveTypesList(function (json) {
                $("#search-status").html("");
                var str = "<option value=''>All</option>";
                $.each(json.employee_status, function (key, obj) {
                    str += "<option value='" + obj.status + "'>" + obj.status + "</option>";
                });
                $("#search-status").html(str);
            });
            // PAGINATION-------------------------------------------------
            $("#perpage,#search-status,#search-class").change(function () {
                var perPage = $("#perpage").val();
                getPositionStatus(0, perPage, function (total) {
                    $("#pagination").pagination('destroy');
                    $("#pagination").pagination({
                        items: total, //default
                        itemsOnPage: $("#perpage").val(),
                        hrefTextPrefix: "#",
                        cssStyle: 'light-theme',
                        onPageClick: function (pagenumber) {
                            var perPage = $("#perpage").val();
                            getPositionStatus((pagenumber * perPage) - perPage, perPage);
                        }
                    });
                });
            });
            $("#btn-search").click(function () {
                $("#perpage").change();
            });
            $("#perpage").change();
            //END OF PAGINATION --------------------------------------

            $("#accordion-credits").on('change', '.input-credit', function () {
                var value = $(this).val();
                var posempstat_id = $(this).data('posempstatid');
                var leaveType_ID = $(this).data('leavetypeid');
                console.log(posempstat_id, leaveType_ID);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/update_leave_credit",
                    data: {
                        value: value,
                        posempstat_id: posempstat_id,
                        leaveType_ID: leaveType_ID
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $.notify({
                                message: 'Successfully changed leave credit.'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                        } else {
                            $.notify({
                                message: 'No Change.'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }

                    }
                });
            });
            $("#accordion-credits").on('input', '.input-credit', function () {
                var val = Math.abs(this.value);
                val = (isNaN(val)) ? 0 : val;
                val = val.toString().substr(0, 3);
                $(this).val(val);
            });
            $("#btn-expand").click(function () {
                if ($(this).data('expand') === 'open') {
                    $(".m-accordion__item-body").removeClass("collapse");
                    $(this).html('<i class="fa fa-compress"></i> <small>Collapse All</small>');
                    $(this).data('expand', 'close');
                } else {
                    $(".m-accordion__item-body").addClass("collapse");
                    $(this).html('<i class="fa fa-expand"></i> <small>Expand All</small>');
                    $(this).data('expand', 'open');
                }

            });
        });
</script>