<style type="text/css">
    body{
        padding-right:0 !important;
    }
    .m-datatable__cell{
        text-transform:  capitalize
    }
    .bg-custom-gray{
        background:#4B4C54 !important;
    }
    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
    .font-white{
        color:white !important;
    }
    .m-content ul.m-subheader__breadcrumbs li,.m-content ul.m-subheader__breadcrumbs li a{
        padding:0 !important;
    }
    .m-content ul.m-subheader__breadcrumbs li a span:hover{
        background: #ddd !important;         
        color:black !important; 
    } 

    .m-content ul.m-subheader__breadcrumbs li a span{
        color:#fff !important;  
        padding:10px 20px !important; 
    } 
    .m-content ul.m-subheader__breadcrumbs{
        background:#4B4C54 !important; 
        padding:2px;
    }
    .my-nav-active{
        background: #eee !important;    
    } 
    li.my-nav-active a span{      
        color:black; 
    }
    .m-content hr{
        border:2.5px solid #eee !important;
    }

    /*DATERANGEPICKER*/
    .daterangepicker {
        font-size:13px !important;
    }
    .daterangepicker table td{
        width:28px !important;
        height:28px !important;
    }
    .daterangepicker_input input{
        padding-top:5px !important;
        padding-bottom:5px !important;
        line-height:1 !important;
    }

    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }   
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/request'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">My Request</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/my_list'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">My Dates</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">	
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">
                                    My Leave Dates
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div id="div-perdate">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                        <div class="m-demo__preview" style="padding:10px">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="">Leave Dates</label>
                                                    <div class="m-input-icon m-input-icon--left" id="date-history-daterange">
                                                        <input type="text" class="form-control m-input" placeholder="Date Range" readonly>
                                                        <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="">Status</label>
                                                    <select class="form-control m-input" id="search-status-history-date">
                                                        <option value="">All</option>
                                                        <option value="pending">Pending</option>
                                                        <option value="approved">Approved</option>
                                                        <option value="rejected">Rejected</option>
                                                        <option value="retracted">Retracted</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="">Description</label>
                                                    <select class="form-control m-input" id="search-minutes-history-date">
                                                        <option value="">All</option>
                                                        <option value="240">Half Day</option>
                                                        <option value="480">Full Day</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="">Payment</label>
                                                    <select class="form-control m-input" id="search-payment-history-date">
                                                        <option value="">All</option>
                                                        <option value="1">Paid</option>
                                                        <option value="0">Unpaid</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label for="">Released</label>
                                                    <select class="form-control m-input" id="search-released-history-date">
                                                        <option value="">All</option>
                                                        <option value="1">Released</option>
                                                        <option value="0">Unreleased</option>
                                                    </select>
                                                </div>
                                            </div>  

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                <div class="m-demo__preview" style="padding:10px">
                                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert">
                                        <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                        <div class="m-alert__text"><strong>No Records Found! </strong> <br>Sorry, no records were found. Please adjust your search criteria and try again.
                                        </div>
                                    </div>
                                    <table class="table table-sm m-table table-striped table-bordered table-hover" id="table-date">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Final Status</th>
                                                <th>Description</th>
                                                <th>Payment</th>
                                                <th>Released</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">
                                        <div class="m-stack__item m-stack__item--left m-stack__item--middle" >
                                            <ul id="date-history-pagination">
                                            </ul>
                                        </div>
                                        <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:8%">
                                            <select class="form-control m-input form-control-sm m-input--air" id="date-history-perpage">
                                                <option>5</option>
                                                <option>10</option>
                                                <option>20</option>
                                                <option>50</option>
                                                <option>100</option>
                                            </select>
                                        </div>
                                        <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="date-history-count"></span> of <span id="date-history-total"></span> records</div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>
        <div class="modal fade" id="date-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Details of Leave Date</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body"  style="max-height:450px;overflow:auto;padding-top:0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="m-widget7">
                                    <div class="m-widget7__user" style='margin-bottom:20px'>
                                        <div class="m-widget7__user-img">							 
                                            <img class="m-widget7__img"  id="date-pic" alt="" style="width:50px">  
                                        </div>
                                        <div class="m-widget7__info">
                                            <span class="m-widget7__username" id="date-name" style="font-size:20px">
                                            </span><br> 
                                            <span class="m-widget7__time m--font-brand" id="date-job" style="font-size:13px">
                                            </span>		 
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <table class="table m-table table-bordered table-sm" >
                                    <tr>
                                        <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Type:</td>
                                        <td id="date-leavetype" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                                    </tr>
                                    <tr>
                                        <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date:</td>
                                        <td id="date-date" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                                    </tr>
                                    <tr>
                                        <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Class:</td>
                                        <td id="date-minutes" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                                    </tr>
                                    <tr>
                                        <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Paid:</td>
                                        <td id="date-ispaid" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                                    </tr>
                                    <tr>
                                        <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Release:</td>
                                        <td id="date-isreleased" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                                    </tr>

                                    <tr>
                                        <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:</td>
                                        <td id="modal-request-reason" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                                    </tr>

                                    <tr>
                                        <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Filed On:</td>
                                        <td id="modal-request-createdOn" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                                    </tr>
                                </table>

                                <hr />
                            </div>
                            <div class="col-lg-12">

                                <p class="m--font-bolder">REQUEST APPROVALS</p>
                                <div  id="tab-modal-request">

                                </div>

                            </div>
                            <div class="col-lg-12">
                                <div id="modal-retraction-approvals" style="display:none">
                                    <hr />
                                    <p class="m--font-bolder">RETRACTION APPROVALS</p>
                                    <div  id="tab-modal-retraction">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">

        var statusClass = ["", "", "warning", "", "info", "success", "danger", "focus", "", "", "", "", "metal", "primary"];
        function getHistoryDate(limiter, perPage, callback) {
            var searchpayment = $("#search-payment-history-date").val();
            var searchreleased = $("#search-released-history-date").val();
            var searchminutes = $("#search-minutes-history-date").val();
            var searchstatus = $("#search-status-history-date").val();
            var datestart = $("#date-history-daterange").data('daterangepicker').startDate._d;
            var dateend = $("#date-history-daterange").data('daterangepicker').endDate._d;
            var uid = "<?php echo $_SESSION['uid']; ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_all_leavedates_list/" + uid,
                data: {
                    limiter: limiter,
                    perPage: perPage,
                    searchpayment: searchpayment,
                    searchreleased: searchreleased,
                    searchminutes: searchminutes,
                    searchstatus: searchstatus,
                    datestart: moment(datestart).format("YYYY-MM-DD"),
                    dateend: moment(dateend).format("YYYY-MM-DD")
                },
                beforeSend: function () {
                    mApp.block('#m_blockui_2_portlet', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());

                    $("#date-history-total").html(res.total);
                    $("#date-history-total").data('count', res.total);
                    var string = "";
                    $.each(res.data, function (i, item) {
                        var minutes = (item.minutes === "480") ? "Full" : "Half";
                        var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                        if (item.overAllStatus === "5") {
                            var isReleased = (item.isReleased === "1") ? "Yes" : "No";
                        } else {
                            var isReleased = "--";
                        }
                        string += "<tr>";
                        string += '<td class="m--font-bolder">' + item.lname + ', ' + item.fname + '</td>';
                        string += '<td class="m--font-boldest">' + moment(item.date, "YYYY-MM-DD").format('MMM DD, YYYY') + '</td>';
                        string += '<td>' + item.leaveType + ' leave</td>';
                        string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.description + '</span></td>';
                        string += '<td>' + minutes + ' day</td>';
                        string += '<td>' + isPaid + '</td>';
                        string += '<td>' + isReleased + '</td>';
                        string += '<td> <a href="#" class="m-link m-link--state m-link--primary" data-toggle="modal" data-target="#date-modal" data-requestleaveid="' + item.requestLeave_ID + '" data-requestleavedetailsid="' + item.requestLeaveDetails_ID + '"><i class="fa fa-eye"></i> View</a></td>';
                        string += "</tr>";
                    });
                    if (string === '') {
                        $("#date-alert").show();
                        $("#table-date").hide();
                    } else {
                        $("#date-alert").hide();
                        $("#table-date").show();
                    }
                    $("#table-date tbody").html(string);
                    var count = $("#date-history-total").data('count');
                    var result = parseInt(limiter) + parseInt(perPage);
                    if (count === 0) {
                        $("#date-history-count").html(0 + ' - ' + 0);
                    } else if (count <= result) {
                        $("#date-history-count").html((limiter + 1) + ' - ' + count);
                    } else if (limiter === 0) {
                        $("#date-history-count").html(1 + ' - ' + perPage);
                    } else {
                        $("#date-history-count").html((limiter + 1) + ' - ' + result);
                    }
                    mApp.unblock('#m_blockui_2_portlet');
                    callback(res.total);
                }, error: function () {
                    swal("Error", "Please contact admin", "error");
                    mApp.unblock('#m_blockui_2_portlet');
                }
            });
        }
        $(function () {
            $('#date-history-daterange .form-control').val(moment().format('MMM DD, YYYY') + ' - ' + moment().add(60, 'days').format('MMM DD, YYYY'));
            $('#date-history-daterange').daterangepicker({
                startDate: moment(),
                endDate: moment().add(60, 'days'),
                opens: 'center',
                drops: 'down',
                autoUpdateInput: true
            },
                    function (start, end, label) {
                        $('#date-history-daterange .form-control').val(start.format('MMM DD, YYYY') + ' - ' + end.format('MMM DD, YYYY'));
                        $("#date-history-perpage").change();
                    });
            $("#date-history-perpage").change(function () {
                var perPage = $("#date-history-perpage").val();
                getHistoryDate(0, perPage, function (total) {
                    $("#date-history-pagination").pagination('destroy');
                    $("#date-history-pagination").pagination({
                        items: total, //default
                        itemsOnPage: $("#date-history-perpage").val(),
                        hrefTextPrefix: "#",
                        cssStyle: 'light-theme',
                        onPageClick: function (pagenumber) {
                            var perPage = $("#date-history-perpage").val();
                            getHistoryDate((pagenumber * perPage) - perPage, perPage);
                        }
                    });
                });
            });

            $("#btn-search-history-date").click(function () {
                $("#date-history-perpage").change();
            });
            $("#search-status-history-date,#search-released-history-date,#search-payment-history-date,#search-minutes-history-date").change(function () {
                $("#date-history-perpage").change();
            });
            $("#date-history-perpage").change();

            $("#date-modal").on('show.bs.modal', function (e) {
                var requestLeaveDetails_ID = $(e.relatedTarget).data('requestleavedetailsid');
                var requestLeave_ID = $(e.relatedTarget).data('requestleaveid');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/get_date_approval_flow",
                    data: {
                        requestLeave_ID: requestLeave_ID,
                        requestLeaveDetails_ID: requestLeaveDetails_ID
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            var request_leave = res.request_leave;
                            var retract_leave = res.retract_leave;
                            var request_leave_approval = res.request_leave_approval;
                            var retract_leave_approval = res.retract_leave_approval;

                            var minutes = (request_leave.minutes === "480") ? "Full Day" : "Half Day";

                            var ispaid = (request_leave.isPaid === "1") ? "Yes" : "No";
                            var isReleased = (request_leave.isReleased === "1") ? "Yes" : "No";
                            $("#date-isreleased").html(isReleased);
                            $("#date-ispaid").html(ispaid);
                            $("#date-minutes").html(minutes);
                            $("#date-name").html(request_leave.fname + " " + request_leave.lname);
                            $("#date-pic").attr('src', request_leave.pic);
                            $("#date-job").html(request_leave.job + " <small>(" + request_leave.jobstat + ")</small>");
                            $("#date-leavetype").html("<span style='text-transform:uppercase'>" + request_leave.leaveType + " LEAVE</span>");
                            $("#date-date").html("<span>" + moment(request_leave.date, 'YYYY-MM-DD').format("MMMM DD, YYYY") + "</span>");
                            $("#modal-request-createdOn").html("" + moment(request_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                            $("#modal-request-reason").html('"' + request_leave.reason + '"');

                            $("#tab-modal-request").html("");
                            $("#tab-modal-retraction").html("");
                            var string = '';
                            var customcnt = 1;
                            $.each(request_leave_approval, function (key, obj) {
                                var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                                var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                                var blinker = (obj.semiStatus==='4')?'<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>':'';
//                                -------------------------------------------------------------------------------------- -
                                string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                                string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                                string += '<div class="m-portlet__head-caption">';
                                string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' '+blinker+'</p>';
                                string += '</div></div>';
                                string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                                string += '   <div class="row">';
                                string += '<div class="col-md-12">';
                                string += ' <table class="table m-table table-bordered table-sm">';
                                string += '  <tr>';
                                string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                                string += ' </tr>';
                                string += '</table>';
                                string += '</div>';
                                string += '    <div class="col-md-12" >';
                                string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                                string += '  <i class="fa fa-quote-left"></i>';
                                string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                                string += ' <i class="fa fa-quote-right"></i>';
                                string += ' </blockquote>';
                                string += '</div>';
                                string += '</div>';
                                string += '</div></div></div>';
                                customcnt++;
                            });
                            string += "";
                            $("#tab-modal-request").html(string);

                            if (retract_leave === null || retract_leave_approval === null) {
                                $("#modal-retraction-approvals").hide();
                            } else {

                                var string = '';
                                var customcnt = 1;
                                $.each(retract_leave_approval, function (key, obj) {
                                    var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                                    var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
var blinker = (obj.semiStatus==='4')?'<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>':'';
//                                -------------------------------------------------------------------------------------- -
                                    string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                                    string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                                    string += '<div class="m-portlet__head-caption">';
                                    string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' '+blinker+'</p>';
                                    string += '</div></div>';
                                    string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;;color:black;background:#e6e6e6"">';
                                    string += '   <div class="row">';
                                    string += '<div class="col-md-12">';
                                    string += ' <table class="table m-table table-bordered table-sm">';
                                    string += '  <tr>';
                                    string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                                    string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                                    string += ' </tr>';
                                    string += '  <tr>';
                                    string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                                    string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                                    string += ' </tr>';
                                    string += '  <tr>';
                                    string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                                    string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                                    string += ' </tr>';
                                    string += '</table>';
                                    string += '</div>';
                                    string += '    <div class="col-md-12" >';
                                    string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                                    string += '  <i class="fa fa-quote-left"></i>';
                                    string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                                    string += ' <i class="fa fa-quote-right"></i>';
                                    string += ' </blockquote>';
                                    string += '</div>';
                                    string += '</div>';
                                    string += '</div></div></div>';
                                    customcnt++;
                                });
                                string += "";
                                $("#tab-modal-retraction").html(string);

                                $("#modal-retraction-approvals").show();
                            }

                        } else {
                            $(this).modal('hide');
                            swal("Failed!", "Something happened while retrieving details!", "error");
                        }
                        $("#date-pic").on('error', function () {
                            $(this).attr('onerror', null);
                            $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                        });
                    }
                });
            });
        });
</script>