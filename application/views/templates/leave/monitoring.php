<style type="text/css">
    label {
        font-weight: 402 !important;
        font-size: 12px !important;
    }

    body {
        padding-right: 0 !important;
    }

    .bg-custom-gray {
        background: #4B4C54 !important;
    }

    .font-white {
        color: white !important;
    }

    .portlet-header-report {
        background: #4B4C54 !important;
        color: white !important;
        height: 50px !important;
    }

    .font-poppins {
        font-family: 'Poppins' !important;
    }

    .m-accordion__item {
        border: 1.2px solid #545454 !important;
    }

    .m-accordion__item-mode {
        margin-left: 5px;
        font-size: 15px !important;
    }

    .m-accordion__item-icon i {
        font-size: 18px !important;
    }

    .m-accordion__item-title {
        font-size: 15px !important;
    }

    .m-accordion__item-head {
        background: #545454 !important;
        color: white !important;
    }

    .m-accordion__item-head:hover {
        background: #777 !important;
    }

    textarea {
        resize: none;
    }

    @media screen and (max-width: 500px) {
        .button-text {
            display: none !important;
        }

    }

    .m-table thead th {
        background: #545454;
        color: white;
    }

    .m-table th {
        border: 1px solid #545454 !important;
        text-align: center;
    }

    .m-table tr td {
        border: 1px solid #9B9C9E !important;
        text-align: center;
    }

    .m-demo__preview {
        border-color: #eee !important;
    }

    div {
        word-wrap: break-word;
    }

    .m-content ul.m-subheader__breadcrumbs li,
    .m-content ul.m-subheader__breadcrumbs li a {
        padding: 0 !important;
    }

    .m-content ul.m-subheader__breadcrumbs li a span:hover {
        background: #ddd !important;
        color: black !important;
    }

    .m-content ul.m-subheader__breadcrumbs li a span {
        color: #fff !important;
        padding: 10px 20px !important;
    }

    .m-content ul.m-subheader__breadcrumbs {
        background: #4B4C54 !important;
        padding: 2px;
    }

    .my-nav-active {
        background: #eee !important;
    }

    li.my-nav-active a span {
        color: black;
    }

    .m-content hr {
        border: 2.5px solid #eee !important;
    }

    .modal hr {
        border: 2px solid #eee !important;
    }

    .light-theme a,
    .light-theme span {
        padding: 1.5px 8px !important;
    }

    .m-content .m-stack__item {
        padding: 0 !important;
        background: 0 !important;
        border: 0 !important;
    }

    .m-badge {
        font-weight: 550;
    }

    /*DATERANGEPICKER*/
    .daterangepicker {
        font-size: 13px !important;
    }

    .daterangepicker table td {
        width: 28px !important;
        height: 28px !important;
    }

    .daterangepicker_input input {
        padding-top: 5px !important;
        padding-bottom: 5px !important;
        line-height: 1 !important;
    }

    .blockOverlay {
        opacity: 0.15 !important;
        background-color: #746DCB !important;
    }

    .tdplain tr td {
        border: 1px solid #f4f5f8 !important;
    }
</style>


<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/monitoring'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Monitoring</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-md-9">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline" style="box-shadow:0px 1px 15px 1px rgba(69, 65, 78, 0.08);">
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-list m--font-primary"></i> Summary</span>
                        </a>
                    </li>
                    <li class="m-nav__item my-nav-active">
                        <a href="<?php echo base_url('Leave/monitoring'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text" style="color:#555 !important"><i class="fa fa-dashboard m--font-info"></i> Monitoring</span>
                        </a>
                    </li>
                    <li class="m-nav__item ">
                        <a href="<?php echo base_url('Leave/types'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-table m--font-success"></i> Types</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/rules'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-asterisk m--font-brand"></i> Rules</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/users'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-users m--font-danger"></i> Users</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/credits'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><i class="fa fa-ticket m--font-warning"></i> Credits</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 text-right">
                <button class="m-portlet__nav-link btn btn-brand m-btn--air m-btn--icon" style="padding: 13px;" id="btn-missed" data-stat="close">
                    <div class="row">
                        <div class="col-md-10">
                            <i class="fa fa-chevron-down"></i>
                            Missed Requests
                        </div>
                        <div class="col-md-2">
                            <span class="m-badge m-badge--danger m-badge--wide" id="missed_total" style="    float: right; margin-top: -4px;"><?php echo $missed_total; ?></span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="portlet-missed" style="display:none;">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins" style="color:white !important;font-size:16px">
                                    Missed Requests
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body" id="div-tabs-missed">
                        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--danger" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-missed-request" role="tab"><i class="la la-tasks"></i> Request Approval</a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-missed-retraction" role="tab"><i class="la la-tasks"></i> Retraction Approval</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-missed-request" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h5>
                                            Missed Leave Request Approval
                                        </h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-missed-request" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="request-missed-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="request-missed-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="request-missed-count"></span> of <span id="request-missed-total"></span> records</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-missed-retraction" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h5>
                                            Ongoing Leave Retraction Approval
                                        </h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-missed-retraction" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="retraction-missed-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="retraction-missed-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="retraction-missed-count"></span> of <span id="retraction-missed-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_blockui_2_portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins" style="color:white !important;font-size:16px">
                                    Leave Records
                                </h3>
                            </div>
                        </div>

                        <div class="m-portlet__head-tools">
                            <select class="form-control m-input form-control-sm float-md-right" id="display-type" style="max-width:150px;min-width:125px">
                                <option value="request" selected>Per Request</option>
                                <option value="date">Per Date</option>
                            </select>
                        </div>
                    </div>

                    <div class="m-portlet__body" id="div-tabs">
                        <div id="div-perrequest" style="display:none">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                        <div class="m-demo__preview" style="padding:10px 20px">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label for="">Search Name:</label>
                                                    <input type="text" class="form-control  form-control-sm m-input mb-1" placeholder="Search Employee Name..." id="search-custom-history-request">

                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">Date Filed:</label>
                                                    <div class="m-input-icon m-input-icon--left mb-1" id="request-history-daterange">
                                                        <input type="text" class="form-control  form-control-sm m-input" placeholder="Date Range" readonly>
                                                        <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                    </div>
                                                </div>


                                                <div class="col-md-3">
                                                    <label for="">Request Status:</label>
                                                    <select class="form-control m-input form-control-sm mb-1" id="search-status-history-request">
                                                        <option value="">All</option>
                                                        <option value="ongoing">Ongoing</option>
                                                        <option value="history">History</option>
                                                        <!--<option value="retracted">Retracted</option>-->
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                        <div class="m-demo__preview" style="padding:10px">

                                            <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-history" role="tablist">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                        <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                            <ul id="request-history-pagination">
                                            </ul>
                                        </div>
                                        <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:8%">
                                            <select class="form-control m-input form-control-sm m-input--air" id="request-history-perpage">
                                                <option>5</option>
                                                <option>10</option>
                                                <option>15</option>
                                                <option>20</option>
                                            </select>
                                        </div>
                                        <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="request-history-count"></span> of <span id="request-history-total"></span> records</div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="div-perdate" style="display:none">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                        <div class="m-demo__preview" style="padding:10px 20px">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="">Search Name:</label>
                                                    <input type="text" class="form-control m-input form-control-sm mb-1" placeholder="Search Employee Name..." id="search-name-history-date">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">Leave Dates:</label>
                                                    <div class="m-input-icon m-input-icon--left mb-1" id="date-history-daterange">
                                                        <input type="text" class="form-control  form-control-sm m-input" placeholder="Date Range" readonly>
                                                        <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">Leave Type:</label>
                                                    <select class="form-control form-control-sm m-input mb-1" id="search-leavetype-history-date">
                                                        <option value="">--</option>
                                                        <?php foreach ($leave_types as $type) : ?>
                                                            <option value="<?php echo $type->leaveType_ID; ?>"><?php echo $type->leaveType; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">Status:</label>
                                                    <select class="form-control form-control-sm m-input mb-1" id="search-status-history-date">
                                                        <option value="">All</option>
                                                        <option value="pending">Pending</option>
                                                        <option value="approved">Approved</option>
                                                        <option value="disapproved">Disapproved</option>
                                                        <option value="retracted">Retracted</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">Description:</label>
                                                    <select class="form-control form-control-sm m-input mb-1" id="search-minutes-history-date">
                                                        <option value="">All</option>
                                                        <option value="240">Half Day</option>
                                                        <option value="480">Full Day</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="">Payment:</label>
                                                    <select class="form-control form-control-sm m-input mb-1" id="search-payment-history-date">
                                                        <option value="">All</option>
                                                        <option value="1">Paid</option>
                                                        <option value="0">Unpaid</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4" style='display:none'>
                                                    <label for="">Released:</label>
                                                    <select class="form-control form-control-sm m-input mb-1" id="search-released-history-date">
                                                        <option value="">All</option>
                                                        <!-- <option value="1">Released</option>
                                                        <option value="0">Unreleased</option> -->
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                <div class="m-demo__preview" style="padding:10px">
                                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert">
                                        <div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>
                                        <div class="m-alert__text"><strong>No Records Found! </strong> <br>Sorry, no records were found. Please adjust your search criteria and try again.
                                        </div>
                                    </div>
                                    <table class="table table-sm m-table table-striped table-bordered table-hover" id="table-date">
                                        <thead>
                                            <tr>
                                                <th class=" text-left pl-2">Name</th>
                                                <th>Date</th>
                                                <th class=" text-left pl-2">Leave Name</th>
                                                <th>Final Status</th>
                                                <th>Description</th>
                                                <th>Payment</th>
                                                <!-- <th>Released</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">
                                        <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                            <ul id="date-history-pagination">
                                            </ul>
                                        </div>
                                        <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:8%">
                                            <select class="form-control m-input form-control-sm m-input--air" id="date-history-perpage">
                                                <option>10</option>
                                                <option>20</option>
                                                <option>50</option>
                                                <option>100</option>
                                            </select>
                                        </div>
                                        <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="date-history-count"></span> of <span id="date-history-total"></span> records</div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approvalDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Approval Process</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto;padding-top:0 !important;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img" id="view-pic" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="view-name" style="font-size:20px">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="view-job" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <div class="col-lg-12">
                        <table class="table m-table table-bordered">

                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:
                                <td id="view-reason" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date Filed:</td>
                                <td id="view-requestdate" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>
                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div id="div-viewApproval">
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="date-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Details of Leave Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto;padding-top:0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img" id="date-pic" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="date-name" style="font-size:20px">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="date-job" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <table class="table m-table table-bordered table-sm">
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Type:</td>
                                <td id="date-leavetype" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date:</td>
                                <td id="date-date" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Class:</td>
                                <td id="date-minutes" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Paid:</td>
                                <td id="date-ispaid" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <!-- <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Release:</td>
                                <td id="date-isreleased" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr> -->

                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:</td>
                                <td id="modal-request-reason" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>

                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Filed On:</td>
                                <td id="modal-request-createdOn" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>

                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div id="tab-modal-request">

                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div id="modal-retraction-approvals" style="display:none">
                            <hr />
                            <p class="m--font-bolder">RETRACTION APPROVALS</p>
                            <div id="tab-modal-retraction">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approvalDetailsModal-retraction" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Approval Process</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto;padding-top:0 !important;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img" id="view-pic-retraction" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="view-name-retraction" style="font-size:20px">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="view-job-retraction" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <div class="col-lg-12">
                        <table class="table m-table table-bordered">

                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:
                                <td id="view-reason-retraction" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date Filed:</td>
                                <td id="view-requestdate-retraction" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>
                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div id="div-viewApproval-retraction">
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script type="text/javascript">
    var statusClass = ["", "", "warning", "", "info", "success", "danger", "focus", "", "", "", "", "metal", "primary"];

    //--------------START OF MISSED---------------------------------------------
    function selectAll(that) {
        $(that).closest('.m-accordion__item-content').find("[type=checkbox]").each(function(key, elem) {
            $(elem).prop('checked', true);
        });
    }

    function clearAll(that) {
        $(that).closest('.m-accordion__item-content').find("[type=checkbox]").each(function(key, elem) {
            $(elem).prop('checked', false);
        });
    }

    function submitApproval(that, requestLeave_ID) {
        var comment = $(that).closest('.m-accordion__item-content').find('textarea').val();
        if ($.trim(comment) === '') {
            swal("Incomplete Fields", "Please complete the form to proceed", "warning");
        } else {
            var $checkboxes = $(that).closest('.m-accordion__item-content').find("[type=checkbox]");
            var checkLength = $checkboxes.length;
            var checkedCnt = 0;
            var uncheckedCnt = 0;
            var data = [];
            $checkboxes.each(function(key, elem) {
                var id = $(elem).data('requestleavedetails_id');
                data.push({
                    requestLeaveDetails_ID: id,
                    stat: $(elem).prop('checked')
                });
                if ($(elem).prop('checked')) {
                    checkedCnt++;
                } else {
                    uncheckedCnt++;
                }
            });
            if (checkLength === checkedCnt) {
                var text = "All leave dates will be approved";
                var semiStatus = 5;
            } else if (checkLength === uncheckedCnt) {
                var text = "All leave dates will be disapproved";
                var semiStatus = 6;
            } else {
                var text = "Approval will be made as per your decision";
                var semiStatus = 5;
            }
            swal({
                title: 'Are you sure?',
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Continue!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Leave/submit_approval_decision",
                        data: {
                            requestLeave_ID: requestLeave_ID,
                            comment: comment,
                            semiStatus: semiStatus,
                            data: data,
                            from: 'missed'
                        },
                        cache: false,
                        success: function(res) {
                            res = JSON.parse(res.trim());
                            if (res.status === 'Success') {
                                swal("Good job!", "Leave Approval success!", "success");
                                $.each(res.notif_details, function(i, item) {
                                    notification(item.notif_id);
                                });
                                var total_missed = parseInt($("#missed_total").text()) - 1;
                                $("#missed_total").html((total_missed < 0) ? 0 : total_missed);
                                $('a[href="#tab-missed-retraction"]').click();
                                $('a[href="#tab-missed-request"]').click();
                            } else {
                                swal("Failed!", "Something happened while approving leave request!", "error");
                            }
                        }
                    });
                }
            });
        }
    }

    function submitApprovalRetraction(that, retractLeave_ID) {
        var comment = $(that).closest('.m-accordion__item-content').find('textarea').val();
        if ($.trim(comment) === '') {
            swal("Incomplete Fields", "Please complete the form to proceed", "warning");
        } else {
            var $checkboxes = $(that).closest('.m-accordion__item-content').find("[type=checkbox]");
            var checkLength = $checkboxes.length;
            var checkedCnt = 0;
            var uncheckedCnt = 0;
            var data = [];
            $checkboxes.each(function(key, elem) {
                var id = $(elem).data('retractleavedetails_id');
                data.push({
                    retractLeaveDetails_ID: id,
                    stat: $(elem).prop('checked')
                });
                if ($(elem).prop('checked')) {
                    checkedCnt++;
                } else {
                    uncheckedCnt++;
                }
            });
            if (checkLength === checkedCnt) {
                var text = "All leave dates will be approved for retraction";
                var semiStatus = 5;
            } else if (checkLength === uncheckedCnt) {
                var text = "All leave dates will be disapproved for retraction";
                var semiStatus = 6;
            } else {
                var text = "Retraction approval will be made as per your decision";
                var semiStatus = 5;
            }
            swal({
                title: 'Are you sure?',
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Continue!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Leave/submit_approval_decision_for_retraction",
                        data: {
                            retractLeave_ID: retractLeave_ID,
                            comment: comment,
                            semiStatus: semiStatus,
                            data: data,
                            from: 'missed'
                        },
                        cache: false,
                        success: function(res) {
                            res = JSON.parse(res.trim());
                            if (res.status === 'Success') {
                                swal("Good job!", "Leave Approval success!", "success");
                                $.each(res.notif_details, function(i, item) {
                                    notification(item.notif_id);
                                });
                                var total_missed = parseInt($("#missed_total").text()) - 1;
                                $("#missed_total").html((total_missed < 0) ? 0 : total_missed);
                                $('a[href="#tab-missed-request"]').click();
                                $('a[href="#tab-missed-retraction"]').click();
                            } else {
                                swal("Failed!", "Something happened while approving leave request!", "error");
                            }
                        }
                    });
                }
            });
        }
    }

    //ONGOING REQUEST _-----------------------------------

    function getMissedRequest(limiter, perPage, callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_missed_approval_list",
            data: {
                limiter: limiter,
                perPage: perPage
            },
            beforeSend: function() {
                mApp.block('#portlet-missed', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#request-missed-total").html(res.total);
                $("#request-missed-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-missed-' + item.requestLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    if (item.reason.length > 50) {
                        var substr = item.reason.substring(0, 50) + "...";
                    } else {
                        var substr = item.reason;
                    }
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9"><b>' + item.lname + ', ' + item.fname + '</b> <i class="fa fa-caret-right"></i> <small>' + substr + '</small></div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div></div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body request collapse" data-type="ongoing" data-requestleaveid="' + item.requestLeave_ID + '" id="accordion-item-missed-' + item.requestLeave_ID + '" role="tabpanel" data-parent="#div-accordion-missed-request"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview text-center" style="padding:10px "> <i> "' + item.reason + '"</i></div></div>';
                    string += '<div class="displayhere"></div></div></div></div>';
                });

                if (string === '') {
                    string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> No missed leave request</div>';
                }
                $("#div-accordion-missed-request").html(string);
                var count = $("#request-missed-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#request-missed-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#request-missed-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#request-missed-count").html(1 + ' - ' + perPage);
                } else {
                    $("#request-missed-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#portlet-missed');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#portlet-missed');
            }
        });
    }
    //ONGOING RETRACTION _-----------------------------------

    function getMissedRetraction(limiter, perPage, callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_missed_approval_retraction_list",
            data: {
                limiter: limiter,
                perPage: perPage
            },
            beforeSend: function() {
                mApp.block('#portlet-missed', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#retraction-missed-total").html(res.total);
                $("#retraction-missed-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-retraction-missed-' + item.retractLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    if (item.reason.length > 50) {
                        var substr = item.reason.substring(0, 50) + "...";
                    } else {
                        var substr = item.reason;
                    }
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9"><b>' + item.lname + ', ' + item.fname + '</b> <i class="fa fa-caret-right"></i> <small>' + substr + '</small></div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div></div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body retraction collapse" data-type="ongoing" data-retractleaveid="' + item.retractLeave_ID + '" id="accordion-item-retraction-missed-' + item.retractLeave_ID + '" role="tabpanel" data-parent="#div-accordion-missed-retraction"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview text-center" style="padding:10px "> <i> "' + item.reason + '"</i></div></div>';
                    string += '<div class="displayhere"></div></div></div></div>';
                });

                if (string === '') {
                    string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record !</strong> No missed leave retraction request.	</div>';
                }
                $("#div-accordion-missed-retraction").html(string);
                var count = $("#retraction-missed-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#retraction-missed-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#retraction-missed-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#retraction-missed-count").html(1 + ' - ' + perPage);
                } else {
                    $("#retraction-missed-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#portlet-missed');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#portlet-missed');
            }
        });
    }
    //--------------END OF MISSED---------------------------------------------
    function getHistoryRequest(limiter, perPage, callback) {

        var searchcustom = $("#search-custom-history-request").val();
        var searchstatus = $("#search-status-history-request").val();
        var datestart = $("#request-history-daterange").data('daterangepicker').startDate._d;
        var dateend = $("#request-history-daterange").data('daterangepicker').endDate._d;

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_all_request_list",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchcustom: searchcustom,
                searchstatus: searchstatus,
                datestart: moment(datestart).format("YYYY-MM-DD"),
                dateend: moment(dateend).add(1, 'day').format("YYYY-MM-DD")
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#request-history-total").html(res.total);
                $("#request-history-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + item.requestLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    if (item.reason.length > 50) {
                        var substr = item.reason.substring(0, 50) + "...";
                    } else {
                        var substr = item.reason;
                    }
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9"><b>' + item.lname + ', ' + item.fname + '</b> <i class="fa fa-caret-right"></i> <small>' + substr + '</small></div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div></div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body collapse" data-type="history" data-requestleaveid="' + item.requestLeave_ID + '" id="accordion-item-' + item.requestLeave_ID + '" role="tabpanel" data-parent="#div-accordion-history"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '<div class="row"><div class="col-md-9">';
                    string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview text-center" style="padding:6px "> <i>"' + item.reason + '"</i></div></div></div>';
                    string += '<div class="col-md-3"><a href="#" class="btn btn-brand m-btn m-btn--icon m-btn--outline-2x pull-right"  data-requestleaveid="' + item.requestLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal" title="View Approval"><span><i class="fa  flaticon-list-2"></i><span class="button-text">View Approval</span></span></a><br /><br /></div></div>';
                    string += '<div class="displayhere"></div></div></div></div></div>';
                });
                if (string === '') {
                    string += '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Records Found! </strong> <br> Sorry, no records were found. Please adjust your search criteria and try again.</div>';
                }
                $("#div-accordion-history").html(string);
                var count = $("#request-history-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#request-history-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#request-history-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#request-history-count").html(1 + ' - ' + perPage);
                } else {
                    $("#request-history-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }

    function getHistoryDate(limiter, perPage, callback) {
        var searchpayment = $("#search-payment-history-date").val();
        var searchreleased = $("#search-released-history-date").val();
        var searchname = $("#search-name-history-date").val();
        var searchminutes = $("#search-minutes-history-date").val();
        var searchstatus = $("#search-status-history-date").val();
        var searchleavetype = $("#search-leavetype-history-date").val();
        var datestart = $("#date-history-daterange").data('daterangepicker').startDate._d;
        var dateend = $("#date-history-daterange").data('daterangepicker').endDate._d;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_all_leavedates_list",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchpayment: searchpayment,
                searchreleased: searchreleased,
                searchminutes: searchminutes,
                searchstatus: searchstatus,
                searchname: searchname,
                searchleavetype: searchleavetype,
                datestart: moment(datestart).format("YYYY-MM-DD"),
                dateend: moment(dateend).format("YYYY-MM-DD")
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#date-history-total").html(res.total);
                $("#date-history-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    var minutes = (item.minutes === "480") ? "Full" : "Half";
                    var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                    if (item.overAllStatus === "5") {
                        var isReleased = (item.isReleased === "1") ? "Yes" : "No";
                    } else {
                        var isReleased = "--";
                    }
                    string += "<tr>";
                    string += '<td class="m--font-bolder text-left  pl-2" style="font-size:13px">' + item.lname + ', ' + item.fname + '</td>';
                    string += '<td class="m--font-bolder">' + moment(item.date, "YYYY-MM-DD").format('MMM DD, YYYY') + '</td>';
                    string += '<td class="text-left  pl-2"  style="font-size:13px" title="' + item.leaveType + ' Leave">' + item.leave_name + '</td>';
                    string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.description + '</span></td>';
                    string += '<td>' + minutes + ' day</td>';
                    string += '<td>' + isPaid + '</td>';
                    // string += '<td>' + isReleased + '</td>';
                    string += '<td> <a href="#" class="m-link m-link--state m-link--primary" data-toggle="modal" data-target="#date-modal" data-requestleaveid="' + item.requestLeave_ID + '" data-requestleavedetailsid="' + item.requestLeaveDetails_ID + '"><i class="fa fa-eye"></i> View</a></td>';
                    string += "</tr>";
                });
                if (string === '') {
                    $("#date-alert").show();
                    $("#table-date").hide();
                } else {
                    $("#date-alert").hide();
                    $("#table-date").show();
                }
                $("#table-date tbody").html(string);
                var count = $("#date-history-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#date-history-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#date-history-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#date-history-count").html(1 + ' - ' + perPage);
                } else {
                    $("#date-history-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }

    $(function() {

        //-------------------------------START OF MISSED----------------------------------------------------------------

        $('a[href="#tab-missed-request"]').on('show.bs.tab', function(event) {
            $("#request-missed-perpage").change(function() {
                var perPage = $("#request-missed-perpage").val();
                getMissedRequest(0, perPage, function(total) {
                    $("#request-missed-pagination").pagination('destroy');
                    $("#request-missed-pagination").pagination({
                        items: total, //default
                        itemsOnPage: $("#request-missed-perpage").val(),
                        hrefTextPrefix: "#",
                        cssStyle: 'light-theme',
                        onPageClick: function(pagenumber) {
                            var perPage = $("#request-missed-perpage").val();
                            getMissedRequest((pagenumber * perPage) - perPage, perPage);
                        }
                    });
                });
            });
            $("#request-missed-perpage").change();
        });
        $('a[href="#tab-missed-retraction"]').on('show.bs.tab', function(event) {
            $("#retraction-missed-perpage").change(function() {
                var perPage = $("#retraction-missed-perpage").val();
                getMissedRetraction(0, perPage, function(total) {
                    $("#retraction-missed-pagination").pagination('destroy');
                    $("#retraction-missed-pagination").pagination({
                        items: total, //default
                        itemsOnPage: $("#retraction-missed-perpage").val(),
                        hrefTextPrefix: "#",
                        cssStyle: 'light-theme',
                        onPageClick: function(pagenumber) {
                            var perPage = $("#retraction-missed-perpage").val();
                            getMissedRetraction((pagenumber * perPage) - perPage, perPage);
                        }
                    });
                });
            });
            $("#retraction-missed-perpage").change();
        });
        $('a[href="#tab-missed-request"]').click();

        $("#div-tabs-missed").on('show.bs.collapse', ".m-accordion__item-body.request", function() {

            var requestLeave_ID = $(this).data('requestleaveid');
            var accordionType = $(this).data('type');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_request_details",
                data: {
                    requestLeave_ID: requestLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    //                        console.log(res);
                    var string = '';

                    string += '<div class="row"><div class="col-md-12"><div class="btn-group m-btn-group pull-right" role="group" aria-label="...">\n\
<button type="button" title="View Approval" class="m-btn btn btn-sm btn-brand"  data-requestleaveid="' + requestLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal"><i class="fa flaticon-list-2"></i> <span class="button-text">View Approval</span></button>\n\
<button type="button" title="Select All" class="m-btn btn btn-sm btn-secondary" onclick="selectAll(this)"><i class="fa fa-check m--font-success"></i> <span class="button-text">Select All</span></button>\n\
<button type="button" title="Clear All" class="m-btn btn btn-sm btn-secondary" onclick="clearAll(this)"><i class="fa fa-times m--font-danger"></i> <span class="button-text">Clear All</span></button>\n\
</div>';
                    string += '</div><br /><br /><div class="col-md-12"><div style="overflow:auto;max-height:250px">';
                    string += '<table class="table table-sm m-table table-striped table-bordered table-hover">';
                    string += '<thead>';
                    string += '<tr>';
                    string += '<th>Date</th>';
                    string += '<th>Leave Description</th>';
                    string += '<th>Type</th>';
                    string += '<th>Note</th>';
                    string += '<th>Final Status</th>';
                    string += '<th>Approval</th>';
                    string += '</tr></thead><tbody>';
                    $.each(res.data, function(i, item) {
                        var minutes = (item.minutes === "480") ? "Full" : "Half";
                        var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                        string += "<tr>";
                        string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                        string += "<td>" + item.leave_name + " Leave</td>";
                        string += "<td>" + item.leaveType + " Leave</td>";
                        string += "<td>" + minutes + " Day (" + isPaid + ")</td>";
                        string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.description + '</span></td>';
                        if (item.pickerMode === 'range') {
                            string += '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-requestleavedetails_id="' + item.requestLeaveDetails_ID + '" checked disabled> <span></span></label></td>';
                        } else {
                            string += '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-requestleavedetails_id="' + item.requestLeaveDetails_ID + '" checked> <span></span></label></td>';
                        }
                        string += "</tr>";
                    });
                    string += "</tbody></table>";

                    string += "</div>";
                    string += '<hr /><div class="row"><div class="col-md-12"><div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview" style="padding:10px "><label for="textarea">Enter your comment or notes:</label><textarea class="form-control m-input" maxlength="255" rows="2"></textarea></div></div></div>';

                    string += '<div class="col-md-12 text-center"> <a href="#" onclick="submitApproval(this,' + requestLeave_ID + ')" class=" btn m-btn--pill pull-right btn-outline-brand m-btn m-btn--custom" title="Submit Approval"><span><i class="fa fa-send-o"></i><span class="button-text"> Submit Approval</span></span></a></div></div>';

                    string += '</div></div>';
                    $("#accordion-item-missed-" + requestLeave_ID).find('.displayhere').html(string);
                }
            });
        });
        $("#div-tabs-missed").on('show.bs.collapse', ".m-accordion__item-body.retraction", function() {

            var retractLeave_ID = $(this).data('retractleaveid');
            var accordionType = $(this).data('type');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_retraction_details",
                data: {
                    retractLeave_ID: retractLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    var string = '';

                    string += '<div class="row"><div class="col-md-12"><div class="btn-group m-btn-group pull-right" role="group" aria-label="...">\n\
                        <button type="button" title="View Approval" class="m-btn btn btn-sm btn-brand"  data-retractleaveid="' + retractLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal-retraction"><i class="fa flaticon-list-2"></i> <span class="button-text">View Approval</span></button>\n\
                        <button type="button" title="Select All" class="m-btn btn btn-sm btn-secondary" onclick="selectAll(this)"><i class="fa fa-check m--font-success"></i> <span class="button-text">Select All</span></button>\n\
                        <button type="button" title="Clear All" class="m-btn btn btn-sm btn-secondary" onclick="clearAll(this)"><i class="fa fa-times m--font-danger"></i> <span class="button-text">Clear All</span></button>\n\
                        </div>';
                    string += '</div><br /><br /><div class="col-md-12"><div style="overflow:auto;max-height:250px">';
                    string += '<table class="table table-sm m-table table-striped table-bordered table-hover">';
                    string += '<thead>';
                    string += '<tr>';
                    string += '<th>Date</th>';
                    string += '<th>Type</th>';
                    string += '<th>Final Status</th>';
                    string += '<th>Description</th>';
                    string += '<th>Payment</th>';
                    string += '<th>Approval</th>';
                    string += '</tr></thead><tbody>';
                    $.each(res, function(i, item) {
                        console.log(item);
                        var minutes = (item.minutes === "480") ? "Full" : "Half";
                        var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                        if (item.overAllStatus === '2') {
                            string += "<tr>";
                            string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                            string += "<td>" + item.leaveType + " Leave</td>";
                            string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.status + '</span></td>';
                            string += "<td>" + minutes + " Day</td>";
                            string += "<td>" + isPaid + "</td>";
                            string += '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-retractleavedetails_id="' + item.retractLeaveDetails_ID + '" checked> <span></span></label></td>';
                            string += "</tr>";
                        }

                    });
                    string += "</tbody></table>";

                    string += "</div>";
                    string += '<hr /><div class="row"><div class="col-md-12"><div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview" style="padding:10px "><label for="textarea">Enter your comment or notes:</label><textarea class="form-control m-input" maxlength="255" rows="2"></textarea></div></div></div>';

                    string += '<div class="col-md-12 text-center"> \n\
                        <a href="#" onclick="submitApprovalRetraction(this,' + retractLeave_ID + ')" class="btn m-btn--pill pull-right btn-outline-brand m-btn m-btn--custom" title="Submit Approval"><span><i class="fa fa-send-o"></i><span class="button-text"> Submit Approval</span></span></a></div></div>';

                    string += '</div></div>';
                    $("#accordion-item-retraction-missed-" + retractLeave_ID).find('.displayhere').html(string);
                }
            });
        });
        $("#approvalDetailsModal-retraction").on('show.bs.modal', function(e) {
            var retractLeave_ID = $(e.relatedTarget).data('retractleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_retraction_approval_flow",
                data: {
                    retractLeave_ID: retractLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        //                            console.log(res);
                        var retract_leave = res.retract_leave;
                        var retract_leave_approval = res.retract_leave_approval;
                        $("#view-name-retraction").html(retract_leave.fname + " " + retract_leave.lname);
                        $("#view-pic-retraction").attr('src', '<?php echo base_url(); ?>' + retract_leave.pic);
                        $("#view-job-retraction").html(retract_leave.job + " <small>(" + retract_leave.jobstat + ")</small>");
                        $("#view-requestdate-retraction").html("" + moment(retract_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                        $("#view-reason-retraction").html('"' + retract_leave.reason + '"');
                        $("#div-viewApproval-retraction").html("");
                        var string = '<div class="m-section" style="margin-bottom:0px"><div class="m-section__content">';

                        $.each(retract_leave_approval, function(key, obj) {
                            //                                console.log(obj);
                            var comment = ($.trim(obj.comment) === '') ? 'No comment' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                            string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                            string += '<div class="m-portlet__head-caption">';
                            string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">';
                            string += '<p class="text-center"><strong style="color:white">' + obj.fname + ' ' + obj.lname + '</strong> <small style="color:#ddd;">( ' + decisionOn + ' )</small><br></p>';
                            string += '</p></div></div>';

                            string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                            string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                            string += '  <i class="fa fa-quote-left"></i>';
                            string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                            string += ' <i class="fa fa-quote-right"></i>';
                            string += ' </blockquote>';
                            string += '<div style="max-height:250px;overflow:auto;font-size:12px"><table class="table table-striped m-table table-sm">';
                            string += '<thead><tr><th>Date</th><th>Type</th><th>Description</th><th>Payment</th><th>Decision</th></tr></thead>';
                            string += '<tbody>';
                            $.each(obj.details, function(i, item) {
                                var minutes = (item.minutes === '480') ? "Full Day" : "Half Day";
                                var paid = (item.isPaid === '1') ? "Paid" : "Unpaid";
                                string += '<tr>';
                                string += '<td class="m--font-bolder">' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY') + '</td>';
                                string += '<td>' + item.leaveType + ' Leave</td>';
                                string += '<td>' + minutes + '</td>';
                                string += '<td>' + paid + '</td>';
                                string += '<td><span class="m-badge m-badge--' + statusClass[item.status] + ' m-badge--wide">' + item.description + '</span></td>';
                                string += '</tr>';
                            });
                            string += '</tbody></table></div>';

                            string += '</div></div></div>';
                        });
                        string += "</div></div>";
                        $("#div-viewApproval-retraction").html(string);
                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }
                    $(".m-widget7__img").on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                }
            });
        });
        //-------------------------------END OF MISSED----------------------------------------------------------------
        //DATEPICKER--------------------------------------------------------

        $('#request-history-daterange .form-control').val(moment().subtract(29, "days").format('MMM DD, YYYY') + ' - ' + moment().format('MMM DD, YYYY'));
        $('#request-history-daterange').daterangepicker({
                startDate: moment().subtract(29, "days"),
                endDate: moment(),
                opens: 'center',
                drops: 'down',
                autoUpdateInput: true,
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('year')]
                }
            },
            function(start, end, label) {
                $('#request-history-daterange .form-control').val(start.format('MMM DD, YYYY') + ' - ' + end.format('MMM DD, YYYY'));
                $("#request-history-perpage").change();
            });
        $('#date-history-daterange .form-control').val(moment().subtract(29, "days").format('MMM DD, YYYY') + ' - ' + moment().format('MMM DD, YYYY'));
        $('#date-history-daterange').daterangepicker({
                startDate: moment().subtract(29, "days"),
                endDate: moment(),
                opens: 'center',
                drops: 'down',
                autoUpdateInput: true,
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('year')]
                }
            },
            function(start, end, label) {
                $('#date-history-daterange .form-control').val(start.format('MMM DD, YYYY') + ' - ' + end.format('MMM DD, YYYY'));
                $("#date-history-perpage").change();
            });
        //END OF DATEPICKER-------------------------------------------------
        $("#display-type").change(function() {
            if ($(this).val() === 'request') {
                $("#div-perrequest").show(500);
                $("#div-perdate").hide();
                $("#request-history-perpage").change();
            } else {
                $("#div-perrequest").hide();
                $("#div-perdate").show(500);
                $("#date-history-perpage").change();
            }
        });
        $("#display-type").change();
        $("#div-tabs").on('show.bs.collapse', ".m-accordion__item-body", function() {

            var requestLeave_ID = $(this).data('requestleaveid');
            var accordionType = $(this).data('type');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_request_details",
                data: {
                    requestLeave_ID: requestLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    //                        console.log(res);
                    var string = '';
                    string += '<div style="max-height:250px;overflow:auto"><table class="table table-sm m-table table-striped table-bordered table-hover">';
                    string += '<thead>';
                    string += '<tr>';
                    string += '<th>Date</th>';
                    string += '<th>Leave Description</th>';
                    string += '<th>Type</th>';
                    string += '<th>Note</th>';
                    string += '<th>Final Status</th>';
                    string += '<th>Actions</th>';
                    string += '</tr></thead><tbody>';
                    $.each(res.data, function(i, item) {
                        var minutes = (item.minutes === "480") ? "Full" : "Half";
                        var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                        string += "<tr>";
                        string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                        string += "<td>" + item.leave_name + "</td>";
                        string += "<td>" + item.leaveType + " Leave</td>";
                        string += "<td>" + minutes + " Day (" + isPaid + ")</td>";
                        string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.description + '</span></td>';
                        string += '<td> <a href="#" class="m-link m-link--state m-link--primary" data-toggle="modal" data-target="#date-modal" data-requestleaveid="' + requestLeave_ID + '" data-requestleavedetailsid="' + item.requestLeaveDetails_ID + '"><i class="fa fa-eye"></i> View</a></td>';
                        string += "</tr>";
                    });
                    string += "</tbody></table></div></div>";
                    $("#accordion-item-" + requestLeave_ID).find('.displayhere').html(string);

                }
            });
        });
        $("#approvalDetailsModal").on('show.bs.modal', function(e) {
            var requestLeave_ID = $(e.relatedTarget).data('requestleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_request_approval_flow",
                data: {
                    requestLeave_ID: requestLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        //                            console.log(res);
                        var request_leave = res.request_leave;
                        var request_leave_approval = res.request_leave_approval;
                        $("#view-name").html(request_leave.fname + " " + request_leave.lname);
                        $("#view-pic").attr('src', '<?php echo base_url(); ?>' + request_leave.pic);
                        $("#view-job").html(request_leave.job + " <small>(" + request_leave.jobstat + ")</small>");
                        $("#view-requestdate").html("" + moment(request_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                        $("#view-reason").html('"' + request_leave.reason + '"');
                        $("#div-viewApproval").html("");
                        var string = '<div class="m-section" style="margin-bottom:0px"><div class="m-section__content">';

                        $.each(request_leave_approval, function(key, obj) {
                            var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                            var comment = ($.trim(obj.comment) === '') ? 'No comment' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                            string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                            string += '<div class="m-portlet__head-caption">';
                            string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">';
                            string += '<p class="text-center">' + blinker + '<strong style="color:white">' + obj.fname + ' ' + obj.lname + '</strong> <small style="color:#ddd;">( ' + decisionOn + ' )</small><br></p>';
                            string += '</p></div></div>';

                            string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                            string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                            string += '  <i class="fa fa-quote-left"></i>';
                            string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                            string += ' <i class="fa fa-quote-right"></i>';
                            string += ' </blockquote>';
                            string += '<div style="max-height:250px;overflow:auto;font-size:12px"><table class="table table-striped m-table table-sm">';
                            string += '<thead><tr><th>Date</th><th>Type</th><th>Description</th><th>Payment</th><th>Decision</th></tr></thead>';
                            string += '<tbody>';
                            $.each(obj.details, function(i, item) {
                                var minutes = (item.minutes === '480') ? "Full Day" : "Half Day";
                                var paid = (item.isPaid === '1') ? "Paid" : "Unpaid";
                                string += '<tr>';
                                string += '<td class="m--font-bolder">' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY') + '</td>';
                                string += '<td>' + item.leaveType + ' Leave</td>';
                                string += '<td>' + minutes + '</td>';
                                string += '<td>' + paid + '</td>';
                                string += '<td><span class="m-badge m-badge--' + statusClass[item.status] + ' m-badge--wide">' + item.description + '</span></td>';
                                string += '</tr>';
                            });
                            string += '</tbody></table></div>';

                            string += '</div></div></div>';
                        });
                        string += "</div></div>";
                        $("#div-viewApproval").html(string);
                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }
                    $(".m-widget7__img").on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                }
            });
        });

        $("#date-modal").on('show.bs.modal', function(e) {
            var requestLeaveDetails_ID = $(e.relatedTarget).data('requestleavedetailsid');
            var requestLeave_ID = $(e.relatedTarget).data('requestleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_date_approval_flow",
                data: {
                    requestLeave_ID: requestLeave_ID,
                    requestLeaveDetails_ID: requestLeaveDetails_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        var request_leave = res.request_leave;
                        var retract_leave = res.retract_leave;
                        var request_leave_approval = res.request_leave_approval;
                        var retract_leave_approval = res.retract_leave_approval;

                        var minutes = (request_leave.minutes === "480") ? "Full Day" : "Half Day";

                        var ispaid = (request_leave.isPaid === "1") ? "Yes" : "No";
                        var isReleased = (request_leave.isReleased === "1") ? "Yes" : "No";
                        // $("#date-isreleased").html(isReleased);
                        $("#date-ispaid").html(ispaid);
                        $("#date-minutes").html(minutes);
                        $("#date-name").html(request_leave.fname + " " + request_leave.lname);
                        $("#date-pic").attr('src', request_leave.pic);
                        $("#date-job").html(request_leave.job + " <small>(" + request_leave.jobstat + ")</small>");
                        $("#date-leavetype").html("<span style='text-transform:uppercase'>" + request_leave.leaveType + " LEAVE</span>");
                        $("#date-date").html("<span>" + moment(request_leave.date, 'YYYY-MM-DD').format("MMMM DD, YYYY") + "</span>");
                        $("#modal-request-createdOn").html("" + moment(request_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                        $("#modal-request-reason").html('"' + request_leave.reason + '"');

                        $("#tab-modal-request").html("");
                        $("#tab-modal-retraction").html("");
                        var string = '';
                        var customcnt = 1;
                        $.each(request_leave_approval, function(key, obj) {
                            var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                            //                                -------------------------------------------------------------------------------------- -
                            string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                            string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                            string += '<div class="m-portlet__head-caption">';
                            string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' ' + blinker + '</p>';
                            string += '</div></div>';
                            string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                            string += '   <div class="row">';
                            string += '<div class="col-md-12">';
                            string += ' <table class="table m-table table-bordered table-sm">';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                            string += ' </tr>';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                            string += ' </tr>';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                            string += ' </tr>';
                            string += '</table>';
                            string += '</div>';
                            string += '    <div class="col-md-12" >';
                            string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                            string += '  <i class="fa fa-quote-left"></i>';
                            string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                            string += ' <i class="fa fa-quote-right"></i>';
                            string += ' </blockquote>';
                            string += '</div>';
                            string += '</div>';
                            string += '</div></div></div>';
                            customcnt++;
                        });
                        string += "";
                        $("#tab-modal-request").html(string);

                        if (retract_leave === null || retract_leave_approval === null) {
                            $("#modal-retraction-approvals").hide();
                        } else {

                            var string = '';
                            var customcnt = 1;
                            $.each(retract_leave_approval, function(key, obj) {
                                var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                                var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                                var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                                //                                -------------------------------------------------------------------------------------- -
                                string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                                string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                                string += '<div class="m-portlet__head-caption">';
                                string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' ' + blinker + '</p>';
                                string += '</div></div>';
                                string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;;color:black;background:#e6e6e6"">';
                                string += '   <div class="row">';
                                string += '<div class="col-md-12">';
                                string += ' <table class="table m-table table-bordered table-sm">';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                                string += ' </tr>';
                                string += '</table>';
                                string += '</div>';
                                string += '    <div class="col-md-12" >';
                                string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                                string += '  <i class="fa fa-quote-left"></i>';
                                string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                                string += ' <i class="fa fa-quote-right"></i>';
                                string += ' </blockquote>';
                                string += '</div>';
                                string += '</div>';
                                string += '</div></div></div>';
                                customcnt++;
                            });
                            string += "";
                            $("#tab-modal-retraction").html(string);

                            $("#modal-retraction-approvals").show();
                        }

                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }
                    $("#date-pic").on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                }
            });
        });
        $("#btn-missed").click(function() {
            if ($(this).data('stat') === 'open') {
                $("#portlet-missed").hide(500);
                $(this).data('stat', 'close');
            } else {
                $("#portlet-missed").show(500);
                $(this).data('stat', 'open');
            }

            $('i', this).toggleClass('fa-chevron-up fa-chevron-down');
        });



        //xxx1
        $("#div-accordion-ongoing").html("");
        //HISTORY PAGINATION-----------------------------------------------------

        $("#request-history-perpage").change(function() {
            var perPage = $("#request-history-perpage").val();
            getHistoryRequest(0, perPage, function(total) {
                $("#request-history-pagination").pagination('destroy');
                $("#request-history-pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#request-history-perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    onPageClick: function(pagenumber) {
                        var perPage = $("#request-history-perpage").val();
                        getHistoryRequest((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });
        $("#search-custom-history-request").keyup(function() {
            $("#request-history-perpage").change();
        })
        $("#search-status-history-request").change(function() {
            $("#request-history-perpage").change();
        });
        $("#request-history-perpage").change();

        //xxx2
        $("#date-history-perpage").change(function() {
            var perPage = $("#date-history-perpage").val();
            getHistoryDate(0, perPage, function(total) {
                $("#date-history-pagination").pagination('destroy');
                $("#date-history-pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#date-history-perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    onPageClick: function(pagenumber) {
                        var perPage = $("#date-history-perpage").val();
                        getHistoryDate((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });

        $("#search-name-history-date").keyup(function() {
            $("#date-history-perpage").change();
        });
        $("#search-status-history-date,#search-leavetype-history-date,#search-released-history-date,#search-payment-history-date,#search-minutes-history-date").change(function() {
            $("#date-history-perpage").change();
        });
    });
</script>