<style type="text/css">
    label {
        font-weight: 402 !important;
        font-size: 12px !important;
    }

    .form-control[readonly] {
        background: #ddd !important;
    }

    body {
        padding-right: 0 !important;
    }

    .bg-custom-gray {
        background: #4B4C54 !important;
    }

    .font-white {
        color: white !important;
    }

    .font-poppins {
        font-family: 'Poppins' !important;
    }

    td.day.disabled {
        opacity: 0.2;
    }

    #leaveDatePicker,
    #leaveDateRange {
        padding: 0px;
    }

    .hide {
        display: none;
    }

    .pad0TB {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
    }

    .pad10L {
        padding-left: 10px !important;
    }

    .pad10R {
        padding-right: 10px !important;
    }

    .m-table thead th {
        background: #545454;
        color: white;
    }

    .m-table th {
        border: 1px solid #545454 !important;
        text-align: center;
    }

    .m-table tr:not(.sub-header) td {
        border: 1px solid #9B9C9E !important;
        text-align: center;
    }

    .sub-header {
        background: #716AC8;
        color: white;
    }

    .sub-header td {
        border: 1px solid #716AC8 !important;
        text-align: center;
    }

    .m-accordion__item {
        border: 1.2px solid #545454 !important;
    }

    .m-accordion__item-mode {
        margin-left: 5px;
        font-size: 15px !important;
    }

    .m-accordion__item-icon i {
        font-size: 18px !important;
    }

    .m-accordion__item-title {
        font-size: 15px !important;
    }

    .m-accordion__item-head {
        background: #545454 !important;
        color: white !important;
    }

    .m-accordion__item-head:hover {
        background: #777 !important;
    }

    textarea {
        resize: none;
    }

    @media screen and (max-width: 600px) {
        .button-text {
            display: none !important;
        }

    }

    .m-demo__preview {
        border-color: #eee !important;
    }

    .modal .m-demo .m-demo__preview {
        border: 2px solid #999 !important;
        border-radius: 2px !important;
    }

    div {
        word-wrap: break-word;
    }

    .m-content hr {
        border: 3px solid #eee !important;
    }

    .light-theme a,
    .light-theme span {
        padding: 1.5px 8px !important;
    }

    .m-content .m-stack__item {
        padding: 0 !important;
        background: 0 !important;
        border: 0 !important;
    }

    .m-badge {
        font-weight: 550;
    }

    .blockOverlay {
        opacity: 0.15 !important;
        background-color: #746DCB !important;
    }

    .multipleSchedule {
        color: #5867DD !important;
        border-color: #5867DD !important;
    }

    .tdplain tr td {
        border: 1px solid #f4f5f8 !important;
    }

    .btn-outline-green {
        color: #716ACA !important;
        background: white !important;
        border-color: #716ACA !important;
    }

    .btn-outline-green:hover {
        background: #716ACA !important;
        color: white !important;
        border-color: #716ACA !important;
    }
</style>


<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/request'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">My Request</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="btn-group m-btn-group " role="group" aria-label="...">
                <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Leave/retraction'"><span><i class="fa fa-refresh"></i><span>Leave Retraction</span></span></button>
                <button type="button" class="m-btn btn m-btn m-btn--icon btn-outline-green" onclick="location.href = '<?php echo base_url(); ?>Leave/personal_list'"><span><i class="fa fa-list"></i><span>Personal List</span></span></button>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="alert alert-dismissible fade show m-alert m-alert--air m-alert--outline hide" id="topNotification" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
            <strong id="topNotification-title"></strong> <span id="topNotification-message"></span>
        </div>
        <div class="alert alert-dismissible fade show m-alert m-alert--air m-alert--outline hide" id="topNotification-promote" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
            <strong id="topNotification-title-promote"></strong> <span id="topNotification-message-promote"></span>
        </div>
        <div class="row">

            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_blockui_2_portlet">
                    <div class="m-portlet__head bg-custom-gray">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins">
                                    <span class="font-white"> Request Leave</span> <small class="font-white"> File leave and view transactions</small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                <button type="button" class="m-btn btn" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#fileLeaveModal" title="File a leave"><i class="fa fa-calendar-plus-o" style="margin-bottom:3px"></i> <span class="button-text">File a leave</span></button>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-ongoing" role="tab"><i class="la la-tasks"></i> Ongoing Request</a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-history" role="tab"><i class="la la-history"></i> History</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-ongoing" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h5 class="my-3">
                                            Ongoing Request <br />
                                            <small class="text-muted">Pending and currently under approval requests.</small>
                                        </h5>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-ongoing" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="request-ongoing-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="request-ongoing-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="request-ongoing-count"></span> of <span id="request-ongoing-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-history" role="tabpanel">
                                <div class="row mb-3 no-gutters">
                                    <div class="col-lg-4 col-md-4 text-center">
                                        <h5 class="my-3 text-center">
                                            Leave History <br />
                                            <small class="text-muted">Previous records of requests.</small>
                                        </h5>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <!-- <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px"> -->
                                        <div class="row m-0 p-2" style="background:#eee">
                                           
                                            <div class="col-md-6">
                                                <label for="">Date Filed:</label>
                                                <div class="m-input-icon m-input-icon--left" id="request-history-daterange">
                                                    <input type="text" class="form-control m-input form-control-sm" placeholder="Date Range" readonly>
                                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="">Search Reason:</label>
                                                <input type="text" class="form-control m-input form-control-sm" placeholder="Search Reason..." id="search-custom-history-request">

                                            </div>
                                            <!-- </div>

                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-history" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="request-history-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="request-history-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="request-history-count"></span> of <span id="request-history-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="fileLeaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Leave Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-alert m-alert--outline alert alert-warning fade show" role="alert" id="fileAlert" style="display:none">
                    <span></span>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    <div class="m-portlet__body">
                        <div id="div-filing">
                            <div class="row">
                                <div class="col-lg-7">
                                    <label>Leave Names:</label>
                                    <select class="form-control m-input" id="leave_names" name="leave_names">
                                    </select>
                                    <p class="m-form__help text-center" id="leave_description" style="text-transform: capitalize;"></p>

                                    <label>Reason:</label>
                                    <textarea class="form-control m-input" maxlength="255" id="reason" name="reason" rows="9" required></textarea>
                                    <p class="m-form__help text-center">Please state your reason for leave. <small>(You have <i class="m--font-danger" id="textarea-note">255</i> remaining characters)</small></p>
                                </div>
                                <div class="col-lg-5" class="div-datepicker">
                                    <label>Dates of Leave:</label>
                                    <div class="datepicker hide" id="leaveDatePicker"></div>
                                    <div class="datepicker hide" id="leaveDateRange"></div>
                                    <p class="m-form__help" id="leaveDateHelp"></p>
                                </div>
                            </div>
                            <br />
                            <button type="button" class="btn btn-primary" id="btnGenerate">Continue</button>
                        </div>
                        <div id="div-preview" class="hide">
                            <div id="div-preview-display" style="max-height:450px;overflow:auto"></div>
                            <br />
                            <button type="button" class="btn btn-secondary" id="btnBack">Back to form</button>
                            <button type="button" class="btn btn-primary" id="btnSubmitApplication">Submit Application</button>
                            <br />
                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="date-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Details of Leave Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto;padding-top:0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img" id="date-pic" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="date-name" style="font-size:20px">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="date-job" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <table class="table m-table table-bordered table-sm">
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Type:</td>
                                <td id="date-leavetype" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date:</td>
                                <td id="date-date" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Class:</td>
                                <td id="date-minutes" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Paid:</td>
                                <td id="date-ispaid" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <!-- <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Release:</td>
                                <td id="date-isreleased" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr> -->

                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:</td>
                                <td id="modal-request-reason" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>

                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Filed On:</td>
                                <td id="modal-request-createdOn" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>

                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div id="tab-modal-request">

                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div id="modal-retraction-approvals" style="display:none">
                            <hr />
                            <p class="m--font-bolder">RETRACTION APPROVALS</p>
                            <div id="tab-modal-retraction">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approvalDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Approval Process</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto;padding-top:0 !important;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img" id="view-pic" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="view-name" style="font-size:20px">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="view-job" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <div class="col-lg-12">
                        <table class="table m-table table-bordered">

                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:
                                <td id="view-reason" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date Filed:</td>
                                <td id="view-requestdate" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>
                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div id="div-viewApproval">
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script type="text/javascript">
    var statusClass = ["", "", "warning", "", "info", "success", "danger", "focus", "", "", "", "", "metal", "primary"];
    var getOrdinal = function(number) {
        var ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
        if (((number % 100) >= 11) && ((number % 100) <= 13)) {
            return number + 'th';
        } else {
            return number + ends[number % 10];
        }
    };
    var enumerateDaysBetweenDates = function(startDate, endDate) {
        var now = startDate,
            dates = [];
        while (now.isSameOrBefore(endDate)) {
            dates.push(now.format('YYYY-MM-DD'));
            now.add(1, 'days');
        }
        return dates;
    };

    function deleteRequestDetail(requestLeaveDetails_ID, that) {

        swal({
            title: 'Are you sure?',
            text: "Date will be removed!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn btn-danger m-btn m-btn--air",
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, remove it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/delete_request_detail",
                    data: {
                        requestLeaveDetails_ID: requestLeaveDetails_ID
                    },
                    cache: false,
                    success: function(res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'All') {
                            swal("Success!", "Leave has been deleted!", "success");
                            systemNotification(res.notif_details[0].notif_id);
                            $('a[href="#tab-history"]').click();
                            $('a[href="#tab-ongoing"]').click();
                        } else if (res.status === 'Success') {
                            swal("Success!", "Leave date has deleted!", "success");
                            systemNotification(res.notif_details[0].notif_id);
                            $(that).collapse("hide");
                            $(that).collapse("show");
                        } else {
                            swal("Failed!", "Something happened while deleting leave request details!", "error");
                            $('a[href="#tab-history"]').click();
                            $('a[href="#tab-ongoing"]').click();
                        }

                    }
                });
            }
        });
    }

    function deleteRequest(requestLeave_ID) {

        swal({
            title: 'Are you sure?',
            text: "Request will be removed!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn btn-danger m-btn m-btn--air",
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, remove it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Leave/delete_request",
                    data: {
                        requestLeave_ID: requestLeave_ID
                    },
                    cache: false,
                    success: function(res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            swal("Success!", "Leave request has been deleted!", "success");
                            systemNotification(res.notif_details[0].notif_id);
                            $('a[href="#tab-history"]').click();
                            $('a[href="#tab-ongoing"]').click();
                        } else {
                            swal("Failed!", "Something happened while deleting leave request!", "error");
                            $('a[href="#tab-history"]').click();
                            $('a[href="#tab-ongoing"]').click();
                        }
                    }
                });
            }
        });
    }

    function saveNormalFiling() {
        var leave_id = $("#leave_names").val();
        var reason = $("#reason").val();
        var data = [];
        $(".final-data").each(function(index, elem) {
            data.push({
                date: $(elem).data('date'),
                paid: $(elem).data('paid'),
                fullhalf: $(elem).find('select[name^=fullhalfselector]').val(),
                schedule_ID: $(elem).find('select.scheduleSelector').val(),
                useBackup: $(elem).find('input[type=checkbox]').is(':checked')
            });
        });
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/new_leave_request",
            data: {
                leave_id: leave_id,
                reason: reason,
                data: data
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $('#fileLeaveModal').modal('hide');
                if (res.status === 'Success') {
                    swal("Good job!", "Leave Request success!", "success");
                    notification(res.notif_details.notif_id);
                    getOngoingRequest(0, $("#request-ongoing-perpage").val(), function() {});
                } else {
                    swal("Failed!", "Something happened while requesting leave!", "error");
                }

            }
        });
    }

    function getOngoingRequest(limiter, perPage, callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_ongoing_request_list",
            data: {
                limiter: limiter,
                perPage: perPage
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#request-ongoing-total").html(res.total);
                $("#request-ongoing-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + item.requestLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9">' + item.reason + '</div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div> </div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body collapse" data-type="ongoing" data-requestleaveid="' + item.requestLeave_ID + '" id="accordion-item-' + item.requestLeave_ID + '" role="tabpanel" data-parent="#div-accordion-ongoing"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '</div></div></div>';
                });
                if (string === '') {
                    string = '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have ongoing leave request. </div>';
                }
                $("#div-accordion-ongoing").html(string);
                var count = $("#request-ongoing-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#request-ongoing-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#request-ongoing-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#request-ongoing-count").html(1 + ' - ' + perPage);
                } else {
                    $("#request-ongoing-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }

    function getHistoryRequest(limiter, perPage, callback) {

        var searchcustom = $("#search-custom-history-request").val();
        var datestart = $("#request-history-daterange").data('daterangepicker').startDate._d;
        var dateend = $("#request-history-daterange").data('daterangepicker').endDate._d;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_other_request_list",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchcustom: searchcustom,
                datestart: moment(datestart).format("YYYY-MM-DD"),
                dateend: moment(dateend).add(1, 'day').format("YYYY-MM-DD")
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#request-history-total").html(res.total);
                $("#request-history-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + item.requestLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9">' + item.reason + '</div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div> </div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body collapse" data-type="history" data-requestleaveid="' + item.requestLeave_ID + '" id="accordion-item-' + item.requestLeave_ID + '" role="tabpanel" data-parent="#div-accordion-history"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '</div></div></div>';
                });
                if (string === '') {
                    string = '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have leave request history yet. </div>';
                }
                $("#div-accordion-history").html(string);
                var count = $("#request-history-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#request-history-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#request-history-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#request-history-count").html(1 + ' - ' + perPage);
                } else {
                    $("#request-history-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }
    var schedulelogFilter = function(that) {
    	//2020-07-29
    	//REMOVE THIS LINE IF IBALIK NA ANG ORIGINAL FILTERS
 		$(that).siblings('select').html("<option value='480' selected>Full Leave</option><option value='240'>Half Leave</option>");
		$(that).siblings('select').show();
		//UNCOMMENT BELOW IF OKAY NA ANG ALLOWED NA ANG FILTERS AGAIN

        // var allowhalf = $(that).data('allowhalf');
        // var date = $(that).data('date');
        // var sched_id = $(that).val();
        // var schedtype_id = $(that).find(':selected').data('schedtypeid');
        // var clock_in = $(that).find(':selected').data('clockin');
        // var clock_out = $(that).find(':selected').data('clockout');

        // $(that).closest('tr').css("background-color", "");
        // $(that).siblings('h5').hide();
        // if (sched_id === '') {

        //     if (allowhalf === 1) {
        //         $(that).siblings('select').html("<option value='480' selected>Full Leave</option><option value='240'>Half Leave</option>");
        //     } else {
        //         $(that).siblings('select').html("<option value='480' selected>Full Leave</option>");
        //     }
        //     $(that).siblings('select').show();
        //     $(that).closest('tr').data('isOkay', true);
        // } else {
        //     if (schedtype_id === 1 || schedtype_id === 4 || schedtype_id === 5) {
        //         $.ajax({
        //             type: "POST",
        //             url: "<?php echo base_url(); ?>Leave/totalHoursForLeave",
        //             data: {
        //                 sched_id: sched_id,
        //                 clock_in: clock_in,
        //                 clock_out: clock_out
        //             },
        //             cache: false,
        //             success: function(res) {
        //                 res = JSON.parse(res.trim());

        //                 if (res.remaining_hour <= 0) {

        //                     //                                console.log("COMPLETE LOGS");
        //                     $(that).closest('tr').css("background-color", "#ccc");
        //                     $(that).siblings('select').hide();

        //                     $(that).siblings('h5').show();
        //                     $(that).siblings('h5').attr('data-original-title', "Already completed shift");
        //                     mApp.initTooltip($(that).siblings('h5'));
        //                     $(that).closest('tr').data('isOkay', false);
        //                 } else if (res.remaining_hour < 4) {
        //                     //                                console.log("CANNOT FILE LEAVE");//ex: 6 hrs na ang naduty but undertime ug 2 hrs pero ifile japon ug half para macover ang uban
        //                     if (allowhalf === 1) {
        //                         $(that).siblings('select').html("<option value='240' selected>Half Leave</option>");
        //                         $(that).siblings('select').show();
        //                         $(that).closest('tr').data('isOkay', true);
        //                     } else {
        //                         $(that).closest('tr').css("background-color", "#ccc");
        //                         $(that).siblings('select').hide();

        //                         $(that).siblings('h5').show();
        //                         $(that).siblings('h5').attr('data-original-title', "This type of leave does not allow a half leave");
        //                         mApp.initTooltip($(that).siblings('h5'));
        //                         $(that).closest('tr').data('isOkay', false);
        //                     }

        //                 } else if (res.remaining_hour >= 4 && res.remaining_hour <= 8) {
        //                     //                                console.log("ONLY HALF LEAVE");//ex:undertime jud cya. ex. 6 hrs undertime then file half leave, 2 hrs undertime, pwede japon full leave nalang
        //                     if (allowhalf === 1) {
        //                         $(that).siblings('select').html("<option value='480' selected>Full Leave</option><option value='240'>Half Leave</option>");
        //                         $(that).siblings('select').show();
        //                         $(that).closest('tr').data('isOkay', true);
        //                     } else {
        //                         $(that).siblings('select').html("<option value='480' selected>Full Leave</option>");
        //                         $(that).siblings('select').show();
        //                         $(that).closest('tr').data('isOkay', true);
        //                     }
        //                 } else {
        //                     //                                console.log("HALF AND FULL LEAVE");
        //                     if (allowhalf === 1) {
        //                         $(that).siblings('select').html("<option value='480' selected>Full Leave</option><option value='240'>Half Leave</option>");
        //                     } else {
        //                         $(that).siblings('select').html("<option value='480' selected>Full Leave</option>");
        //                     }
        //                     $(that).siblings('select').show();
        //                     $(that).closest('tr').data('isOkay', true);
        //                 }
        //             }
        //         });
        //     } else {
        //         //                    console.log("LOGS NOT REQUIRED");

        //         if (allowhalf === 1) {
        //             $(that).siblings('select').html("<option value='480' selected>Full Leave</option><option value='240'>Half Leave</option>");
        //         } else {
        //             $(that).siblings('select').html("<option value='480' selected>Full Leave</option>");
        //         }
        //         $(that).siblings('select').show();
        //         $(that).closest('tr').data('isOkay', true);
        //     }
        // }

    };
    var perScheduleSelector = function(callback) {
        $.when($('.scheduleSelector').each(function(i, obj) {
            schedulelogFilter(obj);
        })).then(function() {
            var isOkay = true;
            $('.scheduleSelector').each(function(i, obj) {
                if ($(obj).closest('tr').data('isOkay') === false) {
                    isOkay = false;
                }
            });
            callback(isOkay);
        })
    };
    var autoUpdatePaidStat = function(credit, keyid, callback) {
        var counter = 0;
        $("[name=fullhalfselector" + keyid + "]").each(function(x, obj) {
            var minutes = $(obj).val();
            var $tr = $(obj).closest('tr');
            var $tdpaid = $(obj).closest('td').next();
            var $tdreserve = $(obj).closest('td').next().next();
            minutes = minutes / 480;
            counter = counter + minutes;
            if (counter === credit) {
                $tr.data('paid', 1);
                $tdpaid.html('Paid');
                $tdreserve.html('');
            } else if (counter > credit) {
                $tr.data('paid', 0);
                $tdpaid.html('Unpaid');
                $tdreserve.html('<label class="m-checkbox"><input name="checkbox-backup' + keyid + '" type="checkbox"><span></span></label>');
            } else {
                $tr.data('paid', 1);
                $tdpaid.html('Paid');
                $tdreserve.html('');
            }
        });
        callback();
    };
    $(function() {

        $.when(fetchGetData('/Leave/request_init')).then(function(json) {
            json = JSON.parse(json.trim());
            if (json.status === 'Success' || json.status === 'Empty') {
                if (json.status === 'Success') {
                    $("#topNotification").hide();
                } else {
                    $("#topNotification-title").html("No available leave");
                    $("#topNotification-message").html("Please ask the human resources team to assign your account.");
                    $("#topNotification").addClass("alert-warning");
                    $("#topNotification").show();
                }
                $("#leave_names").html("");
                var string = "<option value=''>--</option>";
                $.each(json.leave_names, function(i, item) {
                    string += '<option value="' + item.leave_id + '">' + item.leave_name + '</option>';
                });
                $("#leave_names").html(string);
                $('.datepicker').datepicker({
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    },
                    multidate: true,
                    clearBtn: true,
                    format: 'yyyy-mm-dd',
                    multidateSeparator: ' , ',
                    maxViewMode: 0
                });
                $("#leave_names").change(function() {
                    $("#fileAlert").hide();
                    $("#leave_description").html("");
                    $("#leaveDateRange").datepicker('clearDates');
                    $("#leaveDatePicker").datepicker('clearDates');
                    if ($(this).val() !== '') {
                        $.when(fetchPostData({
                                leave_id: $(this).val()
                            }, '/Leave/get_leave_details'))
                            .then(function(res) {
                                res = JSON.parse(res.trim());
                                var leaveDetails = res.leave_details;
                                $("#leave_description").html(leaveDetails.description);
                                $("#leave_description").show();
                                var allowedStartDate = moment().subtract(parseInt(leaveDetails.allowedDateStart), 'days').format('MM/DD/YYYY');
                                var allowedEndDate = moment().add(parseInt(leaveDetails.allowedDateEnd), 'days').format('MM/DD/YYYY');
                                if (leaveDetails.preDays !== 0) {
                                    allowedStartDate = moment(allowedStartDate, 'MM/DD/YYYY').add(parseInt(leaveDetails.preDays), 'days').format('MM/DD/YYYY');
                                }
                                if (leaveDetails.isStatic === '1') {
                                    var betweenStart = parseInt(leaveDetails.betweenStart);
                                    var betweenEnd = parseInt(leaveDetails.betweenEnd);
                                    var staticDate = leaveDetails.staticDate;
                                    var staticMoment = moment(staticDate, 'YYYY-MM-DD');
                                    var newStatic = moment([moment().year(), staticMoment.month(), staticMoment.date()]);
                                    if (newStatic.isBefore(moment()) === false) {
                                        allowedStartDate = moment(newStatic).subtract(betweenStart, 'days').format('MM/DD/YYYY');
                                        allowedEndDate = moment(newStatic).add(betweenEnd, 'day').format('MM/DD/YYYY');
                                        //UNSURE COMMENT HERE --------------------------------------------------------------------
                                        // var preDaysStart = moment().add(parseInt(leaveDetails.preDays), 'days');
                                        // if (preDaysStart.isBefore(newStatic) === false) {
                                        //     allowedStartDate = preDaysStart.format('MM/DD/YYYY');
                                        // }
                                        //END OF UNSURE COMMENT HERE --------------------------------------------------------------------

                                        if (leaveDetails.pickerMode === 'range') {

                                            $('#leaveDatePicker').datepicker('clearDates');
                                            $('#leaveDateRange').datepicker("setDate", new Date(moment().year(), staticMoment.month(), staticMoment.date()));
                                        } else {
                                            $('#leaveDatePicker').datepicker('clearDates');
                                            $('#leaveDatePicker').datepicker("setDate", new Date(moment().year(), staticMoment.month(), staticMoment.date()));
                                        }
                                    } else {
                                                                                       console.log("PAST DATE");
                                        $("#fileAlert span").html("<strong>For Next Year!</strong> Available leave dates have already passed for this year.");
                                        $("#fileAlert").show();
                                        //=======================================ALLOW NEXT YEAR RULE=================================================
                                        newStatic = moment([moment().add(1, 'year').year(), staticMoment.month(), staticMoment.date()]); //change static date to next year
                                     
                                        allowedStartDate = moment(newStatic).subtract(betweenStart, 'days').format('MM/DD/YYYY');
                                        allowedEndDate = moment(newStatic).add(betweenEnd, 'days').format('MM/DD/YYYY');
                                        // var preDaysStart = moment().add(parseInt(leaveDetails.preDays), 'days');
                                        var preDaysStart = moment();//Change On: Dec 19, 2019 05:01AM
                                        if (preDaysStart.isBefore(newStatic) === false) {
                                            allowedStartDate = preDaysStart.format('MM/DD/YYYY');
                                        }
                                        if (leaveDetails.pickerMode === 'range') {

                                            $('#leaveDatePicker').datepicker('clearDates');
                                            $('#leaveDateRange').datepicker("setDate", new Date(moment().add(1, 'year').year(), staticMoment.month(), staticMoment.date()));
                                        } else {
                                            $('#leaveDatePicker').datepicker('clearDates');
                                            $('#leaveDatePicker').datepicker("setDate", new Date(moment().add(1, 'year').year(), staticMoment.month(), staticMoment.date()));
                                        }
                                        //=======================================ALLOW NEXT YEAR RULE=================================================
                                    }

                                }
                                var min = (leaveDetails.minimumDays === null) ? '1' : leaveDetails.minimumDays;
                                var max = (leaveDetails.maximumDays === null) ? '~' : leaveDetails.maximumDays;
                                $("#btnGenerate").data('min', min);
                                $("#btnGenerate").data('max', max);
                                $("#btnGenerate").data('pickermode', leaveDetails.pickerMode);
                                if (leaveDetails.pickerMode === 'range') {
                                    $('#leaveDatePicker').hide().data('active', 0);
                                    $('#leaveDateRange').show().data('active', 1);
                                    $("#leaveDateHelp").html("&nbsp;&nbsp;&nbsp; Type: Range &nbsp;&nbsp;&nbsp; Min: " + min + " &nbsp;&nbsp;&nbsp; Max: " + max);
                                    $("#leaveDateRange").datepicker('setStartDate', new Date(allowedStartDate));
                                    $("#leaveDateRange").datepicker('setEndDate', new Date(allowedEndDate));
                                } else {
                                    $('#leaveDatePicker').show().data('active', 1);
                                    $('#leaveDateRange').hide().data('active', 0);
                                    $("#leaveDateHelp").html("&nbsp;&nbsp;&nbsp; Type: Picker &nbsp;&nbsp;&nbsp; Min: " + min + " &nbsp;&nbsp;&nbsp; Max: " + max);
                                    $("#leaveDatePicker").datepicker('setStartDate', new Date(allowedStartDate));
                                    $("#leaveDatePicker").datepicker('setEndDate', new Date(allowedEndDate));
                                }
                                $("#leaveDatePicker").on('changeDate', function() {
                                    var date_list = $("#leaveDatePicker").datepicker('getDates');
                                    if (date_list.length === parseInt(leaveDetails.maximumDays)) {
                                        $("#leaveDatePicker *").prop('disabled', true);
                                    } else {
                                        $("#leaveDatePicker *").prop('disabled', false);
                                    }
                                });
                                $("#leaveDateRange .datepicker-days").on('click', 'td.day', function() {
                                    var startdate = moment($(this).data('date'));
                                    $("#leaveDateRange").datepicker('clearDates');
                                    var enddate = moment(startdate).add(parseInt(leaveDetails.maximumDays - 1), 'days');
                                    var daysInBetween = enumerateDaysBetweenDates(startdate, enddate);
                                    $("#leaveDateRange").datepicker('setDates', daysInBetween);
                                });
                            });
                    } else {
                        $("#leaveDateHelp").html("");
                        $('#leaveDatePicker').hide();
                        $('#leaveDateRange').hide();
                    }
                });
                $("#fileLeaveModal").on('show.bs.modal', function() {
                    $("#leave_description").html("Please select leave name.");
                    $("#leaveDateRange").datepicker('clearDates');
                    $("#leaveDatePicker").datepicker('clearDates');
                    $("#leave_names").val("");
                    $("#reason").val("");
                    $("#div-filing").show();
                    $("#div-preview").hide();
                });
                $("#btnGenerate").click(function() {
                    $("#fileAlert").hide();
                    //-------------filter first minimum------------
                    var min = $(this).data('min');
                    var max = $(this).data('max');
                    var pickermode = $(this).data('pickermode');
                    var leave_id = $("#leave_names").val();
                    var reason = $("#reason").val();
                    if ($("#leaveDatePicker").data('active') === 1) {
                        var date_list = $("#leaveDatePicker").datepicker('getDates');
                    } else {
                        var date_list = $("#leaveDateRange").datepicker('getDates');
                        if (date_list.length < max) {
                            var rangeStart = moment(date_list[0]);
                            var rangeEnd = moment(date_list[0]).add(parseInt(max) - 1, 'days');
                            date_list = enumerateDaysBetweenDates(rangeStart, rangeEnd);
                            for (var x = 0; x < date_list.length; x++) {
                                date_list[x] = new Date(date_list[x]);
                            }
                        }
                    }
                    if (date_list.length < parseFloat(min) || date_list.length > parseFloat(max)) {
                        $("#fileAlert  span").html("<strong>Minimum and Maximum Dates!</strong> Please select dates between the displayed min and max");
                        $("#fileAlert").show();
                    } else {

                        if (date_list.length === 0 || $.trim(reason) === '' || leave_id === '') {
                            $("#fileAlert  span").html("<strong>Incomplete!</strong> Please complete the form to continue");
                            $("#fileAlert").show();
                        } else {
                            var sorted = date_list.sort(function(date1, date2) {
                                return date1.getTime() - date2.getTime();
                            });
                            for (var x = 0; x < sorted.length; x++) {
                                sorted[x] = moment(sorted[x]).format('YYYY-MM-DD');
                            }
                            $("#div-preview-display").html("");
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>Leave/get_preview_leave_details",
                                data: {
                                    leave_id: leave_id,
                                    date_list: date_list
                                },
                                cache: false,
                                success: function(result) {
                                    result = JSON.parse(result.trim()); //array
                                    var defaultPayment = result.defaultPayment;
                                    if (result.status === 'Success') {
                                        var res = result.data;
                                        $("#btnSubmitApplication").data('data', res);
                                        if (res.allowHalfDay === '1') {
                                            var selectOptions = '<option value=\'full\' selected>Full Leave</option><option value=\'half\'>Half Leave</option>';
                                        } else {
                                            var selectOptions = '<option value=\'full\' selected>Full Leave</option>';
                                        }
                                        if (res.refreshOccur === 'quarter' || res.refreshOccur === 'semiannual') {

                                            $.each(res.final, function(i, obj) { //history and year
                                                obj = JSON.parse(obj);
                                                $.each(obj.history.reverse(), function(xx, xxhistory) {
                                                    var str = "<table class='table table-bordered table-sm m-table'>";
                                                    str += "<thead class='thead-inverse'>";
                                                    str += "<th style='width:120px'>" + obj.year + "</th>";
                                                    str += "<th colspan=2>" + xxhistory.pos_details + "</th>";
                                                    str += "<th style='width:70px'>" + xxhistory.status + "</th>";
                                                    str += "<th style='width:90px'>Credit</th>";
                                                    //uncomment-by nicca
                                                    // if (res.allowBackup === '1') {
                                                    //     str += "<th style='width:40px'><small>Reserve</small></th>";
                                                    // }

                                                    str += "</thead>";
                                                    str += "<tbody>";
                                                    $.each(xxhistory.details, function(key, item) {
                                                        str += "<tr class='sub-header' data-credit='" + item.credit + "'>";
                                                        str += "<td>" + key + "</td>";
                                                        str += "<td colspan='3'</td>"; 
                                                        if(item.credit===0){
                                                            str += "<td style='font-size:16px' class='m-animate-blink'>" + item.credit + "</td>";
                                                        }else{
                                                            str += "<td style='font-size:16px'>" + item.credit + "</td>";//updated 12/24/2019 1:25AM
                                                        }
                                                        
                                                         //uncomment-by nicca
                                                        if (res.allowBackup === '1') {
                                                            var least = Math.min(item.backupCredits, res.backupMax);
                                                            least = (least < 0) ? 0 : least;
                                                            str += "<td>" + least + "</td>";
                                                        }
                                                        str += "</tr>";
                                                        var cnt = 1;
                                                        $.each(item.dates, function(x, date) {
                                                            if (item.schedulelogs[x] === 'null') {
                                                                var schedulelog = [];
                                                            } else {
                                                                var schedulelog = JSON.parse($.trim(item.schedulelogs[x]));
                                                            }
                                                            var colorgreen = 'green';
                                                            var colorred = 'red';
                                                            var paidcolor = colorgreen;
                                                            if (defaultPayment === 'paid') {
                                                                var paidStatus = 'Paid';
                                                                var paid = 1;
                                                            } else if (defaultPayment === 'unpaid') {
                                                                var paidStatus = 'Unpaid';
                                                                var paid = 0;
                                                                paidcolor=colorred;
                                                            } else {
                                                                if (cnt <= parseFloat(item.credit)) {
                                                                    var paidStatus = 'Paid';
                                                                    var paid = 1;
                                                                } else {
                                                                    var paidStatus = 'Unpaid';
                                                                    var paid = 0;
                                                                paidcolor=colorred;
                                                                }
                                                            }

                                                            cnt++;
                                                            str += "<tr class='final-data' data-date='" + date + "' data-paid='" + paid + "'>";
                                                            str += "<td>" + moment(date, 'YYYY-MM-DD').format('MMM DD, YYYY') + "</td>";
                                                            if (schedulelog.length === 0) {
                                                                str += "<td class='m--font-brand'>No Existing Schedule</td><td>-</td>";
                                                            } else {
                                                                var last = schedulelog.slice(-1)[0];
                                                                var myschedule = "";
                                                                var mylogs = "";
                                                                var len = schedulelog.length;
                                                                var counter = 0;
                                                                $.each(schedulelog, function(schedkey, schedlog) {
                                                                    if (len > 1) {
                                                                        counter++;
                                                                    } else {
                                                                        counter = "";
                                                                    }
                                                                    var clock_in = (schedlog.clock_in === "") ? "" : "IN: " + moment(schedlog.clock_in).format("MMM DD, YYYY hh:mm A");
                                                                    var clock_out = (schedlog.clock_out === "") ? "" : "OUT: " + moment(schedlog.clock_out).format("MMM DD, YYYY hh:mm A");
                                                                    var logs = (clock_in === '') ? "" : '<span class="m--font-boldest">LOGS: </span><br>' + clock_in + ' <br>' + clock_out;
                                                                    var schedtime = (schedlog.time_start === null || schedlog.time_end === null) ? "" : schedlog.time_start + ' - ' + schedlog.time_end;
                                                                    var schedLeaveType = (schedlog.leaveType === null) ? "" : schedlog.leaveType + " Leave";
                                                                    var schedLeaveType = (schedlog.schedtype_id === '3' || schedlog.schedtype_id === '8') ? schedLeaveType : "";
                                                                    if (logs === "") {
                                                                        myschedule += '<p class="text-center">';
                                                                        myschedule += '<span class="m--font-boldest">EXISTING SCHEDULE ' + counter + ': </span><br> <span class="m-badge m-badge--wide m-badge--rounded" style="background-color: ' + schedlog.style + ';color: white;">' + schedlog.type + '</span> <br> <span class="m--font-bold">' + schedtime + '</span><span class="m--font-bold">' + schedLeaveType + '</span>';
                                                                        myschedule += '</p>';
                                                                        mylogs += "<p>-</p>";
                                                                    } else {
                                                                        myschedule += '<p class="text-center">';
                                                                        myschedule += '<span class="m--font-boldest">EXISTING SCHEDULE ' + counter + ': </span><br> <span class="m-badge m-badge--wide m-badge--rounded" style="background-color: ' + schedlog.style + ';color: white;">' + schedlog.type + '</span> <br> <span class="m--font-bold">' + schedtime + '</span><span class="m--font-bold">' + schedLeaveType + '</span>';
                                                                        myschedule += '</p>';
                                                                        mylogs += '<p class="m--font-bold">' + logs + '</p>';
                                                                        if (last.sched_id !== schedlog.sched_id) {
                                                                            myschedule += "<hr style='margin:2px'/>";
                                                                            mylogs += "<hr style='margin:2px'/>";
                                                                        }
                                                                    }

                                                                });
                                                                str += "<td style='font-size:12px;'>" + myschedule + "</td>";
                                                                str += "<td style='font-size:12px;width:170px'>" + mylogs + "</td>";
                                                            }
                                                            var counter = 0;

                                                            var selectOptionsSched = "";
                                                            if (len > 0) {
                                                                $.each(schedulelog, function(schedkey, schedlog) {
                                                                    counter++;
                                                                    if (counter === 1) {
                                                                        selectOptionsSched += "<option value='" + schedlog.sched_id + "' selected data-clockin='" + schedlog.clock_in + "' data-clockout='" + schedlog.clock_out + "' data-schedtypeid='" + schedlog.schedtype_id + "'>Schedule " + counter + "</option>";
                                                                    } else {
                                                                        selectOptionsSched += "<option value='" + schedlog.sched_id + "' data-clockin='" + schedlog.clock_in + "' data-clockout='" + schedlog.clock_out + "'  data-schedtypeid='" + schedlog.schedtype_id + "'>Schedule " + counter + "</option>";
                                                                    }
                                                                });
                                                            } else {
                                                                selectOptionsSched = "<option value='' selected></option>";
                                                            }

                                                            str += "<td><select class='form-control-sm m-input' name='fullhalfselector" + obj.year + "" + key + "' style='display:none'></select>";
                                                            str += '<h5 class="m--font-danger" data-toggle="m-tooltip" title="" data-original-title="Tooltip title" style="display:none"><i class="fa fa-info"></i></h5>';
                                                            str += (len > 1) ? " <br><hr><select class='form-control-sm m-input scheduleSelector multipleSchedule' id='scheduleSelector" + date + "'   data-allowhalf='" + res.allowHalfDay + "'  data-date='" + date + "'>" + selectOptionsSched + "</select>" : "<select class='form-control-sm m-input scheduleSelector'  id='scheduleSelector" + date + "'  data-allowhalf='" + res.allowHalfDay + "'  data-date='" + date + "' style='display:none'>" + selectOptionsSched + "</select>";
                                                            str += "</td>";
                                                            str += "<td class='paidstatus m--font-bolder' style='color:"+paidcolor+"'>" + paidStatus + "</td>";
                                                            //uncomment-by nicca
                                                            // if (res.allowBackup === '1') {
                                                            //     if (paid === 0) {
                                                            //         str += '<td><label class="m-checkbox checkbox-backup"><input  name="checkbox-backup' + obj.year + "" + key + '" type="checkbox"><span></span></label></td>';
                                                            //     } else {
                                                            //         str += "<td></td>";
                                                            //     }
                                                            // }
                                                            str += "</tr>";
                                                        });
                                                    });
                                                    str += "</tbody>";
                                                    str += "</table></div>";
                                                    $("#div-preview-display").append(str);
                                                    mApp.block('#fileLeaveModal .modal-content', {
                                                        overlayColor: '#000000',
                                                        type: 'loader',
                                                        state: 'brand',
                                                        size: 'lg',
                                                        message: 'Checking schedule and logs...'
                                                    });
                                                    perScheduleSelector(function(isOkay) {
                                                        if (isOkay === true) {
                                                            $("#btnSubmitApplication").prop('disabled', false);
                                                        } else {
                                                            $("#btnSubmitApplication").prop('disabled', true);
                                                        }

                                                        mApp.unblock('#fileLeaveModal .modal-content');
                                                    });

                                                    $('.multipleSchedule').each(function(i, obj) {
                                                        $("#div-preview-display").on('change', "#" + $(this).attr('id'), function() {
                                                            var that = this;
                                                            mApp.block('#fileLeaveModal .modal-content', {
                                                                overlayColor: '#000000',
                                                                type: 'loader',
                                                                state: 'brand',
                                                                size: 'lg',
                                                                message: 'Checking schedule and logs...'
                                                            });
                                                            //REPLACED AJAXSTOP FUNCTION KAI ERROR
                                                            $.when(schedulelogFilter(that)).then(function() {
                                                                var isOkay = true;
                                                                $('.scheduleSelector').each(function(i, obj) {
                                                                    if ($(obj).closest('tr').data('isOkay') === false) {
                                                                        isOkay = false;
                                                                    }
                                                                });
                                                                if (isOkay === true) {
                                                                    $("#btnSubmitApplication").prop('disabled', false);
                                                                } else {
                                                                    $("#btnSubmitApplication").prop('disabled', true);
                                                                }
                                                                mApp.unblock('#fileLeaveModal .modal-content');
                                                            });

                                                        });
                                                    });
                                                    $.each(xxhistory.details, function(key, item) {
                                                        var idx = obj.year + "" + key;
                                                        $("#div-preview-display").on('change', "[name=checkbox-backup" + idx + "]", function() {
                                                            var counter = 0;
                                                            var remaining = item.backupCredits;
                                                            var max = res.backupMax;
                                                            var touse = Math.min(remaining, max);
                                                            $("[name=checkbox-backup" + idx + "]:checked").each(function() {
                                                                var value = parseFloat($(this).closest('td').prev().prev().find('select[name^=fullhalfselector]').val());
                                                                counter = counter + value;
                                                            });
                                                            counter = counter / 480;
                                                            if (counter === touse) {
                                                                $("[name=checkbox-backup" + idx + "]:not(:checked)").prop('disabled', true);
                                                            } else if (counter > touse) {
                                                                $(this).prop('checked', false);
                                                                $("[name=checkbox-backup" + idx + "]:not(:checked)").prop('disabled', true);
                                                            } else {
                                                                $("[name=checkbox-backup" + idx + "]:not(:checked)").prop('disabled', false);
                                                            }

                                                        });
                                                        $("#div-preview-display").on('change', "[name=fullhalfselector" + idx + "]", function() {
                                                            var that = this;
                                                            var credit = $(that).closest('tr').prevAll("tr.sub-header").data('credit');
                                                            //-----------------------------------------FOR AUTO UPDATE PAID STATUS----------------------------------------------
                                                            autoUpdatePaidStat(credit, idx, function() {
                                                                var tr = $(that).closest('tr');
                                                                var type = $(that).val();
                                                                if (type === 'half') {
                                                                    var value = 240;
                                                                } else {
                                                                    var value = 480;
                                                                }
                                                                tr.find('input[type=checkbox]').val(value);
                                                                //-------------------------------------------------------------
                                                                var counter = 0;
                                                                var remaining = item.backupCredits;
                                                                var max = res.backupMax;
                                                                var touse = Math.min(remaining, max);
                                                                $("[name=checkbox-backup" + idx + "]:checked").each(function() {
                                                                    var value = parseFloat($(that).val());
                                                                    counter = counter + value;
                                                                });
                                                                counter = counter / 480;
                                                                if (counter === touse) {
                                                                    $("[name=checkbox-backup" + idx + "]").prop('disabled', true);
                                                                } else if (counter > touse) {
                                                                    $(that).prop('checked', false);
                                                                    $("[name=checkbox-backup" + idx + "]").prop('disabled', true);
                                                                } else {
                                                                    $("[name=checkbox-backup" + idx + "]").prop('disabled', false);
                                                                }
                                                                $("[name=checkbox-backup" + idx + "]").prop('checked', false);
                                                            });

                                                        });
                                                    });
                                                });
                                                $("[name^=checkbox-backup]").change();
                                            });
                                        } else {

                                            $.each(res.final, function(i, obj) { //history and year
                                                obj = JSON.parse(obj);
                                                $.each(obj.history.reverse(), function(xx, xxhistory) {

                                                    var str = "<table class='table table-bordered table-sm m-table'>";
                                                    str += "<thead class='thead-inverse'>";
                                                    str += "<th style='width:130px'>" + obj.year + "</th>";
                                                    str += "<th colspan=2>" + xxhistory.pos_details + "</th>";
                                                    str += "<th style='width:70px'>" + xxhistory.status + "</th>";
                                                    str += "<th style='width:90px'>Credit</th>";
                                                     //uncomment-by nicca
                                                    // if (res.allowBackup === '1') {
                                                    //     str += "<th style='width:40px'><small>Reserve</small></th>";
                                                    // }
                                                    str += "</thead>";
                                                    str += "<tbody>";
                                                    var item = xxhistory.details;
                                                    str += "<tr class='sub-header' data-credit='" + item.credit + "'>";
                                                    str += "<td colspan='4'></td>";
                                                    if(item.credit===0){
                                                        str += "<td style='font-size:16px' class='m-animate-blink'>" + item.credit + "</td>";
                                                    }else{
                                                        str += "<td style='font-size:16px'>" + item.credit + "</td>";
                                                    }
                                                    
                                                     //uncomment-by nicca
                                                    // if (res.allowBackup === '1') {
                                                    //     var least = Math.min(item.backupCredits, res.backupMax);
                                                    //     least = (least < 0) ? 0 : least;
                                                    //     str += "<td>" + least + "</td>";
                                                    // }
                                                    str += "</tr>";
                                                    var cnt = 1;

                                                    $.each(item.dates, function(x, date) {
                                                        if (item.schedulelogs[x] === 'null') {
                                                            var schedulelog = [];
                                                        } else {
                                                            var schedulelog = JSON.parse($.trim(item.schedulelogs[x]));
                                                        }
                                                        var colorgreen = 'green';
                                                            var colorred = 'red';
                                                            var paidcolor = colorgreen;
                                                        if (defaultPayment === 'paid') {
                                                            var paidStatus = 'Paid';
                                                            var paid = 1;
                                                        } else if (defaultPayment === 'unpaid') {
                                                            var paidStatus = 'Unpaid';
                                                            var paid = 0;
                                                            paidcolor = colorred;
                                                        } else {
                                                            if (cnt <= parseFloat(item.credit)) {
                                                                var paidStatus = 'Paid';
                                                                var paid = 1;
                                                            } else {
                                                                var paidStatus = 'Unpaid';
                                                                var paid = 0;
                                                            paidcolor = colorred;
                                                            }
                                                        }
                                                        cnt++;
                                                        str += "<tr class='final-data' data-date='" + date + "' data-paid='" + paid + "'>";
                                                        str += "<td>" + moment(date, 'YYYY-MM-DD').format('MMM DD, YYYY') + "</td>";
                                                        var len = schedulelog.length;
                                                        if (schedulelog.length === 0) {
                                                            str += "<td class='m--font-brand'>No Existing Schedule</td><td>-</td>";
                                                        } else {
                                                            var last = schedulelog.slice(-1)[0];
                                                            var myschedule = "";
                                                            var mylogs = "";
                                                            var counter = 0;
                                                            $.each(schedulelog, function(schedkey, schedlog) {

                                                                if (len > 1) {
                                                                    counter++;
                                                                } else {
                                                                    counter = "";
                                                                }
                                                                var clock_in = (schedlog.clock_in === "") ? "" : "IN: " + moment(schedlog.clock_in).format("MMM DD, YYYY hh:mm A");
                                                                var clock_out = (schedlog.clock_out === "") ? "" : "OUT: " + moment(schedlog.clock_out).format("MMM DD, YYYY hh:mm A");
                                                                var logs = (clock_in === '') ? "" : '<span class="m--font-boldest">LOGS: </span><br>' + clock_in + ' <br>' + clock_out;
                                                                var schedtime = (schedlog.time_start === null || schedlog.time_end === null) ? "" : schedlog.time_start + ' - ' + schedlog.time_end;
                                                                var schedLeaveType = (schedlog.leaveType === null) ? "" : schedlog.leaveType + " Leave";
                                                                var schedLeaveType = (schedlog.schedtype_id === '3' || schedlog.schedtype_id === '8') ? schedLeaveType : "";
                                                                if (logs === "") {
                                                                    myschedule += '<p class="text-center">';
                                                                    myschedule += '<span class="m--font-boldest">EXISTING SCHEDULE ' + counter + ': </span><br> <span class="m-badge m-badge--wide m-badge--rounded" style="background-color: ' + schedlog.style + ';color: white;">' + schedlog.type + '</span> <br> <span class="m--font-bold">' + schedtime + '</span><span class="m--font-bold">' + schedLeaveType + '</span>';
                                                                    myschedule += '</p>';
                                                                    mylogs += "<p>-</p>";
                                                                } else {
                                                                    myschedule += '<p class="text-center">';
                                                                    myschedule += '<span class="m--font-boldest">EXISTING SCHEDULE ' + counter + ': </span><br> <span class="m-badge m-badge--wide m-badge--rounded" style="background-color: ' + schedlog.style + ';color: white;">' + schedlog.type + '</span> <br> <span class="m--font-bold">' + schedtime + '</span><span class="m--font-bold">' + schedLeaveType + '</span>';
                                                                    myschedule += '</p>';
                                                                    mylogs += '<p class="m--font-bold">' + logs + '</p>';
                                                                    if (last.sched_id !== schedlog.sched_id) {
                                                                        myschedule += "<hr style='margin:2px'/>";
                                                                        mylogs += "<hr style='margin:2px'/>";
                                                                    }
                                                                }

                                                            });
                                                            str += "<td style='font-size:12px;'>" + myschedule + "</td>";
                                                            str += "<td style='font-size:12px;width:170px'>" + mylogs + "</td>";
                                                        }
                                                        var counter = 0;
                                                        var selectOptionsSched = "";
                                                        if (len > 0) {
                                                            $.each(schedulelog, function(schedkey, schedlog) {
                                                                counter++;
                                                                if (counter === 1) {
                                                                    selectOptionsSched += "<option value='" + schedlog.sched_id + "' selected data-clockin='" + schedlog.clock_in + "' data-clockout='" + schedlog.clock_out + "' data-schedtypeid='" + schedlog.schedtype_id + "'>Schedule " + counter + "</option>";
                                                                } else {
                                                                    selectOptionsSched += "<option value='" + schedlog.sched_id + "' data-clockin='" + schedlog.clock_in + "' data-clockout='" + schedlog.clock_out + "'  data-schedtypeid='" + schedlog.schedtype_id + "'>Schedule " + counter + "</option>";
                                                                }
                                                            });
                                                        } else {
                                                            selectOptionsSched = "<option value='' selected></option>";
                                                        }

                                                        str += "<td><select class='form-control-sm m-input' name='fullhalfselector" + obj.year + "' style='display:none'></select>";
                                                        str += '<h5 class="m--font-danger" data-toggle="m-tooltip" title="" data-original-title="Tooltip title" style="display:none"><i class="fa fa-info"></i></h5>';
                                                        str += (len > 1) ? " <br><hr><select class='form-control-sm m-input scheduleSelector multipleSchedule' id='scheduleSelector" + date + "'   data-allowhalf='" + res.allowHalfDay + "'  data-date='" + date + "'>" + selectOptionsSched + "</select>" : "<select class='form-control-sm m-input scheduleSelector'  id='scheduleSelector" + date + "'  data-allowhalf='" + res.allowHalfDay + "'  data-date='" + date + "' style='display:none'>" + selectOptionsSched + "</select>";
                                                        str += "</td>";
                                                        str += "<td class='paidstatus m--font-bolder' style='color:"+paidcolor+"'>" + paidStatus + "</td>";
                                                         //uncomment-by nicca
                                                        // if (res.allowBackup === '1') {
                                                        //     if (paid === 0) {
                                                        //         str += '<td><label class="m-checkbox"><input name="checkbox-backup' + obj.year + '" type="checkbox"><span></span></label></td>';
                                                        //     } else {
                                                        //         str += "<td></td>";
                                                        //     }
                                                        // }
                                                        str += "</tr>";
                                                    });
                                                    str += "</tbody>";
                                                    str += "</table></div>";
                                                    $("#div-preview-display").append(str);

                                                    mApp.block('#fileLeaveModal .modal-content', {
                                                        overlayColor: '#000000',
                                                        type: 'loader',
                                                        state: 'brand',
                                                        size: 'lg',
                                                        message: 'Checking schedule and logs...'
                                                    });
                                                    perScheduleSelector(function(isOkay) {
                                                        if (isOkay === true) {
                                                            $("#btnSubmitApplication").prop('disabled', false);
                                                        } else {
                                                            $("#btnSubmitApplication").prop('disabled', true);
                                                        }

                                                        mApp.unblock('#fileLeaveModal .modal-content');
                                                    });

                                                    $('.multipleSchedule').each(function(i, obj) {
                                                        $("#div-preview-display").on('change', "#" + $(this).attr('id'), function() {
                                                            var that = this;
                                                            mApp.block('#fileLeaveModal .modal-content', {
                                                                overlayColor: '#000000',
                                                                type: 'loader',
                                                                state: 'brand',
                                                                size: 'lg',
                                                                message: 'Checking schedule and logs...'
                                                            });
                                                            //REPLACE THE AJAXSTOP FUNCTION KAY ERROR
                                                            $.when(schedulelogFilter(that)).then(function() {
                                                                var isOkay = true;
                                                                $('.scheduleSelector').each(function(i, obj) {
                                                                    if ($(obj).closest('tr').data('isOkay') === false) {
                                                                        isOkay = false;
                                                                    }
                                                                });
                                                                if (isOkay === true) {
                                                                    $("#btnSubmitApplication").prop('disabled', false);
                                                                } else {
                                                                    $("#btnSubmitApplication").prop('disabled', true);
                                                                }
                                                                mApp.unblock('#fileLeaveModal .modal-content');
                                                            });

                                                        });
                                                    });
                                                    $("#div-preview-display").on('change', "[name=checkbox-backup" + obj.year + "]", function() {
                                                        var counter = 0;
                                                        var remaining = item.backupCredits;
                                                        var max = res.backupMax;
                                                        var touse = Math.min(remaining, max);
                                                        $("[name=checkbox-backup" + obj.year + "]:checked").each(function() {
                                                            var value = parseFloat($(this).closest('td').prev().prev().find('select[name^=fullhalfselector]').val());
                                                            counter = counter + value;
                                                        });
                                                        counter = counter / 480;
                                                        if (counter === touse) {
                                                            $("[name=checkbox-backup" + obj.year + "]:not(:checked)").prop('disabled', true);
                                                        } else if (counter > touse) {
                                                            $(this).prop('checked', false);
                                                            $("[name=checkbox-backup" + obj.year + "]:not(:checked)").prop('disabled', true);
                                                        } else {
                                                            $("[name=checkbox-backup" + obj.year + "]:not(:checked)").prop('disabled', false);
                                                        }
                                                    });
                                                    $("#div-preview-display").on('change', "[name=fullhalfselector" + obj.year + "]", function() {
                                                        var that = this;
                                                        var credit = $(that).closest('tr').prevAll("tr.sub-header").data('credit');
                                                        //-----------------------------------------FOR AUTO UPDATE PAID STATUS----------------------------------------------
                                                        autoUpdatePaidStat(credit, obj.year, function() {
                                                            var tr = $(that).closest('tr');
                                                            var type = $(that).val();
                                                            if (type === 'half') {
                                                                var value = 240;
                                                            } else {
                                                                var value = 480;
                                                            }
                                                            tr.find('input[type=checkbox]').val(value);
                                                            //------------------------------------------------
                                                            var counter = 0;
                                                            var remaining = item.backupCredits;
                                                            var max = res.backupMax;
                                                            var touse = Math.min(remaining, max);
                                                            $("[name=checkbox-backup" + obj.year + "]:checked").each(function() {
                                                                var value = parseFloat($(that).val());
                                                                counter = counter + value;
                                                            });
                                                            counter = counter / 480;
                                                            if (counter === touse) {
                                                                $("[name=checkbox-backup" + obj.year + "]").prop('disabled', true);
                                                            } else if (counter > touse) {
                                                                $(that).prop('checked', false);
                                                                $("[name=checkbox-backup" + obj.year + "]").prop('disabled', true);
                                                            } else {
                                                                $("[name=checkbox-backup" + obj.year + "]").prop('disabled', false);
                                                            }
                                                            $("[name=checkbox-backup" + obj.year + "]").prop('checked', false);
                                                        });
                                                    });
                                                });

                                                $("[name^=checkbox-backup]").change();
                                            });
                                        }

                                        $("#div-filing").hide();
                                        $("#div-preview").show(500);
                                    } else {
                                        //                                            swal("No Leave Credit found", "Ask HR to assign leave credits to your position.", "error");
                                        $("#fileAlert  span").html("<strong>No Leave Credit!</strong> Ask HR for assistance.");
                                        $("#fileAlert").show();
                                        $("#div-filing").show();
                                        $("#div-preview").hide();
                                    }

                                }
                            });
                        }
                    }
                });
                $("#btnSubmitApplication").click(function() {
                    swal({
                        title: 'Are you sure ?',
                        text: "This request will be submitted to your supervisor!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes, continue',
                        confirmButtonClass: "btn btn-brand m-btn m-btn--air"
                    }).then(function(result) {
                        if (result.value) {
                            saveNormalFiling();
                        }
                    });
                });
                $("#btnBack").click(function() {
                    $("#div-filing").show(500);
                    $("#div-preview").hide();
                });
            } else {
                var status = json.status;
                var missing = status.missing;
                var emppromoteMissing = status.emppromoteMissing;
                if (missing !== '') {
                    $("#topNotification-title").html("Missing Information!");
                    $("#topNotification-message").html("The following are missing from your personal information : <b>" + missing + "</b>.");
                    $("#topNotification").addClass("alert-danger");
                    $("#topNotification").show();
                }
                if (emppromoteMissing !== '') {
                    $("#topNotification-title-promote").html("Missing Start Dates!");
                    $("#topNotification-message-promote").html("The following positions have missing start dates : <b>" + emppromoteMissing + "</b>.");
                    $("#topNotification-promote").addClass("alert-danger");
                    $("#topNotification-promote").show();
                }

                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#ff0000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            }

        });
        //OTHER CODES --------------------------------------------------



        $('a[href="#tab-ongoing"]').on('show.bs.tab', function(event) {
            $("#div-accordion-history").html("");
            //ONGOING PAGINATION-------------------------------------------------
            $("#request-ongoing-perpage").change(function() {
                var perPage = $("#request-ongoing-perpage").val();
                getOngoingRequest(0, perPage, function(total) {
                    $("#request-ongoing-pagination").pagination('destroy');
                    $("#request-ongoing-pagination").pagination({
                        items: total, //default
                        itemsOnPage: $("#request-ongoing-perpage").val(),
                        hrefTextPrefix: "#",
                        cssStyle: 'light-theme',
                        onPageClick: function(pagenumber) {
                            var perPage = $("#request-ongoing-perpage").val();
                            getOngoingRequest((pagenumber * perPage) - perPage, perPage, function() {});
                        }
                    });
                });
            });
            $("#request-ongoing-perpage").change();
            //END OF ONGOING PAGINATION --------------------------------------
        });
        $('a[href="#tab-history"]').on('show.bs.tab', function(event) {
            $("#div-accordion-ongoing").html("");
            $('#request-history-daterange .form-control').val(moment().startOf('year').format('MM/DD/YYYY') + ' - ' + moment().endOf('year').format('MM/DD/YYYY'));
            $('#request-history-daterange').daterangepicker({
                    startDate: moment().startOf('year'),
                    endDate: moment().endOf('year'),
                    opens: 'center',
                    drops: 'down',
                    autoUpdateInput: true,
                    ranges: {
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                        'This Year': [moment().startOf('year'), moment().endOf('year')]
                    }
                },
                function(start, end, label) {
                    $('#request-history-daterange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                    $("#request-history-perpage").change();
                });
            //HISTORY PAGINATION-----------------------------------------------------

            $("#request-history-perpage").change(function() {
                var perPage = $("#request-history-perpage").val();
                getHistoryRequest(0, perPage, function(total) {
                    $("#request-history-pagination").pagination('destroy');
                    $("#request-history-pagination").pagination({
                        items: total, //default
                        itemsOnPage: $("#request-history-perpage").val(),
                        hrefTextPrefix: "#",
                        cssStyle: 'light-theme',
                        onPageClick: function(pagenumber) {
                            var perPage = $("#request-history-perpage").val();
                            getHistoryRequest((pagenumber * perPage) - perPage, perPage, function() {});
                        }
                    });
                });
            });
            $("#search-custom-history-request").keyup(function() {
                $("#request-history-perpage").change();
            });
            $("#request-history-perpage").change();
            //END OF HISTORY PAGINATION----------------------------------------------
        });
        $(".tab-content").on('show.bs.collapse', ".m-accordion__item-body", function() {
            var that = this;
            var requestLeave_ID = $(this).data('requestleaveid');
            var accordionType = $(this).data('type');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_request_details",
                data: {
                    requestLeave_ID: requestLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    var string = '<div class="m-accordion__item-content" >';
                    string += '<div>';
                    // string += '<div class="row"><div class="col-md-6"><h4>REQUEST</h4></div><div class="col-md-6"><div class="pull-right"> <a href="#" class="rangeDeletebtn btn btn-danger btn-sm m-btn m-btn--icon m-btn--outline-2x"  onclick="deleteRequest(' + requestLeave_ID + ')" title="Delete Request"><span><i class="fa  fa-trash"></i><span class="button-text">Delete Request</span></span></a><a href="#" class="btn btn-brand btn-sm m-btn m-btn--icon m-btn--outline-2x"  data-requestleaveid="' + requestLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal" title="View Approval"><span><i class="fa  flaticon-list-2"></i><span class="button-text">View Approval</span></span></a></div></div></div> <br />';
                    string += (accordionType === 'ongoing') ? '<div class="row"><div class="col-md-6"><h4>REQUEST</h4></div><div class="col-md-6"><div class="pull-right"> <a href="#" class="rangeDeletebtn btn btn-danger btn-sm m-btn m-btn--icon m-btn--outline-2x"  onclick="deleteRequest(' + requestLeave_ID + ')" title="Delete Request"><span><i class="fa  fa-trash"></i><span class="button-text">Delete Request</span></span></a><a href="#" class="btn btn-brand btn-sm m-btn m-btn--icon m-btn--outline-2x"  data-requestleaveid="' + requestLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal" title="View Approval"><span><i class="fa  flaticon-list-2"></i><span class="button-text">View Approval</span></span></a></div></div></div> <br />' : '<div class="row"><div class="col-md-6"><h4>REQUEST</h4></div><div class="col-md-6"><div class="pull-right"> <a href="#" class="btn btn-brand btn-sm m-btn m-btn--icon m-btn--outline-2x"  data-requestleaveid="' + requestLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal" title="View Approval"><span><i class="fa  flaticon-list-2"></i><span class="button-text">View Approval</span></span></a></div></div></div> <br />';
                    string += '<div style="max-height:250px;overflow:auto"><table class="table table-sm m-table table-striped table-bordered table-hover" style="">';
                    string += '<thead>';
                    string += '<tr>';
                    string += '<th>Date</th>';
                    string += '<th>Leave Description</th>';
                    string += '<th>Type</th>';
                    string += '<th>Note</th>';
                    string += '<th>Final Status</th>';
                    string += '<th>Actions</th>';
                    string += '</tr></thead><tbody>';
                    $.each(res.data, function(i, item) {
                        var minutes = (item.minutes === "480") ? "Full" : "Half";
                        var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                        string += "<tr>";
                        string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                        string += "<td>" + item.leave_name + "</td>";
                        string += "<td>" + item.leaveType + " Leave</td>";
                        string += "<td>" + minutes + " Day (" + isPaid + ")</td>";
                        string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.description + '</span></td>';
                        var viewdetails = '<td> <a href="#" class="m-link m-link--state m-link--primary" data-toggle="modal" data-target="#date-modal" data-requestleaveid="' + requestLeave_ID + '" data-requestleavedetailsid="' + item.requestLeaveDetails_ID + '"><i class="fa fa-eye"></i> View Details</a></td>';
                        var viewdetailsWithDelete = '<td><a href="#" title="Delete Leave Date" class="btn btn-sm btn-outline-danger m-btn m-btn--icon m-btn--icon-only" onclick="deleteRequestDetail(' + item.requestLeaveDetails_ID + ',this)"><i class="la la-trash"></i></a> <a href="#" title="View Details" class="btn btn-sm btn-outline-brand m-btn m-btn--icon m-btn--icon-only" data-toggle="modal" data-target="#date-modal" data-requestleaveid="' + requestLeave_ID + '" data-requestleavedetailsid="' + item.requestLeaveDetails_ID + '"><i class="fa fa-eye"></i></a></td>';
                        if (item.pickerMode === 'range') {
                            string += (accordionType === 'ongoing') ? viewdetails : viewdetails;
                        } else {
                            if (res.approvalCount === '0') { //completed
                                string += (accordionType === 'ongoing') ? viewdetails : viewdetails;
                            } else { //pending or current

                                string += (accordionType === 'ongoing') ? viewdetailsWithDelete : viewdetails;
                            }
                        }
                        string += "</tr>";
                    });
                    string += "</tbody></table></div></div>";
                    $("#accordion-item-" + requestLeave_ID).html(string);
                    if (res.approvalCount === '0') {
                        $(that).find(".rangeDeletebtn").hide();
                    }
                }
            });
        });
        //            $(".tab-content").on('show.bs.collapse', ".m-accordion__item-body", function () {
        //              $(".m-accordion__item-head.collapsed.show").closest('.tab-content').collapse("show");
        //            });
        $(".tab-content").on('hide.bs.collapse', ".m-accordion__item-body", function(event) {
            $("#" + event.currentTarget.id).collapse("show");
        });
        $("#approvalDetailsModal").on('show.bs.modal', function(e) {
            var requestLeave_ID = $(e.relatedTarget).data('requestleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_request_approval_flow",
                data: {
                    requestLeave_ID: requestLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        var request_leave = res.request_leave;
                        var request_leave_approval = res.request_leave_approval;
                        $("#view-name").html(request_leave.fname + " " + request_leave.lname);
                        $("#view-pic").attr('src', '<?php echo base_url(); ?>' + request_leave.pic);
                        $("#view-job").html(request_leave.job + " <small>(" + request_leave.jobstat + ")</small>");
                        $("#view-requestdate").html("" + moment(request_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                        $("#view-reason").html('"' + request_leave.reason + '"');
                        $("#div-viewApproval").html("");
                        var string = '<div class="m-section" style="margin-bottom:0px"><div class="m-section__content">';

                        $.each(request_leave_approval, function(key, obj) {
                            var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                            var comment = ($.trim(obj.comment) === '') ? 'No comment' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                            string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                            string += '<div class="m-portlet__head-caption">';
                            string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">';
                            string += '<p class="text-center">' + blinker + '<strong style="color:white">' + obj.fname + ' ' + obj.lname + '</strong> <small style="color:#ddd;">( ' + decisionOn + ' )</small><br></p>';
                            string += '</p></div></div>';

                            string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                            string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                            string += '  <i class="fa fa-quote-left"></i>';
                            string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                            string += ' <i class="fa fa-quote-right"></i>';
                            string += ' </blockquote>';
                            string += '<div style="max-height:250px;overflow:auto;font-size:12px"><table class="table table-striped m-table table-sm">';
                            string += '<thead><tr><th>Date</th><th>Type</th><th>Description</th><th>Payment</th><th>Decision</th></tr></thead>';
                            string += '<tbody>';
                            $.each(obj.details, function(i, item) {
                                var minutes = (item.minutes === '480') ? "Full Day" : "Half Day";
                                var paid = (item.isPaid === '1') ? "Paid" : "Unpaid";
                                string += '<tr>';
                                string += '<td class="m--font-bolder">' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY') + '</td>';
                                string += '<td>' + item.leaveType + ' Leave</td>';
                                string += '<td>' + minutes + '</td>';
                                string += '<td>' + paid + '</td>';
                                string += '<td><span class="m-badge m-badge--' + statusClass[item.status] + ' m-badge--wide">' + item.description + '</span></td>';
                                string += '</tr>';
                            });
                            string += '</tbody></table></div>';

                            string += '</div></div></div>';
                        });
                        string += "</div></div>";
                        $("#div-viewApproval").html(string);
                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }
                    $(".m-widget7__img").on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                }
            });
        });
        $("#date-modal").on('show.bs.modal', function(e) {
            var requestLeaveDetails_ID = $(e.relatedTarget).data('requestleavedetailsid');
            var requestLeave_ID = $(e.relatedTarget).data('requestleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_date_approval_flow",
                data: {
                    requestLeave_ID: requestLeave_ID,
                    requestLeaveDetails_ID: requestLeaveDetails_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        var request_leave = res.request_leave;
                        var retract_leave = res.retract_leave;
                        var request_leave_approval = res.request_leave_approval;
                        var retract_leave_approval = res.retract_leave_approval;

                        var minutes = (request_leave.minutes === "480") ? "Full Day" : "Half Day";

                        var ispaid = (request_leave.isPaid === "1") ? "Yes" : "No";
                        var isReleased = (request_leave.isReleased === "1") ? "Yes" : "No";
                        // $("#date-isreleased").html(isReleased);
                        $("#date-ispaid").html(ispaid);
                        $("#date-minutes").html(minutes);
                        $("#date-name").html(request_leave.fname + " " + request_leave.lname);
                        $("#date-pic").attr('src', request_leave.pic);
                        $("#date-job").html(request_leave.job + " <small>(" + request_leave.jobstat + ")</small>");
                        $("#date-leavetype").html("<span style='text-transform:uppercase'>" + request_leave.leaveType + " LEAVE</span>");
                        $("#date-date").html("<span>" + moment(request_leave.date, 'YYYY-MM-DD').format("MMMM DD, YYYY") + "</span>");
                        $("#modal-request-createdOn").html("" + moment(request_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                        $("#modal-request-reason").html('"' + request_leave.reason + '"');

                        $("#tab-modal-request").html("");
                        $("#tab-modal-retraction").html("");
                        var string = '';
                        var customcnt = 1;
                        $.each(request_leave_approval, function(key, obj) {
                            var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                            //-------------------------------------------------------------------------------------- -
                            string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                            string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                            string += '<div class="m-portlet__head-caption">';
                            string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' ' + blinker + '</p>';
                            string += '</div></div>';
                            string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                            string += '   <div class="row">';
                            string += '<div class="col-md-12">';
                            string += ' <table class="table m-table table-bordered table-sm">';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                            string += ' </tr>';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                            string += ' </tr>';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                            string += ' </tr>';
                            string += '</table>';
                            string += '</div>';
                            string += '    <div class="col-md-12" >';
                            string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                            string += '  <i class="fa fa-quote-left"></i>';
                            string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                            string += ' <i class="fa fa-quote-right"></i>';
                            string += ' </blockquote>';
                            string += '</div>';
                            string += '</div>';
                            string += '</div></div></div>';
                            customcnt++;
                        });
                        string += "";
                        $("#tab-modal-request").html(string);

                        if (retract_leave === null || retract_leave_approval === null) {
                            $("#modal-retraction-approvals").hide();
                        } else {

                            var string = '';
                            var customcnt = 1;
                            $.each(retract_leave_approval, function(key, obj) {
                                var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                                var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                                var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                                //                                -------------------------------------------------------------------------------------- -
                                string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                                string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                                string += '<div class="m-portlet__head-caption">';
                                string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' ' + blinker + '</p>';
                                string += '</div></div>';
                                string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;;color:black;background:#e6e6e6"">';
                                string += '   <div class="row">';
                                string += '<div class="col-md-12">';
                                string += ' <table class="table m-table table-bordered table-sm">';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                                string += ' </tr>';
                                string += '</table>';
                                string += '</div>';
                                string += '    <div class="col-md-12" >';
                                string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                                string += '  <i class="fa fa-quote-left"></i>';
                                string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                                string += ' <i class="fa fa-quote-right"></i>';
                                string += ' </blockquote>';
                                string += '</div>';
                                string += '</div>';
                                string += '</div></div></div>';
                                customcnt++;
                            });
                            string += "";
                            $("#tab-modal-retraction").html(string);

                            $("#modal-retraction-approvals").show();
                        }

                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }
                    $("#date-pic").on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                }
            });
        });
        var $control = $('#reason'),
            $remaining = $('#textarea-note'),
            textLimit = parseInt($control.attr('maxlength'), 10);
        $remaining.text(textLimit - $('#reason').val().length);
        $control.on('keyup paste', function() {
            $control.val($control.val().substring(0, textLimit));
            $remaining.text(textLimit - parseInt($control.val().length, 10));
        });
        $('a[href="#tab-ongoing"]').click();
    });
</script>