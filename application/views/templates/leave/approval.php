<style type="text/css">
    label {
        font-weight: 402 !important;
        font-size: 12px !important;
    }

    .form-control[readonly] {
        background: #ddd !important;
    }

    body {
        padding-right: 0 !important;
    }

    .bg-custom-gray {
        background: #4B4C54 !important;
    }

    .font-white {
        color: white !important;
    }

    .portlet-header-report {
        background: #4B4C54 !important;
        color: white !important;
        height: 50px !important;
    }

    .font-poppins {
        font-family: 'Poppins' !important;
    }

    .m-accordion__item {
        border: 1.2px solid #545454 !important;
    }

    .m-accordion__item-mode {
        margin-left: 5px;
        font-size: 15px !important;
    }

    .m-accordion__item-icon i {
        font-size: 18px !important;
    }

    .m-accordion__item-title {
        font-size: 15px !important;
    }

    .m-accordion__item-head {
        background: #545454 !important;
        color: white !important;
    }

    .m-accordion__item-head:hover {
        background: #777 !important;
    }

    textarea {
        resize: none;
    }

    @media screen and (max-width: 500px) {
        .button-text {
            display: none !important;
        }

    }

    .m-table thead th {
        background: #545454;
        color: white;
    }

    .m-table th {
        border: 1px solid #545454 !important;
        text-align: center;
    }

    .m-table tr td {
        border: 1px solid #9B9C9E !important;
        text-align: center;
    }

    .m-demo__preview {
        border-color: #eee !important;
    }

    div {
        word-wrap: break-word;
    }

    .m-content ul.m-subheader__breadcrumbs li,
    .m-content ul.m-subheader__breadcrumbs li a {
        padding: 0 !important;
    }

    .m-content ul.m-subheader__breadcrumbs li a span:hover {
        background: #ddd !important;
        color: black !important;
    }

    .m-content ul.m-subheader__breadcrumbs li a span {
        color: #fff !important;
        padding: 10px 20px !important;
    }

    .m-content ul.m-subheader__breadcrumbs {
        background: #4B4C54 !important;
        padding: 2px;
    }

    .my-nav-active {
        background: #eee !important;
    }

    li.my-nav-active a span {
        color: black;
    }

    .m-content hr {
        border: 2.5px solid #eee !important;
    }

    .modal hr {
        border: 2px solid #eee !important;
    }

    .light-theme a,
    .light-theme span {
        padding: 1.5px 8px !important;
    }

    .m-content .m-stack__item {
        padding: 0 !important;
        background: 0 !important;
        border: 0 !important;
    }

    .m-badge {
        font-weight: 550;
    }

    /*DATERANGEPICKER*/
    .daterangepicker {
        font-size: 13px !important;
    }

    .daterangepicker table td {
        width: 28px !important;
        height: 28px !important;
    }

    .daterangepicker_input input {
        padding-top: 5px !important;
        padding-bottom: 5px !important;
        line-height: 1 !important;
    }

    .blockOverlay {
        opacity: 0.15 !important;
        background-color: #746DCB !important;
    }

    .tdplain tr td {
        border: 1px solid #f4f5f8 !important;
    }

    .table-credits {
        background: white !important;
    }

    .table-credits tr td {
        vertical-align: middle;
    }

    .table-credits tr td:first-child {
        background: #545454 !important;
        color: white !important;
        font-size: 12px;
    }

    .table-credits tr td:nth-child(2) {
        color: black !important;
        background: white !important;
        font-weight: 400;
        font-size: 14px;
    }

    .btn-credits-tooltip {
        font-size: 13px !important;
    }

    .btn-credits-tooltip i {
        font-size: 15px !important;
    }
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/approval'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Approval</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head bg-custom-gray">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon font-white">
                                    <i class="fa fa-drivers-license-o"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins">
                                    <span class="font-white"> Leave Approval</span> <small class="font-white"> List of requests requiring the approval of Supervisors</small>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-ongoing-request" role="tab"><i class="la la-tasks"></i> Request Approval</a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-ongoing-retraction" role="tab"><i class="la la-tasks"></i> Retraction Approval</a>
                            </li>
                            <li class="nav-item dropdown m-tabs__item">
                                <a class="nav-link m-tabs__link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="la la-tasks"></i> History</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" data-toggle="tab" href="#tab-history-request"><i class="la la-tasks"></i> Request History</a>
                                    <a class="dropdown-item" data-toggle="tab" href="#tab-history-retraction"><i class="la la-tasks"></i> Retraction History</a>
                                </div>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-ongoing-request" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h5 class="my-3 text-center">
                                            Ongoing Request Approval <br />
                                            <small class="text-muted">Pending and currently requires your approval.</small>
                                        </h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-ongoing-request" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="request-ongoing-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="request-ongoing-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="request-ongoing-count"></span> of <span id="request-ongoing-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-history-request" role="tabpanel">
                                <div class="row mb-3 no-gutters">
                                    <div class="col-lg-4 col-md-4 text-center">
                                        <h5 class="my-3">
                                            Request Approval History <br />
                                            <small class="text-muted">Previous records of approvals.</small>
                                        </h5>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <!-- <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px"> -->
                                        <div class="row m-0 p-2" style="background:#eee">

                                            <div class="col-md-6">
                                                <label for="">Search Name:</label>
                                                <input type="text" class="form-control form-control-sm m-input mb-1" placeholder="Search Employee Name... " id="search-custom-history-request">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="">Date Filed:</label>
                                                <div class="m-input-icon m-input-icon--left mb-1" id="request-history-daterange">
                                                    <input type="text" class="form-control form-control-sm m-input" placeholder="Date Range" readonly>
                                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                </div>
                                            </div>
                                            <!-- </div>

                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-history-request" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="request-history-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="request-history-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="request-history-count"></span> of <span id="request-history-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-ongoing-retraction" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h5 class="my-3 text-center">
                                            Ongoing Retraction Approval <br />
                                            <small class="text-muted">Pending and currently requires your approval.</small>
                                        </h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-ongoing-retraction" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="retraction-ongoing-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="retraction-ongoing-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="retraction-ongoing-count"></span> of <span id="retraction-ongoing-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-history-retraction" role="tabpanel">
                                <div class="row mb-3 no-gutters">
                                    <div class="col-lg-4 col-md-4 text-center">
                                        <h5 class="my-3">
                                            Retraction Approval History <br />
                                            <small class="text-muted">Previous records of retraction approvals.</small>
                                        </h5>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <!-- <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px"> -->
                                        <div class="row m-0 p-2" style="background:#eee">

                                            <div class="col-md-6">

                                                <label for="">Search Name:</label>
                                                <input type="text" class="form-control m-input form-control-sm" placeholder="Search Employee Name..." id="search-custom-history-retraction">

                                            </div>

                                            <div class="col-md-6">

                                                <label for="">Date Filed:</label>

                                                <div class="m-input-icon m-input-icon--left" id="retraction-history-daterange">
                                                    <input type="text" class="form-control m-input form-control-sm" placeholder="Date Range" readonly>
                                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                </div>
                                            </div>
                                            <!-- </div>

                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                            <div class="m-demo__preview" style="padding:10px">

                                                <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-history-retraction" role="tablist">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle">
                                                <ul id="retraction-history-pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:6%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="retraction-history-perpage">
                                                    <option>5</option>
                                                    <option>10</option>
                                                    <option>15</option>
                                                    <option>20</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="retraction-history-count"></span> of <span id="retraction-history-total"></span> records</div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approvalDetailsModal-retraction" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Approval for Retraction</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3 id="view-name-retraction"></h3>
                        <p id="view-job-retraction"></p>
                    </div>
                    <div class="col-lg-12">
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview" style="padding:10px 5px 0px 10px">
                                        <blockquote class="blockquote text-center" style="font-size:13px">
                                            <h5><i><small class="mb-0" id="view-reason-retraction"></small></i></h5>
                                            <footer class="blockquote-footer" id="view-requestdate-retraction"></footer>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5 style="font-size:14px">-RETRACTION APPROVALS-</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="div-viewApproval-retraction"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="date-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Details of Leave Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto;padding-top:0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img" id="date-pic" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="date-name" style="font-size:20px">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="date-job" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <table class="table m-table table-bordered table-sm">
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Type:</td>
                                <td id="date-leavetype" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date:</td>
                                <td id="date-date" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Class:</td>
                                <td id="date-minutes" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Paid:</td>
                                <td id="date-ispaid" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <!-- <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Release:</td>
                                <td id="date-isreleased" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr> -->

                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:</td>
                                <td id="modal-request-reason" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>

                            <tr>
                                <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Filed On:</td>
                                <td id="modal-request-createdOn" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>

                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div id="tab-modal-request">

                        </div>

                    </div>
                    <div class="col-lg-12">
                        <div id="modal-retraction-approvals" style="display:none">
                            <hr />
                            <p class="m--font-bolder">RETRACTION APPROVALS</p>
                            <div id="tab-modal-retraction">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="approvalDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Approval Process</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:450px;overflow:auto;padding-top:0 !important;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-widget7">
                            <div class="m-widget7__user" style='margin-bottom:20px'>
                                <div class="m-widget7__user-img">
                                    <img class="m-widget7__img" id="view-pic" alt="" style="width:50px">
                                </div>
                                <div class="m-widget7__info">
                                    <span class="m-widget7__username" id="view-name" style="font-size:20px">
                                    </span><br>
                                    <span class="m-widget7__time m--font-brand" id="view-job" style="font-size:13px">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                    <div class="col-lg-12">
                        <table class="table m-table table-bordered">

                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Reason:
                                <td id="view-reason" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                            <tr>
                                <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Date Filed:</td>
                                <td id="view-requestdate" style="border:1px solid #f4f5f8 !important;text-align:left !important"></td>
                            </tr>
                        </table>
                        <hr />
                    </div>
                    <div class="col-lg-12">

                        <p class="m--font-bolder">REQUEST APPROVALS</p>
                        <div id="div-viewApproval">
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script type="text/javascript">
    var statusClass = ["", "", "warning", "", "info", "success", "danger", "focus", "", "", "", "", "metal", "primary"];

    function selectAll(that) {
        $(that).closest('.m-accordion__item-content').find("[type=checkbox]").each(function(key, elem) {
            $(elem).prop('checked', true);
        });
    }

    function clearAll(that) {
        $(that).closest('.m-accordion__item-content').find("[type=checkbox]").each(function(key, elem) {
            $(elem).prop('checked', false);
        });
    }

    function submitApproval(that, requestLeave_ID) {
        var comment = $(that).closest('.m-accordion__item-content').find('textarea').val();
        if ($.trim(comment) === '') {
            swal("Incomplete Fields", "Please complete the form to proceed", "warning");
        } else {
            var $checkboxes = $(that).closest('.m-accordion__item-content').find("[type=checkbox]");
            var checkLength = $checkboxes.length;
            var checkedCnt = 0;
            var uncheckedCnt = 0;
            var data = [];
            $checkboxes.each(function(key, elem) {
                var id = $(elem).data('requestleavedetails_id');
                data.push({
                    requestLeaveDetails_ID: id,
                    stat: $(elem).prop('checked')
                });
                if ($(elem).prop('checked')) {
                    checkedCnt++;
                } else {
                    uncheckedCnt++;
                }
            });
            if (checkLength === checkedCnt) {
                var text = "All leave dates will be approved";
                var semiStatus = 5;
            } else if (checkLength === uncheckedCnt) {
                var text = "All leave dates will be rejected";
                var semiStatus = 6;
            } else {
                var text = "Approval will be made as per your decision";
                var semiStatus = 5;
            }
            swal({
                title: 'Are you sure?',
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Continue!'
            }).then((result) => {
                if (result.value) {
                    //                        console.log(data);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Leave/submit_approval_decision",
                        data: {
                            requestLeave_ID: requestLeave_ID,
                            comment: comment,
                            semiStatus: semiStatus,
                            data: data
                        },
                        cache: false,
                        success: function(res) {
                            res = JSON.parse(res.trim());
                            //                                console.log(res);
                            if (res.status === 'Success') {
                                swal("Good job!", "Leave Approval success!", "success");
                                $.each(res.notif_details, function(i, item) {
                                    notification(item.notif_id);
                                });

                                $('a[href="#tab-history-request"]').click();
                                $('a[href="#tab-ongoing-request"]').click();
                            } else {
                                swal("Failed!", "Something happened while approving leave request!", "error");
                            }
                        }
                    });
                }
            });
        }
    }

    function submitApprovalRetraction(that, retractLeave_ID) {
        var comment = $(that).closest('.m-accordion__item-content').find('textarea').val();
        if ($.trim(comment) === '') {
            swal("Incomplete Fields", "Please complete the form to proceed", "warning");
        } else {
            var $checkboxes = $(that).closest('.m-accordion__item-content').find("[type=checkbox]");
            var checkLength = $checkboxes.length;
            var checkedCnt = 0;
            var uncheckedCnt = 0;
            var data = [];
            $checkboxes.each(function(key, elem) {
                var id = $(elem).data('retractleavedetails_id');
                var requestLeaveDetails_ID = $(elem).data('requestleavedetails_id');
                data.push({
                    retractLeaveDetails_ID: id,
                    requestLeaveDetails_ID: requestLeaveDetails_ID,
                    stat: $(elem).prop('checked')
                });
                if ($(elem).prop('checked')) {
                    checkedCnt++;
                } else {
                    uncheckedCnt++;
                }
            });
            if (checkLength === checkedCnt) {
                var text = "All leave dates will be approved for retraction";
                var semiStatus = 5;
            } else if (checkLength === uncheckedCnt) {
                var text = "All leave dates will be rejected for retraction";
                var semiStatus = 6;
            } else {
                var text = "Retraction approval will be made as per your decision";
                var semiStatus = 5;
            }
            swal({
                title: 'Are you sure?',
                text: text,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Continue!'
            }).then((result) => {
                if (result.value) {
                    //                        console.log(data);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Leave/submit_approval_decision_for_retraction",
                        data: {
                            retractLeave_ID: retractLeave_ID,
                            comment: comment,
                            semiStatus: semiStatus,
                            data: data
                        },
                        cache: false,
                        success: function(res) {
                            res = JSON.parse(res.trim());
                            if (res.status === 'Success') {
                                swal("Good job!", "Leave Approval success!", "success");
                                $.each(res.notif_details, function(i, item) {
                                    notification(item.notif_id);
                                });

                                $('a[href="#tab-history-retraction"]').click();
                                $('a[href="#tab-ongoing-retraction"]').click();
                            } else {
                                swal("Failed!", "Something happened while approving leave request!", "error");
                            }
                        }
                    });
                }
            });
        }
    }

    //ONGOING REQUEST _-----------------------------------

    function getOngoingRequest(limiter, perPage, callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_ongoing_approval_list",
            data: {
                limiter: limiter,
                perPage: perPage
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#request-ongoing-total").html(res.total);
                $("#request-ongoing-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + item.requestLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    if (item.reason.length > 50) {
                        var substr = item.reason.substring(0, 50) + "...";
                    } else {
                        var substr = item.reason;
                    }
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9"><b>' + item.lname + ', ' + item.fname + '</b> <i class="fa fa-caret-right"></i> <small>' + substr + '</small></div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div></div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body request collapse" data-type="ongoing" data-requestleaveid="' + item.requestLeave_ID + '" id="accordion-item-' + item.requestLeave_ID + '" role="tabpanel" data-parent="#div-accordion-ongoing-request"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview text-center" style="padding:10px "> <i> "' + item.reason + '"</i></div></div>';
                    string += '<div class="displayhere"></div></div></div></div>';
                });

                if (string === '') {
                    string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have ongoing leave request. </div>';
                }
                $("#div-accordion-ongoing-request").html(string);
                var count = $("#request-ongoing-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#request-ongoing-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#request-ongoing-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#request-ongoing-count").html(1 + ' - ' + perPage);
                } else {
                    $("#request-ongoing-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }
    //HISTORY REQUEST _-----------------------------------

    function getHistoryRequest(limiter, perPage, callback) {
        var searchcustom = $("#search-custom-history-request").val();
        var datestart = $("#request-history-daterange").data('daterangepicker').startDate._d;
        var dateend = $("#request-history-daterange").data('daterangepicker').endDate._d;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_other_approval_list",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchcustom: searchcustom,
                datestart: moment(datestart).format("YYYY-MM-DD"),
                dateend: moment(dateend).add(1, 'day').format("YYYY-MM-DD")
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#request-history-total").html(res.total);
                $("#request-history-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-' + item.requestLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    if (item.reason.length > 50) {
                        var substr = item.reason.substring(0, 50) + "...";
                    } else {
                        var substr = item.reason;
                    }
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9"><b>' + item.lname + ', ' + item.fname + '</b> <i class="fa fa-caret-right"></i> <small>' + substr + '</small></div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div></div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body request collapse" data-type="history" data-requestleaveid="' + item.requestLeave_ID + '" id="accordion-item-' + item.requestLeave_ID + '" role="tabpanel" data-parent="#div-accordion-history-request"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview text-center" style="padding:10px "> <i>"' + item.reason + '"</i></div></div>';
                    string += '<div class="displayhere"></div></div></div></div>';
                });
                if (string === '') {
                    string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have a history of approvals yet.    </div>';
                }
                $("#div-accordion-history-request").html(string);
                var count = $("#request-history-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#request-history-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#request-history-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#request-history-count").html(1 + ' - ' + perPage);
                } else {
                    $("#request-history-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }
    //ONGOING RETRACTION _-----------------------------------

    function getOngoingRetraction(limiter, perPage, callback) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_ongoing_approval_retraction_list",
            data: {
                limiter: limiter,
                perPage: perPage
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#retraction-ongoing-total").html(res.total);
                $("#retraction-ongoing-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-retraction-' + item.retractLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    if (item.reason.length > 50) {
                        var substr = item.reason.substring(0, 50) + "...";
                    } else {
                        var substr = item.reason;
                    }
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9"><b>' + item.lname + ', ' + item.fname + '</b> <i class="fa fa-caret-right"></i> <small>' + substr + '</small></div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div></div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body retraction collapse" data-type="ongoing" data-retractleaveid="' + item.retractLeave_ID + '" id="accordion-item-retraction-' + item.retractLeave_ID + '" role="tabpanel" data-parent="#div-accordion-ongoing-retraction"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview text-center" style="padding:10px "> <i> "' + item.reason + '"</i></div></div>';
                    string += '<div class="displayhere"></div></div></div></div>';
                });

                if (string === '') {
                    string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have ongoing leave retraction request.  </div>';
                }
                $("#div-accordion-ongoing-retraction").html(string);
                var count = $("#retraction-ongoing-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#retraction-ongoing-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#retraction-ongoing-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#retraction-ongoing-count").html(1 + ' - ' + perPage);
                } else {
                    $("#retraction-ongoing-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }
    //HISTORY RETRACTION _-----------------------------------

    function getHistoryRetraction(limiter, perPage, callback) {
        var searchcustom = $("#search-custom-history-retraction").val();
        var datestart = $("#retraction-history-daterange").data('daterangepicker').startDate._d;
        var dateend = $("#retraction-history-daterange").data('daterangepicker').endDate._d;
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Leave/get_other_approval_retraction_list",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchcustom: searchcustom,
                datestart: moment(datestart).format("YYYY-MM-DD"),
                dateend: moment(dateend).add(1, 'day').format("YYYY-MM-DD")
            },
            beforeSend: function() {
                mApp.block('#m_blockui_2_portlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#retraction-history-total").html(res.total);
                $("#retraction-history-total").data('count', res.total);
                var string = "";
                $.each(res.data, function(i, item) {
                    string += '<div class="m-accordion__item">';
                    string += '<div class="m-accordion__item-head collapsed" srole="tab" data-toggle="collapse" href="#accordion-item-retraction-' + item.retractLeave_ID + '" aria-expanded="  false">';
                    string += '<span class="m-accordion__item-icon"><i class="fa flaticon-list-1"></i></span>';
                    if (item.reason.length > 50) {
                        var substr = item.reason.substring(0, 50) + "...";
                    } else {
                        var substr = item.reason;
                    }
                    string += '<span class="m-accordion__item-title"><div class="row"><div class="col-md-9"><b>' + item.lname + ', ' + item.fname + '</b> <i class="fa fa-caret-right"></i> <small>' + substr + '</small></div><div class="col-md-3"><small class="pull-right">' + moment(item.createdOn, "YYYY-MM-DD HH:mm:ss").format("ddd MMM DD, YYYY") + '</small></div></div></span>';
                    string += '<span class="m-accordion__item-mode"></span>';
                    string += '</div>';
                    string += '<div class="m-accordion__item-body retraction collapse" data-type="history" data-retractleaveid="' + item.retractLeave_ID + '" id="accordion-item-retraction-' + item.retractLeave_ID + '" role="tabpanel" data-parent="#div-accordion-history-retraction"> ';
                    string += '<div class="m-accordion__item-content" >';
                    string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview text-center" style="padding:10px "> <i>"' + item.reason + '"</i></div></div>';
                    string += '<div class="displayhere"></div></div></div></div>';
                });
                if (string === '') {
                    string = '<div style="margin-top:15px" class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div><div class="m-alert__text"><strong>No Record!</strong> You do not have a history of retraction approvals yet. </div>';
                }
                $("#div-accordion-history-retraction").html(string);
                var count = $("#retraction-history-total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#retraction-history-count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#retraction-history-count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#retraction-history-count").html(1 + ' - ' + perPage);
                } else {
                    $("#retraction-history-count").html((limiter + 1) + ' - ' + result);
                }
                mApp.unblock('#m_blockui_2_portlet');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#m_blockui_2_portlet');
            }
        });
    }
    $(function() {
        $('a[href="#tab-ongoing-request"]').on('show.bs.tab', function(event) {
            $("#div-accordion-history-request").html("");

            $("#request-ongoing-perpage").change();
            //END OF ONGOING PAGINATION --------------------------------------
        });
        //ONGOING REQUEST PAGINATION-------------------------------------------------
        $("#request-ongoing-perpage").change(function() {
            var perPage = $("#request-ongoing-perpage").val();
            getOngoingRequest(0, perPage, function(total) {
                $("#request-ongoing-pagination").pagination('destroy');
                $("#request-ongoing-pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#request-ongoing-perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    onPageClick: function(pagenumber) {
                        var perPage = $("#request-ongoing-perpage").val();
                        getOngoingRequest((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });
        $('a[href="#tab-history-request"]').on('show.bs.tab', function(event) {
            $("#div-accordion-ongoing-request").html("");
            $('#request-history-daterange .form-control').val(moment().startOf('year').format('MM/DD/YYYY') + ' - ' + moment().endOf('year').format('MM/DD/YYYY'));
            $('#request-history-daterange').daterangepicker({
                    startDate: moment().startOf('year'),
                    endDate: moment().endOf('year'),
                    opens: 'left',
                    drops: 'down',
                    autoUpdateInput: true,
                    ranges: {
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                        'This Year': [moment().startOf('year'), moment().endOf('year')]
                    }
                },
                function(start, end, label) {
                    $('#request-history-daterange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                    $("#request-history-perpage").change();
                });

            $("#request-history-perpage").change();
            //END OF HISTORY REQUEST PAGINATION----------------------------------------------
        });
        //HISTORY REQUEST PAGINATION-----------------------------------------------------

        $("#request-history-perpage").change(function() {
            var perPage = $("#retraction-history-perpage").val();
            getHistoryRequest(0, perPage, function(total) {
                $("#request-history-pagination").pagination('destroy');
                $("#request-history-pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#request-history-perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    onPageClick: function(pagenumber) {
                        var perPage = $("#request-history-perpage").val();
                        getHistoryRequest((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });
        $("#search-custom-history-request").keyup(function() {
            $("#request-history-perpage").change();
        })
        $(".tab-content").on('show.bs.collapse', ".m-accordion__item-body.request", function() {

            var requestLeave_ID = $(this).data('requestleaveid');
            var accordionType = $(this).data('type');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_request_details",
                data: {
                    requestLeave_ID: requestLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        var string = '';

                        //end of credits
                        string += (accordionType === 'ongoing') ? '<div class="row"><div class="col-md-6"><h4>REQUEST APPROVAL <a href="" onclick="event.preventDefault();" class="btn-credits-tooltip m-link"><i class="flaticon-coins"></i> Credits</a></h4></div><div class="col-md-6"><div class="btn-group m-btn-group pull-right" role="group" aria-label="...">\n\
                        <button type="button" title="View Approval" class="m-btn btn btn-sm btn-brand"  data-requestleaveid="' + requestLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal"><i class="fa flaticon-list-2"></i> <span class="button-text">View Approval</span></button>\n\
                        <button type="button" title="Select All" class="m-btn btn btn-sm btn-secondary" onclick="selectAll(this)"><i class="fa fa-check m--font-success"></i> <span class="button-text">Select All</span></button>\n\
                        <button type="button" title="Clear All" class="m-btn btn btn-sm btn-secondary" onclick="clearAll(this)"><i class="fa fa-times m--font-danger"></i> <span class="button-text">Clear All</span></button>\n\
                        </div>' : '<div class="row"><div class="col-md-6"><h4>REQUEST APPROVAL</h4></div><div class="col-md-6"><a href="#" title="View Approval" class="btn btn-brand btn-sm m-btn m-btn--icon m-btn--outline-2x pull-right" data-requestleaveid="' + requestLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal"><span><i class="fa  flaticon-list-2"></i><span class="button-text">View Approval</span></span></a><br /><br />';
                        string += '</div><br /><br /><div class="col-md-12"><div style="overflow:auto;max-height:250px">';
                        string += '<table class="table table-sm m-table table-striped table-bordered table-hover">';
                        string += '<thead>';
                        string += '<tr>';
                        string += '<th>Date</th>';
                        string += '<th>Leave Description</th>';
                        string += '<th>Type</th>';
                        string += '<th>Note</th>';
                        string += '<th>Final Status</th>';
                        string += '<th>Action</th>';
                        string += (accordionType === 'ongoing') ? '<th>Approval</th>' : '';
                        string += '</tr></thead><tbody>';

                        $.each(res.data, function(i, item) {
                            var minutes = (item.minutes === "480") ? "Full" : "Half";
                            var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                            var viewdetails = '<td> <a href="#" class="m-link m-link--state m-link--primary" data-toggle="modal" data-target="#date-modal" data-requestleaveid="' + requestLeave_ID + '" data-requestleavedetailsid="' + item.requestLeaveDetails_ID + '"><i class="fa fa-eye"></i> View Details</a></td>';
                            if (accordionType === 'ongoing') {
                                if (item.overAllStatus === '2') {
                                    string += "<tr>";
                                    string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                                    string += "<td>" + item.leave_name + "</td>";
                                    string += "<td>" + item.leaveType + " Leave</td>";
                                    string += "<td>" + minutes + " Day (" + isPaid + ")</td>";
                                    string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.description + '</span></td>';
                                    string += viewdetails;

                                    if (item.pickerMode === 'range') {
                                        string += (accordionType === 'ongoing') ? '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-requestleavedetails_id="' + item.requestLeaveDetails_ID + '" checked disabled> <span></span></label></td>' : '';
                                    } else {
                                        string += (accordionType === 'ongoing') ? '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-requestleavedetails_id="' + item.requestLeaveDetails_ID + '" checked> <span></span></label></td>' : '';
                                    }
                                    string += "</tr>";
                                }

                            } else {
                                string += "<tr>";
                                string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                                string += "<td>" + item.leave_name + " Leave</td>";
                                string += "<td>" + item.leaveType + " Leave</td>";
                                string += "<td>" + minutes + " Day (" + isPaid + ")</td>";
                                string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.description + '</span></td>';
                                string += viewdetails;
                                string += (accordionType === 'ongoing') ? '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-requestleavedetails_id="' + item.requestLeaveDetails_ID + '" checked> <span></span></label></td>' : '';
                                string += "</tr>";
                            }
                        });
                        string += "</tbody></table>";

                        string += "</div>";

                        string += (accordionType === 'ongoing') ? '<hr /><div class="row"><div class="col-md-12"><div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview" style="padding:10px "><label for="textarea">Enter your comment or notes:</label><textarea class="form-control m-input" maxlength="255" rows="2" required></textarea></div></div></div>' : '';

                        string += (accordionType === 'ongoing') ? '<div class="col-md-12 text-center"> \n\
                        <a href="#" onclick="submitApproval(this,' + requestLeave_ID + ')" class=" btn m-btn--pill pull-right btn-outline-brand m-btn m-btn--custom" title="Submit Approval"><span><i class="fa fa-send-o"></i><span class="button-text"> Submit Approval</span></span></a></div></div>' : '';

                        string += '</div></div>';
                        $("#accordion-item-" + requestLeave_ID).find('.displayhere').html(string);

                        if (res.credits === null) {
                            $("#accordion-item-" + requestLeave_ID).find(".btn-credits-tooltip").hide();
                        } else {
                            $("#accordion-item-" + requestLeave_ID).find(".btn-credits-tooltip").show();
                            var creditsString = '<table class=\'table table-sm m-table table-credits mb-0\'><tbody><tr><td>Type</td><td>' + res.credits.leaveType + ' Leave</td></tr><tr><td>Credits</td><td>' + res.credits.credits + '</td></tr><td>Taken</td><td>' + res.credits.taken + '</td></tr><tr><td>Remaining</td><td>' + res.credits.remaining + '</td></tr></tbody></table>';
                            $("#accordion-item-" + requestLeave_ID).find(".btn-credits-tooltip").popover({
                                trigger: 'hover',
                                placement: 'bottom',
                                html: true,
                                template: '\
                                <div class="m-popover popover" role="tooltip">\
                                <div class="arrow"></div>\
                                <h3 class="popover-header"></h3>\
                                <div class="popover-body"></div>\
                                </div>',
                                content: creditsString
                            })
                        }

                    } else {
                        $("#accordion-item-" + requestLeave_ID).find('.displayhere').html('<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show" role="alert"><div class="m-alert__icon"><i class="flaticon-exclamation-1"></i><span></span></div><div class="m-alert__text"><strong>Missing record details</strong> Please contact administrator.</div></div>');
                    }
                }
            });
        });

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------

        $('a[href="#tab-ongoing-retraction"]').on('show.bs.tab', function(event) {
            $("#div-accordion-history-retraction").html("");

            $("#retraction-ongoing-perpage").change();
            //END OF RETRACTION PAGINATION --------------------------------------
        });
        //ONGOING RETRACTION PAGINATION-------------------------------------------------
        $("#retraction-ongoing-perpage").change(function() {
            var perPage = $("#retraction-ongoing-perpage").val();
            getOngoingRetraction(0, perPage, function(total) {
                $("#retraction-ongoing-pagination").pagination('destroy');
                $("#retraction-ongoing-pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#retraction-ongoing-perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    onPageClick: function(pagenumber) {
                        var perPage = $("#retraction-ongoing-perpage").val();
                        getOngoingRetraction((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });
        $('a[href="#tab-history-retraction"]').on('show.bs.tab', function(event) {
            $("#div-accordion-ongoing-retraction").html("");
            $('#retraction-history-daterange .form-control').val(moment().startOf('year').format('MM/DD/YYYY') + ' - ' + moment().endOf('year').format('MM/DD/YYYY'));
            $('#retraction-history-daterange').daterangepicker({
                    startDate: moment().startOf('year'),
                    endDate: moment().endOf('year'),
                    opens: 'left',
                    drops: 'down',
                    autoUpdateInput: true,
                    ranges: {
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                        'This Year': [moment().startOf('year'), moment().endOf('year')]
                    }
                },
                function(start, end, label) {
                    $('#retraction-history-daterange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
                    $("#retraction-history-perpage").change();
                });

            $("#retraction-history-perpage").change();
            //END OF HISTORY RETRACTIOn PAGINATION----------------------------------------------
        });
        //HISTORY RETRACTIOn PAGINATION-----------------------------------------------------
        $("#retraction-history-perpage").change(function() {
            var perPage = $("#retraction-history-perpage").val();
            getHistoryRetraction(0, perPage, function(total) {
                $("#retraction-history-pagination").pagination('destroy');
                $("#retraction-history-pagination").pagination({
                    items: total, //default
                    itemsOnPage: $("#retraction-history-perpage").val(),
                    hrefTextPrefix: "#",
                    cssStyle: 'light-theme',
                    onPageClick: function(pagenumber) {
                        var perPage = $("#retraction-history-perpage").val();
                        getHistoryRetraction((pagenumber * perPage) - perPage, perPage);
                    }
                });
            });
        });

        $("#search-custom-history-retraction").keyup(function() {
            $("#retraction-history-perpage").change();
        })
        $(".tab-content").on('show.bs.collapse', ".m-accordion__item-body.retraction", function() {

            var retractLeave_ID = $(this).data('retractleaveid');
            var accordionType = $(this).data('type');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_retraction_details",
                data: {
                    retractLeave_ID: retractLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    var string = '';

                    string += (accordionType === 'ongoing') ? '<div class="row"><div class="col-md-6"><h4>RETRACTION APPROVAL</h4></div><div class="col-md-6"><div class="btn-group m-btn-group pull-right" role="group" aria-label="...">\n\
                    <button type="button" title="View Approval" class="m-btn btn btn-sm btn-brand"  data-retractleaveid="' + retractLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal-retraction"><i class="fa flaticon-list-2"></i> <span class="button-text">View Approval</span></button>\n\
                    <button type="button" title="Select All" class="m-btn btn btn-sm btn-secondary" onclick="selectAll(this)"><i class="fa fa-check m--font-success"></i> <span class="button-text">Select All</span></button>\n\
                    <button type="button" title="Clear All" class="m-btn btn btn-sm btn-secondary" onclick="clearAll(this)"><i class="fa fa-times m--font-danger"></i> <span class="button-text">Clear All</span></button>\n\
                    </div>' : '<div class="row"><div class="col-md-6"><h4>RETRACTION APPROVAL</h4></div><div class="col-md-6"><a href="#" title="View Approval" class="btn btn-brand btn-sm m-btn m-btn--icon m-btn--outline-2x pull-right" data-retractleaveid="' + retractLeave_ID + '" data-toggle="modal" data-target="#approvalDetailsModal-retraction"><span><i class="fa  flaticon-list-2"></i><span class="button-text">View Approval</span></span></a><br /><br />';
                    string += '</div><br /><br /><div class="col-md-12"><div style="overflow:auto;max-height:250px">';
                    string += '<table class="table table-sm m-table table-striped table-bordered table-hover">';
                    string += '<thead>';
                    string += '<tr>';
                    string += '<th>Date</th>';
                    string += '<th>Type</th>';
                    string += '<th>Final Status</th>';
                    string += '<th>Description</th>';
                    string += '<th>Payment</th>';
                    string += (accordionType === 'ongoing') ? '<th>Approval</th>' : '';
                    string += '</tr></thead><tbody>';
                    $.each(res, function(i, item) {
                        //                            console.log(item);
                        var minutes = (item.minutes === "480") ? "Full" : "Half";
                        var isPaid = (item.isPaid === "1") ? "Paid" : "Unpaid";
                        if (accordionType === 'ongoing') {
                            if (item.overAllStatus === '2') {
                                string += "<tr>";
                                string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                                string += "<td>" + item.leaveType + " Leave</td>";
                                string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.status + '</span></td>';
                                string += "<td>" + minutes + " Day</td>";
                                string += "<td>" + isPaid + "</td>";
                                string += (accordionType === 'ongoing') ? '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-retractleavedetails_id="' + item.retractLeaveDetails_ID + '" data-requestleavedetails_id="' + item.requestLeaveDetails_ID + '" checked> <span></span></label></td>' : '';
                                string += "</tr>";
                            }
                        } else {
                            string += "<tr>";
                            string += "<td>" + moment(item.date, 'YYYY-MM-DD').format("MMM DD, YYYY") + "</td>";
                            string += "<td>" + item.leaveType + " Leave</td>";
                            string += '<td> <span class="m-badge m-badge--' + statusClass[item.overAllStatus] + ' m-badge--wide">' + item.status + '</span></td>';
                            string += "<td>" + minutes + " Day</td>";
                            string += "<td>" + isPaid + "</td>";
                            string += (accordionType === 'ongoing') ? '<td><label class="m-checkbox m-checkbox--state-brand"><input type="checkbox" data-retractleavedetails_id="' + item.retractLeaveDetails_ID + '" data-requestleavedetails_id="' + item.requestLeaveDetails_ID + '" checked> <span></span></label></td>' : '';
                            string += "</tr>";
                        }
                    });
                    string += "</tbody></table>";

                    string += "</div>";
                    string += (accordionType === 'ongoing') ? '<hr /><div class="row"><div class="col-md-12"><div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview" style="padding:10px "><label for="textarea">Enter your comment or notes:</label><textarea class="form-control m-input" maxlength="255" rows="2" required></textarea></div></div></div>' : '';

                    string += (accordionType === 'ongoing') ? '<div class="col-md-12 text-center"> \n\
                    <a href="#" onclick="submitApprovalRetraction(this,' + retractLeave_ID + ')" class="btn m-btn--pill pull-right btn-outline-brand m-btn m-btn--custom" title="Submit Approval"><span><i class="fa fa-send-o"></i><span class="button-text"> Submit Approval</span></span></a></div></div>' : '';

                    string += '</div></div>';
                    $("#accordion-item-retraction-" + retractLeave_ID).find('.displayhere').html(string);
                }
            });
        });
        $("#approvalDetailsModal-retraction").on('show.bs.modal', function(e) {
            var retractLeave_ID = $(e.relatedTarget).data('retractleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_retraction_approval_flow",
                data: {
                    retractLeave_ID: retractLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        //                            console.log(res);request_leave
                        var retract_leave = res.retract_leave;
                        var retract_leave_approval = res.retract_leave_approval;
                        $("#view-name-retraction").html(retract_leave.fname + " " + retract_leave.lname);
                        $("#view-job-retraction").html(retract_leave.job + " <small>(" + retract_leave.jobstat + ")</small>");
                        $("#view-requestdate-retraction").html("<b>" + moment(retract_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "</b>");
                        $("#view-reason-retraction").html(retract_leave.reason);
                        $("#div-viewApproval-retraction").html("");
                        var string = '<div class="m-section" style="margin-bottom:-2px"><div class="m-section__content">';

                        $.each(retract_leave_approval, function(key, obj) {
                            var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                            var comment = ($.trim(obj.comment) === '') ? 'No comment' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            string += '<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false"><div class="m-demo__preview" style="padding:10px ">';
                            string += '<p>' + blinker + '<strong style="color:#018BCA;">' + obj.fname + ' ' + obj.lname + '</strong> <small style="color:#726BCA;">( ' + decisionOn + ' )</small><br>-<small><i>' + comment + '</i></small></p>';
                            string += '<div style="max-height:250px;overflow:auto;font-size:12px"><table class="table table-striped m-table table-sm">';
                            string += '<thead><tr><th>Date</th><th>Type</th><th>Description</th><th>Payment</th><th>Decision</th></tr></thead>';
                            string += '<tbody>';
                            $.each(obj.details, function(i, item) {
                                var minutes = (item.minutes === '480') ? "Full Day" : "Half Day";
                                var paid = (item.isPaid === '1') ? "Paid" : "Unpaid";
                                string += '<tr>';
                                string += '<td>' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY') + '</td>';
                                string += '<td>' + item.leaveType + '</td>';
                                string += '<td>' + minutes + '</td>';
                                string += '<td>' + paid + '</td>';
                                string += '<td><span class="m-badge m-badge--' + statusClass[item.status] + ' m-badge--wide">' + item.description + '</span></td>';
                                string += '</tr>';
                            });
                            string += '</tbody></table></div>';
                            string += "</div></div>";
                        });
                        string += "</div></div>";
                        $("#div-viewApproval-retraction").html(string);
                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }

                }
            });
        });
        $("#approvalDetailsModal").on('show.bs.modal', function(e) {
            var requestLeave_ID = $(e.relatedTarget).data('requestleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_request_approval_flow",
                data: {
                    requestLeave_ID: requestLeave_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        //                            console.log(res);
                        var request_leave = res.request_leave;
                        var request_leave_approval = res.request_leave_approval;
                        $("#view-name").html(request_leave.fname + " " + request_leave.lname);
                        $("#view-pic").attr('src', '<?php echo base_url(); ?>' + request_leave.pic);
                        $("#view-job").html(request_leave.job + " <small>(" + request_leave.jobstat + ")</small>");
                        $("#view-requestdate").html("" + moment(request_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                        $("#view-reason").html('"' + request_leave.reason + '"');
                        $("#div-viewApproval").html("");
                        var string = '<div class="m-section" style="margin-bottom:0px"><div class="m-section__content">';

                        $.each(request_leave_approval, function(key, obj) {
                            var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                            var comment = ($.trim(obj.comment) === '') ? 'No comment' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                            string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                            string += '<div class="m-portlet__head-caption">';
                            string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">';
                            string += '<p class="text-center">' + blinker + '<strong style="color:white">' + obj.fname + ' ' + obj.lname + '</strong> <small style="color:#ddd;">( ' + decisionOn + ' )</small><br></p>';
                            string += '</p></div></div>';

                            string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                            string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                            string += '  <i class="fa fa-quote-left"></i>';
                            string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                            string += ' <i class="fa fa-quote-right"></i>';
                            string += ' </blockquote>';
                            string += '<div style="max-height:250px;overflow:auto;font-size:12px"><table class="table table-striped m-table table-sm">';
                            string += '<thead><tr><th>Date</th><th>Type</th><th>Description</th><th>Payment</th><th>Decision</th></tr></thead>';
                            string += '<tbody>';
                            $.each(obj.details, function(i, item) {
                                var minutes = (item.minutes === '480') ? "Full Day" : "Half Day";
                                var paid = (item.isPaid === '1') ? "Paid" : "Unpaid";
                                string += '<tr>';
                                string += '<td class="m--font-bolder">' + moment(item.date, 'YYYY-MM-DD').format('MMM DD, YYYY') + '</td>';
                                string += '<td>' + item.leaveType + ' Leave</td>';
                                string += '<td>' + minutes + '</td>';
                                string += '<td>' + paid + '</td>';
                                string += '<td><span class="m-badge m-badge--' + statusClass[item.status] + ' m-badge--wide">' + item.description + '</span></td>';
                                string += '</tr>';
                            });
                            string += '</tbody></table></div>';

                            string += '</div></div></div>';
                        });
                        string += "</div></div>";
                        $("#div-viewApproval").html(string);
                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }
                    $(".m-widget7__img").on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                }
            });
        });
        $("#date-modal").on('show.bs.modal', function(e) {
            var requestLeaveDetails_ID = $(e.relatedTarget).data('requestleavedetailsid');
            var requestLeave_ID = $(e.relatedTarget).data('requestleaveid');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Leave/get_date_approval_flow",
                data: {
                    requestLeave_ID: requestLeave_ID,
                    requestLeaveDetails_ID: requestLeaveDetails_ID
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        var request_leave = res.request_leave;
                        var retract_leave = res.retract_leave;
                        var request_leave_approval = res.request_leave_approval;
                        var retract_leave_approval = res.retract_leave_approval;

                        var minutes = (request_leave.minutes === "480") ? "Full Day" : "Half Day";

                        var ispaid = (request_leave.isPaid === "1") ? "Yes" : "No";
                        var isReleased = (request_leave.isReleased === "1") ? "Yes" : "No";
                        // $("#date-isreleased").html(isReleased);
                        $("#date-ispaid").html(ispaid);
                        $("#date-minutes").html(minutes);
                        $("#date-name").html(request_leave.fname + " " + request_leave.lname);
                        $("#date-pic").attr('src', request_leave.pic);
                        $("#date-job").html(request_leave.job + " <small>(" + request_leave.jobstat + ")</small>");
                        $("#date-leavetype").html("<span style='text-transform:uppercase'>" + request_leave.leaveType + " LEAVE</span>");
                        $("#date-date").html("<span>" + moment(request_leave.date, 'YYYY-MM-DD').format("MMMM DD, YYYY") + "</span>");
                        $("#modal-request-createdOn").html("" + moment(request_leave.createdOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A') + "");
                        $("#modal-request-reason").html('"' + request_leave.reason + '"');

                        $("#tab-modal-request").html("");
                        $("#tab-modal-retraction").html("");
                        var string = '';
                        var customcnt = 1;
                        $.each(request_leave_approval, function(key, obj) {
                            var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                            var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                            var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                            //                                -------------------------------------------------------------------------------------- -
                            string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                            string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                            string += '<div class="m-portlet__head-caption">';
                            string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' ' + blinker + '</p>';
                            string += '</div></div>';
                            string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;color:black;background:#e6e6e6">';
                            string += '   <div class="row">';
                            string += '<div class="col-md-12">';
                            string += ' <table class="table m-table table-bordered table-sm">';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                            string += ' </tr>';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                            string += ' </tr>';
                            string += '  <tr>';
                            string += '     <td class="m--font-bolder" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                            string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                            string += ' </tr>';
                            string += '</table>';
                            string += '</div>';
                            string += '    <div class="col-md-12" >';
                            string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                            string += '  <i class="fa fa-quote-left"></i>';
                            string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                            string += ' <i class="fa fa-quote-right"></i>';
                            string += ' </blockquote>';
                            string += '</div>';
                            string += '</div>';
                            string += '</div></div></div>';
                            customcnt++;
                        });
                        string += "";
                        $("#tab-modal-request").html(string);

                        if (retract_leave === null || retract_leave_approval === null) {
                            $("#modal-retraction-approvals").hide();
                        } else {

                            var string = '';
                            var customcnt = 1;
                            $.each(retract_leave_approval, function(key, obj) {
                                var comment = ($.trim(obj.comment) === '') ? 'No Notes found.' : $.trim(obj.comment);
                                var decisionOn = (obj.decisionOn === '0000-00-00 00:00:00') ? 'No Decision Yet' : moment(obj.decisionOn, 'YYYY-MM-DD HH:mm:ss').format('MMM DD, YYYY hh:mm A');
                                var blinker = (obj.semiStatus === '4') ? '<span class="m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink" style="padding:5px;margin-left:5px;margin-right:5px"></span>' : '';
                                //                                -------------------------------------------------------------------------------------- -
                                string += ' <div class="m-portlet m-portlet--rounded m-portlet--bordered m-portlet--unair">';
                                string += '<div class="m-portlet__head" style="height:auto;background: #666;">';
                                string += '<div class="m-portlet__head-caption">';
                                string += '<p class="m-portlet__head-text" style="font-size:14px;font-weight:400;margin-top:10px;color:white !important">APPROVER ' + customcnt + ' ' + blinker + '</p>';
                                string += '</div></div>';
                                string += ' <div class="m-portlet__body" style="padding:20px;padding-bottom:10px;;color:black;background:#e6e6e6"">';
                                string += '   <div class="row">';
                                string += '<div class="col-md-12">';
                                string += ' <table class="table m-table table-bordered table-sm">';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Name:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + obj.fname + ' ' + obj.lname + '</td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Decision:</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important"><span class="m-badge m-badge--' + statusClass[obj.details.status] + ' m-badge--wide m--font-bolder">' + obj.details.description + '</span></td>';
                                string += ' </tr>';
                                string += '  <tr>';
                                string += '     <td class="m--font-boldest" style="border:1px solid #f4f5f8 !important;text-align:left !important">Datetime :</td>';
                                string += '   <td style="border:1px solid #f4f5f8 !important;text-align:left !important">' + decisionOn + '</td>';
                                string += ' </tr>';
                                string += '</table>';
                                string += '</div>';
                                string += '    <div class="col-md-12" >';
                                string += ' <blockquote class=" p-3" style="border-left: 8px solid #545455!important; background: #daf7fb!important;">';
                                string += '  <i class="fa fa-quote-left"></i>';
                                string += '  <span class="mb-0 text-capitalize">' + comment + '</span>';
                                string += ' <i class="fa fa-quote-right"></i>';
                                string += ' </blockquote>';
                                string += '</div>';
                                string += '</div>';
                                string += '</div></div></div>';
                                customcnt++;
                            });
                            string += "";
                            $("#tab-modal-retraction").html(string);

                            $("#modal-retraction-approvals").show();
                        }

                    } else {
                        $(this).modal('hide');
                        swal("Failed!", "Something happened while retrieving details!", "error");
                    }
                    $("#date-pic").on('error', function() {
                        $(this).attr('onerror', null);
                        $(this).attr('src', '<?php echo base_url('assets/images/img/sz.png'); ?>');
                    });
                }
            });
        });

        $('a[href="#tab-ongoing-request"]').click();
    });
</script>