<style type="text/css">
    .no-padding{
        padding:0 !important;
    }
    .padding-30A{
        padding:30px !important;
    }
    .portlet-header-report{
        background:#5E697D !important;
        color:white  !important;
    }
    #highchart-daterange input{
        color:white !important;
    }
    #highchart-chart{
        height:400px !important;
    }
    .highcharts-credits,.highcharts-contextmenu,.highcharts-exporting-group{
        display:none;
    }

    .m-demo__preview{
        border-color:#eee !important;
    }
    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
    }
    .table thead th{
        padding:  10px 4px
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Leave Reports</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url(); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/reports'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Reports</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/report_status'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Status</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">	
                <!--begin::Portlet-->
                <div class="m-portlet" id="m_blockui_2_portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-calendar"></i>
                                </span>
                                <h3 class="m-portlet__head-text" style="color:white !important">
                                    Leave Report Per Status
                                </h3>
                            </div>			
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" class="m-portlet__nav-link btn btn-brand m-btn m-btn--pill" id="btn-filters">
                                        Show Filters <i class="fa fa-filter"></i>
                                    </a>
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="#" class="m-portlet__nav-link btn btn-success m-btn m-btn--pill" id="export">
                                        Export <i class="fa fa-download"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false" style="margin-top:0">
                                    <div class="m-demo__preview" id="filters" style="display:none;padding:10px">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="">Year:</label>
                                                <div class="form-group m-form__group">
                                                    <select class="form-control m-input m-input--square" id="search-year">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="">Class:</label>
                                                <select class="form-control m-input m-input--square" id="search-class">
                                                    <option value="">All</option>
                                                    <option value="Admin">Admin</option>
                                                    <option value="Agent">Agent</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="">Paid:</label>
                                                <select class="form-control m-input m-input--square" id="search-paid">
                                                    <option value="">All</option>
                                                    <option value="1">Paid</option>
                                                    <option value="0">Unpaid</option>
                                                </select>
                                            </div>

                                            <div class="col-md-2">
                                                <label for="">Date Type:</label>
                                                <select class="form-control m-input m-input--square" id="search-minutes">
                                                    <option value="">All</option>
                                                    <option value="480">Full Day</option>
                                                    <option value="240">Half Day</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="">Released:</label>
                                                <select class="form-control m-input m-input--square" id="search-released">
                                                    <option value="">All</option>
                                                    <option value="1">Released</option>
                                                    <option value="0">Unreleased</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="">Department:</label>
                                                <select class="form-control m-input m-input--square" id="search-department">
                                                    <option value="">All</option>
                                                    <?php foreach ($departments as $dep): ?>
                                                        <option value="<?php echo $dep->dep_id; ?>"><?php echo $dep->dep_name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>


                                            <div class="col-md-5">
                                                <label for="">Accounts:</label>
                                                <select class="form-control m-input m-input--square" id="search-account" disabled>
                                                    <option value="">All</option>
                                                    <?php foreach ($accounts as $acc): ?>
                                                        <option value="<?php echo $acc->acc_id; ?>"><?php echo $acc->acc_name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview" style="padding:10px;">
                                        <div id="highchart-chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="tabular">

                            <div class="col-md-12">

                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview" style="padding:10px">
                                        <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="div-accordion-tabular" role="tablist">                      

                                        </div>
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo" style="height:0 !important">


                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle" >
                                                <ul id="pagination">
                                                </ul>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width:8%">
                                                <select class="form-control m-input form-control-sm m-input--air" id="perpage">
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>6</option>
                                                    <option>12</option>
                                                </select>
                                            </div>
                                            <div class="m-stack__item m-stack__item--middle m-stack__item--right" style="width:20%;font-size:12px">Showing <span id="count"></span> of <span id="total"></span> records</div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
                <!--end::Portlet-->
            </div>
        </div>

    </div>
    <div class="modal fade" id="modal-report-status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document" style="max-width:1000px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title-status">Daily Leave Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"  style="max-height:450px;overflow:auto">
                    <div id="highcharts-chart-modal" style="height: 400px; margin: 0 auto"></div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">
        var getDaysArrayByMonth = function (date) {
            var daysInMonth = moment(date).daysInMonth();
            var arrDays = [];

            while (daysInMonth) {
                var current = moment(date).date(daysInMonth);
                arrDays.push(current.format('YYYY-MM-DD'));
                daysInMonth--;
            }
            return arrDays.reverse();
        };
        var getReportStatusYear = function (callback) {
            var startyear = 2017;
            var endyear = moment().add(1, 'year').year();
            var yeardata = [];
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/Leave/get_report_status_year",
                data: {
                    startyear: startyear,
                    endyear: endyear
                }, beforeSend: function () {
                    mApp.block('#m_blockui_2_portlet', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = res.trim();
                    res = JSON.parse(res);

                    var iterator = 0;
                    $.each(res.yeardata, function (i, item) {
                        yeardata[iterator] = {
                            "name": item.year,
                            "y": parseInt(item.count),
                            "drilldown": true
                        };

                        iterator++;
                    });
                    mApp.unblock('#m_blockui_2_portlet');
                    callback(yeardata);
                }
            });
        }
        $(function () {
            var startyear = 2017;
            var endyear = moment().add(1, 'year').year();
            var years = '';
            for (var x = startyear; x <= endyear; x++) {
                if (moment().year() === x) {
                    years += '<option value="' + x + '" selected>' + x + '</option>';
                } else {
                    years += '<option value="' + x + '">' + x + '</option>';
                }
            }
            $("#pagination").pagination({
                items: 12, //default
                itemsOnPage: $("#perpage").val(),
                hrefTextPrefix: "#",
                cssStyle: 'light-theme',
                onPageClick: function (pagenumber) {
                    var perPage = $("#perpage").val();
                    changeDisplay((pagenumber * perPage) - perPage, perPage);
                }
            });
            $("#search-year").html(years);
            var months = ["", 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var monthsShort = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            getReportStatusYear(function (yeardata) {
                Highcharts.chart('highchart-chart', {
                    chart: {
                        type: 'bar',
                        events: {
                            load: function (event) {
                                this.series[0].data[1].doDrilldown();
                            },
                            drilldown: function (e) {
                                if (!e.seriesOptions) {
                                    var chart = this;
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>index.php/Leave/get_report_status_month",
                                        beforeSend: function () {
                                            chart.showLoading('Simulating Ajax ...');
                                        },
                                        data: {
                                            year: e.point.name
                                        },
                                        cache: false,
                                        success: function (res) {
                                            res = res.trim();
                                            res = JSON.parse(res);
                                            var details = [];
                                            var iterator = 0;
                                            $.each(res.monthdata, function (x, count) {
                                                var monthname = months[x];
                                                details[iterator] = [monthname, parseInt(count)];
                                                iterator++;
                                            });
                                            var arr = {
                                                name: e.point.name,
                                                data: details
                                            };
                                            chart.addSeriesAsDrilldown(e.point, arr);
                                            chart.hideLoading();
                                        }
                                    });
                                }

                            }
                        }
                    },
                    colors: ['#AED144', '#A8518A', '#DE563D', '#5FAEDF', '#F8BE38', '#F9690E', '#4285F4', '#8EB846', '#F9690E'],
                    title: {
                        text: null
                    },
                    xAxis: {
                        type: 'category'
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            cursor: 'pointer',
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true
                            },
                            point: {
                                events: {
                                    click: function () {
//                                        alert(moment(this.name+'-01-01', 'YYYY-MM-DD', true)); //for year
                                        var yearmonthdate = moment(this.series.name + '-' + this.name, 'YYYY-MMMM', true); //for month
                                        if (yearmonthdate.isValid()) {
                                            //FOR DRILLDOWN SERIES
                                            $("#modal-report-status").data('date', yearmonthdate.format('YYYY-MM'));
                                            $("#modal-report-status").modal("show");
                                        } else {
                                            //FOR MAIN SERIES
                                        }
                                    }
                                }
                            }
                        }
                    },
                    series: [{
                            name: 'Yearly',
                            colorByPoint: true,
                            data: yeardata
                        }],
                    drilldown: {
                        series: []
                    }
                });
            });
            $("#modal-report-status").on('show.bs.modal', function () {
                var date = $(this).data('date') + '-01';
                var arr = getDaysArrayByMonth(date);
                var categories = [];
                arr.forEach(function (item) {
                    categories.push(moment(item, 'YYYY-MM-DD').format('MMM DD'));
                });
                $("#modal-report-status").find('.modal-body').animate({
                    scrollTop: 0
                }, 500);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/Leave/get_report_status_details",
                    data: {
                        dateArray: arr
                    },
                    beforeSend: function () {
                        mApp.block('#m_blockui_2_portlet', {
                            overlayColor: '#000000',
                            type: 'loader',
                            state: 'brand',
                            size: 'lg'
                        });
                    },
                    cache: false,
                    success: function (res) {
                        res = res.trim();
                        res = JSON.parse(res);
                        var pending = res.details[2];
                        pending = pending.map(Number);
                        var approved = res.details[5];
                        approved = approved.map(Number);
                        var rejected = res.details[6];
                        rejected = rejected.map(Number);
                        var retracted = res.details[13];
                        retracted = retracted.map(Number);
//                        var totaldata = categories.length;
//                        var height = totaldata * 40;
//                        $("#highcharts-chart-modal").css("height", height + "px");
                        Highcharts.chart('highcharts-chart-modal', {
                            chart: {
                                type: 'column'
                            },
                            rangeSelector: {
                                selected: 1
                            },
                            legend: {
                                align: 'center',
                                verticalAlign: 'top',
                                floating: false,
                                reversed: true
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                categories: categories,
                                crosshair: true
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Leave Date Count '
                                },
                                showEmpty: false
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:14px"><b>{point.key}</b></span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    pointPadding: 0,
                                    borderWidth: 0,

                                },
                                series: {
                                    stacking: 'normal'
                                }
                            },
                            series: [{
                                    name: 'Retracted',
                                    color: '#A8518A',
//                                    visible: false,
                                    data: retracted

                                }, {
                                    name: 'Rejected',
                                    color: '#DE563D',
//                                    visible: false,
                                    data: rejected
                                }, {
                                    name: 'Approved',
                                    color: '#8EB846',
                                    data: approved
                                }, {
                                    name: 'Pending',
                                    color: '#F78C0A',
//                                    visible: false,
                                    data: pending
                                }]
                        });
                        mApp.unblock('#m_blockui_2_portlet');
                    }
                });
            });
            $("#btn-filters").click(function () {
                if ($("#filters").is(":hidden")) {
                    $("#filters").show(500);
                } else {
                    $("#filters").hide(500);
                }
            });
        });
</script>