<style type="text/css">
    .portlet-header-report{
        background:#4B4C54 !important;
        color:white  !important;
        height:50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Leave Module</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Leave/control_panel'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Summary</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <div class="m-content">  
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet">
                    <div class="m-portlet__head portlet-header-report">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="la  la-pie-chart"></i>
                                </span>
                                <h3 class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">
                                    LOA Raw Report
                                </h3>
                            </div>			
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-2">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>Year:</label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="custom-year">
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-2">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>Status:</label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="custom_leave_type">
                                            <?php foreach($leave_types as $lt):?>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-2">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label class="m-label m-label--single">Type:</label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="custom_emp_type">
                                            <option value="">All</option>
                                            <option value="Admin">SZ Team</option>
                                            <option value="Agent">Ambassador</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label class="m-label m-label--single">Search:</label>
                                    </div>
                                    <div class="m-form__control">
                                        <input class="form-control m-input" id="custom-search" placeholder="Search here ...">
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                        </div>
                        <!--begin: Datatable -->
                        <div class="m_datatable" id="raw_datatable"></div>
                        <!--end: Datatable -->
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
        function datatable_init() {

            var datatable = $('.m_datatable').mDatatable({
                // datasource definition
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            // sample GET method
                            method: 'POST',
                            url: 'get_leave_types',
                            map: function (raw) {
                                // sample data mapping
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            }
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                // layout definition
                layout: {
                    theme: 'default', // datatable theme
                    class: '', // custom wrapper class
                    scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
//                    height: 550, // datatable's body's fixed height
                    footer: false // display/hide footer
                },
                // column sorting
                sortable: true,
                pagination: true,
                toolbar: {
                    // toolbar placement can be at top or bottom or both top and bottom repeated
                    placement: ['bottom'],
                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [5, 10, 20, 30, 50, 100]
                        }
                    }
                },
                search: {
                    input: $('#custom-search')
                },
                // columns definition
                columns: [
                    {

                        field: 'leaveType',
                        title: 'Leave Type',
                        sortable: true,
                        width: 300
                    }, {
                        field: 'description',
                        title: 'Description',
                        sortable: true,
                        width: 300
                    }, {
                        field: 'Actions',
                        width: 110,
                        title: 'Actions',
                        sortable: false,
                        overflow: 'visible',
                        template: function (row, index, datatable) {
//                            row = {leaveType,leaveType_ID}
                            return '\
                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"  onclick="updateModalTrigger(' + row.leaveType_ID + ')">\
                                                        <i class="flaticon-edit-1"></i>\
                                                </a>\
                                        ';
                        }
                    }]
            });
            return datatable;
        }

        $(function () {
            var startyear = 2017;
            var endyear = moment().add(1, 'year').year();
            var years = '';
            for (var x = startyear; x <= endyear; x++) {
                if (moment().year() === x) {
                    years += '<option value="' + x + '" selected>' + x + '</option>';
                } else {
                    years += '<option value="' + x + '">' + x + '</option>';
                }
            }
            $("#custom-year").html(years);
            var datatable = datatable_init();
            $('#m_form_status').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Status');
            });

            $('#m_form_type').on('change', function () {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#custom_year, #custom_emp_type, #custom_leave_type').selectpicker();
        });
</script>