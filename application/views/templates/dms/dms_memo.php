<style type="text/css">
    pre.memoPre {
        overflow-x: auto;
        white-space: pre-wrap;
        white-space: -moz-pre-wrap;
        white-space: -pre-wrap;
        white-space: -o-pre-wrap;
        word-wrap: break-word;
        font-family: Poppins;
        font-size: 14px;
        margin-bottom: 0 !important;
    }

    #card-container td {
        font-size: 13px;
    }

    .note-toolbar-wrapper {
        height: unset !Important;
    }

    .pdfobject-container {
        width: 100%;
        height: 600px;
    }

    .embed_pdf:hover {
        box-shadow: 2px 2px 7px #777;
        z-index: 2;
        -webkit-transition: all 100ms ease-in;
        -webkit-transform: scale(1.01);
        -ms-transition: all 100ms ease-in;
        -ms-transform: scale(1.01);
        -moz-transition: all 100ms ease-in;
        -moz-transform: scale(1.01);
        transition: all 100ms ease-in;
        transform: scale(1.01);
        cursor: pointer;
    }
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Discipline Management System</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content pb-0">
        <div class="m-portlet  m-portlet--full-height" id="main-portlet">
            <div class="m-portlet__head px-3 py-2" style="height:unset">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Memos
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <p style="font-family:Poppins;font-size:13px;text-align:right;margin-top:10px">Showing <span id="count"></span> of <span id="total"></span> records</p>
                    <select class="form-control m-input form-control-sm m-input--air m--hide" id="perpage">
                        <option>15</option>
                    </select>
                </div>
            </div>
            <div class="m-portlet__body px-0 py-3">
                <div class="px-3">
                    <div id="container-memos" class="row no-gutters"></div>
                </div>
                <hr>
                <div class="px-3">
                    <ul id="pagination"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-memo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body p-0 m-0">
                <div id="container-pdf" class="m-0 p-0"></div>
            </div>
        </div>
    </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/pdfobject.js"></script>
<script type="text/javascript">
    function getAllMemos(limiter, perPage, callback) {
        var searchSubject = $("#search-subject").val();
        var searchHighlight = $("#search-highlight").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Discipline/get_all_memos",
            data: {
                limiter: limiter,
                perPage: perPage,
                searchSubject: searchSubject,
                searchHighlight: searchHighlight
            },
            beforeSend: function() {
                mApp.block('.m-content', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());

                $("#total").html(res.total);
                $("#total").data('count', res.total);
                $("#container-memos").html("");

                $.each(res.memos, function(i, memo) {
                    var string = '<div class="col-4"><div class="card m-2 embed_pdf"  data-toggle="modal" data-target="#modal-memo" data-link="<?php echo base_url(); ?>' + memo.link + '">\
                                <div class="card-body p-2">\
                                    <div class="m-stack m-stack--ver m-stack--general">\
                                        <div class="m-stack__item m-stack__item--center m-stack__item--middle  m--font-danger" style="width: 40px;">\
                                            <i class="fa fa-file-pdf-o" style="font-size:35px"></i>\
                                        </div>\
                                        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--fluid pl-2"><p class="mb-0 m--font-bolder text-truncate" style="font-size:13px;">' + memo.title + '</p><p class="mb-0 m--font-bold" style="font-size:11px">' + memo.number + '</p></div>\
                                    </div>\
                                </div>\
                            </div>\
                            </div>';
                    $("#container-memos").append(string);
                })

                var count = $("#total").data('count');
                var result = parseInt(limiter) + parseInt(perPage);
                if (count === 0) {
                    $("#count").html(0 + ' - ' + 0);
                } else if (count <= result) {
                    $("#count").html((limiter + 1) + ' - ' + count);
                } else if (limiter === 0) {
                    $("#count").html(1 + ' - ' + perPage);
                } else {
                    $("#count").html((limiter + 1) + ' - ' + result);
                }

                mApp.unblock('.m-content');
                callback(res.total);
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#content');
            }
        });
    }
    $(function() {
        $("#perpage").change();

    });
    $("#perpage").change(function() {
        var perPage = $("#perpage").val();
        getAllMemos(0, perPage, function(total) {
            $("#pagination").pagination('destroy');
            $("#pagination").pagination({
                items: total, //default
                itemsOnPage: $("#perpage").val(),
                hrefTextPrefix: "#",
                cssStyle: 'light-theme',
                displayedPages: 10,
                onPageClick: function(pagenumber) {
                    var perPage = $("#perpage").val();
                    getAllMemos((pagenumber * perPage) - perPage, perPage, function() {});
                }
            });
        });
    });
    $("#modal-memo").on('show.bs.modal', function(e) {
        if (e.namespace === 'bs.modal') {
            var link = $(e.relatedTarget).data('link');
            var options = {
                pdfOpenParams: {
                    navpanes: 0,
                    toolbar: 0,
                    statusbar: 0,
                    pagemode: 'thumbs',
                    view: "FitV"
                }
            };
            var myPDF = PDFObject.embed(link, "#container-pdf", options);
            console.log((myPDF) ? "PDFObject successfully embedded '" + link + "'!" : "Uh-oh, the embed didn't work.");
        }
    })
</script>