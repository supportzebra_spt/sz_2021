<style type="text/css">
    #qualifiedIncidentType:first-letter {
        text-transform: capitalize;
    }

    .flex-direction-nav a {
        width: 50px !important;
        height: 50px !Important;
    }

    .flex-direction-nav a:before {
        font-size: 35px !Important;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/lightbox2/dist/css/lightbox.css" />
<script src="<?php echo base_url(); ?>node_modules/lightbox2/dist/js/lightbox.js"></script>
<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/css/flexslider.css" /> -->
<!-- <script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.flexslider-min.js"></script> -->

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Discipline Management System</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <?php if ($status != 'Success') : ?>
        <div class="card">
            <div class="card-body text-center">
                <p><i class="fa 	fa-exclamation-triangle m--font-danger" style="font-size:100px;"></i></p>
                <p style="font-size:35px;font-weight:500">No record to show</p>
                <p style="font-size:25px"><?php echo $status_details; ?></p>
            </div>
        </div>
        <?php else : ?>
        <div class="m-portlet m-portlet--rounded">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text" style="font-family:Poppins;color:white !important">
                            Witness Confirmation <small style="font-family:Poppins">Incident Report</small>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!-- <div class="row px-3">
                    <div class="col-12">
                        <p style="font-size:14px;" class="text-justify">Thank you for helping us analyze this incident so that we can help prevent someone from getting hurt or sick in the future. Accuracy is very important in helping us get to the root cause of this incident.</p>
                    </div>
                </div> -->
                <div class="row px-2">
                    <div class="col-2 text-center">
                        <img id="empPicApproval" src="<?php echo $incident_report->pic; ?>" onerror="noImageFound(this)" width="70" alt="" class="rounded-circle">
                    </div>
                    <div class="col-10 text-left">
                        <p class="mb-1" style="font-size: 19px;" id="qualifiedEmployeeName"><?php echo $incident_report->subjectFname . ' ' . $incident_report->subjectLname; ?></p>
                        <p class="mb-1" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="qualifiedEmployeeJob"><?php echo $incident_report->subjectPosition . ' (' . $incident_report->subjectEmpStat . ')'; ?></p>
                        <p class="mb-1" style="font-size: 12px" id="qualifiedEmployeeAccount"><?php echo $incident_report->subjectAccount; ?></p>
                    </div>

                </div>
                <div class="px-2">
                    <div class="row">
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="row px-5">
                                <div class="col-12 col-md-3 font-weight-bold">Source:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedIncidentType"><?php echo $incident_report->src_fname . ' ' . $incident_report->src_lname; ?></div>

                                <div class="col-12 col-md-3 font-weight-bold">Date Of Incident:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedDateIncident"><?php echo date('M d, Y', strtotime($incident_report->incidentDate)); ?></div>
                                <div class="col-12 col-md-3 font-weight-bold">Time of Incident:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedTimeIncident"><?php echo date('h:i A', strtotime(date('Y-m-d ' . $incident_report->incidentTime))); ?></div>

                                <div class="col-12 col-md-3 font-weight-bold">Place Of Incident:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedPlaceIncident"><?php echo $incident_report->place; ?></div>
                                <div class="col-12 col-md-3 font-weight-bold">Incident:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedIncidentType"><?php echo $incident_report->details; ?></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Offense Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="row pl-5 pr-5">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 col-md-3 font-weight-bold">Offense Type:</div>
                                        <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"><?php echo $incident_report->offenseType; ?></div>
                                        <div class="col-12 col-md-3 font-weight-bold">Offense:</div>
                                        <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"><i style="font-size: 13px; font-weight: 400; color: #c31717;" id="qualifiedOffense"><?php echo $incident_report->offense; ?></i></div>
                                        <div class="col-12 col-md-3 font-weight-bold">Category:</div>
                                        <div class="col-12 col-md-9 pl-5 pl-md-0 text-capitalize" style="font-size: 13px; font-weight: 400" id="qualifiedCategory">Category <?php echo $incident_report->category; ?></div>
                                        <div class="col-12 pt-2 pb-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Evidences</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="m-accordion m-accordion--default m-accordion--toggle-arrow m-accordion--solid mx-3" id="accordion-evidence" role="tablist">

                                <!--begin::Item-->
                                <div class="m-accordion__item">
                                    <div class="m-accordion__item-head collapsed" srole="tab" id="accordion-evidence-head" data-toggle="collapse" href="#accordion-evidence-body" aria-expanded="true">
                                        <span class="m-accordion__item-icon"><i class="fa flaticon-interface-2"></i></span>
                                        <span class="m-accordion__item-title">Show Evidences</span>

                                        <span class="m-accordion__item-mode"></span>
                                    </div>

                                    <div class="m-accordion__item-body collapse" id="accordion-evidence-body" role="tabpanel" aria-labelledby="accordion-evidence-head" data-parent="#accordion-evidence" style="">
                                        <div class="m-accordion__item-content">
                                            <div class="row">
                                                <?php foreach ($incident_report->evidences as $key => $mediaTypes) : ?>

                                                <?php if ($key == 'images') : ?>
                                                <?php
                                                            $imgcnt = 0;
                                                            foreach ($incident_report->evidences[$key] as $evidence) :
                                                                $exploded = explode('/', $evidence->link);

                                                                ?>
                                                <div class="col-6 col-sm-4 col-md-3 col-xl-2 my-3" style="height:160px">
                                                    <a class="m-link" href="<?php echo base_url() . $evidence->link; ?>" data-lightbox="roadtrip" data-toggle="m-popover" data-placement="top" data-skin="dark" data-content="<?php echo end($exploded); ?>" data-original-title="" title="">
                                                        <img style="height: 140px; width: 100%; object-fit: cover" src="<?php echo base_url() . $evidence->link; ?>" />

                                                    </a>
                                                    <p class="mt-2 cut-text m--font-bold" style="color:#6875df;font-size: 13px;"><?php echo end($exploded); ?></p>
                                                </div>
                                                <?php
                                                                $imgcnt++;
                                                            endforeach;
                                                            ?>
                                                <?php elseif ($key == 'videos') : ?>

                                                <?php foreach ($incident_report->evidences[$key] as $evidence) :
                                                                $exploded = explode('/', $evidence->link);
                                                                ?>

                                                <div class="col-6 col-sm-4 col-md-3 col-xl-2 my-3" style="height:160px">
                                                    <video controls style="object-fit:scale-down;width:100%;height:140px;background:black;" data-toggle="m-popover" data-placement="top" data-skin="dark" data-content="<?php echo end($exploded); ?>" data-original-title="" title="">
                                                        <source src="<?php echo base_url() . $evidence->link; ?>" type="video/<?php $output = explode(".", $evidence->link);
                                                                                                                                                echo $output[count($output) - 1]; ?>">
                                                        Your browser does not support HTML5 video.
                                                    </video>
                                                    <p class="mt-0 cut-text m--font-bold" style="color:#6875df;font-size: 13px;"><?php echo end($exploded); ?></p>
                                                </div>
                                                <?php endforeach; ?>
                                                <?php elseif ($key == 'audios') : ?>
                                                <?php foreach ($incident_report->evidences[$key] as $evidence) :
                                                                $exploded = explode('/', $evidence->link); ?>
                                                <div class="col-6 col-sm-4 col-md-3 col-xl-2 my-3 text-center" style="height:160px">
                                                    <!-- <audio controls>
                                        <source src="<?php echo base_url() . $evidence->link; ?>" type="audio/<?php echo  $output = explode(".", $evidence->link);
                                                                                                                                $output[count($output) - 1]; ?>">
                                        Your browser does not support HTML5 video.
                                    </audio>  -->
                                                    <a class="m-link witness-audio" data-audiolink="<?php echo base_url() . $evidence->link; ?>" href="<?php echo base_url() . $evidence->link; ?>" style="height:140px;width:100%" data-toggle="m-popover" data-placement="top" data-skin="dark" data-content="<?php echo end($exploded); ?>" data-original-title="" title="">
                                                        <i class="fa fa-music" style="font-size: 140px; color: #bb80a9;"></i>
                                                    </a>
                                                    <p class="mt-2 cut-text m--font-bold text-left" style="color:#6875df;font-size: 13px;"><?php echo end($exploded); ?></p>
                                                </div>
                                                <?php endforeach; ?>
                                                <?php elseif ($key == 'text') : ?>
                                                <?php foreach ($incident_report->evidences[$key] as $evidence) :
                                                                $exploded = explode('/', $evidence->link);
                                                                ?>
                                                <div class="col-6 col-sm-4 col-md-3 col-xl-2 my-3 text-center" style="height:160px">
                                                    <a class="m-link" href="<?php echo base_url() . $evidence->link; ?>" target="_blank" style="height:140px;width:100%" data-toggle="m-popover" data-placement="top" data-skin="dark" data-content="<?php echo end($exploded); ?>" data-original-title="" title="">
                                                        <i class="fa fa-file" style="font-size: 140px; color: cadetblue;"></i></a>
                                                    <p class="mt-2 cut-text m--font-bold text-left" style="color:#6875df;font-size: 13px;"><?php echo end($exploded); ?></p>
                                                </div>
                                                <?php endforeach; ?>
                                                <?php elseif ($key == 'others') : ?>
                                                <?php foreach ($incident_report->evidences[$key] as $evidence) :
                                                                $exploded = explode('/', $evidence->link);
                                                                ?>
                                                <div class="col-6 col-sm-4 col-md-3 col-xl-2 my-3 text-center" style="height:160px">
                                                    <a class="m-link" href="<?php echo base_url() . $evidence->link; ?>" target="_blank" style="height:140px;width:100%" data-toggle="m-popover" data-placement="top" data-skin="dark" data-content="<?php echo end($exploded); ?>" data-original-title="" title="">
                                                        <i class="fa fa-file-word-o" style="font-size: 140px; color: cadetblue;"></i></a>
                                                    <p class="mt-2 cut-text m--font-bold text-left" style="color:#6875df;font-size: 13px;"><?php echo end($exploded); ?></p>
                                                </div>
                                                <?php endforeach; ?>
                                                <?php endif; ?>

                                                <?php endforeach; ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Item-->

                            </div>


                        </div>
                    </div>
                </div>

            </div>
            <div class="m-portlet__foot" style="background: #2c2e3eeb;color:white">
                <div class="row">
                    <div class="col-12 px-3">
                        <p style="font-size:14px" class="text-justify">Your confirmation of this incident report can only be done once. You might or might not be able to gain access to this report once confirmed or the incident report has been completed.</p>
                        <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--wide" id="btn-acknowledge" data-statusid="24">
                            <span>
                                <i class="fa fa-check-circle"></i>
                                <span>Acknowledge</span>
                            </span>
                        </a>

                        <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--wide" id="btn-renounce" data-statusid="25">
                            <span>
                                <i class="fa fa-times-circle"></i>
                                <span>Renounce</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php endif; ?>
</div>
</div>
<script type="text/javascript">
    $(function() {
        // $('#carousel').flexslider({
        //     animation: "slide",
        //     controlNav: false,
        //     animationLoop: false,
        //     slideshow: false,
        //     itemWidth: 210,
        //     itemMargin: 5
        // });
        $("#btn-acknowledge,#btn-renounce").click(function(e) {
            e.preventDefault();
            var that = this;
            var status_ID = $(that).data('statusid');
            var incidentReport_ID = "<?php echo $ir_id; ?>";
            if (status_ID === 24) {
                var result_str = 'acknowledge <br>incident report ? ';
                var helper = ' This means that you agree to the incident';
                var btnStr = "Acknowledge";
                var colorStr = '#f4516c';
            } else {
                var result_str = 'renounce <br>incident report ?';
                var helper = ' This means that you disagree to the incident';
                var btnStr = "Renounce";
                var colorStr = '#34bfa3';
            }
            swal({
                title: 'Are you sure to ' + result_str,
                html: "<span class='m--font-brand' style='font-size:12px'>" + helper + "</span><br>Enter your password for confirmation:",
                input: 'password',
                type: 'warning',
                allowOutsideClick: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonText: btnStr,
                confirmButtonColor: colorStr
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Employee/allowDirectoryAccess/personal",
                        data: {
                            password: result.value
                        }
                    }).done(function(res) {
                        res = JSON.parse(res.trim());
                        if (res.status === "Success") {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>Discipline/save_witness_confirmation",
                                data: {
                                    incidentReport_ID: incidentReport_ID,
                                    status_ID: status_ID
                                },
                                cache: false,
                                success: function(res) {
                                    res = JSON.parse(res.trim());

                                    if (res.status === 'Success') {
                                        if (status_ID === 24) {
                                            swal("Acknowledged!", "Witness confirmation is complete!", "success");
                                        } else {
                                            swal("Renounced!", "Witness confirmation is complete!", "success");
                                        }

                                        systemNotification([res.systemNotification_ID], 'dms');
                                        $("#btn-acknowledge,#btn-renounce").remove();
                                        location.reload();
                                    } else {
                                        swal("Failed!", "Something happened while submitting your confirmation as witness!", "error");
                                        location.reload();
                                    }

                                }
                            });
                        } else {
                            swal('WRONG PASSWORD', 'Only allowed personnel can access this feature', 'error');
                        }
                    }).fail(function(errordata) {
                        swal('WRONG PASSWORD', 'Only allowed personnel can access this feature', 'error');
                    });
                }
            });

        });

    });
    $('.witness-audio').on('click', function(e) {
        e.preventDefault();
        var link = $(this).data('audiolink');

        var audio_stringxx = "";
        audio_stringxx += "<audio controls style='width: -webkit-fill-available;'>";
        audio_stringxx += "<source src='" + link + "' type='audio/mp3'>";
        audio_stringxx += "<source src='" + link + "' type='audio/mpeg'>";
        audio_stringxx += "<source src='" + link + "' type='audio/mpeg'>";
        audio_stringxx += "</audio>";
        $("#audio_evidence_player").html(audio_stringxx);
        $("#audio_modal_evidences").modal('show');
    });
</script>