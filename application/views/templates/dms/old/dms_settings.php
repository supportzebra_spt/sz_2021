<style>
.bs-popover-auto[x-placement^=right] .arrow::after, .bs-popover-right .arrow::after {
border-right-color: #282A3C !important;
}
.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
border-left-color: #282A3C !important;
}
.popover-header{
background-color: rgb(52, 55, 78)!important;
border-bottom: 0px !important;
color: white;
max-width: 100% !important;
}
.popover-body{
background-color: #282A3C!important;
max-width: 100% !important;
padding: 1.0rem 0.5rem !important;
text-align: center;
padding-bottom: 1px !important;
}
.list-active{
    background: #dcdcdc;
}

.active-nav-a{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell{
    background:unset !important;
}
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover{
    background:#F0F0F2 !important;
    color: black !important;
}

.codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
    color: #8c8c8c;
}

.m-nav.m-nav--inline>.m-nav__item:first-child {
    padding-left: 20px !important;
}

.dms.m-nav.m-nav--inline>.m-nav__item {
    padding: 10px 20px 10px 20px !important;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active {
    background: #d4d7da;
}
.dms.m-nav.m-nav--inline>.m-nav__item:hover {
    background: #ececec;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
    color:#4f668a;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
    color: black;
}
.m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
    height: 100%;
    padding: 0.8rem 0 0.8rem 0;
}

.m-portlet .m-portlet__head {
    height: 3.9rem;
}

.m-portlet.m-portlet--tabs.active{
    display:block;
}
.dtrAutoIrSideTab{
    padding-left: 0.5rem !important; 
    padding-right: 0.5rem !important;
    padding-top: 0.8rem !important;
    padding-bottom: 0.8rem !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__clear {
    margin-right: 1px !important;
}


.editableStyle{
    border-bottom: dashed 1px #0088cc;
    cursor: pointer;
}
/* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
/* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

.select2-container--default .select2-selection--single .select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem !important;
    white-space: normal;
}

.select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
    top: 47% !important;
}


</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Discipline Managment System Control Panel</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">DMS Control Panel</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
             <div class="m-demo__preview" style="padding:0px !important">
                 <ul class="dms m-nav m-nav--inline">
                     <li class="m-nav__item active">
                         <a href="#" class="dmsMainNav m-nav__link active" data-navcontent="codSettings">
                             <i class="m-nav__link-icon fa fa-balance-scale"></i>
                             <span class="m-nav__link-text">COD Settings</span>
                         </a>
                     </li>
                     <li class="m-nav__item">
                         <a href="#" class="dmsMainNav m-nav__link" data-navcontent="dtrSettings" onclick="initAutomationTabContent()">
                             <i class="m-nav__link-icon fa fa-gears"></i>
                             <span class="m-nav__link-text">Automation Settings</span>
                             <!-- <span class="m-nav__link-badge">
                                 <span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span>
                             </span> -->
                         </a>
                     </li>
                     <li class="m-nav__item">
                         <a href="#" class="dmsMainNav m-nav__link" data-navcontent="assignmentSettings" onclick="initAssignmentTabContent()">
                             <i class="m-nav__link-icon fa fa-tags"></i>
                             <span class="m-nav__link-text">Assignment</span>
                         </a>
                     </li>
                     <li class="m-nav__item">
                         <a href="#" class="dmsMainNav m-nav__link" data-navcontent="memoSettings">
                             <i class="m-nav__link-icon fa fa-newspaper-o"></i>
                             <span class="m-nav__link-text">Memos</span>
                             <!-- <span class="m-nav__link-badge">
                                 <span class="m-badge m-badge--success m-badge--wide">23</span>
                             </span> -->
                         </a>
                     </li>
                 </ul>
             </div>
         </div>
        <div class="m-portlet m-portlet--tabs active" id="codSettings">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                         <li class="nav-item m-tabs__item">
                            <a id="disciplinaryActionTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#disciplinaryAction" role="tab">
                                <i class="fa fa-balance-scale"></i>Disciplanary Action</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseCategoryTab" class="nav-link m-tabs__link" data-toggle="tab" href="#offenseCategory" role="tab">
                                <i class="fa fa-list-alt"></i>Category of Offenses</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab" href="#offense" role="tab">
                                <i class="fa fa-legal"></i>Offenses</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">                   
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="disciplinaryAction" role="tabpanel">
                        <div class="row" id="noDisciplinaryAction" style="display:none; margin-right: 0px; margin-left: 0px;">
                            <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                <div  class="col-md-12 text-center">
                                    <div class="m-demo-icon__preview">
                                        <i style="font-size: 40px;" class="flaticon-notes"></i>
                                    </div>
                                    <span class="m-demo-icon__class">No Disciplinary Action was set</span>
                                    <div class="col-12">
                                        <a href="#" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-target="#disciplinaryActionModal" transacton="add">
                                            <span>
                                                <i class="fa fa-plus-circle"></i>
                                                <span>Add a Disciplinary Action</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="disciplinaryActionTableContainer" style="display:none">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="disciplinaryActionTable">
                                        <thead style="background:#d8d8d8;">
                                            <tr class="d-flex">
                                                <th class="text-center col-5" style="word-break: break-word;">Disciplinary Action
                                                    <button type="btn" class="ml-3 btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air pull-right" data-toggle="modal" data-target="#disciplinaryActionModal" transacton="add" style="width: 22px !important;height: 22px !important;">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </th>
                                                <th class="text-center col-2" style="word-break: break-word;">Abbreviation</th>
                                                <th class="text-center col-3" style="word-break: break-word;">Prescriptive Period (Months)</th>
                                                <th class="text-center col-2" style="word-break: break-word;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="disciplinaryActionTableBody"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="tab-pane fade" id="offenseCategory" role="tabpanel">
                        <div class="row" id="noOffenseCategory" style="display:none; margin-right: 0px; margin-left: 0px;">
                            <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                <div class="col-md-12 text-center">
                                    <div class="m-demo-icon__preview">
                                        <i style="font-size: 40px;" class="flaticon-notes"></i>
                                    </div>
                                    <span class="m-demo-icon__class">No Offense Category was set</span>
                                    <div class="col-12">
                                        <a href="#" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" onclick="getMaxCategory()" transacton="add">
                                            <span>
                                                <i class="fa fa-plus-circle"></i>
                                                <span>Add an Offense Category</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="OffenseCategoryTableContainer">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="OffenseCategoryTable">
                                        <thead>
                                            <tr class="" style="background:#d8d8d8;border: 3px solid #b5b2b2 !important;">
                                                <th class="text-center align-middle">Category 
                                                    <button type="btn" class="ml-3 btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air pull-right" aria-haspopup="true" aria-expanded="false" onclick="getMaxCategory()" transacton="add" style="width: 22px !important;height: 22px !important;">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </th>
                                                <th class="text-center align-middle">Offense Level</th>
                                                <th class="text-center align-middle" colspan="2">Disciplinary Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="OffenseCategoryTableBody"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="offense" role="tabpanel">
                        <div class="row" id="noSpecificOffenseCategory" style="display:none;margin-right: 0px; margin-left: 0px;">
                            <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                <div  class="col-md-12 text-center">
                                    <div class="m-demo-icon__preview">
                                        <i style="font-size: 40px;" class="flaticon-notes"></i>
                                    </div>
                                    <span class="m-demo-icon__class">No Specific Offense Type was set</span>
                                    <div class="col-12">
                                        <a href="#" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" onclick="getMaxOffenseCategory()">
                                            <span>
                                                <i class="fa fa-plus-circle"></i>
                                                <span>Add an Offense Type</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="specificOffenseCategoryDiv">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="margin-top: -2.3rem !important;margin-left: -1.1rem !important;margin-bottom: -2.2rem !important;padding-top: 2rem;border-right: 1px solid #dedede;background: #c1c2c73d;">
                                <div class="m-demo__preview" id="offenseType" style="margin-left: -11px;">
                                    <div class="form-group m-form__group" id="addOffenseCategoryDiv" style="margin-top: -2rem;padding-top: 0.8rem;margin-left: -0.4rem;padding-left: 0.7rem;margin-right: -1.1rem;padding-right: 0.7rem;padding-bottom: 0.8rem;">
                                        <div class="input-group">
                                            <div class="input-group-append d-none d-sm-block" style="margin-left: 1px;padding-top: 0.6rem;padding-bottom: 0.1rem;padding-right: 0rem;margin-bottom: 0.1rem;" id="showSearchOffenseCategoryBtn">
                                                <span class="input-group-text" id="basic-addon2" style="border-color: #ded8d8;background-color: #ffffff;padding-left: 0.9rem;padding-right: 0.9rem;padding-bottom: 0.56rem;padding-top: 0.55rem;margin-right: -0.4rem;">
                                                    <i class="fa fa-search"></i>
                                                </span>
                                            </div>
                                            <a id="addOffenseCategoryBtn1" href="#" class="col btn btn-success m-btn m-btn--icon m-btn--wide d-none d-sm-block" style="border-radius: unset !important;" onclick="getMaxOffenseCategory()">
                                                <span class="col-12">
                                                    <span class="text-truncate"><i class="fa fa-plus mr-2"></i>Add Offense Type</span>
                                                </span>
                                            </a>
                                            <a id="addOffenseCategoryBtn2 href="#" class="btn btn-success m-btn m-btn--icon btn-lg m-btn--icon-only d-block d-sm-none" style="width: 51px;height: 34px;" onclick="getMaxOffenseCategory()">
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                        <!-- <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data shown in the table below.</span> -->
                                    </div>
                                    <div class="form-group m-form__group" id="searchOffenseCategory" style="display:none;margin-top: -2rem;padding-top: 0.8rem;margin-left: -0.4rem;padding-left: 0.7rem;margin-right: -1.1rem;padding-right: 0.7rem;padding-bottom: 0.8rem;">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Offense Type" id="searchOffenseCategoryVal">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="closeSearchOffenseCategoryBtn">
                                                    <i class="fa fa-close"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="m-nav" id="specOffenseCategoryList" style="margin-left: -0.3rem;margin-right: -1.2rem;margin-top: -1rem;">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8" style="margin-top: -2.3rem;padding-top: 2rem;padding-left: 2rem !important;padding-right: 0.2rem !important;margin-right: -4rem !important;margin-bottom: -1rem;" >
                                <div class="form-group m-form__group" id="OffenseDetails" style="height: 100% !important;">
                                    <div class="row" id="noOffenses" style="display:none; margin-right: 0px; margin-left: 0px;height: 89%;margin-bottom: 9rem;">
                                        <div class="col-md-12 p-0" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display: flex;align-items:center;">
                                            <div  class="col-md-12 p-0 text-center">
                                                <div class="m-demo-icon__preview">
                                                    <i style="font-size: 5rem;" class="flaticon-notes"></i>
                                                </div>
                                                <span class="m-demo-icon__class">No Offenses was set</span>
                                                <div class="col-12 p-0">
                                                    <button type="button" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" onclick="initOffenseModal()">
                                                        <span>
                                                            <i class="fa fa-plus-circle"></i>
                                                            <span>Add an Offense Description</span>
                                                        </span>
                                                    </button>
                                                </div>
                                                <span>OR</span>
                                                <div class="col-12 p-0">
                                                    <button type="button" id="emptyOffenseTypeDeleteBtn" class="ml-3 mt-2 btn btn-outline-danger btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" onclick="confirmDeleteSpecificOffenseCategory(this)">
                                                        <span>
                                                            <i class="fa fa-remove"></i>
                                                            <span>Remove Offense Type</span>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="noOffenseType" style="display:none; margin-right: 0px; margin-left: 0px;height: 89%;margin-bottom: 9rem;">
                                        <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display: flex;align-items:center;">
                                            <div  class="col-md-12 text-center">
                                                <div class="m-demo-icon__preview">
                                                    <i style="font-size: 5rem;" class="flaticon-notes"></i>
                                                </div>
                                                <span class="m-demo-icon__class" id="offenseSearchVal" style="font-size: 20px;word-break: break-word;"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="offenseDiv">
                                        <button type="button" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only pull-right" id="deleteOffenseTypeBtn" onclick="confirmDeleteSpecificOffenseCategory(this)">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                        <button type="button" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only pull-right mr-1" id="editOffenseTypeBtn" onclick="getSpecificOffenseCategory(this)">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <h2 class="text-center" id="offenseCategoryLabel" style="word-wrap: break-word;"></h2>
                                        <p id="offenseOptions" class="text-center" style="border-bottom: 1px solid #d5d7da !important;padding-bottom: 0.9rem !important;"><a href="#" onclick="initOffenseModal()">Add an Offense</a>  | <a id="searchOffense" href="#" onmouseover="initOffenseSelectDiscCateg()">Search Offense</a></p>
                                        <div class="row" id="offenseSearch" style="display:none">
                                            <div class="col-lg-12 input-group mb-3">
                                                <input type="text" class="form-control" placeholder="" id="offenseSearchField">
                                                <div class="input-group-append">
                                                    <span class="input-group-text closeOffenseSearchBtn">
                                                        <i class="fa fa-close"></i>
                                                    </span>
                                                </div>
                                            </div> 
                                            <!-- <div class="col-lg-3 input-group mb-3">
                                                <select class="custom-select form-control m-input" id="offenseCategorySelect" name="offenseCategorySelect">
                                                </select>
                                                 <div class="input-group-append">
                                                    <span class="input-group-text closeOffenseSearchBtn">
                                                        <i class="fa fa-close"></i>
                                                    </span>
                                                </div>
                                            </div> -->
                                        </div>
                                        <div id="offenseDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="dtrSettings" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                         <li class="nav-item m-tabs__item">
                            <a id="dtrAutIrTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#dtrAutIr" role="tab">
                                <i class="fa fa-clock-o"></i>DTR Violation Auto IR settings</a>
                        </li>
                         <li class="nav-item m-tabs__item">
                            <a id="deadlineTab" class="nav-link m-tabs__link" data-toggle="tab" href="#deadline" role="tab">
                                <i class="fa fa-hourglass-half"></i>IR Deadlines</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">                   
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="dtrAutIr" role="tabpanel">
                        <div class="row" id="dtrAutIrContainer">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="margin-top: -2.3rem !important;margin-left: -1.1rem !important;margin-bottom: -2.2rem !important;padding-top: 1rem;border-right: 1px solid #dedede;background: #c1c2c73d;">
                                <div class="m-demo__preview" id="offenseType" style="margin-left: -11px;">
                                    <ul class="m-nav" id="dtrViolations" style="margin-left: -0.3rem;margin-right: -1.2rem;margin-top: -1rem;">
                                        <li class="dtrAutoIrSideTab m-nav__item list-active" data-violationtypeid="1">
                                            <a class="m-nav__link active pl-sm-3" style="">
                                                <span class="m-nav__link-icon active-nav-a text-center text-sm-left">
                                                    <i class="fa fa-bell" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Late"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block active-nav-a pl-3 pt-2" style="padding-top: 0.3rem;padding-bottom: 0.1rem;">Late</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="4">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-tachometer" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Over Break"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Over Break</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="2">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-hourglass-3" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Undertime"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">UnderTime</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="3">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-unlock-alt" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Forgot to Logout"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Forgot to Logout</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="6">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-history" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Incomplete Break"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Incomplete Break</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="7">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-clock-o" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Incomplete DTR Logs"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Incomplete DTR Logs</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="5">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-user-times" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Absent"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Absent</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8" id="dtrVioAutIrContainer" style="margin-top: -2.3rem;padding-top: 2rem;padding-left: 2rem !important;padding-right: 0.2rem !important;margin-right: -4rem !important;margin-bottom: -1rem;" >
                                <h3 class="text-center" style="color:#4e4e4e;">Auto IR Settings for <span id="violationTypeVal">LATE</span></h3>
                                <div id="dtr3rule">
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Auto IR Notification</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-checkbox" style="font-size: 15px;font-weight: 400;">
                                                        <input type="checkbox" name="oneTimeOccChkbox" id="oneTimeOccChkbox" data-field="oneTimeStat">
                                                        One Time Occurence
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Time duration:
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize" style="font-size: 1.4rem;" id="oneTimeVal">0 Minutes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-checkbox" style="font-size: 15px;font-weight: 400;">
                                                        <input type="checkbox" name="consecOccChkbox" id="consecOccChkbox" data-field="consecutiveStat">
                                                        Consecutive Occurence
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Number of occurence regardless of Time duration:
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize" style="font-size: 1.4rem;" id="consecutiveVal">0 Times</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-checkbox" style="font-size: 15px;font-weight: 400;">
                                                        <input type="checkbox" name="nonConsecOccChkbox" id="nonConsecOccChkbox" data-field="nonConsecutiveStat">
                                                        Non-Consecutive Occurence
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Accumulated Time:
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize"  style="font-size: 1.4rem;" id="nonConsecutiveVal">0 Minutes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="incompleteLogsRule">
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Auto IR Notification</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-checkbox" style="font-size: 15px;font-weight: 400;">
                                                        <input type="checkbox" name="incOccurence" id="incOccurence" data-field="nonConsecutiveStat">
                                                        Number of Occurence
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Occurence regardless if consecutive or not
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize" style="font-size: 1.4rem;" id="nonConsecutiveValInc">0 Times</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="prescriptiveRule">
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Prescriptive Period</span>
                                        <div class="col">
                                            <hr style="margin-top: 0.8rem !important;">
                                        </div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowDtrPrescriptive">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" name="dtrPrescriptive" value="1">
                                                        Fixed Durations
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Fixed Refreshment Period:
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize prescriptiveValue" style="font-size: 1.4rem;" id="fixedDurationsVal">12 Months</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowDtrPrescriptive">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" name="dtrPrescriptive" value="2">
                                                        Custom Durations
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Custom Refreshment Period (On Occurence):
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize prescriptiveValue text-capitalize" style="font-size: 1.4rem;" id="customDurationsVal">60 days</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowDtrPrescriptive">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" name="dtrPrescriptive" value="3">
                                                        COD Prescriptive Period
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Follow the set COD Prescriptive Period:
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <span style="font-size: 1.4rem;" class="prescriptiveValue" id="customDurationsVal">COD</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="offenseTagRule">
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Default Offense Tag</span>
                                        <div class="col">
                                            <hr style="margin-top: 0.8rem !important;">
                                        </div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 pl-0">
                                                <div class="col" style="padding-left: 3.3rem!important;font-size: 15px;font-weight: 400;" id="offenseTypeVal">
                                                    <!-- Offenses Against Public Health and Safety -->
                                                </div>
                                                <div class="col mt-2" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    <i style="font-size: 13px; font-weight: 400; color: #c31717;" id="offenseVal">
                                                    <!-- "Absence Without Leave(AWOL) per day-Absence without permission; Absent without notice; Absence denial of permission to take leave" -->
                                                    </i>
                                                </div>
                                               
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a onclick="initDefaultOffenseModal()" class="editableStyle" style="font-size: 1rem;"><i class="fa fa-pencil"></i> Edit</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-9 col-lg-10 col-xl-10 pl-0">
                                                <div class="col" style="font-size: 15px;font-weight: 500;padding-left: 3.3rem!important;" id="categoryVal">
                                                <!-- Category A, B (depending on the gravity of the offense) -->
                                                <!-- Category A -->
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-3 col-lg-2 col-xl-2" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a onclick="initDefaultCategoryModal()" class="editableStyle" style="font-size: 1rem;"><i class="fa fa-pencil"></i> Edit</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="deadline" role="tabpanel">
                        <div class="row" id="deadlineContainer">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="margin-top: -2.3rem !important;margin-left: -1.1rem !important;margin-bottom: -2.2rem !important;padding-top: 1rem;border-right: 1px solid #dedede;background: #c1c2c73d;">
                                <div class="m-demo__preview" id="deadlineOptions" style="margin-left: -11px;">
                                    <ul class="m-nav" id="deadlineList" style="margin-left: -0.3rem;margin-right: -1.2rem;margin-top: -1rem;">
                                        <li class="deadlineSideTab m-nav__item list-active" data-deadlinetype="1">
                                            <a class="m-nav__link active pl-sm-3" style="">
                                                <span class="m-nav__link-icon active-nav-a text-center text-sm-left">
                                                    <i class="fa fa-clock-o" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Filter DTR Violation IR"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block active-nav-a pl-3 pt-2" style="padding-top: 0.3rem;padding-bottom: 0.1rem;">File DTR Violation IR</span>
                                            </a>
                                        </li>
                                        <li class="deadlineSideTab m-nav__item" data-deadlinetype="2">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-pencil-square-o" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Deadline to Explain"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Deadline to Explain</span>
                                            </a>
                                        </li>
                                        <li class="deadlineSideTab m-nav__item" data-deadlinetype="3">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-group" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Deadline to Recommend"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Deadline to Recommend</span>
                                            </a>
                                        </li>
                                        <li class="deadlineSideTab m-nav__item" data-deadlinetype="4">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-legal" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Deadline for Last Recommendation"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Deadline for Last Recommendation</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8" style="margin-top: -2.3rem;padding-top: 2rem;padding-left: 2rem !important;padding-right: 0.2rem !important;margin-right: -4rem !important;margin-bottom: -1rem;" >
                                <div id="deadlineDtr" class="deadlineContent" style="">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h3 class="" style="color:#4e4e4e;">Deadline for <span id="deadlineviolationType" class="text-capitalize">Late</span></h3>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="custom-select form-control" id="deadlineDtrViolation" name="deadlineDtrViolation" placeholder="Select a violation type">
                                                <option value="1">Late</option>
                                                <option value="4">Over Break</option>
                                                <option value="2">Undertime</option>
                                                <option value="3">Forgot to Log Out</option>
                                                <option value="6">Incomplete Break</option>
                                                <option value="7">Incomplete DTR Logs</option>
                                                <option value="5">Absent</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Deadline Duration</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="font-size: 1.3rem; padding-left: 3.3rem!important">
                                                    Deadline
                                                </div>
                                                <!-- <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important"></div> -->
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize editableStyle" style="font-size: 1.4rem;" id="dtrDeadlineVal">0 Days</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Missed Deadline Options</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row dtrRowMissedOption">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" id="dtrViolationMissedOption" name="dtrViolationMissedOption" value="1">
                                                        Proceed to Next level Supervisor
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Will set status to Missed and Proceed to Next level Supervisor:
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row dtrRowMissedOption">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" id="dtrViolationMissedOption" name="dtrViolationMissedOption" value="2">
                                                        Set as a Reminder
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Not set status to Missed, but will notify as a reminder.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="deadlineExplain" class="deadlineContent" style="display:none">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h3 class="text-center" style="color:#4e4e4e;">Deadline to Explain</h3>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Deadline Duration</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="font-size: 1.3rem; padding-left: 3.3rem!important">
                                                    Deadline
                                                </div>
                                                <!-- <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important"></div> -->
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize editableStyle" style="font-size: 1.4rem;" id="deadlineExplainVal">0 Days</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Missed Deadline Options</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row explainRowMissedOption">
                                            <div class="col-12 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" id="explainMissedOption" name="explainMissedOption" value="1" data-field="deadlineToExplainOption" data-deadlinetype="2">
                                                        Proceed to Recommendation
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Will set status to Missed and Proceed to Recommendation Process:
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row explainRowMissedOption">
                                            <div class="col-12 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" id="explainMissedOption" name="explainMissedOption" value="2" data-field="deadlineToExplainOption" data-deadlinetype="2">
                                                        Allow Supervisor to skip Explanation
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Direct supervisor will specify if the Subject intentionally delays his or her explanation
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="deadlineRecommend" class="deadlineContent" style="display:none">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h3 class="text-center" style="color:#4e4e4e;">Deadline to Recommend</h3>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Deadline Duration</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="font-size: 1.3rem; padding-left: 3.3rem!important">
                                                    Deadline for Each Recommending Supervisor
                                                </div>
                                                <!-- <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important"></div> -->
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize editableStyle" style="font-size: 1.4rem;" id="deadlineToRecommendVal">0 Days</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Recommendation Flow</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row recommendationFlow">
                                            <div class="col-12 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" name="deadlineToRecommendOption" value="1" data-field="deadlineToRecommendOption" data-deadlinetype="3">
                                                        Sequential Flow
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Recommendation in a Sequential Manner
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row recommendationFlow">
                                            <div class="col-12 pl-0">
                                                <div class="col" style="">
                                                    <label class="m-radio m-radio--solid m-radio--success" style="font-size: 15px;font-weight: 400;">
                                                        <input type="radio" name="deadlineToRecommendOption" value="2" data-field="deadlineToRecommendOption" data-deadlinetype="3">
                                                        Parallel Flow
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Recommendation can be done concurrently
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="deadlineLastRecommend" class="deadlineContent" style="display:none">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h3 class="text-center" style="color:#4e4e4e;">Deadline For Last Recommendation</h3>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Deadline Duration</span>
                                        <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                                    </div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="font-size: 1.3rem; padding-left: 3.3rem!important">
                                                    Last Recommendation deadline
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Employee Relation's Officer Deadline for Last Recommendation
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize editableStyle" style="font-size: 1.4rem;" id="deadlineLastRecommendationVal">0 Days</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="font-size: 1.3rem; padding-left: 3.3rem!important">
                                                    Deadline for Final Decision
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    For Cases where Client's approval is necessary
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize editableStyle" style="font-size: 1.4rem;" id="deadlineFinalDecisionVal">0 Days</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                    <div style="margin-top:1rem; margin-bottom:1rem;">
                                        <div class="row rowCheckBox">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0">
                                                <div class="col" style="font-size: 1.3rem; padding-left: 3.3rem!important">
                                                    Deadline to Upload Necessary Documents
                                                </div>
                                                <div class="col comment" style="font-size: 13px;padding-left: 3.3rem!important">
                                                    Documents such as NTE or NOR
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                <div class="col-12 pr-0 text-right">
                                                    <a class="text-capitalize editableStyle" style="font-size: 1.4rem;" id="deadlineUploadDocsVal">0 Days</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="assignmentSettings" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                         <li class="nav-item m-tabs__item">
                            <a id="assignmentsTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#assignments" role="tab">
                                <i class="fa fa-paperclip"></i>DMS Assignments</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body pl-3 pr-3">                   
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="assignments" role="tabpanel">
                        <div class="col-12">
                        <div class="row" id="assignmentContainer">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="margin-top: -2.3rem !important;margin-left: -1.1rem !important;margin-bottom: -2.2rem !important;padding-top: 1rem;border-right: 1px solid #dedede;background: #c1c2c73d;">
                                <div class="m-demo__preview" id="assignmentOptions" style="margin-left: -11px;">
                                    <ul class="m-nav" id="assignmentList" style="margin-left: -0.3rem;margin-right: -1.2rem;margin-top: -1rem;">
                                        <li class="assignmentSideTab m-nav__item list-active" data-assignmenttype="1">
                                            <a class="m-nav__link active pl-sm-3" style="">
                                                <span class="m-nav__link-icon active-nav-a text-center text-sm-left">
                                                    <i class="fa fa-user" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Employee Relations Officer"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block active-nav-a pl-3 pt-2" style="padding-top: 0.3rem;padding-bottom: 0.1rem;">Employee Relations Officer</span>
                                            </a>
                                        </li>
                                        <li class="assignmentSideTab m-nav__item" data-assignmenttype="2">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-legal" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Offense Type Assignment"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Offense Type Assignment</span>
                                            </a>
                                        </li>
                                        <li class="assignmentSideTab m-nav__item" data-assignmenttype="3">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-tags" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="QIP Assignment"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">QIP Assignment</span>
                                            </a>
                                        </li>
                                        <li class="assignmentSideTab m-nav__item" data-assignmenttype="4">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-group" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Recommendation Settings"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Recommendation Settings</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8" style="margin-top: -2.3rem;padding-top: 2rem;padding-left: 2rem !important;padding-right: 0.2rem !important;margin-right: -4rem !important;margin-bottom: -1rem;" >
                                <div id="eroAssign" class="assignContent" style="display:none">
                                    <!-- <div class=""> -->
                                        <div class="row pt-0 pl-0 pr-0" id="noAssignedEro" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display:none">
                                            <div class="col-md-12 text-center">
                                                <div class="m-demo-icon__preview">
                                                    <i style="font-size: 40px;" class="flaticon-notes"></i>
                                                </div>
                                                <span class="m-demo-icon__class">No Employee Relations Officer added</span>
                                                <div class="col-12 mb-2">
                                                    <a href="#" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" onclick="initEroAssignmentModal()" transacton="add">
                                                        <span>
                                                            <i class="fa fa-plus-circle"></i>
                                                            <span>Add an Officer</span>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="showEroDatatable" style="display:none">
                                            <!-- <div class="row"> -->
                                                <div class="col-12 text-center">
                                                    <h3>DMS Employee Relations Officer</h3>
                                                </div>
                                                <div class="col-12">
                                                    <p id="eroOptions" class="text-center" style="border-bottom: 1px solid #d5d7da !important;padding-bottom: 0.9rem !important;"><a href="#" onclick="initEroAssignmentModal()">Assign Officer</a>  | <a id="searchEro">Search Record</a></p>
                                                </div>
                                                <!-- <label class="col-lg-1 col-form-label">ERO:</label>
                                                <div class="col-lg-5">
                                                    <select class="form-control m-select2" id="eroSearchSelect2" placeholder="Select Multiple Categories" name="eroSearchSelect2" style="width: 100% !important"></select>
                                                </div>
                                                <label class="col-lg-1 col-form-label">Supevisor:</label>
                                                <div class="col-lg-5">
                                                    <select class="form-control m-select2" id="eroSupSearchSelect2" placeholder="Select Multiple Categories" name="eroSupSearchSelect2" style="width: 100% !important"></select>
                                                </div> -->
                                                <div class="col-12 input-group mb-3" id="eroSearch" style="display:none">
                                                    <div class="col-md-12 p-0" style="margin-top: -12px;">
                                                        <hr class="bg-secondary">
                                                    </div>
                                                    <input type="text" class="form-control" id="eroSearchField" placeholder="Search by ERO name, of ERO Supervisor Name">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text closeEroSearchBtn">
                                                            <i class="fa fa-close"></i>
                                                        </span>
                                                    </div>
                                                </div> 
                                                <div class="col-12">
                                                    <div id="eroDatatable"></div>
                                                </div>
                                            <!-- </div> -->
                                        </div>
                                    <!-- </div> -->
                                </div>
                                <div id="offenseTypeAssign" class="assignContent" style="display:none">
                                    <div class="row">
                                        <div class="col-md-12 p-0" id="noAssignedOffenseType" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display:none">
                                            <div class="col-md-12 text-center">
                                                <div class="m-demo-icon__preview">
                                                    <i style="font-size: 40px;" class="flaticon-notes"></i>
                                                </div>
                                                <span class="m-demo-icon__class">No Offense Type assigned</span>
                                                <div class="col-12 mb-3">
                                                    <button type="button" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" onclick="initOffenseTypeAssignmentModal()" transacton="add">
                                                        <span>
                                                            <i class="fa fa-plus-circle"></i>
                                                            <span>Assign Offense Type</span>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="showOffenseTypeAssignmentDatatable" style="display:none">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <h3>Offense Type Assignment</h3>
                                                </div>
                                                <div class="col-md-12" style="margin-top: -12px;">
                                                    <hr class="bg-secondary">
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 input-group mb-3" style="">
                                                    <!-- <input type="text" class="form-control" placeholder="Search by Employee Name" id="qipSearchField"> -->
                                                    <select class="form-control m-select2 select2" id="offenseTypeAccount" name="offenseTypeAccount" placeholder="Disciplinary Action"></select>
                                                </div> 
                                                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                                    <button type="button" class="btn btn-success m-btn m-btn--icon pull-right" onclick="initOffenseTypeAssignmentModal()">
                                                        <span>
                                                            <i class="fa fa-plus"></i>
                                                            <span>Assign</span>
                                                        </span>
                                                    </button>
                                                </div>
                                                <div class="col-12">
                                                    <div id="offenseTypeAssDatatable"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="qipAssign" class="assignContent" style="display:none">
                                    <div class="row">
                                        <div class="col-md-12 pt-0 pl-0 pr-0" id="noAssignedQip" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display:none">
                                            <div class="col-md-12 text-center">
                                                <div class="m-demo-icon__preview">
                                                    <i style="font-size: 40px;" class="flaticon-notes"></i>
                                                </div>
                                                <span class="m-demo-icon__class">No QIP Privilege assigned</span>
                                                <div class="col-12">
                                                    <button type="button" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" aria-haspopup="true" aria-expanded="false" onclick="initQipAssignmentModal()" transacton="add">
                                                        <span>
                                                            <i class="fa fa-plus-circle"></i>
                                                            <span>Assign Privilege</span>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12" id="showQipDatatable" style="display:none">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <h3>Quality Improvement Plan IR Filing Assignment</h3>
                                                </div>
                                                <div class="col-12">
                                                    <p id="qipOptions" class="text-center" style="border-bottom: 1px solid #d5d7da !important;padding-bottom: 0.9rem !important;"><a href="#" onclick="initQipAssignmentModal()">Assign QIP</a>  | <a id="searchQip" >Search Employee</a></p>
                                                </div>
                                                <div class="col-12 input-group mb-3" id="qipSearch" style="display:none">
                                                    <div class="col-md-12 p-0" style="margin-top: -12px;">
                                                        <hr class="bg-secondary">
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Search by Employee Name" id="qipSearchField">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text closeQipSearchBtn">
                                                            <i class="fa fa-close"></i>
                                                        </span>
                                                    </div>
                                                </div> 
                                                <div class="col-12">
                                                    <div id="qipDatatable"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="recommendationSettings" class="assignContent" style="display:none">
                                    <div class="col-12 p-0">
                                        <div class="col-12 text-center">
                                            <h3>Recommendation Settings</h3>
                                        </div>  
                                        <div class="col-12 mt-3">
                                            <div class="row">
                                                <span class="ml-1" style="font-size: 17px; font-weight: 500; color: #5d4747;">Default Setting</span>
                                                <div class="col pr-0"><hr style="margin-top: 0.8rem !important;"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-9 pl-0 mt-2 mb-3">
                                                    <div class="col" style="">
                                                        <label class="" style="font-size: 15px;font-weight: 400;">
                                                            <!-- <input type="checkbox" name="oneTimeOccChkbox" id="oneTimeOccChkbox" data-field="oneTimeStat"> -->
                                                            Default Recommendation Number
                                                            <!-- <span></span> -->
                                                        </label>
                                                    </div>
                                                    <div class="col comment" style="font-size: 13px;">
                                                        Recommendation Level:
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3" style="display: flex; align-items: center;">
                                                    <div class="col-12 pr-0 text-right">
                                                        <a class="text-capitalize" style="font-size: 1.4rem;" id="defaultRecommendationNum"></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <span class="ml-1" style="font-size: 17px; font-weight: 500; color: #5d4747;">Custom Level</span>
                                                <div class="col pr-0"><hr style="margin-top: 0.8rem !important;"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 pl-1 pr-1">
                                                    <div class="row pl-4 pr-4 pt-3 pb-3">
                                                        <div class="col-md-12 p-0" id="noRecommendationSet" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display:none;">
                                                            <div class="col-md-12 text-center">
                                                                <div class="m-demo-icon__preview">
                                                                    <i style="font-size: 40px;" class="flaticon-notes"></i>
                                                                </div>
                                                                <span class="m-demo-icon__class">No Custom Levels set</span>
                                                                <div class="col-12 mb-3 p-0">
                                                                    <button type="button" class="ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" onclick="initRecommendationNumberModal()">
                                                                        <span>
                                                                            <i class="fa fa-plus-circle"></i>
                                                                            <span>Set Levels</span>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 p-0" id="accountRecommendationNum" style="display:none">
                                                            <div class="row">
                                                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 input-group mb-3" style="">
                                                                    <select class="form-control m-select2 select2" id="recommendationNumAccount" name="recommendationNumAccount" placeholder="Account"></select>
                                                                </div> 
                                                                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                                                    <button type="button" class="btn btn-success m-btn m-btn--icon pull-right" onclick="initRecommendationNumberModal()">
                                                                        <span>
                                                                            <i class="fa fa-plus"></i>
                                                                            <span>Assign</span>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div id="customRecommendationNumberDatatable"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <span class="ml-1" style="font-size: 17px; font-weight: 500; color: #5d4747;">Custom Changes</span>
                                                <div class="col pr-0"><hr style="margin-top: 0.8rem !important;"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 pl-1 pr-1">
                                                    <div class="row pl-4 pr-4 pt-3 pb-3">
                                                        <div class="col-md-12 p-0" id="noCustomChangesSet" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display:none;">
                                                            <div class="col-md-12 text-center p-0">
                                                                <div class="m-demo-icon__preview">
                                                                    <i style="font-size: 40px;" class="flaticon-notes"></i>
                                                                </div>
                                                                <span class="m-demo-icon__class">No Custom Changes set</span>
                                                                <div class="col-12 mb-3 p-0 mt-1">
                                                                    <div class="dropdown">
                                                                        <button class="dropdown-toggle ml-3 mt-2 btn btn-outline-accent btn-sm m-btn m-btn--icon m-btn--outline-2x" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fa fa-plus"></i>
                                                                            Assign
                                                                        </button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                            <button type="button" class="dropdown-item" onclick="initAssignChangeModal()">Changes</button>
                                                                            <button type="button" class="dropdown-item" onclick="initAssignExemptModal()">Exemption</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 p-0" id="customChanges" style="">
                                                            <div class="row">
                                                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 input-group mb-3" style="">
                                                                    <select class="form-control m-select2 select2" id="recommendationSupervisorChange" name="recommendationSupervisorChange" placeholder="Employee Names"></select>
                                                                </div> 
                                                                <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                                                    <div class="dropdown pull-right">
                                                                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            <i class="fa fa-plus"></i>
                                                                            Assign
                                                                        </button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                            <button type="button" class="dropdown-item" onclick="initAssignChangeModal()">Changes</button>
                                                                            <button type="button" class="dropdown-item" onclick="initAssignExemptModal()">Exemption</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div id="customChangesDatatable"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="disciplinaryActionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="disciplinaryActionForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-balance-scale text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Disciplinary Action</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Disciplinary Action:</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <input type="text" class="form-control m-input" placeholder="Input Disciplinary Action" name="action" id="action">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-legal"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Abbreviation:</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <input type="text" class="form-control m-input" placeholder="Input Abbreviation" name="abreviation" id="abreviation">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-compress"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Prescriptive Period:</label>
                            <div class="col-lg-8">
                                <div class="m-bootstrap-touchspin-brand">
                                    <input type="text" class="form-control" value="0" name="prescreptivePeriod" placeholder="" type="text" id="prescreptivePeriod">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="addDisciplanyActionBtn">Add</button>
                    <button type="submit" class="btn btn-primary" id="updateDisciplanyActionBtn" style="display:none">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="categoryForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-tag text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Category Label:</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <input type="text" class="form-control m-input" placeholder="Input Category Label A, B, C etc." name="category" id="category">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-tag"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="addCategoryBtn">Add</button>
                    <button type="submit" class="btn btn-primary" id="updateCategoryBtn" style="display:none">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="offenseLevelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="offenseLevelForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-tag text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Category <span id="offenseCategory"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <!-- <div class="alert alert-brand alert-dismissible fade show  m-alert m-alert--square m-alert--air text-center" role="alert">
						  	</i>Offense Level # <strong>1</strong>					  	
						</div> -->
                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show" role="alert">
                            <div class="m-alert__icon" style="padding: 0.45rem 0.75rem !important;">
                                <i class="fa fa-bullseye"></i>
                                <span></span>
                            </div>
                            <div class="m-alert__text text-center" style="padding: 0.45rem 1.25rem !important; padding-right: 26px !important;">
                                Offense Level # <strong id="offenseLevel"></strong>		
                            </div>	
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Disciplinary Action:</label>
                            <div class="col-lg-8">
                               <select class="form-control m-select2 select2" id="offenseDisciplinaryAction" name="offenseDisciplinaryAction" placeholder="Disciplinary Action"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="addCategoryBtn">Add</button>
                    <button type="submit" class="btn btn-primary" id="updateCategoryBtn" style="display:none">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="offenseCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="offenseCategoryForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-tag text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Specific Offense Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-4 col-form-label">Category Label:</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <input type="text" class="form-control m-input" placeholder="Offense Category Label A, B, C etc." name="categoryLetter" id="categoryLetter">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-tag"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Category:</label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <input type="text" class="form-control m-input" placeholder="Offense Category" name="offenseCategoryVal" id="offenseCategoryVal">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fa fa-tag"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="addSpecOffenseCategoryBtn">Add</button>
                    <button type="submit" class="btn btn-primary" id="updateSpecOffenseCategoryBtn" style="display:none">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="offenseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="offenseForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-tag text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Specific Offense</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show" role="alert">
                            <div class="m-alert__icon" style="padding: 0.45rem 0.75rem !important;">
                                <i class="fa fa-bullseye"></i>
                                <span></span>
                            </div>
                            <div class="m-alert__text text-center" style="padding: 0.45rem 1.25rem !important; padding-right: 26px !important;">
                                <p class="" style="margin-bottom: 0rem !important;font-size: 13px !important;color: #4a4949 !important;" id="offenseCategLabelModal"></p>
                            </div>	
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Offense</label>
                            <div class="col-lg-10">
                                <input id="offenseOrderNum" type="text" class="form-control bootstrap-touchspin-vertical-btn" name="offenseOrderNum" placeholder="1, 2..." type="number">
                            </div>
                            <!-- <label class="col-lg-2 col-form-label">Category:</label>
                            <div class="col-lg-4">
                                <select class="custom-select form-control m-input" id="offenseCategoryLetter" name="offenseCategoryLetter">
                                </select>
                            </div> -->
                        </div>
                        <div class="form-group m-form__group row" id="multiCategoryField">
                            <div class="col-lg-12">
                                <label for="">Category</label><span id="categoryAlert" class="ml-2 text-danger" style="font-size:12px; display:none">Please assign 1 or more Categories*</span>
                            </div>
                            <div class="col-lg-8">
                                <select class="form-control m-select2" id="categoryList" placeholder="Select Multiple Categories" name="categoryList" multiple style="width: 100% !important"></select>
                            </div>
                            <div class="col-lg-4 pt-2">
                                <label class="m-checkbox m-checkbox--bold">
                                    <input type="checkbox" id="allCateg">Assign All
                                    <span></span>
								</label>
                            </div>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleTextarea">Offense Description</label>
                            <textarea class="form-control m-input" name="offenseDescription" id="offenseDescription" rows="3"></textarea>
                        </div>
                        
                        <!-- <p>
                            <a id="multiCategoryBtn" style="font-size: 13px;">Assign multiple Categories</a>
                            <a class="btn btn-secondary btn-sm m-btn m-btn m-btn--icon m-btn--pill" id="multiCategCancel" style="display:none;height: 23px;padding-top: 2px;">
								<span>
									<i class="fa fa-close" style="font-size: 11px;"></i>
									<span style="font-size: 11px;">Cancel</span>
								</span>
							</a>
                        </p> -->
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="addOffenseBtn">Add</button>
                    <button type="submit" class="btn btn-primary" id="updateOffenseBtn" style="display:none">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="defaultOffenseModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="disciplinaryActionForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-balance-scale text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Default Offense Tag for <span class="text-capitalize" id="dtrViolationTypeDefOff"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                            <label class="col-lg-12 col-form-label">Offense Type:</label>
                            <div class="col-lg-12">
                                <select class="form-control m-select2 select2" id="defaultOffenseType" name="defaultOffenseType" placeholder="Select Offense Type"></select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="offenseFormGroup" style="display:none">
                            <label class="col-lg-12 col-form-label">Default Offense:</label>
                            <div class="col-lg-12">
                                <select class="form-control m-select2 select2" id="defaultOffense" name="defaultOffense" placeholder="Select Offense Type"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="updateDefaultOffenseBtn">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="defaultCategoryModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="defaultCategoryForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-balance-scale text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Default Category Tag for <span class="text-capitalize" id="dtrViolationTypeDefCateg"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-form__section m-form__section--first">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="defaultCategoryAlert" style="display:none">
                            <strong>No Category set!</strong> Please set Category First. <span onclick="categoryTab()" style="font-size: 14px;text-decoration: underline;color: #FFEB3B;">Click here</span> to proceed on category page</span>.
                        </div> 
                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">Category</label>
                            <div class="col-lg-10"> 
                                <select class="custom-select form-control" id="defaultCategoryVal" name="defaultCategoryVal" placeholder="Select a category"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="updateDefaultCategoryBtn">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="qipAssignmentModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="qipAssignmentForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-tags text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Assign QIP Filing Privilege</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <button id="assignQipFilterBtn" data-stat="0" type="button" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: -41px;float: right;">
                        <i class="fa fa-filter"></i>
                    </button>
                    <div class="m-form__section m-form__section--first" id="assignQipFilter" style="margin-left: -25px; margin-right: -25px; margin-top: -26px; padding-top: 1.8rem;padding-left: 1.8rem;padding-right: 1.8rem;padding-bottom: 1rem; background: #dadada; display:none">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Class:</label>
                            <div class="col-lg-9">
                                <select class="custom-select form-control m-input" id="employeeTypeQip" name="employeeTypeQip" placeholder="Select Offense Type">
                                    <option value="0">All Team</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Account:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="accountQip" name="accountQip"></select>
                            </div>
                        </div>
                        
                    </div>
                    <div class="m-form__section">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-12 col-form-label">Employee</label>
                            <div class="col-lg-12">
                                <select class="form-control m-select2 select2" id="employeeQip" placeholder="Select Multiple Categories" name="employeeQip" multiple style="width: 100% !important"></select>
                            </div>
                        </div>
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="eroAccountsModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="eroAccountsForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <div class="row">
                         <div class="col-12 col-sm-3 col-md-3 col-lg-3">
                            <img id="eroPic" src="" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                        </div>
                        <div class="col-12 col-sm-9 col-md-9 col-lg-9">
                            <div class="row">
                                <div class="col-12 col-md-12 mb-1 text-center text-sm-left text-capitalize" style="font-size: 19px; color:#d4d4d4;" id="eroNameInfo"></div>
                                <div class="col-md-12 text-center text-sm-left text-capitalize" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: #07bdbd;" id="eroJobInfo"></div>
                                <div class="col-md-12 mb-1 text-center text-sm-left text-capitalize" style="font-size: 12px; color:#d4d4d4;" id="eroAccountInfo"></div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                     <div class="form-group m-form__group row">
                        <label class="col-lg-4 col-form-label">DMS Supervisor:</label>
                        <div class="col-lg-8">
                           <select class="form-control m-select2 select2" id="eroSup" name="eroSup"></select>
                        </div>
                    </div>
                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                    <div class="m-form__section mt-3">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-2 col-form-label">Class:</label>
                            <div class="col-lg-4">
                                <select class="custom-select form-control m-input" id="classEroAccount" name="classEroAccount" placeholder="Select Offense Type">
                                    <option value="0">All Team</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                            <label class="col-lg-1 col-form-label">Site:</label>
                            <div class="col-lg-5">
                                <select class="form-control m-select2 select2" id="siteAccountEro" placeholder="" name="siteAccountEro" style="width: 100% !important"></select>
                            </div>
                            <label class="col-lg-2 col-form-label mt-3">Search:</label>
                            <div class="col-lg-10 input-group mt-3">
                                <input type="text" class="form-control" placeholder="" id="eroAccountSearchField">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                    <div id="eroAccountDatatable">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="eroAssignmentModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="eroAssignmentForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Assign Employee Relations Officer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <button id="assignEroFilterBtn" data-stat="0" type="button" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: -41px;float: right;">
                        <i class="fa fa-filter"></i>
                    </button>
                    <div class="m-form__section m-form__section--first" id="assignEroFilter" style="margin-left: -25px; margin-right: -25px; margin-top: -26px; padding-top: 1.8rem;padding-left: 1.8rem;padding-right: 1.8rem;padding-bottom: 1rem; background: #dadada; display:none">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Class:</label>
                            <div class="col-lg-9">
                                <select class="custom-select form-control m-input" id="employeeTypeEro" name="employeeTypeEro" placeholder="Select Offense Type">
                                    <option value="0">All Team</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Account:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="accountEro" name="accountEro"></select>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__section mt-3">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Employee</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="employeeEro" name="employeeEro" style="width: 100% !important"></select>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__section">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Site</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="eroSiteId" placeholder="" name="eroSiteId" style="width: 100% !important"></select>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__section">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Account</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="eroAccountId" placeholder="" name="eroAccountId" style="width: 100% !important" multiple></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="assignOffenseTypeModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="assignOffenseTypeForm" method="post" data-formtype="add">
                <div class="modal-header">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Assign Offense Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <button id="assignOffenseTypeFilterBtn" data-stat="0" type="button" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: -41px;float: right;">
                        <i class="fa fa-filter"></i>
                    </button>
                    <div class="m-form__section m-form__section--first" id="assignOffenseTypeFilter" style="margin-left: -25px; margin-right: -25px; margin-top: -26px; padding-top: 1.8rem;padding-left: 1.8rem;padding-right: 1.8rem;padding-bottom: 1rem; background: #dadada; display:none">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Class:</label>
                            <div class="col-lg-9">
                                <select class="custom-select form-control m-input" id="employeeTypeOffenseType" name="employeeTypeOffenseType" placeholder="Select Offense Type">
                                    <option value="0">All Team</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__section mt-3">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Account:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="accountOffenseType" name="accountOffenseType"></select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Offense Type</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="OffenseTypeSelect" name="OffenseTypeSelect" style="width: 100% !important" multiple></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="offenseTypeAssignmentModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="offenseTypeAssignmentForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-tags text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Assigned Offense Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div id="" class="col-12">
                        <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show" role="alert" style="background-color:white !important;border-color: white !important;color: white !important">
                            <div class="m-alert__icon pt-2 pb-2 pl-0 pr-3" style="background: white !important;border-right: 1px solid #c1c1c1;">
                                <i class="text-dark fa fa-group" style="font-size: 2.5rem;"></i>
                            </div>
                            <div class="m-alert__text pt-2 pb-2 pl-3 pr-0" style="">
                                <span class="text-dark" style="font-size: 1.3rem;" id="accNameInfo"></span></br>
                                <span id="depNameInfo" style="color: #30a0b3;"></span>		
                            </div>	
                        </div>
                        <!-- <i class="fa fa-group" style="font-size: 1.5rem;"></i>
                        <span style="font-size: 1.3rem;" id="accNameInfo"></span></br>
                        <span id="depNameInfo"></span> -->
                    </div>
                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                    <div class="col-12 mb-3">
                        <div class="row">
                            <label class="col-lg-2 col-form-label mt-3">Search:</label>
                            <div class="col-lg-10 input-group mt-3">
                                <input type="text" class="form-control" placeholder="" id="assignedOffenseTypeSearch">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-search"></i>
                                    </span>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                    <div class="col-12">
                        <div id="assignedOffenseTypeDatatable"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="recommendationNumberModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="recomendationNumberForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-tags text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Recommendation Number</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <button id="recommendationNumberFilterBtn" data-stat="0" type="button" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: -41px;float: right;">
                        <i class="fa fa-filter"></i>
                    </button>
                    <div class="m-form__section m-form__section--first" id="recommendationNumberFilter" style="margin-left: -21px; margin-right: -21px; margin-top: -26px; padding-top: 1.8rem;padding-left: 1.8rem;padding-right: 1.8rem;padding-bottom: 1rem; background: #dadada; display:none">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Class:</label>
                            <div class="col-lg-9">
                                <select class="custom-select form-control m-input" id="employeeTypeRecommendationNum" name="employeeTypeRecommendationNum" placeholder="Select Offense Type">
                                    <option value="0">All Team</option>
                                    <option value="Admin">SZ Team</option>
                                    <option value="Agent">Ambassador</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="m-form__section mt-3">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Account:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="accountRecommendationNumber" name="accountRecommendationNumber"></select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 col-form-label">Number:</label>
                            <div class="col-lg-9">
                                <div class="m-bootstrap-touchspin-brand">
                                    <input type="text" class="form-control" value="0" name="recommendationLevel" placeholder="" type="text" id="recommendationLevel">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" id="updateRecommendationNumBtn" style="display:none">Update</button>
                    <button type="submit" class="btn btn-primary" id="addRecommendationNumBtn">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="assignChangeModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="assignChangeForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-tags text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Assign Recommendation Change</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" id="existChangeAlert" style="display:none">
					  	<strong>Already Exist!</strong> and assigned to <span id="assignedTo"></span>.
					</div>  
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" id="sameChangeAlert" style="display:none">
					  	<strong>Invalid Assignment!</strong> Recommender and Employee to be assigned to should not be the same person.
					</div>  
                    <div class="m-form__section mt-3">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Recommender:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="empAssignChanges" name="empAssignChanges"></select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Account:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="accountAssignChanges" name="accountAssignChanges"></select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 col-form-label">Level:</label>
                            <div class="col-lg-9">
                                <div class="m-bootstrap-touchspin-brand">
                                    <input type="text" class="form-control" value="0" name="levelAssignChanges" placeholder="" type="text" id="levelAssignChanges">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                            <div class="col text-center font-italic" style="font-size: 1.2rem;">Assign To</div>
                            <div class="col"><hr style="margin-top: 0.8rem !important;"></div>
                        </div>
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Employee:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="empAssignToChanges" name="empAssignToChanges"></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <!-- <button type="submit" class="btn btn-primary" id="updateRecommendationNumBtn" style="display:none">Update</button> -->
                    <button type="submit" class="btn btn-primary" id="addRecommendationChangesBtn">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="assignExemptionModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="assignExemptionForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-tags text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Assign Recommendation Exemption</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" id="existExemptionAlert" style="display:none">
					  	<strong>Already Exist!</strong> This exemption was already set</span>.
					</div>  
                    <div class="m-form__section mt-3">
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Recommender:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="empAssignExemption" name="empAssignExemption"></select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" style="">
                            <label class="col-lg-3 col-form-label">Account:</label>
                            <div class="col-lg-9">
                                <select class="form-control m-select2 select2" id="accountAssignExemption" name="accountAssignExemption"></select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-lg-3 col-form-label">Level:</label>
                            <div class="col-lg-9">
                                <div class="m-bootstrap-touchspin-brand">
                                    <input type="text" class="form-control" value="0" name="levelAssignExemption" placeholder="" type="text" id="levelAssignExemption">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <!-- <button type="submit" class="btn btn-primary" id="updateRecommendationNumBtn" style="display:none">Update</button> -->
                    <button type="submit" class="btn btn-primary" id="addRecommendationExemptionBtn">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="customChangesModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="assignExemptionForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-tags text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">Recommendation Reassignment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="form-group m-form__group row">
                         <label for="" class="col-6 col-sm-5 col-md-5">Recommender:</label>
                         <div class="col-6 col-sm-7 col-md-7 text-capitalize font-weight-bold" id="recommenderName"></div>
                     </div>
                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                    <div class="form-group m-form__group row mt-3" style="">
                        <label class="col-lg-3 col-form-label">Account:</label>
                        <div class="col-lg-9">
                            <select class="form-control m-select2 select2" id="accountCustomChanges" name="accountCustomChanges"></select>
                        </div>
                    </div>
                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                    <div id="reassignmentDatatable"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <!-- <button type="submit" class="btn btn-primary" id="updateRecommendationNumBtn" style="display:none">Update</button> -->
                    <button type="submit" class="btn btn-primary" id="addRecommendationExemptionBtn">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/incrementLetter/incrementLetter.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/cod_settings.js"></script>
