<style>
.bs-popover-auto[x-placement^=right] .arrow::after, .bs-popover-right .arrow::after {
border-right-color: #282A3C !important;
}
.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
border-left-color: #282A3C !important;
}
.popover-header{
background-color: rgb(52, 55, 78)!important;
border-bottom: 0px !important;
color: white;
max-width: 100% !important;
}
.popover-body{
background-color: #282A3C!important;
max-width: 100% !important;
padding: 1.0rem 0.5rem !important;
text-align: center;
padding-bottom: 1px !important;
}
.list-active{
    background: #dcdcdc;
}

.active-nav-a{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell{
    background:unset !important;
}
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover{
    background:#F0F0F2 !important;
    color: black !important;
}

.codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
    color: #8c8c8c;
}

.m-nav.m-nav--inline>.m-nav__item:first-child {
    padding-left: 20px !important;
}

.dms.m-nav.m-nav--inline>.m-nav__item {
    padding: 10px 20px 10px 20px !important;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active {
    background: #d4d7da;
}
.dms.m-nav.m-nav--inline>.m-nav__item:hover {
    background: #ececec;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
    color:#4f668a;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
    color: black;
}
.m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
    height: 100%;
    padding: 0.8rem 0 0.8rem 0;
}

.m-portlet .m-portlet__head {
    height: 3.9rem;
}

.m-portlet.m-portlet--tabs.active{
    display:block;
}
.dtrAutoIrSideTab{
    padding-left: 0.5rem !important; 
    padding-right: 0.5rem !important;
    padding-top: 0.8rem !important;
    padding-bottom: 0.8rem !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__clear {
    margin-right: 1px !important;
}


.editableStyle{
    border-bottom: dashed 1px #0088cc;
    cursor: pointer;
}
/* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
/* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

.select2-container--default .select2-selection--single .select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem !important;
    white-space: normal;
}

.select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
    top: 47% !important;
}

#irAbleDtrViolationDatatable>.m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__foot, .m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__head {
    background: #e0e0e0;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}

</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">DMS Supervision</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview" style="padding:0px !important">
                <ul class="dms m-nav m-nav--inline">
                    <li class="m-nav__item active">
                        <a href="#" class="dmsSupervisionMainNav m-nav__link active" data-navcontent="dtrViolations">
                            <i class="m-nav__link-icon fa fa-clock-o"></i>
                            <span class="m-nav__link-text">DTR Violations</span>
                            <span class="m-nav__link-badge" id="dtrViolationBadge" style="display:none">
                                <span class="m-badge m-badge--primary m-badge--wide" id="dtrViolationBadgeCount"></span>
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsSupervisionMainNav m-nav__link" data-navcontent="irSupervision">
                            <i class="m-nav__link-icon fa fa-gears"></i>
                            <span class="m-nav__link-text">IR Supervision</span>
                            <span class="m-nav__link-badge">
                                <span class="m-badge m-badge--primary m-badge--wide">23</span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs active" id="dtrViolations">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                         <li class="nav-item m-tabs__item">
                            <a id="violationLogTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#violationsLog" role="tab">
                                <i class="fa fa-balance-scale"></i>Violations Log</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrTab" class="nav-link m-tabs__link" data-toggle="tab" href="#fileDtrViolationIr" role="tab">
                                <i class="fa fa-list-alt"></i>File DTR Violation IR
                                <span class="m-nav__link-badge ml-2" style="display:none" id="fileDtrViolationIrBadge">
                                <!-- <span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span> -->
                                    <span class="m-badge m-badge--primary m-badge--wide" id="fileDtrViolationIrBadgeCount">23</span>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrRecordTab" class="nav-link m-tabs__link" data-toggle="tab" href="#fileDtrViolationIrRecord" role="tab">
                                <i class="fa fa-legal"></i>Record</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">                   
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="violationsLog" role="tabpanel">
                        <div class="row">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="margin-top: -2.3rem !important;margin-left: -2.2rem !important;margin-bottom: -2.2rem !important;padding-top: 1rem;border-right: 1px solid #dedede;background: #c1c2c73d;">
                                <div class="" style="margin-left: -11px;">
                                    test
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8" style="margin-top: -2.3rem;padding-top: 2rem;padding-left: 2rem !important;padding-right: 0.2rem !important;margin-right: -4rem !important;margin-bottom: -1rem;">
                                content here
                            </div>
                        </div>
                    </div> 
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIr" role="tabpanel">
                        <div class="row">
                            <div class="col-md-5 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpSelect" id="fileDtrViolationEmpSelect" name="fileDtrViolationEmpSelect" data-toggle="m-tooltip" title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationType" name="violationType" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Forgot to Log Out</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control m-input datePicker" readonly="" id="dtrViolationSched" name="dtrViolationSched" placeholder="Select a Date">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data shown in the table below.</span> -->
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolation" style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div  class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolation" style="display:none;">
                                    <div class="col-md-12 col-lg-3 p-0">
                                        <div class="col-md-12" id="pendingLateCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingLateCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingObCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingObCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Over Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingUtCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingUtCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Undertime</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingForgotToLogoutCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingForgotToLogoutCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Forgot to Logout</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncBreakCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingIncBreakCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Incomplete Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncDtrCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingIncDtrCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Incomplete DTR</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingAbsentCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingAbsentCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-9 pl-0">
                                        <div id="irAbleDtrViolationDatatable"></div>
                                    </div>
                                </div>
                                <!-- <div id="approvalAhrPendingDatatable"></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIrRecord" role="tabpanel">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpRecordSelect" id="fileDtrViolationEmpRecordSelect" name="fileDtrViolationEmpRecordSelect" data-toggle="m-tooltip" title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="fileDtrViolationRecordDateRange">
                                    <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Schedule">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeRecord" name="violationTypeRecord" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Forgot to Log Out</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-md-2 mb-3">
                                <select class="custom-select form-control" id="fileDtrViolationRecordStatus" name="fileDtrViolationRecordStatus" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="3">Completed</option>
                                    <option value="12">Missed</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolationRecord" style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div  class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Record Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolationRecord" style="display:none">
                                    <div class="col-md-12 col-lg-3 p-0">
                                        <div class="col-md-12" id="recordLateCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordLateCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordObCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordObCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Over Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordUtCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordUtCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Undertime</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordForgotToLogoutCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordForgotToLogoutCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Forgot to Logout</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncBreakCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordIncBreakCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Incomplete Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncDtrCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordIncDtrCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Incomplete DTR</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordAbsentCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordAbsentCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-9 pl-0">
                                        <div id="irAbleDtrViolationRecordDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>      
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="irSupervision" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                         <li class="nav-item m-tabs__item">
                            <a id="disciplinaryActionTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#explanationTab" role="tab">
                                <i class="fa fa-balance-scale"></i>Subject Explanation</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseCategoryTab" class="nav-link m-tabs__link" data-toggle="tab" href="#recommendationTab" role="tab">
                                <i class="fa fa-list-alt"></i>Recommendation</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab" href="#clientConfirmationTab" role="tab">
                                <i class="fa fa-legal"></i>Client Confirmation</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab" href="#effectivityDatesTab" role="tab">
                                <i class="fa fa-legal"></i>Effectivity Dates</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">                   
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="explanationTab" role="tabpanel">
                    </div> 
                    <div class="tab-pane fade show col-md-12" id="recommendationTab" role="tabpanel">
                    </div> 
                    <div class="tab-pane fade show col-md-12" id="effectivityDatesTab" role="tabpanel">
                    </div> 
                </div>      
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->

<div class="modal fade" id="qualifiedModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="eroAccountsForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">File DTR Violation Incident Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-3">
                                    <img id="empPicApproval" src="http://3.18.221.50/sz/assets/images/img/sz.png" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="qualifiedEmployeeName">Jamelyn A. Sencio </div>
                                        <div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="qualifiedEmployeeJob">Customer Care Ambassador 1</div>
                                        <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="qualifiedEmployeeAccount">ONYX Enterprises</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row p-2">
                                <div class="col-12">
                                    <div class="row pl-4 pr-4">
                                        <span  class="font-weight-bold mr-2">Date Of Incident:</span>
                                        <span class="" id="qualifiedDateIncident">Dec 4, 2018</span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row pl-4 pr-4">
                                        <span  class="font-weight-bold mr-2">Incident:</span>
                                        <span class="" id="qualifiedIncidentType">Forgout to Logout</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5" id="incidentDetails"></div>
                        </div>
                        <div id="withPreviousUserDtrViolation" class="row pl-3 pr-3">
                            <div class="col-12">
                                <div class="row p-3">
                                    <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Previous Related Incidents</span>
                                    <div class="col">
                                        <hr style="margin-top: 0.8rem !important;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row pl-5 pr-5">
                                    <div class="col-12" id="previousDetails">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="withRule">
                            <div class="col-12">
                                <div class="row p-3">
                                    <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR Rule</span>
                                    <div class="col">
                                        <hr style="margin-top: 0.8rem !important;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-1 font-weight-bold">
                                        Rule:
                                    </div>
                                    <div class="col-12 col-md-11 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">
                                        <i style="font-size: 13px; font-weight: 400;" id="qualifiedRule"></i>
                                    </div>
                                    <div id="withPreviousUserDtrViolation">
                                        <div class="col-12 mt-2 font-weight-bold">
                                                Previous Consecutive Occurence:
                                        </div>
                                        <div class="col-12">
                                                <!-- late, UT,  Forgout to logout-->
                                                <!-- <table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">
                                                    <thead style="background: #555;color: white;">
                                                        <tr>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Shift</th>
                                                            <th class="text-center">Actual In</th>
                                                            <th class="text-center">Duration</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-center" style="font-weight:400">December 30, 2018</td>
                                                            <td class="text-center">10:00 PM-7:00 AM</td>
                                                            <td class="text-center" style="font-weight:400">10:20 PM</td>
                                                            <td class="text-center" style="font-weight:400">1 hrs & 20 mins</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-center" style="font-weight:400">December 30, 2018</td>
                                                            <td class="text-center">10:00 PM-7:00 AM</td>
                                                            <td class="text-center" style="font-weight:400">10:20 PM</td>
                                                            <td class="text-center" style="font-weight:400">1 hrs & 20 mins</td>
                                                        </tr>
                                                    </tbody>
                                                </table> -->
                                                <!-- BREAK -->
                                                <!-- <table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">
                                                    <thead style="background: #555;color: white;">
                                                        <tr>
                                                            <th class="text-center">Date</th>
                                                            <th class="text-center">Shift</th>
                                                            <th class="text-center">Allowable</th>
                                                            <th class="text-center">Covered</th>
                                                            <th class="text-center">Over Break</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-center" style="font-weight:400">December 30, 2018</td>
                                                            <td class="text-center">10:00 PM-7:00 AM</td>
                                                            <td class="text-center" style="font-weight:400">1 hrs</td>
                                                            <td class="text-center" style="font-weight:400">1 hrs & 20 mins</td>
                                                            <td class="text-center" style="font-weight:400">20 mins</td>
                                                        </tr>
                                                    </tbody>
                                                </table> -->
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="withIrHistory">
                            <div class="col-12">
                                <div class="row p-3">
                                    <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;">Incident History</span>
                                    <div class="col">
                                        <hr style="margin-top: 0.8rem !important;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row pl-5 pr-5">
                                <div class="col-12">
                                        <table class="table m-table table-hover table-sm table-bordered" id="table-dtr" style="font-size:13px">
                                            <thead style="background: #555;color: white;">
                                                <tr>
                                                    <th class="text-center">Date Filed</th>
                                                    <th class="text-center">Dated as Liable</th>
                                                    <th class="text-center">IR ID</th>
                                                    <th class="text-center">Level</th>
                                                    <th class="text-center">Disciplinary Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center" style="font-weight:400">December 30, 2018</td>
                                                    <td class="text-center" style="font-weight:400">January 1, 2019</td>
                                                    <td class="text-center">000000001</td>
                                                    <td class="text-center">1</td>
                                                    <td class="text-center" style="font-weight:400">Final Written Warning</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center" style="font-weight:400">December 30, 2018</td>
                                                    <td class="text-center" style="font-weight:400">January 1, 2019</td>
                                                    <td class="text-center">000000001</td>
                                                    <td class="text-center">2</td>
                                                    <td class="text-center" style="font-weight:400">Final Written Warning</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Offense Details and Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold">Offense Type:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Offense:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"><i style="font-size: 13px; font-weight: 400; color: #c31717;" id="qualifiedOffense"></i></div>
                                <div class="col-12 col-md-3 font-weight-bold">Category:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedCategory"></div>
                                <div class="col-12 pt-2 pb-2">
                                    <div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;"></div>
                                </div>
                                <div class="col-12 col-md-3 font-weight-bold">Level:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedLevel"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Disciplinary Action:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 17px; font-weight: 500" id="qualifiedDisciplinaryAction"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Period:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedPeriod"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Witnesses </span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pr-5 pl-5">
                            <!-- <div class="row pl-5 pr-5" id="noWitnessesDtrViolation" style="margin-right: 0px; margin-left: 0px;">
                                <div class="col-md-12 text-center" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4">
                                        <span><i style="font-size: 25px;" class="flaticon-notes"></i></span> 
                                        <span style="font-size: 20px;">No Witnesses Added</span>
                                    </span>
                                </div> -->
                                <!-- <div class="row"> -->
                                    <select class="form-control m-select2" id="qualifiedWitnesses" placeholder="Select Multiple Witnesses" name="qualifiedWitnesses" style="width: 100% !important" multiple></select>
                                <!-- </div> -->
                            <!-- </div> -->
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--end::Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/cod_settings.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision.js"></script>
