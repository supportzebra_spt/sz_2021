<style>
.bs-popover-auto[x-placement^=right] .arrow::after, .bs-popover-right .arrow::after {
border-right-color: #282A3C !important;
}
.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
border-left-color: #282A3C !important;
}
.popover-header{
background-color: rgb(52, 55, 78)!important;
border-bottom: 0px !important;
color: white;
max-width: 100% !important;
}
.popover-body{
background-color: #282A3C!important;
max-width: 100% !important;
padding: 1.0rem 0.5rem !important;
text-align: center;
padding-bottom: 1px !important;
}
.list-active{
    background: #dcdcdc;
}

.active-nav-a{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell{
    background:unset !important;
}
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover{
    background:#F0F0F2 !important;
    color: black !important;
}

.codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
    color: #8c8c8c;
}

.m-nav.m-nav--inline>.m-nav__item:first-child {
    padding-left: 20px !important;
}

.dms.m-nav.m-nav--inline>.m-nav__item {
    padding: 10px 20px 10px 20px !important;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active {
    background: #d4d7da;
}
.dms.m-nav.m-nav--inline>.m-nav__item:hover {
    background: #ececec;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
    color:#4f668a;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
    color: black;
}
.m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
    height: 100%;
    padding: 0.8rem 0 0.8rem 0;
}

.m-portlet .m-portlet__head {
    height: 3.9rem;
}

.m-portlet.m-portlet--tabs.active{
    display:block;
}
.dtrAutoIrSideTab{
    padding-left: 0.5rem !important; 
    padding-right: 0.5rem !important;
    padding-top: 0.8rem !important;
    padding-bottom: 0.8rem !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__clear {
    margin-right: 1px !important;
}


.editableStyle{
    border-bottom: dashed 1px #0088cc;
    cursor: pointer;
}
/* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
/* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

.select2-container--default .select2-selection--single .select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem !important;
    white-space: normal;
}

.select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
    top: 47% !important;
}

#irAbleDtrViolationDatatable>.m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__foot, .m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__head {
    background: #e0e0e0;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}

</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">DMS Monitoring</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview" style="padding:0px !important">
                <ul class="dms m-nav m-nav--inline">
                    <li class="m-nav__item active">
                        <a href="#" class="dmsMonitoringMainNav m-nav__link active" data-navcontent="dtrViolationsMonitoring">
                            <i class="m-nav__link-icon fa fa-clock-o"></i>
                            <span class="m-nav__link-text">DTR Violations Monitoring</span>
                            <span class="m-nav__link-badge" id="dtrViolationBadgeMonitoring" style="display:none">
                                <span class="m-badge m-badge--primary m-badge--wide" id="dtrViolationBadgeCountMonitoring"></span>
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsMonitoringMainNav m-nav__link" data-navcontent="incidentReportMonitoring">
                            <i class="m-nav__link-icon fa fa-gears"></i>
                            <span class="m-nav__link-text">Incident Report Monitoring</span>
                            <span class="m-nav__link-badge">
                                <span class="m-badge m-badge--primary m-badge--wide">23</span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs active" id="dtrViolationsMonitoring">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                         <li class="nav-item m-tabs__item">
                            <a id="violationLogMonitoringTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#violationsLogMonitoring" role="tab">
                                <i class="fa fa-balance-scale"></i>Violations Log</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrMonitoringTab" class="nav-link m-tabs__link" data-toggle="tab" href="#fileDtrViolationIrMonitoring" role="tab">
                                <i class="fa fa-list-alt"></i>File DTR Violation IR
                                <span class="m-nav__link-badge ml-2" style="display:none" id="fileDtrViolationIrBadgeMonitoring">
                                    <span class="m-badge m-badge--primary m-badge--wide" id="fileDtrViolationIrBadgeCountMonitoring">23</span>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrRecordMonitoringTab" class="nav-link m-tabs__link" data-toggle="tab" href="#fileDtrViolationIrRecordMonitoring" role="tab">
                                <i class="fa fa-legal"></i>Record</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">                   
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="violationsLogMonitoring" role="tabpanel">
                        <div class="row">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="margin-top: -2.3rem !important;margin-left: -2.2rem !important;margin-bottom: -2.2rem !important;padding-top: 1rem;border-right: 1px solid #dedede;background: #c1c2c73d;">
                                <div class="" style="margin-left: -11px;">
                                    test
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8" style="margin-top: -2.3rem;padding-top: 2rem;padding-left: 2rem !important;padding-right: 0.2rem !important;margin-right: -4rem !important;margin-bottom: -1rem;">
                                content here
                            </div>
                        </div>
                    </div> 
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIrMonitoring" role="tabpanel">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpSelectMonitoring" id="fileDtrViolationEmpSelectMonitoring" name="fileDtrViolationEmpSelectMonitoring" data-toggle="m-tooltip" title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeMonitoring" name="violationTypeMonitoring" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Forgot to Log Out</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control m-input datePicker" readonly="" id="dtrViolationSchedMonitoring" name="dtrViolationSchedMonitoring" placeholder="Select a Date">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <select class="custom-select form-control" id="fileDtrViolationMonitoringStatus" name="fileDtrViolationMonitoringStatus" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="2">Pending</option>
                                    <option value="12">Missed</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolationMonitoring" style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div  class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolationMonitoring" style="display:none;">
                                    <div class="col-12">
                                        <div class="alert alert-dismissible fade show m-alert m-alert--air m-alert--outline alert-warning hide" id="missedAlert" role="alert" style="display:none">
                                            <strong id="missedHighlight">Error</strong> <span id="missedMssg">This is a test message</span>         
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-3 p-0">
                                        <div class="col-md-12" id="pendingLateCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingLateCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingObCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingObCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Over Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingUtCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingUtCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">UT</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingForgotToLogoutCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingForgotToLogoutCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Forgot to Logout</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncBreakCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingIncBreakCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncDtrCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingIncDtrCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc DTR</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingAbsentCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingAbsentCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-9 pl-0">
                                        <div id="irAbleDtrViolationMonitoringDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIrRecordMonitoring" role="tabpanel">
                        <div class="row">
                            <div class="col-md-5 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpMonitoringRecordSelect" id="fileDtrViolationEmpMonitoringRecordSelect" name="fileDtrViolationEmpRecordSelect" data-toggle="m-tooltip" title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="fileDtrViolationMonitoringRecordDateRange">
                                    <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Schedule">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeMonitoringRecord" name="violationTypeMonitoringRecord" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Forgot to Log Out</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <!-- <div class="col-md-2 mb-3">
                                <select class="custom-select form-control" id="fileDtrViolationMonitoringRecordStatus" name="fileDtrViolationMonitoringRecordStatus" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="17">Liable</option>
                                    <option value="18">Non-Liable</option>
                                </select>
                            </div> -->
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolationRecordMonitoring" style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div  class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Record Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolationRecordMonitoring" style="display:none">
                                    <div class="col-md-12 col-lg-3 p-0">
                                        <div class="col-md-12" id="recordLateCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordLateCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordObCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordObCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Over Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordUtCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordUtCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Undertime</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordForgotToLogoutCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordForgotToLogoutCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Forgot to Logout</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncBreakCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordIncBreakCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Incomplete Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncDtrCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordIncDtrCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Incomplete DTR</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordAbsentCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordAbsentCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-9 pl-0">
                                        <div id="irAbleDtrViolationRecordMonitoringDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>      
            </div>
        </div>
        <!-- <div class="m-portlet m-portlet--tabs" id="incidentReportMonitoring" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                         <li class="nav-item m-tabs__item">
                            <a id="disciplinaryActionTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#explanationTab" role="tab">
                                <i class="fa fa-balance-scale"></i>Subject Explanation</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseCategoryTab" class="nav-link m-tabs__link" data-toggle="tab" href="#recommendationTab" role="tab">
                                <i class="fa fa-list-alt"></i>Recommendation</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab" href="#clientConfirmationTab" role="tab">
                                <i class="fa fa-legal"></i>Client Confirmation</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab" href="#effectivityDatesTab" role="tab">
                                <i class="fa fa-legal"></i>Effectivity Dates</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">                   
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="explanationTab" role="tabpanel">
                    </div> 
                    <div class="tab-pane fade show col-md-12" id="recommendationTab" role="tabpanel">
                    </div> 
                    <div class="tab-pane fade show col-md-12" id="effectivityDatesTab" role="tabpanel">
                    </div> 
                </div>      
            </div>
        </div> -->
    </div>
</div>

<!--begin::Modal-->
<!--end::Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_monitoring.js"></script>
