<!-- 
<div style="background-image:url(assets/images/img/logo_header.png);background-size:100%;background-repeat:no-repeat;height:100%;">
    <div style="padding: 72px 24px 0px 24px;"> 1 inch and 1/2 inch -->
<div>
    <!-- <h2 style="text-transform: capitalize"><b></b></h2> -->
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td colspan="3" style="text-align:center;border: 1px solid #ddd;padding:8px;">
                <h4>EMPLOYEE INCIDENT REPORT (FORM 002)</h4>
            </td>
        </tr>
        <tr>
            <td colspan='3' style="border: 1px solid #ddd;padding:2px 4px;font-size:12px;text-align:right">IR-ID: <b><?php echo str_pad($details->incidentReport_ID, 8, '0', STR_PAD_LEFT); ?></b>&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Name of Employee:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:13px;width:50%;text-transform:capitalize"><b><?php echo $details->subjectName; ?></b></td>
            <td style="border: 1px solid #ddd;padding:6px 4px;font-size:11px;width:25%;text-align:center">Date of Incident: <b><?php echo date('M d, Y', strtotime($details->incidentDate)); ?></b></td>
        </tr>

    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Offense Type:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%"><?php echo $details->offenseType; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Offense:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%"><?php echo $details->offense; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Category:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%;text-transform:capitalize">Category <?php echo $details->category; ?></td>
        </tr>

        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Incident:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%;"><?php echo $details->details; ?></td>
        </tr>

        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Expected Course of Action:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%;"><?php echo $details->expectedAction; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Suggested Sanction:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:13px;width:50%;"><b><?php echo $details->suggestedAbbreviation ?></b></td>
            <!-- <td style="border: 1px solid #ddd;padding:6px 4px;font-size:11px;width:25%;text-align:center">Cure Date: <b><?php echo date('M d, Y', strtotime($details->prescriptionEnd)); ?></b></td> -->
        </tr>
    </table>
    <?php if ($history !== null && !empty($history)) : ?>
        <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
            <tr>
                <td colspan='5' style="border: 1px solid #ddd;padding:6px;font-size:12px;text-align:left">Incident
                    Report History</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:13.5%;text-align:center;background:#eee;font-weight:bold">
                    IR-ID</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:39.5%;text-align:center;background:#eee;font-weight:bold">
                    Incident Date</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:13.5%;text-align:center;background:#eee;font-weight:bold">
                    Suggested</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:13.5%;text-align:center;background:#eee;font-weight:bold">
                    Final</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:20%;text-align:center;background:#eee;font-weight:bold">
                    Cure Date</td>
            </tr>
            <?php foreach ($history as $hist) : ?>
                <tr>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:13.5%;text-align:center"><?php echo str_pad($hist->incidentReport_ID, 8, '0', STR_PAD_LEFT); ?>
                    </td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:39.5%;text-align:center"><?php echo date('M d, Y', strtotime($hist->incidentDate)) . ' ' . date('h:i:s A', strtotime($hist->incidentDate . " " . $hist->incidentTime)); ?></td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:13.5%;text-align:center"><?php echo strtoupper($hist->sugAbbreviation); ?></td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:13.5%;text-align:center">
                        <?php echo strtoupper($hist->abbreviation); ?></td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:20%;text-align:center"><?php echo ($hist->label === 't') ? "n/a" : date('M d, Y', strtotime($hist->prescriptionEnd)); ?></td>
                </tr>
            <?php endforeach; ?>

        </table>
    <?php endif; ?>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Employee's Explanation:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%;"><?php echo $details->subjectExplanation; ?></td>
        </tr>
    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Reported By:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:50%;"><b><?php echo $details->sourceName; ?></b></td>
            <td style="border: 1px solid #ddd;padding:6px 2px;font-size:11px;width:25%;text-align:center">Explanation Date: <b><?php echo ($details->explanationDate === null) ? "None" : date('M d, Y', strtotime($details->explanationDate)); ?></b></td>
        </tr>
    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Name and Signature of the Witness/es:</td>
            <td style="border: 1px solid #ddd;padding:0px 6px 4px 8px;font-size:12px;width:75%;text-align:center">

                <?php foreach ($witnesses as $key => $witness) : ?>
                    <div style="display:block;width:100%;padding:2px 6px">
                        <?php if ($witness->status_ID === '2') : ?>
                            <br><br><br>
                            <p><strong style="text-transform:uppercase"><?php echo $witness->name; ?></strong> <br /><span style="font-size:11px">(Witness)</span> <br /> </p>
                        <?php else : ?>
                            <?php if (file_exists($witness->signature)) : ?>
                                <img src="<?php echo base_url() . $witness->signature; ?>" alt="No Signature" style="height:60px;object-fit:contain;margin:0 !important">
                            <?php else : ?>
                                <br><small style="color:gray"><i>No signature</i></small><br><br>
                            <?php endif; ?>
                            <p style="font-size:11px;"><strong style="text-transform:uppercase"><?php echo $witness->name; ?></strong> <br /> <span style="font-size:11px"><?php echo ($witness->status_ID === '24') ? "Acknowledged" : "Renounced";
                                                                                                                                                                                    echo " (" . date('F d, Y', strtotime($witness->datedStatus)) . ")"; ?></span> <br /> </p>
                        <?php endif; ?>
                    </div>
                    <br />
                <?php endforeach; ?>

            </td>
        </tr>

    </table>

    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Name and Signature of the Employee:</td>
            <td style="border: 1px solid #ddd;padding:0px 6px 4px 6px;font-size:12px;width:75%;text-align:center">

                <?php if ($details->subjectExplanationStat === '3' && $details->subjectExplanation !== '' && $details->subjectExplanation !== NULL) : ?>
                    <?php if (file_exists($details->subjectSignature)) : ?>
                        <img src="<?php echo base_url() . $details->subjectSignature; ?>" alt="No Signature" style="height:60px;object-fit:contain;margin:0 !important">
                    <?php else : ?>
                        <br><small style="color:gray"><i>No signature</i></small><br><br>
                    <?php endif; ?>
                <?php else : ?>
                    <br><br><br>
                <?php endif; ?>

                <p style="text-transform:uppercase"><strong><?php echo $details->subjectName; ?></strong></p>
                <p style="font-size:11px">(The Signature acknowledges that the report has been discussed)</p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Name and Signature of the Recommenders:</td>
            <td style="border: 1px solid #ddd;padding:0px 6px 4px 6px;font-size:12px;width:75%;text-align:center">

                <?php foreach ($recommenders as $key2 => $rec) : ?>
                    <div style="display:block;width:100%;padding:2px 6px;">
                        <?php if ($rec->liabilityStat_ID === '17' || $rec->liabilityStat_ID === '18') : ?>
                            <?php if (file_exists($rec->signature)) : ?>
                                <img src="<?php echo base_url() . $rec->signature; ?>" alt="No Signature" style="height:60px;object-fit:contain;margin:0 !important">

                                <p style="font-size:12px"><strong style="text-transform:uppercase"><?php echo $rec->name; ?></strong> <br /> <span style="font-size:11px">Recommender <?php echo $rec->level;
                                                                                                                                                                                                    echo " (" . date('F d, Y', strtotime($rec->datedLiabilityStat)) . ")"; ?> </span> <br /> </p>
                            <?php else : ?>
                                <br><small style="color:gray"><i>No signature</i></small>
                                <br>
                                <br>
                                <p style="font-size:12px"><strong style="text-transform:uppercase"><?php echo $rec->name; ?></strong> <br /> <span style="font-size:11px"> Recommender <?php echo $rec->level; ?> </span><br /> </p>
                            <?php endif; ?>
                        <?php else : ?>
                            <br>
                            <br>
                            <br>
                            <p style="font-size:12px"><strong style="text-transform:uppercase"><?php echo $rec->name; ?></strong> <br /> <span style="font-size:11px">Recommender <?php echo $rec->level; ?> </span><br /> </p>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>

            </td>
        </tr>

    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Human Resources Team:</td>
            <td style="border: 1px solid #ddd;padding:0px 6px 4px 6px;font-size:12px;width:75%;text-align:center;">
                <div style="display:block:width:100%;padding:2px 6px;">
                    <?php if ($details->finalRecommenderLiabilityStat_ID === '17' || $details->finalRecommenderLiabilityStat_ID === '18') : ?>
                        <?php if (file_exists($details->finalRecommenderSignature)) : ?>
                            <img src="<?php echo base_url() . $details->finalRecommenderSignature; ?>" alt="No Signature" style="height:60px;object-fit:contain;margin:0 !important">

                            <p style="font-size:12px"><strong style="text-transform:uppercase"><?php echo $details->finalRecommender; ?></strong> <br /> <span style="font-size:11px">Employee Relations Officer <?php echo " (" . date('F d, Y', strtotime($details->finalRecommenderDatedLiabilityStat)) . ")"; ?> </span> <br /> </p>
                        <?php else : ?>
                            <br><small style="color:gray"><i>No signature</i></small>
                            <br>
                            <br>
                            <p style="font-size:12px"><strong style="text-transform:uppercase"><?php echo $details->finalRecommender; ?></strong> <br /> <span style="font-size:11px">Employee Relations Officer</span><br /> </p>
                        <?php endif; ?>
                    <?php else : ?>
                        <br>
                        <br>
                        <br>
                        <p style="font-size:12px"><strong style="text-transform:uppercase"><?php echo $details->finalRecommender; ?></strong> <br /> <span style="font-size:11px">Employee Relations Officer</span><br /> </p>
                    <?php endif; ?>
                </div>
            </td>
        </tr>
    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Liability Status: &nbsp;&nbsp;</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:14px;width:50%;"><b><?php echo strtoupper($details->description); ?></b>&nbsp;&nbsp;<i style="font-size:9px;"><?php echo "(" . date('M d, Y', strtotime($details->datedLiabilityStat)) . ")" ?></i></td>
            <td colspan="3" style="border: 1px solid #ddd;padding:6px;font-size:11px;width:25%;text-align:center">Date: <b><?php echo ($details->liabilityStat === 2) ? ($details->datedLiabilityStat === null) ? "None" : date('M d, Y', strtotime($details->datedLiabilityStat)) : "None"; ?></b></td>
        </tr>
        <?php if ((int) $details->liabilityStat == 17) : ?>
            <tr>
                <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%;">Final Sanction: &nbsp;&nbsp;</td>
                <td style="border: 1px solid #ddd;padding:6px;font-size:14px;width:30%;"><b><?php echo strtoupper($details->finalAbbreviation); ?></b></td>
                <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:20%;"><b>Cure Date</b>: &nbsp;&nbsp;</td>
                <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:14px;width:25%;"><?php echo date('M d, Y', strtotime($details->prescriptionEnd))?></td>
            </tr>
        <?php endif; ?>
    </table>
</div>
<!-- </div> -->