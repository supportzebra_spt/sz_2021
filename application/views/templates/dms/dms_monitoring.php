<style>
.bs-popover-auto[x-placement^=right] .arrow::after,
.bs-popover-right .arrow::after {
    border-right-color: #282A3C !important;
}

.bs-popover-auto[x-placement^=left] .arrow::after,
.bs-popover-left .arrow::after {
    border-left-color: #282A3C !important;
}

.popover-header {
    background-color: rgb(52, 55, 78) !important;
    border-bottom: 0px !important;
    color: white;
    max-width: 100% !important;
}

.popover-body {
    background-color: #282A3C !important;
    max-width: 100% !important;
    padding: 1.0rem 0.5rem !important;
    text-align: center;
    padding-bottom: 1px !important;
}

.list-active {
    background: #dcdcdc;
}

.active-nav-a {
    font-weight: bold !important;
    color: #6f727d !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
    background: unset !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
    background: #F0F0F2 !important;
    color: black !important;
}

.codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover {
    font-weight: bold !important;
    color: #6f727d !important;
}

.m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
    color: #8c8c8c;
}

.m-nav.m-nav--inline>.m-nav__item:first-child {
    padding-left: 20px !important;
}

.dms.m-nav.m-nav--inline>.m-nav__item {
    padding: 10px 20px 10px 20px !important;
}

.dms.m-nav.m-nav--inline>.m-nav__item.active {
    background: #d4d7da;
}

.dms.m-nav.m-nav--inline>.m-nav__item:hover {
    background: #ececec;
}

.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
    color: #4f668a;
}

.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
    color: black;
}

.m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
    height: 100%;
    padding: 0.8rem 0 0.8rem 0;
}

.m-portlet .m-portlet__head {
    height: 3.9rem;
}

.m-portlet.m-portlet--tabs.active {
    display: block;
}

.dtrAutoIrSideTab {
    padding-left: 0.5rem !important;
    padding-right: 0.5rem !important;
    padding-top: 0.8rem !important;
    padding-bottom: 0.8rem !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__clear {
    margin-right: 1px !important;
}


.editableStyle {
    border-bottom: dashed 1px #0088cc;
    cursor: pointer;
}

/* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
/* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

.select2-container--default .select2-selection--single .select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem !important;
    white-space: normal;
}

.select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
    top: 47% !important;
}

#irAbleDtrViolationDatatable>.m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__foot,
.m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__head {
    background: #e0e0e0;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}

/*NICCA CSS*/
.font-12 {
    font-size: 12px !important;
}

.font-13 {
    font-size: 13px !important;
}

#dms_dashboard select option {
    color: black !important;
}

.m-datatable__head {
    background: #e0e0e0;
}

.highcharts-exporting-group,
.highcharts-credits {
    display: none
}

.highcharts-title {
    font-family: 'Poppins' !important;
}

/* cha css  */

.custom_modal {
    background-color: #060606;
    opacity: 0.8;
}

.label_attach {
    color: #1996f6 !important;
    font-size: larger;
    font-weight: 500;
}

.datepicker.datepicker-inline {
    width: 410px !important;
}

#irAbleDtrViolationMonitoringDatatable.m-datatable.m-datatable--default>.m-datatable__pager {
    margin-top: 0;
}

#irAbleDtrViolationRecordMonitoringDatatable.m-datatable.m-datatable--default>.m-datatable__pager {
    margin-top: 0;
}

#irAbleDtrViolationRecordMonitoringDatatable.m-datatable__body.mCustomScrollbar._mCS_26.mCS-autoHide.mCS_no_scrollbar {
    height: none !important;
}

/* cha end css  */
</style>
<link rel="stylesheet"
    href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">DMS Monitoring</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <input type="hidden" data-tab="1" id="hidden_base_url" name="hidden_base_url" value="<?php echo base_url(); ?>" />
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview" style="padding:0px !important">
                <!-- <ul class="dms m-nav m-nav--inline">
                    <li class="m-nav__item active">
                        <a href="#" class="dmsMonitoringMainNav m-nav__link active" data-navcontent="dms_dashboard">
                            <i class="m-nav__link-icon fa fa-dashboard"></i>
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsMonitoringMainNav m-nav__link" data-navcontent="dtrViolationsMonitoring">
                            <i class="m-nav__link-icon fa fa-clock-o"></i>
                            <span class="m-nav__link-text">DTR Violations Monitoring</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" id="monitoring_records_button" class="dmsMonitoringMainNav m-nav__link"
                            data-navcontent="incidentReportMonitoring">
                            <i class="m-nav__link-icon fa fa-gears"></i>
                            <span class="m-nav__link-text">Incident Report Monitoring</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" id="eroPendingIR_tabbutton" class="dmsMonitoringMainNav m-nav__link"
                            data-navcontent="eroPendingIR">
                            <i class="m-nav__link-icon fa fa-tasks"></i>
                            <span class="m-nav__link-text">ERO Tasks</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsMonitoringMainNav m-nav__link" data-navcontent="monitoringIrProfile"
                            id="monitoringIrProfileContent">
                            <i class="m-nav__link-icon fa fa-user-circle-o"></i>
                            <span class="m-nav__link-text">IR Profile</span>
                        </a>
                    </li>
                </ul> -->
                <?php echo $main_nav; ?>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="dms_dashboard" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" href="" role="tab" onclick="event.preventDefault()">
                                <i class="fa fa-dashboard"></i>IR Dashboard Charts</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-3 col-md-2 col-6 pb-3">
                        <select class="form-control text-capitalize font-13" style="color:#888"
                            id="chart-dashboard-empStat" data-toggle="m-tooltip" data-skin="dark" title=""
                            data-original-title="Employee Status">
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-2 col-6 pb-3">
                        <select name="" class="m-input form-control font-13 " style="color:#888"
                            id="chart-dashboard-class" data-toggle="m-tooltip" data-skin="dark" title=""
                            data-original-title="Employee Type">
                            <option selected disabled hidden>Employee Type</option>
                            <option value="">All Class</option>
                            <option value="Agent">Ambassador</option>
                            <option value="Admin">SZ Team</option>
                        </select>
                    </div>

                    <div class="col-lg-6 col-md-3 col-6 pb-3">
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" class="form-control m-input font-13"
                                style="padding-bottom:12px;color:black" placeholder="Select date range"
                                id="chart-dashboard-date" data-toggle="m-tooltip" data-skin="dark" title=""
                                data-original-title="Incident Dates" />
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                        class="fa fa-calendar"></i></span></span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-3 col-6 pb-3">
                        <select name="" class="m-input form-control font-13" style="color:#888"
                            id="chart-dashboard-accounts" data-toggle="m-tooltip" data-skin="dark" title=""
                            data-original-title="Accounts">
                        </select>
                    </div>

                    <div class="col-lg-6 col-md-2 col-6 pb-3">
                        <select class="form-control text-capitalize font-13" style="color:#888"
                            id="chart-dashboard-natureOffense" data-toggle="m-tooltip" data-skin="dark" title=""
                            data-original-title="Nature of Offense">
                        </select>
                    </div>
                </div>

                <hr />
                <ul class="nav nav-tabs" role="tablist" id="nav-tab-chart">
                    <li class="nav-item">
                        <a class="nav-link active" style="font-weight:600 !important" data-toggle="tab"
                            href="#tab-chart-offensetype"><i class="fa fa-bar-chart m--font-brand"></i> Nature of
                            Offense Breakdown</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="font-weight:600 !important" data-toggle="tab"
                            href="#tab-chart-offense"><i class="fa fa-bar-chart m--font-brand"></i> Incident
                            Breakdown</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" style="font-weight:600 !important" data-toggle="tab"
                            href="#tab-chart-account"><i class="fa fa-bar-chart m--font-brand"></i> Account
                            Breakdown</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-chart-offensetype" role="tabpanel">
                        <div id="dashboard-chart-offensetype" style="height: 550px;margin: 0 auto"></div>
                    </div>
                    <div class="tab-pane" id="tab-chart-offense" role="tabpanel">

                        <div id="dashboard-chart-offense" style="height: 700px;margin: 0 auto"></div>
                    </div>
                    <div class="tab-pane" id="tab-chart-account" role="tabpanel">

                        <div id="dashboard-chart-account" style="height: 900px;margin: 0 auto"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs " id="dtrViolationsMonitoring" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a id="violationLogMonitoringTab" class="nav-link m-tabs__link active" data-toggle="tab"
                                href="#violationsLogMonitoring" role="tab">
                                <i class="fa fa-balance-scale"></i>Violations Log</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrMonitoringTab" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#fileDtrViolationIrMonitoring" role="tab">
                                <i class="fa fa-list-alt"></i>File DTR Violation IR
                                <!-- <span class="m-nav__link-badge ml-2" style="display:none" id="fileDtrViolationIrBadgeMonitoring">
                                    <span class="m-badge m-badge--primary m-badge--wide" id="fileDtrViolationIrBadgeCountMonitoring">23</span>
                                </span> -->
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrRecordMonitoringTab" class="nav-link m-tabs__link"
                                data-toggle="tab" href="#fileDtrViolationIrRecordMonitoring" role="tab">
                                <i class="fa fa-list-alt"></i>Record</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="dismissedDtrViolationsTab" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#dismissedDtrViolations" role="tab">
                                <i class="fa fa-window-close-o"></i>Dismissed Qualified Violations</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="violationsLogMonitoring" role="tabpanel">
                        <div class="row no-gutters">
                            <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                <select name="" class="m-input form-control font-12" id="violation-class"
                                    data-toggle="m-tooltip" title="" data-original-title="Employee Type">
                                    <option value="">All Class</option>
                                    <option value="Agent">Ambassador</option>
                                    <option value="Admin">SZ Team</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                <select name="" class="m-input form-control font-12" id="violation-accounts"
                                    data-toggle="m-tooltip" title="" data-original-title="Accounts">
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="violation-date" data-toggle="m-tooltip"
                                        title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                <select class="form-control text-capitalize font-12" id="violation-type"
                                    data-toggle="m-tooltip" title="" data-original-title="Violation Type">
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Search Name..." id="violation-search" data-toggle="m-tooltip"
                                        title="" data-original-title="Employee Name" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-search"></i></span></span>
                                </div>
                            </div>
                        </div>

                        <h5 id="violation-header" class="mt-3 text-center"></h5>
                        <hr style="border-style: dashed;">
                        <ul class="nav nav-tabs px-1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link show active " data-toggle="tab" href="#violation-chart"><i
                                        class="fa fa-pie-chart m--font-brand"></i> Graphical View</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#violation-tabular"><i
                                        class="fa fa-table m--font-info"></i> Tabular View</a>
                            </li>
                        </ul>
                        <div class="tab-content px-1">
                            <div class="tab-pane show active" id="violation-chart" role="tabpanel">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-xl-6">
                                        <div id="container-chart-pie" style="height: 300px;margin: 0 auto"></div>
                                    </div>
                                    <div class="col-xl-6 py-5 px-4">
                                        <div class="m-widget15">
                                            <div class="m-widget15__items" id="violationList">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="violation-tabular" role="tabpanel">
                                <div class="m_datatable" id="datatable_dtrviolations"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIrMonitoring" role="tabpanel">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpSelectMonitoring"
                                    id="fileDtrViolationEmpSelectMonitoring" name="fileDtrViolationEmpSelectMonitoring"
                                    data-toggle="m-tooltip" title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeMonitoring"
                                    name="violationTypeMonitoring" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Late Clockout</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control m-input datePicker" readonly=""
                                            id="dtrViolationSchedMonitoring" name="dtrViolationSchedMonitoring"
                                            placeholder="Select a Date">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <select class="custom-select form-control" id="fileDtrViolationMonitoringStatus"
                                    name="fileDtrViolationMonitoringStatus" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="4">Pending</option>
                                    <option value="12">Missed</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolationMonitoring"
                                    style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12"
                                        style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolationMonitoring" style="display:none;">
                                    <div class="col-12">
                                        <div class="alert alert-dismissible fade show m-alert m-alert--air m-alert--outline alert-warning hide"
                                            id="missedAlert" role="alert" style="display:none">
                                            <strong id="missedHighlight">Error</strong> <span id="missedMssg">This is a
                                                test message</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-2 p-0">
                                        <div class="col-md-12" id="pendingLateCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="pendingLateCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingObCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="pendingObCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">OB</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingUtCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="pendingUtCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">UT</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingForgotToLogoutCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="pendingForgotToLogoutCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">LCO</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncBreakCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="pendingIncBreakCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncDtrCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="pendingIncDtrCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc DTR</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingAbsentCountDivMonitoring" style="">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="pendingAbsentCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-10 pl-0">
                                        <div id="irAbleDtrViolationMonitoringDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIrRecordMonitoring" role="tabpanel">
                        <div class="row">
                            <div class="col-md-5 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpMonitoringRecordSelect"
                                    id="fileDtrViolationEmpMonitoringRecordSelect"
                                    name="fileDtrViolationEmpRecordSelect" data-toggle="m-tooltip"
                                    title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly=""
                                        placeholder="Select date range" id="fileDtrViolationMonitoringRecordDateRange">
                                    <div class="input-group-append" data-toggle="m-tooltip" title=""
                                        data-original-title="Date Schedule">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeMonitoringRecord"
                                    name="violationTypeMonitoringRecord" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Late Clockout</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <!-- <div class="col-md-2 mb-3">
                                <select class="custom-select form-control" id="fileDtrViolationMonitoringRecordStatus" name="fileDtrViolationMonitoringRecordStatus" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="17">Liable</option>
                                    <option value="18">Non-Liable</option>
                                </select>
                            </div> -->
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolationRecordMonitoring"
                                    style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12"
                                        style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Record Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolationRecordMonitoring" style="display:none">
                                    <div class="col-md-12 col-lg-2 p-0">
                                        <div class="col-md-12" id="recordLateCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="recordLateCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordObCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordObCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">OB</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordUtCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordUtCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">UT</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordForgotToLogoutCountDivMonitoring"
                                            style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="recordForgotToLogoutCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">LCO</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncBreakCountDivMonitoring"
                                            style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="recordIncBreakCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">INC BREAK</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncDtrCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="recordIncDtrCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">INC DTR</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordAbsentCountDivMonitoring" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show"
                                                role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon"
                                                        style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20"
                                                        id="recordAbsentCountMonitoring">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-10 pl-0">
                                        <div id="irAbleDtrViolationRecordMonitoringDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="dismissedDtrViolations" role="tabpanel">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <select class="form-control m-select2 dismissedEmpMonitoring"
                                    id="dismissedEmpMonitoring" name="dismissedEmpMonitoring" data-toggle="m-tooltip"
                                    title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly=""
                                        placeholder="Select date range" id="dismissedEmpMonitoringDateRange"
                                        style="font-size: 0.8rem;">
                                    <div class="input-group-append" data-toggle="m-tooltip" title=""
                                        data-original-title="Date Schedule">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeMonitoringDismissed" name="violationTypeMonitoringDismissed" data-toggle="m-tooltip" title="Violations">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Late Clockout</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-md-2 mb-2">
                                <button
                                    class="btn btn-outline-success btn-sm m-btn m-btn--custom m-btn--icon m-btn--pill"
                                    id="downloadDismissed" disabled="disabled">
                                    <span>
                                        <i class="la la-download"></i>
                                        <span>Export Excel</span>
                                    </span>
                                </button>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noDismissedQualifiedDtrViolations"
                                    style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12"
                                        style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No Dismissed DTR Violation Record
                                                Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="dismissedQualifiedDtrViolations" style="display:none">
                                    <div class="col-md-12 pl-0">
                                        <div id="dismissedQualifiedDtrVioDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="incidentReportMonitoring" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a id="pendingir_Tab" class="nav-link m-tabs__link active" data-toggle="tab" href="#irPendingRecords" role="tab" data-type="">
                                <i class="fa fa-spinner"></i>Pending IR Records</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="allrecordsir_Tab" class="nav-link m-tabs__link" data-toggle="tab" href="#irRecords"
                                role="tab" data-type="">
                                <i class="fa fa-list-alt"></i>IR Records</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="susTermination_Tab" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#susTermRecords" role="tab">
                                <i class="fa fa-calendar"></i>Suspension and Termination Dates</a>
                        </li>
                        <!-- <li class="nav-item m-tabs__item">
                            <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab" href="#effectivityDatesTab" role="tab">
                                <i class="fa fa-legal"></i>Effectivity Dates</a>
                        </li> -->
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="irPendingRecords" role="tabpanel">
                        <div class="row">
                            <div class="col-md-4 mb-2">
                                <select name="" class="m-input form-control font-12 m-select2" id="employeepending_type"
                                    data-toggle="m-tooltip" title="" data-original-title="Employee Type">
                                    <!-- <option value="">All</option>
                                    <option value="Admin" class="AgentHide" data-class="">Admin
                                    </option>
                                    <option value="Agent" class="AdminHide" data-class="">Agent</option> -->
                                </select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select name="" class="m-input form-control font-12" id="reportpending_type"
                                    data-toggle="m-tooltip" title="" data-original-title="Report Type">
                                    <option value="">All</option>
                                    <option value="1">Normal</option>
                                    <option value="2">QIP</option>
                                </select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="irpending_subjectname"
                                    id="irpending_subjectname"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="irpending_account"
                                    id="irpending_account"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="irpending_natureofoffense"
                                    id="irpending_natureofoffense"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="irpending_offense"
                                    id="irpending_offense"></select>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID"
                                            id="pendingir_searchid">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1"
                                        style="font-size: 10px;">NOTE: Search format must be similar to the
                                        data below.</span>
                                </div>
                            </div>
                            <div class="col-md-12" id="ir_col_pending">
                                <div id="irrecords_pendingDatatable">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12" id="irRecords" role="tabpanel">
                        <div class="row">
                            <div class="col-md-3 mb-2">
                                <select name="" class="m-input form-control font-12 m-select-2" id="employeeall_type"
                                    data-toggle="m-tooltip" title="" data-original-title="Employee Type">
                                    <!-- <option value="">All</option>
                                    <option value="Admin" class="AgentHide" data-class="">Admin
                                    </option>
                                    <option value="Agent" class="AdminHide" data-class="">Agent</option> -->
                                </select>
                            </div>
                            <div class="col-md-3 mb-2">
                                <select name="" class="m-input form-control font-12" id="liabilityall_type"
                                    data-toggle="m-tooltip" title="" data-original-title="Liability Status">
                                    <option value="">All</option>
                                    <option value="17" data-class="">Liable</option>
                                    <option value="18" data-class="">Not Liable</option>
                                    <option value="2" data-class="">Pending</option>
                                </select>
                            </div>
                            <div class="col-md-3 mb-2">
                                <select name="" class="m-input form-control font-12" id="reportall_type"
                                    data-toggle="m-tooltip" title="" data-original-title="Report Type">
                                    <option value="">All</option>
                                    <option value="1">Normal</option>
                                    <option value="2">QIP</option>
                                </select>
                            </div>
                            <div class="col-md-3 mb-2">
                                <select class="form-control m-select2" name="accall_select" id="accall_select"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="ir_subject" id="irall_subject"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="offenseall_filter"
                                    id="offenseall_filter"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="natureall_offense"
                                    id="natureall_offense"></select>
                            </div>
                            <div class="col-md-5 mb-2">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px"
                                        placeholder="Select date range" id="recordsall-date" data-toggle="m-tooltip"
                                        title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-md-5 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID"
                                            id="reportsall_irid">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1"
                                        style="font-size: 10px;">NOTE: Search format must be similar to the
                                        data below.</span>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <button
                                    class="btn btn-outline-success btn-sm m-btn m-btn--custom m-btn--icon m-btn--pill mt-2"
                                    id="btn-downloadallirrecords">
                                    <span>
                                        <i class="la la-download"></i>
                                        <span>Export Excel</span>
                                    </span>
                                </button>
                            </div>
                            <div class="col-md-12">
                                <div id="irrecordsDatatable"></div>
                            </div>
                            <!-- ========= -->
                            <div class="col-md-12" id="ir_col_all">
                                <div id="irrecords_allDatatable">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12" id="susTermRecords" role="tabpanel">
                        <div class="row">
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="irpending_subjectname"
                                    id="irsusterm_subjectname"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select2" name="irpending_account"
                                    id="irsusterm_account"></select>
                            </div>
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select" name="irpending_account"
                                    id="irsusterm_discAction">
                                    <option value="">All</option>
                                    <option value="s">Suspension</option>
                                    <option value="t">Termination</option>
                                </select>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID"
                                            id="irsusterm_searchid">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1"
                                        style="font-size: 10px;">NOTE: Search format must be similar to the
                                        data below.</span>
                                </div>
                            </div>
                            <div class="col-md-12" id="ir_col_susterm">
                                <div id="irsusterm_Datatable">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="eroPendingIR" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a id="pendingRec_Tab" class="nav-link m-tabs__link active" data-toggle="tab"
                                href="#pendingRecom" role="tab">
                                <i class="fa fa-spinner"></i>Pending Recommendations</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="pending_uploadtab" class="nav-link m-tabs__link" data-toggle="tab" href="#pendingUp"
                                role="tab">
                                <i class="fa fa-upload"></i>Pending Uploads</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="eroMissedDeadline_tab" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#missedDead" role="tab">
                                <i class="fa fa-clock-o"></i>ERO Missed Deadlines</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="pendingRecom" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select class="form-control m-select2" name="pendingRecsubject_select"
                                    id="pendingRecsubject_select"></select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select class="form-control m-select2" name="pendingEroRec_select"
                                    id="pendingEroRec_select"></select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control font-12" id="recoType_file"
                                    data-toggle="m-tooltip" title="" data-original-title="Recommendation Type">
                                    <option value="0">All</option>
                                    <option value="2" data-class="">Last</option>
                                    <option value="3" data-class="">Final</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID"
                                            id="eropendingRec_irid">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1"
                                        style="font-size: 10px;">NOTE: Search format must be similar to the
                                        data below.</span>
                                </div>
                            </div>
                            <div class="col-md-12" id="ir_col_penRec">
                                <div id="erorecommendation_pendingDatatable">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12" id="pendingUp" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select class="form-control m-select2" name="pendingsubject_select"
                                    id="pendingsubject_select"></select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select class="form-control m-select2" name="pendingEroUp_select"
                                    id="pendingEroUp_select"></select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select name="" class="m-input form-control font-12" id="requiredero_file"
                                    data-toggle="m-tooltip" title="" data-original-title="Required file type">
                                    <option value="0">All</option>
                                    <option value="3" data-class="">NTE</option>
                                    <option value="2" data-class="">NOR</option>
                                </select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID"
                                            id="eropendingUp_irid">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1"
                                        style="font-size: 10px;">NOTE: Search format must be similar to the
                                        data below.</span>
                                </div>
                            </div>
                            <div class="col-md-12" id="ir_col_penUps">
                                <div id="pendinguploads_Datatable">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12" id="missedDead" role="tabpanel">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select class="form-control m-select2" name="pendingmissedDeadline_subjectname"
                                    id="pendingmissedDeadline_subjectname"></select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select class="form-control m-select2" name="pendingmissedDeadline_eroname"
                                    id="pendingmissedDeadline_eroname"></select>
                            </div>
                            <div class="col-md-6 mb-2">
                                <select class="form-control m-select2" name="pendingmissedDeadline_task"
                                    id="pendingmissedDeadline_task"></select>
                            </div>
                            <!--                             
                            <div class="col-md-4 mb-2">
                                <select class="form-control m-select" name="irpending_account" id="irsusterm_discAction">
                                    <option value="">All</option>
                                    <option value="s">Suspension</option>
                                    <option value="t">Termination</option>
                                </select>
                            </div> -->
                            <div class="col-md-6 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID"
                                            id="pendingmissedDeadline_searchid">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1"
                                        style="font-size: 10px;">NOTE: Search format must be similar to the
                                        data below.</span>
                                </div>
                            </div>
                            <div class="col-md-12" id="ir_col_missedDead">
                                <div id="missedDeadline_Datatable">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12" style="background: white;margin-bottom: 1.5rem;display:none" id="empIrProfileSelect">
            <div class="form-group m-form__group row p-2">
                <label class="col-lg-3 col-form-label font-weight-bold">Select Employee Name:</label>
                <div class="col-lg-9">
                    <select class="form-control m-select2 text-truncate" id="empListHistory" name="empListHistory"
                        data-toggle="m-tooltip" title="Select Employee Name"></select>
                </div>
            </div>
        </div>
        <div class="" id="monitoringIrProfile"
            style="box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;margin-bottom: 2.2rem;display:none">
            <div class="" style="padding: 2.2rem 2.2rem;color: #575962;">
                <div class="tab-content">
                    <div class="row p-4"
                        style="margin-left: -3.3rem;margin-right:-3.3rem;margin-top: -2.3rem;margin-bottom: -2.3rem; box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">
                        <div class="col-12 col-md-3 mb-2">
                            <img src="" onerror="noImageFound(this)" alt="" class="img-fluid mx-auto d-block"
                                style="border: 0.3rem solid gainsboro;" id="profilePic">
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="row mb-3">
                                <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Employee
                                    Information</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="row pl-3 pr-5 mb-3">
                                <div class="col-12 col-md-4 font-weight-bold">Fullname:</div>
                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"
                                    id="profileName"></div>
                                <div class="col-12 col-md-4 font-weight-bold">Current Position:</div>
                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">
                                    <span id="profilePosition"
                                        style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span>
                                    - <span id="profileEmpStatus" style="font-size: 11px;color: #0012ff;"></span></div>
                                <div class="col-12 col-md-4 font-weight-bold">Current Team:</div>
                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"
                                    id="profileAccount"></div>
                            </div>
                            <div class="row mb-4">
                                <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">IR Record
                                    Range</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="input-group pull-right">
                                        <input type="text" class="form-control m-input" readonly=""
                                            placeholder="Select date range" id="irProfileDateRange"
                                            style="background: white;">
                                        <div class="input-group-append" data-toggle="m-tooltip" title=""
                                            data-original-title="Date Schedule">
                                            <span class="input-group-text" style="background: #fbfbfb !important;">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <select class="custom-select form-control" id="irProfileLiabilityStat"
                                        name="irProfileLiabilityStat" placeholder="Select a liability">
                                        <option value="0">All</option>
                                        <option value="17">Liable</option>
                                        <option value="18">Non-Liable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-row--no-padding" id="showIrPofileRecords" style="display:none">
            <div class="col-xl-7 pr-xl-4">
                <div class=""
                    style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 2.2rem 2.2rem;">
                        <div class="m-widget26">
                            <div class="m-widget26__content">
                                <div class="natureOffenseChart" id="natureOffenseChart"
                                    style="overflow: visible !important;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5">
                <div class=""
                    style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 2.2rem 2.2rem;">
                        <div class="m-widget26">
                            <div class="m-widget26__content text-center">
                                <span class="mr-2">
                                    <i class="fa fa-file-text" style="font-size: 2rem;color: #ff5371;"></i>
                                </span>
                                <span class="m-widget26__number mr-2" id="irTotalCount">640</span>
                                <span class="m-widget26__desc" style="font-weight: 500 !important;">Total Incident
                                    Report Count</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""
                    style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 0 !important">
                        <div class="m-widget1" style="padding-top: 1rem !important;padding-bottom: 1rem !important"
                            id="discActionCount">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12" id="offenseHistory" style="display:none">
                <div class=""
                    style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 2.2rem 2.2rem;">
                        <div class="m-widget26">
                            <div class="m-widget26__content">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <select class="form-control m-select2 text-truncate" id="specificOffenseHistory"
                                            name="specificOffenseHistory" data-toggle="m-tooltip"
                                            title="Select Employee Name"></select>
                                    </div>
                                    <div class="col-10 col-md-3">
                                        <div class="form-group m-form__group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search by IR-ID"
                                                    id="searchProfileHistory">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2 col-md-1">
                                        <button
                                            class="btn btn-outline-success m-btn m-btn--icon btn-md m-btn--icon-only m-btn--pill m-btn--air"
                                            id="exportHistoryBtn">
                                            <i class="la la-download"></i>
                                        </button>
                                    </div>
                                    <div class="col-12">
                                        <div class="mt-1" id="profileIrHistoryDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="noIrPofileRecords" style="display:none;">
            <div class="col-12 py-4 mx-3 text-center" style="background: white;">
                <i style="font-size: 42px;margin-right:1rem" class="fa fa-file-o"></i>
                <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">No Records To Show</span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="irHistoryProfile" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static"
    aria-hidden="true" data-pw="" data-irid="" data-recommenlvl="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="m-form m-form--fit m-form--label-align-right form-horizontal">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title">Incident Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row ir_details_template" id="layoutHistoryIrDetails">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="qualifiedDetailsModalMonitor" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">DTR Violation Details</h5>
                <button type="button" class="close closeModalBtn" data-dismiss="modal" aria-label="Close"
                    style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 col-lg-3">
                                <img id="empPicApprovalMonitor" src="http://3.18.221.50/sz/assets/images/img/sz.png"
                                    onerror="noImageFound(this)" width="75" alt=""
                                    class="rounded-circle mx-auto d-block">
                            </div>
                            <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                <div class="row">
                                    <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;"
                                        id="qualifiedViewEmployeeNameMonitor">Jamelyn A. Sencio
                                    </div>
                                    <div class="col-md-12 text-center text-md-left"><span
                                            id="qualifiedViewEmployeeJobMonitor"
                                            style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span><span
                                            id="qualifiedViewPosEmpStat" style="font-size: 11px;color: #0012ff;"></span>
                                    </div>
                                    <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px"
                                        id="qualifiedViewEmployeeAccountMonitor">ONYX Enterprises</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="row p-2">
                            <div class="col-12">
                                <div class="row pl-4 pr-4">
                                    <span class="font-weight-bold mr-2">Incident Date:</span>
                                    <span class="" id="qualifiedViewDateIncidentMonitor">Dec 4, 2018</span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row pl-4 pr-4">
                                    <span class="font-weight-bold mr-2">Incident:</span>
                                    <span class="" id="qualifiedViewIncidentTypeMonitor"
                                        style="color: #f50000;font-weight: 600;">Forgout to Logout</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                    class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row pl-5 pr-5" id="incidentDetailsViewMonitor"></div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                    class="la la-tag" style="font-size: 1.1rem;"></i>Expected Course Of
                                Action</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-12 pl-5 pr-5">
                            <div class="col-md-12 ahr_quote pt-3 pb-3 text-center"
                                style="background: #e7ffe8 !important;">
                                <blockquote style="margin: 0 0 0rem;">
                                    <i class="fa fa-quote-left"></i>
                                    <span class="mb-0" id="qualifiedExpectedActionViewMonitor"
                                        style="font-size: 13px"></span>
                                    <i class="fa fa-quote-right"></i>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div id="withRuleViewMonitor" style="display:none">
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                        class="la la-tag" style="font-size: 1.1rem;"></i>IR Rule</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-1 font-weight-bold">
                                    Rule:
                                </div>
                                <div class="col-12 col-md-11 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">
                                    <i style="font-size: 13px; font-weight: 400;" id="qualifiedRuleViewMonitor"></i>
                                </div>
                                <div class="col-12">
                                    <div class="row" id="withPreviousUserDtrViolationViewMonitor">
                                        <div class="col-12 mt-2 font-weight-bold">
                                            Previous Consecutive Occurence:
                                        </div>
                                        <div class="col-12 mt-2" id="previousDetailsViewMonitor">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                    class="la la-tag" style="font-size: 1.1rem;"></i>Reason for dismissal</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-12 pl-5 pr-5">
                            <div class="row py-3"
                                style="background: aliceblue;margin-left: 0.1rem;border-left: 8px solid #545455!important;margin-right: 0.1rem;">
                                <div class="col-sm-12 col-md-4 col-lg-2">
                                    <img id="directSupPicMonitor" src="" onerror="noImageFound(this)" width="50" alt=""
                                        class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-10 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 text-center text-md-left" style="font-size: 1rem;"
                                            id="directSupNameMonitor">Mark Jeffrey Magparoc</div>
                                        <div class="col-md-12 text-center text-md-left" id="directSupJobMonitor"
                                            style="font-size: 0.8rem;/* margin-top: -8px !important; */font-weight: bolder;color: darkcyan;">
                                            Senior Software Developer II</div>
                                        <div class="col-md-12 text-center text-md-left" id="dismissDate"
                                            style="font-size: 0.8rem;/* margin-top: -8px !important; */font-weight: bolder">
                                        </div>
                                        <div class="col-md-12 pt-3 font-italic" id="reasonMonitor">Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->

<!--begin: Charise :Modal-->
<div class="modal fade" data-ir="0" id="modal_displayallirevidences" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header custom-evidence-modal">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #fff !important;">
                    <!-- <i class="fa fa-paperclip"></i>  -->
                    IR Attachments
                </h5>
                <button type="button" id="media_closebutton2" class="close closeModalBtn" data-dismiss="modal"
                    aria-label="Close" style="color: #fff !important;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mb-5" style="margin-top:-21px !important">
                <div class="m-portlet__body">
                    <div class="row" data-irid="0" id="row_irid">
                        <div class="col-lg-12 col-md-12">
                            <label class="label_attach mt-3 mb-3" id="label_attach"></label>
                            <div class="row" style="padding:10px">
                                <div class="col-lg-12 col-md-12">
                                    <label class="label_select" id="">Attachment Types</label>
                                    <select class="form-control m-select" id="evidenceall-types">
                                        <option value="">All</option>
                                        <option value="1">Photos</option>
                                        <option value="2">Videos</option>
                                        <option value="3">Audios</option>
                                        <option value="4">Files</option>
                                        <option value="5">Others</option>
                                    </select>
                                </div>
                                <!-- 
                                <div class="col-lg-6 col-md-12">
                                    <label class="label_select" id="">Attachment Purpose</label>
                                    <select class="form-control m-select" id="attachment-purpose">
                                        <option value="">All</option>
                                        <option value="1">Evidences</option>
                                        <option value="4">HR attachment</option>
                                    </select>
                                </div> -->
                            </div>
                            <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-5 text-center"
                                style="max-height: 250px; height: 250px; position: relative; overflow: visible;padding: 13px;">
                                <div class="row" id="mediairall_display">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- File an IR modal -->

<div class="modal fade" id="calendar_displaydate" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" data-date="" data-origdate="" data-termid="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="m-form m-form--fit m-form--label-align-right form-horizontal" method="post">
                <div class="modal-header" style="padding: 18px !important;">
                    <!-- <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span> -->
                    <h5 id="modaldate_title" class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12 mb-3" id="remaining_days">
                            <!-- <h5 id="remaining_days"></h5> -->
                        </div>
                        <div class="col-12">
                            <ul id="irsusterm_dates" class="list-group">
                                <!-- <li class="list-group-item"><i class="fa fa-calendar-check-o text-info mx-2"></i>David Copperfield
                                </li>
                                <li class="list-group-item"><i class="fas fa-venus text-warning mx-2"></i>The Portrait
                                    of a Lady</li>
                                <li class="list-group-item"><i class="fas fa-gavel text-danger mx-2"></i> The Trial</li> -->
                            </ul>
                        </div>
                        <!-- <div class="col-12">
                            <label for="exampleTextarea" id="termNote"></label>
                            <div class="datepicker" id="susTermDatepicker_all"></div>
                        </div> -->
                    </div>
                </div>
                <div class="modal-footer div-datepicker">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade yu" id="audio_modal_evidences3" tabindex="-1" role="dialog" aria-labelledby='exampleModalLabel'
    aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content custom_modal'>
            <div class='modal-body'>
                <div class='col-12' id="audio_evidence_player3">
                </div>
                <button type='button' class='btn btn-link' data-dismiss="modal">Close</button>";
            </div>
        </div>
    </div>
</div>

<!-- ====== -->


<div class="modal fade" id="modal_attachedfiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header custom-modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #6d6565 !important;"> HR Attachment
                </h5>
                <button type="button" id="media_closebutton2" class="close" data-dismiss="modal" aria-label="Close"
                    style="color: #6d6565 !important;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body">
                    <div class="row" data-irid="0" id="row_irid">
                        <div class="col-lg-5 col-md-12">
                            <form id="upload_evidence" method="post">
                                <div class="col-12 text-center mt-3">
                                    <span style="font-size:smaller">Each file to attach should not exceed
                                        to <strong style="color:#558e82;">5MB.</strong> Allowed file types: <strong
                                            style="color:#c096ca">
                                            doc, docx, pdf, txt, xml, xlsx, xlsm, ppt, pptx, jpeg, jpg, png, gif, mp4,
                                            mov, 3gp,
                                            wmv, avi, mpeg, mp3, wav </strong></span><br><br>

                                    <label
                                        class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"
                                        id="add_evidences_button"><i class="fa fa-plus"></i> Add Attachment
                                        <input type="file" name="upload_evidence_files[]" id="upload_evidence_files"
                                            style="display: none; " onchange="makeFileList()" multiple>
                                    </label>
                                    <br><label id="files_select_label" style="color:green;"></label>
                                </div>
                                <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-3"
                                    style="max-height: 100px; height: 100px; position: relative; overflow: visible;"
                                    id="display_div_file">
                                    <ul id="fileList" style="list-style-type: none;">
                                    </ul>
                                </div>
                                <div class="text-center" id="sub_button" style="display:none;"><button id="up_button"
                                        type="submit"
                                        class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air"><i
                                            class="fa fa-upload"></i> Upload </button>
                                </div>
                            </form>
                        </div>

                        <div class="col-lg-7 col-md-12 ">
                            <label class="label_attach" id="label_attach"></label>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <label class="label_select" id="">Attachment Types</label>
                                    <select class="form-control m-select" id="evidence-types_ero">
                                        <option value="">All</option>
                                        <option value="1">Photos</option>
                                        <option value="2">Videos</option>
                                        <option value="3">Audios</option>
                                        <option value="4">Others</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label class="label_select" id="">Attachment Purpose</label>
                                    <select class="form-control m-select" id="attachment-purpose">
                                        <!-- <option value="">All</option>
                                        <option value="1">Evidences</option>
                                        <option value="4">HR attachment</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-5"
                                style="max-height: 250px; height: 250px; position: relative; overflow: visible;">
                                <!-- <div class="row" id="evidence_display"> -->
                                <div class="row" id="attachment_display_ero">
                                </div>
                                <span id="passed_irid"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- details modal  -->
<div class="modal fade" id="allirDetails" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">Incident Report Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row ir_details_template" id="allir_filed_details" data-irid="0"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <span>
                        <i class="fa fa-close"></i>
                        <span>Close</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- qualified modal -->


<div class="modal fade" id="qualifiedModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="qualifiedDtrVioForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">File DTR Violation Incident Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-3">
                                    <img id="empPicApproval" src="http://3.18.221.50/sz/assets/images/img/sz.png"
                                        onerror="noImageFound(this)" width="75" alt=""
                                        class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-md-left"
                                            style="font-size: 19px;" id="qualifiedEmployeeName">Jamelyn A. Sencio </div>
                                        <div class="col-md-12 text-center text-md-left"><span id="qualifiedEmployeeJob"
                                                style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span><span
                                                id="qualifiedPosEmpStat" style="font-size: 11px;color: #0012ff;"></span>
                                        </div>
                                        <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px"
                                            id="qualifiedEmployeeAccount">ONYX Enterprises</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row p-2">
                                <div class="col-12">
                                    <div class="row pl-4 pr-4">
                                        <span class="font-weight-bold mr-2">Incident Date:</span>
                                        <span class="" id="qualifiedDateIncident">Dec 4, 2018</span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row pl-4 pr-4">
                                        <span class="font-weight-bold mr-2">Incident:</span>
                                        <span class="" id="qualifiedIncidentType"
                                            style="color: #f50000;font-weight: 600;">Forgout to Logout</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                        class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5" id="incidentDetails"></div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                        class="la la-tag" style="font-size: 1.1rem;"></i>Expected Course Of
                                    Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 pl-5 pr-5">
                                <div class="col-md-12 ahr_quote pt-3 pb-1 text-center"
                                    style="background: #e7ffe8 !important;">
                                    <blockquote>
                                        <i class="fa fa-quote-left"></i>
                                        <span class="mb-0" id="qualifiedExpectedAction"></span>
                                        <i class="fa fa-quote-right"></i>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 withRule" style="display:none">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                        class="la la-tag" style="font-size: 1.1rem;"></i>IR Rule</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 withRule" style="display:none">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-1 font-weight-bold">
                                    Rule:
                                </div>
                                <div class="col-12 col-md-11 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">
                                    <i style="font-size: 13px; font-weight: 400;" id="qualifiedRule"></i>
                                </div>
                                <div class="col-12">
                                    <div class="row" id="withPreviousUserDtrViolation">
                                        <div class="col-12 mt-2 font-weight-bold">
                                            Previous Consecutive Occurence:
                                        </div>
                                        <div class="col-12 mt-2" id="previousDetails">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="withIrHistory" class="col-12" style="display:none">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row p-3">
                                        <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                                class="la la-tag" style="font-size: 1.1rem;"></i>IR History</span>
                                        <div class="col">
                                            <hr style="margin-top: 0.8rem !important;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row pl-5 pr-5">
                                        <div class="col-12 mt-2" id="irHistoryDetails">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                        class="la la-tag" style="font-size: 1.1rem;"></i> Offense Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold">Nature of Offense:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"
                                    id="qualifiedOffenseType"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Offense:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"><i
                                        style="font-size: 13px; font-weight: 400; color: #c31717;"
                                        id="qualifiedOffense"></i></div>
                                <div class="col-12 col-md-3 font-weight-bold">Category:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"
                                    id="qualifiedCategory"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                        class="la la-tag" style="font-size: 1.1rem;"></i>Suggested Disciplinary
                                    Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold">Level:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"
                                    id="qualifiedLevel"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Disciplinary Action:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 17px; font-weight: 500"
                                    id="qualifiedDisciplinaryAction"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Period:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"
                                    id="qualifiedPeriod"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Cure Date:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"
                                    id="qualifiedCureDate"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><span>Close</span></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--end::Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js"
    type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_ir_profile.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_monitoring.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_dtrviolation.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_monitoring_cha.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_ir_chart.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>
<script type="text/javascript">
function initDtrViolations(){
//------------------NICCA CODES STARTS HERE---------------------------------------------------------------------
    var myoptions = {
        innerSize: 60,
        depth: 45,
        dataLabels: {
            distance: 1
        }
    };
    var currentdate = tz_date(moment());
    var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
    var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
    var datatable_dtrviolations = datatable_dtrviolations_init("get_monitoring_dtr_system_violations", {
        query: {
            incidentDate_start: moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD'),
            incidentDate_end: moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD'),
            violation_accounts: $("#violation-accounts").val(),
            violation_type: $("#violation-type").val(),
            violation_class: $("#violation-class").val()
        }
    });
    $('#violation-date').val(start + ' - ' + end);
    $('#violation-date').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        opens: "left",
        ranges: {
            'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
            'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate,
                'YYYY-MM-DD').subtract(1, 'days')],
            'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(
                currentdate, 'YYYY-MM-DD')],
            'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate,
                'YYYY-MM-DD').endOf('month')],
            'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'),
                moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')
            ]
        }
    }, function(start, end, label) {
        $('#violation-date .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
            'MM/DD/YYYY'));
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['incidentDate_start'] = moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD');
        query['incidentDate_end'] = moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD');
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Monitoring');
    });
    set_violation_types();
    get_all_accounts(function(res) {
        $("#violation-accounts").html("<option value=''>All</option>");
        $.each(res.accounts, function(i, item) {
            $("#violation-accounts").append("<option value='" + item.acc_id +
                "' data-class='" + item.acc_description + "'>" + item.acc_name +
                "</option>");
        });
    });
    $("#violation-class").change(function() {
        var theclass = $(this).val();
        $("#violation-accounts").children('option').css('display', '');
        $("#violation-accounts").val('').change();
        if (theclass !== '') {
            $("#violation-accounts").children('option[data-class!="' + theclass + '"][value!=""]')
                .css('display', 'none');
        }
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['violation_class'] = $("#violation-class").val();
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Monitoring');
    });
    $("#violation-accounts,#violation-type").change(function() {
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['violation_accounts'] = $("#violation-accounts").val();
        query['violation_type'] = $("#violation-type").val();
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Monitoring');
    });
    $('#violation-search').keyup(function() {
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['violation_search'] = $("#violation-search").val();
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Monitoring');
    });
    initialize_chart_dtrviolation(myoptions, 'Monitoring');
//------------------NICCA CODES ENDS HERE---------------------------------------------------------------------
}
$(function(){
    let class_val = "<?php echo $class; ?>";
    let class_arr = class_val.split(',');
    let access = 0;
    let optionsAsString = "";
    if(class_arr=="Admin" || class_arr=="Agent"){
        access=class_arr.toString();
    }
    $.each(class_arr, function(index, val){
        let option_val = val;
        if(option_val == 'All'){
            option_val = 0;
        }
        optionsAsString += '<option value="'+option_val+'" data-class="">'+val+'</option>';
    });
    console.log(optionsAsString);
    $("#employeepending_type, #employeeall_type").html(optionsAsString);
    $('#employeepending_type, #employeeall_type').select2({
        placeholder: "Select a class",
        width: '100%'
    }).trigger('change');;
    
    $("#pendingir_Tab").data('type',access);
    $("#allrecordsir_Tab").data('type',access);
    
    $active_main_nav = $(".m-nav--inline").find(".dmsMonitoringMainNav.active").data('navcontent');
    if($active_main_nav == "dms_dashboard"){
        initDtrViolations();
    }
    $('#'+$active_main_nav).addClass("active");
    $('#'+$active_main_nav).fadeIn();
    $(".m-nav--inline").find(".dmsMonitoringMainNav.active").trigger('click');
})
</script>