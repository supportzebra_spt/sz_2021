<style>
    .bs-popover-auto[x-placement^=right] .arrow::after,
    .bs-popover-right .arrow::after {
        border-right-color: #282A3C !important;
    }

    .bs-popover-auto[x-placement^=left] .arrow::after,
    .bs-popover-left .arrow::after {
        border-left-color: #282A3C !important;
    }

    .popover-header {
        background-color: rgb(52, 55, 78) !important;
        border-bottom: 0px !important;
        color: white;
        max-width: 100% !important;
    }

    .popover-body {
        background-color: #282A3C !important;
        max-width: 100% !important;
        padding: 1.0rem 0.5rem !important;
        text-align: center;
        padding-bottom: 1px !important;
    }

    .list-active {
        background: #dcdcdc;
    }

    .active-nav-a {
        font-weight: bold !important;
        color: #6f727d !important;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
        background: unset !important;
    }

    .m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
        background: unset !important;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
        background: #F0F0F2 !important;
        color: black !important;
    }

    .codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover {
        font-weight: bold !important;
        color: #6f727d !important;
    }

    .m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
        color: #8c8c8c;
    }

    .m-nav.m-nav--inline>.m-nav__item:first-child {
        padding-left: 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item {
        padding: 10px 20px 10px 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active {
        background: #d4d7da;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item:hover {
        background: #ececec;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
        color: #4f668a;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
        color: black;
    }

    .m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
        height: 100%;
        padding: 0.8rem 0 0.8rem 0;
    }

    .m-portlet .m-portlet__head {
        height: 3.9rem;
    }

    .m-portlet.m-portlet--tabs.active {
        display: block;
    }

    .dtrAutoIrSideTab {
        padding-left: 0.5rem !important;
        padding-right: 0.5rem !important;
        padding-top: 0.8rem !important;
        padding-bottom: 0.8rem !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        margin-right: 1px !important;
    }


    .editableStyle {
        border-bottom: dashed 1px #0088cc;
        cursor: pointer;
    }

    /* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
    /* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding: 0.65rem 2.9rem 0.8rem 1rem !important;
        white-space: normal;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
        top: 47% !important;
    }

    thead.m-datatable__head {
        background: #d6d6d6;
    }

    /* charise start code */
    .not_included {
        color: red;
    }

    .container_file {
        margin: auto;
        background: #ececec;
        border-style: groove;
        display: none;
        /* padding: 5px; */
    }

    .custom-modal-header {
        padding: 20px !important;
        background: #dee6e5 !important;
    }

    .div_media {
        /* border-right: 2px solid #ebedf2; */
        margin: auto;
        height: 320px;
    }

    /* img.h_wdisplay {
    width: 100px;
    height: 100px;

    /*Scale down will take the necessary specified space that is 100px x 100px without stretching the image*/
    /* object-fit: cover; */
    /* } */

    .label_attach {
        color: #1996f6;
        font-size: larger;
        font-weight: 500;
    }

    .label_select {
        color: #2da78e;
        font-size: unset;
    }

    .icon_size {
        font-size: 90px;
        color: cadetblue;
    }

    .icon_size2 {
        font-size: 90px;
        color: #bb80a9;
    }

    video::-internal-media-controls-download-button {
        display: none;
    }

    video::-webkit-media-controls-enclosure {
        overflow: hidden;
    }

    .custom_modal {
        background-color: #060606;
        opacity: 0.8;
    }

    .xbutton {
        position: absolute;
        top: 10%;
        left: 78%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        background-color: #c5405a;
        color: white;
        font-size: 14px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
        text-align: center;
    }

    .highcharts-exporting-group,
    .highcharts-credits {
        display: none
    }

    .badgeMJM {
        float: right;
    }

    /* charise end code */
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/lightbox2/dist/css/lightbox.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/videopopup/css/videopopup.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">DMS - Employee Relations Officer</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <input type="hidden" id="hidden_base_url" name="hidden_base_url" value="<?php echo base_url(); ?>" />
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--tabs active" id="personalPortlet">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a id="violationLogTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#violationsLog" role="tab">
                                        <i class="fa fa-balance-scale"></i>Violations Log</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="eroRecommendationTab" class="nav-link m-tabs__link " data-toggle="tab" href="#myIrs" role="tab" onclick="finalLastRecommend(5)">
                                        <i class="fa fa-group"></i>Recommendation <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink badgeMJM" id="badgeRecommend"></span></a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="eroUploadTab" class="nav-link m-tabs__link " data-toggle="tab" href="#myIRUpload" role="tab" onclick="finalLastRecommend(6)">
                                        <i class="fa fa-upload"></i>Upload <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink badgeMJM" id="badgeUpload"></span></a>
                                </li>
                                <!-- charise start code -->
                                <li class="nav-item m-tabs__item">
                                    <a id="irrecords_button" class="nav-link m-tabs__link " data-toggle="tab" href="#myIRrecords" role="tab">
                                        <i class="fa fa-list-alt"></i>Records</a>
                                </li>
                                <!-- charise end code -->
                                <li class="nav-item m-tabs__item">
                                    <a id="eroIrProfileTab" class="nav-link m-tabs__link " data-toggle="tab" href="#eroIrProfile" role="tab">
                                        <i class="fa fa-user-circle-o"></i>Employee IR Profile</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body py-5 px-2">
                        <div class="tab-content">
                            <div class="tab-pane fade show active col-md-12" id="violationsLog" role="tabpanel">
                                <div class="row no-gutters">
                                    <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                        <select name="" class="m-input form-control font-12" id="violation-class" data-toggle="m-tooltip" title="" data-original-title="Employee Type">
                                            <option value="">All Class</option>
                                            <option value="Agent">Ambassador</option>
                                            <option value="Admin">SZ Team</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                        <select name="" class="m-input form-control font-12" id="violation-accounts" data-toggle="m-tooltip" title="" data-original-title="Accounts">
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input font-12" style="padding-bottom:12px" placeholder="Select date range" id="violation-date" data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-calendar"></i></span></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                        <select class="form-control text-capitalize font-12" id="violation-type" data-toggle="m-tooltip" title="" data-original-title="Violation Type">
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input font-12" style="padding-bottom:12px" placeholder="Search Name..." id="violation-search" data-toggle="m-tooltip" title="" data-original-title="Employee Name" />
                                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="m-stack m-stack--ver m-stack--general">
                                    <div class="m-stack__item m-stack__item--center m-stack__item--middle m-stack__item--fluid">
                                        <h5 id="violation-header" class="mt-3 text-left"></h5>
                                    </div>
                                    <div class="m-stack__item m-stack__item--right m-stack__item--middle" style="width: 200px;">
                                        <button class="btn btn-outline-success btn-sm m-btn m-btn--custom m-btn--icon m-btn--pill mt-2" id="btn-downloadexcel-dtrviolation">
                                            <span>
                                                <i class="la la-download"></i>
                                                <span>Export Excel</span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                <hr style="border-style: dashed;">

                                <ul class="nav nav-tabs px-1" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#violation-chart"><i class="fa fa-pie-chart m--font-brand"></i> Graphical View</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#violation-tabular"><i class="fa fa-table m--font-info"></i> Tabular View</a>
                                    </li>
                                </ul>

                                <div class="tab-content px-1">
                                    <div class="tab-pane active" id="violation-chart" role="tabpanel">
                                        <div class="row m-row--no-padding m-row--col-separator-xl">
                                            <div class="col-xl-6">
                                                <div id="container-chart-pie" style="height: 300px;margin: 0 auto">
                                                </div>
                                            </div>
                                            <div class="col-xl-6 py-5 px-4">
                                                <div class="m-widget15">
                                                    <div class="m-widget15__items" id="violationList">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="violation-tabular" role="tabpanel">
                                        <div class="m_datatable" id="datatable_dtrviolations"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade col-md-12" id="myIrs" role="tabpanel">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-7 mb-2">
                                        </div>
                                        <div class="col-md-5 mb-2">
                                            <div class="form-group m-form__group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search by IR-ID" id="ongoingSearch2">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">
                                                            <i class="fa fa-search"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the
                                                    data below.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <!-- <button type="button" id="fetch_all_attachment" data-id="1"
                                                class="btn btn-sm btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air"
                                                data-toggle="modal" data-target="#modal_displaymultimedia" id=""><i
                                                    class="fa fa-external-link"></i></button> -->
                                            <div id="ongoingPulsesTableRecommend"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show col-md-12" id="myIRUpload" role="tabpanel">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2 mb-2">

                                        </div>
                                        <div class="col-md-5 mb-2">

                                        </div>
                                        <div class="col-md-5 mb-2">
                                            <div class="form-group m-form__group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search by IR-ID" id="ongoingSearch3">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">
                                                            <i class="fa fa-search"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the
                                                    data below.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="tblUpload"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- IR Records -->
                            <!-- charise start code -->
                            <div class="tab-pane fade show col-md-12" id="myIRrecords" role="tabpanel">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2 mb-2">
                                            <select name="" class="m-input form-control font-12" id="employee_type" data-toggle="m-tooltip" title="" data-original-title="Employee Type">
                                                <option value="">All</option>
                                                <option value="Admin" data-class="">Admin
                                                </option>
                                                <option value="Agent" data-class="">Agent</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 mb-2">
                                            <select name="" class="m-input form-control font-12" id="liability_type" data-toggle="m-tooltip" title="" data-original-title="Liability Status">
                                                <option value="">All</option>
                                                <option value="17" data-class="">Liable
                                                </option>
                                                <option value="18" data-class="">Not Liable</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 mb-2">
                                            <select name="" class="m-input form-control font-12" id="report_type" data-toggle="m-tooltip" title="" data-original-title="Report Type">
                                                <option value="">All</option>
                                                <option value="1">Normal</option>
                                                <option value="2">QIP</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5 mb-2">
                                            <select class="form-control m-select2" name="ir_subject" id="ir_subject"></select>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <select class="form-control m-select2" name="offense_filter" id="offense_filter"></select>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <select class="form-control m-select2" name="nature_offense" id="nature_offense"></select>
                                        </div>
                                        <div class="col-md-5 mb-2">
                                            <div class="m-input-icon m-input-icon--right">
                                                <input type="text" class="form-control m-input font-12" style="padding-bottom:12px" placeholder="Select date range" id="records-date" data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-calendar"></i></span></span>
                                            </div>
                                        </div>
                                        <div class="col-md-5 mb-2">
                                            <div class="form-group m-form__group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search by IR-ID" id="reports_irid">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">
                                                            <i class="fa fa-search"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the
                                                    data below.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2 mb-2">
                                            <button class="btn btn-outline-success btn-sm m-btn m-btn--custom m-btn--icon m-btn--pill mt-2" id="btn-downloadirrecords">
                                                <span>
                                                    <i class="la la-download"></i>
                                                    <span>Export Excel</span>
                                                </span>
                                            </button>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="irrecordsDatatable"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- charise end code -->
                            <div class="tab-pane fade col-12" id="eroIrProfile" role="tabpanel" style="margin-top: -3rem;margin-bottom: -3rem">
                                <div class="col-12">
                                    <div class="row px-4" style="margin-left: -2.7rem;margin-right: -2.7rem;/* margin-top: -2.3rem; *//* margin-bottom: -2.3rem */background: #f0f1f7;">
                                        <div class="col-12 mt-3" style="background: #f0f1f7;/* margin-bottom: 1.5rem */" id="empIrProfileSelect">
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label font-weight-bold">Select Subordinate Name</label>
                                                <div class="col-lg-9">
                                                    <select class="form-control m-select2 text-truncate" id="empListHistory" name="empListHistory" data-toggle="m-tooltip" title="" data-original-title="Select Employee Name"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row p-4" id="AccessSupIrProfile" style="margin-left: -2.8rem;margin-right: -2.8rem;/* margin-top: -2.3rem; */margin-bottom: -2.3rem;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">
                                        <div class="col-12 col-md-3 mb-2">
                                            <img src="http://10.200.101.250/_sz/assets/images/img/sz.png" onerror="noImageFound(this)" alt="" class="img-fluid mx-auto d-block" style="border: 0.3rem solid gainsboro;" id="profilePic">
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <div class="row mb-3">
                                                <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Employee Information</span>
                                                <div class="col">
                                                    <hr style="margin-top: 0.8rem !important;">
                                                </div>
                                            </div>
                                            <div class="row pl-3 pr-5 mb-3">
                                                <div class="col-12 col-md-4 font-weight-bold">Fullname:</div>
                                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="profileName"></div>
                                                <div class="col-12 col-md-4 font-weight-bold">Current Position:</div>
                                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"> <span id="profilePosition" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span> - <span id="profileEmpStatus" style="font-size: 11px;color: #0012ff;"></span></div>
                                                <div class="col-12 col-md-4 font-weight-bold">Current Team:</div>
                                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="profileAccount"></div>
                                            </div>
                                            <div class="row mb-4">
                                                <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">IR Record Range</span>
                                                <div class="col">
                                                    <hr style="margin-top: 0.8rem !important;">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-7">
                                                    <div class="input-group pull-right">
                                                        <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="irProfileDateRange" style="background: white;">
                                                        <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Schedule">
                                                            <span class="input-group-text" style="background: #fbfbfb !important;">
                                                                <i class="la la-calendar-check-o"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <select class="custom-select form-control" id="irProfileLiabilityStat" name="irProfileLiabilityStat" placeholder="Select a liability">
                                                        <option value="0">All</option>
                                                        <option value="17">Liable</option>
                                                        <option value="18">Non-Liable</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center" id="noAccessSupIrProfile" style="display:none">
                                        <i style="font-size: 42px;margin-right:1rem" class="fa fa-lock"></i>
                                        <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">Authorized Access Only</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-row--no-padding" id="showIrPofileRecords" style="display:none">
                    <div class="col-xl-7 pr-xl-4">
                        <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                            <div class="" style="padding: 2.2rem 2.2rem;">
                                <div class="m-widget26">
                                    <div class="m-widget26__content">
                                        <div class="natureOffenseChart" id="natureOffenseChart" style="overflow: visible !important;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                            <div class="" style="padding: 2.2rem 2.2rem;">
                                <div class="m-widget26">
                                    <div class="m-widget26__content text-center">
                                        <span class="mr-2">
                                            <i class="fa fa-file-text" style="font-size: 2rem;color: #ff5371;"></i>
                                        </span>
                                        <span class="m-widget26__number mr-2" id="irTotalCount">640</span>
                                        <span class="m-widget26__desc" style="font-weight: 500 !important;">Total Incident Report Count</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                            <div class="" style="padding: 0 !important">
                                <div class="m-widget1" style="padding-top: 1rem !important;padding-bottom: 1rem !important" id="discActionCount">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12" id="offenseHistory" style="display:none">
                        <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                            <div class="" style="padding: 2.2rem 2.2rem;">
                                <div class="m-widget26">
                                    <div class="m-widget26__content">
                                        <div class="row">
                                            <div class="col-12 col-md-8">
                                                <select class="form-control m-select2 text-truncate" id="specificOffenseHistory" name="specificOffenseHistory" data-toggle="m-tooltip" title="Select Employee Name"></select>
                                            </div>
                                            <div class="col-10 col-md-3">
                                                <div class="form-group m-form__group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="Search by IR-ID" id="searchProfileHistory">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2 col-md-1">
                                                <button class="btn btn-outline-success m-btn m-btn--icon btn-md m-btn--icon-only m-btn--pill m-btn--air" id="exportHistoryBtn">
                                                    <i class="la la-download"></i>
                                                </button>
                                            </div>
                                            <div class="col-12">
                                                <div class="mt-1" id="profileIrHistoryDatatable"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->

<div class="modal fade" id="norNteModalDownload" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-download text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">Download Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row " id="norNteOutput" data-irid="0">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <span>
                        <i class="fa fa-close"></i>
                        <span>Close</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="subjectExplainModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true" data-pw="" data-irid="" data-recommenlvl="" data-nteornor="">
    <div class="modal-dialog " id="modalShowDiv" role="document">
        <div class="modal-content">
            <form class="m-form m-form--fit m-form--label-align-right form-horizontal" id="defaultForm" method="post">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">IR Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <!--<div class="row">
                                <div class="col-12 col-md-8">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 col-lg-3">
                                            <img id="empPicSE" src="http://3.18.221.50/sz/assets/images/img/sz.png" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                        </div>
                                        <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                            <div class="row">
                                                <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="empNameSE"> </div>
                                                <div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="empJobSE"></div>
                                                <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="empAccountSE"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<hr>-->
                    <!-- <div class="col-12 divNorNte"> -->
                    <div class="row ir_details_template divNorNte" id="layoutIrDetails"> ** Michael's code ** </div>
                    <!-- </div> -->
                    <div class="col-12 divNorNte ">
                        <div class="row ">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Recommendation</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 p-1 divNorNte">
                        <div class="row pl-4 pr-4">
                            <div class="row pl-5 pr-5 pb-2" id="divRecommenaders">
                            </div>
                        </div>
                    </div>
                    <div class="divViewIR">
                        <div class="col-12 p-0 divNorNte">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>

                        <div id="uploadNorNteDiv" class="text-center ">
                            <div class="col-12">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-12">
                                        <label>For Notice Of Resolution: <span id="NORreq" class="text-danger"></span></label> <br>
                                        <label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent"><i class="fa fa-paperclip"></i> Upload NOR
                                            <input type="file" style="display: none;" name="upload_files_nor" id="upload_files_nor">
                                        </label>
                                        <br>
                                        <small id="NORname" data-filesize='' data-filetype=''></small>

                                    </div>
                                    <div class="col-lg-12" style="margin-top: 32px;">
                                        <label>For Notice To Explain: <span id="NTEreq" class="text-danger"></span></label><br>
                                        <label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"><i class="fa fa-paperclip"></i> Upload NTE
                                            <input type="file" style="display: none;" name="upload_files_nte" id="upload_files_nte">
                                        </label>
                                        <br>
                                        <small id="NTEname" data-filesize='' data-filetype=''></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="divNorNte px-3" id="divRecommend">
                            <div class="col-12">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                        <label for="exampleTextarea">Type</label>
                                        <select class="form-control m-select2" id="recType">

                                        </select>
                                        <small class="m--font-danger" id="txtImportant"></small>
                                    </div>
                                    <div class="col-lg-4" id="divCateg">
                                        <label for="exampleTextarea">Category</label>
                                        <select class="form-control m-select2" id="recCateg" ></select>
                                    </div>
                                    <div class="col-lg-4" id="divDiscAct">
                                        <label for="exampleTextarea">Disciplinary Action</label>
                                        <select class="form-control m-select2" id="recDisAct" onchange="checkIfSusOrTerm()"></select>
                                    </div>
                                    <div class="col-lg-4" id="divDiscAct">
                                        <div id="DaysRequire" style="display:none">
                                            <label for="exampleTextarea">Days</label>
                                            <div class="col-lg-12">
                                                <input id="reqNumDays" type="text" class="form-control bootstrap-touchspin-vertical-btn" value="1" name="demo1" placeholder="1" type="text" readonly>
                                            </div>
                                        </div>
                                        <div id="DateRequire" style="display:none">
                                            <label for="exampleTextarea">Date</label>
                                            <input type="text" class="form-control m-input" readonly placeholder="Select date" id="reqDate" value="<?php echo date("Y-m-d"); ?>">
                                        </div>



                                    </div>
                                </div>

                            </div>
                            <br>
                            <div class="col-12">
                                <div class="form-group m-form__group">
                                    <label for="exampleTextarea">Comment</label>
                                    <textarea class="form-control m-input m-input--solid" id="noteArea" rows="3"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="button" class="btn btn-primary divViewIR" id="btnSubmitIR">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!--begin: Charise :Modal-->
<div class="modal fade" id="modal_attachedfiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header custom-modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #6d6565 !important;"> HR Attachment
                </h5>
                <button type="button" id="media_closebutton2" class="close" data-dismiss="modal" aria-label="Close" style="color: #6d6565 !important;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body">
                    <div class="row" data-irid="0" id="row_irid">
                        <div class="col-lg-5 col-md-12">
                            <form id="upload_evidence" method="post">
                                <div class="col-12 text-center mt-3">
                                    <span style="font-size:smaller">Each file to attach should not exceed
                                        to <strong style="color:#558e82;">5MB.</strong> Allowed file types: <strong style="color:#c096ca">
                                            doc, docx, pdf, txt, xml, xlsx, xlsm, ppt, pptx, jpeg, jpg, png, gif, mp4,
                                            mov, 3gp,
                                            wmv, avi, mpeg, mp3, wav </strong></span><br><br>

                                    <label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent" id="add_evidences_button"><i class="fa fa-plus"></i> Add Attachment
                                        <input type="file" name="upload_evidence_files[]" id="upload_evidence_files" style="display: none; " onchange="makeFileList()" multiple>
                                    </label>
                                    <br><label id="files_select_label" style="color:green;"></label>
                                </div>
                                <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-3" style="max-height: 100px; height: 100px; position: relative; overflow: visible;" id="display_div_file">
                                    <ul id="fileList" style="list-style-type: none;">
                                    </ul>
                                </div>
                                <div class="text-center" id="sub_button" style="display:none;"><button id="up_button" type="submit" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air"><i class="fa fa-upload"></i> Upload </button>
                                </div>
                            </form>
                        </div>

                        <div class="col-lg-7 col-md-12 ">
                            <label class="label_attach" id="label_attach"></label>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <label class="label_select" id="">Attachment Types</label>
                                    <select class="form-control m-select" id="evidence-types_ero">
                                        <option value="">All</option>
                                        <option value="1">Photos</option>
                                        <option value="2">Videos</option>
                                        <option value="3">Audios</option>
                                        <option value="4">Files</option>
                                        <option value="5">Others</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <label class="label_select" id="">Attachment Purpose</label>
                                    <select class="form-control m-select" id="attachment-purpose">
                                        <!-- <option value="">All</option>
                                        <option value="1">Evidences</option>
                                        <option value="4">HR attachment</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-5" style="max-height: 250px; height: 250px; position: relative; overflow: visible;">
                                <!-- <div class="row" id="evidence_display"> -->
                                <div class="row" id="attachment_display_ero">
                                </div>
                                <span id="passed_irid"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="irDetails" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">Incident Report Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row ir_details_template" id="ir_filed_details" data-irid="0"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <span>
                        <i class="fa fa-close"></i>
                        <span>Close</span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade yu" id="audio_modal_evidences2" tabindex="-1" role="dialog" aria-labelledby='exampleModalLabel' aria-hidden='true'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content custom_modal'>
            <div class='modal-body'>
                <div class='col-12' id="audio_evidence_player2">
                </div>
                <button type='button' class='btn btn-link' data-dismiss="modal">Close</button>";
            </div>
        </div>
    </div>
</div>
<!--end: Charise:Modal-->

<!--end::Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/incrementLetter/incrementLetter.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/cod_settings.js"></script>
<script src="<?php echo base_url(); ?>node_modules/lightbox2/dist/js/lightbox.js"></script>
<script src="<?php echo base_url(); ?>node_modules/videopopup/js/videopopup.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_ero_cha.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_dtrviolation.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_ir_profile.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_ero.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>
<script>
    var ajax_req = null;
    function get_dms_action_category(){
        if (ajax_req != null) ajax_req.abort();
        ajax_req = $.ajax({
            type: 'POST',
            url: baseUrl +'/discipline/get_dms_recommend_action_categ',
            data: {
                action_categ_settings:  $('#modalShowDiv').data('actioncategsetting'),
                category_id: $('#recCateg').val()
            },
            cache: false,
            beforeSend: function() {
                // $(placeholder).addClass('loading');
            },
            success: function(data) {
                let action_category = JSON.parse(data);
                let option_str = "";
                if(parseInt(action_category.status)){
                    $.each(action_category.record, function(key, value) {
                        let selected = (key == 0) ? "selected" : "";
                        option_str += "<option value=" + value.disciplinaryActionCategory_ID + " " +selected + ">" + value.action +"</option>";
                    });
                }
                $('#recDisAct').html(option_str);
                $('#recDisAct').select2({
                    placeholder: "-Select-",
                    width: '100%'
                }).trigger('change');
            },
            error: function(xhr) { // if error occured
                // alert("Error occured.please try again");
                console.log(xhr.statusText + xhr.responseText);
                // $(placeholder).removeClass('loading');
            },
            complete: function() {
                //     $(placeholder).removeClass('loading');
            },
        });
    }
    // MJM start code

    $(function() {
        getNumTabs();
        finalLastRecommend(5);
        $('#recType,#recDisAct,#recCateg').select2({
            placeholder: "Select Type of Recommendation.",
            width: '100%'
        });
        $('#recCateg').on('change', function(){
            get_dms_action_category();
        })
        $('#recType').change(function() {
            var ty = $(this).val();
            $("#txtImportant").html("");
            if (ty == "17") {
                $("#divCateg").css("display", "block");
                $("#divDiscAct").css("display", "block");
                checkIfSusOrTerm();
            } else {
                $("#divCateg").css("display", "none");
                $("#divDiscAct").css("display", "none");
                $("#DateRequire").css("display", "none");
                $("#DaysRequire").css("display", "none");
            }
        });
        $('#reqNumDays').TouchSpin({
            buttondown_class: 'btn btn-secondary',
            buttonup_class: 'btn btn-secondary',
            verticalbuttons: true,
            verticalupclass: 'la la-plus',
            verticaldownclass: 'la la-minus',
            min: 1,
            max: 50,
        });
        $('#reqDate').datepicker({
            todayHighlight: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            },
            format: 'yyyy-mm-dd'
        });
    });

    function checkIfSusOrTerm() {
        var ch = $("#recDisAct option:selected").data("lbl");
        if (ch == "s") {
            $("#DaysRequire").css("display", "block");
            $("#DateRequire").css("display", "none");
        } else if (ch == "t") {
            $("#DateRequire").css("display", "block");
            $("#DaysRequire").css("display", "none");
        } else {
            $("#DateRequire").css("display", "none");
            $("#DaysRequire").css("display", "none");
        }

    }

    function saveRecommend() {
        var rectype = $("#recType").val();
        var recDisAct = $("#recDisAct").val();

        if (rectype == null || rectype == "0") {

            $("#txtImportant").html("<b> This field is required.</b> ");

        } else {
            $("#txtImportant").html("");
            if (rectype == 18) {
                saveIRrecommend(rectype, 0);
                //continue saving record ..
            } else {
                saveIRrecommend(rectype, recDisAct);
            }
        }

    }

    function saveIRrecommend(liabilityStat_ID, disciplinaryActionCategory_ID) {
        var pw = $("#subjectExplainModal").data("pw");
        var irid = $("#subjectExplainModal").data("irid");
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {

            var recommenlvl = $("#subjectExplainModal").data("recommenlvl");
            if (result.value == pw) {
                submitIRrecommend(irid, liabilityStat_ID, disciplinaryActionCategory_ID);

            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });
    }

    function submitIRrecommend(irid, liabilityStat_ID, disciplinaryActionCategory_ID) {
        var txt = $("#noteArea").val().trim();
        var reqNumDays = $("#reqNumDays").val();
        var reqDate = $("#reqDate").val();
        var recommenlvl = $("#subjectExplainModal").data("recommenlvl");
        var recDisAct = $("#recDisAct option:selected").data("lbl");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>discipline/submitRecommend/" + irid,
            data: {
                txt: txt,
                liabilityStat_ID: liabilityStat_ID,
                disciplinaryActionCategory_ID: disciplinaryActionCategory_ID,
                recDisAct: recDisAct,
                reqDate: reqDate,
                reqNumDays: reqNumDays,
                recommenlvl: recommenlvl
            },
            cache: false,
            success: function(res) {
                if (parseInt(res.trim()) == 1) {
                    $("#subjectExplainModal").modal("hide");
                    finalLastRecommend(5);
                } else {
                    swal("Error", 'Something went wrong. Please contact the administrator.', 'error');
                }

            }
        });

    }

    function initSubjExplain(id, incidentReport_ID, typ, lvl, disciplineCategory_ID, disciplinaryActionCategorySettings_ID,
        disciplinaryActionCategory_ID, incidentReportRecommendationType_ID, norORnte1) {
        $('#modalShowDiv').data('actioncategsetting', disciplinaryActionCategorySettings_ID); 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>discipline/empDetailsSE",
            data: {
                id: id,
                typ: typ,
                lvl: lvl,
                disciplineCategory_ID: disciplineCategory_ID,
                disciplinaryActionCategorySettings_ID: disciplinaryActionCategorySettings_ID,
                incidentReport_ID: incidentReport_ID,
                disciplinaryActionCategory_ID: disciplinaryActionCategory_ID
            },
            cache: false,
            success: function(res) {
                var result = JSON.parse(res);
                var nteornor = (norORnte1 == 2) ? "NORreq" : "NTEreq";

                $("#" + nteornor + "").html("*");
                $("#divDiscAct").css("display", "none");
                $("#divCateg").css("display", "none");
                if (typ == 1) {
                    $("#divSubjExplain").show();
                    $("#divRecommend").hide();
                    $("#btnSubmitIR").attr("onclick", "saveExplain()");
                } else if (typ == 2 || typ == 3 || typ == 7 || typ == 5) {
                    $("#divSubjExplain").hide();
                    $("#divRecommend").show();
                    $("#btnSubmitIR").attr("onclick", "saveRecommend()");
                    str = "";
                    categ_option = "";
                    if ((result["client_confirm"]).length > 0) {
                        var elem = (result["client_confirm"][0].label == 's') ? "#reqNumDays" : "#reqDate";
                        $(elem).val(result["client_confirm"][0].suggestion);
                    }
                    if ((result["category"]).length > 0) {
                        $.each(result["category"], function(key, value) {
                            let selected = (key == 0) ? "selected" : "";
                            categ_option += "<option value=" + value.disciplineCategory_ID + " " +selected + ">" + value.category.toUpperCase() +"</option>";
                        });
                    }
                    if ((result["discipline_cat"]).length != 0) {
                        if ((result["discipline_cat"]).length <= 1) {
                            str += '<option value=' + result["discipline_cat"][0]
                                .disciplinaryActionCategory_ID + ' selected  data-lbl="' + result[
                                    "discipline_cat"][0].label + '">' + result["discipline_cat"][0].action +
                                '</option>';
                        } else {
                            var ctr = 0;
                            $.each(result["discipline_cat"], function(key, data) {
                                var selected = (ctr == 0) ? "selected" : "";
                                str += "<option value=" + data.disciplinaryActionCategory_ID + " " +
                                    selected + " data-lbl='" + data.label + "'>" + data.action +
                                    "</option>";
                                ctr++;
                            });

                        }
                    }
                    
                    str2 = '<option value=0 selected disabled>--</option>';
                    str2 += '<option value=17>Liable</option>';
                    str2 += '<option value=18>Non-Liable</option>';



                    $("#recType").html(str2);
                    $("#recDisAct").html(str);
                    $("#recCateg").html(categ_option);
                    $("#recDisAct").trigger("changed");

                    var rec = "";
                    if (typ == 5 || typ == 7) {
                        var i = 1;
                        $.each(result["discipline_recommenders"], function(key, data) {
                            if (data.incidentReportRecommendationType_ID == 2) {
                                var r = "Last Recommender";
                                var b = "danger ";
                                var ii = "";
                            } else if (data.incidentReportRecommendationType_ID == 3) {
                                var r = "Final Recommender";
                                var b = "info";
                                var ii = "";
                            } else if (data.incidentReportRecommendationType_ID == 4) {
                                var r = "Client Recommendation";
                                var b = "info";
                                var ii = "";
                            } else {
                                var r = "Recommender ";
                                var b = "warning";
                                var ii = "#" + i++;

                            }
                            if (data.liabilityStat_ID == "17") {
                                var ifLiable = data.action + ' (Liable ) ';


                            } else if (data.liabilityStat_ID == "18") {
                                var ifLiable = data.action + ' (Non-Liable ) ';

                            } else {
                                var ifLiable = "Missed to recommend";
                            }
                            rec += '<div class="col-12">'
                            rec += '<div class="row p-3">'
                            rec += '<span class="m-menu__link-badge"><span class="m-badge m-badge--' + b + ' pl-2 pr-2">' + r + ' ' + ii + ' </span></span>'
                            rec += '<div class="col">'
                            rec += '<div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;margin-top: 10px;"></div>'
                            rec += '</div>'
                            rec += '</div>'
                            rec += '</div>'
                            rec += '<div class="col-12 col-md-4 font-weight-bold">Name:</div>'
                            rec += '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedLevel">' + data.fname + " " + data.lname + '</div>'
                            rec += '<div class="col-12 col-md-4 font-weight-bold">Category:</div>'
                            rec +='<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"> Category ' + data.category.toUpperCase()+ '</div>'
                            rec += '<div class="col-12 col-md-4 font-weight-bold">Disciplinary Action:</div>'
                            rec += '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 17px; font-weight: 500" id="qualifiedDisciplinaryAction">' + ifLiable + '</div>'
                            if (data.susTermDetails != null) {
                                if ((data.susTermDetails).includes("-")) {
                                    var lab = "Date";
                                } else {
                                    var lab = "# of Day(s)";
                                }

                                rec += '<div class="col-12 col-md-4 font-weight-bold">' + lab +
                                    ':</div>'
                                rec +=
                                    '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">' +
                                    data.susTermDetails + '</div>';
                            }
                            if (data.liabilityStat_ID != "12") {
                                let noteRec = "No Notes Found"
                                if (data.notes != "" && data.notes != null) {
                                    noteRec = data.notes;
                                }
                                rec += '<div class="col-12 col-md-4 font-weight-bold">Note:</div>'
                                rec +=
                                    '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedPeriod">' +
                                    noteRec + '</div>';
                            }
                            if (data.incidentReportRecommendationType_ID == 4 && result["client_attach"].length > 0) {
                                rec += '<div class="col-12 col-md-4 font-weight-bold">Screenshot:</div>'
                                var aa = "<?php echo base_url(); ?>" + result["client_attach"][0].link;
                                rec += '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"><a href="' + aa + '" target="_blank"><i class="fa fa-search"></i> View</a></div>';

                            }
                        });
                    }
                    $("#divRecommenaders").html(rec);

                } else if (typ == 6) {
                    $("#btnSubmitIR").attr("onclick", "uploadFile(6)");
                    $("#uploadNorNteDiv").show();
                } else {
                    $("#divSubjExplain").hide();
                    $("#divRecommend").hide();
                }
                if (typ == 7) {
                    $(".divViewIR").css("display", "none");
                } else {
                    $(".divViewIR").css("display", "block");
                }

                if (typ == 6) {
                    $(".divNorNte").hide();
                    $("#uploadNorNteDiv").show();
                    $("#modalShowDiv").removeClass("modal-lg");
                    $("#upload_files_nte").val("");
                    $("#upload_files_nor").val("");
                    $("#NORname").text("");
                    $("#NTEname").text("");

                } else {

                    $(".divNorNte").show();
                    $("#uploadNorNteDiv").hide();
                    $("#modalShowDiv").addClass("modal-lg");
                    $("#layoutIrDetails").html(result.layout["ir_details_layout"]);
                }
                $("#empNameSE").text(result["emp_details"].lname + ", " + result["emp_details"].fname);
                $("#empJobSE").text(result["emp_details"].positionDescription);
                $("#empAccountSE").text(result["emp_details"].accountDescription);
                $("#subjectExplainModal").data("pw", result["emp_details"].pw);
                $("#subjectExplainModal").data("irid", incidentReport_ID);
                $("#subjectExplainModal").data("nteornor", norORnte1);
                $("#subjectExplainModal").data("recommenlvl", incidentReportRecommendationType_ID);
                $("#noteArea").val("")
                $("#txtOutput").html("");
                $("#subjectExplainModal").modal();

                $("#DateRequire").css("display", "none");
                $("#DaysRequire").css("display", "none");
            }
        });
    }

    $("#upload_files_nte").bind('change', function(e) {
        var file = $(this).val();
        var size = this.files[0].size;
        var typ = this.files[0].type;

        file = (file.length > 0) ? file : "fakepath";
        var name = file.split('fakepath');
        var filen4me = "";
        $("#NTEname").data("filetype", typ);
        if (size > 5000000) {
            filen4me = (file.length > 0) ? "<strong class='text-danger'>NOTE: File must not exceed to 5MB.</strong>" : "";
            $("#NTEname").data("filesize", "error");
        } else {
            filen4me = (file.length > 0) ? name[1].slice(1) : "";

            $("#NTEname").data("filesize", size);
        }
        $("#NTEname").html(filen4me);
    });
    $("#upload_files_nor").bind('change', function(e) {
        var file = $(this).val();
        var size = this.files[0].size;
        var typ = this.files[0].type;

        file = (file.length > 0) ? file : "fakepath";
        var name = file.split('fakepath');
        var filen4me = "";
        $("#NORname").data("filetype", typ);
        if (size > 5000000) {
            filen4me = (file.length > 0) ? "<strong class='text-danger'>NOTE: File must not exceed to 5MB.</strong>" : "";
            $("#NORname").data("filesize", "error");
        } else {
            filen4me = (file.length > 0) ? name[1].slice(1) : "";

            $("#NORname").data("filesize", size);
        }
        $("#NORname").html(filen4me);
    });
    $("#defaultForm").on('submit', function(e) {
        e.preventDefault();
        var record = new FormData(this);
        var nteornor = $("#subjectExplainModal").data("nteornor");
        var irid = $("#subjectExplainModal").data("irid");
        console.log(record);
        $.ajax({
            url: "<?php echo base_url(); ?>discipline/uploadFile/" + irid + "/" + nteornor,
            type: "POST",
            data: record,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                if (data > 0) {
                    swal("Success!", "File were  successfully uploaded.", 'success');
                    finalLastRecommend(6);
                    $("#subjectExplainModal").modal("hide");
                } else {
                    swal("Error",
                        "File were not successfully uploaded. Please contact the Administrator.",
                        'error');
                }
            }

        });

    });

    function uploadFile(typ) {
        var nteornor = $("#subjectExplainModal").data("nteornor");
        var nte = $("#upload_files_nte").val();
        var nor = $("#upload_files_nor").val();
        var ok_nor = 0;
        var ok_nte = 0;
        var submit1 = 0;
        var submit2 = 1;



        if (nor != "") {
            var size = $("#NORname").data("filesize");
            var filetype = $("#NORname").data("filetype");

            if (filetype === "application/pdf") {
                if (size < 5000000) {
                    ok_nor = 1;
                } else {
                    ok_nor = 2;
                }
            } else {
                ok_nor = 3;
            }

            // $("#defaultForm").submit();
        } else {
            $("#NORname").text("");
        }

        if (nte != "") {
            var size = $("#NTEname").data("filesize");
            var filetype = $("#NTEname").data("filetype");

            if (filetype === "application/pdf") {
                if (size < 5000000) {
                    ok_nte = 1;
                } else {
                    ok_nte = 2;
                }
            } else {
                ok_nte = 3;
            }
            // $("#defaultForm").submit();
        } else {
            $("#NTEname").text("");

        }
        var status = (nteornor == 2) ? "ok_nor" : "ok_nte";
        var name = (nteornor == 2) ? "NOR" : "NTE";
        if (status == "ok_nor") {
            var n = "NOR";
            if (ok_nor == 1) {
                submit1 = 1;
                // swal("Error", "Required to upload NTE File!", 'success');
            } else if (ok_nor == 2) {
                toastr.error("NOTE: " + name + " must be in a PDF format with maximum of 5MB file size");
            } else if (ok_nor == 3) {
                toastr.error("NOTE: " + name + " must be PDF File");
            } else {
                swal("Error", "Required to upload " + name + " File!", 'error');
            }
        }
        if (status == "ok_nte") {
            var n = "NTE";
            if (ok_nte == 1) {
                submit1 = 1;
                // swal("succes", "Required to upload NTE File!", 'success');
            } else if (ok_nte == 2) {
                toastr.error("NOTE: " + name + " must be in a PDF format with maximum of 5MB file size");
            } else if (ok_nte == 3) {
                toastr.error("NOTE: " + name + " must be PDF File");
            } else {
                swal("Error", "Required to upload " + name + " File!", 'error');
            }
        }

        var status1 = (n == "NOR") ? ok_nte : ok_nor;
        var status_name = (n == "NOR") ? "NTE" : "NOR";

        if (status1 == 1) {
            // swal("succes", "Required to upload NTE File!", 'success');
        } else if (status1 == 2) {
            submit2 = 0;
            toastr.error("NOTE: " + status_name + " must be in a PDF format with maximum of 5MB file size");
        } else if (status1 == 3) {
            submit2 = 0;
            toastr.error("NOTE: " + status_name + " must be PDF File");
        }
        if (submit1 == 1 && submit2 == 1) {
            $("#defaultForm").submit();
        }
        //$("#defaultForm").submit();
    }

    function getNumTabs() {
        $.ajax({
            url: "<?php echo base_url(); ?>discipline/getNumTabsERO/",
            type: "POST",
            success: function(data) {
                var result = JSON.parse(data);

                var explain = parseInt(result["ero"][0].cnt);
                if (explain > 0) {
                    $("#badgeRecommend").css("display", "block");
                } else {
                    $("#badgeRecommend").css("display", "none");
                }
                var upload = parseInt(result["upload"][0].cnt);
                if (upload > 0) {
                    $("#badgeUpload").css("display", "block");
                } else {
                    $("#badgeUpload").css("display", "none");
                }
            }

        });
    }

    function finalLastRecommend(typ) {
        getNumTabs();
        if (typ == 5) {
            var tbl = '#ongoingPulsesTableRecommend';
        } else {
            var tbl = '#tblUpload';
        }
        $(tbl).mDatatable('destroy');

        var choice = $("#ongoingStatus").val();
        var empid = $("#empSelectIntervene").val();
        empid = (empid != null) ? empid : 0;

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/discipline/getAllSubordinates/' + typ,
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        map: function(raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: 'dashed-table',
                scroll: false,
                footer: false,
                header: true
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#ongoingSearch' + typ),
            },
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 150,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function(data) {
                    var incidentReport_ID = (data.incidentReport_ID).padLeft(8);
                    var html = "<div class = 'col-md-12'>" + incidentReport_ID + "</div>";
                    return html;
                }
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 150,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {

                    var html = "<div class = 'col-md-12'>" + moment(data.incidentDate).format('LL'); +
                    "</div>";
                    return html;
                }
            }, {
                field: "lname",
                title: "Subject",
                width: 150,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                    var html = "<div class = 'col-md-12'>" + data.lname + ", " + data.fname + "</div>";
                    return html;
                }
            }, {
                field: (typ == 5) ? "incidentReportRecommendationType_ID" : "irNecessaryDocument_ID",
                title: (typ == 5) ? "Recommendation Level" : "Required File",
                width: 150,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                    if (typ == 5) {
                        var dt = (data.incidentReportRecommendationType_ID == 2) ?
                            '<span class="m-menu__link-badge"><span class="m-badge m-badge--info m-badge--wide"> Last </span></span>' :
                            '<span class="m-menu__link-badge"><span class="m-badge m-badge--primary m-badge--wide"> Final </span></span>';
                    } else {
                        var dt = (data.incidentReportAttachmentPurpose_ID == 2) ?
                            '<span class="m-menu__link-badge"><span class="m-badge m-badge--danger m-badge--wide"> NOR </span></span>' :
                            '<span class="m-menu__link-badge"><span class="m-badge m-badge--primary m-badge--wide"> NTE </span></span>';
                    }
                    var html = "<div class = 'col-md-12'>" + dt + "</div>";
                    return html;
                }
            }, {
                field: "",
                title: "Action",
                selector: false,
                sortable: false,
                textAlign: 'center',
                overflow: 'invisible',
                width: 100,
                template: function(data) {

                    var lvl = (typ > 2) ? data.level : 0;
                    var disciplineCategory_ID = (typ > 2) ? data.disciplineCategory_ID : 0;
                    var norORnte = (data.incidentReportAttachmentPurpose_ID == 2) ? 2 : 1;

                    var onclick = 'onclick="initSubjExplain(' + data.subjectEmp_ID + ',' + data
                        .incidentReport_ID + ',' + typ + ',' + lvl + ',' + disciplineCategory_ID + ',' +
                        data.disciplinaryActionCategorySettings_ID + ',' + data
                        .disciplinaryActionCategory_ID + ',' + data.incidentReportRecommendationType_ID +
                        ',' + norORnte + ')"';


                    var html =
                        '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="File IR" data-qualifiedid="111" ' +
                        onclick + '><i class="la la-edit"></i></a>';
                    if (typ == 6) {
                        var onclick = 'onclick="initSubjExplain(' + data.subjectEmp_ID + ',' + data
                            .incidentReport_ID + ',7,' + lvl + ',' + disciplineCategory_ID + ',' +
                            data.disciplinaryActionCategorySettings_ID + ',' + data
                            .disciplinaryActionCategory_ID + ',' + data.incidentReportRecommendationType_ID +
                            ',' + norORnte + ')"';

                        html += '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View IR Information" data-qualifiedid="111" ' + onclick + '"><i class="la la-search"></i></a>';
                        var onclick2 = 'onclick="modalNorNte(' + data.incidentReport_ID + ')"';
                        html += '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="Download NOR/NTE File" ' + onclick2 + '"><i class="la la-download"></i></a>';
                    }

                    return html;
                }
            }, ],
        };

        $(tbl).mDatatable(options);


    }

    function modalNorNte(ir) {
        $("#norNteModalDownload").modal();
        var txt = "";
        var nte = "<?php echo base_url(); ?>discipline/downloadPdf_nte/" + ir;
        var nor = "<?php echo base_url(); ?>discipline/downloadPdf_nor/" + ir;
        txt += '<div class="col-12 text-center">';
        txt += '<div class="form-group m-form__group row">';
        txt += '<div class="col-lg-12">';
        txt += '<label>For Notice Of Resolution: <span id="NORreq" class="text-danger"></span></label> <br>';
        txt += '<a href="' + nor + '"><label   class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent"><i class="fa fa-download"></i>  NOR Template </label></a>';
        txt += '<br>';

        txt += '</div>';
        txt += '<div class="col-lg-12" style="margin-top: 32px;">';
        txt += '<label>For Notice To Explain: <span id="NTEreq" class="text-danger"></span></label><br>';
        txt += '<a href="' + nte + '"><label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"><i class="fa fa-download"></i>  NTE Template</label></a>';
        txt += '<br>';
        txt += '</div>';
        txt += '</div>';
        txt += '</div>';
        $("#norNteOutput").html(txt);
    }
    // MJM end code
</script>
<script type="text/javascript">
    //------------------NICCA CODES STARTS HERE---------------------------------------------------------------------

    $(function() {
        var myoptions = {
            innerSize: 60,
            depth: 45,
            dataLabels: {
                distance: 1
            }
        };
        var currentdate = tz_date(moment());
        var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
        var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
        var datatable_dtrviolations = datatable_dtrviolations_init("get_tabular_dtr_system_violations", {
            query: {
                incidentDate_start: moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD'),
                incidentDate_end: moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD'),
                violation_accounts: $("#violation-accounts").val(),
                violation_type: $("#violation-type").val(),
                violation_class: $("#violation-class").val()
            }
        });
        $('#violation-date').val(start + ' - ' + end);
        $('#violation-date').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
                'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate,
                    'YYYY-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(currentdate,
                    'YYYY-MM-DD')],
                'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate,
                    'YYYY-MM-DD').endOf('month')],
                'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'),
                    moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')
                ]
            }
        }, function(start, end, label) {
            $('#violation-date .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
                'MM/DD/YYYY'));
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['incidentDate_start'] = moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD');
            query['incidentDate_end'] = moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD');
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'ERO');
        });
        set_violation_types();
        get_ero_accounts(function(res) {
            $("#violation-accounts").html("<option value=''>All</option>");
            $.each(res.accounts, function(i, item) {
                $("#violation-accounts").append("<option value='" + item.acc_id + "' data-class='" +
                    item.acc_description + "'>" + item.acc_name + "</option>");
            });
        });
        $("#violation-class").change(function() {
            var theclass = $(this).val();
            $("#violation-accounts").children('option').css('display', '');
            $("#violation-accounts").val('').change();
            if (theclass !== '') {
                $("#violation-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css(
                    'display', 'none');
            }
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['violation_class'] = $("#violation-class").val();
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'ERO');
        });
        $("#violation-accounts,#violation-type").change(function() {
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['violation_accounts'] = $("#violation-accounts").val();
            query['violation_type'] = $("#violation-type").val();
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'ERO');
        });
        $('#violation-search').keyup(function() {
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['violation_search'] = $("#violation-search").val();
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'ERO');
        });
        initialize_chart_dtrviolation(myoptions, 'ERO');
    });
    $("#btn-downloadexcel-dtrviolation").click(function() {
        $.ajax({
            url: "<?php echo base_url(); ?>Discipline/downloadExcel_dtrSystemViolations",
            method: 'POST',
            data: {
                incidentDate_start: $('#violation-date').data('daterangepicker').startDate.format(
                    'YYYY-MM-DD'),
                incidentDate_end: $('#violation-date').data('daterangepicker').endDate.format('YYYY-MM-DD'),
                violation_accounts: $("#violation-accounts").val(),
                violation_type: $("#violation-type").val(),
                violation_class: $("#violation-class").val(),
                violation_search: $("#violation-search").val()
            },
            beforeSend: function() {
                mApp.block('#personalPortlet', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg',
                    message: 'Downloading...'
                });
            },
            xhrFields: {
                responseType: 'blob'
            },
            success: function(data) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'SZ_DTRSystemViolations.xlsx';
                a.click();
                window.URL.revokeObjectURL(url);
                mApp.unblock('#personalPortlet');
            },
            error: function(data) {
                $.notify({
                    message: 'Error in exporting excel'
                }, {
                    type: 'danger',
                    timer: 1000
                });
                mApp.unblock('#personalPortlet');
            }
        });
    })
    //------------------NICCA CODES ENDS HERE---------------------------------------------------------------------
</script>