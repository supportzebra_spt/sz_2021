<style>
    .bs-popover-auto[x-placement^=right] .arrow::after,
    .bs-popover-right .arrow::after {
        border-right-color: #282A3C !important;
    }

    .bs-popover-auto[x-placement^=left] .arrow::after,
    .bs-popover-left .arrow::after {
        border-left-color: #282A3C !important;
    }

    .popover-header {
        background-color: rgb(52, 55, 78) !important;
        border-bottom: 0px !important;
        color: white;
        max-width: 100% !important;
    }

    .popover-body {
        background-color: #282A3C !important;
        max-width: 100% !important;
        padding: 1.0rem 0.5rem !important;
        text-align: center;
        padding-bottom: 1px !important;
    }

    .list-active {
        background: #e8e8e8;
    }

    .active-nav-a {
        font-weight: bold !important;
        color: #313131 !important;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
        background: unset !important;
    }

    .m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
        background: unset !important;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
        background: #F0F0F2 !important;
        color: black !important;
    }

    .codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover {
        font-weight: bold !important;
        color: #6f727d !important;
    }

    .m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
        color: #8c8c8c;
    }

    .m-nav.m-nav--inline>.m-nav__item:first-child {
        padding-left: 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item {
        padding: 10px 20px 10px 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active {
        background: #d4d7da;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item:hover {
        background: #ececec;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
        color: #4f668a;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
        color: black;
    }

    .m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
        height: 100%;
        padding: 0.8rem 0 0.8rem 0;
    }

    .m-portlet .m-portlet__head {
        height: 3.9rem;
    }

    .m-portlet.m-portlet--tabs.active {
        display: block;
    }

    .dtrAutoIrSideTab {
        padding-left: 0.5rem !important;
        padding-right: 0.5rem !important;
        padding-top: 0.8rem !important;
        padding-bottom: 0.8rem !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        margin-right: 1px !important;
    }


    .editableStyle {
        border-bottom: dashed 1px #0088cc;
        cursor: pointer;
    }

    /* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
    /* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding: 0.65rem 2.9rem 0.8rem 1rem !important;
        white-space: normal;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
        top: 47% !important;
    }

    .embed_pdf:hover {
        box-shadow: 2px 2px 7px #777;
        z-index: 2;
        -webkit-transition: all 100ms ease-in;
        -webkit-transform: scale(1.01);
        -ms-transition: all 100ms ease-in;
        -ms-transform: scale(1.01);
        -moz-transition: all 100ms ease-in;
        -moz-transform: scale(1.01);
        transition: all 100ms ease-in;
        transform: scale(1.01);
        cursor: pointer;
    }

    .note-toolbar-wrapper {
        height: unset !Important;
    }

    .pdfobject-container {
        width: 100%;
        height: 550px;
    }

    #container-memos tbody tr {
        cursor: pointer;
    }

    #container-memos tbody tr:hover {
        background: #f6fffe;
        color: #34bfa3
    }

    .memo-close {
        z-index: 2000;
        font-size: 30px;
        position: absolute;
        top: 0px;
        right: 5px;
    }

    .memo-close:hover {
        text-shadow: 1px 1px 1px gray;
    }

    .memo-close:focus {
        outline: none !Important;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Add Latest IR Record</h3>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--tabs active" id="discActionCateg">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <a href="#" class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x" id="addIrModal">
                        <span>
                            <i class="fa fa-file-o"></i>
                            <span>Add IR</span>
                        </span>
                    </a>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="latestIrRecord" role="tabpanel">
                        <div class="row mt-3" id="noIrHistoryRecords" style="display:none;margin-right: 0px; margin-left: 0px;">
                            <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                <div class="col-md-12 text-center">
                                    <div class="m-demo-icon__preview">
                                        <i style="font-size: 50px;" class="flaticon-notes"></i>
                                    </div>
                                    <span class="m-demo-icon__class" style="font-size: 2rem;">No Added Records</span>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="ir_history_div">
                            <div class="col-md-6 mb-3">
                            </div>
                            <div class="col-md-6 mb-3">
                                <select class="form-control m-select2" name="subjectEmployeeSearch" id="subjectEmployeeSearch">
                                </select>
                            </div>
                            <div class="col-md-12" id="ir_history_datatable">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addIrModalForm" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="addIrForm" name="addIrForm" method="post" data-formtype="add">
            <div class="modal-content">
                <div class="modal-header" style="padding: 18px !important;">
                    <div class="col-8">
                        <h5 class="modal-title" id="exampleModalLabel">Add an Incident Report</h5>
                    </div>
                    <div class="col-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;"><span aria-hidden="true">×</span></button>
                    </div>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-2 font-weight-bold pt-2">Employee:</div>
                                <div class="col-12 col-md-5" style="font-size: 13px; font-weight: 400" id="">
                                    <select class="form-control m-select2" name="subjectEmployee" id="subjectEmployee">
                                    </select>
                                </div>
                                <div class="col-12 col-md-3 font-weight-bold pt-2">Date of Incident:</div>
                                <div class="col-12 col-md-2 p-0" style="font-size: 13px; font-weight: 400" id="">
                                    <input name="dateOfIncident" placeholder="Date" id="dateOfIncident" class='form-control m-input input_customize' readonly>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-12 mt-3">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold pt-2">Employee Type:</div>
                                <div class="col-12 col-md-9" style="font-size: 13px; font-weight: 400" id="">
                                    <select class="custom-select form-control" name="empType" id="empType">
                                        <option value="Admin">SZ Admin Team</option>
                                        <option value="Agent">SZ Ambassador</option>
                                    </select>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-12" id="discipline_details" data-dcid="0" data-dcsid="0">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="row pl-5 pr-5">
                                <!-- <div class="form-group col-6">
                                    <div class="font-weight-bold">Date of Incident:</div><input name="dateOfIncident" placeholder="Click to select date of incident" id="dateOfIncident" class='form-control m-input input_customize' readonly>
                                </div> -->
                                <!-- <div class="form-group col-6">
                                    <div class="font-weight-bold">Time of Incident:</div><input id="timeOfIncident" placeholder="Click to select time of incident" class='form-control m-input input_customize' name="timeOfIncident" readonly>
                                </div> -->
                            </div>
                            <div class="row pl-5 pr-5">
                                <div class="col-12 font-weight-bold">
                                    Description of Incident:
                                </div>
                                <div class="form-group col-12">
                                    <textarea class='form-control m-input' name='descriptionOfIncident' id='descriptionOfIncident' rows='1' data-min-rows='1' style='resize: none; overflow: hidden; height: 55px;background-color: #f1f1f1' placeholder='Type incident description here...' data-mentions-input='true'></textarea>
                                </div>
                                <!-- <br><br><br> -->
                                <!-- <div class="col-12 font-weight-bold">
                                    Place of Incident:
                                </div>
                                <div class="form-group col-12">
                                    <input type="text" class='form-control m-input input_customize' name='placeOfIncident' id='placeOfIncident' rows='1' placeholder='Type place of incident here...' data-mentions-input='true'>
                                </div>
                                <br><br><br>
                                <div class="col-12 font-weight-bold">
                                    Expected Course of Action:
                                </div>
                                <div class="form-group col-12">
                                    <textarea class='form-control m-input' name='courseOfAction' id='courseOfAction' rows='1' data-min-rows='1' style='resize: none; overflow: hidden; height: 55px;background-color: #f1f1f1' placeholder='Type expected course of action here...' data-mentions-input='true'></textarea>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-12">
                            <div id="offense_det_action" data-filing="0" class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Offense Details and
                                    Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold">Offense:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0 mb-3" style="font-size: 13px; font-weight: 400" id="">
                                    <select class="form-control m-select2" name="offenseList" id="offenseList">
                                    </select>
                                </div>
                                <div class="col-12 col-md-3 font-weight-bold">Class:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0 mb-3" style="font-size: 13px; font-weight: 400" id="">
                                    <select class="form-control m-select2" name="categoryList" id="categoryList">
                                    </select>
                                </div>
                                <div class="col-12 col-md-3 font-weight-bold">Disciplinary Action:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0 mb-3" style="font-size: 13px; font-weight: 400" id="">
                                    <select class="form-control m-select2" name="actionList" id="actionList">
                                    </select>
                                </div>
                                <div class="col-12 col-md-3 font-weight-bold">Cure Date:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="">
                                    <input name="cureDate" placeholder="Click to Select Cure Date" id="cureDate" class='form-control m-input input_customize' readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i>Close</button>
                    <button type="submit" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"><i class="fa fa-check"></i>Add IR</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="modal fade" id="showIrHistoryDetails" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <div class="col-8">
                    <h5 class="modal-title" id="exampleModalLabel">View Incident Report History</h5>
                </div>
                <div class="col-2">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;"><span aria-hidden="true">×</span></button>
                </div>
            </div>
            <div class="modal-body p-4">
                <div class="row" id="irDetailsIrView">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i>Close</button>
                <button type="submit" class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"><i class="fa fa-check"></i>Add IR</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/cod_settings.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision.js"></script>
<script>
    var addHistoryDatatable = function() {
        var addHistory = function(empIdVal) {
            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: baseUrl + "/discipline/get_added_ir_history_datatable",
                            params: {
                                query: {
                                    empId: empIdVal
                                },
                            },
                        }
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    theme: 'default', // datatable theme
                    class: '', // custom wrapper class
                    scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                    height: 550, // datatable's body's fixed height
                    footer: false // display/hide footer
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    // toolbar placement can be at top or bottom or both top and bottom repeated
                    placement: ['bottom'],

                    // toolbar items
                    items: {
                        // pagination
                        pagination: {
                            // page size select
                            pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
                        },
                    }
                },
                // columns definition
                columns: [{
                    field: 'incidentReport_ID',
                    title: 'IR-ID',
                    textAlign: 'center',
                    sortable: true,
                    width: 120,
                    template: function(row, index, datatable) {
                        return "<span style='font-size:13px;'>" + row.incidentReport_ID.padLeft(8) + "</span>";
                    }
                }, {
                    field: 'name',
                    title: 'Subject',
                    textAlign: 'center',
                    sortable: true,
                    width: 120,
                    template: function(row, index, datatable) {
                        return "<span style='font-size:13px;'>" + row.subjectLname + ", " + row.subjectFname + "</span>";
                    }
                }, {
                    field: 'offense',
                    title: 'Offense',
                    textAlign: 'center',
                    sortable: true,
                    template: function(row, index, datatable) {
                        return "<span style='font-size:12px;'>" + row.offense + "</span>";
                    }
                }, {
                    field: 'finalAction',
                    title: 'Disciplinary Action',
                    sortable: true,
                    textAlign: 'center',
                    template: function(row, index, datatable) {
                        return "<span style='font-size:12px;'>" + row.finalAction + "</span>";
                    }
                }, {
                    field: 'incidentDate',
                    title: 'Incident Datetime',
                    textAlign: 'center',
                    sortable: true,
                    width: 90,
                    template: function(row, index, datatable) {
                        return moment(row.incidentDate).format('MMM DD YYYY');
                    }
                }, {
                    field: 'Actions',
                    width: 90,
                    title: 'Actions',
                    textAlign: 'center',
                    sortable: false,
                    overflow: 'visible',
                    template: function(row, index, datatable) {
                        return '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air mr-2 irHistoryView" title="View Details" data-incidentreportid="' + row.incidentReport_ID + '"><i class="la la-eye"></i></a> <a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air removeIrHistory" title="View Details" data-incidentreportid="' + row.incidentReport_ID + '"><i class="fa fa-trash-o"></i></a>';
                    }
                }],
            };
            var datatable = $('#ir_history_datatable').mDatatable(options);
        };
        return {
            init: function(empIdVal) {
                addHistory(empIdVal);
            }
        };

    }();

    function initIrHistoryRecord() {
        $('#ir_history_datatable').mDatatable('destroy');
        addHistoryDatatable.init($('#subjectEmployeeSearch').val());
    }

    function init_emp_ir_history_list(callback) {
        $.when(fetchGetData('/discipline/get_add_ir_emp_list')).done(function(emp) {
            let empObj = $.parseJSON(emp.trim());
            console.log(empObj);
            let optionsAsString = "<option val='0'>All</option>";
            // Subject list
            if (parseInt(empObj.exist)) {
                $.each(empObj.record, function(index, value) {
                    optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.subjectLname + ", " + value.subjectFname) + "</option>";
                });
                $('#subjectEmployeeSearch').html(optionsAsString);
                $('#subjectEmployeeSearch').select2({
                    placeholder: "Select an Employee",
                    width: '100%'
                });
                $('#noIrHistoryRecords').hide();
                $('#ir_history_div').fadeIn();
            } else {
                $('#noIrHistoryRecords').fadeIn();
                $('#ir_history_div').hide();
                $('#subjectEmployeeSearch').html(optionsAsString);
                $('#subjectEmployeeSearch').select2({
                    placeholder: "Select an Employee",
                    width: '100%'
                });
            }

            callback(empObj.exist);
        });
    }

    function initIrView(element) {
        $.when(fetchPostData({
            ir_id: $(element).data('incidentreportid'),
        }, '/discipline/get_ir_details')).then(function(irDetails) {
            let irDetailsObj = $.parseJSON(irDetails.trim());
            $('#irDetailsIrView').html(irDetailsObj.ir_details_layout);
            $('#showIrHistoryDetails').modal('show');
        });
    }

    function init_disc_action(callback) {
        $.when(fetchPostData({
            categId: $('#categoryList').val(),
        }, '/discipline/get_all_assignable_disc_action_record')).then(function(discAction) {
            let discActionObj = $.parseJSON(discAction.trim());
            let optionsAsString = "<option></option>";
            if (parseInt(discActionObj.exist)) {
                $.each(discActionObj.record, function(index, value) {
                    optionsAsString += "<option value='" + value.disciplinaryActionCategory_ID + "' data-discactionsettingsid='" + value.disciplinaryActionCategorySettings_ID + "' >" + toTitleCase(value.action) + "</option>";
                });
            }
            $('#actionList').html(optionsAsString);
            $('#actionList').select2({
                placeholder: "Select a Disciplinary Action",
                width: '100%'
            })
            callback(discAction);
        });
    }

    function get_all_offense_categ(callback) {
        $.when(fetchGetData('/discipline/get_all_offenses_records'), fetchGetData('/discipline/get_all_class_records')).done(function(offenses, categories) {
            let offensesObj = $.parseJSON(offenses[0].trim());
            let categoriesObj = $.parseJSON(categories[0].trim());
            let optionsAsStringOffense = "<option></option>";
            let optionsAsStringCateg = "<option></option>";
            // Offenses
            if (parseInt(offensesObj.exist)) {
                $.each(offensesObj.record, function(index, value) {
                    optionsAsStringOffense += "<option value='" + value.offense_ID + "'>" + toTitleCase(value.offense) + "</option>";
                });
            }
            $('#offenseList').html(optionsAsStringOffense);
            $('#offenseList').select2({
                placeholder: "Select an Offense",
                width: '100%'
            })

            // categories
            if (parseInt(categoriesObj.exist)) {
                $.each(categoriesObj.record, function(index, value) {
                    optionsAsStringCateg += "<option value='" + value.categId + "'>" + toTitleCase(value.category) + "</option>";
                });
            }
            $('#categoryList').html(optionsAsStringCateg);
            $('#categoryList').select2({
                placeholder: "Select a Class",
                width: '100%'
            })
            callback(offensesObj);
        });
    }

    function add_ir() {
        $.when(fetchPostData({
            empId: $('#subjectEmployee').val(),
            incidentDate: $('#dateOfIncident').val(),
            incidentDescription: $('#descriptionOfIncident').val(),
            offenseId: $('#offenseList').val(),
            offense: $('#offenseList option:selected').text(),
            discActionCateg: $('#actionList').val(),
            discActionCategSettingsId: $('#actionList option:selected').data('discactionsettingsid'),
            cureDate: $('#cureDate').val()
        }, '/discipline/add_ir_history')).then(function(addIrHistory) {
            let addIrHistoryObj = $.parseJSON(addIrHistory.trim());
            if (parseInt(addIrHistoryObj.insert_stat) == 2) {
                swal("Already Added", "An existing record of the same subject and the same offense already exist", "error");
            } else if (parseInt(addIrHistoryObj.insert_stat) == 1) {
                swal("Successful Added", "", "success");
                $('#addIrModalForm').modal('hide');
            } else {
                swal("Error", "Something went wrong while adding the IR record. Please report this immediately to the system administrator", "error");
                $('#addIrModalForm').modal('hide');
            }
            init_emp_ir_history_list(function(exist) {
                initIrHistoryRecord();
            });
        });
    }

    function confirm_add_ir() {
        swal({
            title: 'Is this Final?',
            html: 'Kindly double check before you proceed. Record is not editable after submission',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Add IR',
            cancelButtonText: 'Back and Review',
            reverseButtons: true,
            // width: '500px'
        }).then(function(result) {
            if (result.value) {
                add_ir();
            } else if (result.dismiss === 'cancel') {
                $('#addIrForm').formValidation('disableSubmitButtons', false);
            }
        });
    }

    function remove_ir(element) {
        $.when(fetchPostData({
            irId: $(element).data('incidentreportid')
        }, '/discipline/remove_ir_history')).then(function(removeIrHistory) {
            let removeIrHistoryVal = $.parseJSON(removeIrHistory.trim());
            if (parseInt(removeIrHistoryVal) == 1) {
                swal("Successful Removed", "", "success");
            } else if (parseInt(removeIrHistoryVal) == 2) {
                swal("Record Does Not Exist", "It seems that the record you are about to remove was already deleted, or does not really exist. Please report this immediately to the system administrator", "error");
            } else {
                swal("Error", "Something went wrong while removing the IR record. Please report this immediately to the system administrator", "error");
            }
            init_emp_ir_history_list(function(exist) {
                initIrHistoryRecord();
            });
        });
    }

    function confirm_remove(element) {
        let ir_id = $(element).data('incidentreportid') + '';
        swal({
            title: 'Are you sure you want to remove IR history ' + ir_id.padLeft(8) + '?',
            html: 'You will not be able to revert back the record after removal',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, remove',
            cancelButtonText: 'Cancel',
            reverseButtons: true,
            // width: '500px'
        }).then(function(result) {
            if (result.value) {
                remove_ir(element);
            } else if (result.dismiss === 'cancel') {
                $('#addIrForm').formValidation('disableSubmitButtons', false);
            }
        });
    }

    $('#addIrModal').on('click', function() {
        getEmployee(0, 0, '', '#subjectEmployee', function(empExistStat) {
            get_all_offense_categ(function(offenses) {
                init_disc_action(function(disc_action) {
                    initDatePicker('#dateOfIncident', true, function() {});
                    initDatePicker('#cureDate', true, function() {});
                    $('#addIrModalForm').modal('show');
                })
            });
        })
    })

    $('#subjectEmployeeSearch').on('change', function() {
        initIrHistoryRecord();
    })
    $('#addIrForm')
        .find('[name="subjectEmployee"]')
        .change(function(e) {
            $('#addIrForm').formValidation('revalidateField', 'subjectEmployee');
        })
        .end()
        .find('[name="offenseList"]')
        .change(function(e) {
            $('#addIrForm').formValidation('revalidateField', 'offenseList');
        })
        .end()
        .find('[name="categoryList"]')
        .change(function(e) {
            $('#addIrForm').formValidation('revalidateField', 'categoryList');
        })
        .end()
        .find('[name="actionList"]')
        .change(function(e) {
            $('#addIrForm').formValidation('revalidateField', 'actionList');
        })
        .end()
        .find('[name="cureDate"]')
        .change(function(e) {
            $('#addIrForm').formValidation('revalidateField', 'cureDate');
        })
        .end()
    $('#addIrForm').formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        //        live: 'disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            subjectEmployee: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Subject Employee is Required'
                    }
                }
            },
            dateOfIncident: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Subject Employee is Required'
                    }
                }
            },
            // timeOfIncident: {
            //     validators: {
            //         notEmpty: {
            //             message: 'Oops! Subject Employee is Required'
            //         }
            //     }
            // },
            descriptionOfIncident: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Incident Description is Required'
                    }
                }
            },
            categoryList: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Class is Required'
                    }
                }
            },
            actionList: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Disciplinary Action is Required'
                    }
                }
            },
            offenseList: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Offense is Required'
                    }
                }
            },
            cureDate: {
                validators: {
                    notEmpty: {
                        message: 'Oops! Cure Date is Required'
                    }
                }
            },
            // placeOfIncident: {
            //     validators: {
            //         notEmpty: {
            //             message: 'Oops! Subject Employee is Required'
            //         }
            //     }
            // },
            // courseOfAction: {
            //     validators: {
            //         notEmpty: {
            //             message: 'Oops! Subject Employee is Required'
            //         }
            //     }
            // },
        }
    }).on('success.form.fv', function(e, data) {
        e.preventDefault();
        confirm_add_ir();
        // $('#qualifiedDtrVioForm').formValidation('disableSubmitButtons', true);
        // var frm = new FormData(this);
        // // fileIrQualifiedDtrViolation(frm);
        // confirmFileIrQualified(frm)
    });
    $('#categoryList').on('change', function() {
        init_disc_action(function(discAction) {})
    });
    $('#ir_history_div').on('click', '.removeIrHistory', function() {
        confirm_remove(this);
    })
    $('#ir_history_div').on('click', '.irHistoryView', function() {
        initIrView(this);
    })
    $('#addIrModalForm').on('hidden.bs.modal', function() {
        formReset('#addIrForm');
    });
    $('#timeOfIncident').timepicker();

    $(function() {
        init_emp_ir_history_list(function(exist) {
            initIrHistoryRecord();
        });
    })
</script>