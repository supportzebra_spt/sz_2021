<!-- <div style="background-image:url(assets/images/img/logo_header.png);background-size:100%;background-repeat:no-repeat;height:100%;"> -->
<!-- <div style="padding: 72px 24px 0px 24px;"> -->
<div>
    <!-- 1 inch and 1/2 inch-->
    <h2 style="text-transform: capitalize"><b></b></h2>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="text-align:center;border: 1px solid #ddd;padding:8px;">
                <h4>NOTICE TO EXPLAIN</h4>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:13px;text-align:center">To:
                &nbsp;&nbsp;<b><?php echo $details->subjectName; ?></b> </td>
        </tr>
    </table>

    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:50%;text-align:center">Position:
                &nbsp;&nbsp; <?php echo $details->subjectPosition; ?></td>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:50%;text-transform: capitalize;text-align:center">
                Team: &nbsp;&nbsp; <?php echo $details->subjectAccount; ?></td>
        </tr>
        <tr>
            <td colspan="3" style="border: 1px solid #ddd;padding:20px 6px; font-size:12px; text-align: justify;text-justify: inter-word;border-bottom:none ">
                Based on the report(s) submitted, you are hereby required to explain in writing within 5 days from
                receipt of this memorandum why no disciplinary
                action shall be imposed on you for alleged violation of the Company Rules and Regulations which are
                as follows:
            </td>
        </tr>
    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">IR-ID:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%"> <b><?php echo str_pad($details->incidentReport_ID, 8, '0', STR_PAD_LEFT); ?></b></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Incident Details:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <?php echo ucfirst($details->details); ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Date of Incident:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <?php echo date('M d, Y', strtotime($details->incidentDate)); ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Place of Incident:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <?php echo ucfirst($details->place); ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Time of Incident:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <?php echo date('h:m A', strtotime(date('Y-m-d') . " " . $details->incidentTime)); ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Category:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%;text-transform: capitalize">
                Category <?php echo $details->category; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Offense:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <?php echo $details->offense; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Company Rules and Regulations
                violated:</td>
            <td colspan="2" style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%;text-transform: capitalize">
                <?php echo $details->offenseType; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Suggested Sanction:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:13px;width:50%;"><b><?php echo strtoupper($details->suggestedAbbreviation) ?></b></td>
            <td style="border: 1px solid #ddd;padding:6px 4px;font-size:11px;width:25%;text-align:center">Cure Date: <b><?php echo date('M d, Y', strtotime($details->prescriptionEnd)); ?></b></td>
        </tr>
    </table>
    <?php if ($history !== null && !empty($history)) : ?>
        <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
            <tr>
                <td colspan='5' style="border: 1px solid #ddd;padding:6px;font-size:12px;text-align:left">Incident
                    Report History</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:13.5%;text-align:center;background:#eee;font-weight:bold">
                    IR-ID</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:39.5%;text-align:center;background:#eee;font-weight:bold">
                    Incident Date</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:13.5%;text-align:center;background:#eee;font-weight:bold">
                    Suggested</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:13.5%;text-align:center;background:#eee;font-weight:bold">
                    Final</td>
                <td style="border: 1px solid #ddd;padding:5px 8px;font-size:12px;width:20%;text-align:center;background:#eee;font-weight:bold">
                    Cure Date</td>
            </tr>
            <?php foreach ($history as $hist) : ?>
                <tr>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:13.5%;text-align:center"><?php echo str_pad($hist->incidentReport_ID, 8, '0', STR_PAD_LEFT); ?>
                    </td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:39.5%;text-align:center"><?php echo date('M d, Y', strtotime($hist->incidentDate)) . ' ' . date('h:i:s A', strtotime($hist->incidentDate . " " . $hist->incidentTime)); ?></td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:13.5%;text-align:center"><?php echo strtoupper($hist->sugAbbreviation); ?></td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:13.5%;text-align:center">
                        <?php echo strtoupper($hist->abbreviation); ?></td>
                    <td style="border: 1px solid #ddd;padding:5px 8px;font-size:10px;width:20%;text-align:center"><?php echo ($hist->label === 't') ? "n/a" : date('M d, Y', strtotime($hist->prescriptionEnd)); ?></td>
                </tr>
            <?php endforeach; ?>

        </table>
    <?php endif; ?>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td colspan="3" style="border: 1px solid #ddd;padding:20px 6px; font-size:12px; text-align: justify;text-justify: inter-word;border-bottom:none ">
                Failure to submit the necessary explanation for the allegation made against you shall automatically
                waive the right to be heard and the Management shall, therefore,
                exercise its right to implement necessary disciplinary actions accorded under the provision of the
                Company Rules and Regulations and provisions as well stated under the Philippines Labor Law.
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border: 1px solid #ddd;padding:15px 6px;font-size:12px;"><b>For your immediate
                    compliance.</b></td>
        </tr>
    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:2px 6px;font-size:12px;width:33.3%;text-align:center">Prepared
                By:</td>
            <td style="border: 1px solid #ddd;padding:2px 6px;font-size:12px;width:33.3%;text-align:center">Approved
                By:</td>
            <td style="border: 1px solid #ddd;padding:2px 6px;font-size:12px;width:33.3%;text-align:center">Received
                and Accepted By:</td>
        </tr>

        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:33.3%;text-align:center">
                <br><br><br>
                <p style="text-transform:uppercase"><strong><?php echo $details->finalRecommender; ?></strong></p>
                <p><i>(Employee Relations Officer)</i></p>
            </td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:33.3%;text-align:center">
                <br><br>
                <p style="text-transform:uppercase"><strong><?php echo $details->supervisorName; ?></strong></p>
                <p><i>(Supervisor)</i></p>
            </td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:33.3%;text-align:center">
                <br><br>
                <p style="text-transform:uppercase"><strong><?php echo $details->subjectName; ?></strong></p>
                <p><i>(Subject)</i></p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:33.3%;">Date: </td>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:33.3%;">Date: </td>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:33.3%;">Date: </td>
        </tr>
    </table>
</div>
<!-- </div> -->