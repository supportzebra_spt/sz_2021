<!-- <div style="background-image:url(assets/images/img/logo_header.png);background-size:100%;background-repeat:no-repeat;height:100%;">
    <div style="padding: 72px 24px 0px 24px;"> -->
<div>
    <!-- 1 inch and 1/2 inch-->
    <h2 style="text-transform: capitalize"><b></b></h2>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td colspan="3" style="text-align:center;border: 1px solid #ddd;padding:8px;">
                <h4>NOTICE OF RESOLUTION</h4>
            </td>
        </tr>

    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:13px;text-align:center">To:
                &nbsp;&nbsp;<b><?php echo $details->subjectName; ?></b> </td>
        </tr>
    </table>

    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:50%;text-align:center">Position:
                &nbsp;&nbsp; <?php echo $details->subjectPosition; ?></td>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:50%;text-transform: capitalize;text-align:center">
                Team: &nbsp;&nbsp; <?php echo $details->subjectAccount; ?></td>
        </tr>
    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">IR-ID:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%"> <b><?php echo str_pad($details->incidentReport_ID, 8, '0', STR_PAD_LEFT); ?></b></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Notice to explain:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <input type="checkbox" <?php echo ($attachmentPurpose === null) ? 'checked="checked"' : "" ?>> Not Submitted &nbsp;&nbsp;
                <input type="checkbox" <?php echo (!isset($attachmentPurpose->purpose)) ? '' : ($attachmentPurpose->purpose === '3') ? 'checked="checked"' : '' ?>> Submitted &nbsp;&nbsp;
                <input type="checkbox" <?php echo (!isset($attachmentPurpose->purpose)) ? '' : ($attachmentPurpose->purpose === '5') ? 'checked="checked"' : '' ?>> Attached to IR Form
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Nature of Offense:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%"> <?php echo $details->offenseType; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Category:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%;text-transform: capitalize">
                Category <?php echo $details->category; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Offense:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <?php echo $details->offense; ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Incident Details:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%">
                <?php echo ucfirst($details->details); ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Date of incident:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%"><?php echo date('M d, Y', strtotime($details->incidentDate)); ?></td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:25%">Place of incident:</td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:75%"><?php echo ucfirst($details->place); ?></td>
        </tr>
        <tr>
            <td colspan="3" style="border: 1px solid #ddd;padding:20px 6px; font-size:12px; text-align: justify;text-justify: inter-word; ">
                We appreciate your response to your
                <b><?php echo ($attachmentPurpose === null) ? "Incident Report" : "Notice to Explain" ?>
                </b> with regard to the incident
                <b><?php echo $details->offense; ?></b> stated above.
                <br><br>
                Please be reminded that you, as <b>
                    <?php echo $details->subjectPosition; ?></b>,
                should follow the Rules and Regulations set before you. <br /><br />
                Basing on our Code of Discipline/Memorandum on <b>
                    <?php echo $details->offenseType; ?></b>,
                you are hereby sanctioned to <b>
                    <?php if (isset($suspension->days)) {
                        if ((int) $suspension->days > 1) {
                            echo $suspension->days . " days";
                        } else {
                            echo "a ". $suspension->days . " day";
                        }
                    } else {
                        echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>";
                    } ?>
                </b> suspension.

            </td>
        </tr>
        <tr>
            <td colspan="3" style="border: 1px solid #ddd;padding:20px 6px;font-size:12px;text-align: justify;text-justify: inter-word;">
                This serves as a FINAL WARNING. REPETITION of the same infraction will result to
                another suspension or TERMINATION.</td>
        </tr>
        <tr>
            <td colspan="3" style="border: 1px solid #ddd;padding:15px 6px;font-size:12px;"><b>For your strict
                    compliance.</b></td>
        </tr>
    </table>
    <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
        <tr>
            <td style="border: 1px solid #ddd;padding:2px 6px;font-size:12px;width:33.3%;text-align:center">Prepared
                By:</td>
            <td style="border: 1px solid #ddd;padding:2px 6px;font-size:12px;width:33.3%;text-align:center">Noted
                By:</td>
            <td style="border: 1px solid #ddd;padding:2px 6px;font-size:12px;width:33.3%;text-align:center">Received
                and Accepted By:</td>
        </tr>

        <tr>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:33.3%;text-align:center">
                <br><br><br>
                <p style="text-transform:uppercase"><strong><?php echo $details->finalRecommender; ?></strong></p>
                <p><i>(Employee Relations Officer)</i></p>
            </td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:33.3%;text-align:center">
                <br><br>
                <p style="text-transform:uppercase"><strong><?php echo $details->supervisorName; ?></strong></p>
                <p><i>(Supervisor)</i></p>
            </td>
            <td style="border: 1px solid #ddd;padding:6px;font-size:12px;width:33.3%;text-align:center">
                <br><br>
                <p style="text-transform:uppercase"><strong><?php echo $details->subjectName; ?></strong></p>
                <p><i>(Subject)</i></p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:33.3%;">Date: </td>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:33.3%;">Date: </td>
            <td style="border: 1px solid #ddd;padding:5px 6px;font-size:12px;width:33.3%;">Date: </td>
        </tr>
    </table>
</div>
<!-- </div> -->