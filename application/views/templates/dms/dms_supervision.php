<style>
    .bs-popover-auto[x-placement^=right] .arrow::after,
    .bs-popover-right .arrow::after {
        border-right-color: #282A3C !important;
    }

    .bs-popover-auto[x-placement^=left] .arrow::after,
    .bs-popover-left .arrow::after {
        border-left-color: #282A3C !important;
    }

    .popover-header {
        background-color: rgb(52, 55, 78) !important;
        border-bottom: 0px !important;
        color: white;
        max-width: 100% !important;
    }

    .popover-body {
        background-color: #282A3C !important;
        max-width: 100% !important;
        padding: 1.0rem 0.5rem !important;
        text-align: center;
        padding-bottom: 1px !important;
    }

    .list-active {
        background: #dcdcdc;
    }

    .list-active {
        background: #dcdcdc;
    }

    .active-nav-a {
        font-weight: bold !important;
        color: #6f727d !important;
    }

    /*charise start code*/
    .input_customize {
        resize: none;
        overflow: hidden;
        background-color: #f1f1f1 !important;
        /* height: 25px;*/
    }

    /*FOR CUSTOM POPOVER*/
    .bs-popover-auto[x-placement^=right] .arrow::after,
    .bs-popover-right .arrow::after {
        border-right-color: #282A3C !important;
    }

    .bs-popover-auto[x-placement^=left] .arrow::after,
    .bs-popover-left .arrow::after {
        border-left-color: #282A3C !important;
    }

    .popover-header {
        background-color: rgb(52, 55, 78) !important;
        border-bottom: 0px !important;
        color: white;
        max-width: 100% !important;
    }

    .popover-body {
        background-color: #282A3C !important;
        max-width: 100% !important;
        padding: 1.0rem 0.5rem !important;
        text-align: center;
        padding-bottom: 1px !important;
    }

    /*FOR CUSTOM POPOVER*/

    /*charise end code*/
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
        background: unset !important;
    }

    .m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
        background: unset !important;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
        background: #F0F0F2 !important;
        color: black !important;
    }

    .codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover {
        font-weight: bold !important;
        color: #6f727d !important;
    }

    .m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
        color: #8c8c8c;
    }

    .m-nav.m-nav--inline>.m-nav__item:first-child {
        padding-left: 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item {
        padding: 10px 20px 10px 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active {
        background: #d4d7da;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item:hover {
        background: #ececec;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
        color: #4f668a;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
        color: black;
    }

    .m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
        height: 100%;
        padding: 0.8rem 0 0.8rem 0;
    }

    .m-portlet .m-portlet__head {
        height: 3.9rem;
    }

    .m-portlet.m-portlet--tabs.active {
        display: block;
    }

    .dtrAutoIrSideTab {
        padding-left: 0.5rem !important;
        padding-right: 0.5rem !important;
        padding-top: 0.8rem !important;
        padding-bottom: 0.8rem !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        margin-right: 1px !important;
    }

    .editableStyle {
        border-bottom: dashed 1px #0088cc;
        cursor: pointer;
    }

    #ongoingPulsesTable,
    #ongoingPulsesTableRecommend,
    #ongoingPulsesTableClient,
    #ongoingPulsesTableEffective,
    #ongoingPulsesTableInvolve.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell {
        font-size: 0.9rem !important;
    }

    /* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
    } */
    /* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
    } */

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding: 0.65rem 2.9rem 0.8rem 1rem !important;
        white-space: normal;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
        top: 47% !important;
    }

    #irAbleDtrViolationDatatable>.m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__foot,
    .m-datatable.m-datatable--default.m-datatable--scroll>.m-datatable__table>.m-datatable__head {
        background: #e0e0e0;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }

    .select2-selection__arrow b {
        display: none !important;
    }

    .alert_details_custom {
        margin: auto;
        width: -webkit-fill-available;
    }

    /*.select2-container--default .select2-selection--single {
        border: 1px solid #ffffff;
        }*/

    /*Charise start Code
span.select2-selection.select2-selection--single {
    border: 1px;
    border-bottom-style: dashed;
}

span.select2-selection__arrow {
    display: none !important;
}
*/
    ul.select2-selection__rendered {
        background: #f1f1f1 !important;
    }

    .modal {
        overflow: auto !important;
    }

    .pendingIRlabel {
        color: #0d72bf;
        font-style: italic;
    }

    .photosize img {
        height: auto;
        width: 100%;
        overflow: hidden;
    }

    .img {
        width: 100px;
        height: 100px;

        /*Scale down will take the necessary specified space that is 100px x 100px without stretching the image*/
        object-fit: cover;

    }

    /*charise end code*/
    td.disabled.day {
        text-decoration: line-through;
        color: #cecece !important;
    }

    /*NICCA CSS*/
    .font-12 {
        font-size: 12px !important;
    }

    .m-datatable__head {
        background: #e0e0e0;
    }

    .highcharts-exporting-group,
    .highcharts-credits {
        display: none
    }

    .highcharts-title {
        font-family: 'Poppins' !important;
    }

    #tblSusTermDetails thead td {
        padding: 5px;
        border: 1px solid #5ba05e;
        text-align: center;
    }

    tbody#dateShow td {
        border: 1px solid #95c797;
    }

    .datepicker.datepicker-inline {
        width: 410px !important;
    }

    .badgeMJM {
        float: right;
    }

    .locked {
        font-weight: inherit;
        font-size: x-large;
    }
</style>
<!-- <link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/lightbox2/dist/css/lightbox.css" /> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link href="<?php echo base_url(); ?>assets/src/custom/plugins/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">DMS Supervision</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>

    </div>
    <div class="m-content">
        <input type="hidden" id="hidden_base_url" name="hidden_base_url" value="<?php echo base_url(); ?>" />
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview" style="padding:0px !important">
                <ul class="dms m-nav m-nav--inline">
                    <li class="m-nav__item active">
                        <a href="#" class="dmsSupervisionMainNav m-nav__link active" data-navcontent="dtrViolations">
                            <i class="m-nav__link-icon fa fa-clock-o"></i>
                            <span class="m-nav__link-text">DTR Violations</span>
                            <span class="m-nav__link-badge" id="dtrViolationBadge" style="display:none">
                                <span class="m-badge m-badge--primary m-badge--wide" id="dtrViolationBadgeCount"></span>
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsSupervisionMainNav m-nav__link" data-navcontent="irSupervision" onclick="setTabActive(1)">
                            <i class="m-nav__link-icon fa fa-gears"></i>
                            <span class="m-nav__link-text">IR Supervision</span>
                            <span class="m-nav__link-badge">
                                <span class="m-badge m-badge--primary m-badge--wide" id="supervisionBadge">0</span>
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsSupervisionMainNav m-nav__link" data-navcontent="irProfile" id="supervisionIrProfile">
                            <i class="m-nav__link-icon fa fa-user-circle-o"></i>
                            <span class="m-nav__link-text">IR Profile</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs active" id="dtrViolations">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a id="violationLogTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#violationsLog" role="tab">
                                <i class="fa fa-balance-scale"></i>Violations Log</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrTab" class="nav-link m-tabs__link" data-toggle="tab" href="#fileDtrViolationIr" role="tab">
                                <i class="fa fa-list-alt"></i>File DTR Violation IR
                                <span class="m-nav__link-badge ml-2" style="display:none" id="fileDtrViolationIrBadge">
                                    <!-- <span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">new</span> -->
                                    <span class="m-badge m-badge--primary m-badge--wide" id="fileDtrViolationIrBadgeCount">23</span>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="fileDtrViolationIrRecordTab" class="nav-link m-tabs__link" data-toggle="tab" href="#fileDtrViolationIrRecord" role="tab">
                                <i class="fa fa-list"></i>Record</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="dismissedDtrViolationsTab_supervision" class="nav-link m-tabs__link" data-toggle="tab"
                                href="#dismissedDtrViolations_supervision" role="tab">
                                <i class="fa fa-window-close-o"></i>Dismissed Qualified Violations</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="violationsLog" role="tabpanel">
                        <div class="row no-gutters">
                            <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                <select name="" class="m-input form-control font-12" id="violation-class" data-toggle="m-tooltip" title="" data-original-title="Employee Type">
                                    <option value="">All Class</option>
                                    <option value="Agent">Ambassador</option>
                                    <option value="Admin">SZ Team</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                <select name="" class="m-input form-control font-12" id="violation-accounts" data-toggle="m-tooltip" title="" data-original-title="Accounts">
                                </select>
                            </div>

                            <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px" placeholder="Select date range" id="violation-date" data-toggle="m-tooltip" title="" data-original-title="Date Recorded" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-calendar"></i></span></span>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                <select class="form-control text-capitalize font-12" id="violation-type" data-toggle="m-tooltip" title="" data-original-title="Violation Type">
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input font-12" style="padding-bottom:12px" placeholder="Search Name..." id="violation-search" data-toggle="m-tooltip" title="" data-original-title="Employee Name" />
                                    <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                                </div>
                            </div>
                        </div>

                        <h5 id="violation-header" class="mt-3 text-center"></h5>
                        <hr style="border-style: dashed;">
                        <ul class="nav nav-tabs px-1" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#violation-chart"><i class="fa fa-pie-chart m--font-brand"></i> Graphical View</a>
                            </li>
                            <li class="nav-item">
                                <a id="violation-tabular-tab" class="nav-link" data-toggle="tab" href="#violation-tabular"><i class="fa fa-table m--font-info"></i> Tabular View</a>
                            </li>
                        </ul>
                        <div class="tab-content px-1">
                            <div class="tab-pane active" id="violation-chart" role="tabpanel">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-xl-6">
                                        <div id="container-chart-pie" style="height: 300px;margin: 0 auto"></div>
                                    </div>
                                    <div class="col-xl-6 py-5 px-4">
                                        <div class="m-widget15">
                                            <div class="m-widget15__items" id="violationList">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="violation-tabular" role="tabpanel">
                                <div class="row" id="secureDtrViolationDatatable" style="margin-right: 0px; margin-left: 0px;">
                                    <div class="col-12 text-center">
                                        <i style="font-size: 42px;margin-right:1rem" class="fa fa-lock"></i>
                                        <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">Authorized Access Only</span>
                                    </div>
                                </div>
                                <div class="m_datatable" id="datatable_dtrviolations" style="display:none"></div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIr" role="tabpanel">
                        <div class="row" id="secureQualified" style="margin-right: 0px; margin-left: 0px;">
                            <div class="col-12 text-center">
                                <i style="font-size: 42px;margin-right:1rem" class="fa fa-lock"></i>
                                <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">Authorized Access Only</span>
                            </div>
                        </div>
                        <div class="row" id="qualifiedTabView" style="display:none">
                            <div class="col-md-5 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpSelect" id="fileDtrViolationEmpSelect" name="fileDtrViolationEmpSelect" data-toggle="m-tooltip" title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationType" name="violationType" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Late Clock Out</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control m-input datePicker" readonly="" id="dtrViolationSched" name="dtrViolationSched" placeholder="Select a Date">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data shown in the table below.</span> -->
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolation" style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolation" style="display:none;">
                                    <div class="col-md-12 col-lg-2 p-0">
                                        <div class="col-md-12" id="pendingLateCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingLateCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingObCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingObCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">OB</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingUtCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingUtCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">UT</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingForgotToLogoutCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingForgotToLogoutCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">FTL</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncBreakCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingIncBreakCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingIncDtrCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingIncDtrCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc DTR</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="pendingAbsentCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="pendingAbsentCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-10 pl-0">
                                        <div id="irAbleDtrViolationDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="fileDtrViolationIrRecord" role="tabpanel">
                        <div class="row" id="secureQualifiedRecord" style="margin-right: 0px; margin-left: 0px;">
                            <div class="col-12 text-center">
                                <i style="font-size: 42px;margin-right:1rem" class="fa fa-lock"></i>
                                <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">Authorized Access Only</span>
                            </div>
                        </div>
                        <div class="row" id="qualifiedRecordTabView" style="display:none">
                            <div class="col-md-4 mb-3">
                                <select class="form-control m-select2 fileDtrViolationEmpRecordSelect" id="fileDtrViolationEmpRecordSelect" name="fileDtrViolationEmpRecordSelect" data-toggle="m-tooltip" title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="fileDtrViolationRecordDateRange">
                                    <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Schedule">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeRecord" name="violationTypeRecord" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Late Clock Out</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-md-2 mb-3">
                                <select class="custom-select form-control" id="fileDtrViolationRecordStatus" name="fileDtrViolationRecordStatus" data-toggle="m-tooltip" title="action">
                                    <option value="0">ALL</option>
                                    <option value="3">Completed</option>
                                    <option value="12">Missed</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noIrableDtrViolationRecord" style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No DTR Violation Record Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="irableDtrViolationRecord" style="display:none">
                                    <div class="col-md-12 col-lg-2 p-0">
                                        <div class="col-md-12" id="recordLateCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-bell text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordLateCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Late</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordObCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-tachometer text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordObCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">OB</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordUtCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-hourglass-2 text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordUtCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">UT</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordForgotToLogoutCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-unlock-alt text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordForgotToLogoutCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">FTL</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncBreakCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-history text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordIncBreakCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc. Break</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordIncDtrCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-clock-o text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordIncDtrCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Inc. Logs</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="recordAbsentCountDiv" style="display:none">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-dismissible fade show" role="alert" style=" border-color: #dcddde!important;">
                                                <div class="m-alert__icon mini_alert bg-secondary">
                                                    <span class="m-alert--icon-solid m-alert__icon" style="padding: 7px 0px 7px 0px !important;">
                                                        <i class="la la-user-times text-info"></i>
                                                    </span>
                                                    <span style="border-left-color: #343a40 !important;"></span>
                                                </div>
                                                <div class="m-alert__text text-center mini_alert text-dark">
                                                    <strong class="font-size-20" id="recordAbsentCount">0</strong>
                                                    <br>
                                                    <span class="text-info" style="font-weight: 500;">Absent</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-10 pl-0">
                                        <div id="irAbleDtrViolationRecordDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade col-md-12" id="dismissedDtrViolations_supervision" role="tabpanel">
                        <div class="row" id="secureQualifiedRecord_dismissed" style="margin-right: 0px; margin-left: 0px;">
                            <div class="col-12 text-center">
                                <i style="font-size: 42px;margin-right:1rem" class="fa fa-lock"></i>
                                <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">Authorized Access Only</span>
                </div>
            </div>
            <div class="row" id="qualifiedRecordTabView_dismissed" style="display:none">
                            <div class="col-md-5 mb-3">
                                <select class="form-control m-select2 dismissedEmpMonitoring"
                                    id="dismissedEmpMonitoring_s" name="dismissedEmpMonitoring_s" data-toggle="m-tooltip"
                                    title="Select Employee Name"></select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly=""
                                        placeholder="Select date range" id="dismissedEmpMonitoringDateRange_s">
                                    <div class="input-group-append" data-toggle="m-tooltip" title=""
                                        data-original-title="Date Schedule">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <select class="custom-select form-control" id="violationTypeMonitoringDismissed_s"
                                    name="violationTypeMonitoringDismissed_s" data-toggle="m-tooltip" title="Violations">
                                    <option value="0">ALL</option>
                                    <option value="1">Late</option>
                                    <option value="4">Over Break</option>
                                    <option value="2">Undertime</option>
                                    <option value="3">Late Clockout</option>
                                    <option value="6">Incomplete Break</option>
                                    <option value="7">Incomplete DTR Logs</option>
                                    <option value="5">Absent</option>
                                </select>
                            </div>
                            <div class="col-12">
                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="row" id="noDismissedQualifiedDtrViolations"
                                    style="display:none; margin-right: 0px; margin-left: 0px;">
                                    <div class="col-md-12"
                                        style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                        <div class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 40px;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class">No Dismissed DTR Violation Record
                                                Found</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="dismissed_ir" style="">
                                    <div class="col-md-12 pl-0">
                                        <div id="dismissedQualifiedDtrVioDatatable_supervision">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="irSupervision" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item ">
                            <a id="irSup_explanation" class="nav-link m-tabs__link active show" data-toggle="tab" href="#explanationTab" role="tab" onclick="subjExplainFunc(1)">
                                <i class="fa fa-balance-scale"></i>Subject Explanation <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink badgeMJM" id="badgeExplain"></span></a>

                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="irSup_recommendation" class="nav-link m-tabs__link" data-toggle="tab" href="#recommendationTab" role="tab" onclick="subjExplainFunc(2)">
                                <i class="fa fa-list-alt"></i>Recommendation <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink badgeMJM" id="badgeRecommend"></span></a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="irSup_clientConfirmation" class="nav-link m-tabs__link" data-toggle="tab" href="#clientConfirmationTab" role="tab" onclick="subjExplainFunc(3)">
                                <i class="fa fa-legal"></i>Client Confirmation <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink badgeMJM" id="badgeClient"></span></a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="irSup_setDates" class="nav-link m-tabs__link" data-toggle="tab" href="#effectivityDatesTab" role="tab" onclick="subjExplainFunc(4)">
                                <i class="fa fa-calendar"></i>Effectivity Dates <span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger m-animate-blink badgeMJM" id="badgeEffective"></span></a>
                        </li>
                        <li class="nav-item m-tabs__item" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="This tab is where you can view all IR records of your subordinates and also your involvement with other IR's as recommender.">
                            <a id="irSup_involvement" class="nav-link m-tabs__link" data-toggle="tab" href="#irInvolveTab" role="tab" onclick="subjExplainFunc(7)">
                                <i class="fa fa-group"></i>IR Involvement</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12 active p-1" id="explanationTab" role="tabpanel">
                        <div class="row">
                            <div class="col-md-2 mb-2">
                            </div>
                            <div class="col-md-5 mb-2">
                            </div>
                            <div class="col-md-5 mb-2">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID" id="ongoingSearch1">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data
                                        below.</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="ongoingPulsesTable"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12 p-1" id="recommendationTab" role="tabpanel">
                        <div class="row">
                            <div class="col-md-2 mb-2">
                            </div>
                            <div class="col-md-5 mb-2">
                            </div>
                            <div class="col-md-5 mb-2 irSupervisionFilter" style="display:none">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID" id="ongoingSearch2">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data
                                        below.</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="ongoingPulsesTableRecommend"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12 p-1" id="clientConfirmationTab" role="tabpanel">
                        <div class="row">
                            <div class="col-md-2 mb-2">
                            </div>
                            <div class="col-md-5 mb-2">
                            </div>
                            <div class="col-md-5 mb-2 irSupervisionFilter" style="display:none">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID" id="ongoingSearch3">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data
                                        below.</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="ongoingPulsesTableClient"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12 p-1" id="effectivityDatesTab" role="tabpanel">
                        <div class="row">
                            <div class="col-md-2 mb-2">
                            </div>
                            <div class="col-md-5 mb-2">
                            </div>
                            <div class="col-md-5 mb-2 irSupervisionFilter" style="display:none">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID" id="ongoingSearch4">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data
                                        below.</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="ongoingPulsesTableEffective"></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show col-md-12 p-1" id="irInvolveTab" role="tabpanel">
                        <div class="row">
                            <div class="col-md-3 mb-2 irSupervisionFilter" style="display:none">
                                <div class="input-group">
                                    <select class="form-control m-select2" id="irInvolveType">
                                        <option value="r" selected>Recommendation</option>
                                        <option value="s">Subordinates</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 mb-2 irSupervisionFilter" style="display:none">
                                <div class="input-group pull-right">
                                    <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="myIrs_incidentDate">
                                    <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Incident Date">
                                        <span class="input-group-text">
                                            <i class="la la-calendar-check-o"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 mb-2 irSupervisionFilter" style="display:none">
                                <div class="form-group m-form__group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search by IR-ID" id="ongoingSearch7">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data
                                        below.</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="ongoingPulsesTableInvolve"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12" style="background: white;margin-bottom: 1.5rem;display:none" id="empIrProfileSelect">
            <div class="form-group m-form__group row p-2">
                <label class="col-lg-3 font-weight-bold col-form-label">Select Subordinate Name:</label>
                <div class="col-lg-9">
                    <select class="form-control m-select2 text-truncate" id="empListHistory" name="empListHistory" data-toggle="m-tooltip" title="Select Employee Name"></select>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs" id="irProfile" style="display:none">
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="row p-4" id="AccessSupIrProfile" style="margin-left: -3.3rem;margin-right:-3.3rem;margin-top: -2.3rem;margin-bottom: -2.3rem; box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);display:none;">
                        <div class="col-12 col-md-3 mb-2">
                            <img src="" onerror="noImageFound(this)" alt="" class="img-fluid mx-auto d-block" style="border: 0.3rem solid gainsboro;" id="profilePic">
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="row mb-3">
                                <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">Employee Information</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="row pl-3 pr-5 mb-3">
                                <div class="col-12 col-md-4 font-weight-bold">Fullname:</div>
                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="profileName"></div>
                                <div class="col-12 col-md-4 font-weight-bold">Current Position:</div>
                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"> <span id="profilePosition" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span> - <span id="profileEmpStatus" style="font-size: 11px;color: #0012ff;"></span></div>
                                <div class="col-12 col-md-4 font-weight-bold">Current Team:</div>
                                <div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="profileAccount"></div>
                            </div>
                            <div class="row mb-4">
                                <span class="ml-3" style="font-size: 17px; font-weight: 500; color: #5d4747;">IR Record Range</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="input-group pull-right">
                                        <input type="text" class="form-control m-input" readonly="" placeholder="Select date range" id="irProfileDateRange" style="background: white;">
                                        <div class="input-group-append" data-toggle="m-tooltip" title="" data-original-title="Date Schedule">
                                            <span class="input-group-text" style="background: #fbfbfb !important;">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <select class="custom-select form-control" id="irProfileLiabilityStat" name="irProfileLiabilityStat" placeholder="Select a liability">
                                        <option value="0">All</option>
                                        <option value="17">Liable</option>
                                        <option value="18">Non-Liable</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center" id="noAccessSupIrProfile">
                        <i style="font-size: 42px;margin-right:1rem" class="fa fa-lock"></i>
                        <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">Authorized Access Only</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-row--no-padding" id="showIrPofileRecords" style="display:none">
            <div class="col-xl-7 pr-xl-4">
                <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 2.2rem 2.2rem;">
                        <div class="m-widget26">
                            <div class="m-widget26__content">
                                <div class="natureOffenseChart" id="natureOffenseChart" style="overflow: visible !important;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 2.2rem 2.2rem;">
                        <div class="m-widget26">
                            <div class="m-widget26__content text-center">
                                <span class="mr-2">
                                    <i class="fa fa-file-text" style="font-size: 2rem;color: #ff5371;"></i>
                                </span>
                                <span class="m-widget26__number mr-2" id="irTotalCount">640</span>
                                <span class="m-widget26__desc" style="font-weight: 500 !important;">Total Incident Report Count</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 0 !important">
                        <div class="m-widget1" style="padding-top: 1rem !important;padding-bottom: 1rem !important" id="discActionCount">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12" id="offenseHistory" style="display:none">
                <div class="" style="margin-bottom: 1.4rem !important;border-bottom: 3px solid #f4516c;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);background-color: #fff;">
                    <div class="" style="padding: 2.2rem 2.2rem;">
                        <div class="m-widget26">
                            <div class="m-widget26__content">
                                <div class="row">
                                    <div class="col-12 col-md-9">
                                        <select class="form-control m-select2 text-truncate" id="specificOffenseHistory" name="specificOffenseHistory" data-toggle="m-tooltip" title="Select Employee Name"></select>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <div class="form-group m-form__group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search by IR-ID" id="searchProfileHistory">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mt-1" id="profileIrHistoryDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="noIrPofileRecords" style="display:none;">
            <div class="col-12 py-4 mx-3 text-center" style="background: white;">
                <i style="font-size: 42px;margin-right:1rem" class="fa fa-file-o"></i>
                <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">No Records To Show</span>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->

<div class="modal fade" id="qualifiedModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="qualifiedDtrVioForm" method="post" data-formtype="add">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="exampleModalLabel">File DTR Violation Incident Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-3">
                                    <img id="empPicApproval" src="http://3.18.221.50/sz/assets/images/img/sz.png" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="qualifiedEmployeeName">Jamelyn A. Sencio </div>
                                        <div class="col-md-12 text-center text-md-left"><span id="qualifiedEmployeeJob" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span><span id="qualifiedPosEmpStat" style="font-size: 11px;color: #0012ff;"></span>
                                        </div>
                                        <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="qualifiedEmployeeAccount">ONYX Enterprises</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row p-2">
                                <div class="col-12">
                                    <div class="row pl-4 pr-4">
                                        <span class="font-weight-bold mr-2">Incident Date:</span>
                                        <span class="" id="qualifiedDateIncident">Dec 4, 2018</span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row pl-4 pr-4">
                                        <span class="font-weight-bold mr-2">Incident:</span>
                                        <span class="" id="qualifiedIncidentType" style="color: #f50000;font-weight: 600;">Forgout to Logout</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5" id="incidentDetails"></div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Expected Course Of
                                    Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 pl-5 pr-5">
                                <div class="col-md-12 ahr_quote pt-3 pb-1 text-center" style="background: #e7ffe8 !important;">
                                    <blockquote>
                                        <i class="fa fa-quote-left"></i>
                                        <span class="mb-0" id="qualifiedExpectedAction"></span>
                                        <i class="fa fa-quote-right"></i>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 withRule" style="display:none">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR Rule</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 withRule" style="display:none">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-1 font-weight-bold">
                                    Rule:
                                </div>
                                <div class="col-12 col-md-11 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">
                                    <i style="font-size: 13px; font-weight: 400;" id="qualifiedRule"></i>
                                </div>
                                <div class="col-12">
                                    <div class="row" id="withPreviousUserDtrViolation">
                                        <div class="col-12 mt-2 font-weight-bold">
                                            Previous Consecutive Occurence:
                                        </div>
                                        <div class="col-12 mt-2" id="previousDetails">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="withIrHistory" class="col-12" style="display:none">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row p-3">
                                        <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>IR History</span>
                                        <div class="col">
                                            <hr style="margin-top: 0.8rem !important;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row pl-5 pr-5">
                                        <div class="col-12 mt-2" id="irHistoryDetails">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Offense Details</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold">Nature of Offense:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedOffenseType"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Offense:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"><i style="font-size: 13px; font-weight: 400; color: #c31717;" id="qualifiedOffense"></i></div>
                                <div class="col-12 col-md-3 font-weight-bold">Category:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedCategory"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Suggested Disciplinary
                                    Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-3 font-weight-bold">Level:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedLevel"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Disciplinary Action:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 17px; font-weight: 500" id="qualifiedDisciplinaryAction"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Period:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedPeriod"></div>
                                <div class="col-12 col-md-3 font-weight-bold">Cure Date:</div>
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedCureDate"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Witnesses </span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pr-5 pl-5">
                            <div class="col-12">
                                <select class="form-control m-select2" id="qualifiedWitnesses" placeholder="Select Multiple Witnesses" name="qualifiedWitnesses" style="width: 100% !important" multiple></select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Evidences </span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12">
                                <div class="col-12 text-center mt-3">
                                    <span style="font-size:smaller">Each file to attach should not exceed to <strong style="color:#558e82;">5MB.</strong> Allowed file types: <strong style="color:#c096ca"> doc, docx, pdf, txt, xml, xlsx, xlsm, ppt, pptx, jpeg, jpg, png, gif, mp4, mov, 3gp, wmv, avi, mpeg, mp3, wav </strong></span><br><br>
                                    <label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent" id="add_evidences_button"><i class="fa fa-plus"></i> Add Evidences <input type="file" name="upload_evidence_files_qualified[]" id="upload_evidence_files_qualified" style="display: none; " onchange="makeFileListQualified()" multiple>
                                    </label>
                                    <br>
                                    <label id="files_select_label_qualified" style="color:green;"></label>
                                    <div class="m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide ml-1 mt-3 col-12" style="max-height: 100px; height: 100px; position: relative; overflow: visible;text-align:left !important;display:none" id="display_div_file_qualified">
                                        <ol id="fileListQualified">
                                        </ol>
                                    </div>
                                </div>
                                <!-- <div class="col-12 text-center mt-3" style="background">
                                    <i class="fa fa-check"></i><span>No added attachments</span>
                                </div> -->
                                <div class="col-12 col-md-9 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400;text-align:-webkit-left" id="">
                                    <!-- <div class="col-12 container_file" id="display_div_file_qualified"> -->

                                </div>
                                <div class="text-center" id="sub_button" style="display:none;">
                                    <button id="up_button" type="submit" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air"><i class="fa fa-upload"></i> Upload </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Supervisor's Note</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 pr-5 pl-5 form-group m-form__group">
                                <textarea class='form-control m-input' rows='1' data-min-rows='1' style='resize: none; overflow: hidden; height: 55px;background-color: #f1f1f1' placeholder='' data-mentions-input='true' name="qualifiedSupervisorsNote" id="qualifiedSupervisorsNote"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><span>Close</span></button>
                    <!-- <span id="dismissBtn"></span> -->
                    <button type="button" id="dismissIrFiling" class="btn btn-danger m-btn m-btn--icon m-btn--wide"><span>Dismiss</span></button>
                    <button type="submit" class="btn btn-success m-btn m-btn--icon m-btn--wide"><span>File IR</span></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="removeWitnessesModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-date="" data-origdate="" data-termid="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="m-form m-form--fit m-form--label-align-right form-horizontal" method="post">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title">IR Witnesses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12" id="subjectEmpDetailsWitness">
                        </div>
                        <div class="col-12">
                            <div class="row p-2">
                                <div class="col-12 p-1">
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Incident</span>
                                        <div class="col">
                                            <hr style="margin-top: 0.8rem !important;">
                                        </div>
                                    </div>
                                </div>
                                <div class="row pl-4 pr-4" id="irDetailsWitness">
                                </div>
                                <div class="col-12 mt-2 p-1">
                                    <div class="row">
                                        <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i>Pending Witness</span>
                                        <div class="col">
                                            <hr style="margin-top: 0.8rem !important;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 pl-4 pr-4" id="irWitnessList">
                                    <table class="table m-table table-hover table-sm table-bordered" style="font-size:13px">
                                        <thead style="background: #555;color: white;">
                                            <tr>
                                                <th class="text-center">Witness Name</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="witnessList"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer div-datepicker">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="irHistoryProfile" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true" data-pw="" data-irid="" data-recommenlvl="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="m-form m-form--fit m-form--label-align-right form-horizontal">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title">Incident Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row ir_details_template" id="layoutHistoryIrDetails">
                    </div>
                </div>
                <div class="modal-footer divClientRecBtn div-involve">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--begin: Charise :Modal-->
<div class="modal fade" id="modal_attachedfiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header custom-modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color: #6d6565 !important;">Attached Evidences
                </h5>
                <button type="button" id="media_closebutton2" class="close" data-dismiss="modal" aria-label="Close" style="color: #6d6565 !important;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="m-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#photos_display">Photos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#videos_display">Videos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#link_display">Links</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#docu_display">Other Files</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active show m-scrollable" id="photos_display" role="tabpanel" data-scrollbar-shown="true" data-scrollable="true" data-max-height="350">
                            <!-- Photo start display -->
                            <div class="row">
                                <div class="col-lg-4 col-md-12 mb-4">
                                    <a href="<?php echo base_url('assets/images/img/avatar04.png'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/images/img/avatar04.png'); ?>" class="img z-depth-1"></a>
                                </div>
                                <div class="col-lg-4 col-md-12 mb-4">
                                    <a href="<?php echo base_url('assets/images/img/nature.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/images/img/nature.jpg'); ?>" class="img z-depth-1"></a>
                                </div>
                                <div class="col-lg-4 col-md-12 mb-4">
                                    <a href="<?php echo base_url('assets/images/img/hotdog.png'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/images/img/hotdog.png'); ?>" class="img  z-depth-1"></a>
                                </div>
                                <div class="col-lg-4 col-md-12 mb-4">
                                    <a href="<?php echo base_url('assets/images/img/nature.jpg'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/images/img/nature.jpg'); ?>" class=" img z-depth-1"></a>
                                </div>
                                <div class="col-lg-4 col-md-12 mb-4">
                                    <a href="<?php echo base_url('assets/images/img/hotdog.png'); ?>" data-lightbox="roadtrip"><img src="<?php echo base_url('assets/images/img/hotdog.png'); ?>" class="img z-depth-1"></a>
                                </div>
                            </div>
                            <!-- Photo End Display -->
                        </div>
                        <div class="tab-pane m-scrollable" id="videos_display" role="tabpanel" data-scrollbar-shown="true" data-scrollable="true" data-max-height="350">
                            <!-- Video Display -->
                            <div class="row no-gutters">
                                <div class="col-lg-4 col-md-12 rounded-lg mb-3 border border-success">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col-lg-4 col-md-12 rounded-lg mb-3 border border-success">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col-lg-4 col-md-12 rounded-lg mb-3 border border-success">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col-lg-4 col-md-12 rounded-lg mb-3 border border-success">
                                    <i class="fa fa-check"></i>
                                </div>
                            </div>
                            <!-- Video End Display -->
                        </div>
                        <div class="tab-pane m-scrollable" id="link_display" role="tabpanel" data-scrollbar-shown="true" data-scrollable="true" data-max-height="350">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged
                        </div>
                        <div class="tab-pane m-scrollable" id="docu_display" role="tabpanel" data-scrollbar-shown="true" data-scrollable="true" data-max-height="350">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="media_closebutton" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="qualifiedDetailsModalMonitor_supervision" role="dialog" aria-labelledby="exampleModalLabel"
    data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">DTR Violation Details</h5>
                <button type="button" class="close closeModalBtn" data-dismiss="modal" aria-label="Close"
                    style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 col-lg-3">
                                <img id="empPicApprovalMonitor" src="http://3.18.221.50/sz/assets/images/img/sz.png"
                                    onerror="noImageFound(this)" width="75" alt=""
                                    class="rounded-circle mx-auto d-block">
                            </div>
                            <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                <div class="row">
                                    <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;"
                                        id="qualifiedViewEmployeeNameMonitor">Jamelyn A. Sencio
                                    </div>
                                    <div class="col-md-12 text-center text-md-left"><span
                                            id="qualifiedViewEmployeeJobMonitor"
                                            style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span><span
                                            id="qualifiedViewPosEmpStat" style="font-size: 11px;color: #0012ff;"></span>
                                    </div>
                                    <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px"
                                        id="qualifiedViewEmployeeAccountMonitor">ONYX Enterprises</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="row p-2">
                            <div class="col-12">
                                <div class="row pl-4 pr-4">
                                    <span class="font-weight-bold mr-2">Incident Date:</span>
                                    <span class="" id="qualifiedViewDateIncidentMonitor">Dec 4, 2018</span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row pl-4 pr-4">
                                    <span class="font-weight-bold mr-2">Incident:</span>
                                    <span class="" id="qualifiedViewIncidentTypeMonitor"
                                        style="color: #f50000;font-weight: 600;">Forgout to Logout</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                    class="la la-tag" style="font-size: 1.1rem;"></i> Incident Details</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row pl-5 pr-5" id="incidentDetailsViewMonitor"></div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                    class="la la-tag" style="font-size: 1.1rem;"></i>Expected Course Of
                                Action</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-12 pl-5 pr-5">
                            <div class="col-md-12 ahr_quote pt-3 pb-3 text-center"
                                style="background: #e7ffe8 !important;">
                                <blockquote style="margin: 0 0 0rem;">
                                    <i class="fa fa-quote-left"></i>
                                    <span class="mb-0" id="qualifiedExpectedActionViewMonitor"
                                        style="font-size: 13px"></span>
                                    <i class="fa fa-quote-right"></i>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div id="withRuleViewMonitor" style="display:none">
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                        class="la la-tag" style="font-size: 1.1rem;"></i>IR Rule</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row pl-5 pr-5">
                                <div class="col-12 col-md-1 font-weight-bold">
                                    Rule:
                                </div>
                                <div class="col-12 col-md-11 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">
                                    <i style="font-size: 13px; font-weight: 400;" id="qualifiedRuleViewMonitor"></i>
                                </div>
                                <div class="col-12">
                                    <div class="row" id="withPreviousUserDtrViolationViewMonitor">
                                        <div class="col-12 mt-2 font-weight-bold">
                                            Previous Consecutive Occurence:
                                        </div>
                                        <div class="col-12 mt-2" id="previousDetailsViewMonitor">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                    class="la la-tag" style="font-size: 1.1rem;"></i>Reason for dismissal</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-12 pl-5 pr-5">
                            <div class="row py-3"
                                style="background: aliceblue;margin-left: 0.1rem;border-left: 8px solid #545455!important;margin-right: 0.1rem;">
                                <div class="col-sm-12 col-md-4 col-lg-2">
                                    <img id="directSupPicMonitor" src="" onerror="noImageFound(this)" width="50" alt=""
                                        class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-10 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 text-center text-md-left" style="font-size: 1rem;"
                                            id="directSupNameMonitor">Mark Jeffrey Magparoc</div>
                                        <div class="col-md-12 text-center text-md-left" id="directSupJobMonitor"
                                            style="font-size: 0.8rem;/* margin-top: -8px !important; */font-weight: bolder;color: darkcyan;">
                                            Senior Software Developer II</div>
                                        <div class="col-md-12 text-center text-md-left" id="dismissDate"
                                            style="font-size: 0.8rem;/* margin-top: -8px !important; */font-weight: bolder">
                                        </div>
                                        <div class="col-md-12 pt-3 font-italic" id="reasonMonitor">Lorem ipsum dolor
                                            sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->

<!--end: Charise:Modal-->


<!--start:MJM :Modal-->
<div class="modal fade" id="setTermEffectiveDateModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-date="" data-origdate="" data-termid="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="m-form m-form--fit m-form--label-align-right form-horizontal" method="post">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title">Set Termination Date</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="col-12">
                        <label for="exampleTextarea" id="termNote"></label>
                        <div class="datepicker" id="susTermDatepicker"></div>
                    </div>
                </div>
                <div class="modal-footer div-datepicker">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="setEffectiveDateModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-days="" data-irid="" data-lbl="" data-stid="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="m-form m-form--fit m-form--label-align-right form-horizontal" method="post">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="susTermH5" style="left: 44px;position: absolute;">Set Dates</h5>
                    <a href="#" class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" style="margin: -0.5rem -0rem -1rem auto !important;" onclick="funcInsertSusTermRecord(1)" id="btnInsertSusTerm">
                        <i class="fa fa-plus"></i>
                    </a>

                </div>
                <div class="modal-body p-4">
                    <div class="col-12 div-dateSusTermInsert">
                        <div class="form-group m-form__group row">

                            <div class="col-lg-6">
                                <label for="exampleTextarea">Suspension Date</label>
                                <input type="text" class="form-control m-input" value="<?php echo date('Y-m-d', date(strtotime("+1 day", strtotime(date("Y-m-d"))))); ?>" id="susTermDpicker" style="border: 0;border-bottom: 1px dashed black;cursor: pointer;width: 100%;" readonly />
                            </div>
                            <div class="col-lg-6">
                                <label for="exampleTextarea">Select a Schedule</label><br>
                                <select style="width: 100%;" id="datepickSchedList" onchange="changeGetSched()">
                                    <option value=0>No Schedule</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 div-dateSusTerm">
                        <small id="noteSusTerm">No. of Days: 11</small>
                        <div id="dateList">
                            <table style="width:100%;text-align: center;" id="tblSusTermDetails">
                                <thead>
                                    <tr style='background: #4CAF50;font-weight: 600;'>
                                        <td>Dates</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody id="dateShow">
                                    </thead>

                            </table>
                        </div>

                    </div>
                </div>
                <div class="modal-footer div-datepicker">
                    <button type="button" class="btn btn-info div-dateSusTermInsert" id="btnSubmitEfffective" onclick="submitEffectiveDate(2)">Submit</button>
                    <button type="button" class="btn btn-warning div-dateSusTermInsert" onclick="funcInsertSusTermRecord(0)">Cancel</button>
                    <button type="button" class="btn btn-secondary div-dateSusTerm" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="subjectExplainModal" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" aria-hidden="true" data-pw="" data-irid="" data-recommenlvl="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="m-form m-form--fit m-form--label-align-right form-horizontal">
                <div class="modal-header" style="padding: 18px !important;">
                    <span>
                        <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                    </span>
                    <h5 class="modal-title" id="titleModalMJM">Incident Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -0.5rem -1rem -1rem auto !important;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <!--<div class="row">
                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-3">
                                    <img id="empPicSE" src="http://3.18.221.50/sz/assets/images/img/sz.png" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block">
                                </div>
                                <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="empNameSE">Jamelyn A. Sencio </div>
                                        <div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="empJobSE">Customer Care Ambassador 1</div>
                                        <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="empAccountSE">ONYX Enterprises</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>-->
                    <div class="row ir_details_template" id="layoutIrDetails"> ** Michael's code **
                        <!-- <div class="col-12">
                            <div class="row pl-4 pr-4" id="layoutIrDetails"> ** Michael's code ** </div>
                        </div> -->
                    </div>
                    <div id="divSubjExplain">

                        <form id="defaultForm" method="post">
                            <div>
                                <div class="row p-3">
                                    <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Action</span>
                                    <div class="col">
                                        <hr style="margin-top: 0.8rem !important;">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group pb-2 text-center" style="border-style: dashed;border-width: 2px;border-color: #e4e4e4;margin-right: 53px;margin-left: 53px;">
                                <label>Upload Soft Copy of Written Explanation (<i>Optional</i>): <span id="NORreq" class="text-danger"></span></label> <br>
                                <small>Note: File must be in a PDF format with maximum of 5MB file size.</small> <br><br>
                                <label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent"><i class="fa fa-paperclip"></i> Upload File
                                    <input type="file" style="display: none;" name="upload_file_explain" id="upload_file_explain">
                                </label>
                                <br>
                                <small id="NORname" style="margin-left: 15px;" data-filesize="" data-filetype=""></small>

                            </div>
                            <div class="form-group m-form__group mx-4">
                                <label for="exampleTextarea">Explanation</label>
                                <textarea class="form-control m-input m-input--solid" id="explainTxtArea" name="explainTxtArea" rows="3"></textarea>
                                <small class="m--font-danger" id="txtOutput"></small>
                            </div>

                        </form>
                    </div>

                    <div class="row div-explain">
                        <div class="col-12">
                            <div class="row p-3">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Recommendation</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 div-explain">
                        <div class="row pl-5 pr-5 mb-5" id="divRecommenaders">
                        </div>
                    </div>
                    <div class="col-12 p-3 div-explain text-center" id="divClientRecAction">
                        <hr>
                        <button type="button" class="btn btn-info" onclick="approveClientRec(1)">Approve</button>
                        <button type="button" class="btn btn-danger" onclick="approveClientRec(0)">Disapprove</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>

                    <div class="divClientRec div-explain div-involve">
                        <div class="col-12 mb-2">
                            <div class="row">
                                <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i class="la la-tag" style="font-size: 1.1rem;"></i> Action</span>
                                <div class="col">
                                    <hr style="margin-top: 0.8rem !important;">
                                </div>
                            </div>
                        </div>
                        <div class="px-3" id="divRecommend">
                            <div class="col-12">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                        <label for="exampleTextarea">Type</label>
                                        <select class="form-control m-select2" id="recType">

                                        </select>
                                        <small class="m--font-danger" id="txtImportant"></small>
                                    </div>
                                    <div class="col-lg-4" id="divDiscAct" style="display:none">
                                        <label for="exampleTextarea">Disciplinary Action</label>
                                        <select class="form-control m-select2" id="recDisAct" onchange="recDisActChange()"></select>

                                    </div>
                                    <div class="col-lg-4 divDateDays" id="divDays" style="display:none">
                                        <label for="exampleTextarea">Days</label>
                                        <input id="reqDays" type="text" class="form-control bootstrap-touchspin-vertical-btn" value="1" type="text" readonly>

                                    </div>
                                    <div class="col-lg-4 divDateDays" id="divDate" style="display:none">
                                        <label for="exampleTextarea">Date</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control m-input" readonly placeholder="Select date" id="reqDate" value="<?php echo date("Y-m-d"); ?>" />
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div id="divDisapproveClient">
                                <form id="disapproveClientForm" method="post">
                                    <div class="col-12">
                                        <div class="form-group m-form__group pb-2 pt-3 text-center" style="border-style: dashed;border-width: 2px;border-color: #e4e4e4;margin-right: 28px;margin-left: 28px;">
                                            <label>Upload Screenshot (Required): <span id="NORreq" class="text-danger"></span></label> <br>
                                            <small>Note: File must be an image format(PNG/JPG) with maximum of 5MB file size.</small> <br><br>
                                            <label class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent"><i class="fa fa-paperclip"></i> Upload File
                                                <input type="file" style="display: none;" name="upload_file_client" id="upload_file_client">
                                            </label>
                                            <br>
                                            <small id="Clientname" style="margin-left: 15px;" data-filesize="" data-filetype=""></small>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <br>
                            <div class="col-12">
                                <div class="form-group m-form__group">
                                    <label for="exampleTextarea">Comment</label>
                                    <textarea class="form-control m-input m-input--solid" id="noteArea" rows="3"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer divClientRecBtn div-involve">
                    <button type="button" class="btn btn-danger" id="btnDismissIR" data-dissmissid="">Skip</button>
                    <button type="button" class="btn btn-primary" id="btnSubmitIR">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end: MJM:Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/chartist/dist/chartist.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/src/custom/js/sha1.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/cod_settings.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_ir_profile.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_dtrviolation.js"></script>
<!-- <script src="<?php echo base_url(); ?>node_modules/lightbox2/dist/js/lightbox.js"></script> -->
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_supervision_cha.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>

<script type="text/javascript">
    // charise start code
    // charise end code
    // MJM start code

    function setTabActive(act) {
        if (act == 1) {
            $("#irSup_explanation").click();
        }
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('discipline/getNumTabs'); ?>",
            cache: false,
            success: function(output) {
                var result = JSON.parse(output);

                var explain = parseInt(result["explain"][0].cnt);
                if (explain > 0) {
                    $("#badgeExplain").css("display", "block");
                } else {
                    $("#badgeExplain").css("display", "none");
                }
                var recommendation = parseInt(result["recommendation"][0].cnt);
                if (recommendation > 0) {
                    $("#badgeRecommend").css("display", "block");
                } else {
                    $("#badgeRecommend").css("display", "none");
                }
                var client = parseInt(result["client"][0].cnt);
                if (client > 0) {
                    $("#badgeClient").css("display", "block");
                } else {
                    $("#badgeClient").css("display", "none");
                }
                var effectivity = parseInt(result["effectivity"][0].cnt);
                if (effectivity > 0) {
                    $("#badgeEffective").css("display", "block");
                } else {
                    $("#badgeEffective").css("display", "none");
                }
                var total = explain + recommendation + client + effectivity;
                $("#supervisionBadge").html(total);
            }
        });
    }

    function approveClientRec(act) {
        if (act == 1) {
            swal({
                title: "Approve?",
                text: "Are you sure you want to approve?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes',
                closeOnConfirm: false,
                closeOnCancel: false,
                allowOutsideClick: false

            }).then((result) => {
                if (result.value) {
                    saveIRrecommendClient(act);
                } else {
                    //alert("Ambbot");
                }
            })
        } else {
            $("#divClientRecAction").hide();
            $(".divClientRec").show();
            $(".divClientRecBtn").show();
            $("#divDisapproveClient").show();

        }
    }

    function funcInsertSusTermRecord(typ) {
        submitEffectiveDate(1);
        if (typ == 1) {
            $(".div-dateSusTermInsert").css("display", "block");
            $(".div-dateSusTerm").css("display", "none");
        } else {
            $(".div-dateSusTermInsert").css("display", "none");
            $(".div-dateSusTerm").css("display", "block");
        }

    }

    function initSetEffectiveDate(incidentReport_ID) {
        $('#btnSubmitEfffective').attr("disabled", true);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('discipline/getTermSusDetails'); ?>",
            data: {
                irid: incidentReport_ID
            },
            cache: false,
            success: function(output) {
                var result = JSON.parse(output);
                var typ = (result["ir_details"].label == 's') ? "Suspension" : "Termination";
                $("#setEffectiveDateModal").data("irid", incidentReport_ID);
                $("#setEffectiveDateModal").data("lbl", typ);
                $("#setEffectiveDateModal").data("days", result["ir_details"].days);
                $("#setEffectiveDateModal").data("stid", result["ir_details"].suspensionTerminationDays_ID);

                $("#noteSusTerm").html("<span>No. of " + typ + " Days: <b> " + result["ir_days"].length +
                    " / " + result["ir_details"].days + "</b></span>");
                $(".div-dateSusTermInsert").css("display", "none");
                if (parseInt(result["ir_days"].length) === parseInt(result["ir_details"].days)) {
                    $("#btnInsertSusTerm").css("display", "none");
                } else {
                    $("#btnInsertSusTerm").css("display", "block");
                }
                if (result["ir_days"].length > 0) {
                    var txt = "";
                    $.each(result["ir_days"], function(key, data) {
                        var now = new Date(result["now"]);
                        var susDate = new Date(data.date);
                        console.log(now.getTime() + " " + susDate.getTime());
                        var btn = (susDate > now) ?
                            "<span class='btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air pull-center' onclick=checkSusTerm(" +
                            data.suspensionDates_ID + "," + incidentReport_ID + ")><i class='fa fa-trash'></i></span>" :
                            '<a href="#" class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-check"></i></a>';
                        txt += "<tr><td style='padding: 5px;'>" + moment(data.date).format(
                            "MMMM D, YYYY") + "</td><td>" + btn + "</td></tr>";
                    });
                } else {
                    var txt =
                        "<tr><td colspan=2 style='text-align: center;' class='pb-3'> <i class='fa fa-paperclip' style='color:crimson;font-size:unset'></i>  No Records Found</td></tr>";

                }
                $("#dateShow").html(txt);
                if (result["ir_details"].label == 's') {
                    $("#setEffectiveDateModal").modal();
                } else {
                    var date_list = $('#susTermDatepicker').datepicker('setDate', new Date(moment(result[
                        "ir_days"][0].date)));
                    $("#setTermEffectiveDateModal").data("date", moment(result["ir_days"][0].date).format(
                        'YYYY-MM-DD'));
                    $("#setTermEffectiveDateModal").data("origdate", moment(result["ir_days"][0].date).format(
                        'YYYY-MM-DD'));
                    $("#setTermEffectiveDateModal").data("termid", result["ir_days"][0].suspensionDates_ID);
                    $("#termNote").html(
                        "<b>Note:</b> The approved <b class='text-danger'>Termination</b> date during Final Recommendation is " +
                        moment(result["ir_days"][0].date).format('LL') + ".");

                    $("#setTermEffectiveDateModal").modal();
                }

            }
        });
    }

    function checkSusTerm(id, incidentReport_ID) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('discipline/getIrSusTermDetails'); ?>",
            data: {
                id: id,
            },
            cache: false,
            success: function(output) {
                if (output > 0) {
                    initSetEffectiveDate(incidentReport_ID);
                } else {
                    alert("Error");
                }
            }
        });
    }

    function checkWitnessConfirmation(id, incidentReport_ID, typ, lvl, disciplineCategory_ID, disciplinaryActionCategorySettings_ID, disciplinaryActionCategory_ID) {
        $.when(fetchPostData({
            irId: incidentReport_ID,
        }, '/discipline/check_if_pending_witness_confirmation_exist')).then(function(witness) {
            var witnessObj = $.parseJSON(witness.trim());
            if (parseInt(witnessObj.can_explain)) {
                initSubjExplain1(id, incidentReport_ID, typ, lvl, disciplineCategory_ID, disciplinaryActionCategorySettings_ID, disciplinaryActionCategory_ID);
            } else {
                swal("Ongoing Witness Confirmation", "Sorry, you still have to wait for the witness/es confirmation to finish before you can explain.", "info");
            }
        });
    }

    function initSubjExplain(id, incidentReport_ID, typ, lvl, disciplineCategory_ID, disciplinaryActionCategorySettings_ID, disciplinaryActionCategory_ID) {
        if (typ == 1) {
            swal({
                title: 'Please type your password to proceed.',
                input: 'password',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                allowOutsideClick: false,
                preConfirm: (login) => {
                    // alert(login)
                },
                inputValidator: (value) => {
                    if (!value) {
                        return 'You need to write something!'
                    }
                },
                allowOutsideClick: () => !swal.isLoading()
            }).then((result) => {
                var pw = CryptoJS.SHA1(result.value);
                var pw2 = '<?php echo $_SESSION["pw"]; ?>';
                if (pw == pw2) {

                    if (pw == pw2) {

                        checkWitnessConfirmation(id, incidentReport_ID, typ, lvl, disciplineCategory_ID, disciplinaryActionCategorySettings_ID, disciplinaryActionCategory_ID);
                    }


                } else {
                    swal("Password Error", 'It seems you have typed a wrong password.', 'error');
                }
            });
        } else {
            checkWitnessConfirmation(id, incidentReport_ID, typ, lvl, disciplineCategory_ID, disciplinaryActionCategorySettings_ID, disciplinaryActionCategory_ID);
        }
    }

    function initSubjExplain1(id, incidentReport_ID, typ, lvl, disciplineCategory_ID, disciplinaryActionCategorySettings_ID, disciplinaryActionCategory_ID) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>discipline/empDetailsSE",
            data: {
                id: id,
                typ: typ,
                lvl: lvl,
                disciplineCategory_ID: disciplineCategory_ID,
                disciplinaryActionCategorySettings_ID: disciplinaryActionCategorySettings_ID,
                incidentReport_ID: incidentReport_ID,
                disciplinaryActionCategory_ID: disciplinaryActionCategory_ID
            },
            cache: false,
            success: function(res) {

                var result = JSON.parse(res);
                $("#divDiscAct").css("display", "none");
                $("#divDisapproveClient").css("display", "none");
                if (typ == 1) {
                    $(".div-explain").css("display", "none");
                    $(".divClientRecBtn").show();
                    $("#upload_file_explain").val("");
                } else {
                    $(".div-explain").css("display", "block");
                }
                // if (parseInt(result["can_explain"])) {
                //     $('#btnDismissIR').show();
                //     $('#btnSubmitIR').show();
                // } else {
                //     $('#btnDismissIR').hide();
                //     $('#btnSubmitIR').hide();
                // }
                if (typ == "1") {
                    $("#divSubjExplain").show();
                    $("#divRecommend").hide();
                    $("#btnSubmitIR").attr("onclick", "saveExplain(1)");

                } else if (typ == 2 || typ == 3 || typ == 7) {
                    $("#divSubjExplain").hide();
                    $("#divRecommend").show();
                    $("#btnSubmitIR").attr("onclick", "saveRecommend('" + typ + "')");
                    str = "";
                    if ((result["discipline_cat"]).length != 0) {
                        if ((result["discipline_cat"]).length <= 1) {
                            str += '<option value=' + result["discipline_cat"][0]
                                .disciplinaryActionCategory_ID + ' selected  data-lbl="' + result[
                                    "discipline_cat"][0].label + '">' + result["discipline_cat"][0].action +
                                '</option>';
                        } else {
                            var ctr = 0;
                            $.each(result["discipline_cat"], function(key, data) {
                                var selected = (ctr == 0) ? "selected" : "";
                                str += "<option value=" + data.disciplinaryActionCategory_ID + " " +
                                    selected + " data-lbl='" + data.label + "'>" + data.action +
                                    "</option>";
                                ctr++;
                            });

                        }
                    }
                    str2 = '<option value=0 selected disabled>--</option>';
                    str2 += '<option value=17>Liable</option>';
                    str2 += '<option value=18>Non-Liable</option>';



                    $("#recType").html(str2);
                    $("#recDisAct").html(str);
                    $("#recDisAct").trigger("changed");
                    var rec = "";

                    if (typ == 2 || typ == 3 || typ == 7) {

                        var i = 1;

                        if (result["discipline_recommenders"].length > 0) {
                            $.each(result["discipline_recommenders"], function(key, data) {
                                if (data.incidentReportRecommendationType_ID == 2) {
                                    var r = "Last Recommendation";
                                    var b = "danger ";
                                    var ii = "";
                                } else if (data.incidentReportRecommendationType_ID == 3) {
                                    var r = "Final Recommendation";
                                    var b = "info";
                                    var ii = "";
                                } else if (data.incidentReportRecommendationType_ID == 4) {
                                    var r = "Client Recommendation";
                                    var b = "primary";
                                    var ii = "";



                                } else {
                                    var r = "Recommendation ";
                                    var b = "warning";
                                    var ii = "#" + i++;

                                }

                                if (data.liabilityStat_ID == "17") {
                                    var ifLiable = data.action + ' (Liable ) ';


                                } else if (data.liabilityStat_ID == "18") {
                                    var ifLiable = data.action + ' (Non-Liable ) ';

                                } else {
                                    var ifLiable = "Missed to recommend";
                                }
                                rec += '<div class="col-12">'
                                rec += '<div class="row p-3">'
                                rec += '<span class="m-menu__link-badge"><span class="m-badge m-badge--' + b + ' pl-2 pr-2">' + ii + ' ' + r + ' </span></span>'
                                rec += '<div class="col">'
                                rec +='<div style="border-bottom: 1px dashed #e6e6e6 !important;padding-bottom: 0 !important;margin-top: 10px;"></div>'
                                rec += '</div>'
                                rec += '</div>'
                                rec += '</div>'
                                rec += '<div class="col-12 col-md-4 font-weight-bold">Name:</div>'
                                rec +='<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedLevel">' +data.fname + " " + data.lname + '</div>'
                                rec += '<div class="col-12 col-md-4 font-weight-bold">Category:</div>'
                                rec +='<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"> Category ' + data.category.toUpperCase()+ '</div>';
                                rec +='<div class="col-12 col-md-4 font-weight-bold">Disciplinary Action:</div>'
                                rec +='<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 17px; font-weight: 500" id="qualifiedDisciplinaryAction">' + ifLiable + '</div>'
                                if (data.susTermDetails != null && data.liabilityStat_ID == 17) {
                                    if ((data.susTermDetails).includes("-")) {
                                        var lab = "Date";
                                    } else {
                                        var lab = "# of Day(s)";
                                    }

                                    rec += '<div class="col-12 col-md-4 font-weight-bold">' + lab +
                                        ':</div>'
                                    rec +=
                                        '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400">' +
                                        data.susTermDetails + '</div>';
                                }
                                if (data.liabilityStat_ID != "12") {
                                    let noteRec = "No Notes Found"
                                    if (data.notes != "" && data.notes != null) {
                                        noteRec = data.notes;
                                    }
                                    rec += '<div class="col-12 col-md-4 font-weight-bold">Note:</div>'
                                    rec += '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400" id="qualifiedPeriod">' +
                                        noteRec + '</div>';
                                }
                                if (data.incidentReportRecommendationType_ID == 4 && result["client_attach"].length > 0) {
                                    rec += '<div class="col-12 col-md-4 font-weight-bold">Screenshot:</div>'
                                    var aa = "<?php echo base_url(); ?>" + result["client_attach"][0].link;
                                    rec += '<div class="col-12 col-md-8 pl-5 pl-md-0" style="font-size: 13px; font-weight: 400"><a href="' + aa + '" target="_blank"><i class="fa fa-search"></i> View</a></div>';

                                }
                            });
                        } else {
                            rec +=
                                "<span colspan=2 style='padding: 20px;margin: 0 auto;'> <i class='fa fa-paperclip' style='color:crimson;font-size:unset'></i> No Records Found</span>";
                        }

                    }
                    if (typ == 2 || typ == 3) {
                        $("#subjectExplainModal").data("recommenlvl", result["currentLevel"].level);
                    }
                    $("#divRecommenaders").html(rec);
                    if (typ == 3 || typ == 7) {
                        if (typ == 7) {
                            $("#divClientRecAction").hide();

                        } else {
                            $("#divClientRecAction").show();
                        }
                        $(".divClientRec").hide();
                        $(".divClientRecBtn").hide();


                    } else {
                        $("#divClientRecAction").hide();
                        $(".divClientRec").show();
                        $(".divClientRecBtn").show();
                    }
                } else {
                    $("#divSubjExplain").hide();
                    $("#divRecommend").hide();
                }
                if (typ == "1") {
                    $("#btnDismissIR").attr("onclick", "saveExplain(0)");
                    if (parseInt(result["check_dismiss_deadline"])) {
                        // if(parseInt(result["can_explain"])){
                        $("#btnDismissIR").css("display", "block");
                        // }
                    } else {
                        $("#btnDismissIR").css("display", "none");
                    }
                } else {
                    $("#btnDismissIR").css("display", "none");
                }
                var titleModal = "Incident Report Details";
                if (typ == 1) {
                    titleModal = "Incident Report for Subject Explanation";
                } else if (typ == 2) {
                    titleModal = "Incident Report for Recommendation";
                } else if (typ == 3) {
                    titleModal = "Incident Report for client Recommendation";
                }
                $("#titleModalMJM").text(titleModal);
                
				 if (typ == 1) {
                     $("#subjectExplainModal").data("pw", result["emp_details_subject"].pw);
                     $("#subjectExplainModal").data("pwSup", result["emp_details"].pw); 
				 }else{
					$("#subjectExplainModal").data("pw", result["emp_details"].pw); 
				 }
                 
                $("#subjectExplainModal").data("irid", incidentReport_ID);

                $("#explainTxtArea").val("");
                $("#layoutIrDetails").html(result.layout["ir_details_layout"]);
                $("#noteArea").val("");
                $("#txtOutput").html("");
                $(".divDateDays").css("display", "none");
                $("#subjectExplainModal").modal();
            }
        });
    }

    function changeGetSched(e) {
        var rs = $("#datepickSchedList option:selected").data("schedtypeid");
        if (rs == 12) {
            $("#btnSubmitEfffective").attr("disabled", true);
        } else {
            $("#btnSubmitEfffective").attr("disabled", false);
        }
    }

    function submitEffectiveTermDate() {
        var date_list = moment($('#susTermDatepicker').datepicker('getDate')).format('YYYY-MM-DD');
        var date_list_format = moment($('#susTermDatepicker').datepicker('getDate')).format('LL');
        var previousdate = $("#setTermEffectiveDateModal").data("date");
        var termid = $("#setTermEffectiveDateModal").data("termid");

        if (date_list != previousdate) {
            $("#setTermEffectiveDateModal").data("date", moment($('#susTermDatepicker').datepicker('getDate')).format(
                'YYYY-MM-DD'));
            swal({
                title: 'Are you sure?',
                text: date_list_format + " will be set Final as Termination date.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes'
            }).then(function(result) {
                if (result.value) {
                    //alert(date_list);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('discipline/saveTermDates'); ?>",
                        data: {
                            termid: termid,
                            dated: date_list,
                        },
                        cache: false,
                        success: function(output) {
                            if (output > 0) {
                                $("#setTermEffectiveDateModal").modal("hide");
                            }

                        }
                    });
                }
            });
        }

    }

    function submitEffectiveDate(e) {

        var date_list = $('#susTermDpicker').val();
        var irid = $("#setEffectiveDateModal").data("irid");
        var max = $("#setEffectiveDateModal").data("days");
        var typ = $("#setEffectiveDateModal").data("lbl");

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('discipline/getSchedforDMS'); ?>",
            data: {
                irid: irid,
                dates: date_list
            },
            cache: false,
            success: function(output) {
                //datepickList
                var result = JSON.parse(output);
                var txt = "";

                if (result["sched"].length > 0) {
                    var ctr = 0;
                    $.each(result["sched"], function(key, data) {
                        var selected = (ctr == 0) ? "selected" : "";
                        if (data.acc_time_id != null && data.schedtype_id != "12") {
                            txt += "<option value=" + data.sched_id + " data-date='" + data.sched_date +
                                "' " + selected + ">" + data.time_start + " - " + data.time_end +
                                "</option>";
                        } else {
                            txt += "<option value=" + data.sched_id + " data-schedtypeid=" + data
                                .schedtype_id + " data-date='" + data.sched_date + "' " + selected +
                                ">" + data.type + "</option>";
                        }
                    });
                } else {
                    txt += "<option value='0' data-date='" + date_list +
                        "' data-schedtypeid='0' selected>No Schedule</option>";
                }


                $("#datepickSchedList").html(txt);
                $("#datepickSchedList").trigger("change");
            }
        });
        if (e == 2) { // Suspension
            var stid = $("#setEffectiveDateModal").data("stid");
            var arr_dates = [];
            var sched = $("#datepickSchedList").val();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('discipline/saveSusTermDates'); ?>",
                data: {
                    irid: irid,
                    dates: date_list,
                    sched: sched,
                    stid: stid,
                },
                cache: false,
                success: function(output) {
                    if (output > 0) {
                        initSetEffectiveDate(irid);
                        $(".div-dateSusTermInsert").css("display", "none");
                        $(".div-dateSusTerm").css("display", "block");
                    }

                }
            });

        }
    }
    $(function() {
        setTabActive(0);
        $("#susTermDpicker").change(function() {
            submitEffectiveDate(1);
        });
        $('#susTermDatepicker').datepicker({
            format: 'yyyy-mm-dd',
            startDate: '+2d'
        });
        $('#setTermEffectiveDateModal').on("show.bs.modal", function(e) {

            $("#susTermDatepicker .datepicker-days").on('click', 'td.day', function() {

                setTimeout(function() {
                    submitEffectiveTermDate();
                }, 200);
            });

        });
        $('#recType,#recDisAct').select2({
            placeholder: "Select Type of Recommendation.",
            width: '100%'
        });
        $('.susTermSelect').select2({
            placeholder: "Select Type of Recommendation.",
            width: '100%'
        });
        $('#recType').change(function() {
            var ty = $(this).val();
            $("#txtImportant").html("");
            if (ty == "17") {
                $("#divDiscAct").css("display", "block");
                recDisActChange();
            } else {
                $("#divDiscAct").css("display", "none");
                $(".divDateDays").css("display", "none");
            }

        });
        $("#explainTxtArea").on("keyup", function() {
            var txt = $(this).val();
            if (txt.length > 10) {
                $("#txtOutput").html("");
            } else {
                $("#txtOutput").html("<b>Note:</b> Required of minimum 10 characters.");
            }
        });
        $("#reqDays").TouchSpin({
            min: 1,
            max: 50,
            postfix: 'Days'

        });
        $('#reqDate').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });


    });

    function recDisActChange() {
        var data2 = $('#recDisAct option:selected').attr('data-lbl');
        var recommenlvl = $("#subjectExplainModal").data("recommenlvl");
        //alert(recommenlvl);
        if (recommenlvl != 1) {
            if (data2 == 't') {
                $(".divDateDays").css("display", "block");
                $("#divDate").css("display", "block");
                $("#divDays").css("display", "none");

            } else if (data2 == 's') {
                $(".divDateDays").css("display", "block");
                $("#divDays").css("display", "block");
                $("#divDate").css("display", "none");
            } else {
                $(".divDateDays").css("display", "none");
            }
        } else {
            $(".divDateDays").css("display", "none");
        }



    }

    function saveIRrecommendClient(act) {
        var irid = $("#subjectExplainModal").data("irid");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('discipline/submitRecommendClient'); ?>",
            data: {
                irid: irid
            },
            cache: false,
            success: function(output) {
                if (output > 0) {
                    $("#subjectExplainModal").modal("hide");
                    subjExplainFunc(3);
                } else {

                }

            }
        });
    }

    function saveRecommend(typ) {
        var rectype = $("#recType").val();
        var recDisAct = $("#recDisAct option:selected").val();
        var disAct = (rectype == 18) ? 0 : recDisAct;

        if (rectype == null || rectype == "0") {
            $("#txtImportant").html("<b> This field is required.</b> ");
        } else {
            if (typ == "3") { // If Client Disapprove + Need Screenshot
                $("#txtImportant").html("");

                var size = $("#Clientname").data("filesize");
                var filetype = $("#Clientname").data("filetype");

                var ifHasFile = $("#upload_file_client").val().length;
                if (ifHasFile > 0) {
                    if (filetype === "image/png" || filetype === "image/jpg" || filetype === "image/jpeg") {
                        if (size < 5000000) {
                            saveIRrecommend(rectype, disAct, typ);

                        } else {
                            toastr.error("NOTE: File exceeded 5MB.");
                        }
                    } else {
                        toastr.error("NOTE: File must be an image (screenshot).");
                    }
                } else {
                    toastr.error("NOTE: Need a screenshot of the email from the client.");
                }


            } else {
                $("#txtImportant").html("");
                saveIRrecommend(rectype, disAct, typ);
            }
        }

    }


    function saveIRrecommend(liabilityStat_ID, disciplinaryActionCategory_ID, typ) {
        var pw = $("#subjectExplainModal").data("pw");
        var irid = $("#subjectExplainModal").data("irid");
        //alert(irid+" ***  "+liabilityStat_ID+" ***  "+disciplinaryActionCategory_ID+" ***  "+typ);
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {

            // console.log(result.value);
            // console.log(pw);

            if (result.value == pw) {
                submitIRrecommend(irid, liabilityStat_ID, disciplinaryActionCategory_ID, typ);


            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });
    }

    function submitIRrecommend(irid, liabilityStat_ID, disciplinaryActionCategory_ID, typ) {
        // console.log(irid + " ***  " + liabilityStat_ID + " ***  " + disciplinaryActionCategory_ID + " ***  " + typ);
        var txt = $("#noteArea").val().trim();
        var recDisAct = $('#recDisAct option:selected').attr('data-lbl');
        var recommenlvl = $("#subjectExplainModal").data("recommenlvl");
        var reqNumDays = $("#reqDays").val();
        var reqDate = $("#reqDate").val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>discipline/submitRecommend/" + irid,
            data: {
                txt: txt,
                liabilityStat_ID: liabilityStat_ID,
                disciplinaryActionCategory_ID: disciplinaryActionCategory_ID,
                recDisAct: recDisAct,
                recommenlvl: recommenlvl,
                typ: typ,
                reqDate: reqDate,
                reqNumDays: reqNumDays
            },
            cache: false,
            success: function(res) {
                if (parseInt(res.trim()) == 1) {
                    $("#subjectExplainModal").modal("hide");
                    if (typ == "3") {
                        $("#disapproveClientForm").submit();
                    }
                    subjExplainFunc(typ);
                } else {
                    swal("Error", 'Something went wrong. Please contact the administrator.', 'error');
                }
            }
        });

    }

    function saveExplainwithPass(pw) {
        var size = $("#NORname").data("filesize");
        var filetype = $("#NORname").data("filetype");

        var ifHasFile = $("#upload_file_explain").val().length;
        if (ifHasFile > 0) {
            if (filetype === "application/pdf") {
                if (size < 5000000) {
                    saveExplainCheck(pw);
                } else {
                    toastr.error("NOTE: File must be in a PDF format with maximum of 5MB file size");
                }
            } else {
                toastr.error("NOTE: File must be PDF.");
            }
        } else {
            $("#NORname").html("");
            saveExplainCheck(pw);
        }
    }

    function saveExplainCheck(pw) {
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            if (result.value == pw) {
                $("#defaultForm").submit();
            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });

    }

    function saveExplain(ifDismiss) {
        var txt = $("#explainTxtArea").val().trim();
        var pw = $("#subjectExplainModal").data("pw");
        $("#btnDismissIR").data("dissmissid", ifDismiss);
        if (ifDismiss == 1) {
            if (txt.length > 10) {
                $("#txtOutput").html("");
                saveExplainwithPass(pw);
            } else {
                $("#txtOutput").html("<b>Note:</b> Required of minimum 10 characters.");
            }
        } else {
            var pw = $("#subjectExplainModal").data("pwSup");
            saveExplainwithPass(pw);
        }
    }
    $("#disapproveClientForm").on('submit', function(e) {
        e.preventDefault();

        var irid = $("#subjectExplainModal").data("irid");
        var data = new FormData(this);
        data.append("txt", $("#explainTxtArea").val());
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>discipline/saveScreentShotClient/" + irid,
            data: data,
            processData: false,
            contentType: false,
            success: function(res) {


            }
        });
    });
    $("#defaultForm").on('submit', function(e) {
        e.preventDefault();

        var dissmissid = $("#btnDismissIR").data("dissmissid");
        var irid = $("#subjectExplainModal").data("irid");
        var data = new FormData(this);
        data.append("txt", $("#explainTxtArea").val());
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>discipline/submitExplain/" + irid + "/" + dissmissid,
            data: data,
            processData: false,
            contentType: false,
            success: function(res) {
                if(parseInt(res.trim()) == 1) {
                    $("#subjectExplainModal").modal("hide");
                    subjExplainFunc(1);
                } else {
                    swal("Error", 'Something went wrong. Please contact the administrator.', 'error');
                }

            }
        });
    });
    $("#upload_file_client").bind('change', function(e) {
        var file = $(this).val();
        var size = this.files[0].size;
        var typ = this.files[0].type;
        // alert(typ);
        file = (file.length > 0) ? file : "fakepath";
        var name = file.split('fakepath');
        var filen4me = "";
        $("#Clientname").data("filetype", typ);
        if (size > 5000000) {
            filen4me = (file.length > 0) ? "<strong class='text-danger'>NOTE: File must not exceed to 5MB.</strong>" : "";
            $("#Clientname").data("filesize", "error");
        } else {
            filen4me = (file.length > 0) ? name[1].slice(1) : "";

            $("#Clientname").data("filesize", size);
        }
        $("#Clientname").html(filen4me);
    });
    $("#upload_file_explain").bind('change', function(e) {
        var file = $(this).val();
        var size = this.files[0].size;
        var typ = this.files[0].type;
        // alert(typ);
        file = (file.length > 0) ? file : "fakepath";
        var name = file.split('fakepath');
        var filen4me = "";
        $("#NORname").data("filetype", typ);
        if (size > 5000000) {
            filen4me = (file.length > 0) ? "<strong class='text-danger'>NOTE: File must not exceed to 5MB.</strong>" : "";
            $("#NORname").data("filesize", "error");
        } else {
            filen4me = (file.length > 0) ? name[1].slice(1) : "";

            $("#NORname").data("filesize", size);
        }
        $("#NORname").html(filen4me);
    });



    function submitExplain(irid) {
        var txt = $("#explainTxtArea").val().trim();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>discipline/submitExplain/" + irid,
            data: {
                txt: txt
            },
            cache: false,
            success: function(res) {
                if(parseInt(res.trim()) == 1) {
                    $("#subjectExplainModal").modal("hide");
                    subjExplainFunc(1);
                } else {
                    swal("Error", 'Something went wrong. Please contact the administrator.', 'error');
                }
            }
        });
    }

    function subjExplainFunc(typ, start = null, end = null) {
        $('.irSupervisionFilter').hide();
        if (typ != 1) {
            if (typ == 1) {
                var tbl = "#ongoingPulsesTable";
            } else if (typ == 2) {
                var tbl = "#ongoingPulsesTableRecommend";
            } else if (typ == 3) {
                var tbl = "#ongoingPulsesTableClient";
            } else if (typ == 4) {
                var tbl = "#ongoingPulsesTableEffective";
            } else if (typ == 7) {
                var tbl = "#ongoingPulsesTableInvolve";
            }
            $(tbl).mDatatable('destroy');
            $(tbl).html('<div class="col-12 text-center"><i style="font-size: 42px;margin-right:1rem" class="fa fa-lock"></i><span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">Authorized Access Only</span></div>');
            swal({
                title: 'Please type your password to proceed.',
                input: 'password',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                closeOnConfirm: false,
                closeOnCancel: true,
                allowOutsideClick: false,
                preConfirm: (login) => {
                    // alert(login)
                },
                inputValidator: (value) => {
                    if (!value) {
                        return 'You need to write something!'
                    }
                },
                allowOutsideClick: () => !swal.isLoading()
            }).then((result) => {
                var pw = CryptoJS.SHA1(result.value);
                var pw2 = '<?php echo $_SESSION["pw"]; ?>';
                if (pw == pw2) {
                    if (pw == pw2) {
                        $(tbl).html("");
                        subjExplainFunc1(typ, start = null, end = null);
                        $('.irSupervisionFilter').fadeIn();
                    }

                } else {
                    swal("Password Error", 'It seems you have typed a wrong password.', 'error');
                }
            });

        } else {
            subjExplainFunc1(typ, start = null, end = null);
        }

    }

    function subjExplainFunc1(typ, start = null, end = null) {
        setTabActive(0);
        if (typ == 1) {
            var tbl = "#ongoingPulsesTable";
        } else if (typ == 2) {
            var tbl = "#ongoingPulsesTableRecommend";
        } else if (typ == 3) {
            var tbl = "#ongoingPulsesTableClient";
        } else if (typ == 4) {
            var tbl = "#ongoingPulsesTableEffective";
        } else if (typ == 7) {
            var tbl = "#ongoingPulsesTableInvolve";
            if (start == null) {
                var det = ($("#myIrs_incidentDate").val()).split(" - ");
                var start = moment(det[0]).format('YYYY-MM-DD');
                var end = moment(det[1]).format('YYYY-MM-DD');
            }
        } else {
        }
        $(tbl).mDatatable('destroy');

        var choice = $("#ongoingStatus").val();
        var involveType = $("#irInvolveType").val();
        $('#irInvolveType').change(function() {
            var start = ($("#myIrs_incidentDate").val()).split(" - ");
            subjExplainFunc(7, moment(start[0]).format('YYYY-MM-DD'), moment(start[1]).format('YYYY-MM-DD'));

        });
        $('#irInvolveType').select2({
            placeholder: "Select Type",
            width: '100%'
        });
        $('#susTermDpicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            startDate: '+1d',
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        var empid = $("#empSelectIntervene").val();
        empid = (empid != null) ? empid : 0;

        var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/discipline/getAllSubordinates/' + typ + '/' + involveType,
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
                        map: function(raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },

                        params: {
                            query: {
                                start: start,
                                end: end,
                            },
                        },
                    }
                },
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            search: {
                input: $('#ongoingSearch' + typ),
            },
            toolbar: {
                // toolbar placement can be at top or bottom or both top and bottom repeated
                placement: ['bottom'],
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [5, 10, 20, 30, 50, 100],
                    }
                }
            },
            columns: [{
                field: "incidentReport_ID",
                title: "IR ID",
                width: 90,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function(data) {
                    var incidentReport_ID = (data.incidentReport_ID).padLeft(8);
                    var html = "<div class = 'col-md-12'>" + incidentReport_ID + "</div>";
                    return html;
                }
            }, {
                field: "incidentDate",
                title: "Incident Date",
                width: 180,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {

                    var html = "<div class = 'col-md-12'>" + moment(data.incidentDate).format('LL'); +
                    "</div>";
                    return html;
                }
            }, {
                field: "lname",
                title: (typ == 1) ? "Offense" : "Subject",
                width: 300,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                    var info = (typ == 1) ? data.offense : data.lname + ", " + data.fname;
                    var html = "<div class = 'col-md-12'>" + info + "</div>";
                    return html;
                }
            }, {
                field: "final",
                title: "Disciplinary Action",
                width: 150,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                    var html = "<div class = 'col-md-12'>" + data.action + "</div>";


                    return html;
                }
            }, {
                field: "",
                title: "Action",
                selector: false,
                sortable: false,
                textAlign: 'center',
                overflow: 'invisible',
                width: 80,
                template: function(data) {

                    var lvl = (typ == 2 || typ == 3) ? data.level : 0;
                    var disciplineCategory_ID = (typ == 2 || typ == 3) ? data.disciplineCategory_ID : 0;
                    var disciplinaryActionCategory_ID = (typ == 2 || typ == 3) ? data
                        .disciplinaryActionCategory_ID : 0;
                    if (typ != 4) {
                        var onclick = 'onclick="initSubjExplain(' + data.subjectEmp_ID + ',' + data
                            .incidentReport_ID + ',' + typ + ',' + lvl + ',' + disciplineCategory_ID + ',' +
                            data.disciplinaryActionCategorySettings_ID + ',' +
                            disciplinaryActionCategory_ID + ')"';
                        var icon = (typ == 7) ? "search" : "edit";
                        var title = (typ == 7) ? "View IR Information" : "Update IR Details";
                        var witnessBtn = "";
                        if (typ == 1 && parseInt(data.witnessCount) > 0) {
                            witnessBtn = '<a class="ml-2 btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air witnessViewBtn" title="witnesses" data-irid="' + data.incidentReport_ID + '"><i class="la la-binoculars"></i></a>'
                        }
                        var html = '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="' + title + '" data-qualifiedid="111" ' + onclick + '><i class="la la-' + icon + '"></i></a>' + witnessBtn;

                    } else {
                        var onclick = 'onclick="initSetEffectiveDate(' + data.incidentReport_ID + ')"';
                        var html =
                            '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="Update IR Details" data-qualifiedid="111" ' +
                            onclick + '><i class="la la-edit"></i></a>';
                        html +=
                            '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View IR Information" data-qualifiedid="111" onclick="initSubjExplain(' +
                            data.subjectEmp_ID + ',' + data.incidentReport_ID + ',7,' + lvl + ',' +
                            disciplineCategory_ID + ',' + data.disciplinaryActionCategorySettings_ID + ',' +
                            disciplinaryActionCategory_ID + ')"><i class="la la-search"></i></a>';
                    }

                    return html;
                }
            }, ],
        };

        $(tbl).mDatatable(options);


    }
    // MJM end code
</script>


<!-- MIC CODES -->
<script>
    function removeIrWitness(element) {
        console.log(element);
        $.when(fetchPostData({
            witnessId: $(element).data('witnessid'),
        }, '/discipline/removeWitness')).then(function(removeWitness) {
            var removeWitnessObj = $.parseJSON(removeWitness.trim());
            console.log(removeWitnessObj);
            if (parseInt(removeWitnessObj.confirmed)) {
                if (parseInt(removeWitnessObj.cancel_stat)) {
                    swal(
                        'Successfully cancelled!',
                        'Successfully cancelled the witness confirmation!',
                        'success');
                } else {
                    swal(
                        'Error in Cancelling the Witness!',
                        'Something went wrong while cancelling the witness confirmation. Please report this immediately to the system administrator!',
                        'error');
                }
            } else {
                swal(
                    'Witness is done confirming!',
                    'Cancellation of witness confirmation will be aborted!',
                    'info');
            }
            getWitnessNames($(element).data('irid'), function(witnessObj) {
                subjExplainFunc1(1, start = null, end = null);
            });
        });
    }

    function confirmRemoveWitness(element) {
        swal({
            title: 'Are you sure you want to remove ' + $(element).data('witnessname') + ' as witness?',
            text: "You will not be able to revert back after deletion.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Remove!',
            cancelButtonText: 'No, Don\'t Remove',
            reverseButtons: true,
            // width: '500px'
        }).then(function(result) {
            if (result.value) {
                // deleteDisciplinaryAction(element);
                removeIrWitness(element);
            } else if (result.dismiss === 'cancel') {

            }
        });
    }

    function getWitnessNames(irIdVal, callback) {
        $.when(fetchPostData({
            irId: irIdVal,
        }, '/discipline/get_ir_witnesses')).then(function(witness) {
            var witnessObj = $.parseJSON(witness.trim());
            console.log(witnessObj);
            let witnessList = "";
            if (parseInt(witnessObj.exist)) {
                $.each(witnessObj.witnesses, function(index, value) {
                    witnessList += '<tr>' +
                        '<td class="text-center col-10">' + value.fname + ' ' + value.lname + '</td>' +
                        '<td class="text-center col-2">' +
                        '<a class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air removeWitness" style="width: 22px !important;height: 22px !important;" data-witnessid="' + value.incidentReportWitness_ID + '" data-irid="' + irIdVal + '" data-witnessname="' + value.fname + ' ' + value.lname + '"><i class="fa fa-remove"></i></a>' +
                        '</td>' +
                        '</tr>';
                })
                $('#witnessList').html(witnessList);
            } else {
                $('#removeWitnessesModal').modal('hide');
            }
            callback(witnessObj);
        });
    }

    function initWitnessView(irIdVal) {
        $.when(fetchPostData({
            irId: irIdVal,
        }, '/discipline/get_witness_view')).then(function(witness) {
            var witnessObj = $.parseJSON(witness.trim());
            console.log(witnessObj);
            $('#subjectEmpDetailsWitness').html(witnessObj.subject_details);
            $('#irDetailsWitness').html(witnessObj.incident);
            getWitnessNames(irIdVal, function(witnessList) {})
            $('#removeWitnessesModal').modal('show');
        });
    }

    function verifyQualifiedAccess() {
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            var pw = CryptoJS.SHA1(result.value);
            var pw2 = '<?php echo $_SESSION["pw"]; ?>';
            if (pw == pw2) {
                if (pw == pw2) {
                    // initWitnessView(irId);
                    $('#secureQualified').hide();
                    initIrAbleDtrViolation();
                    $('#qualifiedTabView').fadeIn();
                }
            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });
    }

    function verifyQualifiedRecordAccess() {
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            var pw = CryptoJS.SHA1(result.value);
            var pw2 = '<?php echo $_SESSION["pw"]; ?>';
            if (pw == pw2) {
                if (pw == pw2) {
                    // initWitnessView(irId);
                    $('#secureQualifiedRecord').hide();
                    initQualifiedRecord();
                    $('#qualifiedRecordTabView').fadeIn();
                }
            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });
    }
    function verifyRecords_dismissed() {
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            var pw = CryptoJS.SHA1(result.value);
            var pw2 = '<?php echo $_SESSION["pw"]; ?>';
            if (pw == pw2) {
                if (pw == pw2) {
                    // initWitnessView(irId);
                    $('#secureQualifiedRecord_dismissed').hide();
                    init_dismissedrec();
                    $('#qualifiedRecordTabView_dismissed').fadeIn();
                }
            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });
    }
    function verifyViolationTabularAccess() {
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            var pw = CryptoJS.SHA1(result.value);
            var pw2 = '<?php echo $_SESSION["pw"]; ?>';
            if (pw == pw2) {
                if (pw == pw2) {
                    // initWitnessView(irId);
                    $('#secureDtrViolationDatatable').hide();
                    $('#datatable_dtrviolations').fadeIn();
                }
            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });
    }

    function verifyViewWitness(irId) {
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            var pw = CryptoJS.SHA1(result.value);
            var pw2 = '<?php echo $_SESSION["pw"]; ?>';
            if (pw == pw2) {
                if (pw == pw2) {
                    initWitnessView(irId);
                }

            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
            }
        });
    }

    function verifyViewIrProfile() {
        swal({
            title: 'Please type your password to proceed.',
            input: 'password',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            preConfirm: (login) => {
                // alert(login)
            },
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write something!'
                }
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            console.log(result);
            var pw = CryptoJS.SHA1(result.value);
            var pw2 = '<?php echo $_SESSION["pw"]; ?>';
            console.log(pw + "== " + pw2);
            if (pw == pw2) {
                if (pw == pw2) {
                    initEmployeeIrProfile(3, function(exist) {
                        initEmployeeProfile();
                        $('#AccessSupIrProfile').show();
                        $('#noAccessSupIrProfile').hide();
                    });
                }
            } else {
                swal("Password Error", 'It seems you have typed a wrong password.', 'error');
                $('#AccessSupIrProfile').hide();
                $('#noAccessSupIrProfile').show();
                $('#empIrProfileSelect').hide();
                $('#showIrPofileRecords').hide();
            }
        });
    }
</script>
<!-- END OF CODES -->


<script type="text/javascript">
    //------------------NICCA CODES STARTS HERE---------------------------------------------------------------------

    $(function() {
        var myoptions = {
            innerSize: 60,
            depth: 45,
            dataLabels: {
                distance: 1
            }
        };
        var currentdate = tz_date(moment());
        var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
        var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
        var datatable_dtrviolations = datatable_dtrviolations_init("get_supervisors_dtr_system_violations", {
            query: {
                incidentDate_start: moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD'),
                incidentDate_end: moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD'),
                violation_accounts: $("#violation-accounts").val(),
                violation_type: $("#violation-type").val(),
                violation_class: $("#violation-class").val()
            }
        });
        $('#myIrs_incidentDate').val(start + ' - ' + end);
        $('#myIrs_incidentDate').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
                'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate, 'YYYY-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(currentdate, 'YYYY-MM-DD')],
                'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate, 'YYYY-MM-DD').endOf('month')],
                'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'), moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')]
            }
        }, function(start, end, label) {
            subjExplainFunc(7, start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
        });
        $('#violation-date').val(start + ' - ' + end);
        $('#violation-date').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            startDate: start,
            endDate: end,
            opens: "left",
            ranges: {
                'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
                'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate,
                    'YYYY-MM-DD').subtract(1, 'days')],
                'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(
                    currentdate, 'YYYY-MM-DD')],
                'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate,
                    'YYYY-MM-DD').endOf('month')],
                'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'),
                    moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')
                ]
            }
        }, function(start, end, label) {
            $('#violation-date .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
                'MM/DD/YYYY'));
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['incidentDate_start'] = moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD');
            query['incidentDate_end'] = moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD');
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'Supervisor');
        });
        set_violation_types();
        get_unique_accounts_subordinates(function(res) {
            $("#violation-accounts").html("<option value=''>All</option>");
            $.each(res.accounts, function(i, item) {
                $("#violation-accounts").append("<option value='" + item.acc_id +
                    "' data-class='" + item.acc_description + "'>" + item.acc_name +
                    "</option>");
            });
        });
        $("#violation-class").change(function() {
            var theclass = $(this).val();
            $("#violation-accounts").children('option').css('display', '');
            $("#violation-accounts").val('').change();
            if (theclass !== '') {
                $("#violation-accounts").children('option[data-class!="' + theclass + '"][value!=""]')
                    .css('display', 'none');
            }
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['violation_class'] = $("#violation-class").val();
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'Supervisor');
        });
        $("#violation-accounts,#violation-type").change(function() {
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['violation_accounts'] = $("#violation-accounts").val();
            query['violation_type'] = $("#violation-type").val();
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'Supervisor');
        });
        $('#violation-search').keyup(function() {
            var query = datatable_dtrviolations.getDataSourceQuery();
            query['violation_search'] = $("#violation-search").val();
            datatable_dtrviolations.setDataSourceQuery(query);
            datatable_dtrviolations.reload();
            initialize_chart_dtrviolation(myoptions, 'Supervisor');
        });
        initialize_chart_dtrviolation(myoptions, 'Supervisor');
        $('.dmsSupervisionMainNav[data-navcontent="dtrViolations"],#violationLogTab').click(function() {
            $("a[href='#violation-chart']").click();
        })
    });


    //------------------NICCA CODES ENDS HERE---------------------------------------------------------------------
</script>

<!-- MIC MIC CODES START HERE -->
<script>
    $('#explanationTab').on('click', '.witnessViewBtn', function() {
        verifyViewWitness($(this).data('irid'));
    })
    $('#witnessList').on('click', '.removeWitness', function() {
        confirmRemoveWitness(this);
    })
</script>
<!-- MIC MIC CODES END HERE -->