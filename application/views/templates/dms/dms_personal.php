<style>
.m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
    padding: 1rem 0 !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
    background: unset !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
    background: #F0F0F2 !important;
    color: black !important;
}

#profileOffenseDatatable.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell {
    padding: 7px 10px;
}

#profileOffenseDatatable.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell,
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell {
    padding: 9px 10px;
    =
}

#profileOffenseDatatable.m-datatable.m-datatable--default>.m-datatable__pager {
    margin-top: 0 !important;
}

#profileOffenseDatatable.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table {
    min-height: 75px !important;
}

.m-datatable__head {
    background: #e0e0e0;
}

.highcharts-exporting-group,
.highcharts-credits {
    display: none
}

.highcharts-title {
    font-family: 'Poppins' !important;
}

.font-12 {
    font-size: 12px !important;
}
</style>
<link href="<?php echo base_url(); ?>assets/src/custom/plugins/chartist/dist/chartist.min.css" rel="stylesheet"
    type="text/css" />

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">DMS Personal</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <input type="hidden" id="hidden_base_url" name="hidden_base_url" value="<?php echo base_url(); ?>" />
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--tabs active" id="personalPortlet">
                    <div class="m-portlet__head"
                        style="background: #2c2e3eeb;padding: 0 1.9rem !important;height:4rem !important">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a id="disciplinaryActionTab" class="nav-link m-tabs__link active" data-toggle="tab"
                                        href="#myIrs" role="tab">
                                        <i class="fa fa-balance-scale"></i>My IRs <span
                                            class="m-badge m-badge--dot m-badge--danger m-animate-blink" style="display:none"></span></a>
                                </li>

                                <li class="nav-item m-tabs__item">
                                    <a id="subjectExplanationTab" class="nav-link m-tabs__link" data-toggle="tab"
                                        href="#mySubjectExplanation" role="tab">
                                        <i class="fa fa-address-card"></i>Subject Explanation <span
                                            class="m-badge m-badge--dot m-badge--danger m-animate-blink" style="display:none"></span></a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="offenseCategoryTab" class="nav-link m-tabs__link" data-toggle="tab"
                                        href="#myDtrViolations" role="tab">
                                        <i class="fa fa-list-alt"></i>My DTR Violations</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab"
                                        href="#myIrInvolvement" role="tab">
                                        <i class="fa fa-puzzle-piece"></i>IR Involvement</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="profileTab" class="nav-link m-tabs__link" data-toggle="tab"
                                        href="#myIrProfile" role="tab">
                                        <i class="fa fa-user-circle-o"></i>Profile</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane fade show active col-md-12" id="myIrs" role="tabpanel">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#myIrs_pending"><i
                                                class="fa fa-list-ul m--font-brand"></i>Ongoing Incident Reports</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#myIrs_completed"><i
                                                class="fa fa-archive m--font-brand"></i>Records</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active show" id="myIrs_pending" role="tabpanel">
                                        <div class="row mb-3">
                                            <div class="col-md-5">
                                                <div class="input-group">
                                                    <input type="text" class="form-control m-input"
                                                        placeholder="Search Incident Report Keywords"
                                                        aria-describedby="basic-addon2" id="myIrs_search_pending"
                                                        name="myIrs_search">
                                                    <div class="input-group-append" data-toggle="m-tooltip" title=""
                                                        data-original-title="Offense, Incident Details and Disciplinary Action">
                                                        <span class="input-group-text"><i
                                                                class="la la-search"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="m_datatable" id="datatable_myIrs_pending"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="myIrs_completed" role="tabpanel">
                                        <div class="row mb-3">
                                            <div class="col-md-5">
                                                <div class="input-group">
                                                    <input type="text" class="form-control m-input"
                                                        placeholder="Search Incident Report Keywords"
                                                        aria-describedby="basic-addon2" id="myIrs_search_completed"
                                                        name="myIrs_search">
                                                    <div class="input-group-append" data-toggle="m-tooltip" title=""
                                                        data-original-title="Offense, Incident Details and Disciplinary Action">
                                                        <span class="input-group-text"><i
                                                                class="la la-search"></i></span></div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="input-group pull-right">
                                                    <input type="text" class="form-control m-input" readonly=""
                                                        placeholder="Select date range" id="myIrs_incidentDate">
                                                    <div class="input-group-append" data-toggle="m-tooltip" title=""
                                                        data-original-title="Incident Date">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="custom-select form-control" id="myIrs_liabilityStatus"
                                                    data-toggle="m-tooltip" title=""
                                                    data-original-title="Liability Status">
                                                    <option value="">ALL</option>
                                                    <option value="17">Liable</option>
                                                    <option value="18">Non-liable</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="m_datatable" id="datatable_myIrs_completed"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane col-md-12" id="myDtrViolations" role="tabpanel">
                                <div class="row no-gutters">
                                    <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                        <select name="" class="m-input form-control font-12" id="violation-class"
                                            data-toggle="m-tooltip" title="" data-original-title="Employee Type">
                                            <option value="">All Class</option>
                                            <option value="Agent">Ambassador</option>
                                            <option value="Admin">SZ Team</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                        <select name="" class="m-input form-control font-12" id="violation-accounts"
                                            data-toggle="m-tooltip" title="" data-original-title="Accounts">
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-6 px-1 pb-1">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input font-12"
                                                style="padding-bottom:12px" placeholder="Select date range"
                                                id="violation-date" data-toggle="m-tooltip" title=""
                                                data-original-title="Date Recorded" />
                                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                        class="fa fa-calendar"></i></span></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                        <select class="form-control text-capitalize font-12" id="violation-type"
                                            data-toggle="m-tooltip" title="" data-original-title="Violation Type">
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-6 px-1 pb-1">
                                        <div class="m-input-icon m-input-icon--right">
                                            <input type="text" class="form-control m-input font-12"
                                                style="padding-bottom:12px" placeholder="Search Name..."
                                                id="violation-search" data-toggle="m-tooltip" title=""
                                                data-original-title="Employee Name" />
                                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i
                                                        class="fa fa-search"></i></span></span>
                                        </div>
                                    </div>
                                </div>

                                <h5 id="violation-header" class="mt-3 text-center"></h5>
                                <hr style="border-style: dashed;">
                                <ul class="nav nav-tabs px-1" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#violation-chart"><i
                                                class="fa fa-pie-chart m--font-brand"></i> Graphical View</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#violation-tabular"><i
                                                class="fa fa-table m--font-info"></i> Tabular View</a>
                                    </li>
                                </ul>
                                <div class="tab-content px-1">
                                    <div class="tab-pane active" id="violation-chart" role="tabpanel">
                                        <div class="row m-row--no-padding m-row--col-separator-xl">
                                            <div class="col-xl-6">
                                                <div id="container-chart-pie" style="height: 300px;margin: 0 auto">
                                                </div>
                                            </div>
                                            <div class="col-xl-6 py-5 px-4">
                                                <div class="m-widget15">
                                                    <div class="m-widget15__items" id="violationList">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="violation-tabular" role="tabpanel">
                                        <div class="m_datatable" id="datatable_dtrviolations"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane col-md-12" id="myIrInvolvement" role="tabpanel">
                                <div class="row mb-3">

                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <input type="text" class="form-control m-input"
                                                placeholder="Search Incident Report Keywords"
                                                aria-describedby="basic-addon2" id="myInvolvement_search"
                                                name="myInvolvement_search">
                                            <div class="input-group-append" data-toggle="m-tooltip" title=""
                                                data-original-title="Offense, Incident Details and Disciplinary Action">
                                                <span class="input-group-text"><i class="la la-search"></i></span></div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="input-group pull-right">
                                            <input type="text" class="form-control m-input" readonly=""
                                                placeholder="Select date range" id="myInvolvement_incidentDate">
                                            <div class="input-group-append" data-toggle="m-tooltip" title=""
                                                data-original-title="Incident Date">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="custom-select form-control" id="myInvolvement_type"
                                            data-toggle="m-tooltip" title="" data-original-title="Involvement Type">
                                            <option value="">ALL</option>
                                            <option value="source">Source</option>
                                            <option value="witness">Witness</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-12">
                                        <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;"></div>
                                    </div>
                                    <div class="col-12">
                                        <div class="m_datatable" id="datatable_myInvolvement"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane col-md-12" id="myIrProfile" role="tabpanel">
                                <div class="row p-4"
                                    style="margin-left: -3.3rem;margin-right:-3.3rem;margin-top: -2.3rem; box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">
                                    <div class="col-12 col-md-3 mb-2">
                                        <img src="" onerror="noImageFound(this)" alt=""
                                            class="img-fluid mx-auto d-block" style="border: 0.3rem solid gainsboro;"
                                            id="profilePic">
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="row mb-3">
                                            <span class="ml-3"
                                                style="font-size: 17px; font-weight: 500; color: #5d4747;">Employee
                                                Information</span>
                                            <div class="col">
                                                <hr style="margin-top: 0.8rem !important;">
                                            </div>
                                        </div>
                                        <div class="row pl-3 pr-5 mb-3">
                                            <div class="col-12 col-md-4 font-weight-bold">Fullname:</div>
                                            <div class="col-12 col-md-8 pl-5 pl-md-0"
                                                style="font-size: 13px; font-weight: 400" id="profileName"></div>
                                            <div class="col-12 col-md-4 font-weight-bold">Current Position:</div>
                                            <div class="col-12 col-md-8 pl-5 pl-md-0"
                                                style="font-size: 13px; font-weight: 400"> <span id="profilePosition"
                                                    style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;"></span>
                                                - <span id="profileEmpStatus"
                                                    style="font-size: 11px;color: #0012ff;"></span></div>
                                            <div class="col-12 col-md-4 font-weight-bold">Current Team:</div>
                                            <div class="col-12 col-md-8 pl-5 pl-md-0"
                                                style="font-size: 13px; font-weight: 400" id="profileAccount"></div>
                                        </div>
                                        <div class="row mb-4">
                                            <span class="ml-3"
                                                style="font-size: 17px; font-weight: 500; color: #5d4747;">IR Record
                                                Range</span>
                                            <div class="col">
                                                <hr style="margin-top: 0.8rem !important;">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-sm-5 font-italic pt-2">(Please Specify a Date Range)</div> -->
                                            <div class="col-sm-7">
                                                <div class="input-group pull-right">
                                                    <input type="text" class="form-control m-input" readonly=""
                                                        placeholder="Select date range" id="irProfileDateRange"
                                                        style="background: white;">
                                                    <div class="input-group-append" data-toggle="m-tooltip" title=""
                                                        data-original-title="Date Schedule">
                                                        <span class="input-group-text"
                                                            style="background: #fbfbfb !important;">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <select class="custom-select form-control" id="irProfileLiabilityStat"
                                                    name="irProfileLiabilityStat" placeholder="Select a liability">
                                                    <option value="0">All</option>
                                                    <option value="17">Liable</option>
                                                    <option value="18">Non-Liable</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4" id="showMyIrStats"
                                    style="margin-left: -2rem; margin-right: -2rem;display:none">
                                    <div class="col-12 mt-2">
                                        <div class="row m-row--no-padding">
                                            <div class="col-xl-7 pr-xl-4">
                                                <div class="m-portlet m-portlet--height-fluid-half m-portlet--border-bottom-danger"
                                                    style="margin-bottom: 1.4rem !important;">
                                                    <div class="m-portlet__body m-portlet__body--fluid">
                                                        <div class="m-widget26">
                                                            <div class="m-widget26__content">
                                                                <div class="natureOffenseChart" id="natureOffenseChart"
                                                                    style="overflow: visible !important;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-5">
                                                <div class="m-portlet m-portlet--height-fluid-half m-portlet--border-bottom-danger"
                                                    style="margin-bottom: 1.4rem !important;">
                                                    <div class="m-portlet__body m-portlet__body--fluid">
                                                        <div class="m-widget26">
                                                            <div class="m-widget26__content text-center">
                                                                <span class="mr-2">
                                                                    <i class="fa fa-file-text"
                                                                        style="font-size: 2rem;color: #ff5371;"></i>
                                                                </span>
                                                                <span class="m-widget26__number mr-2"
                                                                    id="irTotalCount">640</span>
                                                                <span class="m-widget26__desc"
                                                                    style="font-weight: 500 !important;">Total Incident
                                                                    Report Count</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-portlet m-portlet--height-fluid-half m-portlet--border-bottom-danger"
                                                    style="margin-bottom: 1.4rem !important;">
                                                    <div class="m-portlet__body m-portlet__body--fluid"
                                                        style="padding: 0 !important">
                                                        <div class="m-widget1"
                                                            style="padding-top: 1rem !important;padding-bottom: 1rem !important"
                                                            id="discActionCount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12" id="offenseHistory" style="display:none">
                                                <div class="m-portlet m-portlet--height-fluid-half m-portlet--border-bottom-danger"
                                                    style="margin-bottom: 1.4rem !important;">
                                                    <div class="m-portlet__body m-portlet__body--fluid">
                                                        <div class="m-widget26">
                                                            <div class="m-widget26__content">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-9">
                                                                        <select
                                                                            class="form-control m-select2 text-truncate"
                                                                            id="specificOffenseHistory"
                                                                            name="specificOffenseHistory"
                                                                            data-toggle="m-tooltip"
                                                                            title="Select Employee Name"></select>
                                                                    </div>
                                                                    <div class="col-12 col-md-3">
                                                                        <div class="form-group m-form__group">
                                                                            <div class="input-group">
                                                                                <input type="text" class="form-control"
                                                                                    placeholder="Search by IR-ID"
                                                                                    id="searchProfileHistory">
                                                                                <div class="input-group-append">
                                                                                    <span class="input-group-text"
                                                                                        id="basic-addon2">
                                                                                        <i class="fa fa-search"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <div class="mt-1"
                                                                            id="profileIrHistoryDatatable"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row" id="noIrRecordProfile" style="margin-right: 0px; margin-left: 0px; height: 80%;">
                                            <div class="col-md-12 p-0" style="display: flex;align-items:center;">
                                                <div class="col-md-12 p-0 text-center mb-4" style="border-style: dashed;border-width: 1px;border-color: #e4e4e4;">
                                                    <div class="m-demo-icon__preview">
                                                        <i style="font-size: 8rem;" class="flaticon-notes"></i>
                                                    </div>
                                                    <span class="m-demo-icon__class" style="font-size: 2rem;">No Records To Show</span>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="row" id="notShowMyIrStats" style="display:none;">
                                    <div class="col-12 py-4 mx-3 mt-5 text-center">
                                        <i style="font-size: 42px;margin-right:1rem" class="fa fa-file-o"></i>
                                        <span class="m-demo-icon__class" style="font-size: 3rem;color: #670101;">No
                                            Records To Show</span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane col-md-12" id="mySubjectExplanation" role="tabpanel">
                                <div class="row mb-3">
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <input type="text" class="form-control m-input"
                                                placeholder="Search Incident Report Keywords"
                                                aria-describedby="basic-addon2" id="mySubjectExplanation_search"
                                                name="mySubjectExplanation_search">
                                            <div class="input-group-append" data-toggle="m-tooltip" title=""
                                                data-original-title="Offense, Incident Details and Disciplinary Action">
                                                <span class="input-group-text"><i class="la la-search"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-3" style="border-bottom: 1px dashed #e6e6e6 !important;">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="m_datatable" id="datatable_mySubjectExplanation"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal-ir-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Incident Report Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row ir_details_template" id="ir_details_template"></div>
                <div id="subjectExplanationForm">
                    <div>
                        <div class="row p-3">
                            <span class="ml-3" style="font-size: 14px;font-weight: 500;color: #5d4747;"><i
                                    class="la la-tag" style="font-size: 1.1rem;"></i> Action</span>
                            <div class="col">
                                <hr style="margin-top: 0.8rem !important;">
                            </div>
                        </div>
                    </div>
                    <div id="subjectContainer" class="px-3">
                        <div id="uploadForm" class="mx-5">
                            <p class="text-center m--font-bolder">Upload Soft Copy of Written Explanation (Optional):
                                <br>
                                <small>Note: File must be in a PDF format with maximum of 5MB file size</small>
                            </p>
                            <div style="border:2px dashed #e4e4e4;" class="p-3 dm-uploader text-center"
                                id="drag-and-drop-zone">
                                <h4 style="color:#aaa">
                                    <i class="fa fa-upload mb-2" style="font-size:40px"></i>
                                    <br>
                                    Drag and Drop</h4>
                                <input type="file" id="input-upload" style="display:none" />
                                <p class="mt-2">or <a href="#" class="m-link m--font-boldest"
                                        id="upload_link">browse</a> your file to
                                    upload
                                </p>
                            </div>
                            <p class="text-center mt-2 m--font-bold" id="upload-debug"></p>
                        </div>

                        <br>
                        <div id="explanationForm" class="mx-5">
                            <div class="form-group m-form__group">
                                <label for="exampleTextarea" class="m--font-bolder">Explanation:</label>
                                <textarea class="form-control m-input m-input--solid" id="explainTxtArea"
                                    name="explainTxtArea" rows="4" spellcheck="false"></textarea>
                                <small class="m--font-danger" id="txtOutput"></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-brand" id="btn-subjectExplanation">Submit</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/plugins/chartist/dist/chartist.min.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_dtrviolation.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/dms_ir_profile.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>

<link href="<?php echo base_url(); ?>assets/src/custom/css/jquery.dm-uploader.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.dm-uploader.min.js"></script>
<script type="text/javascript">
function datatable_myIrs_init(elem, searchelem, paramsquery) {
    var datatable = $(elem).mDatatable({
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    // sample GET method
                    method: 'POST',
                    url: "<?php echo base_url(); ?>Discipline/get_my_personal_irs",
                    map: function(raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        if (elem === '#datatable_myIrs_pending') {
                            if (dataSet.length > 0) {
                                $("#disciplinaryActionTab").find('.m-badge--dot').show();
                            } else {
                                $("#disciplinaryActionTab").find('.m-badge--dot').hide();
                            }
                        }
                        // $.each(raw.data, function (index, value){}
                        return dataSet;
                    },
                    params: paramsquery
                }
            },
            saveState: {
                cookie: false,
                webstorage: false
            },
            pageSize: 5,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        // layout definition
        layout: {
            theme: 'default', // datatable theme
            class: '', // custom wrapper class
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            //                    height: 550, // datatable's body's fixed height
            footer: false // display/hide footer
        },
        // column sorting
        sortable: true,
        pagination: true,
        toolbar: {
            // toolbar placement can be at top or bottom or both top and bottom repeated
            placement: ['bottom'],
            // toolbar items
            items: {
                // pagination
                pagination: {
                    // page size select
                    pageSizeSelect: [5, 10, 20, 30, 50, 100]
                }
            }
        },
        search: {
            input: $(searchelem)
        },
        // columns definition
        columns: [{
            field: 'ir_id',
            title: 'IR-ID',
            textAlign: 'center',
            sortable: true,
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + (row.incidentReport_ID).padLeft(8) +
                    "</span>";
            }
        }, {
            field: 'offense',
            title: 'Offense',
            textAlign: 'center',
            sortable: true,
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + row.offense + "</span>";
            }
        }, {
            field: 'action',
            title: 'Disciplinary Action',
            sortable: true,
            textAlign: 'center',
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + row.action + "</span>";
            }
        }, {
            field: 'incidentDate',
            title: 'Incident Datetime',
            textAlign: 'center',
            sortable: true,
            width: 90,
            template: function(row, index, datatable) {
                return moment(row.incidentDate).format('MMM DD YYYY') + "<br>" + moment(moment()
                    .format('YYYY-MM-DD') + " " + row.incidentTime).format('hh:mm A');
            }
        }, {
            field: 'liabilityStat',
            title: 'Status',
            textAlign: 'center',
            sortable: true,
            width: 90,
            template: function(row, index, datatable) {
                return (row.liabilityStat === '18') ?
                    '<span class="m-badge m-badge--brand m-badge--wide m--font-bolder" style="font-size:10px !important">Non-Liable</span>' :
                    (row.liabilityStat === '17') ?
                    '<span class="m-badge m-badge--danger m-badge--wide m--font-bolder" style="font-size:10px !important">Liable</span>' :
                    '<span class="m-badge m-badge--warning m-badge--wide m--font-bolder" style="font-size:10px !important">Pending</span>';

            }
        }, {
            field: 'Actions',
            width: 60,
            title: 'Actions',
            textAlign: 'center',
            sortable: false,
            overflow: 'visible',
            template: function(row, index, datatable) {
                return '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Details" data-incidentreportid="' +
                    row.incidentReport_ID +
                    '" data-toggle="modal" data-target="#modal-ir-details" data-from="myirs"><i class="la la-eye"></i></a>';
            }
        }]
    });
    return datatable;
}

function datatable_myInvolvement_init(paramsquery) {

    var datatable = $("#datatable_myInvolvement").mDatatable({
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    // sample GET method
                    method: 'POST',
                    url: "<?php echo base_url(); ?>Discipline/get_my_involvement_list",
                    map: function(raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    },
                    params: paramsquery
                }
            },
            saveState: {
                cookie: false,
                webstorage: false
            },
            pageSize: 5,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        // layout definition
        layout: {
            theme: 'default', // datatable theme
            class: '', // custom wrapper class
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            //                    height: 550, // datatable's body's fixed height
            footer: false // display/hide footer
        },
        // column sorting
        sortable: true,
        pagination: true,
        toolbar: {
            // toolbar placement can be at top or bottom or both top and bottom repeated
            placement: ['bottom'],
            // toolbar items
            items: {
                // pagination
                pagination: {
                    // page size select
                    pageSizeSelect: [5, 10, 20, 30, 50, 100]
                }
            }
        },
        search: {
            input: $('#myInvolvement_search')
        },
        // columns definition
        columns: [{
            field: 'ir_id',
            title: 'IR-ID',
            textAlign: 'center',
            sortable: true,
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + (row.incidentReport_ID).padLeft(8) +
                    "</span>";
            }
        }, {
            field: 'name',
            title: 'Subject',
            textAlign: 'center',
            sortable: true,
            width: 90,
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + row.lname + ", " + row.fname + "</span>";
            }
        }, {
            field: 'offense',
            title: 'Offense',
            textAlign: 'center',
            sortable: true,
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + row.offense + "</span>";
            }
        }, {
            field: 'action',
            title: 'Disciplinary Action',
            sortable: true,
            textAlign: 'center',
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + row.action + "</span>";
            }
        }, {
            field: 'customtype',
            title: 'Involvement',
            textAlign: 'center',
            sortable: true,
            width: 90,
            template: function(row, index, datatable) {
                return (row.customtype === 'source') ?
                    '<span class="m-badge m-badge--brand m-badge--wide m--font-bolder" style="font-size:10px !important">Source</span>' :
                    '<span class="m-badge m-badge--info m-badge--wide m--font-bolder" style="font-size:10px !important">Witness</span>';

            }
        }, {
            field: 'incidentDate',
            title: 'Incident Datetime',
            textAlign: 'center',
            sortable: true,
            width: 90,
            template: function(row, index, datatable) {
                return moment(row.incidentDate).format('MMM DD YYYY') + "<br>" + moment(moment()
                    .format('YYYY-MM-DD') + " " + row.incidentTime).format('hh:mm A');
            }
        }, {
            field: 'Actions',
            width: 70,
            title: 'Actions',
            textAlign: 'right',
            sortable: false,
            overflow: 'visible',
            template: function(row, index, datatable) {
                var string = '';
                if (row.customtype === 'source') {
                    string =
                        '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Details" data-incidentreportid="' +
                        row.incidentReport_ID +
                        '" data-toggle="modal" data-target="#modal-ir-details" data-from="myinvolvement" data-type="source"><i class="la la-eye"></i></a>';
                } else if (row.customtype === 'witness' && row.witness_status === '2') {
                    string =
                        '<a target="_blank" href="<?php echo base_url(); ?>Discipline/witness_confirmation/' +
                        row.incidentReport_ID +
                        '" class="btn btn-sm btn-success m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="Go to witness confirmation page"><i class="la la-external-link"></i></a> <a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Details" data-incidentreportid="' +
                        row.incidentReport_ID +
                        '" data-toggle="modal" data-target="#modal-ir-details" data-from="myinvolvement" data-type="witness"><i class="la la-eye"></i></a>';
                } else {
                    string = '-';
                }
                return string;
            }
        }]
    });
    return datatable;
}

function datatable_mySubjectExplanation_init(elem, searchelem) {
    var datatable = $(elem).mDatatable({
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    // sample GET method
                    method: 'POST',
                    url: "<?php echo base_url(); ?>Discipline/get_my_subject_explanation",
                    map: function(raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        if (elem === '#datatable_mySubjectExplanation') {
                            if (dataSet.length > 0) {
                                $("#subjectExplanationTab").find('.m-badge--dot').show();
                            } else {
                                $("#subjectExplanationTab").find('.m-badge--dot').hide();
                            }
                        }
                        // $.each(raw.data, function (index, value){}
                        return dataSet;
                    }
                }
            },
            saveState: {
                cookie: false,
                webstorage: false
            },
            pageSize: 5,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        // layout definition
        layout: {
            theme: 'default', // datatable theme
            class: '', // custom wrapper class
            scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
            //                    height: 550, // datatable's body's fixed height
            footer: false // display/hide footer
        },
        // column sorting
        sortable: true,
        pagination: true,
        toolbar: {
            // toolbar placement can be at top or bottom or both top and bottom repeated
            placement: ['bottom'],
            // toolbar items
            items: {
                // pagination
                pagination: {
                    // page size select
                    pageSizeSelect: [5, 10, 20, 30, 50, 100]
                }
            }
        },
        search: {
            input: $(searchelem)
        },
        // columns definition
        columns: [{
            field: 'ir_id',
            title: 'IR-ID',
            textAlign: 'center',
            sortable: true,
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + (row.incidentReport_ID).padLeft(8) +
                    "</span>";
            }
        }, {
            field: 'offense',
            title: 'Offense',
            textAlign: 'center',
            sortable: true,
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + row.offense + "</span>";
            }
        }, {
            field: 'action',
            title: 'Disciplinary Action',
            sortable: true,
            textAlign: 'center',
            template: function(row, index, datatable) {
                return "<span style='font-size:13px;'>" + row.action + "</span>";
            }
        }, {
            field: 'incidentDate',
            title: 'Incident Datetime',
            textAlign: 'center',
            sortable: true,
            width: 90,
            template: function(row, index, datatable) {
                return moment(row.incidentDate).format('MMM DD YYYY') + "<br>" + moment(moment()
                    .format('YYYY-MM-DD') + " " + row.incidentTime).format('hh:mm A');
            }
        }, {
            field: 'liabilityStat',
            title: 'Status',
            textAlign: 'center',
            sortable: true,
            width: 90,
            template: function(row, index, datatable) {
                return (row.liabilityStat === '18') ?
                    '<span class="m-badge m-badge--brand m-badge--wide m--font-bolder" style="font-size:10px !important">Non-Liable</span>' :
                    (row.liabilityStat === '17') ?
                    '<span class="m-badge m-badge--danger m-badge--wide m--font-bolder" style="font-size:10px !important">Liable</span>' :
                    '<span class="m-badge m-badge--warning m-badge--wide m--font-bolder" style="font-size:10px !important">Pending</span>';

            }
        }, {
            field: 'Actions',
            width: 60,
            title: 'Actions',
            textAlign: 'center',
            sortable: false,
            overflow: 'visible',
            template: function(row, index, datatable) {
                if(row.witnessCount > 0){
                    return  '<span class="m-badge m-badge--info m-badge--wide m--font-bolder" style="font-size:8px !important">Witness Confirmation</span>'
                }else{
                    return '<a class="btn btn-sm btn-secondary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" title="View Details" data-incidentreportid="' + row.incidentReport_ID + '" data-toggle="modal" data-target="#modal-ir-details" data-from="mysubjectexplanation"><i class="la la-eye"></i></a>';
                }
            }
        }]
    });
    return datatable;
}

function tz_date(thedate, format = null) {
    var tz = moment.tz(new Date(thedate), "Asia/Manila");
    return (format === null) ? moment(tz) : moment(tz).format(format);
}
$(function() {

    var currentdate = tz_date(moment());
    var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
    var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
    var datatable_myIrs_pending = datatable_myIrs_init('#datatable_myIrs_pending', "#myIrs_search_pending", {
        query: {
            liabilityStatus: [2]
        }
    });

    var datatable_mySubjectExplanation = datatable_mySubjectExplanation_init('#datatable_mySubjectExplanation',
        "#mySubjectExplanation_search");

    var liabilityStats = ($('#myIrs_liabilityStatus').val() === '') ? [17, 18] : [$('#myIrs_liabilityStatus')
        .val()
    ];
    var datatable_myIrs_completed = datatable_myIrs_init('#datatable_myIrs_completed',
        "#myIrs_search_completed", {
            query: {
                liabilityStatus: liabilityStats,
                incidentDate_start: moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD'),
                incidentDate_end: moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD')
            }
        });

    $('#myIrs_incidentDate').val(start + ' - ' + end);
    $('#myIrs_incidentDate').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        opens: "left",
        ranges: {
            'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
            'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate,
                'YYYY-MM-DD').subtract(1, 'days')],
            'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(currentdate,
                'YYYY-MM-DD')],
            'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate,
                'YYYY-MM-DD').endOf('month')],
            'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'),
                moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')
            ]
        }
    }, function(start, end, label) {
        $('#myIrs_incidentDate .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
            'MM/DD/YYYY'));
        var query = datatable_myIrs_completed.getDataSourceQuery();
        query['incidentDate_start'] = moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD');
        query['incidentDate_end'] = moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD');
        datatable_myIrs_completed.setDataSourceQuery(query);
        datatable_myIrs_completed.reload();
    });
    $('#myIrs_liabilityStatus').on('change', function() {
        var query = datatable_myIrs_completed.getDataSourceQuery();
        query['liabilityStatus'] = ($('#myIrs_liabilityStatus').val() === '') ? [17, 18] : [$(
            '#myIrs_liabilityStatus').val()];
        datatable_myIrs_completed.setDataSourceQuery(query);
        // reload datatable
        datatable_myIrs_completed.reload();
    });

    //INVOLVEMENTES-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    var datatable_myInvolvement = datatable_myInvolvement_init({
        query: {
            involvement_type: $('#myInvolvement_type').val(),
            incidentDate_start: moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD'),
            incidentDate_end: moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD')
        }
    });
    $('#myInvolvement_incidentDate').val(start + ' - ' + end);
    $('#myInvolvement_incidentDate').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        opens: "left",
        ranges: {
            'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
            'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate,
                'YYYY-MM-DD').subtract(1, 'days')],
            'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(currentdate,
                'YYYY-MM-DD')],
            'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate,
                'YYYY-MM-DD').endOf('month')],
            'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'),
                moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')
            ]
        }
    }, function(start, end, label) {
        $('#myInvolvement_incidentDate').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
            'MM/DD/YYYY'));
        var query = datatable_myInvolvement.getDataSourceQuery();
        query['incidentDate_start'] = moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD');
        query['incidentDate_end'] = moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD');
        datatable_myInvolvement.setDataSourceQuery(query);
        datatable_myInvolvement.reload();
    });
    $('#myInvolvement_type').on('change', function() {
        var query = datatable_myInvolvement.getDataSourceQuery();
        query['involvement_type'] = $('#myInvolvement_type').val();
        datatable_myInvolvement.setDataSourceQuery(query);
        // reload datatable
        datatable_myInvolvement.reload();
    });

    $("#modal-ir-details").on('hide.bs.modal', function(e) {
        if ($("#modal-ir-details").data('reloadtable') == true) {
            datatable_mySubjectExplanation.reload();
            $("#modal-ir-details").data('reloadtable', false);
        }
    });
});
</script>
<script type="text/javascript">
//------------------NICCA CODES STARTS HERE---------------------------------------------------------------------

$(function() {
    var myoptions = {
        innerSize: 60,
        depth: 45,
        dataLabels: {
            distance: 1
        }
    };
    var currentdate = tz_date(moment());
    var start = moment(currentdate).startOf('month').format('MM/DD/YYYY');
    var end = moment(currentdate).endOf('month').format('MM/DD/YYYY');
    var datatable_dtrviolations = datatable_dtrviolations_init("get_personal_dtr_system_violations", {
        query: {
            incidentDate_start: moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD'),
            incidentDate_end: moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD'),
            violation_accounts: $("#violation-accounts").val(),
            violation_type: $("#violation-type").val(),
            violation_class: $("#violation-class").val()
        }
    });
    $('#violation-date').val(start + ' - ' + end);
    $('#violation-date').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        opens: "left",
        ranges: {
            'Today': [moment(currentdate, 'YYYY-MM-DD'), moment(currentdate, 'Y-MM-DD')],
            'Yesterday': [moment(currentdate, 'YYY-MM-DD').subtract(1, 'days'), moment(currentdate,
                'YYYY-MM-DD').subtract(1, 'days')],
            'Last 7 Days': [moment(currentdate, 'YYYY-MM-DD').subtract(6, 'days'), moment(currentdate,
                'YYYY-MM-DD')],
            'This Month': [moment(currentdate, 'YYYY-MM-DD').startOf('month'), moment(currentdate,
                'YYYY-MM-DD').endOf('month')],
            'Last Month': [moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').startOf('month'),
                moment(currentdate, 'YYYY-MM-DD').subtract(1, 'month').endOf('month')
            ]
        }
    }, function(start, end, label) {
        $('#violation-date .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format(
            'MM/DD/YYYY'));
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['incidentDate_start'] = moment(start, 'MM/DD/YYYY').format('YYYY-MM-DD');
        query['incidentDate_end'] = moment(end, 'MM/DD/YYYY').format('YYYY-MM-DD');
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Personal');
    });
    set_violation_types();
    get_unique_accounts_subordinates(function(res) {
        $("#violation-accounts").html("<option value=''>All</option>");
        $.each(res.accounts, function(i, item) {
            $("#violation-accounts").append("<option value='" + item.acc_id + "' data-class='" +
                item.acc_description + "'>" + item.acc_name + "</option>");
        });
    });
    $("#violation-class").change(function() {
        var theclass = $(this).val();
        $("#violation-accounts").children('option').css('display', '');
        $("#violation-accounts").val('').change();
        if (theclass !== '') {
            $("#violation-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css(
                'display', 'none');
        }
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['violation_class'] = $("#violation-class").val();
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Personal');
    });
    $("#violation-accounts,#violation-type").change(function() {
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['violation_accounts'] = $("#violation-accounts").val();
        query['violation_type'] = $("#violation-type").val();
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Personal');
    });
    $('#violation-search').keyup(function() {
        var query = datatable_dtrviolations.getDataSourceQuery();
        query['violation_search'] = $("#violation-search").val();
        datatable_dtrviolations.setDataSourceQuery(query);
        datatable_dtrviolations.reload();
        initialize_chart_dtrviolation(myoptions, 'Personal');
    });
    initialize_chart_dtrviolation(myoptions, 'Personal');
});
//-------------CONTINUATION-------------
$("#modal-ir-details").on('show.bs.modal', function(e) {
    if (e.namespace === 'bs.modal') {
        var ir_id = $(e.relatedTarget).data('incidentreportid');
        var from = $(e.relatedTarget).data('from');
        $("#modal-ir-details").data('reloadtable', false);
        $("#explainTxtArea").data('fileupload', 'null');
        $("#btn-subjectExplanation").data('irid', ir_id);
        $("#subjectExplanationForm").hide();
        $("#btn-subjectExplanation").hide().prop('disabled', true);
        $("#upload-debug").html('');
        $("#explainTxtArea").val('');
        if (ir_id == undefined) {} else {
            if (from === 'myirs') {
                var link = "ir_details_myirs";
            } else if (from === 'mysubjectexplanation') {
                var link = "ir_details_myirs";
            } else {
                var type = $(e.relatedTarget).data('type');
                if (type === 'source') {
                    var link = "ir_details_myinvolvement_source";
                } else {
                    var link = "ir_details_myinvolvement_witness";
                }
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Discipline/" + link,
                data: {
                    ir_id: ir_id
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.error === 0) {
                        $("#ir_details_template").html(res.ir_details_layout);
                        console.log(from, $("#modal-ir-details").data('from'));
                        if (from === 'mysubjectexplanation') {
                            $("#subjectExplanationForm").show();
                            $("#btn-subjectExplanation").show().prop('disabled', true);
                        }

                    } else {
                        $("#ir_details_template").html('<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show" role="alert">\
                            <div class="m-alert__icon">\
                                <i class="flaticon-exclamation-1"></i>\
                                <span></span>\
                            </div>\
                            <div class="m-alert__text">\
                                <strong>Well done!</strong> You successfully read this message.\
                            </div>\
                            <div class="m-alert__close">\
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                                </button>\
                            </div>\
                        </div>');
                    }
                }
            });
        }
    }
})


//ADDITIONAL FROM SUPERVISION
function ui_add_log(message, color) {
    color = (typeof color === 'undefined' ? '#34bfa3' : color);
    $("#upload-debug").html("<p style='color:" + color + "'>" + message + "</p>");
}
$('#drag-and-drop-zone').dmUploader({
    auto: false,
    url: "<?php echo base_url('Discipline/submitExplanation') ?>",
    extraData: function() {
        return {
            explanation: $("#explainTxtArea").val(),
            fileupload: $("#explainTxtArea").data('fileupload'),
            irid: $("#btn-subjectExplanation").data('irid')
        };
    },
    maxFileSize: 5000000, // 5 Megs
    multiple: false,
    extFilter: ["pdf"],
    onNewFile: (id, file) => {
        ui_add_log(file.name);
        if ($("#explainTxtArea").val().length < 10) {
            $("#btn-subjectExplanation").prop('disabled', true);
        } else {
            $("#btn-subjectExplanation").prop('disabled', false);
        }
        $("#explainTxtArea").data('fileupload', 'true');
    },
    onFileSizeError: (file) => {
        ui_add_log('File \'' + file.name + '\' exceeded the maximum file size', '#f4516c');
        $("#btn-subjectExplanation").prop('disabled', true);
        $("#explainTxtArea").data('fileupload', 'false');
    },
    onFileExtError: (file) => {
        ui_add_log('File \'' + file.name + '\' should be a PDF file', '#f4516c');
        $("#btn-subjectExplanation").prop('disabled', true);
        $("#explainTxtArea").data('fileupload', 'false');
    },
    onUploadSuccess: (id, data) => {
        data = JSON.parse(data.trim());
        completeSubmitExplanation(data.status);
    },
    onUploadError: (id, xhr, status, errorThrown) => {
        completeSubmitExplanation(errorThrown);
    }
});
$("#explainTxtArea").keyup(() => {
    var value = $("#explainTxtArea").val();

    if (value.length < 10) {
        $("#txtOutput").html("<b>Note:</b> Required of minimum 10 characters.");
        $("#btn-subjectExplanation").prop('disabled', true);
    } else {
        $("#txtOutput").html("");
        if ($("#explainTxtArea").data('fileupload') == 'true' || $("#explainTxtArea").data('fileupload') ==
            'null') {
            $("#btn-subjectExplanation").prop('disabled', false);
        }
    }
})
$("#upload_link").click(e => {
    e.preventDefault();
    $("#input-upload").click();
})

$("#btn-subjectExplanation").click(() => {
    if ($("#explainTxtArea").data('fileupload') == 'true') {
        $('#drag-and-drop-zone').dmUploader("start");
    } else {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Discipline/submitExplanation",
            data: {
                explanation: $("#explainTxtArea").val(),
                fileupload: $("#explainTxtArea").data('fileupload'),
                irid: $("#btn-subjectExplanation").data('irid')
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                completeSubmitExplanation(res.status);
            }
        });
    }
})
const completeSubmitExplanation = (status) => {
    $("#modal-ir-details").data('reloadtable', true);
    $("#modal-ir-details").modal('hide');
    if (status == 'Success') {
        swal('Success', 'Submitted your explanation successfully', 'success');
    } else {
        swal('Failed', 'An error occurred while submitting your explanation', 'error');
    }
}
//------------------NICCA CODES ENDS HERE---------------------------------------------------------------------
//------------------MIC MIC CODES STARTS HERE---------------------------------------------------------------------
$(function(){
    var pathName = window.location.pathname;
    var pathArray = pathName.split('/');
    if (pathArray.length == 5) {
        var portletTab = '#' + pathArray[4];
        $(portletTab).trigger('click');
    }
})
//------------------MIC MIC CODES ENDS HERE---------------------------------------------------------------------

</script>