<style>
.bs-popover-auto[x-placement^=right] .arrow::after, .bs-popover-right .arrow::after {
border-right-color: #282A3C !important;
}
.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
border-left-color: #282A3C !important;
}
.popover-header{
background-color: rgb(52, 55, 78)!important;
border-bottom: 0px !important;
color: white;
max-width: 100% !important;
}
.popover-body{
background-color: #282A3C!important;
max-width: 100% !important;
padding: 1.0rem 0.5rem !important;
text-align: center;
padding-bottom: 1px !important;
}
.list-active{
    background: #dcdcdc;
}

.active-nav-a{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort, .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
    background: unset !important;
}

.m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell{
    background:unset !important;
}
.m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover{
    background:#F0F0F2 !important;
    color: black !important;
}

.codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover{
    font-weight:bold !important;
    color: #6f727d !important;
}

.m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
    color: #8c8c8c;
}

.m-nav.m-nav--inline>.m-nav__item:first-child {
    padding-left: 20px !important;
}

.dms.m-nav.m-nav--inline>.m-nav__item {
    padding: 10px 20px 10px 20px !important;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active {
    background: #d4d7da;
}
.dms.m-nav.m-nav--inline>.m-nav__item:hover {
    background: #ececec;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
    color:#4f668a;
}
.dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
    color: black;
}
.m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
    height: 100%;
    padding: 0.8rem 0 0.8rem 0;
}

.m-portlet .m-portlet__head {
    height: 3.9rem;
}

.m-portlet.m-portlet--tabs.active{
    display:block;
}
.dtrAutoIrSideTab{
    padding-left: 0.5rem !important; 
    padding-right: 0.5rem !important;
    padding-top: 0.8rem !important;
    padding-bottom: 0.8rem !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__clear {
    margin-right: 1px !important;
}


.editableStyle{
    border-bottom: dashed 1px #0088cc;
    cursor: pointer;
}
/* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
/* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

.select2-container--default .select2-selection--single .select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem !important;
    white-space: normal;
}

.select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
    top: 47% !important;
}


</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">DMS Personal</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content ">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--tabs active" id="personalPortlet">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a id="disciplinaryActionTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#myIrs" role="tab">
                                        <i class="fa fa-balance-scale"></i>My IRs</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="offenseCategoryTab" class="nav-link m-tabs__link" data-toggle="tab" href="#myFiledIrs" role="tab">
                                        <i class="fa fa-list-alt"></i>My Filed IRs</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="offenseCategoryTab" class="nav-link m-tabs__link" data-toggle="tab" href="#myDtrViolations" role="tab">
                                        <i class="fa fa-list-alt"></i>My DTR Violations</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a id="offenseTab" class="nav-link m-tabs__link" data-toggle="tab" href="#myIrInvolvement" role="tab">
                                        <i class="fa fa-legal"></i>IR Involvement</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">                   
                        <div class="tab-content">
                            <div class="tab-pane fade show active col-md-12" id="myIrs" role="tabpanel">
                            </div> 
                            <div class="tab-pane fade show active col-md-12" id="myFiledIrs" role="tabpanel">
                            </div> 
                            <div class="tab-pane fade show active col-md-12" id="myDtrViolations" role="tabpanel">
                            </div> 
                            <div class="tab-pane fade show active col-md-12" id="myIrInvolvement" role="tabpanel">
                            </div> 
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->




















<!--end::Modal-->

<script src="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/incrementLetter/incrementLetter.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/cod_settings.js"></script>
