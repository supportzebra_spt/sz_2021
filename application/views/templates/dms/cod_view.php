<style>
    .bs-popover-auto[x-placement^=right] .arrow::after,
    .bs-popover-right .arrow::after {
        border-right-color: #282A3C !important;
    }

    .bs-popover-auto[x-placement^=left] .arrow::after,
    .bs-popover-left .arrow::after {
        border-left-color: #282A3C !important;
    }

    .popover-header {
        background-color: rgb(52, 55, 78) !important;
        border-bottom: 0px !important;
        color: white;
        max-width: 100% !important;
    }

    .popover-body {
        background-color: #282A3C !important;
        max-width: 100% !important;
        padding: 1.0rem 0.5rem !important;
        text-align: center;
        padding-bottom: 1px !important;
    }

    .list-active {
        background: #e8e8e8;
    }

    .active-nav-a {
        font-weight: bold !important;
        color: #313131 !important;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__foot .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort,
    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__head .m-datatable__row>.m-datatable__cell.m-datatable__cell--sort {
        background: unset !important;
    }

    .m-datatable.m-datatable--default.m-datatable--loaded>.m-datatable__table>.m-datatable__body .m-datatable__row>.m-datatable__cell {
        background: unset !important;
    }

    .m-datatable.m-datatable--default>.m-datatable__table>.m-datatable__body .m-datatable__row:hover {
        background: #F0F0F2 !important;
        color: black !important;
    }

    .codOffense.m-nav__item>.m-nav__link>.m-nav__link-text:hover {
        font-weight: bold !important;
        color: #6f727d !important;
    }

    .m-nav.codOffense.m-nav__item>.m-nav__link .m-nav__link-text {
        color: #8c8c8c;
    }

    .m-nav.m-nav--inline>.m-nav__item:first-child {
        padding-left: 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item {
        padding: 10px 20px 10px 20px !important;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active {
        background: #d4d7da;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item:hover {
        background: #ececec;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-icon {
        color: #4f668a;
    }

    .dms.m-nav.m-nav--inline>.m-nav__item.active>.m-nav__link.active>.m-nav__link-text {
        color: black;
    }

    .m-portlet.m-portlet--tabs .m-portlet__head .m-portlet__head-tools .m-tabs-line .m-tabs__link {
        height: 100%;
        padding: 0.8rem 0 0.8rem 0;
    }

    .m-portlet .m-portlet__head {
        height: 3.9rem;
    }

    .m-portlet.m-portlet--tabs.active {
        display: block;
    }

    .dtrAutoIrSideTab {
        padding-left: 0.5rem !important;
        padding-right: 0.5rem !important;
        padding-top: 0.8rem !important;
        padding-bottom: 0.8rem !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__clear {
        margin-right: 1px !important;
    }


    .editableStyle {
        border-bottom: dashed 1px #0088cc;
        cursor: pointer;
    }

    /* li.select2-selection__choice {
    max-width: 100% !important;
    overflow: hidden !important;
    white-space: normal !important; //use this if you want to shorten
} */
    /* ul.select2-selection__rendered {
    padding: 0.65rem 2.9rem 0.8rem 1rem; //overrides select2 style
} */

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding: 0.65rem 2.9rem 0.8rem 1rem !important;
        white-space: normal;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered .select2-selection__clear {
        top: 47% !important;
    }

    .embed_pdf:hover {
        box-shadow: 2px 2px 7px #777;
        z-index: 2;
        -webkit-transition: all 100ms ease-in;
        -webkit-transform: scale(1.01);
        -ms-transition: all 100ms ease-in;
        -ms-transform: scale(1.01);
        -moz-transition: all 100ms ease-in;
        -moz-transform: scale(1.01);
        transition: all 100ms ease-in;
        transform: scale(1.01);
        cursor: pointer;
    }

    .note-toolbar-wrapper {
        height: unset !Important;
    }

    .pdfobject-container {
        width: 100%;
        height: 550px;
    }

    #container-memos tbody tr {
        cursor: pointer;
    }

    #container-memos tbody tr:hover {
        background: #f6fffe;
        color: #34bfa3
    }

    .memo-close {
        z-index: 2000;
        font-size: 30px;
        position: absolute;
        top: 0px;
        right: 5px;
    }

    .memo-close:hover {
        text-shadow: 1px 1px 1px gray;
    }

    .memo-close:focus {
        outline: none !Important;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">COD & DTR Violation Rules</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">DMS Control Panel</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content">
        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
            <div class="m-demo__preview" style="padding:0px !important">
                <ul class="dms m-nav m-nav--inline">
                    <li class="m-nav__item active">
                        <a href="#" class="dmsMainNav m-nav__link active" data-navcontent="discActionCateg">
                            <i class="m-nav__link-icon fa fa-balance-scale"></i>
                            <span class="m-nav__link-text">Code Of Discipline</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsMainNav m-nav__link" data-navcontent="dtrViolation">
                            <i class="m-nav__link-icon fa fa-clock-o"></i>
                            <span class="m-nav__link-text">DTR Violation Rules</span>
                        </a>
                    </li>
                    <li class="m-nav__item">
                        <a href="#" class="dmsMainNav m-nav__link" data-navcontent="tab-memo" id="tab-memo">
                            <i class="m-nav__link-icon fa fa-file-pdf-o"></i>
                            <span class="m-nav__link-text">Memorandums</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet m-portlet--tabs active" id="discActionCateg">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul id="codViewTabs" class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a id="discActionCategTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#discActionCategory" role="tab">
                                <i class="fa fa-list-alt"></i>Disciplinary Action & Category of Offenses</a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a id="offenseViewTab" class="nav-link m-tabs__link" data-toggle="tab" href="#offenseView" role="tab">
                                <i class="fa fa-legal"></i>Offenses</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="discActionCategory" role="tabpanel">
                        <div class="row">
                            <span class="ml-3" style="font-size: 22px; font-weight: 500; color: #5d4747;">Disciplinary Action</span>
                            <div class="col">
                                <hr style="margin-top: 1.2rem !important;">
                            </div>
                        </div>
                        <div class="row mt-3" id="noDiscAction" style="display:none;margin-right: 0px; margin-left: 0px;">
                            <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                <div class="col-md-12 text-center">
                                    <div class="m-demo-icon__preview">
                                        <i style="font-size: 40px;" class="flaticon-notes"></i>
                                    </div>
                                    <span class="m-demo-icon__class">No Disciplinary Action Set</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3" id="withDiscAction">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="">
                                        <thead style="background:#d8d8d8;">
                                            <tr class="d-flex">
                                                <th class="text-center col-5">Action</th>
                                                <th class="text-center col-3">Abbreviation</th>
                                                <th class="text-center col-4" colspan="2">Prescriptive Period (Months)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="discActionTableBody">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12">
                                <p style="font-size: 1.1rem;">A subsequent commision of the same offense during the prescriptive period will stop the running of such prescriptive period for the preceding deviation, and cause the running of the new precriptive period, which will be based on the latest deviation. Habitual deviation will end the effect of prescription. The employeee concerned will be evaluated accordingly.</p>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <span class="ml-3" style="font-size: 22px; font-weight: 500; color: #5d4747;">Category of Offenses</span>
                            <div class="col">
                                <hr style="margin-top: 1.2rem !important;">
                            </div>
                        </div>
                        <div class="row mt-3" id="noCategoryOffense" style="display:none; margin-right: 0px; margin-left: 0px;">
                            <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                                <div class="col-md-12 text-center">
                                    <div class="m-demo-icon__preview">
                                        <i style="font-size: 40px;" class="flaticon-notes"></i>
                                    </div>
                                    <span class="m-demo-icon__class">No Category of Offenses was set</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3" id="withCategoryOffense">
                            <div class="col-12 mt-2">
                                <p style="font-size: 1.1rem;">Offenses shall be classified under <span id="categoryCount"></span> categories depending on gravity, from which the disciplinary action to be imposed shall be based. As a general rule, the disciplinary actions for each category are as follows:</p>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="categoryOffenseTable">
                                        <thead>
                                            <tr class="" style="background:#d8d8d8;border: 3px solid #b5b2b2 !important;">
                                                <th class="text-center align-middle">Category</th>
                                                <th class="text-center align-middle">Offense</th>
                                                <th class="text-center align-middle" colspan="2">Disciplinary Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="categoryOffenseTableBody"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12 mt-2">
                                <p style="font-size: 1.1rem;">Note: All Offenses and disciplinary actions stated here are general in nature; the above stated may be sperseded when an employee is mandated to follow the Policies of his/ her assigned client. Memos on NEW Policies will also supersede simiar list of offenses, unless stated to be suppletory memos.</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="offenseView" role="tabpanel">
                        <div class="row">
                            <div class="col-sm-8">
                                <h3 class="" style="color:#4e4e4e;">List of Offenses</h3>
                            </div>
                            <div class="col-sm-4" id="natureOfOffenseTypeContainer" style="display:none">
                                <select class="custom-select form-control" id="natureOfOffenseType" style="border: 1px solid #807b7b;">
                                    <option value="1">General Offenses</option>
                                    <option value="2">QIP Offenses</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mr-1" id="generalOffenseDiv" style="margin-right: -15px !important;">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="padding-top: 2rem;border-right: 1px solid #dedede;">
                                <div class="m-demo__preview" id="offenseType" style="margin-left: -11px;">
                                    <div class="form-group m-form__group" id="searchOffenseCategory" style="margin-top: -2rem;padding-top: 0.8rem;margin-left: -0.4rem;padding-left: 0.7rem;margin-right: -1.1rem;padding-right: 0.7rem;padding-bottom: 0.8rem;">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Nature of Offense" id="seachNatureOffenseVal">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-search"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="m-nav" id="specOffenseCategoryList" style="margin-left: 0.2rem;margin-right: -1.2rem;margin-top: -1rem;display:none">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8 pl-4">
                                <div class="row mt-4" id="noNatureOffense" style="display:none; margin-right: 0px;margin-left: 10px;height: 89%;">
                                    <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;display: flex;align-items:center;">
                                        <div class="col-md-12 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 8rem;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class" id="noNatureOffenseVal" style="font-size: 2;word-break: break-word;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4" id="natureOffenseDetails">
                                    <div class="col-12">
                                        <h2 class="text-center" id="natureOfOffenseLabel" style="word-wrap: break-word;"></h2>
                                    </div>
                                    <div class="col-12">
                                        <hr style="margin-top: 0.2rem !important;">
                                    </div>
                                </div>
                                <div class="row" id="noOffenses" style="display:none; margin-right: 0px; margin-left: 0px;height: 80%;">
                                    <div class="col-md-12 p-0" style="display: flex;align-items:center;">
                                        <div class="col-md-12 p-0 text-center">
                                            <div class="m-demo-icon__preview">
                                                <i style="font-size: 8rem;" class="flaticon-notes"></i>
                                            </div>
                                            <span class="m-demo-icon__class" style="font-size: 2rem;">No Offenses was set</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="withOffense" style="display:none">
                                    <div class="col-lg-12 input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Specific Offense" id="offenseSearchField">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div id="offenseDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="m-portlet m-portlet--tabs" id="dtrViolation" style="display:none">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-tools">
                    <ul id="dtrVioTabs" class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a id="discActionCategTab" class="nav-link m-tabs__link active" data-toggle="tab" href="#dtrVioRules" role="tab">
                                <i class="fa fa-list-alt"></i>DTR Violation Rules</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="dtrVioRules" role="tabpanel">
                        <div class="row" id="dtrAutIrContainer">
                            <div class="col-2 col-sm-5 col-md-5 col-lg-4" style="margin-top: -2.3rem !important;margin-left: -1.2rem !important; margin-bottom: -2.2rem !important;padding-top: 1rem;border-right: 1px solid #dedede;background: #c1c2c73d;">
                                <div class="m-demo__preview" id="offenseType" style="margin-left: -11px;">
                                    <ul class="m-nav" id="dtrViolations" style="margin-left: -0.3rem;margin-right: -1.2rem;margin-top: -1rem;">
                                        <li class="dtrAutoIrSideTab m-nav__item list-active" data-violationtypeid="1" style="cursor: pointer;">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon active-nav-a text-center text-sm-left">
                                                    <i class="fa fa-bell" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Late"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block active-nav-a pl-3 pt-2" style="padding-top: 0.3rem;padding-bottom: 0.1rem;">Late</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="4" style="cursor: pointer;">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-tachometer" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Over Break"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Over Break</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="2" style="cursor: pointer;">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-hourglass-3" style="font-size: 2.0rem;" data-toggle="m-tooltip" data-original-title="Undertime"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">UnderTime</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="3" style="cursor: pointer;">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-unlock-alt" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Late Clockout"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Late Clockout</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="6" style="cursor: pointer;">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-history" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Incomplete Break"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Incomplete Break</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="7" style="cursor: pointer;">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-clock-o" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Incomplete DTR Logs"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Incomplete DTR Logs</span>
                                            </a>
                                        </li>
                                        <li class="dtrAutoIrSideTab m-nav__item" data-violationtypeid="5" style="cursor: pointer;">
                                            <a class="m-nav__link pl-sm-3" style="">
                                                <span class="m-nav__link-icon text-center text-sm-left">
                                                    <i class="fa fa-user-times" style="font-size: 2.6rem;" data-toggle="m-tooltip" data-original-title="Absent"></i>
                                                </span>
                                                <span class="m-nav__link-text d-none d-sm-block pl-3 pt-2">Absent</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-10 col-sm-7 col-md-7 col-lg-8" id="dtrVioAutIrContainer" style="margin-top: -2.3rem; padding-top: 2rem; margin-bottom: -1rem; padding-left: 1rem !important; padding-right: 1rem !important;margin-left: 1rem !important;">
                                <div class="row">
                                    <div class="col-12 mb-3">
                                        <h2 class="text-center" style="word-wrap: break-word;"><span><i id="dtrVioIcon" class="fa fa-user-times" style="font-size: 2rem; color: #00b0ff;"></i></span> IR Rules for <span id="dtrVioTypeLabel"></span></h2>
                                    </div>
                                    <div class="col-12">
                                        <hr style="margin-top: 0.2rem !important;">
                                    </div>
                                    <div class="col-12 mt-3" id="dtrVioIrRule">
                                        <!-- <p class="" style="font-size: 1.1rem;text-indent: 3rem;">An <b>Incident Report</b> (IR) with a Nature of Offense "A.Attendance/Panctuality" and a Category B offense of "Tardiness" will be automatically prepared by the system for filing if one has committed a <b>Late</b> violation of the following circumstances:</p>
                                        <ul style="font-size: 1.1rem;">
                                            <li>One-time 15 minutes or up </li>
                                            <li>3 consecutive days regardless on the number of minutes</li>
                                            <li>Accumulated 15 minutes</li>
                                        </ul> -->
                                        <!-- FIXED -->
                                        <!-- <p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on a monthly basis, and shall be refreshed on the following month</p>
                                        <p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on a weekly basis, and shall be refreshed on the following week</p>
                                        <p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on 2 months basis, and shall be refreshed on the following month</p> -->
                                        <!-- custom duration -->
                                        <!-- <p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on a 10 days basis starting from the date of commission, and shall be refreshed a day after the 10th day.</p> -->
                                        <!-- COD -->
                                        <!-- <p class="" style="font-size: 1.1rem;">Counting of instance/occurences are based on the prescriptive period stated the Code of Discipine.</p> -->


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet" style="display:none" id="content-memo">
            <div class="m-portlet__head" style="background: #2c2e3eeb;padding: 0 1.9rem !important;">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h5 class="m-portlet__head-text" style="font-size: 22px; font-weight: 500; color: #34bfa3 !important;border-bottom: 4px solid #34bfa3;top:2px;left:0;position:relative !important;font-weight:400 !important;    font-size: 16px !important">
                            <i class="fa fa-file-pdf-o"></i> Memorandums
                        </h5>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-md-5">
                        <h4>Memorandums</h4>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control m-input  form-control-sm" placeholder="Search Memos" id="search-memo" title="Search Input">
                    </div>
                    <div class="col-md-2">
                        <select name="" id="order-memo" class="form-control form-control-sm" title="Order Field">
                            <option value="title">Subject</option>
                            <option value="number">Memo No.</option>
                            <option value="memoDate">Date</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="" id="orderType-memo" class="form-control form-control-sm" title="Order Type">
                            <option value="ASC">ASC</option>
                            <option value="DESC">DESC</option>
                        </select>
                    </div>
                </div>
                <hr>
                <!-- <div id="container-memos" class="row no-gutters">

                </div> -->
                <div class="table-responsive">
                    <table id="container-memos" class="table table-hover table-bordered">
                        <thead style="background:#d8d8d8;font-size:14px">
                            <th colspan=2 class="text-center">Subject</th>
                            <th class="text-center">Memorandum No.</th>
                            <th class="text-center">Date</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-memo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close memo-close" style="color:#46494C" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div id="container-pdf"></div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/src/custom/js/dms/cod_view.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/pdfobject.js"></script>
<script>
    //NICCA ARIQUE FOR MEMO
    function getAllMemos() {
        var searchMemo = $("#search-memo").val();
        var orderMemo = $("#order-memo").val();
        var orderTypeMemo = $("#orderType-memo").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Discipline/get_all_memos",
            data: {
                searchMemo: searchMemo,
                orderMemo: orderMemo,
                orderTypeMemo: orderTypeMemo
            },
            beforeSend: function() {
                mApp.block('.m-content', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#container-memos tbody").html("");

                $.each(res.memos, function(i, memo) {
                    // var string = '<div class="col-6"><div class="card m-2"  data-toggle="modal" data-target="#modal-memo" data-link="<?php echo base_url(); ?>' + memo.link + '">\
                    //             <div class="card-body">\
                    //                 <div class="m-stack m-stack--ver m-stack--general">\
                    //                     <div class="m-stack__item m-stack__item--center m-stack__item--middle  m--font-danger" style="width: 40px;">\
                    //                         <i class="fa fa-file-pdf-o" style="font-size:35px"></i>\
                    //                     </div>\
                    //                     <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--fluid pl-3"><p class="mb-0 m--font-bolder text-truncate" style="font-size:13px;">' + memo.title + '</p><p class="mb-0 m--font-bold" style="font-size:11px">' + memo.number + '</p></div>\
                    //                 </div>\
                    //             </div>\
                    //         </div>\
                    //         </div>';
                    var string = '<tr data-toggle="modal" data-target="#modal-memo" data-link="<?php echo base_url(); ?>' + memo.link + '">\
                    <td style="vertical-align: middle;" class="py-0 text-center"><i class="fa fa-file-pdf-o" style="font-size:16px;color:#DE1D24"></i></td>\
                    <td style="vertical-align: middle;" ><p class="mb-0 m--font-bolder memo-title" style="font-size:14px;">' + memo.title + '</p></td>\
                    <td style="vertical-align: middle;"  class="text-center"><p class="mb-0 m--font-bold" style="font-size:14px">' + memo.number + '</p></td>\
                    <td style="vertical-align: middle;"  class="text-center"><p class="mb-0 m--font-bold" style="font-size:14px">' + moment(memo.memoDate).format('MMM DD, YYYY') + '</p></td>\
                    </tr>';
                    $("#container-memos tbody").append(string);
                })
                mApp.unblock('.m-content');
            },
            error: function() {
                swal("Error", "Please contact admin", "error");
                mApp.unblock('#content');
            }
        });
    }
    // $(function() {
        
    // })
    $("#search-memo").on('keyup', function() {
        getAllMemos();
    })
    $("#order-memo,#orderType-memo").on('change', function() {
        getAllMemos();
    })
    $("#tab-memo").click(function() {
        $("#content-memo").show();
    });
    $("#modal-memo").on('show.bs.modal', function(e) {
        if (e.namespace === 'bs.modal') {
            var link = $(e.relatedTarget).data('link');
            var options = {
                pdfOpenParams: {
                    navpanes: 0,
                    toolbar: 0,
                    statusbar: 0,
                    page: 1,
                    view: "FitH"
                }
            };
            var myPDF = PDFObject.embed(link, "#container-pdf", options);
            console.log((myPDF) ? "PDFObject successfully embedded '" + link + "'!" : "Uh-oh, the embed didn't work.");
        }
    });
</script>