<style>
#tblTitoRqDetails tr td{
	padding: 5px;
}
.arrDataClass tr > td {
    padding: 13px;
    text-align: center;
}
div#containerData {
    width: 80%;
    margin: 0 auto;
}
#container {
	height: 400px; 
	margin: 0 auto;
}
text.highcharts-credits {
    display: none;
}
.highcharts-figure, .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}

</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Time-in & Time-out Report</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Time-in & Time-out Approval</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	  <!--begin: Survey Answers -->
 

<div class="m-content">
		 <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                 
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i> Records</a>
                                </li>
                            </ul>
                        </div>
                    </div> 
			
            <div class="m-portlet__body">
                   
                 
               
                <div class="tab-content">
                  
                   
                    <div class="tab-pane active" id="m_tabs_6_3" role="tabpanel">
					<div class="m-portlet__head-tools">
										<div class="row">
                                            <!-- <div class="row"> -->
											 <div class="col-sm-5" data-select2-id="6">
												 <div class="m-form m-form--label-align-right">
														<div class="m-input-icon m-input-icon--left">
															<input type="text" class="form-control" id="m_daterangepicker_1" readonly placeholder="Select time" type="text" style="color: #00c5dc;background: #fff;    font-weight: 700;text-align: center;">
															<span class="m-input-icon__icon m-input-icon__icon--left">
																<span>
																	<i class="la la-calendar"></i>
																</span>
															</span>
														</div>
													</div>
												
                                                </div>
												 <div class="col-sm-2" data-select2-id="6">
												 <div class="m-form m-form--label-align-right">
														<div class="m-input-icon m-input-icon--left">
															<select class="custom-select form-control" data-toggle="m-tooltip" title="" data-original-title="Employee Type" id="empType">
															<option> All</option>
															<option> Admin</option>
															<option> Agent</option>
															</select>
														</div>
													</div>
												
                                                </div>
                                                <div class="col-sm-5">
                                                   <div class="m-form m-form--label-align-right">
														<div class="m-input-icon m-input-icon--left">
															<input type="text" class="form-control m-input" placeholder="Search using TITO-ID, First name, or Lastname.." id="searchRecord">
															<span class="m-input-icon__icon m-input-icon__icon--left">
																<span>
																	<i class="la la-search"></i>
																</span>
															</span>
														</div>
													</div>
                                                </div>
                                                
                                               
                                                
                                            <!-- </div> -->
                                        </div>

					
                            
                    </div> 
					<br>
					<div class="card card-custom">
									<!--begin::Header-->
									<div class="card-header card-header-tabs-line">
										<ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
											<li class="nav-item">
												<a class="nav-link active" data-toggle="tab" href="#kt_builder_themes">General</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#kt_builder_page">Raw</a>
											</li> 
											<li class="nav-item" style="position: absolute;right: 35px;">
													<button type="button" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn-sm" id="btnExportExcel" data-date1="" data-time1="" data-date2="" data-time2="" disabled="true"><i class="fa fa-file-excel-o"></i> Export </button>
												
											</li> 
										</ul>
									</div>
									<!--end::Header-->
									<!--begin::Form-->
									<form class="form" method="POST">
										<!--begin::Body-->
										<div class="card-body">
											<div class="tab-content pt-3">
												<!--begin::Tab Pane-->
												<div class="tab-pane active" id="kt_builder_themes">
													 <div class="form-group row">
														    <div id="container"></div>
														    <div id="containerData"></div>
													</div>
												</div>
												<!--end::Tab Pane-->
												<!--begin::Tab Pane-->
												<div class="tab-pane" id="kt_builder_page">
													<div class="form-group ">
														  <div class="m_datatable_record"></div>
													</div>
												</div>
												<!--end::Tab Pane-->
											</div>
										</div>
										<!--end::Body-->
									</form>
									<!--end::Form-->
								</div>
					 
                    </div>
                </div>   
 
            </div> 
        </div>
		</div>
    <!--end: Survey Answers -->
</div>
<div class="modal fade" id="myModalTITOrequest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Time-in and Time-out Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <div class="m-scrollable m-form m-form--fit m-form--label-align-right" >
                   <div id="titoRequestDiv"></div>
				   <hr>
				   <div class="form-form-group m-form__group" id="showDetailsTito" data-dtrrequestid="">
				  
					</div>
					<div class="form-group m-form__group divActOnly" style="padding: 7px;">
						<label for="exampleTextarea">Comment <span id="showerror" style="font-size: 13px;font-weight: 400;color:red;"></span></label>
						<textarea class="form-control m-input" rows="3"  id="approveComment" maxlength="100"></textarea>
						
						<span class="character-remaining" style="font-size: 13px;font-weight: 400;"></span>
					</div>
				  
            </div>
      </div>
      <div class="modal-footer divActOnly">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<div class="dropdown">
			<button class="btn btn-brand btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				Action
			</button>
			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" href="#" onclick="titoApproveFunc(5)"> <i class="fa fa-thumbs-o-up text-success"></i> Approve</a>
				<a class="dropdown-item" href="#" onclick="titoApproveFunc(6)"> <i class="fa fa-thumbs-o-down text-danger"></i> Disapprove</a>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
	<div id="showDetails" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" aria-hidden="true">
	  <div class="modal-dialog" role="document">

		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
		   <h4 class="modal-title" id="headerTitle"></h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		   
		  </div>
		  <div class="modal-body">
			  <div id="bodyShowDetails">
			  </div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>

	  </div>
	</div>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-3d.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>
	<script type="text/javascript">
	var imagePath = baseUrl + '/assets/images/';
 
	function titoFunc(id,emp_id,type){
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/checkTitoRequest'); ?>",
			data: {dtrRequest_ID:id,emp_id:emp_id},
			cache: false,
			success: function (output) {
				var rs = JSON.parse(output);
				if(output!="0"){
				$("#showDetailsTito").data("dtrrequestid",rs["titoDetailsqry"][0]["dtrRequest_ID"]);
					var str = "";
					str += '<div class="m-card-user m-card-user--skin-dark">';
						str	+= '<div class="m-card-user__pic"><img src='+imagePath+rs["titoDetailsqry"][0]["pic"]+' onerror="noImage('+rs["titoDetailsqry"][0]["userId"]+')" class="m--img-rounded m--marginless header-userpic'+rs["titoDetailsqry"][0]["userId"]+'"></div>';
						str += '<div class="m-card-user__details"><span class="m-card-user__name m--font-weight-500 user-name" style="color: #000;">'+rs["titoDetailsqry"][0]["fname"]+' '+rs["titoDetailsqry"][0]["lname"]+' </span><span href="#" class="m-card-user__email m--font-weight-300 m-link user-role" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+rs["empPosQry"][0]["pos_details"]+' - '+rs["empPosQry"][0]["status"]+'</span><span href="#" class="m-card-user__email" style="font-size: smaller;">'+moment(rs["titoDetailsqry"][0]["dateCreated"]).format('MMMM Do YYYY, h:mm:ss a')+'</span></div>';
 					str	+= '</div>';
 					 
  					$("#titoRequestDiv").html(str);
  					var data = "";
					data +='<table style="width: 100%;text-transform: capitalize;" id="tblTitoRqDetails">'+
					'<tr><td> <b>Date: </b></td><td>'+moment(rs["titoDetailsqry"][0]["dateRequest"]).format('MMMM D, YYYY')+'</td></tr>'+
					'<tr><td><b>Schedule: </b></td><td>'+rs["titoDetailsqry"][0]["time_start"]+" - "+rs["titoDetailsqry"][0]["time_end"]+'</td></tr>'+
					'<tr><td><b>Start Date & Time: </b></td><td>'+rs["titoDetailsqry"][0]["startDateWorked"]+" "+rs["titoDetailsqry"][0]["startTimeWorked"]+'</td></tr>'+
					'<tr><td><b>End Date & Time:</b> </td><td>'+rs["titoDetailsqry"][0]["endDateWorked"]+" "+rs["titoDetailsqry"][0]["endTimeWorked"]+'</td></tr>'+
					'<tr><td><b>Total: </b></td><td>'+rs["titoDetailsqry"][0]["timetotal"]+' hr.</td></tr>'+
					'<tr><td><b>Status: </b></td><td>'+rs["titoDetailsqry"][0]["description"]+'</td></tr>'+
					'<tr><td><b>Message: </b></td><td><blockquote class="ahr_quote" style="padding:10px;"><i class="la la-quote-left"></i>'+rs["titoDetailsqry"][0]["message"]+'<i class="la la-quote-right"></i></blockquote></td></tr>'+
					'</table>';
					
					
					$("#showDetailsTito").html(data);
					if(type=="view"){
						$(".divActOnly").hide();
					}else{
						$(".divActOnly").show();
						
					}
					$("#approveComment").val("");
					$("#showerror").text("");
					$(".character-remaining").text("");
					$("#approveComment").css("border","1px solid #716aca");					

					$("#myModalTITOrequest").modal("show");
				}else{
					alert("error");
				}
				
				
			}
		 });

	}
 

function chart(date){
        var empType = $("#empType").val();

	$.ajax({
			type: "POST",
			url: baseUrl + '/Dtrreport/listTitoRequestRecordChart/hr',
			data: {
				date: date,
				empType:empType
			},
			cache: false,
			success: function(json) { 
				var rs = JSON.parse(json);
							
				var arr = new Array();
				var arrData = "<table class='arrDataClass' style='width: 100%;'>";
				arrData +="<tr style='background: #3d3e4d;color: #fff;'><td>Type</td><td>Account</td><td># Request(s)</td><td># Employee Filed</td><td>Total Population</td></tr>";

				$.each(rs, function(key, obj) {
					$.each(obj, function(key1, obj1) {
						var totalEmp = "";
						 var uniqueArray = [];
						$.each(obj1, function(key2, obj2) {
							console.log("aa == "+uniqueArray.indexOf(obj2));
							 if(uniqueArray.indexOf(obj2.emp_id) === -1) {
							uniqueArray.push(obj2.emp_id);
							}
						}); 
						
						var color = (key=="Admin") ? "#e7fcff" : "#eaedf3";
						arrData +="<tr style='background:"+color+"'><td>"+key+"</td><td>"+key1+"</td><td>"+obj1.length+"</td><td><b>"+uniqueArray.length+"</b> <br> <small>("+parseFloat((uniqueArray.length/obj1[0].total)*100).toFixed(2)+"%)</small></td><td>"+obj1[0].total+"</td> </tr>";
 						arr.push({
							name: key1,
							y: parseFloat(obj1.length)
						});
					});
				});
				if(arr.length==0){
				arrData +="<tr style='color: red;'><td colspan=3>No Data</td></tr>";	
				}
				arrData +="</table>";
				 $("#containerData").html(arrData);
				Highcharts.chart('container', {
					chart: {
						type: 'pie',
						options3d: {
							enabled: true,
							alpha: 45 
						}
					},
					title: {
						text: 'Number of TITO Requests'
					},
					subtitle: {
						text: date
					},
					plotOptions: {
						 pie: {
                        innerSize: 65,
                        depth: 40,
                        dataLabels: {
                            distance: 5,
							enabled: true,
							        formatter: function() {
								  return this.key + '<br># of Requests: ' + this.y;
								}
                        }
                    }
					},
					series: [{
						name: 'Number of Requests',
						data: arr
					}]
				});
			}
	});
	
	
	
}
function RecordDatatable(date){
	
	
        var user_id = <?php echo $_SESSION["uid"]; ?>;
        var empType = $("#empType").val();
		
          var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/Dtrreport/listTitoRequestRecord/hr',
                        headers: {
                            'x-my-custom-header': 'some value', 
                            'x-test-header': 'the value'
                        },
                        params: {
                            query: {
                                user_id: user_id,
                                date: date,
                                type: empType,
                             },
                        },
                    }
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: '',
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false // display/hide footer
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#searchRecord'),
            },
            columns: [{
                field: "dtrRequest_ID",
                title: "TITO ID",
                width: 80,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                    var ahrId = row.dtrRequest_ID.padLeft(8);
                    var html = "<td class = 'col-md-2'>" + ahrId + "</td>";
                    return html;
                }
            },{
                field: "",
                title: "FULLNAME",
                width: 150,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<td class = 'col-md-12'><b>" +  row.fname+" "+row.lname + "</b> <br><small>"+row.acc_name+"</small></td>";
                    return html;
                }
            },{
                field: "dateCreated",
                title: "DATE REQUESTED",
                width: 150,
                selector: false,
                sortable: 'asc',
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<td class = 'col-md-12'>" + moment(row.dateCreated, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y hh:mm:ss A') + "</td>";
                    return html;
                }
            },{
                field: "time_start",
                title: "REQUESTED SCHEDULE",
                width: 150,
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
                     var html = "<td class = 'col-md-12'>" +moment(row.dateRequest, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y') +" <br>"+row.time_start+" to "+row.time_end + "</td>";
                    return html;
                }
            },{
                field: "requeststatus",
                title: "STATUS",
                width: 100,
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    // console.log(row.additionalHourRequestId.padLeft(8));
					if (row.approverstatus == "approved") {
                        var badgeColor = 'btn-success';
                    } else  {
                        var badgeColor = 'btn-danger';
                    } 
                    var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide' onclick=viewRecord('approver',"+row.dtrRequest_ID+")>" + row.approverstatus + "</span>";
                    return html;
                  
                }
            },{
                field: 'action',
                width: 100,
                title: 'ACTION',
                overflow: 'visible',
                sortable: false,
                textAlign: 'center',
                template: function(row, index, datatable) {
                    var html = '<a href="#" class="btn btn-outline-accent  m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="Edit details" onclick=titoFunc('+row.dtrRequest_ID+','+row.emp_id+',"view")><i class="fa fa-search"></i></a>';
                    return html;
                }
            }

            ],
        };

        $('.m_datatable_record').mDatatable(options);
}

function viewRecord(type,id){
	// alert(type+" "+id);
	if(type=="record"){
		viewModalRecord(id);
	}else{
		viewModalApprover(id);
	}
	
	
}
function viewModalRecord(id){
	var imagePath = baseUrl + '/assets/images/';
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/viewRecordTito'); ?>",
			data: {dtrRequest_ID:id},
			cache: false,
			success: function (output) {
				var rs = JSON.parse(output);
				 var str = "";
					str +='<div class="m-scrollable m-form m-form--fit m-form--label-align-right" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">';
					str += '<div class="m-card-user m-card-user--skin-dark">';
						str	+= '<div class="m-card-user__pic"><img src='+imagePath+rs["titoDetailsqry"][0]["pic"]+' onerror="noImage('+rs["titoDetailsqry"][0]["userId"]+')" class="m--img-rounded m--marginless header-userpic'+rs["titoDetailsqry"][0]["userId"]+'"></div>';
						str += '<div class="m-card-user__details"><span class="m-card-user__name m--font-weight-500 user-name" style="color: #000;">'+rs["titoDetailsqry"][0]["fname"]+' '+rs["titoDetailsqry"][0]["lname"]+' </span><span href="#" class="m-card-user__email m--font-weight-300 m-link user-role" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+rs["empPosQry"][0]["pos_details"]+' - '+rs["empPosQry"][0]["status"]+'</span><span href="#" class="m-card-user__email" style="font-size: smaller;">'+moment(rs["titoDetailsqry"][0]["dateCreated"]).format('MMMM Do YYYY, h:mm:ss a')+'</span></div>';
 					str	+= '</div>';
					str +='<hr>';
					str +='<div class="form-form-group m-form__group">';
					str +='<table style="width: 100%;" id="tblTitoRqDetails">';
					str +='<tr><td>Date: </td><td>'+rs["titoDetailsqry"][0]["dateRequest"]+'</td></tr>';
					str +='<tr><td>Schedule: </td><td>'+rs["titoDetailsqry"][0]["time_start"]+" - "+rs["titoDetailsqry"][0]["time_end"]+'</td></tr>';
					str +='<tr><td>Start Date & Time: </td><td>'+rs["titoDetailsqry"][0]["startDateWorked"]+" "+rs["titoDetailsqry"][0]["startTimeWorked"]+'</td></tr>';
					str +='<tr><td>End Date & Time: </td><td>'+rs["titoDetailsqry"][0]["endDateWorked"]+" "+rs["titoDetailsqry"][0]["endTimeWorked"]+' </td></tr>';
					str +='<tr><td>Total hours: </td><td>'+rs["titoDetailsqry"][0]["timetotal"]+' hr.  </td></tr>';

					str +='<tr><td>Message: </td><td><blockquote style="margin: 7px;"><i class="la la-quote-left"></i><span class="mb-0 text-capitalize" id="reasonAppr">'+rs["titoDetailsqry"][0]["message"]+'</span><i class="la la-quote-right"></i></blockquote></td></tr>';
					str +='</table>';
					str +='</div>';
					str +='</div>';
			$("#bodyShowDetails").html(str);
			$("#headerTitle").html("Request Detail");	
			}
		 });
	$("#showDetails").modal("show");
}
function viewModalApprover(id){
	 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/viewApproverStatus'); ?>",
			data: {dtrRequest_ID:id},
			cache: false,
			success: function (output) {
				var rs = JSON.parse(output);
				var str = "";
				 $.each(rs, function (index, value) {
						var icon = "";
						var color = "";
						var animate = "";
						var animateI = "";
						var current = "";
						var stat = "";
						
					  if(value.remarks == null || value.remarks == " "){
							var note = "No Notes found.";
						}else{
							var note =  value.remarks;
						}
						if (parseInt(value.approvalStatus_ID) == 5) {
							icon = "fa fa-thumbs-o-up";
							color = "btn-success";
							stat = value.description;
						} else if (parseInt(value.approvalStatus_ID) == 6 || parseInt(value.approvalStatus_ID) == 7) {
							icon = "fa fa-thumbs-o-down";
							color = "btn-danger";
							stat = value.description;
 						} else if ((parseInt(value.approvalStatus_ID) == 2) || (parseInt(value.approvalStatus_ID) == 4)) {
							icon = "fa fa-spinner";
							color = "btn-info";
							animate = "fa-spin";
							stat = "pending";
							if (value.approvalStatus_ID == 4){
								current = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink ml-2" style="min-height: 7px;min-width: 7px;"></span>';
							}else{
								current = '';
							}
						} else if (parseInt(value.approvalStatus_ID) == 12){
							icon = "fa fa-exclamation-circle";
							color = "btn-warning";
							animateI = "m-animate-blink";
							stat = value.description;
						}
					   str += '<div class="m-portlet bg-secondary">' +
						'<div class="col-md-12 pt-2 pb-2" style="background: #505a6b30;">' +
						'<div class="row">' +
						'<div class="col-7 col-sm-7 col-md-7 pt-2">APPROVER <span>' + value.approvalLevel + '</span>'+current+'</div>' +
						'<div class="col-5 col-sm-5 col-md-5">' +
						'<div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">' +
						'<div class="btn '+color+' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+animate+'" style="width: 25px !important;height: 25px !important;">' +
						'<i class="' + icon + ' '+animateI+'" style="font-size: 17 px;"></i>' +
						'</div>' +
						'<span class="button-content text-light text-capitalize pl-2">' + stat + '</span>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>  ' +
						'<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">' +
						'<div class="row">' +
						'<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block">' +
						'<img class="rounded-circle" src="<?php echo base_url(); ?>/assets/images/'+value.pic+'"  onerror="noImage('+value.uid+')" width="58" alt="" class="mx-auto">' +
						'</div>' +
						'<div class="col-12 col-sm-10 col-md-10 pl-1">' +
						'<div class="col-md-12 font-weight-bold" style="font-size: 15px;">'+value.fname+' '+value.lname+'</div>' +
						'<div class="col-md-12" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">'+value.position+'</div>' +
						'<span class="col-md-12" style="font-size: 12px;">'+moment(value.dateTimeStatus, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A')+'</span>' +
						'<div class="col-md-12 mt-3" style="font-size: 12px;"><i> - '+note+'</i>' +
						'</div>' +
						'</div>' +
						'<div class="col-md-12 pt-3">' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>'; 
				 });
				
     
 			$("#bodyShowDetails").html(str);	
 			$("#headerTitle").html("Approver");	
			} 
		 });
	$("#showDetails").modal("show");
}
function onChangeHandler(){
		var date= $('#m_daterangepicker_1').val();
		$('.m_datatable_record').mDatatable('destroy');
		RecordDatatable(date);
		chart(date);
		 $("#btnExportExcel").attr("disabled",false);
}
$(document).ready(function () {
	 
	 $('#m_daterangepicker_1').daterangepicker({
			timePicker: true,
			startDate: moment().startOf('hour'),
			endDate: moment().startOf('hour'),
			locale: {
			  format: 'Y/MM/DD hh:mm A'
			},
	}, function(start, end, label) {
		  var date= start.format('MM/DD/YYYY hh:mm A') + ' - ' + end.format('MM/DD/YYYY hh:mm A');
		  $("#btnExportExcel").data("data-date1",start.format('YYYY-MM-DD'));
		  $("#btnExportExcel").data("data-time1",start.format('hh:mm-A'));
		  $("#btnExportExcel").data("data-date2",end.format('YYYY-MM-DD'));
		  $("#btnExportExcel").data("data-time2",end.format('hh:mm-A'));
		  $('.m_datatable_record').mDatatable('destroy');
		  RecordDatatable(date);
		  chart(date);
		  $("#btnExportExcel").attr("disabled",false);
	  });  
 	// var date= $('#m_daterangepicker_1').val();
     // RecordDatatable(date);
     // chart(date);
 	 onChangeHandler();
	$('#empType').change(function (){ 
		onChangeHandler();
 	});
	$('#mySelectSched').select2({ 
			placeholder: "Select your schedule.",
			width: '100%'
	 });
	 	$("#btnExportExcel").click(function(){
			var d1 = $("#btnExportExcel").data("data-date1");
			var t1 = $("#btnExportExcel").data("data-time1");
			var d2 = $("#btnExportExcel").data("data-date2");
			var t2 = $("#btnExportExcel").data("data-time2");
			var empType = $("#empType").val();
			//alert(d1+" "+t1+" "+d2+" "+t1);
				if(d1!=null){
					window.location =  "<?php echo base_url(); ?>/dtrreport/download_tito_report/"+empType+"/"+d1+"/"+t1+"/"+d2+"/"+t2;
				}else{
					swal("Error!", "Please change the date and time details.", "error")
				}
				
			

		});
});

 
</script>