
<style>
.blockOverlay{
    opacity: 0.35 !important;
    background-color: #d4d4d4 !important;
}
text.highcharts-credits {
    display: none;
}
tr:hover {
    background: #fff8b8 !important;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Attendance Report</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">DTR</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			 <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist" style="border: 0;">
                                 <li class="nav-item m-tabs__item" hidden>
                                    <a class="nav-link m-tabs__link " data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                        <i class="fa fa-refresh"></i>Graphical Report</a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_3" role="tab">
                                        <i class="la la-th-list"></i>Attendance Report</a>
                                </li>
							
                            </ul>
                        </div>
                    </div>
			
            <div class="m-portlet__body">                     
                <div class="tab-content">
                    <div class="tab-pane " id="m_tabs_6_1" role="tabpanel" hidden>
                        <div class="row" id="filters" >
                            <div class="col-lg-12 col-md-12">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview" style="padding:10px">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <label for="">Dates</label><br />
                                                <div class="m-input-icon m-input-icon--left" id="search-dates-graph">
                                                    <input type="text" class="form-control m-input" id="search_daterange_graph" placeholder="Date Range" readonly>
                                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Class</label><br />
                                                <select class="form-control m-input" id="search-class-graph">
                                                    <option value="ALL">All</option>
                                                    <option value="Admin">SZ Team</option>
                                                    <option value="Agent">Ambassador</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Accounts</label><br />
                                                <select class="form-control m-input" id="search-accounts-graph" >
                                                </select>
                                            </div>
                                        </div>
										<br>
                                        <div class="row">
										 <div class="col-md-4" id="dataemployeegraph" data-employeegraph="">
                                              <button type="button" class="btn m-btn--pill m-btn--air btn-primary btn-sm" id="btnShowLogsGraph"><i class="la la-search"></i> Show</button>
                                          </div>
											

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="">
							<div class="graphreport">
							<div class="row">
										<div id="div-graph1" class="col-md-12" style="margin-top: 48px;border: 1px solid #c3c3c3;"></div>
											
 										<div id="div-graph2"  class="col-md-12" style="margin-top: 48px;border: 1px solid #c3c3c3;"></div>
                             </div>
                              </div>
							
						</div>
                    </div>
                   
                    <div class="tab-pane active" id="m_tabs_6_3" role="tabpanel">
                         <div class="row" id="filters" >
                            <div class="col-lg-12 col-md-12">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview" style="padding:10px">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="">Dates</label><br />
                                                <div class="m-input-icon m-input-icon--left" id="search-dates">
                                                    <input type="text" class="form-control m-input" id="search_daterange" placeholder="Date Range" readonly>
                                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-calendar"></i></span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Class</label><br />
                                                <select class="form-control m-input" id="search-class">
                                                    <option value="ALL">All</option>
                                                    <option value="Admin">SZ Team</option>
                                                    <option value="Agent">Ambassador</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Accounts</label><br />
                                                <select class="form-control m-input" id="search-accounts" onchange="onChangeData()" >
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="">Accounts</label><br />
                                                <select class="form-control m-input" id="search-isActive" onchange="onChangeData()">
													<option value="yes">Active</option>
													<option value="no">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
										<br>
                                        <div class="row">
										 <div class="col-md-4" id="dataemployee" data-employee="">
                                              <button type="button" class="btn m-btn--pill m-btn--air btn-primary btn-sm" id="btnShowLogs"><i class="la la-search"></i> Show</button>
                                              <button type="button" class="btn m-btn--pill m-btn--air btn-success btn-sm" id="btnExportLogs"><i class="la la-file-excel-o"></i> Export</button>
                                          </div>
											

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="" style="margin-top: 5%;overflow: scroll;text-align: center;">
								<div class="div_record_log_monitoring">
								</div>
									<div class="row" id="div_record_log_monitoring" style="width: 2000px;">
										
									</div>
						</div>
						
						
                    </div>
                </div>  

               
  
            </div>
        </div>
		</div>
	</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/js/themes/dark-unica.js"></script>

 <script>
		function onChangeData() {
				var daterange = $("#search_daterange").val();
				var clazz = $("#search-class").val();
				var account = $("#search-accounts").val();
				var isActive = $("#search-isActive").val();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>dtr/employee_list",
					data: {daterange:daterange,clazz:clazz,account:account,isActive:isActive},
					cache: false,
					success: function (result) {
						result = JSON.parse(result.trim());
						$("#dataemployee").data("employee",result);
					}
				});
		}
		function tz_date(thedate, format = null) {
		var tz = moment.tz(new Date(thedate), "Asia/Manila");
		if (format === null) {
			return moment(tz);
		} else {
			return moment(tz).format(format);
		}

		}
		function get_all_accounts(callback) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>Schedule/get_all_accounts",
			cache: false,
			success: function (result) {
				result = JSON.parse(result.trim());
				callback(result);
			}
		});
		}
 $(function(){
	 
	  
		    $('#search-dates').daterangepicker({
                startDate: tz_date(moment()),
                opens: 'right',
                drops: 'auto',
                autoUpdateInput: true
            },
			function (start, end, label) {
				$('#search-dates .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));

			}); $('#search-dates-graph').daterangepicker({
                startDate: tz_date(moment()),
                endDate: tz_date(moment()),
                maxDate: tz_date(moment()),
                opens: 'right',
                drops: 'auto',
                autoUpdateInput: true
            },
			function (start, end, label) {
				$('#search-dates-graph .form-control').val(tz_date(moment(start), 'MMM DD, YYYY') + ' - ' + tz_date(moment(end), 'MMM DD, YYYY'));

			});
			
            get_all_accounts(function (res) {
                $("#search-accounts").html("<option value='ALL'>All</option>");
                $.each(res.accounts, function (i, item) {
                    $("#search-accounts").append("<option value='" + item.acc_id + "' data-class='" + item.acc_description + "'>" + item.acc_name + "</option>");
                    $("#search-accounts-graph").append("<option value='" + item.acc_id + "' data-class='" + item.acc_description + "'>" + item.acc_name + "</option>");
                });
            });
			
			$("#btnShowLogsGraph").click(function(){
				var date = $("#search_daterange_graph").val();
				var emp = $("#dataemployeegraph").data("employeegraph");
				var clazz = $("#search-class-graph").val();
				var account = $("#search-accounts-graph").val();
						$.ajax({
							type: "POST",
							url: "<?php echo base_url('dtr/graphreport'); ?>",
							data: {
								date : date,
								emp : emp,
								clazz : clazz,
								account : account,
							},
							cache: false,
							beforeSend: function () {
								mApp.block('.graphreport', {
									overlayColor: '#000000',
									type: 'loader',
									state: 'brand',
									size: 'lg',
									message: "Collecting Data.."
								});
							},
							success: function (json) {
							
								if(json!=0){
								var totalShiftWork = 0;
								var totalShiftWorkwBreak = 0;
								var totalActualWork = 0;
								var totalActualWorkwBreak = 0;
								var totalShiftBreakWork = 0;
								var totalBreak = 0;
								var ifOverBreak = 0;
								var isUTLateAbsent = 0;
								var isLeave = 0;
								var temp_break = 0;
								var ctr = 0;
								var attendance = 0;
									var overall_totalShiftWork = 0;
									var overall_totalActualWork = 0;
									var overall_ifOverBreak = 0;
									
									var overall_attendance = 0;
									var overall_absenteeism = 0;
									var overall_loginEfficiency = 0;
									var overall_shrinkage = 0;
									var scheduledHoursArr = [];
									var loggedInHoursArr = [];
									var totalObArr = [];
									
									var perAttendArr = [];
									var perAbsentArr = [];
									var perLoginEffArr = [];
									var perShrinkageArr = [];
									
									
								var result = JSON.parse(json);
								$.each(result, function (x, item){
									$.each(item, function (y, item2){
										$.each(item2, function (z, item3){
											totalShiftWork+=item3.getTotalHours;

											if(item3.logs!="0"){
												totalShiftWorkwBreak+=item3.logs["total_shift_hours"];
												
												totalActualWork+=item3.logs["total_actual_worked_wo_break"];
												totalActualWorkwBreak+=item3.logs["total_actual_worked"];
												totalShiftBreakWork+=item3.logs["totalBreak"];
												isUTLateAbsent+=item3.logs["remaining_hour"];
												var fbo="",fbi="";
												var luo="",lui="";
												var lbo="",lbi="";

												 $.each(item3.breakz, function (x, item){
													if(item.break_type=="FIRST BREAK"){
														if(item.entry=="O" && fbo==""){
														fbo=item.log;
														}
														if(item.entry=="I" && fbi==""){
														fbi=item.log;
														}
													}
													if(item.break_type=="LUNCH"){
														if(item.entry=="O" && luo==""){
															luo=item.log;
														}
														if(item.entry=="I" && lui==""){
															lui=item.log;
														}
													}
													if(item.break_type=="LAST BREAK"){
														if(item.entry=="O" && lbo==""){
														lbo=item.log;
														}
														if(item.entry=="I" && lbi==""){
														lbi=item.log;
														}
													}

												});   
												var totalfb = 0;
												var totallu = 0;
												var totallb = 0;
												var totalwork = 0;
												
												if((fbo!="") && (fbi!="")){
													var f1 = moment(fbo).format();
													var f2 = moment(fbi).format();
													var duration = moment.duration(moment(f2).diff(f1));
													totalfb = duration.asHours();
												}
												if((luo!="") && (lui!="")){
													var lu1 = moment(luo).format();
													var lu2 = moment(lui).format();
													var duration = moment.duration(moment(lu2).diff(lu1));
													totallu =  duration.asHours();
												}
												if((lbo!="") && (lbi!="")){
													var l1 = moment(lbo).format();
													var l2 = moment(lbi).format();
													var duration = moment.duration(moment(l2).diff(l1));
													totallb =  duration.asHours();
				 
												}
												 totalBreak += (totalfb+totallu+totallb);
												 temp_break = (totalfb+totallu+totallb);
												 temp_break = (x=="Athletic Greens") ? ((temp_break>1.5) ? temp_break : 1) : temp_break;
												 var totalBreak = (x=="Athletic Greens" ) ? 1.5: item3.logs["totalBreak"] ;
												 var s =  temp_break - totalBreak;
												 ifOverBreak += (s>0) ? s : 0 ;
												 
												 
 
											}else{

												totalShiftWorkwBreak+=0;
												
												totalActualWork+=0;
												totalActualWorkwBreak+=0;
												totalShiftBreakWork+=0;
												isUTLateAbsent+=0;

											}
											if(item3.type=="Leave-WP" || item3.type=="Leave-WoP"){
													 isLeave+= (item3.leaveRemarks=="Manual-Full") ? 8 : 4;
											}
											 // console.log(item3.lname+" == "+z+" -=--  "+totalShiftWork+"  --- "+totalActualWork+"  ---  "+ifOverBreak+"  ---  "+temp_break+" -*- "+item3.logs["totalBreak"])
											 // console.log(item3.lname+" == "+z+" -=--  "+item3.type=="Leave-WoP"+" -=--  "+item3.logs["total_shift_hours_wo_break"])
										});
										
									});
										 attendance = totalActualWork/totalShiftWork;
										 attendance = (attendance>0) ? attendance : 0 ;
										var absenteeism = (attendance>0) ? 1-attendance :0;
										var loginEfficiency  = (totalShiftWorkwBreak-isUTLateAbsent-ifOverBreak)/totalShiftWorkwBreak;
										var shrinkage  = (isUTLateAbsent+isLeave)/(isLeave+totalShiftWork);
										  console.log(x+"  = Scheduled Hours = "+totalShiftWork+"** Loggedin Hours ="+totalActualWork+"** TOTAL OB ="+ifOverBreak+"** Attendace % ="+(attendance*100)+"** Absenteeism % ="+(absenteeism*100)+"** Login Efficiency % ="+(loginEfficiency*100)+"** Shrinkage % ="+(shrinkage*100));
										// console.log();
										overall_totalShiftWork+= totalShiftWork;
										overall_totalActualWork+= totalActualWork;
										overall_ifOverBreak+= ifOverBreak;
										
										overall_attendance+= (attendance>0) ? attendance : 0 ;
										overall_absenteeism+= (absenteeism>0) ?  absenteeism : 0 ;
										overall_loginEfficiency+= (loginEfficiency>0) ?  loginEfficiency : 0 ;
										overall_shrinkage+= (shrinkage>0) ?  shrinkage : 0 ;
										
 													scheduledHoursArr[ctr] = [x,totalShiftWork];
 													loggedInHoursArr[ctr] = [x,totalActualWork];
 													totalObArr[ctr] = [x,ifOverBreak];
 														perAttendArr[ctr] = [x,(attendance*100)];
 														perAbsentArr[ctr] = [x,(absenteeism*100)];
 														perLoginEffArr[ctr] = [x,(loginEfficiency*100)];
  														perShrinkageArr[ctr] = [x,(shrinkage*100)];
  													
 													
 													
 													
												 
										totalShiftWork = 0;
										totalActualWork = 0;
										isUTLateAbsent = 0;
										totalShiftBreakWork = 0;
										totalActualWorkwBreak = 0;
										totalShiftWorkwBreak = 0;
										ifOverBreak = 0;
										totalBreak = 0;
										ctr++;
										
										// console.log(ctr+" "+attendance+" -*- "+overall_attendance+" -*- "+totalActualWork+" -*- "+totalShiftWork);

								});
								// console.log(scheduledHoursArr);
								 mApp.unblock('.graphreport');
											var r_details1 = [{
													"name": "Scheduled Hours",
													"y": overall_totalShiftWork,
													"drilldown": "Scheduled Hours"
												},{
													"name": "Loggedin Hours",
													"y": overall_totalActualWork,
													"drilldown": "Loggedin Hours"
												},{
													"name": "Total Overbreak",
													"y": overall_ifOverBreak,
													"drilldown": "Total Overbreak"
												}];
												
											var r_details2 = [{
													"name": "Attendance %",
													"y": ((overall_attendance/ctr)*100),
													"drilldown": "Attendance"
												},{
													"name": "Absenteeism %",
													"y": ((overall_absenteeism/ctr)*100),
													"drilldown": "Absenteeism"
												},{ 
													"name": "Login Efficiency %",
													"y": ((overall_loginEfficiency/ctr)*100),
													"drilldown": "Login"
												},{
													"name": "Shrinkage %",
													"y": ((overall_shrinkage/ctr)*100),
													"drilldown": "Shrinkage"
												}]
												
												
										 Highcharts.chart('div-graph1', {
												chart: {
													type: 'column',
													
												},
												title: {
													text: "Attendance Logs Report in Hours (hr)"
												},
												subtitle: {
													text: 'Logs for '+date
												},												
												xAxis: {
													type: 'category'
												},
												legend: {
													enabled: false
												},
												plotOptions: {
													series: {
														cursor: 'pointer',
														borderWidth: 0,
														dataLabels: {
															enabled: true,
															format: '{point.y:.2f} hr'
														}
													}
												},
												series: [{
														name: 'Hours',
														colorByPoint: true,
														data: r_details1
													}],
												 drilldown: {
														series: [
															{
																name: "Scheduled Hours",
																id: "Scheduled Hours",
																data: scheduledHoursArr
															},
															{
																name: "Loggedin Hours",
																id: "Loggedin Hours",
																data: loggedInHoursArr
															},
															{
																name: "Total Overbreak",
																id: "Total Overbreak",
																data: totalObArr
															}
														]
													}
											});

										
										 Highcharts.chart('div-graph2', {
												chart: {
													type: 'column',
													
												},
												title: {
													text: "Attendance Logs Report in Percentage (%)"
												},
												subtitle: {
													text: 'Logs for '+date
												},											
												xAxis: {
													type: 'category'
												},
												legend: {
													enabled: true
												},
												tooltip: {
													headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
													pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
												},												
												plotOptions: {
													series: {
														cursor: 'pointer',
														borderWidth: 0,
														dataLabels: {
															enabled: true,
															format: '{point.name}: {point.y:.2f}%'

														}
													}
												},
												series: [{
														name: 'Percentages',
														colorByPoint: true,
														data: r_details2
													}],
												 drilldown: {
														series: [
															{
																name: "Attendance",
																id: "Attendance",
																data: perAttendArr
															},
															{
																name: "Absenteeism",
																id: "Absenteeism",
																data: perAbsentArr
															},
															{
																name: "Login",
																id: "Login",
																data: perLoginEffArr
															},
															{
																name: "Shrinkage",
																id: "Shrinkage",
																data: perShrinkageArr
															}
														]
													}
											});
										
								}
								
							}
					});
				
				
			});
			$("#btnShowLogs").click(function(){
				var date = $("#search_daterange").val();
				var emp = $("#dataemployee").data("employee");
				var clazz = $("#search-class").val();
				var account = $("#search-accounts").val();
				var isActive = $("#search-isActive").val();

					$.ajax({
							type: "POST",
							url: "<?php echo base_url('dtr/dtr_view_logs/report/'); ?>"+isActive,
							data: {
								date : date,
								emp : emp,
								clazz : clazz,
								account : account,
							},
							cache: false,
							beforeSend:function(data){
								$(".div_record_log_monitoring").html("<span style='margin:0 auto;'><img src='<?php echo base_url("/assets/img/spinner1.gif"); ?>'></span>");
								$("#div_record_log_monitoring").html("");
								
							},
							success: function (json) {
								$(".div_record_log_monitoring").html("");
								if(json!=0){
								
								
								var result = JSON.parse(json);
								var tbl = "<table class='table m-table table-hover table-sm table-bordered' id='table-access' style='font-size:13px;width: 2000px !important;'>";
								tbl+="<thead style='background: #555;color: white;'><tr>"+
									 "<th>ID NUMBER</th>"+
									 "<th>LAST NAME</th>"+
									 "<th>FIRST NAME</th>"+
									 "<th>ACCOUNT TYPE</th>"+
									 "<th>ACCOUNT</th>"+
									 "<th style='width: 100px;'>DATE</th>"+
									 "<th>TYPE</th>"+
									 "<th  style='width: 100px;'>SHIFT START</th>"+
									 "<th  style='width: 100px;'>SHIFT END</th>"+
									 "<th>REMARKS</th>"+
									 "<th>ACTUAL IN</th>"+
									 "<th>ACTUAL OUT</th>"+
									 "<th>FIRST BREAK-OUT</th>"+
									 "<th>FIRST BREAK-IN</th>"+
									 "<th>LUNCH-OUT</th>"+
									 "<th>LUNCH-IN</th>"+
									 "<th>LAST BREAK-OUT</th>"+
									 "<th>LAST BREAK-IN</th>"+
									 "<th>TOTAL BREAK</th>"+
									 "<th>TOTAL WORK</th>"+
									 "</tr></thead>";
									 var i=1;
									
								$.each(result, function (x, item){
									 var bg = (i%2 == 0) ? "#fff" : "#c4c5d65e";
									 var color = (i%2 == 0) ? "#575962" : "#000";
									tbl+= "<tr><td colspan=5></td></tr>";
									$.each(item, function (y, item2){
										$.each(item2, function (z, item3){
											var fbo="",fbi="";
											var luo="",lui="";
											var lbo="",lbi="";
											var newLogin=0;
											var newLogout=0;
											var totalBreak="";
												newLogin  = (item3.actual_in!=0) ? item3.logs["login"] : 0;
												newLogout  = (item3.actual_out!=0) ?  item3.logs["logout"] : 0;
												total_Break  = (item3.logs!=0) ?  item3.logs["totalBreak"] : 0;
												
											
											console.log(item3.fname+" "+y+" "+newLogin+" "+newLogout+" "+total_Break);
											
 											var shift_start = (item3.timeschedule!="--") ? item3.timeschedule[0].time_start : "<span class='text-danger'>N/A</span>";
											var shift_end = (item3.timeschedule!="--") ? item3.timeschedule[0].time_end : "<span class='text-danger'>N/A</span>";
											var login = (item3.actual_in!=0) ?  moment(item3.actual_in[0].login).format('lll') : "<span class='text-danger'>No Login</span>";
											var logout = (item3.actual_out!=0) ? moment(item3.actual_out[0].logout).format('lll') : "<span class='text-danger'>No Logout</span>";
											
											tbl+="<tr style='background:"+bg+";color:"+color+"'>"+
												"<td>"+item3.id_num+"</td>"+
												"<td>"+item3.lname+"</td>"+
												"<td>"+item3.fname+"</td>"+
												"<td>"+item3.descr+"</td>"+
												"<td>"+item3.acc_name+"</td>"+
												"<td>"+y+"</td>"+
												"<td><span style='color:"+item3.style+"'>"+item3.type+item3.isLeaveName+"</span></td>"+
												"<td>"+shift_start+"</td>"+
												"<td>"+shift_end+"</td>"+
												"<td>"+item3.statusLog+"</td>"+
												"<td>"+login+"</td>"+
												"<td>"+logout+"</td>";
												
												 $.each(item3.breakz, function (x, item){
													if(item.break_type=="FIRST BREAK"){
														if(item.entry=="O" && fbo==""){
														fbo=item.log;
														}
														if(item.entry=="I" && fbi==""){
														fbi=item.log;
														}
													}
													if(item.break_type=="LUNCH"){
														if(item.entry=="O" && luo==""){
															luo=item.log;
														}
														if(item.entry=="I" && lui==""){
															lui=item.log;
														}
													}
													if(item.break_type=="LAST BREAK"){
														if(item.entry=="O" && lbo==""){
														lbo=item.log;
														}
														if(item.entry=="I" && lbi==""){
														lbi=item.log;
														}
													}

												});  
												var totalfb = 0;
												var totallu = 0;
												var totallb = 0;
												var totalwork = 0;
												
												if((fbo!="") && (fbi!="")){
													var f1 = moment(fbo).format();
													var f2 = moment(fbi).format();
													var duration = moment.duration(moment(f2).diff(f1));
													totalfb = duration.asHours();
												}
												if((luo!="") && (lui!="")){
													var lu1 = moment(luo).format();
													var lu2 = moment(lui).format();
													var duration = moment.duration(moment(lu2).diff(lu1));
													totallu =  duration.asHours();
												}
												if((lbo!="") && (lbi!="")){
													var l1 = moment(lbo).format();
													var l2 = moment(lbi).format();
													var duration = moment.duration(moment(l2).diff(l1));
													totallb =  duration.asHours();
				 
												}
												var totalBreak = ((totalfb+totallu+totallb)*60);
												var totalBreakforTotalWork = (totalBreak>=60) ? (totalBreak/60) : total_Break;
												// if((newLogin!=0) && (newLogout!=0)){
													// var w1 = moment(newLogin).format();
													// var w2 = moment(newLogout).format();
													// var duration = moment.duration(moment(w2).diff(w1));
													// totalwork = duration.asHours()-totalBreakforTotalWork;
												// }
												totalwork  = (item3.logs!=0) ? item3.logs["total_actual_worked"] : 0;

												// console.log(item3.fname+" "+y+" "+totalfb+" "+totallu+" "+totallb);
												tbl+="<td>"+((fbo!="") ? moment(fbo).format('lll') : "<span class='text-danger'>No Logs</span>")+"</td>";
												tbl+="<td>"+((fbi!="") ? moment(fbi).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
												tbl+="<td>"+((luo!="") ? moment(luo).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
												tbl+="<td>"+((lui!="") ? moment(lui).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
												tbl+="<td>"+((lbo!="") ? moment(lbo).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
												tbl+="<td>"+((lbi!="") ? moment(lbi).format('lll')  : "<span class='text-danger'>No Logs</span>")+"</td>";
												tbl+="<td>"+totalBreak.toFixed(2)+" mins.</td>";
												tbl+="<td>"+totalwork.toFixed(2)+" hr.</td>";
												
												tbl+="</tr>";
										}); 
									}); 
									i++;
								}); 
								tbl += "</table>";				
								$("#div_record_log_monitoring").html(tbl);
								 
							}else{
								var div =  '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="date-alert">'+
                                    '<div class="m-alert__icon"><i class="flaticon-tool"></i><span></span></div>'+
                                    '<div class="m-alert__text"><strong>No Records Found! </strong> <br>Sorry, no records were found. Please adjust your search criteria and try again.'+
                                    '</div>'+
									'</div>';
								$(".div_record_log_monitoring").html(div);
								
							}
							}
					});
});
			 
		/* 	$("#search-accounts").change(function () {
				var daterange = $("#search_daterange").val();
				var clazz = $("#search-class").val();
				var account = $("#search-accounts").val();
				var isActive = $("#search-isActive").val();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>dtr/employee_list",
					data: {daterange:daterange,clazz:clazz,account:account,isActive:isActive},
					cache: false,
					success: function (result) {
						result = JSON.parse(result.trim());
						$("#dataemployee").data("employee",result);
					}
				});
			}); */
			$("#search-accounts-graph").change(function () {
				var daterange = $("#search_daterange_graph").val();
				var clazz = $("#search-class-graph").val();
				var account = $("#search-accounts-graph").val();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>dtr/employee_list",
					data: {daterange:daterange,clazz:clazz,account:account},
					cache: false,
					success: function (result) {
						result = JSON.parse(result.trim());
						$("#dataemployeegraph").data("employeegraph",result);
					}
				});
			});
			$("#search-class").change(function () {
                var theclass = $(this).val();
                $("#search-accounts").children('option').css('display', '');
                $("#search-accounts").val('').change();
                if (theclass !== '') {
                    $("#search-accounts").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
                }

            });
			$("#search-class-graph").change(function () {
                var theclass = $(this).val();
                $("#search-accounts-graph").children('option').css('display', '');
                $("#search-accounts-graph").val('').change();
                if (theclass !== '') {
                    $("#search-accounts-graph").children('option[data-class!="' + theclass + '"][value!=""]').css('display', 'none');
                }

            });
			
				$("#btnExportLogs").click(function(){
			var date =  ($("#search_daterange").val()).split(" - ");
				var date1 =moment(date[0]).format("YYYY-MM-DD");
				var date2 =moment(date[1]).format("YYYY-MM-DD");

			var clazz =  $("#search-class").val();
			var account =  $("#search-accounts").val();
			var isActive =  $("#search-isActive").val();
				  window.open("<?php echo base_url(); ?>index.php/dtr/export_dtr_log/"+date1+"/"+date2+"/"+clazz+"/"+account+"/"+isActive,"_blank");
			
			});

 });
 </script>