
<style>
.time ul{
	  list-style-type: none;
    margin-left: -39px;
	}
	.note{
	font-size: initial;
    background: #4a4a4a;
    padding: 10px;
}
	 
	.timeDetails{
	 padding: 10px;
    background: gray;
	font-size: 100px !important;
	}
	.time{
    border-radius: 13px;
    color: white;
    font-family: fantasy;
    display: inline-flex;
	}
	.totoldok {
    font-size: 104px;
    color: #4a4a4a;
    padding: 10px 15px 10px 15px;
    font-weight: 600;
}
	 </style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Break</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Break</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content text-center" >
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head" style="background: #575962;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title" style="float: left;">
					<span class="m-portlet__head-icon m--hide">
					<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #fff !important; ">
						<i class="la la-clock-o"></i> On-Break..
					</h3>
				</div>
			
			</div>
		</div>
			<div class="m-portlet__body" style="background-image: url(<?php echo base_url("assets/images/img/break.png"); ?>);background-size: cover;background-position-y: -129px;background-repeat: no-repeat;">
				<div class="row">
				<?php 
					// var_dump($emp_last_logs);
				?>
					<div class="col-lg-12">
					<?php 
					$time = explode(":",$break[0]->diff);
					$color = array("FIRST BREAK"=>"info","LUNCH"=>"warning","","LAST BREAK"=>"success");
					$icon = array("FIRST BREAK"=>"la-hotel","LUNCH"=>"la-coffee","LAST BREAK"=>"la-yelp");

					?>
						<div id="tesst1" hidden><?php echo $break[0]->diff; ?></div>
						<div id="tesst" style="text-align: center;">
								 
						</div>
					</div>
					<div style="text-align: center;width: 100%;margin-top: 41px;">
					<button class="btn btn-<?php echo $color[$emp_last_logs[0]->break_type]; ?>" onclick="takeBreak(<?php echo $emp_last_logs[0]->acc_time_id.",'I',".$emp_last_logs[0]->dtr_id; ?>)">
						<i class="la <?php echo $icon[$emp_last_logs[0]->break_type]; ?>"></i> Back to Work
					</button></div>
				</div>
			</div>
		</div>
	</div>
</div>
 <script type="text/javascript">
	function takeBreak(sbrk_id,type,note){
		  $.ajax({
			type: "POST",
			url: "<?php echo base_url('dtr/takeBreak'); ?>",
			data: {sbrk_id:sbrk_id,type:type,note:note},
			cache: false,
			success: function (output) {
					  var result = JSON.parse(output);

				if(result.result>0){
					swal(
					  'Success',
					  "Successfully back from Break.",
					  'success'
					)
 					window.location.assign("<?php echo base_url();?>dtr/attendance");
				}else{
					swal(
					  'ERROR',
					  "There is an error. Please refresh the page or inform your supervisor (w/ screenshot). Thank you.",
					  'error'
					)
				}
			}
		 });
		$("#breakModal").modal("show");
	}
	
	function tz_date(thedate, format = null) {
            var tz = moment.tz(new Date(thedate), "Asia/Manila");
            if (format === null) {
                return moment(tz);
            } else {
                return moment(tz).format(format);
			}
        }
		
        $(function () {
					 setInterval(function(){
						var tz_date2 = tz_date(moment(),"lll");
						var worked = $("#tesst1");

						var myTime = worked.html();
						var ss = myTime.split(":");
						// var dt = new Date();
						var dt = new Date(tz_date2);
						dt.setHours(ss[0]);
						dt.setMinutes(ss[1]);
						dt.setSeconds(ss[2]);
						var dt2 = new Date(dt.valueOf() + 1000);
						var ts = dt2.toTimeString().split(" ")[0];
						var res = ts.split(":");
						worked.html(ts);
 						$("#tesst").html('<span class="time"><ul> <li class="timeDetails">'+res[0]+'</li><li class="note"> hour(s) </li></ul> </span><span class="totoldok">:</span><span class="time"><ul> <li class="timeDetails">'+res[1]+'</li><li class="note"> minute(s) </li></ul></span><span class="totoldok">:</span><span class="time"><ul> <li class="timeDetails">'+res[2]+'</li><li class="note"> second(s) </li></ul></span>');
						 
						 
					 },1000);
					// setTimeout(update, 1000);
         }); 

$(window).focus(function() {
location.reload();
});
$(window).blur(function() {
location.reload();
});
</script>