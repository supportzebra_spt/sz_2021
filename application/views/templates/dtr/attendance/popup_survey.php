 
<div class="modal fade" id="popup_survey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true"
    data-backdrop="static" data-keyboard="false" data-intervention="">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="survey_question" style="color: white;font-size: 24px;">
                    <?php echo $survey_data['survey_data'][0]->question ;?>
                 </h2>
				
                <button type="button" class="close-survey-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="">
                <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="submit_daily_survey">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-lg-12 col-md-offset-5" id="survey_choices" style="text-align: center;display: block;    margin-bottom: 10px;">
                                <?php $count = count($survey_data['survey_data'][0]->clabels) ; ?>
                                <?php foreach($survey_data['survey_data'][0]->clabels as $data) : ?>
                                <div class="m-widget3__user-img" id="choiceId" data-id="<?php echo $data['choiceId'] ; ?>" style="display: inline-block; padding: 10px">
                                    <a data-toggle="m-tooltip" data-skin="dark" title="" data-original-title="<?php echo $data['label'] ; ?>" id="<?php echo $data['img'] ; ?>"
                                        title="<?php echo $data['label'] ; ?>" class="btn btn-lg m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill choices survey-answer-pills"
                                        onClick="clickSurvey(<?php echo $count . ',' . $data['num'] . ','. $data['choiceId']; ?>)">
                                        <img class="m-widget3__img" src="<?php echo base_url('assets/images/') . $data['img'] ;?>" alt="">
                                    </a>
                                </div>
                                <?php endforeach ;?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-offset-5" id="feelings_ratio" style="text-align: center;">

                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group" id="followup_question" style="display:none;">
                        <label id="for-question"></label>
                        <input type="hidden" name="answerNum" />
                        <input type="hidden" name="median" />
                        <input type="hidden" name="labelId" />
                        <input type="hidden" name="survey" value="<?php echo $survey_data['survey_data'][0]->surveyId ;?>" />
                        <textarea class="form-control m-input survey-answer" id="detailedAnswer" placeholder="" value=""></textarea>
                        <div class="row" style="text-align:center;margin-top: 15px;">
                            <div class="col-lg-12 ml-lg-auto col-center">
                                <button type="submit" class="btn m-btn btn-primary" id="submit_survey" disabled="disabled">Submit</button>
                            </div>
                        </div> 
                    </div>
                    <!-- <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row" style="text-align:  center">
                                <div class="col-lg-12 ml-lg-auto col-center">
                                    <button type="submit" class="btn btn-danger submit-form" id="submit_survey" disabled="disabled">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
</div>
    <script src="<?php echo base_url(); ?>assets/src/confetti/js/randomColor.js"></script>
    <script src="<?php echo base_url(); ?>assets/src/confetti/js/confettiKit.js"></script>

<script type="text/javascript">
    $(function () {
        $('body').on("click", '.choices', function () {
            var id = $(this).attr("id"); //for future use
            var img = baseUrl + '/assets/images/' + id.replace(/png/, 'gif');
            $(this).find('img').attr('src', img)
            $(this).addClass('chosen');
            $(this).tooltip();
        });
    });

    function clickSurvey(level, choice, labelId) {

        // pre-remove active class for all choices
        $("a").removeClass('chosen');

        $('.choices').each(function (i, obj) {
            var img = $(this).find('img').attr('src').replace(/gif/, 'png');
            $(this).find('img').attr('src', img);
        });

        // add active class for the selected choice
        $("#choice" + choice).addClass('chosen');
        // add value to hidden input choice/asnwer
        $('#submit_daily_survey').find('input[name=answerNum]').val(choice);

        $('#feelings_ratio').html('<p style="color: #777;font-size: 16px; ">Feeling: <span id="feeling-ration">' +
            choice + '</span>/' + level + '</p>');

        // get median or middle value for the choice level. ex. [1,2,3,4,5]
        var median = getMedian(level);

        $('#submit_daily_survey').find('input[name=median]').val(median);
        $('#submit_daily_survey').find('input[name=labelId]').val(labelId);

        if (level == choice) {
            $('#for-question').html('<span>That’s amazing! What’s your secret?</span>');
        } else if (choice > median && choice != level) {
            $('#for-question').html('<span>Anything that would kick it up a notch?</span>');
        } else if (choice == median) {
            $('#for-question').html('<span>Care to elaborate?</span>');
        } else if (choice < median) {
            $('#for-question').html('<span>Got it. Anything that would have you feel better?</span>');
        }
        // // Followup question is required if median is equal or lesser than the maximun choice level
        // Do not erase code, this is for enabling unrequired followup answers
        // if (choice <= median) {
        $('#followup_question').show();
        $('textarea').attr('name', 'requiredAnswer');
        $('textarea').attr('placeholder', 'Enter detailed answer...');
        $('#submit_survey').removeAttr('disabled');
        // } else {
        //     $('#followup_question').show();
        //     $('textarea').attr('name', 'notrequiredAnswer');
        //     $('textarea').attr('placeholder', 'Enter detailed answer...');
        //     $('#submit_survey').removeAttr('disabled');
        //     $('.m-form__group').removeClass('has-danger');
        //     $('#detailedAnswer-error').remove();
        // }
    }

    function getMedian(level) {
        var numbers = [1];
        numbers.push(level);

        var median = 0,
            numsLen = numbers.length;
        numbers.sort();

        if (numsLen % 2 === 0) { // is even
            // average of two middle numbers
            median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
        } else { // is odd
            // middle number only
            median = numbers[(numsLen - 1) / 2];
        }
        return median;
    }

    function showSurvey(){

        var sched_id = $('#schedIDdiv').data('id');
        $('#popup_survey').modal('show');
		console.log("INNNN");
        // $.when(fetchPostData({sched_id:sched_id},'/szfive/popup_survey'))
            // .then(function (response) {
                // var res = jQuery.parseJSON(response);
                // if (res.has_taken == true) {
                    // console.log('Do not show survey anymore. Survey alreday taken.');
                // } else {
                    // console.log('Show survey.');
                    // var data = res.data;
                    // popupSurvey(data);
                // }
            // });
    }

    function popupSurvey(data) {

        $('#survey_question').html("<center>" + data[0].question + "</center>");
        // $('#submit_daily_survey').find('input[name=employee]').val();
        $('#submit_daily_survey').find('input[name=survey]').val(data[0].surveyId);

        var label = data[0].choiceLabels[0];
        var level = data[0].choiceLabels.length;
        var str = '';

        $.each(data[0].clabels, function (key, val) {
            str += '<div class="m-widget3__user-img" id="choiceId" data-id="' + val.choiceId +
                '" style="display: inline-block; padding: 10px">';
            str += '<a data-toggle="m-tooltip" title="" data-original-title="' + val.label + '" id="' + val.img +
                '" title="' + val.label +
                '"  class="btn btn-lg m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill choices survey-answer-pills" onClick="clickSurvey(' +
                level + ',' + val.num + ',' + val.choiceId + ')">';
            str += '<img class="m-widget3__img" src="' + baseUrl + '/assets/images/' + val.img + '" alt="">';
            str += '</a></div>';
        });

        $('#survey_choices').html(str);
        /*  $('#popup_survey').modal({
            backdrop: 'static',
            keyboard: false,
            closeOnEscape: false,
            open: function (event, ui) {
                $(".close").hide();
            }
        });  */
        $('#popup_survey').modal('show');
    }

    var SurveyValidation = function () {

        var submitSurvey = function () {

            $("#submit_daily_survey").validate({
                rules: {
                    requiredAnswer: {
                        required: true,
                        minlength: 5,
                        maxlength: 250,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                },
                invalidHandler: function (event, validator) {
                    swal({
                        "title": "",
                        "text": "Please add some comments about your answer.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },
                submitHandler: function (form) {

                    // $('#submit_survey').prop('disabled', true);

                    // var employee = $('#submit_daily_survey').find('input[name=employee]').val();
                    var answerNum = $('#submit_daily_survey').find('input[name=answerNum]').val();
                    var median = $('#submit_daily_survey').find('input[name=median]').val();
                    var survey = $('#submit_daily_survey').find('input[name=survey]').val();
                    var requiredAnswer = $('#submit_daily_survey').find(
                        'textarea[name=requiredAnswer]').val();
                    // var notrequiredAnswer = $('#submit_daily_survey').find('textarea[name=notrequiredAnswer]').val();
                    var sched_id = $('#schedIDdiv').data('id');
                    var choiceId = $('#submit_daily_survey').find('input[name=labelId]').val();

                    requiredAnswer = requiredAnswer !== undefined ? requiredAnswer.replace(/\s\s+/g,
                        ' ') : null;
					// alert(answerNum);
				 
 
							var options = {
									url: "<?php echo base_url('szfive/submit_survey'); ?>",
									type: 'POST',
									dataType: "json",
									data: {
										requiredAnswer: requiredAnswer,
										// notrequiredAnswer: notrequiredAnswer,
										answerNum: answerNum,
										median: median,
										survey: survey,
										sched_id: sched_id,
										choiceId: choiceId,
									},
									beforeSend: function() {
										// setting a timeout
										$("#submit_survey").addClass("m-loader m-loader--light m-loader--left");
										$("#submit_survey").text("Submitting..");
										$('#submit_survey').prop('disabled', true);
									},
									success: function (response) {

										if (response.status === true) {
											// alert(response.systemNotifID);
											if (response.systemNotifID != 0) {
												systemNotification([response.systemNotifID]);
											}
											/* 
											 swal({
												"title": "Thank you.",
												"text": "We are glad to hear about it.",
												"type": "success",
												"timer": 1500,
												"showConfirmButton": false
											}); 
												location.reload();
											$('#popup_survey').modal('hide');*/
											
										//Daily Pulse --
											
										$("#submit_survey").addClass("m-loader m-loader--light m-loader--left");
											$("#submit_survey").text("Submitting..");
											answerIntervene(answerNum,response.answer_id);
										
									}
									},
									error: function () {
										alert('error');
									},
								};
								$(form).ajaxSubmit(options); 
                   
                }
            });
        }
        return {
            init: function () {
                submitSurvey();
            }
        };
    }();
 
        function showConfetti(){
			   new confettiKit({
					confettiCount: 100,
					angle: 90,
					startVelocity: 50,
					elements: {
						'confetti': {
							direction: 'down',
							rotation: true,
						},
						'star': {
							count: 10,
							direction: 'down',
							rotation: true,
						},
						'ribbon': {
							count: 5,
							direction: 'down',
							rotation: true,
						}
					},
					position: 'topLeftRight',
				});
		}

 	function answerIntervene(answerNum,answerId){
 		$.ajax({
			url: "<?php echo base_url('Dailypulse/identify_occurence/'); ?>"+answerNum+"/"+answerId,
			type: 'POST',
			beforeSend: function() {
				// setting a timeout
				$("#submit_survey").addClass("m-loader m-loader--light m-loader--left");
				$("#submit_survey").text("Submitting..");
				$('#submit_survey').prop('disabled', true);
			},
			success: function(data) {
				// alert(data);
				$("#submit_survey").removeClass("m-loader m-loader--light m-loader--left");
				$("#submit_survey").text("Submitted.");
				$('#submit_survey').prop('disabled', true);
				var rs = JSON.parse(data);
				var cnt = 0;
				var cntTXT = "";
					
					if(rs.isConfetti!="0"){
						cnt+=1;
						cntTXT+="isConfetti---";
						showConfetti();
					}
					if(rs.message!="0" || rs.icon!="0"){
							cnt+=1;
							cntTXT+="message/icon---";
						$("#popup_survey").modal("hide");
						 swal({
							 title:'',
							 text: (rs.message!="0") ? rs.message : '',
							 imageUrl: (rs.icon!="0") ? '<?php echo base_url("assets/images/"); ?>'+rs.img : '',
							showConfirmButton: true,
							allowOutsideClick: false,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Ok',
						}).then((result) => {							
						  if (result.value){ 
 							  location.reload();
							  // notrequiredAnswer = notrequiredAnswer !== undefined ? notrequiredAnswer.replace(/\s\s+/g, ' ') : null;
								 
								
						  }
						});
					}
 					if(rs.followUpQuestion!="0"){
						cnt+=1;
						cntTXT+="followUpQuestion---";

						$("#popup_survey").modal("hide");
						swal({
						  title: "",
						  input: 'textarea',
						  inputAttributes: {
							autocapitalize: 'off',
							maxlength: 500,
							minlength: 5,
						  },
						  html:'<div class="swal2-custom-title"><h4>'+rs.followUpQuestion+'</h4></div>' ,
						  showCancelButton: false,
						  confirmButtonText: 'Submit',
						  showLoaderOnConfirm: true,
						  imageUrl: (rs.icon!="0") ? '<?php echo base_url("assets/images/"); ?>'+rs.img : '',
						  allowOutsideClick: false,
						  inputValidator: (value) => {
								if (!value) {
								  return 'You need to write something!'
								}
								if (value.length<5) {
								  return 'Please enter at least 5 characters.'
								}
								if (value.length>300) {
								  return 'Only 300 characters are allowed.'
								}
							  },
						}).then((result) => {
							
						  if (result.value){ 
						  $("#popup_survey").data("intervention",rs.followUpQuestion);
						  saveFollowQuestionAnswer(result.value,answerId);
							//location.reload();
							  // notrequiredAnswer = notrequiredAnswer !== undefined ? notrequiredAnswer.replace(/\s\s+/g, ' ') : null;
								 
								
						  }
						});
					}
					if(rs.intervention!="0"){
						cnt+=1;
						cntTXT+="intervention---";
						
						
						interventionData(rs.escalation_ID,answerId,cnt);
						
						
						//
					}
					
			}
		});
	}
	
 	function interventionData(escalation_ID,answerId,cnt){
 		$.ajax({
			url: "<?php echo base_url('Dailypulse/interventionEsacation/'); ?>",
			type: 'POST',
			data: {escalation_ID:escalation_ID,answerId:answerId},
			success: function(res) {
				// alert(data);
 				if(res!="0"){
					 systemNotification([res]);
					
						if(cnt==1){
									
							 swal({
								title: "Thank you.",
								text: "Have a nice day!",
								type: "success",
								showConfirmButton: true,
								allowOutsideClick: false,
							}).then((result) => {
							
							  if (result.value){ 
									 location.reload();								
							  }
							}); 
 							
						}else{
							
						}
				}
					
			}
		});
	}
 	function saveFollowQuestionAnswer(txt,answerId){
		var txtIntervention = $("#popup_survey").data("intervention");
		// alert(txtIntervention);

 		$.ajax({
			url: "<?php echo base_url('Dailypulse/savefollowanswer/'); ?>",
			type: 'POST',
			data: {txt:txt,answerId:answerId,followUpQuestion:txtIntervention},
			success: function(data) {
				// alert(data);
				var rs = JSON.parse(data);
				if(data>0){
					location.reload();
				}
					
			}
		});
	}
	<?php 
	if(isset($survey_data['isAnsweredFollowQues']) && $survey_data['isAnsweredFollowQues']==1){ 
	?>
 	answerIntervene(<?php echo $survey_data['answer']; ?>,<?php echo $survey_data['answerId']; ?>)
  	
	<?php } ?>
    $(document).ready(function () {
         SurveyValidation.init();
		<?php if ($survey_data['has_taken'] == false && $emp_last_logs_DTR[0]->entry=='I'){ ?>
			<?php if($dtrCutStatus!=0){ ?>
 				showSurvey();
		
			<?php } ?>
		<?php } ?>
        $(document).on("contextmenu",function(){
           return false;
        }); 

        $(document).keydown(function(event){
            if(event.keyCode==123){
                return false;
            }
            else if (event.ctrlKey && event.shiftKey && event.keyCode==73){        
                    return false;
            }
        });

    });
</script>