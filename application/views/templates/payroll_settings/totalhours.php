<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Payroll Employee Total Hours</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Dashboard'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            TOTAL HOURS <small>Excel Download</small>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-md-6">
                        <form action="" class="m-form m-form--fit m-form--label-align-right">
                            <div class="form-group m-form__group row">
                                <label for="year" class="col-3 col-form-label">YEAR:</label>
                                <div class="col-9">
                                    <select class="form-control " id="year"></select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="month" class="col-3 col-form-label">MONTH:</label>
                                <div class="col-9">
                                    <select class="form-control " id="month"></select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="coverage" class="col-3 col-form-label">COVERAGE:</label>
                                <div class="col-9">
                                    <select class="form-control" id="coverage"></select>
                                    <span class="m-form__help m--font-danger" id="alert-coverage">Required *</span>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-md-6">
                        <form action="" class="m-form m-form--fit m-form--label-align-right">
                            <div class="form-group m-form__group row">
                                <label for="type" class="col-3 col-form-label">TYPE:</label>
                                <div class="col-9">
                                    <select class="form-control m-bootstrap-select m_selectpicker" multiple id="type">
                                        <option value="tsn">TSN</option>
                                        <!--<option value="admin">SZ Team</option>-->
                                        <option value="agent">Ambassador</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="text-center">
                            <button class="btn btn-primary m-btn m-btn--custom m-btn m-btn--icon" id="btn-download">
                                <span>
                                    <i class="la la-archive"></i>
                                    <span>Download Excel</span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
var getCoverage = function() {
    var year = $("#year").val();
    var month = $("#month").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Payroll_settings/get_coverage",
        cache: false,
        data: {
            year: year,
            month: month
        },
        success: function(res) {
            res = JSON.parse(res.trim());
            var coverageOptions = '<option value="">--</option>';
            $.each(res.coverage, function(i, item) {
                let cover = item.daterange.split(' - ');
                let coverA = moment(cover[0], 'MM/DD/YYYY').format('MMM DD, YYYY');
                let coverB = moment(cover[1], 'MM/DD/YYYY').format('MMM DD, YYYY');
                coverageOptions += '<option value="' + item.coverage_id + '">' + coverA +
                    ' - ' + coverB + '</option>';
            });
            $("#coverage").html(coverageOptions);
            $("#coverage").change();
        }
    });
};
$(function() {
    var startyear = 2017;
    var endyear = moment().year();
    var yearOptions = '<option value="">All</option>';
    for (var x = startyear; x <= endyear; x++) {
        yearOptions += '<option value="' + x + '">' + x + '</option>';
    }
    $("#year").html(yearOptions);

    var monthsList = moment.months();
    var monthOptions = '<option value="">All</option>';
    for (var x = 0; x < monthsList.length; x++) {
        monthOptions += '<option value="' + monthsList[x] + '">' + monthsList[x] + '</option>';
    }
    $("#month").html(monthOptions);

    getCoverage();
    $('#type').selectpicker();
});
$("#year").change(function() {
    getCoverage();
});
$("#month").change(function() {
    getCoverage();
});
$("#coverage").change(function() {
    console.log($(this).val());
    if ($(this).val() == '') {
        $("#alert-coverage").show();
        $("#btn-download").addClass('disabled');

    } else {
        $("#alert-coverage").hide();
        $("#btn-download").removeClass('disabled');
    }
})
$("#btn-download").click(function() {
    var coverage = $("#coverage").val();
    var type = $("#type").val();
    var coverage_text = $('#coverage option:selected').html();
    $.ajax({
        url: '<?php echo base_url(); ?>Payroll_settings/downloadExcel_payroll_totalhours',
        method: 'POST',
        data: {
            coverage_id: coverage,
            type: type,
            coverage_text: coverage_text
        },
        beforeSend: function() {
            mApp.block('.m-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg',
                message: 'Downloading...'
            });
        },
        xhrFields: {
            responseType: 'blob'
        },
        success: function(data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'SZ_PayrollTotalHours.xlsx';
            a.click();
            window.URL.revokeObjectURL(url);
            mApp.unblock('.m-content');
        }
    });
})
</script>