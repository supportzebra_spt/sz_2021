<style type="text/css">
    .body{
        padding-right:0 !important;
    }
    .font-13{
        font-size:13px;
    }
    .font-12{
        font-size:12px;
    }
    .font-darkgray{
        color:#505a6b;
    }
    .bg-darkgray{
        background:#505a6b;
    }
    .swal2-title{
        font-family:'Poppins' !important;
    }
    .font-poppins{
        font-family:'Poppins' !important;
    }
    .m-content .m-stack__item{
        padding:0 !important;
        background:0 !important;
        border:0 !important;
        line-height: 1.2;
    }
    .card-shadow{
        box-shadow: 0 1px 8px rgba(0,0,0,0.15);
    }
    .form-control{
        font-size:12px;
    }
    ul#pagination li a,ul#pagination li span{
        padding:0px 5px;
        font-size:12px
    }
    .row.display-flex {
        display: flex;
        flex-wrap: wrap;
        margin-bottom:20px;
    }
    .row.display-flex > [class*='col-'] {
        flex-grow: 1;
    }

    /* only for demo not required */
    .box {
        /*border:1px #666 solid;*/
        height: 100%;
    }

    .row.display-flex [class*='col-'] {
        /*background-color: #cceeee;*/
    }

    .clickable-row{
        cursor: pointer;
    }
    .clickable-row:hover{
        background:#fdf5e9 !important;
        color:black !important;
    }
    .clickable-row:hover i{
        color:#ffb822;
    }
    .clickable-row.active i{
        color:#ffb822;
    }
    .clickable-row.active{
        background:#faebd7 !important;
        color:black !important;
    }
    .clickable-row > td{
        font-size:13px;
        vertical-align:middle;
    }
    /*    .popover-header{
            background:#505a6b !important;
            color:white !Important;
            padding:12px;
            font-size:13px;
            text-align:center;
        }*/
    .popover{
        border: 2px solid rgb(255, 184, 34);
    }
    /*NEW CSS ACCORDIONs*/
    .m-accordion__item-head span, .m-accordion__item-icon i{
        font-size:14px !important;
        font-weight:401;
    }
    .m-accordion__item-head{
        padding: 10px 15px !important;
    }
    .m-accordion__item-body{
        padding: 5px !important;
    }
    .m-accordion__item-content{
        padding:0 !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Payroll Settings</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('Settings/user_access_rights'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Rates</span>
                        </a>
                    </li>
                </ul>
            </div>
            <button class="btn btn-warning m-btn m-btn--icon" id="btn-modal-insert" data-toggle="modal" data-target="#modal-position" data-type="insert">
                <span>
                    <i class="fa fa-plus"></i> 
                    <span>New Position</span>
                </span>
            </button>
        </div>
    </div>

    <div class="m-content">
        <div class="card card-shadow">
            <div class="card-footer p-2 bg-darkgray">
                <div class="row" id="filters">
                    <div class="col-md-2">
                        <select class="form-control form-control-sm m-input" id="perpage">
                            <option>15</option>
                            <option>25</option>
                            <option>50</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control form-control-sm m-input" id="search-site">
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="" class="m-input form-control form-control-sm font-12" id="search-class">
                            <option value="">All Class</option>
                            <option value="Agent">Ambassador</option>
                            <option value="Admin">SZ Team</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="" class="m-input form-control form-control-sm font-12" id="search-departments">
                        </select>
                    </div>
                    <div class="col-md-3">
                        <div class="m-input-icon m-input-icon--right">
                            <input type="text" class="form-control form-control-sm m-input font-12" placeholder="Search Position..." id="search-position"/>
                            <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-search"></i></span></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-8">
                <div class="card card-shadow ">
                    <div class="card-header p-0">
                        <table class="table m-table table-sm table-striped m-0">
                            <thead class="bg-darkgray m--font-warning font-13">
                            <th class="text-center py-3" style="width:15%">Code</th>
                            <th class=" pl-1 py-3" style="width:50%">Position Name</th>
                            <th  class="text-center py-3"style="width:10%">Class</th>
                            <th  class="text-center py-3"style="width:10%">Site</th>
                            <th class="text-center py-3" style="width:10%">Hiring</th>
                            <th class="text-center py-3" style="width:5%"></th>
                            </thead>

                        </table>
                    </div>
                    <div class="card-body p-0">
                        <table id="card-container" class="table m-table table-sm table-striped m-0">
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer p-3">

                        <div class="row">
                            <div class="col-md-8"><ul id="pagination"></ul></div>
                            <div class="col-md-4 text-right"> <div style='font-size:12px;'><p>Showing <span id="count"></span>  of <span id="total"></span> records</p></div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-shadow">
                    <div class="card-header p-2" style="background: #505a6b;color: white">
                        <div class="m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-stack__item--center m-stack__item--middle" style="width: 70px;">
                                <button class="btn btn-outline-warning m-btn m-btn--icon btn-sm" id="btn-modal-update" data-toggle="modal" data-target="#modal-position" data-type="update" style="display:none">
                                    <span>
                                        <i class="fa fa-edit"></i>
                                        <span>Edit</span>
                                    </span>
                                </button>
                            </div>
                            <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                <p class="text-center m--font-bolder mb-0" style="font-size:14px" id='position-title'></p>
                            </div>

                        </div>

                    </div>
                    <div class="card-body">
                        <div class="m-alert m-alert--square alert alert-danger alert-dismissible fade show" role="alert" id="alert-rate">
                            <strong>No Record!</strong> <br />Select a position to view rates, allowances and bonuses.
                        </div>

                        <div class="m-accordion m-accordion--default m-accordion--solid" id="my_accordion" role="tablist" style="display:none">   

                            <!--begin::Item-->              
                            <div class="m-accordion__item  m-accordion__item--warning">
                                <div class="m-accordion__item-head collapsed" role="tab" id="my_accordion_head_rates" data-toggle="collapse" href="#my_accordion_body_rates" aria-expanded="    false">
                                    <span class="m-accordion__item-icon"><i class="fa flaticon-piggy-bank"></i></span>
                                    <span class="m-accordion__item-title">Rates</span>
                                    <span class="m-accordion__item-mode"></span>     
                                </div>

                                <div class="m-accordion__item-body collapse" id="my_accordion_body_rates" role="tabpanel" aria-labelledby="my_accordion_head_rates" data-parent="#my_accordion"> 
                                    <div class="m-accordion__item-content" class="m-scrollable" data-scrollable="true" data-max-height="200">

                                        <table class="table m-table table-bordered table-striped m-0" id='table-rate' style='font-size:12px;'>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--end::Item--> 

                            <!--begin::Item--> 
                            <div class="m-accordion__item  m-accordion__item--warning">
                                <div class="m-accordion__item-head collapsed" role="tab" id="my_accordion_head_allowance" data-toggle="collapse" href="#my_accordion_body_allowance" aria-expanded="    false">
                                    <span class="m-accordion__item-icon"><i class="fa flaticon-technology-1"></i></span>
                                    <span class="m-accordion__item-title">Allowances</span>

                                    <span class="m-accordion__item-mode"></span>     
                                </div>

                                <div class="m-accordion__item-body collapse" id="my_accordion_body_allowance" role="tabpanel" aria-labelledby="my_accordion_head_allowance" data-parent="#my_accordion"> 
                                    <div class="m-accordion__item-content" class="m-scrollable" data-scrollable="true" data-max-height="160">
                                        <div class="alert alert-brand" role="alert" id="subalert-allowance">
                                            <strong>No Record!</strong> <br />Assign rate first before you can assign allowances.
                                        </div>
                                        <table class="table m-table table-bordered table-hover m-0" id='table-allowance' style='font-size:12px;'>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>   
                            <!--end::Item--> 

                            <!--begin::Item--> 
                            <div class="m-accordion__item  m-accordion__item--warning">
                                <div class="m-accordion__item-head collapsed" role="tab" id="my_accordion_head_bonus" data-toggle="collapse" href="#my_accordion_body_bonus" aria-expanded="    false">
                                    <span class="m-accordion__item-icon"><i class="fa flaticon-gift"></i></span>
                                    <span class="m-accordion__item-title">Bonuses</span>

                                    <span class="m-accordion__item-mode"></span>     
                                </div>

                                <div class="m-accordion__item-body collapse" id="my_accordion_body_bonus" role="tabpanel" aria-labelledby="my_accordion_head_bonus" data-parent="#my_accordion"> 
                                    <div class="m-accordion__item-content" class="m-scrollable" data-scrollable="true" data-max-height="270">
                                        <div class="alert alert-brand" role="alert" id="subalert-bonus">
                                            <strong>No Record!</strong> <br />Assign rate first before you can assign bonuses.
                                        </div>
                                        <table class="table m-table table-bordered table-hover m-0" id='table-bonus' style='font-size:12px;'>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>                       
                            </div>
                            <!--end::Item-->                       
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-position" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Position Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-md-4">
                        <label for="">Code:</label>
                        <input type="text" class="form-control m-input" id="input-code"/>
                    </div>
                    <div class="col-md-8">
                        <label for="">Position Name:</label>
                        <input type="text" class="form-control m-input"  id="input-positionname"/>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <label for="">Is Hiring:</label>
                        <select class="form-control m-input" id="input-ishiring">
                            <option value="">--</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-md-8">
                        <label for="">Department:</label>
                        <select class="form-control m-input" id="input-department"></select>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6">
                        <label for="">Class:</label>
                        <select class="form-control m-input" id="input-class">
                            <option value="">--</option>
                            <option value="Agent">Ambassador</option>
                            <option value="Admin">SZ Team</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="">Site:</label>
                        <select class="form-control m-input" id="input-site">
                            <option value="">--</option>
                            <option value="CDO">CDO</option>
                            <option value="CEBU">CEBU</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" id="btn-insert">Create Position</button>
                <button type="button" class="btn btn-brand" id="btn-update">Save changes</button>
            </div>
        </div>
    </div>
</div>	
<div class="modal fade" id="modal-allowance" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Allowance Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info">Create Position</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-bonus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Bonus Modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info">Create Position</button>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js" ></script>
<script type="text/javascript">
        function get_all_sites(callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Payroll_settings/get_all_sites",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    callback(result);
                }
            });
        }
        function get_all_departments(callback) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Payroll_settings/get_all_departments",
                cache: false,
                success: function (result) {
                    result = JSON.parse(result.trim());
                    callback(result);
                }
            });
        }
        function getPositions(limiter, perPage, callback) {
            var searchposition = $("#search-position").val();
            var searchclass = $("#search-class").val();
            var searchdepartments = $("#search-departments").val();
            var searchsite = $("#search-site").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Payroll_settings/getPositions",
                data: {
                    limiter: limiter,
                    perPage: perPage,
                    searchposition: searchposition,
                    searchclass: searchclass,
                    searchdepartments: searchdepartments,
                    searchsite: searchsite
                },
                beforeSend: function () {
                    mApp.block('#content', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    $("#total").html(res.total);
                    $("#total").data('count', res.total);

                    $("#card-container tbody").html("");
                    var string = "";
                    $.each(res.positions, function (key, pos) {
                        var ishiring = (pos.isHiring === 'No') ? "<small>Not Hiring</small>" : "<span class='m--font-success m--font-bolder'>Hiring</span>";
                        string += '<tr class="clickable-row"  data-posid="' + pos.pos_id + '" data-position="' + pos.pos_details + '">'
                                + '<td class="text-center font-12" style="width:15%;font-stretch:condensed"><span class="row-code">' + pos.pos_name + '</span></td>'
                                + '<td style="width:50%"><span class="m--font-bolder pl-1 row-positionname font-darkgray font-12">' + pos.pos_details + '</span></td>'
                                + '<td class="text-center row-class"  style="width:10%;">' + pos.class + '</td>'
                                + '<td class="text-center row-site"  style="width:10%;">' + pos.site + '</td>'
                                + '<td class="text-center row-ishiring"  style="width:10%;">' + ishiring + '</td>'
                                + '<td class="text-center row-arrow"  style="width:5%;"><i class="fa fa-arrow-right"></i></td>'
                                + '</tr>';
                    });
                    if (string === '') {
                        string = '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand fade show" role="alert" id="alert-position"><div class="m-alert__icon"><i class="flaticon-exclamation-1"></i><span></span></div> <div class="m-alert__text"><strong>No Record!</strong> Please change your search criteria.	</div>'
                    }
                    $("#card-container tbody").html(string);
                    $("#card-container").parent().scrollTop(0);
                    var count = $("#total").data('count');
                    var result = parseInt(limiter) + parseInt(perPage);
                    if (count === 0) {
                        $("#count").html(0 + ' - ' + 0);
                    } else if (count <= result) {
                        $("#count").html((limiter + 1) + ' - ' + count);
                    } else if (limiter === 0) {
                        $("#count").html(1 + ' - ' + perPage);
                    } else {
                        $("#count").html((limiter + 1) + ' - ' + result);
                    }
                    mApp.unblock('#content');
                    callback(res.total);
                }, error: function () {
                    swal("Error", "Please contact admin", "error");
                    mApp.unblock('#content');
                }
            });
        }
        $(function () {

            get_all_departments(function (res) {
                $("#search-departments").html("<option value=''>All Departments</option>");
                $.each(res.departments, function (i, item) {
                    $("#search-departments").append("<option value='" + item.dep_id + "'>" + item.dep_details + "</option>");
                });
                $("#input-department").html("<option value=''>--</option>");
                $.each(res.departments, function (i, item) {
                    $("#input-department").append("<option value='" + item.dep_id + "'>" + item.dep_details + "</option>");
                });
            });
            get_all_sites(function (res) {
                $("#search-site").html("<option value=''>All Sites</option>");
                $.each(res.sites, function (i, item) {
                    $("#search-site").append("<option value='" + item.site_ID + "'>" + item.code + "</option>");
                });
                $("#input-site").html("<option value=''>--</option>");
                $.each(res.sites, function (i, item) {
                    $("#input-site").append("<option value='" + item.site_ID + "'>" + item.code + "</option>");
                });
            });
            $("#search-position,#search-departments,#search-class,#search-site").change(function () {
                $("#perpage").change();
            });
            $("#perpage").change(function () {
                $("#btn-modal-update").hide();
                $("#position-title").html("");
                $("#my_accordion").hide();
                $("#alert-rate").show();
                var perPage = $("#perpage").val();
                getPositions(0, perPage, function (total) {
                    $("#pagination").pagination('destroy');
                    $("#pagination").pagination({
                        items: total, //default
                        itemsOnPage: $("#perpage").val(),
                        hrefTextPrefix: "#",
                        cssStyle: 'light-theme',
                        displayedPages: 2,
                        onPageClick: function (pagenumber) {
                            var perPage = $("#perpage").val();
                            getPositions((pagenumber * perPage) - perPage, perPage);
                            $("#btn-modal-update").hide();
                            $("#position-title").html("");
                            $("#my_accordion").hide();
                            $("#alert-rate").show();
                        }
                    });
                });
            });
            $("#perpage").change();

            $("#card-container").on('click', 'tr.clickable-row', function () {
                var that = this;
                var pos_id = $(that).data('posid');
                $('#btn-modal-update').data('posid', pos_id);
                var position = $(that).data('position');
                if (pos_id === 'undefined' || pos_id === null) {
                    swal("No Position was Found");
                    $("#my_accordion").hide();
                    $("#alert-rate").show();
                } else {
                    $(".clickable-row").removeClass('active');
                    $(that).addClass('active');
                    $('.custom-link').popover('hide');
                    $('.custom-link-allowance').popover('hide');
                    $('.custom-link-bonus').popover('hide');
                    $("#position-title").html(position);
                    $("#my_accordion").data('pos_id', pos_id);
                    $(".m-accordion__item-body").addClass("collapse");
                    $(".m-accordion__item-body").removeClass("show");
                    $(".m-accordion__item-head").addClass("collapsed");
                    $("#my_accordion").show();
                    $("#alert-rate").hide();
                    $('#btn-modal-update').show();
                }

            });
            $("#my_accordion").on('show.bs.collapse', "#my_accordion_body_rates", function () {
                var that = this;
                var pos_id = $("#my_accordion").data('pos_id');
                if (pos_id === null || pos_id === undefined) {
                    swal("Error", "Please refresh page.", "error");
                } else {

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Payroll_settings/getStatusRates",
                        data: {
                            pos_id: pos_id
                        },
                        cache: false,
                        success: function (result) {
                            result = JSON.parse(result.trim());
                            var statuses = result.statuses;
                            $("#table-rate tbody").html("");
                            var string = "";
                            $.each(statuses, function (key, status) {
                                if (status.details === null) {
                                    string += '<tr>'
                                            + '<td style="vertical-align:middle"><a href="" class="m-link m-link--state m-link--info custom-link" data-posid="' + pos_id + '" data-empstatid="' + status.empstat_id + '"data-rate="" data-date="" data-rateid=""><i class="fa fa-plus"></i></a></td>'
                                            + '<td class="m--font-bolder">' + status.status + '</td>'
                                            + '<td><small class="m--font-danger">Empty</small></td>'
                                            + '</tr>';
                                } else {
                                    string += '<tr>'
                                            + '<td style="vertical-align:middle"><a href="" class="m-link m-link--state m-link--primary custom-link"  data-posid="' + pos_id + '" data-empstatid="' + status.empstat_id + '" data-rate="' + status.details.rate + '" data-date="' + status.details.date_effect + '" data-rateid="' + status.details.rate_id + '"><i class="fa fa-pencil"></i></a></td>'
                                            + '<td class="m--font-bolder">' + status.status + '</td>'
                                            + '<td>' + status.details.rate + '</td>'
                                            + '</tr>';
                                }

                            });


                            $("#table-rate tbody").html(string);

                            $(".custom-link").each(function (id, that) {
                                var rate = $(that).data('rate');
                                var date = $(that).data('date');
                                var pos_id = $(that).data('posid');
                                var empstat_id = $(that).data('empstatid');
                                var rate_id = $(that).data('rateid');
                                var buttons = '<div class="row no-gutters">'
                                        + '<div class="col-6"><button class="btn btn-outline-info btn-block btn-sm py-1 px-2 popover-approve" data-rateid="' + rate_id + '" data-empstatid="' + empstat_id + '" data-posid="' + pos_id + '" type="button"><i class="fa fa-check" style="font-size:20px"></i></button> </div>'
                                        + '<div class="col-6"><button class="btn btn-outline-danger btn-block btn-sm  py-1 px-2 popover-reject" type="button"><i class="fa fa-times"  style="font-size:20px"></i></button></div>'
                                        + '</div>';
                                var contentrate = '<input type="number" min=0 style="width:150px" name="" value="' + rate + '"  data-value="' + rate + '" class="form-control form-control-sm m-input input-rate"/><small class="notif-rate m--font-danger">Required*</small>';
                                var contentDate = '<input type="text"  name="' + id + '"  data-value="' + date + '" class="form-control custom-datepicker" style="width:150px"  readonly="" ' + date + '><small class="notif-date m--font-danger">Required*</small>';
                                $(that).popover({
                                    trigger: 'click',
                                    placement: 'bottom',
//                                    title: "Rate Details Changes",
                                    html: true,
                                    content: '<div class="p-2">Rate:' + contentrate + '<br /><br />Date Effective:' + contentDate + '<br /><br />' + buttons + '</div>'
                                }).on('click', function (e) {

                                    $('.custom-link').not(that).popover('hide');
                                    $('.custom-link-allowance').popover('hide');
                                    $('.custom-link-bonus').popover('hide');
                                    e.preventDefault();
                                }).on('inserted.bs.popover', function () {
                                    var popover_elem = $(this).data("bs.popover").tip;
                                    var input_elem = $(popover_elem).find('.custom-datepicker');
                                    input_elem.datepicker({
//                                        orientation: "bottom left",
                                        templates: {
                                            leftArrow: '<i class="la la-angle-left"></i>',
                                            rightArrow: '<i class="la la-angle-right"></i>'
                                        },
                                        format: 'yyyy-mm-dd'
                                    });
                                    input_elem.datepicker('update', new Date(date));
                                });
                            });
                        }
                    });
                }
            });
            $("#my_accordion").on('show.bs.collapse', "#my_accordion_body_allowance", function () {
                var that = this;
                var pos_id = $("#my_accordion").data('pos_id');
                if (pos_id === null || pos_id === undefined) {
                    swal("Error", "Please refresh page.", "error");
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Payroll_settings/getAllowances",
                        data: {
                            pos_id: pos_id
                        },
                        cache: false,
                        success: function (result) {
                            result = JSON.parse(result.trim());
                            var statuses = result.statuses;//values
                            var allowances = result.allowances;//allowance names
                            $("#table-allowance tbody").html("");
                            var string = "";
                            $.each(statuses, function (key1, status) {
                                string += '<tr style="background:#ddd"><td colspan=3 class="text-center m--font-bolder" >' + status.status + '</td></tr>';
                                $.each(allowances, function (key2, name) {
                                    var isOkay = false;
                                    $.each(status.details, function (key3, detail) {
                                        if (name === detail.allowance_name) {
                                            string += '<tr>'
                                                    + '<td style="vertical-align:middle"><a href="" class="m-link m-link--state m-link--primary custom-link-allowance" data-empstatid="' + status.empstat_id + '" data-allowance_name="' + name + '" data-posid="' + pos_id + '"data-value="' + detail.value + '"  data-allowance_id="' + detail.allowance_id + '"><i class="fa fa-pencil"></i></a></td>'
                                                    + '<td class="m--font-bolder">' + name + '</td>'
                                                    + '<td>' + detail.value + '</td>'
                                                    + '</tr>';
                                            isOkay = true;
                                        }
                                    });
                                    if (isOkay === false) {
                                        string += '<tr>'
                                                + '<td style="vertical-align:middle"><a href="" class="m-link m-link--state m-link--info custom-link-allowance" data-empstatid="' + status.empstat_id + '" data-allowance_name="' + name + '" data-posid="' + pos_id + '"  data-value="" data-allowance_id="" ><i class="fa fa-plus"></i></a></td>'
                                                + '<td class="m--font-bolder">' + name + '</td>'
                                                + '<td><small class="m--font-danger">Empty</small></td>'
                                                + '</tr>';
                                    }

                                });




                            });
                            if (string === '') {
                                $("#subalert-allowance").show();
                            } else {
                                $("#subalert-allowance").hide();
                            }
                            $("#table-allowance tbody").html(string);
                            $(".custom-link-allowance").each(function (id, that) {//data: posid,allwoance_name,value,allowance_id
                                var value = $(that).data('value');
                                var pos_id = $(that).data('posid');
                                var allowance_name = $(that).data('allowance_name');
                                var empstat_id = $(that).data('empstatid');
                                var allowance_id = $(that).data('allowance_id');
                                var buttons = '<div class="row no-gutters">'
                                        + '<div class="col-6"><button class="btn btn-outline-info btn-block btn-sm py-1 px-2 popover-approve-allowance" data-allowance_id="' + allowance_id + '" data-empstatid="' + empstat_id + '" data-posid="' + pos_id + '" data-allowance_name="' + allowance_name + '" type="button"><i class="fa fa-check" style="font-size:20px"></i></button> </div>'
                                        + '<div class="col-6"><button class="btn btn-outline-danger btn-block btn-sm  py-1 px-2 popover-reject-allowance" type="button"><i class="fa fa-times"  style="font-size:20px"></i></button></div>'
                                        + '</div>';
                                var contentallowance = '<input type="number" min=0 style="width:150px" name="" value="' + value + '"  data-value="' + value + '" class="form-control form-control-sm m-input input-allowance"/><small class="notif-allowance m--font-danger">Required*</small>';

                                $(that).popover({
                                    trigger: 'click',
                                    placement: 'bottom',
//                                    title: "Rate Details Changes",
                                    html: true,
                                    content: '<div class="p-2">Allowance Value:' + contentallowance + '<br /><br />' + buttons + '</div>'
                                }).on('click', function (e) {
                                    $('.custom-link').popover('hide');
                                    $('.custom-link-allowance').not(that).popover('hide');
                                    $('.custom-link-bonus').popover('hide');
                                    e.preventDefault();
                                });
                            });
                        }
                    });
                }
            });
            $("#my_accordion").on('show.bs.collapse', "#my_accordion_body_bonus", function () {
                var that = this;
                var pos_id = $("#my_accordion").data('pos_id');

                if (pos_id === null || pos_id === undefined) {
                    swal("Error", "Please refresh page.", "error");
                } else {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Payroll_settings/getBonuses",
                        data: {
                            pos_id: pos_id
                        },
                        cache: false,
                        success: function (result) {
                            result = JSON.parse(result.trim());
                            var statuses = result.statuses;//values
                            var bonuses = result.bonuses;//allowance names
                            $("#table-bonus tbody").html("");
                            var string = "";
                            $.each(statuses, function (key1, status) {
                                string += '<tr style="background:#ddd"><td colspan=3 class="text-center m--font-bolder" >' + status.status + '</td></tr>';
                                $.each(bonuses, function (key2, bonus) {
                                    var isOkay = false;
                                    $.each(status.details, function (key3, detail) {
                                        if (bonus.bonus_id === detail.bonus_id) {
                                            string += '<tr>'
                                                    + '<td style="vertical-align:middle"><a href="" class="m-link m-link--state m-link--primary custom-link-bonus"  data-empstatid="' + status.empstat_id + '"  data-posid="' + pos_id + '"data-amount="' + detail.amount + '"  data-bonus_id="' + detail.bonus_id + '" data-pbonus_id="' + detail.pbonus_id + '"><i class="fa fa-pencil"></i></a></td>'
                                                    + '<td class="m--font-bolder">' + bonus.bonus_name + '</td>'
                                                    + '<td>' + detail.amount + '</td>'
                                                    + '</tr>';
                                            isOkay = true;
                                        }
                                    });
                                    if (isOkay === false) {
                                        string += '<tr>'
                                                + '<td style="vertical-align:middle"><a href="" class="m-link m-link--state m-link--info custom-link-bonus"  data-empstatid="' + status.empstat_id + '" data-posid="' + pos_id + '" data-bonus_id="' + bonus.bonus_id + '" data-amount="" data-pbonus_id="" ><i class="fa fa-plus"></i></a></td>'
                                                + '<td class="m--font-bolder">' + bonus.bonus_name + '</td>'
                                                + '<td><small class="m--font-danger">Empty</small></td>'
                                                + '</tr>';
                                    }

                                });




                            });

                            if (string === '') {
                                $("#subalert-bonus").show();
                            } else {
                                $("#subalert-bonus").hide();
                            }
                            $("#table-bonus tbody").html(string);
                            $(".custom-link-bonus").each(function (id, that) {//data: posid,bonus_id,amount,pbonus_id
                                var amount = $(that).data('amount');
                                var pos_id = $(that).data('posid');
                                var empstat_id = $(that).data('empstatid');
                                var bonus_id = $(that).data('bonus_id');
                                var pbonus_id = $(that).data('pbonus_id');
                                var buttons = '<div class="row no-gutters">'
                                        + '<div class="col-6"><button class="btn btn-outline-info btn-block btn-sm py-1 px-2 popover-approve-bonus" data-bonus_id="' + bonus_id + '" data-pbonus_id="' + pbonus_id + '" data-empstatid="' + empstat_id + '" data-posid="' + pos_id + '" type="button"><i class="fa fa-check" style="font-size:20px"></i></button> </div>'
                                        + '<div class="col-6"><button class="btn btn-outline-danger btn-block btn-sm  py-1 px-2 popover-reject-bonus" type="button"><i class="fa fa-times"  style="font-size:20px"></i></button></div>'
                                        + '</div>';
                                var contentbonus = '<input type="number" min=0 style="width:150px" name="" value="' + amount + '"  data-value="' + amount + '" class="form-control form-control-sm m-input input-bonus"/><small class="notif-bonus m--font-danger">Required*</small>';

                                $(that).popover({
                                    trigger: 'click',
                                    placement: 'bottom',
//                                    title: "Rate Details Changes",
                                    html: true,
                                    content: '<div class="p-2">Bonus Amount:' + contentbonus + '<br /><br />' + buttons + '</div>'
                                }).on('click', function (e) {
                                    $('.custom-link').popover('hide');
                                    $('.custom-link-allowance').popover('hide');
                                    $('.custom-link-bonus').not(that).popover('hide');
                                    e.preventDefault();
                                });
                            });
                        }
                    });
                }
            });
            $("#my_accordion").on('hide.bs.collapse', ".m-accordion__item-body", function () {
                $('.custom-link').popover('hide');
                $('.custom-link-allowance').popover('hide');
                $('.custom-link-bonus').popover('hide');
            });
            $("html").on('click', '.custom-link,.custom-link-allowance,.custom-link-bonus', function (e) {
                e.preventDefault();
            });
            $("html").on('click', '.popover-reject,.popover-reject-allowance,.popover-reject-bonus', function () {
                $(this).closest('.popover').popover('hide');
            });
            $("html").on('click', '.popover-approve', function () {
                var that = this;
                var rate_elem = $(that).closest('.popover').find('.input-rate');
                var date_elem = $(that).closest('.popover').find('.custom-datepicker');
                var notif_rate = $(that).closest('.popover').find('.notif-rate');
                var notif_date = $(that).closest('.popover').find('.notif-date');
                var pos_id = $(that).data('posid');
                var empstat_id = $(that).data('empstatid');
                var rate_id = $(that).data('rateid');
                var rate = $(rate_elem).val();
                var date = $(date_elem).val();
                notif_rate.hide();
                notif_date.hide();
                var isOkay = true;
                if (rate === '') {
                    notif_rate.show();
                    isOkay = false;
                }
                if (date === '') {
                    notif_date.show();
                    isOkay = false;
                }
                if (isOkay === true) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Payroll_settings/saveRatesDetails",
                        data: {
                            empstat_id: empstat_id,
                            rate_id: rate_id,
                            pos_id: pos_id,
                            rate: rate,
                            date: date
                        },
                        cache: false,
                        success: function (result) {
                            result = JSON.parse(result.trim());
                            if (result.status === 'Success') {
                                $.notify({
                                    message: 'Successfully Updated'
                                }, {
                                    type: 'success',
                                    timer: 1000
                                });
                            } else {
                                $.notify({
                                    message: 'An error occured while processing your request'
                                }, {
                                    type: 'danger',
                                    timer: 1000
                                });
                            }

                            $("#my_accordion_body_rates").collapse("hide");
                            $("#my_accordion_body_rates").collapse("show");
                            $('.custom-link').popover('hide');
                        }
                    });
                }

            });
            $("html").on('click', '.popover-approve-allowance', function () {
                var that = this;
                var allowance_elem = $(that).closest('.popover').find('.input-allowance');
                var notif_allowance = $(that).closest('.popover').find('.notif-allowance');
                var pos_id = $(that).data('posid');
                var empstat_id = $(that).data('empstatid');
                var allowance_id = $(that).data('allowance_id');
                var allowance_name = $(that).data('allowance_name');
                var allowance = $(allowance_elem).val();
                notif_allowance.hide();
                var isOkay = true;
                if (allowance === '') {
                    notif_allowance.show();
                    isOkay = false;
                }
                if (isOkay === true) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Payroll_settings/saveAllowanceDetails",
                        data: {
                            empstat_id: empstat_id,
                            allowance_id: allowance_id,
                            pos_id: pos_id,
                            allowance: allowance,
                            allowance_name: allowance_name
                        },
                        cache: false,
                        success: function (result) {
                            result = JSON.parse(result.trim());
                            if (result.status === 'Success') {
                                $.notify({
                                    message: 'Successfully Updated'
                                }, {
                                    type: 'success',
                                    timer: 1000
                                });
                            } else {
                                $.notify({
                                    message: 'An error occured while processing your request'
                                }, {
                                    type: 'danger',
                                    timer: 1000
                                });
                            }

                            $("#my_accordion_body_allowance").collapse("hide");
                            $("#my_accordion_body_allowance").collapse("show");
                            $('.custom-link-allowance').popover('hide');
                        }
                    });
                }

            });
            $("html").on('click', '.popover-approve-bonus', function () {
                var that = this;
                var bonus_elem = $(that).closest('.popover').find('.input-bonus');
                var notif_bonus = $(that).closest('.popover').find('.notif-bonus');
                var pos_id = $(that).data('posid');
                var empstat_id = $(that).data('empstatid');
                var bonus_id = $(that).data('bonus_id');
                var pbonus_id = $(that).data('pbonus_id');
                var bonus = $(bonus_elem).val();
                notif_bonus.hide();
                var isOkay = true;
                if (bonus === '') {
                    notif_bonus.show();
                    isOkay = false;
                }
                if (isOkay === true) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Payroll_settings/saveBonusDetails",
                        data: {
                            empstat_id: empstat_id,
                            pbonus_id: pbonus_id,
                            bonus_id: bonus_id,
                            pos_id: pos_id,
                            bonus: bonus
                        },
                        cache: false,
                        success: function (result) {
                            result = JSON.parse(result.trim());
                            if (result.status === 'Success') {
                                $.notify({
                                    message: 'Successfully Updated'
                                }, {
                                    type: 'success',
                                    timer: 1000
                                });
                            } else {
                                $.notify({
                                    message: 'An error occured while processing your request'
                                }, {
                                    type: 'danger',
                                    timer: 1000
                                });
                            }

                            $("#my_accordion_body_bonus").collapse("hide");
                            $("#my_accordion_body_bonus").collapse("show");
                            $('.custom-link-bonus').popover('hide');
                        }
                    });
                }

            });
            $("#modal-position").on('show.bs.modal', function (e) {
                $('.custom-link').popover('hide');
                $('.custom-link-allowance').popover('hide');
                $('.custom-link-bonus').popover('hide');
                var type = $(e.relatedTarget).data('type');
                $("#input-code").val("");
                $("#input-positionname").val("");
                $("#input-ishiring").val("");
                $("#input-department").val("");
                $("#input-class").val("");
                $("#input-site").val("");
                if (type === 'update') {
                    var pos_id = $('#btn-modal-update').data('posid');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>Payroll_settings/getSinglePosition",
                        data: {
                            pos_id: pos_id
                        },
                        cache: false,
                        success: function (res) {
                            res = JSON.parse(res.trim());
                            var position = res.position;
                            if (position !== null) {
                                $("#input-code").val(position.pos_name);
                                $("#input-positionname").val(position.pos_details);
                                $("#input-ishiring").val(position.isHiring);
                                $("#input-department").val(position.dep_id);
                                $("#input-class").val(position.class);
                                $("#input-site").val(position.site_ID);
                            }
                        }
                    });
                    $("#btn-update").show();
                    $("#btn-insert").hide();
                    $("#btn-update").data('posid', pos_id);
                } else {
                    $("#btn-insert").show();
                    $("#btn-update").hide();
                }
            });
            $("#btn-insert,#btn-update").click(function () {
                var pos_id = $(this).data('posid');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Payroll_settings/savePosition",
                    data: {
                        pos_id: pos_id,
                        code: $("#input-code").val(),
                        positionname: $("#input-positionname").val(),
                        ishiring: $("#input-ishiring").val(),
                        department: $("#input-department").val(),
                        class: $("#input-class").val(),
                        site_ID: $("#input-site").val()
                    },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $.notify({
                                message: 'Successfully ' + res.from + ' position'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                            if (res.from === 'updated') {
                                var ishiring = ($("#input-ishiring").val() === 'No') ? "<small>Not Hiring</small>" : "<span class='m--font-success m--font-bolder'>Hiring</span>";
                                var elem = $(".clickable-row[data-posid=" + pos_id + "]");
                                $(elem).find('.row-code').html($("#input-code").val());
                                $(elem).find('.row-positionname').html($("#input-positionname").val());
                                $(elem).find('.row-department').html($("#input-department  option:selected").text());
                                $(elem).find('.row-class').html($("#input-class").val());
                                $(elem).find('.row-site').html($("#input-site  option:selected").text());
                                $(elem).find('.row-ishiring').html(ishiring);
                                $("#position-title").html($("#input-positionname").val());
                            }
                        } else {
                            $.notify({
                                message: 'An error occured while processing your request'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }
                        $("#modal-position").modal('hide');
                    }
                });
            });
        });
</script>