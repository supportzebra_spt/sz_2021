<style type="text/css">
label {
    font-weight: 401;
    font-size: 14px;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">System Settings</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('payroll_settings/page'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Payroll Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
            <button class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#holidayModal">
                <span>
                    <i class="fa fa-plus-circle"></i>
                    <span>New Holiday</span>
                </span>
            </button>
        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--bordered m-portlet--rounded">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-link"></i>
                                </span>
                                <h3 class="m-portlet__head-text" style='color: #34bfa3 !important; '>
                                    Philippine Holidays
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="" id="search-types" class='form-control m-input'>
                                        <option value="">All Holiday Types</option>
                                        <option value="Regular">Regular Only</option>
                                        <option value="Special">Special Only</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select name="" id="search-sites" class='form-control m-input'>
                                        <option value="">All Sites</option>
                                        <?php foreach($sites as $site):?>
                                        <option value="<?php echo $site->site_ID?>"><?php echo $site->code?> Only
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <table class="table table-bordered table-hover table-sm" id="table-holiday">
                            <thead>
                                <th>Holiday Name</th>
                                <th>Date</th>
                                <th>Type</th>
                                <th>Site</th>
                                <th>Description</th>
                                <th>Action</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="holidayModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Holidays</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger fade show" role="alert" id="modalError" style="display:none">
                    <strong>Incomplete Form! </strong> Please complete the form to proceed.
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Holiday Name:</label>
                        <input type="text" class="form-control m-input" id="holiday-name" /><br />
                    </div>
                    <div class="col-md-12">
                        <label for="">Description:</label>
                        <input type="text" class="form-control m-input" id="holiday-description" /><br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">


                        <label for="">Date of Holiday:</label>
                        <!-- <input type="text" class="form-control" id="holiday-date" readonly="" placeholder="Select date"> -->
                        <div class id="holiday-date"></div>

                    </div>
                    <div class="col-md-4">
                        <div class="col">
                            <label for="">Holiday Type:</label>
                            <div class="m-radio-list">
                                <label class="m-radio">
                                    <input type="radio" name="holiday-type" value="Regular"> Regular
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="holiday-type" value="Special"> Special
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <br>

                        <div class="col">
                            <label for="">Site:</label>
                            <div class="m-checkbox-list">
                                <?php foreach($sites as $site):?>
                                <label class="m-checkbox">
                                    <input type="checkbox" name="holiday-site"
                                        value="<?php echo $site->site_ID?>"><?php echo $site->code?>
                                    <span></span>
                                </label>
                                <?php endforeach; ?>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id='btn-createholiday'>Add Holiday</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function getMasterHolidays() {
    var searchType = $("#search-types").val();
    var searchSite = $("#search-sites").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Payroll_settings/get_master_holidays",
        data: {
            searchType: searchType,
            searchSite: searchSite
        },
        beforeSend: function() {
            mApp.block('.m-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'brand',
                size: 'lg'
            });
        },
        cache: false,
        success: function(result) {
            var holidays = JSON.parse(result.trim());
            $("#table-holiday tbody").html("");
            var string = "";
            var isOdd = false;
            var prevMonth = null;
            $.each(holidays, function(i, item) {
                if (moment(item.date).month() != prevMonth) {
                    isOdd = !isOdd;
                    prevMonth = moment(item.date).month();
                }
                string += `<tr style="background:${isOdd?"#efefef":"#fafafa"}">\
                                    <td class="m--font-bold m--font-info" style="width:250px;text-transform:capitalize">${item.holiday}</td>\
                                    <td class="m--font-bolder">${moment(item.date).format("MMMM DD")}</td>\
                                    ${item.type=="Regular"?'<td class="m--font-success m--font-bold">'+item.type+'</td>':'<td class="m--font-warning  m--font-bold">'+item.type+'</td>'}\
                                    <td>${item.formattedSites}</td>\
                                    <td style="width:250px;color:#888">${item.description}</td>\
                                    <td style="width:100px;">
                                    <a href="#" class="btn btn-outline-danger m-btn btn-sm m-btn--icon btn-remove" data-holidayid="${item.holiday_id}">
                                    <span>
                                        <i class="la la-trash"></i>
                                        <span>Remove</span>
                                        </span>
                                    </a></td>\
                                    </tr>`;
            });
            $("#table-holiday tbody").html(string);
            mApp.unblock('.m-content');
        }
    });
}

var removeYearFromHeader = () => {
    let $datepickerHeader = $("#holiday-date").find(".datepicker-switch");
    let currentHeader = $datepickerHeader.text().split(" ");
    if (currentHeader.length != 1) {
        $datepickerHeader.text(currentHeader.slice(0, -1).join(" "));
    }
}

$(function() {


    getMasterHolidays();
    $('#holiday-date').datepicker({
        todayHighlight: false,
        orientation: "bottom left",
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        },
        format: "MM dd",
        maxViewMode: 1,
    }).on('changeDate', () => {
        removeYearFromHeader();
    }).on('changeMonth', () => {
        removeYearFromHeader()
    }).on('changeYear', () => {
        removeYearFromHeader()
    }).on('show', () => {
        removeYearFromHeader()
    });


});
$("#search-types,#search-sites").change(function() {
    getMasterHolidays();
});
$("#holidayModal").on("show.bs.modal", function(e) {
    removeYearFromHeader()
})
$("#holidayModal").on('hide.bs.modal', function(e) {
    $("#holiday-name").val('');
    $("#holiday-description").val('');
    // $("[name='holiday-site']").siblings('span:after').css('display','none');
    // $("[name='holiday-type']").siblings('span:after').css('display','none');
    $("#holiday-date").val("").datepicker("update");
    $("#modalError").hide();

});
$("#btn-createholiday").click(function() {
    var that = this;
    var isOkayName = $("#holiday-name").val() == '' ? false : true;
    var isOkayDesc = $("#holiday-description").val() == '' ? false : true;
    var isOkayType = $("[name='holiday-type']:checked").length == 0 ? false : true;
    var isOkaySite = $("[name='holiday-site']:checked").length == 0 ? false : true;
    var isOkayDate = moment($("#holiday-date").datepicker("getDate")).isValid();

    if (isOkayName && isOkayDesc && isOkayType && isOkaySite && isOkayDate) {
        $("#modalError").hide();
        var holiday_name = $("#holiday-name").val();
        var holiday_desc = $("#holiday-description").val();
        var holiday_type = $("[name='holiday-type']:checked").val();
        var holiday_date = moment($("#holiday-date").datepicker("getDate")).format("YYYY-MM-DD");
        var holiday_site = $("[name='holiday-site']:checked").map((index, item) => {
            return $(item).val();
        }).get().toString();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Payroll_settings/createMasterHoliday",
            data: {
                holiday_name: holiday_name,
                holiday_desc: holiday_desc,
                holiday_type: holiday_type,
                holiday_date: holiday_date,
                holiday_site: holiday_site
            },
            beforeSend: function() {
                mApp.block('#holidayModal', {
                    overlayColor: '#000000',
                    type: 'loader',
                    state: 'brand',
                    size: 'lg'
                });
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                if (res.status === 'Success') {
                    $.notify({
                        message: 'Succesfully added the holiday.'
                    }, {
                        type: 'success',
                        timer: 1000
                    });
                } else {
                    $.notify({
                        message: 'An error occured while adding the holiday.'
                    }, {
                        type: 'danger',
                        timer: 1000
                    });
                }
                $("#holidayModal").modal('hide');
                mApp.unblock('#holidayModal');
                getMasterHolidays();
            }
        });
    } else {
        $("#modalError").show();
    }

});
$("#table-holiday").on("click", ".btn-remove", function(e) {
    var that = this;
    e.preventDefault();
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, remove it!'
    }).then(function(result) {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Payroll_settings/removeMasterHoliday/" + $(that)
                    .data('holidayid'),
                beforeSend: function() {
                    mApp.block('.m-content', {
                        overlayColor: '#000000',
                        type: 'loader',
                        state: 'brand',
                        size: 'lg'
                    });
                },
                cache: false,
                success: function(res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        $.notify({
                            message: 'Succesfully removed the holiday.'
                        }, {
                            type: 'success',
                            timer: 1000
                        });
                        $(that).closest('tr').remove();
                    } else {
                        $.notify({
                            message: 'An error occured while removing the holiday.'
                        }, {
                            type: 'danger',
                            timer: 1000
                        });
                    }
                    mApp.unblock('.m-content');
                }
            });
        }
    });
})
</script>