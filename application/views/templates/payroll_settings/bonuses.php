<style type="text/css">
    label{
        font-weight:401;
        font-size:14px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator font-poppins">System Settings</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url('dashboard'); ?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('payroll_settings/page'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Payroll Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
        	<button class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#bonusModal" data-type='insert'>
                <span>
                    <i class="fa fa-plus-circle"></i>
                    <span>New Bonus</span>
                </span>
            </button>
        </div>
    </div>

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <div class="m-portlet m-portlet--bordered m-portlet--rounded">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="fa fa-link"></i>
                                </span>
                                <h3 class="m-portlet__head-text" style='color: #34bfa3 !important; '>
                                    Payroll Bonuses
                                </h3>
                            </div>			
                        </div>
                        <div class="m-portlet__head-tools">
	                        <div class="row">
                        		<div class="col-md-6">
                    			    <select name="" id="search-status" class='form-control m-input'>
		                                <option value="">All Status</option>
		                                <option value="1">Active Only</option>
		                                <option value="0">Inactive Only</option>
		                            </select>
                        		</div>
                        		<div class="col-md-6">
                    			    <select name="" id="search-taxable" class='form-control m-input'>
		                                <option value="">All Bonuses</option>
		                                <option value="1">Taxable Bonuses</option>
		                                <option value="0">Non-taxable Bonuses</option>
		                            </select>
                        		</div>
	                        </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <table class="table table-bordered table-hover table-sm" id="table-bonus">
                            <thead>
	                            <th>Bonus Name</th>
	                            <th>Description</th>
	                            <th class='text-center'>is this taxable ?</th>
	                            <th class='text-center'>Status</th>
	                            <th class='text-center'>Actions</th>
                            </thead>
                            <tbody></tbody>
                        </table>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="bonusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bonuses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Bonus Name:</label>
                        <input type="text" class="form-control m-input" style="background:#eee" id="bonus-name" /><br />
                    </div>
                    <div class="col-md-12">
                        <label for="">Description:</label>
                        <input type="text" class="form-control m-input" style="background:#eee" id="bonus-description" /><br />
                    </div>
					<br>
                    <div class="col-md-6">
                        <label for="">Taxable:</label>
                         <select name="" id="bonus-taxable" class='form-control m-input'>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="">Status:</label>
                         <select name="" id="bonus-status" class='form-control m-input'>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id='btn-createbonus'>Create Bonus</button>
                <button type="button" class="btn btn-warning" id='btn-updatebonus'>Update Bonus</button>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">
        function getMasterBonuses() {
            var searchStatus = $("#search-status").val();
            var searchTaxable = $("#search-taxable").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Payroll_settings/get_master_bonuses",
                data: {
                   	searchStatus : searchStatus,
                	searchTaxable: searchTaxable
                },
                beforeSend: function() {
	                mApp.block('.m-content', {
	                    overlayColor: '#000000',
	                    type: 'loader',
	                    state: 'brand',
	                    size: 'lg'
	                });
	            },
                cache: false,
                success: function (result) {
                    var bonuses = JSON.parse(result.trim());
                    $("#table-bonus tbody").html("");
                    var string = "";
                    $.each(bonuses, function (i, item) {
                        string += `<tr>\
                        			<td>${item.bonus_name}</td>\
                        			<td>${item.description}</td>\
                        			<td class='text-center' style='color:${parseInt(item.isTaxable)?"#FBBC05":"#4285F4"}'>${parseInt(item.isTaxable)?"Yes":"No"}</td>\
                        			<td class='text-center' style='color:${parseInt(item.isActive)?"#34A853":"#EA4335"}'>${parseInt(item.isActive)?"Active":"Inactive"}</td>\
                        			<td class='text-center'><a href="#" class="btn btn-outline-brand m-btn btn-sm m-btn--icon" data-toggle="modal" data-target="#bonusModal" data-type='update' data-bonus_id='${item.bonus_id}' data-bonus_name='${item.bonus_name}' data-bonus_desc='${item.description}' data-bonus_status='${item.isActive}' data-bonus_taxable='${item.isTaxable}'>
									<span>
										<i class="la la-pencil"></i>
										<span>Edit</span>
										</span>
									</a></td>\
                        			</tr>`;
                    });
                    $("#table-bonus tbody").html(string);
                    mApp.unblock('.m-content');
                }
            });
        }
        $(function () {
            getMasterBonuses();
        });
        $("#search-taxable,#search-status").change(function () {
            getMasterBonuses();
        });
        $("#bonusModal").on('hide.bs.modal', function (e) {
            $("#bonus-name").val('');
            $("#bonus-description").val('');
        });
        $("#bonusModal").on('show.bs.modal', function (e) {
            var type = $(e.relatedTarget).data('type'); //insert,update
            console.log(type);
            if (type === 'insert') {
                $("#btn-createbonus").show();
                $("#btn-updatebonus").hide();
            console.log("CREATE");
            $("#bonus-name,#bonus-description").removeAttr('readonly',"");
            } else {
                var bonus_id = $(e.relatedTarget).data('bonus_id');
                var bonus_name = $(e.relatedTarget).data('bonus_name');
                var bonus_desc = $(e.relatedTarget).data('bonus_desc');
                var bonus_status = $(e.relatedTarget).data('bonus_status');
                var bonus_taxable = $(e.relatedTarget).data('bonus_taxable');
                $("#btn-updatebonus").data('bonus_id', bonus_id);
	          	$("#bonus-name").val(bonus_name);
		        $("#bonus-description").val(bonus_desc);
	          	$("#bonus-status").val(bonus_status);
		        $("#bonus-taxable").val(bonus_taxable);
                $("#btn-createbonus").hide();
                $("#btn-updatebonus").show();

            console.log("UPDATE");
            $("#bonus-name,#bonus-description").attr('readonly',"");
            }
        });
        $("#btn-createbonus").click(function () {
            var that = this;
            var bonus_name = $("#bonus-name").val();
            var bonus_desc = $("#bonus-description").val();
            var bonus_taxable = $("#bonus-taxable").val();
            var bonus_status = $("#bonus-status").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Payroll_settings/createMasterBonus",
                data: {
                    bonus_name: bonus_name,
                    bonus_desc: bonus_desc,
                    bonus_taxable:bonus_taxable,
                    bonus_status:bonus_status
                },
                 beforeSend: function() {
	                mApp.block('#bonusModal', {
	                    overlayColor: '#000000',
	                    type: 'loader',
	                    state: 'brand',
	                    size: 'lg'
	                });
	            },
                cache: false,
                success: function (res) {
                    res = JSON.parse(res.trim());
                    if (res.status === 'Success') {
                        $.notify({
                            message: 'Succesfully created the bonus.'
                        }, {
                            type: 'success',
                            timer: 1000
                        });
                    } else {
                        $.notify({
                            message: 'An error occured while creating the bonus.'
                        }, {
                            type: 'danger',
                            timer: 1000
                        });
                    }
                    $("#bonusModal").modal('hide');
                    mApp.unblock('#bonusModal');
            		getMasterBonuses();
                }
            });
        });
        $("#btn-updatebonus").click(function () {
            var that = this;
            var bonus_id = $(that).data('bonus_id');
            var bonus_name = $("#bonus-name").val();
            var bonus_desc = $("#bonus-description").val();
            var bonus_taxable = $("#bonus-taxable").val();
            var bonus_status = $("#bonus-status").val();

            if ($.trim(bonus_name) === '' || $.trim(bonus_desc) === '') {
                $.notify({
                    message: 'Bonus name and description cannot be empty.'
                }, {
                    type: 'warning',
                    timer: 1000,
                    z_index: 99999
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>Payroll_settings/updateMasterBonus",
                    data: {
                    	bonus_id: bonus_id,
                    	bonus_name:bonus_name,
                        bonus_desc:bonus_desc,
                        bonus_taxable:bonus_taxable,
                        bonus_status:bonus_status
                    },
                     beforeSend: function() {
		                mApp.block('#bonusModal', {
		                    overlayColor: '#000000',
		                    type: 'loader',
		                    state: 'brand',
		                    size: 'lg'
		                });
		            },
                    cache: false,
                    success: function (res) {
                        res = JSON.parse(res.trim());
                        if (res.status === 'Success') {
                            $.notify({
                                message: 'Succesfully updated the bonus details.'
                            }, {
                                type: 'success',
                                timer: 1000
                            });
                           
                        } else {
                            $.notify({
                                message: 'An error occured while updating the bonus.'
                            }, {
                                type: 'danger',
                                timer: 1000
                            });
                        }
                        $("#bonusModal").modal('hide');
                    	mApp.unblock('#bonusModal');
                        getMasterBonuses();
                    }
                });
            }
        });
</script>