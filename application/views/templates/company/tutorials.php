<style type="text/css">
    .card-shadow {
        box-shadow: 0 1px 8px rgba(0, 0, 0, 0.15);
    }

    #video-list .overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .3s ease;
        background-color: black;
    }

    #video-list .card-body:hover .overlay {
        opacity: 0.5;
    }

    /* The icon inside the overlay is positioned in the middle vertically and horizontally */
    #video-list .icon {
        color: white;
        font-size: 0px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    /* When you move the mouse over the icon, change color */
    #video-list .fa {
        font-size: 70px;
        color: #eee;
    }

    #video-list .fa:hover {
        color: #fff;
    }

    #iframe-container {
        position: relative;
        width: 100%;
        height: 0;
        padding-bottom: 56.25%;
    }

    #tutorial-iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    #tutorial-close {
        color: white;
    }

    #tutorial-close:hover {
        color: #ccc !important;
    }
    video:focus{
        outline:none;
    }
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content pb-0">
        <div class="m-portlet">
            <div class="m-portlet__head" style="background: #2c2e3eeb;">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="fa fa-film"></i>
                        </span>
                        <h3 class="m-portlet__head-text"  style="color: #34bfa3 !important; ">
                            Tutorial Videos
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <div class="row">
                        <div class="col-6">
                            <input type="text" class="form-control m-input" id="search-titledesc" placeholder="Search title or description">
                        </div>
                        <div class="col-6">
                            <select name="" class="form-control m-input" id="search-category">
                                <option value="">All</option>
                                <option value="AHR" data-color="rgb(29, 114, 208)">AHR</option>
                                <option value="Chat" data-color="#203342">Chat</option>
                                <option value="DMS" data-color="#F87D6E">DMS</option>
                                <option value="EMS" data-color="rgb(57, 129, 48)">EMS</option>
                                <option value="FAQ" data-color="#5E8DD3">FAQ</option>
                                <option value="Leave" data-color="rgb(112, 56, 169)">Leave</option>
                                <option value="PMS" data-color="#EE7193">PMS</option>
                                <option value="SnapSZ" data-color="rgb(39, 156, 111)">SnapSZ</option>
                                <option value="TITO" data-color="rgb(199, 129, 4)">TITO</option>
                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div id="video-list" class="row no-gutters">

                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="modal-tutorial-play" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="background:#030000;">
            <div class="modal-header p-2 m-0" style="border-bottom:none;background:#FF779D">
                <h5 id="tutorial-title" class="m-2" style="color:white"></h5>
                <a href="#" onclick="event.preventDefault()" id="tutorial-close" data-dismiss="modal" class="mb-0" style="position:absolute;top:5px;right:10px;"><i class="fa fa-times" style="font-size:30px"></i></a>

            </div>
            <div class="modal-body p-0 m-0">
                <div id="iframe-container">
                   <!--  <iframe src="" id="tutorial-iframe" frameborder="0" scrolling="no" allow="autoplay; encrypted-media" seamless="" allowfullscreen autoplay></iframe> -->
                     <video  id="tutorial-iframe" controls  style="width:100%; height:450px; border:none; margin:0 !important; padding:0 !important;background:#444">
                   <!--    <source src type="video/mp4">
                      Your browser does not support the video tag. -->
                    </video>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var getTutorialVideos = function() {
        var search_string = $("#search-titledesc").val();
        var search_category = $("#search-category").val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Company/get_demo_videos",
            data: {
                search_string: search_string,
                search_category: search_category
            },
            cache: false,
            success: function(res) {
                res = JSON.parse(res.trim());
                $("#video-list").html('');
                var string = "";
                $.each(res.videos, function(i, obj) {
                    //col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3
                    string += '<div class="col-12 col-sm-6 col-md-4 col-lg-6 col-xl-4 ">\
                        <div style="background:#white;" class="px-3 pt-3 pb-0">\
                                    <div class="card card-shadow mb-0" style="background:#030000;border-radius:0">\
                                        <div class="card-body p-0">\
                                            <img style="height: 165px; width: 100%; object-fit: contain" src="<?php echo base_url(); ?>' + obj.thumbnail + '" alt="">\
                                            <div class="overlay">\
                                                <a href="#" class="icon tutorial-play"  title="' + obj.title + '" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modal-tutorial-play" data-drive_link="' + obj.drive_link + '" data-category="' + obj.category + '" data-title="' + obj.title + '" data-link="' + obj.link + '" data-thumbnail="'+obj.thumbnail+'">\
                                                <i class="fa fa-play-circle"></i>\
                                                </a>\
                                             </div>\
                                        </div>\
                                    </div>\
                                    <div class="py-2" style="background:#white;">\
                                    <p class="mb-0 cut-text m--font-bolder" style="font-size:12px;" title="' + obj.title + '">' + obj.title + '</p>\
                                    <p class="mb-0 cut-text" style="font-size:11px;color:#777" title="' + obj.description + '">' + obj.description + '</p>\
                                   </div>\
                                    </div>\
                                    </div>';
                });
                $("#video-list").html(string);
            }
        });
    }
    $(function() {
        getTutorialVideos();
    });
    $("#search-titledesc").on('keyup', function() {
        getTutorialVideos();
    });
    $("#search-category").change(function() {
        getTutorialVideos();
    });
    $("#video-list").on("click", ".tutorial-play", function(e) {
        e.preventDefault();

    })
    $("#modal-tutorial-play").on('show.bs.modal', function(e) {
        if (e.namespace === 'bs.modal') {
            var title = $(e.relatedTarget).data('title');
            var link = $(e.relatedTarget).data('link');
            var thumbnail = $(e.relatedTarget).data('thumbnail');
            var category = $(e.relatedTarget).data('category');
            var drive_link = $(e.relatedTarget).data('drive_link');
            var color = $('#search-category option[value="' + category + '"]').data('color');

            if (color === null || color === undefined) {
                $("#modal-tutorial-play").find('.modal-header').css('background', '#000000');
            } else {
                $("#modal-tutorial-play").find('.modal-header').css('background', color);
            }
            // $("#tutorial-iframe").attr('src', "http://10.200.101.250/_sz/" + link);
            $("#tutorial-iframe").attr('src', "<?php echo base_url() ?>" + link);
            $("#tutorial-iframe").attr('poster', "<?php echo base_url() ?>" + thumbnail);
            var drive_link_elem = '<a href="' + drive_link + '" class="m-link" style="color:white" target="_blank" title="External View"><i class="fa fa-external-link"></i></a>'
            $("#tutorial-title").html(title + "  &nbsp;" + drive_link_elem);
        }
    })
    $("#modal-tutorial-play").on('hide.bs.modal', function(e) {
        if (e.namespace === 'bs.modal') {
            $("#tutorial-iframe").attr('src', '');
            $("#tutorial-title").html('');
        }
    });
</script>
<!-- <div class="card-footer px-3" style="background:white;">\
                                            <p class="mb-0 cut-text m--font-bolder" style="font-size:12px;">' + obj.title + '</p>\
                                        </div>\ -->