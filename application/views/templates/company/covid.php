<style type="text/css">
/*.carousel-item img{
    background:black;
    height:300px;
    object-fit: contain;
}*/
.lb-image{
    background:black
}
.lighbox-img .card-body{
    background:white;
}
video:focus{
    outline:none;
}
}
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <div class="m-content pb-0">
        <div class="m-portlet">
            <div class="m-portlet__head" style="background: #2c2e3eeb;">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-unlock"></i>
                        </span>
                        <h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
                           Covid 19 Info Drive
                        </h3>
                    </div>          
                </div>
            </div>
            <div class="m-portlet__body" style="padding:0 !important;margin:0 !important">
              <video id="myvideo" controls style="width:100%; height:450px; border:none; margin:0 !important; padding:0 !important;background:#444" src="<?php echo base_url();?>uploads/others/Covid19InfoDrive.mp4" poster="<?php echo base_url();?>uploads/others/covid19infodrivethumbnail.png">
                </video>
            </div>
        </div>
   
    </div>
</div>

<script>
    $(function() {
        $("#myvideo").hover(function(event) {
        if(event.type === "mouseenter") {
            $(this).attr("controls", "");
        } else if(event.type === "mouseleave") {
            $(this).removeAttr("controls");
        }
    });
    });
</script>