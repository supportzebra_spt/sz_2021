<style type="text/css">
/*.carousel-item img{
    background:black;
    height:300px;
    object-fit: contain;
}*/
.lb-image{
    background:black
}
.lighbox-img .card-body{
    background:white;
}
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
    <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/Helix.png') ?>" data-lightbox="roadtrip" >
           <div class="card">
                <div class="card-body">
                     <img  src="<?php echo base_url('assets/images/img/corevalues/Helix.png') ?>" class="card-img-top" style="background: #222">
                </div>
            </div>
        </a>
        <br>
    <div class="row">
        <div class="col-4">
             <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/mission.jpg') ?>" data-lightbox="roadtrip">
            <div class="card">
                <div class="card-body">
                     <img src="<?php echo base_url('assets/images/img/corevalues/mission.jpg') ?>" class="card-img-top">
                </div>
            </div>
        </a>
        </div>
        <div class="col-4">
            <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/VISION.jpg') ?>" data-lightbox="roadtrip">
            <div class="card">
                <div class="card-body">
                     <img src="<?php echo base_url('assets/images/img/corevalues/VISION.jpg') ?>" class="card-img-top">
                </div>
            </div>
        </a>
        </div>

        <div class="col-4">
            <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/Core.jpg') ?>" data-lightbox="roadtrip">
                <div class="card"> 
                    <div class="card-body">
                         <img src="<?php echo base_url('assets/images/img/corevalues/Core.jpg') ?>"data-lightbox="roadtrip" class="card-img-top">
                    </div>
                </div>
            </a>
        </div>
    </div> 
        <br>  
    <div class="row">
        <div class="col-4">
             <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/Family.jpg') ?>" data-lightbox="roadtrip">
                <div class="card">
                    <div class="card-body">
                         <img src="<?php echo base_url('assets/images/img/corevalues/Family.jpg') ?>" class="card-img-top">
                    </div>
                </div>
            </a>
        </div>

        <div class="col-4">
             <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/Wow.jpg') ?>" data-lightbox="roadtrip">
                <div class="card">
                    <div class="card-body">
                         <img src="<?php echo base_url('assets/images/img/corevalues/Wow.jpg') ?>" class="card-img-top">
                    </div>
                </div>
            </a>
        </div>
        <div class="col-4">
            <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/Integrity.jpg') ?>" data-lightbox="roadtrip">   
                <div class="card"> 
                    <div class="card-body">
                         <img src="<?php echo base_url('assets/images/img/corevalues/Integrity.jpg') ?>"data-lightbox="roadtrip" class="card-img-top">
                    </div>
                </div>
            </a>
        </div>
    </div>   
    <br>    
    <div class="row">

       

        <div class="col-4">
            <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/Generosity.jpg') ?>" data-lightbox="roadtrip">
                <div class="card"> 
                    <div class="card-body">
                         <img src="<?php echo base_url('assets/images/img/corevalues/Generosity.jpg') ?>" class="card-img-top">
                    </div>
                </div>
            </a>
        </div>
          <div class="col-4">
              <a class="lighbox-img" href="<?php echo base_url('assets/images/img/corevalues/Positivity.jpg') ?>" data-lightbox="roadtrip">
                <div class="card"> 
                    <div class="card-body">
                         <img src="<?php echo base_url('assets/images/img/corevalues/Positivity.jpg') ?>"data-lightbox="roadtrip"  class="card-img-top">
                    </div>
                </div>
            </a>
        </div>
    </div>     
    </div>
  
        <!-- <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
               <img src="<?php echo base_url('assets/images/img/corevalues/mission.jpg') ?>" class="d-block w-100">
            </div>
            <div class="carousel-item">
               <img src="<?php echo base_url('assets/images/img/corevalues/Helix.png') ?>"  class="d-block w-100">
            </div>
            <div class="carousel-item">
                <img src="<?php echo base_url('assets/images/img/corevalues/Positivity.jpg') ?>"  class="d-block w-100">
            </div>
            <div class="carousel-item">
                <img src="<?php echo base_url('assets/images/img/corevalues/Core.jpg') ?>"  class="d-block w-100">
            </div>
            <div class="carousel-item">
               <img src="<?php echo base_url('assets/images/img/corevalues/Family.jpg') ?>"  class="d-block w-100">
            </div>
            <div class="carousel-item">
                <img src="<?php echo base_url('assets/images/img/corevalues/Generosity.jpg') ?>"  class="d-block w-100">
            </div>
            <div class="carousel-item">
               <img src="<?php echo base_url('assets/images/img/corevalues/Integrity.jpg') ?>"  class="d-block w-100">
            </div>
          </div>
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
        </div> -->
    </div>
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>node_modules/lightbox2/dist/css/lightbox.css" />
<script src="<?php echo base_url(); ?>node_modules/lightbox2/dist/js/lightbox.js"></script>
<script>
    $(function() {
       // $('.carousel').carousel()
    });
</script>