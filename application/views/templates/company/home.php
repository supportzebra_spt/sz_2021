<style type="text/css">
    .card-header {
        background: #242627 !important;
        color: #55ABE1 !important;
        font-weight: 402 !important;
        font-size: 20px !important;
    }

    .card-body: {
        font-weight: 402 !important;
    }
    .mycontainer{
        padding:10px;
        background:white;
        box-shadow:2px 2px 4px 3px #bbb;
        margin-bottom:25px;
    }
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content ">
        <div class="row">
            <div class="col-md-12">
                <div class="mycontainer">
                    <img src="<?php echo base_url('assets/images/img/missionvision.jpg') ?>" style="width:100%;object-fit: cover;" alt="">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="mycontainer">
                            <img src="<?php echo base_url('assets/images/img/corevalues2.jpg') ?>" style="width:100%;object-fit: cover;" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mycontainer">
                            <img src="<?php echo base_url('assets/images/img/wowfactors.jpg') ?>" style="width:100%;object-fit: cover;" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>