<style type="text/css">
	div.container-forms {
		background: #f3f0f0 !important;
		padding: 10px !important;
	}

	.tabtopcolor {
		background-color: #3d3e4d !important;
	}

	.foldercolor {
		background-color: #4d5167;
		/*#585858;*/
		border-width: 15px
	}

	.icon-color {
		color: #cc827d !important;
	}

	.colorwhitesize {
		color: white !important;
		font-size: 0.9em;
	}

	.colorlightblue {
		background-color: #9cdfda;
	}

	.coloricons {
		color: #4371a2 !important;
	}

	.colorblue {
		color: #3297e4 !important;
	}

	.checklistcolor {
		background: #97e2af;
	}

	.bottom-icons {
		color: #3c803f !important;
	}

	.pro-name {
		color: #416d90;
		font-size: 0.9em;
		margin-left: 1em;
		font-weight: bold;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator font-poppins">Archived Records</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">
						<</li> <li class="m-nav__item">
							<a href="http://localhost/supportz/process" class="m-nav__link">
								<span class="m-nav__link-text">Folder</span>
							</a>
					</li>
					<li class="m-nav__separator">
						<</li> <li class="m-nav__item">
							<a href="#" class="m-nav__link">
								<span class="m-nav__link-text">Process</span>
							</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content" id="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="m-portlet m-portlet--tabs">
					<div class="m-portlet__head tabtopcolor">
						<div class="m-portlet__head-tools">
							<ul class="nav nav-tabs m-tabs-line m-tabs-line--accent m-tabs-line--2x" role="tablist">
								<!-- <li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#foldertab" role="tab">
										<i class="fa fa-folder "></i>Folders</a>
										<input type="hidden" id="display_id">
										<input type="hidden" id="displayprocess_id">
										<input type="hidden" id="displaychecklist_id">
									</li> -->
								<li class="nav-item m-tabs__item active">
									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#processtab" role="tab">
										<i class="fa fa-file-text-o"></i>Process Templates</a>
								</li>
								<li class="nav-item m-tabs__item">
									<a class="nav-link m-tabs__link" data-toggle="tab" href="#checklisttab" role="tab">
										<i class="fa fa-check-square-o"></i>Checklist</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="m-portlet__body">
						<div class="tab-content">
							<!-- Archived Folder -->
							<!-- <div class="tab-pane fade col-md-12" id="foldertab" role="tabpanel">
											<div class="m-form m-form--label-align-right m--margin-bottom-30">
												<div class="row align-items-center"> -->
							<!-- SEARCH SECTION -->
							<!-- <div class="col-xl-8 order-2 order-xl-1 container-forms">
														<div class="form-group m-form__group row align-items-center">
															<div class="col-md-8">
																<div class="m-input-icon m-input-icon--left">
																	<input type="text" class="form-control m-input " id="searchString" placeholder="Search by folder name..." >
																	<span class="m-input-icon__icon m-input-icon__icon--left">
																		<span><i class="la la-search"></i></span>
																	</span>
																</div>
															</div>
															<div class="col-md-4">
																<div class="m-input-icon">
																	<select id="selectStatus" onchange="" class="form-control m-input m-input--square">
																		<option value="0">All</option>
																		<option value="10">Active</option>
																		<option value="11">Inactive</option>
																		<option value="00">Assigned</option>
																		<option value="15">Archived</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div> -->
							<!-- TAB CONTENT -->
							<!-- <div id="folrow" class="row">
											</div>
										</div> -->
							<!-- Archived Process Templates -->
							<div class="tab-pane fade show active col-md-12" id="processtab" role="tabpanel">
								<div class="m-form m-form--label-align-right m--margin-bottom-30">
									<div class="row align-items-center">
										<!-- SEARCH SECTION -->
										<div class="col-xl-12 container-forms">
											<div class="m-input-icon m-input-icon--left">
												<input type="text" class="form-control m-input " id="searchProcess" placeholder="Search by process name...">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span><i class="la la-search"></i></span>
												</span>
											</div>
										</div>
									</div>
								</div>
								<!-- TAB CONTENT -->
								<div id="prorow" class="row"></div>
								<div class="row no-gutters">
									<div class="col-md-8 col-12">
										<ul id="prorow-pagination">
										</ul>
									</div>
									<div class="col-md-1 text-right col-4">
										<select class="form-control m-input form-control-sm m-input--air" id="prorow-perpage">
											<option>6</option>
											<option>12</option>
											<option>24</option>
											<option>60</option>
											<option>120</option>
										</select>
									</div>
									<div class="col-md-3 text-right col-8">
										<p style="font-family:Poppins;font-size:13px;text-align:right;margin-top:3px">Showing <span id="prorow-count"></span> of <span id="prorow-total"></span> records</p>
									</div>
								</div>
							</div>
							<!-- Archived Checklists Templates -->
							<div class="tab-pane fade col-md-12" id="checklisttab" role="tabpanel">
								<div class="m-form m-form--label-align-right m--margin-bottom-30">
									<div class="row align-items-center">
										<!-- SEARCH SECTION -->
										<div class="col-xl-12 container-forms">
											<div class="m-input-icon m-input-icon--left">
												<input type="text" class="form-control m-input " id="searchChecklist" placeholder="Search by checklist name...">
												<span class="m-input-icon__icon m-input-icon__icon--left">
													<span><i class="la la-search"></i></span>
												</span>
											</div>
										</div>
									</div>
								</div>
								<!-- TAB CONTENT -->
								<div id="checkrow" class="row"></div>
								<div class="row no-gutters">
									<div class="col-md-8 col-12">
										<ul id="checkrow-pagination">
										</ul>
									</div>
									<div class="col-md-1 text-right col-4">
										<select class="form-control m-input form-control-sm m-input--air" id="checkrow-perpage">
											<option>6</option>
											<option>12</option>
											<option>24</option>
											<option>60</option>
											<option>120</option>
										</select>
									</div>
									<div class="col-md-3 text-right col-8">
										<p style="font-family:Poppins;font-size:13px;text-align:right;margin-top:3px">Showing <span id="checkrow-count"></span> of <span id="checkrow-total"></span> records</p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>

<script type="text/javascript">
	$(function() {
		$('#prorow-perpage').change();
	});
	//JQUERY DISPLAY
	// function displayfolders(word) {
	// 	var d_id = $("#display_id").val();
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "<?php echo base_url(); ?>process/display_archivedfolders",
	// 		data: {
	// 			word: word,
	// 			display_folder: d_id
	// 		},
	// 		cache: false,
	// 		success: function(res) {
	// 			var stringx = "";
	// 			var stringlastid = "";
	// 			if (res != 0) {
	// 				var result = JSON.parse(res.trim());
	// 				var lastid = 0;
	// 				$.each(result, function(key, item) {
	// 					lastid = item.folder_ID;
	// 					var foldername = item.folderName;
	// 					var substringname = "";
	// 					if (foldername.length > 22) {
	// 						substringname = foldername.substring(0, 22) + "...";
	// 					} else {
	// 						substringname = foldername.substring(0, 22);
	// 					}
	// 					stringx += "<div class='col-lg-4 foldercontent' id='" + item.status_ID + "'>";
	// 					stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
	// 					stringx += "<div class='m-portlet__head border-accent rounded foldercolor'>";

	// 					stringx += "<div class='row' style='margin-top:1em'>";
	// 					stringx += "<div class='m-portlet__head-caption'>";
	// 					stringx += "<div class='m-portlet__head-title'>";
	// 					stringx += "<span class='m-portlet__head-icon'>";
	// 					stringx += "<i class='fa fa-folder icon-color' id='" + item.folder_ID + "'></i>";
	// 					stringx += "</span>";
	// 					stringx += "<a onclick='unarchive_notif()' data-toggle='tooltip' data-placement='bottom' title='" + item.folderName + "' style='color:#9ce4f5;display:table-cell;font-size: 1.2rem;font-weight: 500;' class='foldname'>" + substringname + "</a>";

	// 					stringx += "<input type='hidden' value='" + item.folder_ID + "' id='eachfolderid'>";
	// 					stringx += "<input type='hidden' class='folStat' value='" + item.status_ID + "'";
	// 					stringx += "<input type='hidden' class='fetchval' id='fetchval'>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";
	// 					stringx += "&nbsp;&nbsp;&nbsp;&nbsp;<span class='colorwhitesize createdby'>" + item.fname + "&nbsp;" + item.lname + "</span>";
	// 					if (item.referenceFolder != 1) {
	// 						stringx += "&nbsp;&nbsp;<span class='colorwhitesize createdby'>(Subfolder)</span>";
	// 					}
	// 					stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
	// 					stringx += "<div class='m-portlet__head-tools'>";
	// 					stringx += "<ul class='m-portlet__nav'>";
	// 					stringx += "<li class='m-portlet__nav-item'>";
	// 					stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
	// 					stringx += "<i class='la la-cog'></i></a>";
	// 					stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
	// 					stringx += "<button class='dropdown-item' type='button' data-id='" + item.folder_ID + "' onclick='folderset_unarchive(this)' data-toggle='modal' data-target='#updatemodal'>";
	// 					stringx += "<i class='fa fa-file-archive-o'></i>&nbsp;Unarchive this folder</button>";
	// 					stringx += "</div>";
	// 					stringx += "</li>";

	// 					stringx += "<li class='m-portlet__nav-item'>";
	// 					stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle'></i></a> ";
	// 					stringx += "</li>";
	// 					stringx += "</a>";
	// 					stringx += "</li>";
	// 					stringx += " <li class='m-portlet__nav-item'>";
	// 					stringx += " <span class='m-badge m-badge--success m-badge--wide'>Archived</span>";
	// 					stringx += "</li> ";

	// 					stringx += "</ul>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";

	// 					stringx += "</div>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";
	// 				});
	// 			} else {
	// 				stringx += "<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records</strong></h5></div>";
	// 			}
	// 			$("#folrow").html(stringx);
	// 			// stringlastid+= "<div class='load-more col-lg-12' lastID='"+lastid+"' style='display: none;'>";
	// 			// stringlastid+= "<h5 style='text-align: center;'>";
	// 			// stringlastid+= "<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
	// 			// stringlastid+= "</div> Loading more folder...</h5></div>";
	// 			// $("#folrow").append(stringlastid);
	// 		}
	// 	});
	// }

	//New Process Display

	function processtemp_archiveddisplay(limiter, perPage, callback) {
		var inputSearch = $("#searchProcess").val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>process/display_archivedprocesstemp",
			data: {
				limiter: limiter,
				perPage: perPage,
				inputSearch: inputSearch
			},
			beforeSend: function() {
				mApp.block('#content', {
					overlayColor: '#000000',
					type: 'loader',
					state: 'brand',
					size: 'lg'
				});
			},
			cache: false,
			success: function(res) {
				res = JSON.parse(res.trim());

				$("#prorow-total").html(res.total);
				$("#prorow-total").data('count', res.total);
				$("#prorow").html("");
				var stringx = "";
				var stringlastid = "";
				var lastid = 0;
				$.each(res.process_details, function(key, item) {
					lastid = item.process.process_ID;
					var processname = item.process.processTitle;
					var substringprocess = "";
					if (processname.length > 22) {
						substringprocess = processname.substring(0, 22) + "...";
					} else {
						substringprocess = processname.substring(0, 22);
					}

					stringx += "<div class='col-lg-4 processcontent pr+processtatusid'>";
					stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>"
					stringx += "<div class='m-portlet__head border-accent rounded colorlightblue'>";
					stringx += "<div class='row' style='margin-top:1em'>";
					stringx += "<div class='m-portlet__head-caption'>";
					stringx += "<div class='m-portlet__head-title'>";
					stringx += "<span class='m-portlet__head-icon'>";
					stringx += "<i class='fa fa-archive colorblue'></i>";
					stringx += "</span>";

					stringx += "<a onclick='unarchive_notif()' data-toggle='tooltip' data-placement='bottom' title='" + item.process.processTitle + "' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' class='m-portlet__head-text protitle'>" + substringprocess + "</a>";

					stringx += "</div>";
					stringx += "</div>";
					stringx += "</div>";

					// stringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>"+item.process.folderName+"</b></span>";
					stringx += "<input type='hidden' class='proStat' value='" + item.process.processstatus_ID + "'>";
					stringx += "<input type='hidden' class='eachprocessid' value=" + item.process.process_ID + ">";
					stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
					stringx += "<div class='m-portlet__head-tools'>";
					stringx += "<ul class='m-portlet__nav'>";


					if (item.process.processstatus_ID == "15") {
						stringx += "<li class='m-portlet__nav-item'>";
						stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
						stringx += "<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";

						stringx += "<button class='dropdown-item' type='button' data-type='Process' data-id='" + item.process.process_ID + "' onclick='processset_unarchive(this)' data-target=''><i class='fa fa-file-archive-o'>";
						stringx += "</i>&nbsp;Unarchive this template</button>";
						stringx += "</div> ";
						stringx += "</li>";
					} else if (item.process.processstatus_ID == "2" || item.process.processstatus_ID == "10") {
						stringx += "<li class='m-portlet__nav-item'>";
						stringx += " <span class='m-menu__link-badge'>";
						stringx += " <span class='m-badge m-badge--secondary m-badge--wide'>Folder Archived</span>";
						stringx += "</span>";
						stringx += "</li> ";
					}

					stringx += "</li> &nbsp;|&nbsp;";
					stringx += "<li class='m-portlet__nav-item'>";
					stringx += " <span class='m-menu__link-badge'>";
					stringx += " <span class='m-badge m-badge--metal' data-toggle='tooltip' data-placement='top' title='All Checklist'>" + item.statuses.all + "</span>";
					stringx += "</span>";
					stringx += "</li> ";
					stringx += "<li class='m-portlet__nav-item'>";
					stringx += " <span class='m-menu__link-badge'>";
					stringx += "<span class='m-badge m-badge--info' data-toggle='tooltip' data-placement='top' title='Pending Checklist'>" + item.statuses.pending + "</span>";

					stringx += "</span>";
					stringx += "</li>";
					stringx += "<li class='m-portlet__nav-item'>";
					stringx += "<span class='m-menu__link-badge'>";
					stringx += "<span class='m-badge m-badge--danger' data-toggle='tooltip' data-placement='top' title='Overdue Checklist'>" + item.statuses.overdue + "</span>";
					stringx += "</span>";
					stringx += "</li>";
					stringx += "<li class='m-portlet__nav-item'>";
					stringx += "<span class='m-menu__link-badge'>";
					stringx += "<span class='m-badge m-badge--success' data-toggle='tooltip' data-placement='top' title='Completed Checklist'>" + item.statuses.completed + "</span>";
					stringx += " </span>";
					stringx += "</li>";

					stringx += "</ul>";
					stringx += "</div>";
					stringx += "</div>";
					stringx += "</div>";
					stringx += "</div>"
					stringx += "</div>";
				});
				$("#prorow").html(stringx);
				if ($("#prorow").html() == "") {
					$("#prorow").html('<div class="col-12"><div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline text-center"><div class="m-alert__text"><strong>No Archived Template</strong><br>Archive a template from the main display and it will show up here</div></div></div>');
				}
				var count = $("#prorow-total").data('count');
				var result = parseInt(limiter) + parseInt(perPage);
				if (count === 0) {
					$("#prorow-count").html(0 + ' - ' + 0);
				} else if (count <= result) {
					$("#prorow-count").html((limiter + 1) + ' - ' + count);
				} else if (limiter === 0) {
					$("#prorow-count").html(1 + ' - ' + perPage);
				} else {
					$("#prorow-count").html((limiter + 1) + ' - ' + result);
				}

				mApp.unblock('#content');
				callback(res.total);
			},
			error: function() {
				swal("Error", "Please contact admin", "error");
				mApp.unblock('#content');
			}
		});
	}

	// function processtemp_archiveddisplay() {
	// 	var pro_id = $("#displayprocess_id").val();
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "<?php echo base_url(); ?>process/display_archivedprocesstemp",
	// 		data: {
	// 			process_display: pro_id
	// 		},
	// 		cache: false,
	// 		success: function(res) {
	// 			var stringx = "";
	// 			var stringlastid = "";
	// 			var lastid = 0;
	// 			var result = JSON.parse(res.trim());
	// 			console.log(result);

	// 			$.each(result.process_details, function(key, item) {
	// 				lastid = item.process.process_ID;
	// 				var processname = item.process.processTitle;
	// 				var substringprocess = "";
	// 				if (processname.length > 22) {
	// 					substringprocess = processname.substring(0, 22) + "...";
	// 				} else {
	// 					substringprocess = processname.substring(0, 22);
	// 				}

	// 				stringx += "<div class='col-lg-4 processcontent pr+processtatusid'>";
	// 				stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>"
	// 				stringx += "<div class='m-portlet__head border-accent rounded colorlightblue'>";
	// 				stringx += "<div class='row' style='margin-top:1em'>";
	// 				stringx += "<div class='m-portlet__head-caption'>";
	// 				stringx += "<div class='m-portlet__head-title'>";
	// 				stringx += "<span class='m-portlet__head-icon'>";
	// 				stringx += "<i class='fa fa-archive colorblue'></i>";
	// 				stringx += "</span>";

	// 				stringx += "<a onclick='unarchive_notif()' data-toggle='tooltip' data-placement='bottom' title='" + item.process.processTitle + "' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' class='m-portlet__head-text protitle'>" + substringprocess + "</a>";

	// 				stringx += "</div>";
	// 				stringx += "</div>";
	// 				stringx += "</div>";

	// 				// stringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>"+item.process.folderName+"</b></span>";
	// 				stringx += "<input type='hidden' class='proStat' value='" + item.process.processstatus_ID + "'>";
	// 				stringx += "<input type='hidden' class='eachprocessid' value=" + item.process.process_ID + ">";
	// 				stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
	// 				stringx += "<div class='m-portlet__head-tools'>";
	// 				stringx += "<ul class='m-portlet__nav'>";


	// 				if (item.process.processstatus_ID == "15") {
	// 					stringx += "<li class='m-portlet__nav-item'>";
	// 					stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
	// 					stringx += "<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";

	// 					stringx += "<button class='dropdown-item' type='button' data-type='Process' data-id='" + item.process.process_ID + "' onclick='processset_unarchive(this)' data-target=''><i class='fa fa-file-archive-o'>";
	// 					stringx += "</i>&nbsp;Unarchive this template</button>";
	// 					stringx += "</div> ";
	// 					stringx += "</li>";
	// 				} else if (item.process.processstatus_ID == "2" || item.process.processstatus_ID == "10") {
	// 					stringx += "<li class='m-portlet__nav-item'>";
	// 					stringx += " <span class='m-menu__link-badge'>";
	// 					stringx += " <span class='m-badge m-badge--secondary m-badge--wide'>Folder Archived</span>";
	// 					stringx += "</span>";
	// 					stringx += "</li> ";
	// 				}

	// 				stringx += "</li> &nbsp;|&nbsp;";
	// 				stringx += "<li class='m-portlet__nav-item'>";
	// 				stringx += " <span class='m-menu__link-badge'>";
	// 				stringx += " <span class='m-badge m-badge--metal' data-toggle='tooltip' data-placement='top' title='All Checklist'>" + item.statuses.all + "</span>";
	// 				stringx += "</span>";
	// 				stringx += "</li> ";
	// 				stringx += "<li class='m-portlet__nav-item'>";
	// 				stringx += " <span class='m-menu__link-badge'>";
	// 				stringx += "<span class='m-badge m-badge--info' data-toggle='tooltip' data-placement='top' title='Pending Checklist'>" + item.statuses.pending + "</span>";

	// 				stringx += "</span>";
	// 				stringx += "</li>";
	// 				stringx += "<li class='m-portlet__nav-item'>";
	// 				stringx += "<span class='m-menu__link-badge'>";
	// 				stringx += "<span class='m-badge m-badge--danger' data-toggle='tooltip' data-placement='top' title='Overdue Checklist'>" + item.statuses.overdue + "</span>";
	// 				stringx += "</span>";
	// 				stringx += "</li>";
	// 				stringx += "<li class='m-portlet__nav-item'>";
	// 				stringx += "<span class='m-menu__link-badge'>";
	// 				stringx += "<span class='m-badge m-badge--success' data-toggle='tooltip' data-placement='top' title='Completed Checklist'>" + item.statuses.completed + "</span>";
	// 				stringx += " </span>";
	// 				stringx += "</li>";

	// 				stringx += "</ul>";
	// 				stringx += "</div>";
	// 				stringx += "</div>";
	// 				stringx += "</div>";
	// 				stringx += "</div>"
	// 				stringx += "</div>";
	// 			});
	// 			$("#prorow").html(stringx);
	// 		}
	// 	})
	// }

	// function displayprocesstemp(word) {
	// 	var pro_id = $("#displayprocess_id").val();
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "<?php echo base_url(); ?>process/display_archivedprocesstemp",
	// 		data: {
	// 			word: word,
	// 			process_display: pro_id
	// 		},
	// 		cache: false,
	// 		success: function(res) {
	// 			var stringx = "";
	// 			var stringlastid = "";
	// 			var lastid = 0;
	// 			var result = JSON.parse(res.trim());
	// 			console.log(result);


	// 			// 			if(res!=0){
	// 			// 				var result = JSON.parse(res.trim());

	// 			// 				$.each(result, function (key, item) {
	// 			// 					lastid=item.process.process_ID;
	// 			// 					var processname=item.process.processTitle;
	// 			// 					var substringprocess="";
	// 			// 					if(processname.length > 22){
	// 			// 						substringprocess = processname.substring(0,22)+"...";
	// 			// 					}else{
	// 			// 						substringprocess = processname.substring(0,22);
	// 			// 					}
	// 			// 					stringx +="<div class='col-lg-4 processcontent pr+processtatusid'>";
	// 			// 					stringx +="<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>"
	// 			// 					stringx +="<div class='m-portlet__head border-accent rounded colorlightblue'>";
	// 			// 					stringx +="<div class='row' style='margin-top:1em'>";
	// 			// 					stringx +="<div class='m-portlet__head-caption'>";
	// 			// 					stringx +="<div class='m-portlet__head-title'>";
	// 			// 					stringx +="<span class='m-portlet__head-icon'>";
	// 			// 					stringx +="<i class='fa fa-archive colorblue'></i>";
	// 			// 					stringx +="</span>" ;

	// 			// 					stringx +="<a onclick='unarchive_notif()' data-toggle='tooltip' data-placement='bottom' title='"+item.process.processTitle+"' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' class='m-portlet__head-text protitle'>"+substringprocess+"</a>";

	// 			// 					stringx +="</div>";
	// 			// 					stringx +="</div>";
	// 			// 					stringx +="</div>";

	// 			// 					// stringx +="<span style='font-size: 0.9em;margin-left:1em'>";
	// 			// 					// var dateString=item.process.dateTimeCreated;
	// 			// 					// var dateObj = new Date(dateString);
	// 			// 					// var momentObj = moment(dateObj);
	// 			// 					// var momentString = momentObj.format('llll');
	// 			// 					// stringx +="<span>"+momentString+"</span><br>";
	// 			// 					// stringx +="</span>";

	// 			// 					stringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>"+item.process.folderName+"</b></span>";

	// 			// 					stringx +="<input type='hidden' class='proStat' value='"+item.process.processstatus_ID+"'>";
	// 			// 					stringx +="<input type='hidden' class='eachprocessid' value="+item.process.process_ID+">";
	// 			// 					stringx +="<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
	// 			// 					stringx +="<div class='m-portlet__head-tools'>";
	// 			// 					stringx +="<ul class='m-portlet__nav'>";

	// 			// 					if(item.process.processstatus_ID=="15"){
	// 			// 						stringx +="<li class='m-portlet__nav-item'>";
	// 			// 						stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
	// 			// 						stringx +="<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";

	// 			// 						stringx +="<button class='dropdown-item' type='button' data-type='Process' data-id='"+item.process.process_ID+"' onclick='processset_unarchive(this)' data-target=''><i class='fa fa-file-archive-o'>";
	// 			// 						stringx +="</i>&nbsp;Unarchive this template</button>";
	// 			// 						stringx +="</div> ";
	// 			// 						stringx +="</li>";
	// 			// 					}else if(item.process.processstatus_ID=="2" || item.process.processstatus_ID=="10"){
	// 			// 						stringx += "<li class='m-portlet__nav-item'>";
	// 			// 						stringx += " <span class='m-menu__link-badge'>";
	// 			// 						stringx +=" <span class='m-badge m-badge--secondary m-badge--wide'>Folder Archived</span>";
	// 			// 						stringx +="</span>"  ; 
	// 			// 						stringx +="</li> " ;
	// 			// 					}
	// 			// 					stringx +="</li> &nbsp;|&nbsp;";
	// 			// 					stringx += "<li class='m-portlet__nav-item'>";
	// 			// 					stringx += " <span class='m-menu__link-badge'>";
	// 			// 					stringx += " <span class='m-badge m-badge--metal' data-toggle='tooltip' data-placement='top' title='All Checklist'>"+item.statuses.all+"</span>";
	// 			// 					stringx +="</span>"  ; 
	// 			// 					stringx +="</li> " ;
	// 			// 					stringx +="<li class='m-portlet__nav-item'>";
	// 			// 					stringx +=" <span class='m-menu__link-badge'>";
	// 			// 					stringx +="<span class='m-badge m-badge--info' data-toggle='tooltip' data-placement='top' title='Pending Checklist'>"+item.statuses.pending+"</span>";

	// 			// 					stringx +="</span>"; 
	// 			// 					stringx +="</li>";
	// 			// 					stringx +="<li class='m-portlet__nav-item'>";
	// 			// 					stringx +="<span class='m-menu__link-badge'>";
	// 			// 					stringx +="<span class='m-badge m-badge--danger' data-toggle='tooltip' data-placement='top' title='Overdue Checklist'>"+item.statuses.overdue+"</span>";
	// 			// 					stringx +="</span>";   
	// 			// 					stringx +="</li>";
	// 			// 					stringx +="<li class='m-portlet__nav-item'>";
	// 			// 					stringx +="<span class='m-menu__link-badge'>";
	// 			// 					stringx +="<span class='m-badge m-badge--success' data-toggle='tooltip' data-placement='top' title='Completed Checklist'>"+item.statuses.completed+"</span>";
	// 			// 					stringx +=" </span>" ;  
	// 			// 					stringx +="</li>";

	// 			// 					// stringx +="<li class='m-portlet__nav-item'>";
	// 			// 					// stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle coloricons'></i></a> " ;
	// 			// 					// stringx +="</li>";



	// 			// 					stringx +="</ul>";
	// 			// 					stringx += "</div>";
	// 			// 					stringx +="</div>";
	// 			// 					stringx +="</div>";
	// 			// 					stringx +="</div>"
	// 			// 					stringx +="</div>";
	// 			//     });//end of each
	// 			// }else{
	// 			// 	stringx+="<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records</strong></h5></div>";
	// 			// }
	// 			$("#prorow").html(stringx);
	// 			// console.log(lastid);
	// 			// stringlastid+= "<div class='processload-more col-lg-12' processlastID='"+lastid+"' style='display: none;'>";
	// 			// stringlastid+= "<h5 style='text-align: center;'>";
	// 			// stringlastid+= "<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
	// 			// stringlastid+= "</div> Loading more process template...</h5></div>";
	// 			// $("#prorow").append(stringlastid);
	// 		} //end of success
	// 	});
	// }

	//NEW DISPLAY CHECKLIST
	function checklists_archiveddisplay(limiter, perPage, callback) {
		var inputSearch = $("#searchChecklist").val();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>process/display_archivedchecklists",
			data: {
				limiter: limiter,
				perPage: perPage,
				inputSearch: inputSearch
			},
			beforeSend: function() {
				mApp.block('#content', {
					overlayColor: '#000000',
					type: 'loader',
					state: 'brand',
					size: 'lg'
				});
			},
			cache: false,
			success: function(res) {
				res = JSON.parse(res.trim());

				$("#checkrow-total").html(res.total);
				$("#checkrow-total").data('count', res.total);
				$("#checkrow").html("");
				var stringx = "";
				$.each(res.checklist_details, function(key, item) {
					var cid = item.checklist.checklist_ID;
					var checklist_name = item.checklist.checklistTitle;
					var substringname = "";

					if (checklist_name.length > 22) {
						substringname = checklist_name.substring(0, 22) + "...";
					} else {
						substringname = checklist_name.substring(0, 22);
					}
					stringx += "<div class='col-lg-4'>";
					stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
					stringx += "<div class='m-portlet__head border-accent rounded checklistcolor'>";

					stringx += "<div class='row' style='margin-top:1em'>";
					stringx += "<div class='m-portlet__head-caption'>";
					stringx += "<div class='m-portlet__head-title'>";
					stringx += "<span class='m-portlet__head-icon'>";
					if (item.checklist.status_ID == "2") {
						stringx += "<i class='fa fa-clock-o coloricons' id=''></i>";
					} else if (item.checklist.status_ID == "3") {
						stringx += "<i class='fa fa-check coloricons' id=''></i>";
					}
					stringx += "</span>";
					stringx += "<a onclick='unarchive_notif()' data-toggle='tooltip' data-placement='bottom' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' class='foldname'>" + substringname + "</a>";
					stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
					stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
					stringx += "</div>";
					stringx += "</div>";
					stringx += "</div>";

					stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";

					stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
					stringx += " <div class='col-md-12'>";
					stringx += "<div class='progress'>"


					var alltask = item.statuses.all;
					var allcomp = item.statuses.completed;
					var allpending = item.statuses.pending;

					if (alltask == allcomp) {
						stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
						stringx += "</div>"
					} else if (allcomp == 0) {
						stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
						stringx += "</div>"
					} else {
						var percentage_pending = (allpending / alltask) * 100;
						stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-toggle='m-tooltip' data-placement='top' data-original-title='" + allpending + "/" + alltask + "'></div>"
						stringx += "</div>"
					}


					stringx += "</div>";
					stringx += "</div>";

					stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
					stringx += "<div class='m-portlet__head-tools'>";
					stringx += "<ul class='m-portlet__nav'>";

					if (item.checklist.isArchived == '1') {
						stringx += "<li class='m-portlet__nav-item'>";
						stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
						stringx += "<i class='la la-cog bottom-icons'></i></a>";
						stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
						stringx += "<button data-type='Remove' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_unarchive(this)'>";
						stringx += "<i class='la la-archive'></i>&nbsp;Unarchive this Checklist</button>";
						stringx += "</div>";
						stringx += "</li>";
					} else {
						stringx += "<li class='m-portlet__nav-item'>";
						stringx += " <span class='m-menu__link-badge'>";
						stringx += " <span class='m-badge m-badge--info m-badge--wide'>Template Archived</span>";
						stringx += "</span>";
						stringx += "</li> ";
					}
					stringx += "<li class='m-portlet__nav-item'>";
					stringx += "<span class='m-menu__link-badge'>";



					if (item.checklist.dueDate != null) {
						var dateString2 = item.checklist.dueDate;
						var dateObj2 = new Date(dateString2);
						var dateObj3 = new Date();
						var datediff = new Date(dateObj2 - dateObj3);
						var days = datediff / 1000 / 60 / 60 / 24;
						var rounddays = Math.round(days);

						var momentObj2 = moment(dateObj2);
						var momentString2 = momentObj2.format('ll');

						if (dateObj2 > dateObj3) {
							stringx += "<span class='m-badge m-badge--success m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date:" + rounddays + "left'>" + momentString2 + "</span>";
						} else if (dateObj2 < dateObj3) {
							stringx += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue!'>" + momentString2 + "</span>";
						} else if (dateObj2 == dateObj3) {
							stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";
						}
					}

					stringx += "</span>";
					stringx += "</li>";

					stringx += "</ul>";
					stringx += "</div>";
					stringx += "</div>";

					stringx += "</div>";
					stringx += "</div>";
					stringx += "</div>";
				});
				$("#checkrow").html(stringx);
				if ($("#checkrow").html() == "") {
					$("#checkrow").html('<div class="col-12"><div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline text-center"><div class="m-alert__text"><strong>No Checklist Available</strong><br>Run a checklist and it will show up here</div></div></div>');
				}
				var count = $("#checkrow-total").data('count');
				var result = parseInt(limiter) + parseInt(perPage);
				if (count === 0) {
					$("#checkrow-count").html(0 + ' - ' + 0);
				} else if (count <= result) {
					$("#checkrow-count").html((limiter + 1) + ' - ' + count);
				} else if (limiter === 0) {
					$("#checkrow-count").html(1 + ' - ' + perPage);
				} else {
					$("#checkrow-count").html((limiter + 1) + ' - ' + result);
				}

				mApp.unblock('#content');
				callback(res.total);
			},
			error: function() {
				swal("Error", "Please contact admin", "error");
				mApp.unblock('#content');
			}
		});
	}
	// function checklists_archiveddisplay() {
	// 	var check_id = $("#displaychecklist_id").val();
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "<?php echo base_url(); ?>process/display_archivedchecklists",
	// 		data: {
	// 			checklist_display: check_id
	// 		},
	// 		cache: false,
	// 		success: function(res) {
	// 			var stringx = "";
	// 			var result = JSON.parse(res.trim());
	// 			$.each(result.checklist_details, function(key, item) {
	// 				var cid = item.checklist.checklist_ID;
	// 				var checklist_name = item.checklist.checklistTitle;
	// 				var substringname = "";

	// 				if (checklist_name.length > 22) {
	// 					substringname = checklist_name.substring(0, 22) + "...";
	// 				} else {
	// 					substringname = checklist_name.substring(0, 22);
	// 				}
	// 				stringx += "<div class='col-lg-4'>";
	// 				stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
	// 				stringx += "<div class='m-portlet__head border-accent rounded checklistcolor'>";

	// 				stringx += "<div class='row' style='margin-top:1em'>";
	// 				stringx += "<div class='m-portlet__head-caption'>";
	// 				stringx += "<div class='m-portlet__head-title'>";
	// 				stringx += "<span class='m-portlet__head-icon'>";
	// 				if (item.checklist.status_ID == "2") {
	// 					stringx += "<i class='fa fa-clock-o coloricons' id=''></i>";
	// 				} else if (item.checklist.status_ID == "3") {
	// 					stringx += "<i class='fa fa-check coloricons' id=''></i>";
	// 				}
	// 				stringx += "</span>";
	// 				stringx += "<a onclick='unarchive_notif()' data-toggle='tooltip' data-placement='bottom' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' class='foldname'>" + substringname + "</a>";
	// 				stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
	// 				stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
	// 				stringx += "</div>";
	// 				stringx += "</div>";
	// 				stringx += "</div>";

	// 				stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";

	// 				stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
	// 				stringx += " <div class='col-md-12'>";
	// 				stringx += "<div class='progress'>"


	// 				var alltask = item.statuses.all;
	// 				var allcomp = item.statuses.completed;
	// 				var allpending = item.statuses.pending;

	// 				if (alltask == allcomp) {
	// 					stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
	// 					stringx += "</div>"
	// 				} else if (allcomp == 0) {
	// 					stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
	// 					stringx += "</div>"
	// 				} else {
	// 					var percentage_pending = (allpending / alltask) * 100;
	// 					stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-toggle='m-tooltip' data-placement='top' data-original-title='" + allpending + "/" + alltask + "'></div>"
	// 					stringx += "</div>"
	// 				}


	// 				stringx += "</div>";
	// 				stringx += "</div>";

	// 				stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
	// 				stringx += "<div class='m-portlet__head-tools'>";
	// 				stringx += "<ul class='m-portlet__nav'>";

	// 				if (item.checklist.isArchived == '1') {
	// 					stringx += "<li class='m-portlet__nav-item'>";
	// 					stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
	// 					stringx += "<i class='la la-cog bottom-icons'></i></a>";
	// 					stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
	// 					stringx += "<button data-type='Remove' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_unarchive(this)'>";
	// 					stringx += "<i class='la la-archive'></i>&nbsp;Unarchive this Checklist</button>";
	// 					stringx += "</div>";
	// 					stringx += "</li>";
	// 				} else {
	// 					stringx += "<li class='m-portlet__nav-item'>";
	// 					stringx += " <span class='m-menu__link-badge'>";
	// 					stringx += " <span class='m-badge m-badge--info m-badge--wide'>Template Archived</span>";
	// 					stringx += "</span>";
	// 					stringx += "</li> ";
	// 				}
	// 				stringx += "<li class='m-portlet__nav-item'>";
	// 				stringx += "<span class='m-menu__link-badge'>";



	// 				if (item.checklist.dueDate != null) {
	// 					var dateString2 = item.checklist.dueDate;
	// 					var dateObj2 = new Date(dateString2);
	// 					var dateObj3 = new Date();
	// 					var datediff = new Date(dateObj2 - dateObj3);
	// 					var days = datediff / 1000 / 60 / 60 / 24;
	// 					var rounddays = Math.round(days);

	// 					var momentObj2 = moment(dateObj2);
	// 					var momentString2 = momentObj2.format('ll');

	// 					if (dateObj2 > dateObj3) {
	// 						stringx += "<span class='m-badge m-badge--success m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date:" + rounddays + "left'>" + momentString2 + "</span>";
	// 					} else if (dateObj2 < dateObj3) {
	// 						stringx += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue!'>" + momentString2 + "</span>";
	// 					} else if (dateObj2 == dateObj3) {
	// 						stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";
	// 					}
	// 				}

	// 				stringx += "</span>";
	// 				stringx += "</li>";

	// 				stringx += "</ul>";
	// 				stringx += "</div>";
	// 				stringx += "</div>";

	// 				stringx += "</div>";
	// 				stringx += "</div>";
	// 				stringx += "</div>";
	// 			})
	// 			$("#checkrow").html(stringx);
	// 		}
	// 	})
	// }

	// function displaychecklists(word) {
	// 	var check_id = $("#displaychecklist_id").val();
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "<?php echo base_url(); ?>process/display_archivedchecklists",
	// 		data: {
	// 			word: word,
	// 			checklist_display: check_id
	// 		},
	// 		cache: false,
	// 		success: function(res) {
	// 			var stringx = "";
	// 			if (res != 0) {
	// 				var result = JSON.parse(res.trim());
	// 				$.each(result, function(key, item) {
	// 					var cid = item.checklist.checklist_ID;
	// 					var checklist_name = item.checklist.checklistTitle;
	// 					var substringname = "";

	// 					if (checklist_name.length > 22) {
	// 						substringname = checklist_name.substring(0, 22) + "...";
	// 					} else {
	// 						substringname = checklist_name.substring(0, 22);
	// 					}
	// 					stringx += "<div class='col-lg-4'>";
	// 					stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
	// 					stringx += "<div class='m-portlet__head border-accent rounded checklistcolor'>";

	// 					stringx += "<div class='row' style='margin-top:1em'>";
	// 					stringx += "<div class='m-portlet__head-caption'>";
	// 					stringx += "<div class='m-portlet__head-title'>";
	// 					stringx += "<span class='m-portlet__head-icon'>";
	// 					if (item.checklist.status_ID == "2") {
	// 						stringx += "<i class='fa fa-clock-o coloricons' id=''></i>";
	// 					} else if (item.checklist.status_ID == "3") {
	// 						stringx += "<i class='fa fa-check coloricons' id=''></i>";
	// 					}
	// 					stringx += "</span>";
	// 					stringx += "<a onclick='unarchive_notif()' data-toggle='tooltip' data-placement='bottom' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' class='foldname'>" + substringname + "</a>";
	// 					stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
	// 					stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";

	// 					stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";

	// 					// var dateString=item.dateTimeCreated;
	// 					// var dateObj = new Date(dateString);
	// 					// var momentObj = moment(dateObj);
	// 					// var momentString = momentObj.format('llll');
	// 					// stringx +="&nbsp;&nbsp;&nbsp;&nbsp;<span class='createdby' style='color:#043106;font-size:0.9em'>"+momentString+"</span><br>";

	// 					// stringx +="<span style='font-size: 0.9em;margin-left:1em'>";
	// 					// stringx +="<span class='m-badge m-badge--accent m-badge--wide'>CH</span>";
	// 					// stringx +="<span class='m-badge m-badge--accent m-badge--wide'>SB</span>";
	// 					// stringx +="</span>";

	// 					stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
	// 					stringx += " <div class='col-md-12'>";
	// 					stringx += "<div class='progress'>"


	// 					var alltask = item.statuses.all;
	// 					var allcomp = item.statuses.completed;
	// 					var allpending = item.statuses.pending;

	// 					if (alltask == allcomp) {
	// 						stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
	// 						stringx += "</div>"
	// 					} else if (allcomp == 0) {
	// 						stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
	// 						stringx += "</div>"
	// 					} else {
	// 						var percentage_pending = (allpending / alltask) * 100;
	// 						stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-toggle='m-tooltip' data-placement='top' data-original-title='" + allpending + "/" + alltask + "'></div>"
	// 						stringx += "</div>"
	// 					}


	// 					stringx += "</div>";
	// 					stringx += "</div>";

	// 					stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
	// 					stringx += "<div class='m-portlet__head-tools'>";
	// 					stringx += "<ul class='m-portlet__nav'>";

	// 					if (item.checklist.isArchived == '1') {
	// 						stringx += "<li class='m-portlet__nav-item'>";
	// 						stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
	// 						stringx += "<i class='la la-cog bottom-icons'></i></a>";
	// 						stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
	// 						stringx += "<button data-type='Remove' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_unarchive(this)'>";
	// 						stringx += "<i class='la la-archive'></i>&nbsp;Unarchive this Checklist</button>";
	// 						stringx += "</div>";
	// 						stringx += "</li>";
	// 					} else if (item.checklist.folstat == '15') {
	// 						stringx += "<li class='m-portlet__nav-item'>";
	// 						stringx += " <span class='m-menu__link-badge'>";
	// 						stringx += " <span class='m-badge m-badge--secondary m-badge--wide'>Folder Archived</span>";
	// 						stringx += "</span>";
	// 						stringx += "</li> ";
	// 					} else {
	// 						stringx += "<li class='m-portlet__nav-item'>";
	// 						stringx += " <span class='m-menu__link-badge'>";
	// 						stringx += " <span class='m-badge m-badge--info m-badge--wide'>Template Archived</span>";
	// 						stringx += "</span>";
	// 						stringx += "</li> ";
	// 					}
	// 					stringx += "<li class='m-portlet__nav-item'>";
	// 					stringx += "<span class='m-menu__link-badge'>";

	// 					if (item.checklist.dueDate != null) {
	// 						var dateString2 = item.checklist.dueDate;
	// 						var dateObj2 = new Date(dateString2);
	// 						var dateObj3 = new Date();
	// 						var datediff = new Date(dateObj2 - dateObj3);
	// 						var days = datediff / 1000 / 60 / 60 / 24;
	// 						var rounddays = Math.round(days);

	// 						var momentObj2 = moment(dateObj2);
	// 						var momentString2 = momentObj2.format('ll');

	// 						if (dateObj2 > dateObj3) {
	// 							stringx += "<span class='m-badge m-badge--success m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date:" + rounddays + "left'>" + momentString2 + "</span>";
	// 						} else if (dateObj2 < dateObj3) {
	// 							stringx += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue!'>" + momentString2 + "</span>";
	// 						} else if (dateObj2 == dateObj3) {
	// 							stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";
	// 						}
	// 					}
	// 					stringx += "</span>";
	// 					stringx += "</li>";

	// 					stringx += "</ul>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";

	// 					stringx += "</div>";
	// 					stringx += "</div>";
	// 					stringx += "</div>";
	// 				});
	// 			} else {
	// 				stringx += "<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records</strong></h5></div>";
	// 			}
	// 			$("#checkrow").html(stringx);
	// 		}
	// 	});
	// }

	function folderset_unarchive(element) {
		var selected_folid = $(element).data('id');
		var status = '10';
		swal({
			title: 'Are you sure?',
			text: "All Process templates and checklists will be active again.",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, unarchive!'
		}).then(function(result) {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>process/updatefolder_archive",
					data: {
						folder_ID: selected_folid,
						status_ID: status
					},
					cache: false,
					success: function(res) {
						var result = JSON.parse(res.trim());
						swal({
							position: 'center',
							type: 'success',
							title: 'Unarchived the folder',
							showConfirmButton: false,
							timer: 1500
						});
						displayfolders();
						// displayprocesstemp();
					}
				});
			} else if (result.dismiss === 'cancel') {
				swal(
					'Cancelled',
					'Setting folder to unarchive has been cancelled',
					'error'
				)
			}
		});
	}

	function processset_unarchive(element) {
		var processid = $(element).data('id');
		var status = '2';
		swal({
			title: 'Are you sure?',
			text: "All Checklists under this process template will be active again.",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, unarchive!'
		}).then(function(result) {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>process/updateprocess_archive",
					data: {
						process_ID: processid,
						status_ID: status
					},
					cache: false,
					success: function(res) {
						var result = JSON.parse(res.trim());
						$("#prorow-perpage").change();
						swal({
							position: 'center',
							type: 'success',
							title: 'Unarchived the Process Template',
							showConfirmButton: false,
							timer: 1500
						});
						// displayprocesstemp();xxpro
					}
				});
			} else if (result.dismiss === 'cancel') {
				swal(
					'Cancelled',
					'Setting Process Template to unarchive has been cancelled',
					'error'
				)
			}
		});
	}

	function checklistset_unarchive(element) {
		var chid = $(element).data('id');
		var isArchived = 0;
		swal({
			title: 'Are you sure?',
			text: "This checklist will be active",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, unarchive!'
		}).then(function(result) {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>process/updatechecklist_archive",
					data: {
						checklist_ID: chid,
						isArchived: isArchived
					},
					cache: false,
					success: function(res) {
						var result = JSON.parse(res.trim());
						$("#checkrow-perpage").change();
						swal({
							position: 'center',
							type: 'success',
							title: 'Unarchived the checklist',
							showConfirmButton: false,
							timer: 1500
						});
						// displaychecklists();xxxcheckrow
					}
				});
			} else if (result.dismiss === 'cancel') {
				swal(
					'Cancelled',
					'Setting checklist to uarchive has been cancelled',
					'error'
				)
			}
		});
	}


	$("#searchProcess").on("keyup", function() {
		$("#prorow-perpage").change();
	});
	$("#searchChecklist").on("keyup", function() {
		$("#checkrow-perpage").change();
	});


	function folderpermission_access() {
		var displayid = 0;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>process/fetch_access",
			cache: false,
			success: function(res) {
				var result = JSON.parse(res.trim());
				if (result.length > 0) {
					// 1 if admin
					displayid = 1;
					$("#display_id").val(displayid);
					displayfolders();
				} else {
					//2 if owned
					displayid = 2;
					$("#display_id").val(displayid);
					displayfolders();
				}
			}
		});
	}

	function processpermission_access() {
		var folder = "process";
		var displayid = 0;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>process/fetch_access",
			cache: false,
			success: function(res) {
				var result = JSON.parse(res.trim());
				if (result.length > 0) {
					// 1 if admin
					displayid = 1;
					$("#displayprocess_id").val(displayid);
					// displayprocesstemp();xxpro
				} else {
					//2 if owned
					displayid = 2;
					$("#displayprocess_id").val(displayid);
					// displayprocesstemp();xxpro
				}
			}
		});
	}

	function checklistpermission_access() {
		var displayid = 0;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>process/fetch_access",
			cache: false,
			success: function(res) {
				var result = JSON.parse(res.trim());
				if (result.length > 0) {
					// 1 if admin
					displayid = 1;
					$("#displaychecklist_id").val(displayid);
					// displaychecklists();xxxcheckrow
				} else {
					//2 if owned
					displayid = 2;
					$("#displaychecklist_id").val(displayid);
					// displaychecklists();xxxcheckrow
				}
			}
		});
	}
	//UNARCHIVE NOTIF
	function unarchive_notif() {
		swal("Unarchived this first to see further!", "Click unarchive", "info");
	}

	$("#prorow-perpage").change(function() {
		var perPage = $("#prorow-perpage").val();
		processtemp_archiveddisplay(0, perPage, function(total) {
			$("#prorow-pagination").pagination('destroy');
			$("#prorow-pagination").pagination({
				items: total, //default
				itemsOnPage: $("#prorow-perpage").val(),
				hrefTextPrefix: "#",
				cssStyle: 'light-theme',
				displayedPages: 10,
				onPageClick: function(pagenumber) {
					var perPage = $("#prorow-perpage").val();
					processtemp_archiveddisplay((pagenumber * perPage) - perPage, perPage, function() {});
				}
			});
		});
	});
	$("#checkrow-perpage").change(function() {
		var perPage = $("#checkrow-perpage").val();
		checklists_archiveddisplay(0, perPage, function(total) {
			$("#checkrow-pagination").pagination('destroy');
			$("#checkrow-pagination").pagination({
				items: total, //default
				itemsOnPage: $("#checkrow-perpage").val(),
				hrefTextPrefix: "#",
				cssStyle: 'light-theme',
				displayedPages: 10,
				onPageClick: function(pagenumber) {
					var perPage = $("#checkrow-perpage").val();
					checklists_archiveddisplay((pagenumber * perPage) - perPage, perPage, function() {});
				}
			});
		});
	});

	$('[href="#processtab"]').click(function() {
		$('#prorow-perpage').change();
	});
	$('[href="#checklisttab"]').click(function() {
		$('#checkrow-perpage').change();
	});
</script>