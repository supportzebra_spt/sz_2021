<style>
    th,
    td {
        white-space: nowrap;
    }

    .first-col {
        position: absolute;
        width: 20em;
        margin-left: -20em;
        margin-top: -1px;
        background: #d4d4d4b3;
    }

    .table-wrapper {
        overflow-x: scroll;
        margin: 0 0 0 20rem;
        border: 1px solid #d2d2d2;
    }

    .bootstrap-select.btn-group .dropdown-menu.inner {
        max-height: 250px !important;
    }

    .progress {
        position: relative;
    }

    .progress span {
        position: absolute;
        display: block;
        width: 100%;
        color: black;
    }


    .table-bordered td {
        border: 1px solid #dadada;
    }

    .assigneeInfo:hover {
        cursor: pointer;
    }

    .checklistTitle {
        color: black;
    }
    .checklistTitle:hover {
        color: black;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class=" align-items-center">
            <div class="mr-auto">
                <h3 id="processId" class="m-subheader__title m-subheader__title--separator" data-processid="<?php echo $process_details->process_ID; ?>"><span><i class="fa fa-file-text-o mr-2" style="font-size: 1.5rem;"></i><?php echo $process_details->processTitle; ?></h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon fa fa-folder"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('process') ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Back to Process List</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content pt-0">
        <div class="m-demo__preview" style="padding:0px !important">
            <div class="row">
                <div class="col-md-6">
                    <p class="ml-2 mb-0 text-capitalize"><span class="mr-3 mr-md-2" style="vertical-align: middle;"><i class="fa fa-user" style="font-size: 1rem;"></i></span>Created By: &nbsp;<?php echo $process_details->fname . " " . $process_details->lname; ?></p>
                    <p class="ml-2 mb-0 text-capitalize"><span class="mr-3 mr-md-2" style="vertical-align: middle;"><i class="fa fa-asterisk" style="font-size: 1rem;"></i></span>Process Status: &nbsp;<?php echo $process_details->description; ?></p>
                </div>
                <div class="col-md-6 text-md-right" style="font-size: 0.9rem;">
                    <!-- <p class="ml-2 mb-0"><span class="mr-3 mr-md-2 text-capitalize" style="vertical-align: middle;"><i class="fa fa-user " style="font-size: 1rem;"></i></span>Created By <?php echo $process_details->fname . " " . $process_details->lname; ?></p> -->
                    <p class="ml-2 mb-0"><span class="mr-2" style="vertical-align: middle;"><i class="fa fa-calendar-plus-o" style="font-size: 1.2rem;"></i></span>Created on <?php $date_created = new DateTime($process_details->dateTimeCreated);
                                                                                                                                                                                echo $date_created->format('M j, Y g:i:s A'); ?> </p>
                    <?php if ($update_details['update_stat']) {
                        $date_updated = new DateTime($process_details->dateTimeUpdated);
                        echo '<p class="ml-2 mb-0"><span class="mr-2" style="vertical-align: middle;"><i class="fa fa-edit" style="font-size: 1.2rem;"></i></span>Updated last ' . $date_updated->format('M j, Y g:i:s A') . ' by ' . $update_details['update_details']->fname . ' ' . $update_details['update_details']->lname . '</p>';
                    } ?>
                </div>
                <!-- <div class="col-md-6 pt-2">
                    <h3 class=""><span class="mr-2"><i class="fa fa-file-text-o" style="font-size: 1.5rem;"></i></span>
                    </h3>
                    <p class="ml-2 mb-0"><span class="mr-3 mr-md-2 text-capitalize" style="vertical-align: middle;"><i class="fa fa-user " style="font-size: 1rem;"></i></span>Created By <?php echo $process_details->fname . " " . $process_details->lname; ?></p>
                </div> -->
                <!-- <div class="col-md-6">
                    <span class="text-md-right" style="font-size: 0.9rem;">
                        <p class="ml-2 mb-0"><span class="mr-3 mr-md-2 text-capitalize" style="vertical-align: middle;"><i class="fa fa-user " style="font-size: 1rem;"></i></span>Created By <?php echo $process_details->fname . " " . $process_details->lname; ?></p>
                        <p class="ml-2 mb-0"><span class="mr-2" style="vertical-align: middle;"><i class="fa fa-calendar-plus-o" style="font-size: 1.2rem;"></i></span>Created on <?php $date_created = new DateTime($process_details->dateTimeCreated);
                                                                                                                                                                                    echo $date_created->format('M j, Y g:i:s A'); ?></p>
                        <?php if ($update_details['update_stat']) {
                            $date_updated = new DateTime($process_details->dateTimeUpdated);
                            echo '<p class="ml-2 mb-0"><span class="mr-2" style="vertical-align: middle;"><i class="fa fa-edit" style="font-size: 1.2rem;"></i></span>Updated last ' . $date_updated->format('M j, Y g:i:s A') . ' by ' . $update_details['update_details']->fname . ' ' . $update_details['update_details']->lname . '</p>';
                        } ?>
                    </span>
                </div> -->
            </div>
        </div>
        <div class="m-demo__preview mt-3" id="coachingLogReportSelectDiv" style="padding:0px !important;display: none;">
            <div class="row pt-2 mx-0">
                <div class="col-12 px-0">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 pl-2 pr-2 pb-0 float-right">
                        <div class="form-group m-form__group" style="margin-bottom: 1px;">
                            <select class="form-control m-bootstrap-select" id="confirmationStatus" name="confirmationStatus" multiple>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-demo__preview mt-3" style="padding:0px !important">
            <div id="processReportSelectDiv" class="row pt-2 mr-1 ml-1" style="background: #464646; display: none;">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 pl-2 pr-2 pb-0">
                    <div class="form-group m-form__group" style="margin-bottom: 8px;">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Checklist Name" id="checklistReportSearch">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-2 pl-2 pr-2 pb-0">
                    <div class="form-group m-form__group" style="margin-bottom: 8px;">
                        <select class="form-control m-bootstrap-select" id="checklistReportStatus" name="checklistReportStatus" multiple>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-5 col-xl-3 pl-2 pr-2 pb-0">
                    <div class="form-group m-form__group" style="margin-bottom: 8px;">
                        <select class="form-control m-bootstrap-select m_selectpicker-assignees" id="checklistReportAssignees" multiple>
                        </select>
                    </div>
                </div>
                <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-3 pl-2 pr-2 pb-0">
                    <div class="form-group m-form__group" style="margin-bottom: 8px;">
                        <select class="form-control m-bootstrap-select m_selectpicker-columns" id="checklistReportColumns" multiple>
                        </select>
                    </div>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-1 pl-2 pr-2 pb-0">
                    <!-- <button type="button" class="btn btn-outline-success m-btn--outline-2x btn-block" onclick="exportExcelReport()">
                            <i class="fa fa-download"></i>
                    </button> -->
                    <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right btn-block" data-dropdown-toggle="hover">
                        <button type="button" class="btn btn-outline-success m-btn--outline-2x btn-block m-dropdown__toggle dropdown-toggle">
                            <i class="fa fa-download"></i>
                        </button>
                        <div class="m-dropdown__wrapper">
                            <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                            <div class="m-dropdown__inner">
                                <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                        <ul class="m-nav">
                                            <li class="m-nav__section m-nav__section--first">
                                                <span class="m-nav__section-text">Export Excel Report</span>
                                            </li>
                                            <li class="m-nav__item">
                                                <a href="#" class="m-nav__link" onclick="exportExcelReport(1)">
                                                    <i class="m-nav__link-icon flaticon-interface-5"></i>
                                                    <span class="m-nav__link-text">Export only visible columns</span>
                                                </a>
                                            </li>
                                            <li class="m-nav__item">
                                                <a href="#" class="m-nav__link" onclick="exportExcelReport(0)">
                                                    <i class="m-nav__link-icon flaticon-list-1"></i>
                                                    <span class="m-nav__link-text">Export all columns</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="loadingReportSpinner" class="m-demo__preview mt-3" style="padding:0px !important; display:none">
            <div class="row">
                <div class="col-12 text-center">
                    <span class="m-loader m-loader--info mr-3" style="width: 30px; display: inline-block;"></span><span>Loading Checklist Record</span>
                </div>
            </div>
        </div>
        <div class="m-demo__preview mt-3">
            <div class="row" id="noChecklistRecord" style="display:none; margin-right: 0px; margin-left: 0px;">
                <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                    <div class="col-md-12 text-center">
                        <div class="m-demo-icon__preview">
                            <i style="font-size: 40px;" class="flaticon-notes"></i>
                        </div>
                        <span class="m-demo-icon__class">No Check list Record Found</span>
                    </div>
                </div>
            </div>
            <div class="row mr-1 ml-1" id="processTable" style="display:none;">
                <div class="col-12 p-0 table-responsive">
                    <div class="table-wrapper">
                        <table class="table table-bordered table-hover mb-0">
                            <thead style="background:#5a5a5a; color:white">
                                <tr class="" id="processTableHeader">
                                </tr>
                            </thead>
                            <tbody id="processTableBody">
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="loadMoreDiv" class="col-12" style="display:none">
                    <button type="button" id="loadMoreBtn" class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x mr-2" style="display:none">
                        <span>
                            <i class="fa fa-list"></i>
                            <span>Load More</span>
                        </span>
                    </button>
                    <span id="checkListCountInfo" class="mr-1"></span>
                    <span id="loadMoreSpinner" style="display:none;"><span class="m-loader m-loader--info mr-3" style="width: 30px; display: inline-block;margin-bottom: 5px;"></span></span>
                </div>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="loadingExportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
    <button type="button" id="closeModal" data-dismiss="modal" style="display:none">&times;</button>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class=" p-3 text-center" style="">
                <i class="fa fa-spinner text-primary fa-spin mr-2" style="font-size:20px"></i>
                EXPORTING EXCEL FILE...
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="assigneeDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="padding: 18px !important;">
                <span>
                    <i class="fa fa-user text-light mr-3" style="font-size: 21px;"></i>
                </span>
                <h5 class="modal-title" id="exampleModalLabel">Assignee Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close closeModalBtn" style="margin: -0.5rem -1rem -1rem auto !important;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
                    <div id="assigneeDetails" class="pl-2 pr-3">
                        <!-- <div class="col-12 py-3" style="background: #dadada;box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 col-lg-3"><img src="http://3.18.221.50/sz/uploads/profiles/szprofile_557.jpg" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block"></div>
                                <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                    <div class="row">
                                        <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="annexSubName">Michael Sanchez</div>
                                        <div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="annexSubPosition">Junior Software Developer II</div>
                                        <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="annexSubAccount">Software Programming Team</div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary closeModalBtn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
<script src="<?php echo base_url(); ?>assets/src/custom/js/process/reports.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>