<style type="text/css">
  .colorwhite {
    color: white !important;
  }

  .coloricons {
    color: #009688 !important;
  }

  .colorblue {
    color: #3297e4 !important;
  }

  .modallabel {
    padding: 20px;
    font-size: 1.3em;
  }

  .dropdownwidth {
    width: 100% !important;
  }

  .topdistance {
    margin-top: 50px;
  }

  .checklistcolor {
    background: #4cc34a54;
    border-right: 7px solid #4CAF50;
  }

  .coachingLogicons {
    color: #2196F3 !important;
  }

  .coachingLogcolor {
    background: #3f51b54a;
    border-right: 7px solid #673AB7;
  }

  .bottom-icons {
    color: #3c803f !important;
  }

  .pro-name {
    color: #416d90;
    font-size: 0.9em;
    font-weight: bold;
  }

  #bootstrap-duallistbox-nonselected-list_from option,
  #bootstrap-duallistbox-selected-list_from option {
    font-size: 13px !important;
  }

  .m-portlet.m-portlet--collapsed.m-portlet--head-sm:hover {
    box-shadow: rgb(136, 135, 135) 2px 3px 10px 1px !important;
    cursor: pointer;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo site_url('process'); ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i><span> - Back to Process List</span> 
            </a>
          </li>
          <li class="m-nav__separator">
          </li>
          <!-- <li class="m-nav__item">
              <a href="<?php echo base_url('process/subfolder') . '/' . $processinfo->folder_ID; ?>" class="m-nav__link">
                <span class="m-nav__link-text">Source Folder</span>
              </a>
          </li> -->
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content mb-4">
    <div class="row">
      <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="m-portlet">
          <div class="m-portlet__head portlet-header-report" style="background-color:#2c2e3eeb" id="process_type">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <!-- <a href="<?php echo site_url('process'); ?>" class="m-portlet__head-icon font-white">
                  <i class="fa fa-arrow-left"></i>
                </a> -->
                <input type="hidden" id="user_id" value="<?php echo $user; ?>">
                <input type="hidden" id="display_id">
                <input type="hidden" id="displayprocess_id">
                <input type="hidden" id="displaychecklist_id">
                <?php foreach ($user_info as $ui) { ?>
                  <input type="hidden" class="form-control" id="default_addchecklistName" name="default_addchecklistName" value="<?php echo $ui->fname;
                  echo "&nbsp;";
                  echo $ui->lname;
                  echo "'s";
                  echo "&nbsp;";
                  echo date("h:i a");
                  echo "&nbsp;";
                  echo "checklist" ?>">
                <?php } ?>
               <h4 class="m-portlet__head-text font-poppins" style="color:white !important;font-size:16px"> <?php echo $processinfo->processTitle; ?></h4>
<!-- 
                <a href="<?php echo site_url('process'); ?>" class="m-portlet__head-text font-poppins" style="color:white !important;font-size:16px">
                  <?php echo $processinfo->processTitle; ?>
                </a> -->
              </div>
            </div>
          </div>

          <div class="m-portlet__body">
            <!--begin: Search Form -->

            <!-- original = 7-784 -5 -->
            <div class="m-form m-form--label-align-right mb-4">
              <div class="row align-items-center">
                <div class="col-xl-9 order-2 order-xl-1 container-forms">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-5 mb-2">
                      <div class="m-input-icon m-input-icon--left">
                        <input id="searchChecklist" type="text" class="form-control m-input" placeholder="Search by checklist title...">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span><i class="la la-search"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="m-input-icon">
                        <select id="selectedchecklistStatus" class="form-control m-input m-input--square">
                          <option value="">All</option>
                          <option value="3">Completed</option>
                          <option value="16">Overdue</option>
                          <option value="2">Pending</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-3 order-2 order-xl-1 container-forms mt-2">
                  <div class="form-group m-form__group row align-items-center pull-right">
                    <div class="col-md-6 mb-2" id="run_a_checklist">
                    </div>
                    <!-- <div class="col-md-6 mb-2" id="schedule_a_checklist">
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="m-form m-form--label-align-right mb-5">
              <div class="row">        -->
            <!-- <div class="col-xl-12 order-2 order-xl-1">
                  <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-4">
                      <div class="m-input-icon m-input-icon--left">
                        <input id="searchChecklist" type="text" class="form-control m-input" placeholder="Search by checklist name...">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                          <span><i class="la la-search"></i></span>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="m-input-icon">
                        <select id="selectedchecklistStatus" class="form-control m-input m-input--square">
                          <option value="">All</option>
                          <option value="3">Completed</option>
                          <option value="16">Overdue</option>
                          <option value="2">Pending</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2" style="margin-right: 40px;">
                      <div class="btn-group">
                        <button type="button" class="btn btn-brand dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bullhorn"></i> Schedule Checklist</button>
                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                          <a class="dropdown-item" href="<?php echo base_url("process/schedule_checklist"); ?>">View Scheduled Checklist</a>
                          <div class="dropdown-divider"></div>
                          <a id="runcheckbutton2" href="#" class="dropdown-item" data-toggle="modal" data-target="#run_schedule-checklist-modal" onclick="resetDeadlineForm()">Schedule Checklist</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="m-input-icon">
                        <button id="runcheckbutton" href="#" onclick="defaultchecklist_name()" class="btn btn-success m-btn m-btn--icon m-btn--wide" data-toggle="modal" data-target="#run_checklist-modal">
                          <span>
                            <i class="fa fa-play"></i>
                            <span>Run Another Checklist</span>
                          </span>
                        </button>
                      </div>
                    </div>

                  </div>
                </div> -->
            <!-- </div>
            </div> -->
            <!--end: Search Form -->
            <!-- <div class="m-loader m-loader--info loading-icon load-style"></div> -->
            <div class="row" id="checkrow">
            </div>
            <div class="row no-gutters">
              <div class="col-md-8 col-12">
                <ul id="checkrow-pagination">
                </ul>
              </div>
              <div class="col-md-1 text-right col-4">
                <select class="form-control m-input form-control-sm m-input--air" id="checkrow-perpage">
                  <option>6</option>
                  <option>12</option>
                  <option>24</option>
                  <option>60</option>
                  <option>120</option>
                </select>
              </div>
              <div class="col-md-3 text-right col-8">
                <p style="font-family:Poppins;font-size:13px;text-align:right;margin-top:3px">Showing <span id="checkrow-count"></span> of <span id="checkrow-total"></span> records</p>
              </div>
            </div>
            <div class="text-center">
              <!-- <button class="btn btn-metal" id="checkLoadMore" data-whichfunction='0'>Load More</button> -->
            </div>
          </div>
        </div>
        <!--end::Portlet-->
      </div>
    </div>

  </div>
  <!--begin::Modal-->
  <div class="modal fade" id="updateChecklistmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form id="form_checklistnameupdate" name="form_checklistnameupdate" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Update Checklist Name</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Checklist Name:</label>
              <input type="text" class="form-control" id="updatechecklistName" name="updatechecklistName" placeholder="Enter Process Name">
              <span style="font-size:0.9em;color:gray" id="updatechecklistcharNum"></span>
              <input type="hidden" id="pid" value="<?php echo $processinfo->process_ID; ?>">
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent">Save Changes</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="run_schedule-checklist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content modal-lg">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Checklist schedulin' time!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="formupdate" name="formupdate" method="post">
            <div class="form-group">
              <h5 style="color:#34bfa3">Base on <strong>'<?php echo $processinfo->processTitle; ?> Template'</strong></h5>
              <?php
              foreach ($user_info as $ui) {
                ?>
                <label for="recipient-name" class="form-control-label"></label>
                <input type="text" maxlength="40" class="form-control" id="addcheck1" name="addchecklistName" value="<?php echo $processinfo->processTitle . " - {{ Date }} at {{ Time }}" ?>" placeholder="Enter Checklist Name" disabled>
                <small style="font-size:0.9em;color:gray" id=""> Example: <strong><?php echo $processinfo->processTitle . " - " . date("l M d Y") . " at " . date("h:i a"); ?></small>
              <?php } ?>
            </div>
            <div class="form-group">
              <h5 style="color:#34bfa3">Assignees <span style="color: #F44336;">*</span> <small id="assignError"></small></h5>
              <br>
              <select class="selectpicker" id="listAssignees" multiple data-selected-text-format="count > 2" data-count-selected-text="{0} Assignees" data-live-search="true">
                <?php
                foreach ($employees as $row) {
                  echo "<option value=" . $row->uid . ">" . $row->lname . ", " . $row->fname . "</option>";
                }
                ?>
              </select>

            </div>
            <br>
            <div class="form-group">
              <h5 style="color:#34bfa3">Schedule Checklist <span style="color: #F44336;">*</span> <small id="cronError"></small></h5>
              <div>
                <a href="#" class="btn btn-outline-accent btn-sm  m-btn m-btn--icon m-btn--pill" data-toggle="modal" data-target="#run_set_auto_sched">
                  <span>
                    <i class="fa fa-calendar"></i>
                    <span>Set Schedule</span>
                  </span>
                </a>
                <br>
                <small id="cronExpress" hidden></small>

              </div>

            </div>
            <br>
            <div class="form-group">
              <h5 style="color:#34bfa3">Set Deadline?</h5>
              <div>
                <label class="m-radio m-radio--success"><input class="dueAfter" id="dueAfterYes" type="radio" value="1" name="dueAfter">Yes<span></span></label><br>
                <label class="m-radio m-radio--success"><input class="dueAfter" id="dueAfterNo" type="radio" value="0" name="dueAfter" checked="checked">No<span></span></label><br>
              </div>
              <div id="divDeadline" style="display:none">
                <small>Note: Specify the Deadline.</small>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group m-form__group">
                      <label for="example-number-input" class="col-2 col-form-label">Year(s)</label>
                      <input class="form-control m-input" type="number" min="0" max="10" value="0" id="schedDeadlineYear">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group m-form__group">
                      <label for="example-number-input" class="col-2 col-form-label">Month(s)</label>
                      <input class="form-control m-input" type="number" min="0" max="12" value="0" id="schedDeadlineMonth">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group m-form__group">
                      <label for="example-number-input" class="col-2 col-form-label">day(s)</label>
                      <input class="form-control m-input" type="number" min="0" max="364" value="0" id="schedDeadlineDay">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group m-form__group">
                      <label for="example-number-input" class="col-2 col-form-label">hour(s)</label>
                      <input class="form-control m-input" type="number" min="0" max="24" value="0" id="schedDeadlineHour">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group m-form__group">
                      <label for="example-number-input" class="col-2 col-form-label">minutes(s)</label>
                      <input class="form-control m-input" type="number" min="0" max="60" value="0" id="schedDeadlineMinutes">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="form-group">
                               <h5 style="color:#34bfa3">Repeats</h5>
                               <br>
                              <select class="form-control m-select2" id="scheduleRepeat">
                         <option selected>None</option>
                         <option>Daily</option>
                         <option>Weekly</option>
                         <option>Monthly</option>
                         <option>Yearly</option>
                          </select>
                             </div>
                         <div class="form-group">
                               <h5 style="color:#34bfa3">Repeat every</h5>
                         <div>
                         
                         </div>
                         
                             </div>
                         <div class="form-group">
                               <h5 style="color:#34bfa3">Start date *</h5>
                         <input type="text" class="form-control datetimepicker-input" placeholder="Select date" id="dateActualWorked" name="dateActualWorked" data-toggle="datetimepicker" data-target="#dateActualWorked"/>
                   
                       </div>-->
          </form>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" data-id="<?php echo $processinfo->process_ID; ?>" onclick="setScheduleProcess()" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Schedule</button>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="run_set_auto_sched" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="width: 120%;margin-left: -50px;">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Set Schedule</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="overflow: scroll;">
          <div id="divDateTime">
            <div id="divMinutes">
              <table>
                <tr>
                  <td>
                    <table class="generatorMinutes">
                      <tbody>
                        <tr>
                          <th colspan="2" style="padding: 10px;background: #8BC34A;text-align: center;">Minutes</th>
                        </tr>
                        <tr>
                          <td style="padding: 15px;border: 1px solid #d4e4c1;">
                            <label class="m-radio m-radio--success"><input id="everyMinute" type="radio" checked="checked" value="*" name="minutes">
                              Every Minute<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyEvenMinute" type="radio" value="*/2" name="minutes">
                              Even Minutes<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyOddMinute" type="radio" value="1-59/2" name="minutes">
                              Odd Minutes<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every5Minute" type="radio" value="*/5" name="minutes">
                              Every 5 Minutes<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every15Minute" type="radio" value="*/15" name="minutes">
                              Every 15 Minutes<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every30Minute" type="radio" value="*/30" name="minutes">
                              Every 30 Minutes<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="specificMinute" type="radio" value="specific" name="minutes">
                              Specific<span></span></label><br>
                            <select class="selectpicker" id="specificMinuteSelect" data-text="selected" onchange="autoSelectSpecific('Minute', 'minutes')" multiple data-selected-text-format="count > 5" data-count-selected-text="{0} selected" data-live-search="true">
                              <option value="0" selected>0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                              <option value="11">11</option>
                              <option value="12">12</option>
                              <option value="13">13</option>
                              <option value="14">14</option>
                              <option value="15">15</option>
                              <option value="16">16</option>
                              <option value="17">17</option>
                              <option value="18">18</option>
                              <option value="19">19</option>
                              <option value="20">20</option>
                              <option value="21">21</option>
                              <option value="22">22</option>
                              <option value="23">23</option>
                              <option value="24">24</option>
                              <option value="25">25</option>
                              <option value="26">26</option>
                              <option value="27">27</option>
                              <option value="28">28</option>
                              <option value="29">29</option>
                              <option value="30">30</option>
                              <option value="31">31</option>
                              <option value="32">32</option>
                              <option value="33">33</option>
                              <option value="34">34</option>
                              <option value="35">35</option>
                              <option value="36">36</option>
                              <option value="37">37</option>
                              <option value="38">38</option>
                              <option value="39">39</option>
                              <option value="40">40</option>
                              <option value="41">41</option>
                              <option value="42">42</option>
                              <option value="43">43</option>
                              <option value="44">44</option>
                              <option value="45">45</option>
                              <option value="46">46</option>
                              <option value="47">47</option>
                              <option value="48">48</option>
                              <option value="49">49</option>
                              <option value="50">50</option>
                              <option value="51">51</option>
                              <option value="52">52</option>
                              <option value="53">53</option>
                              <option value="54">54</option>
                              <option value="55">55</option>
                              <option value="56">56</option>
                              <option value="57">57</option>
                              <option value="58">58</option>
                              <option value="59">59</option>
                              <option value="59">59</option>
                            </select>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td>
                    <table class="" style="margin-top: -29px;">
                      <tbody>
                        <tr>
                          <th colspan="2" style="padding: 10px;background: #8BC34A;text-align: center;">Hours</th>
                        </tr>
                        <tr>
                          <td style="padding: 15px;border: 1px solid #d4e4c1;">
                            <label class="m-radio m-radio--success"><input id="everyHour" type="radio" checked="checked" value="*" name="hours">
                              Every Hour<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyEvenHours" type="radio" value="*/2" name="hours">
                              Even Hours<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyOddHours" type="radio" value="1-23/2" name="hours">
                              Odd Hours<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every6Hours" type="radio" value="*/6" name="hours">
                              Every 6 Hours<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every12Hours" type="radio" value="*/12" name="hours">
                              Every 12 Hours<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="specificHour" type="radio" value="specific" name="hours">
                              Specific<span></span></label><br>
                            <select class="selectpicker" id="specificHourSelect" data-text="selected" onchange="autoSelectSpecific('Hour', 'hours')" multiple data-selected-text-format="count > 5" data-count-selected-text="{0} selected" data-live-search="true">
                              <option value="0" selected>Midnight</option>
                              <option value="1">1am</option>
                              <option value="2">2am</option>
                              <option value="3">3am</option>
                              <option value="4">4am</option>
                              <option value="5">5am</option>
                              <option value="6">6am</option>
                              <option value="7">7am</option>
                              <option value="8">8am</option>
                              <option value="9">9am</option>
                              <option value="10">10am</option>
                              <option value="11">11am</option>
                              <option value="12">Noon</option>
                              <option value="13">1pm</option>
                              <option value="14">2pm</option>
                              <option value="15">3pm</option>
                              <option value="16">4pm</option>
                              <option value="17">5pm</option>
                              <option value="18">6pm</option>
                              <option value="19">7pm</option>
                              <option value="20">8pm</option>
                              <option value="21">9pm</option>
                              <option value="22">10pm</option>
                              <option value="23">11pm</option>
                              <option value="23">11pm</option>
                            </select>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td>
                    <table class="generatorDays" style="margin-top: -60px;">
                      <tbody>
                        <tr>
                          <th colspan="2" style="padding: 10px;background: #8BC34A;text-align: center;">Days</th>
                        </tr>
                        <tr>
                          <td style="padding: 15px;border: 1px solid #d4e4c1;">
                            <label class="m-radio m-radio--success"><input id="everyDay" type="radio" checked="checked" value="*" name="days">
                              Every Day<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyEvenDays" type="radio" value="*/2" name="days">
                              Even Days<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyOddDays" type="radio" value="1-31/2" name="days">
                              Odd Days<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every5Days" type="radio" value="*/5" name="days">
                              Every 5 Days<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="specificDay" type="radio" value="specific" name="days">
                              Specific<span></span></label><br>
                            <select class="selectpicker" id="specificDaySelect" data-text="selected" onchange="autoSelectSpecific('Day', 'days')" multiple data-selected-text-format="count > 5" data-count-selected-text="{0} selected" data-live-search="true">
                              <option value="1" selected>1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                              <option value="11">11</option>
                              <option value="12">12</option>
                              <option value="13">13</option>
                              <option value="14">14</option>
                              <option value="15">15</option>
                              <option value="16">16</option>
                              <option value="17">17</option>
                              <option value="18">18</option>
                              <option value="19">19</option>
                              <option value="20">20</option>
                              <option value="21">21</option>
                              <option value="22">22</option>
                              <option value="23">23</option>
                              <option value="24">24</option>
                              <option value="25">25</option>
                              <option value="26">26</option>
                              <option value="27">27</option>
                              <option value="28">28</option>
                              <option value="29">29</option>
                              <option value="30">30</option>
                              <option value="31">31</option>
                              <option value="31">31</option>
                            </select>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td>
                    <table class="generatorMonths" style="margin-top: -29px;">
                      <tbody>
                        <tr>
                          <th colspan="2" style="padding: 10px;background: #8BC34A;text-align: center;">Months</th>
                        </tr>
                        <tr>
                          <td style="padding: 15px;border: 1px solid #d4e4c1;">
                            <label class="m-radio m-radio--success"><input id="everyMonth" type="radio" checked="checked" value="*" name="months">
                              Every Month<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyEvenMonths" type="radio" value="*/2" name="months">
                              Even Months<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyOddMonths" type="radio" value="1-11/2" name="months">
                              Odd Months<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every3Months" type="radio" value="*/3" name="months">
                              Every 3 Months<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="every6Months" type="radio" value="*/6" name="months">
                              Every Half Year<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="specificMonth" type="radio" value="specific" name="months">
                              Specific<span></span></label><br>
                            <select class="selectpicker" id="specificMonthSelect" data-text="selected" onchange="autoSelectSpecific('Month', 'months')" multiple data-selected-text-format="count > 5" data-count-selected-text="{0} selected" data-live-search="true">
                              <option value="1" selected>Jan</option>
                              <option value="2">Feb</option>
                              <option value="3">Mar</option>
                              <option value="4">Apr</option>
                              <option value="5">May</option>
                              <option value="6">Jun</option>
                              <option value="7">Jul</option>
                              <option value="8">Aug</option>
                              <option value="9">Sep</option>
                              <option value="10">Oct</option>
                              <option value="11">Nov</option>
                              <option value="12">Dec</option>
                              <option value="12">Dec</option>
                            </select>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  <td>
                    <table class="generatorWeekdays" style="margin-top: -90px;">
                      <tbody>
                        <tr>
                          <th colspan="2" style="padding: 10px;background: #8BC34A;text-align: center;">Weekday</th>
                        </tr>
                        <tr>
                          <td style="padding: 15px;border: 1px solid #d4e4c1;">
                            <label class="m-radio m-radio--success"><input id="everyWeekday" type="radio" checked="checked" value="*" name="weekday">
                              Every Weekday<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyMonFri" type="radio" value="1-5" name="weekday">
                              Monday-Friday<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="everyWeekdend" type="radio" value="0,6" name="weekday">
                              Weekend Days<span></span></label><br>
                            <label class="m-radio m-radio--success"><input id="specificWeek" type="radio" value="specific" name="weekday">
                              Specific<span></span></label><br>
                            <select class="selectpicker" id="specificWeekSelect" data-text="selected" onchange="autoSelectSpecific('Week', 'weekday')" multiple data-selected-text-format="count > 5" data-count-selected-text="{0} selected" data-live-search="true">
                              <option value="0" selected>Sun</option>
                              <option value="1">Mon</option>
                              <option value="2">Tue</option>
                              <option value="3">Wed</option>
                              <option value="4">Thu</option>
                              <option value="5">Fri</option>
                              <option value="6">Sat</option>
                            </select>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" data-id="<?php echo $processinfo->process_ID; ?>" onclick="setScheduleCron(this)" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Set Schedule for this Checklist</button>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->


  <!--begin::Modal-->
  <div class="modal fade" id="run_checklist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form id="form_runanotherchecklist" name="form_runanotherchecklist" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Let's run another checklist!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <h5 style="color:#34bfa3">Base on <strong>'<?php echo $processinfo->processTitle; ?> Template'</strong></h5>
              <br>
              <label for="recipient-name" class="form-control-label">Name of your checklist (For example, the name of the employee you’re onboarding) :</label>
              <input type="text" class="form-control" id="addchecklistName" name="addchecklistName">
              <span style="font-size:0.9em;color:gray" id="charNum"></span>
            </div>
            <div class="alert alert-info" role="alert" id="modalnote" style="font-weight: 400;font-size: 12px;">
              <strong>Note:</strong> This will run a checklist that automatically adds the same assignees as "<span id="modalnotename"></span>" checklist.
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="run_checklist-modal-coaching" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form id="form_runchecklist-coaching" name="form_runchecklist-coaching" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Let's run checklists!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h5 style="color:#34bfa3">Base on <strong>'<?php echo $processinfo->processTitle; ?> Template'</strong></h5>
            <br>
            <div class="row">
              <div class="col-xl-6">
                <div class="form-group">
                  <label>Select a Team</label>
                  <select class="form-control m-select2" id='team_names'>
                  </select>
                </div>
              </div>
              <div class="col-xl-6">
                <div class="form-group">
                  <label>Names from selected team</label>
                  <select class="form-control m-bootstrap-select" id="names_assignee_cl" name="names_assignee_cl" data-live-search="true" data-actions-box='true' multiple>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <br>
              <label for="recipient-name" class="form-control-label"> Notes: </label>
              <input type="text" class="form-control" id="addchecklistName_e" name="addchecklistName_e" placeholder="Enter Checklist Name">
              <span style="font-size:0.9em;color:gray" id="charNum"></span>
              <input type="hidden" value="<?php echo $processinfo->process_ID; ?>" id="fetchprocessid_d">
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--end::Modal-->


  <!--begin::Modal-->
  <div class="modal fade" id="assign_membersmodal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Members</h5>
          <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <input type="hidden" id="folprotaskcheck_id" name="">
          <input type="hidden" id="assign_type">
          <input type="hidden" id="process_d">
        </div>
        <div class="modal-body">
          <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
            <li class="nav-item m-tabs__item">
              <a class="nav-link m-tabs__link active show added_empnamez" onclick="reinitAssigneeDatatable()" data-toggle="tab" href="#tab-member" role="tab" aria-selected="true"><i class="la la-users"></i>Members</a>
            </li>
            <li id="addmem_tab" class="nav-item m-tabs__item">
              <a class="nav-link m-tabs__link add_empnamez" data-toggle="tab" href="#tab-addmember" role="tab" aria-selected="false"><i class="la la-users"></i>Add Members</a>
            </li>
          </ul>
          <div class="tab-content">

            <div class="tab-pane fade show active" id="tab-member">
              <div class="row">
                <label class="modallabel colorblue">Members</label>
                <div class="col-12">
                  <div class="form-group m-form__group" style="margin-bottom: 8px;">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search Employee Name" id="assigneeSearch">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="fa fa-search"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12" id="assigned_memberdisplay">
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="tab-addmember">
              <div class="row topdistance">
                <div class="col-xl-6">
                  <form id="" class="" name="formupdate" method="post">
                    <label for="recipient-name" class="form-control-label colorblue">Select Members by Team</label>
                    <select id="account" class="selectpicker dropdownwidth" data-live-search="true" onchange="getMembersByAccount()">
                      <optgroup data-max-options="1">
                        <option disabled selected="">Click to select</option>
                        <?php foreach ($account as $acc) { ?>
                          <option value='<?php echo $acc->acc_id ?>'><?php echo $acc->acc_name; ?></option>
                        <?php } ?>
                      </optgroup>
                    </select>
                  </form>
                  <br>
                </div>
                <div class="col-xl-6">
                  <label for="recipient-name" class="form-control-label colorblue">Select a Name</label>
                  <select id="ass_empnames" class="m-select dropdownwidth ass_getempnames" data-live-search="true">
                  </select>
                </div>
                <div class="col-12">
                  <div class="m-form__group form-group">
                    <select name="from" id="members" size="20" multiple="multiple" onchange="getaccmembers('multi')">
                    </select>
                  </div>
                </div>
              </div>
              <div class='row'>
                <label class="modallabel colorblue">Search results</label>
                <div class="col-12">
                  <table class="table m-table table-hover" id="memberdetails">
                    <thead align="center">
                      <tr>
                        <th>Name</th>
                        <th>Team</th>
                        <th>Access</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="myTable" align="center">
                    </tbody>
                  </table>
                  <div class="m-loader m-loader--info spinner-icon load-style-search" hidden="hidden"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end::Modal-->
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<link href="<?php echo base_url(); ?>assets/src/custom/css/bootstrap-duallistbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.bootstrap-duallistbox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/process/process_checklist_pending.js"></script>

<script type="text/javascript">
  var check_ids = [];

  function resetCheck() {
    $("#checkLoadMore").show();
    $("#checkLoadMore").data('whichfunction', 0);
    $("#checkrow").html("");
    check_ids = [];
  }
  $("#searchChecklist").change(function() {
    $("#checkrow-perpage").change();
  });
  $("#selectedchecklistStatus").change(function() {
    $("#checkrow-perpage").change();
  });

  function resetDeadlineForm() {
    document.getElementById("dueAfterNo").checked = true;
    $("#divDeadline").css("display", "none");
    $("#assignError").text("");
    $("#cronError").text("");
    $("#cronError").text("");
    $("#cronExpress").text("");
    $("#listAssignees").find('option').prop('selected', false);
    $("#listAssignees").selectpicker('refresh');
  }

  function setScheduleProcess() {
    var processID = <?php echo $processinfo->process_ID; ?>;
    var assignees = $("#listAssignees").val();
    var cronEx = $("#cronExpress").text();
    if (assignees == "") {
      $("#assignError").text("Required Field.");
      $("#assignError").css("color", "#F44336");

    } else {
      $("#assignError").text("Ok");
      $("#assignError").css("color", "#34bfa3");
    }
    if (cronEx == "") {
      $("#cronError").text("Required Field.");
      $("#cronError").css("color", "#F44336");

    } else {
      $("#cronError").text("Ok");
      $("#cronError").css("color", "#34bfa3");
    }
    var vidType = $(".dueAfter:checked").val();
    var deadline = "0,0,0,0,0";
    if (vidType == 1) {
      var yearz = $("#schedDeadlineYear").val();
      var monthz = $("#schedDeadlineMonth").val();
      var dayz = $("#schedDeadlineDay").val();
      var hourz = $("#schedDeadlineHour").val();
      var minz = $("#schedDeadlineMinutes").val();
      deadline = yearz + "," + monthz + "," + dayz + "," + hourz + "," + minz;
    }
    // alert(processID+" "+assignees+" "+cronEx);
    $.ajax({
      type: 'POST',
      url: "<?php echo base_url(); ?>process/add_auto_sched_cron",
      data: {
        process_ID: processID,
        assignees: assignees,
        cronEx: cronEx,
        deadline: deadline,
      },
      success: function(html) {
        if (html > 0) {
          $("#run_schedule-checklist-modal").modal("hide");
          swal('Schedule was successfully set!', 'Process was successfully set a schedule to run automatically.', 'success');
        } else {
          swal('Error!', 'Please check if all fields are supplied or Contact the System Administrator.', 'error');
        }
      }
    });
  }

  function setScheduleCron(word) {
    $("#run_set_auto_sched").modal("hide");
    var minutes = $('input[name="minutes"]:checked').val();
    minutes = (minutes == "specific") ? $("#specificMinuteSelect").val() : minutes;
    var hours = $('input[name="hours"]:checked').val();
    hours = (hours == "specific") ? $("#specificHourSelect").val() : hours;
    var days = $('input[name="days"]:checked').val();
    days = (days == "specific") ? $("#specificDaySelect").val() : days;
    var months = $('input[name="months"]:checked').val();
    months = (months == "specific") ? $("#specificMonthSelect").val() : months;
    var weekday = $('input[name="weekday"]:checked').val();
    weekday = (weekday == "specific") ? $("#specificWeekSelect").val() : weekday;
    var cronExpress = minutes + " " + hours + " " + days + " " + months + " " + weekday;
    $("#cronExpress").text(cronExpress);
  }

  function autoSelectSpecific(word, name) {
    $("#specific" + word).attr("checked", true);
  }

  //checklist name
  function defaultchecklist_name() {
    var check_defaultname = $("#default_addchecklistName").val();
    display_accountsforcl();
    $("#addchecklistName").val(check_defaultname);
    var pt = $("#process_type").attr('data-protype');
    if (pt == 1) {
      $("#run_checklist-modal").modal();
    } else if (pt == 2) {
      $("#run_checklist-modal-coaching").modal();
    }
  }

  $(function() {
    $('#names_assignee_cl').selectpicker({
      placeholder: "Select Names",
      width: '100%',
      allowClear: true
    });
    $('#team_names').select2({
      placeholder: "Select Account",
      width: '100%'
    });
    $('#ass_empnames').select2({
      placeholder: "Select Names",
      width: '100%'
    });
    $("#checkrow-perpage").change();
    var dualListBox_members = $('#members').bootstrapDualListbox({
      nonSelectedListLabel: '<span class="m--font-bolder m--font-danger">Non-selected:</span>',
      selectedListLabel: '<span class="m--font-bolder m--font-success">Selected Employee:</span>',
      selectorMinimalHeight: 150
    });

    var dualListContainer = dualListBox_members.bootstrapDualListbox('getContainer');
    dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
    dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
    dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
    dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');

    $("#checkLoadMore").click(function() {
      displaychecklists();
    });
    fetch_processtemp_permission();

    //MODAL DISMISS AND RESET
    $('#run_checklist-modal').on('hidden.bs.modal', function() {
      formReset('#form_runanotherchecklist');
      $('#charNum').text("");
      $('#addchecklistName').val("");
    })
    $('#run_checklist-modal-coaching').on('hidden.bs.modal', function() {
      formReset('#form_runchecklist-coaching');
      // $('#charNum').text("");
      $('#addchecklistName_e').val("");
      $('#names_assignee_cl').empty();
      $("#names_assignee_cl").selectpicker("destroy");

      $('#names_assignee_cl').selectpicker({
        placeholder: "Select Names",
        width: '100%'
      });
    })

    $('#updateChecklistmodal').on('hidden.bs.modal', function() {
      formReset('#form_checklistnameupdate');
      $('#updatechecklistcharNum').text("");
      $('#updatechecklistName').val("");
    });

    //FORM VALIDATION
    $('#form_runanotherchecklist').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        addchecklistName: {
          validators: {
            stringLength: {
              max: 80,
              message: 'The checklist name must be less than 80 characters'
            },
            notEmpty: {
              message: 'Oops! checklist name is required!'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_runanotherchecklist').formValidation('disableSubmitButtons', true);
      if ($('#form_runanotherchecklist').data('type') == 'Duplicate') {
        var checkListId = $('#form_runanotherchecklist').data('checklistid');
        runChecklistWithAssignees(checkListId);
      } else {
        runChecklist();
      }
    });

    $('#form_runchecklist-coaching').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        addchecklistName_e: {
          validators: {
            stringLength: {
              max: 80,
              message: 'The remarks must be less than 80 characters'
            },
            notEmpty: {
              message: 'Oops! remarks is required!'
            },
          }
        },
        names_assignee_cl: {
          validators: {
            notEmpty: {
              message: 'Oops! Assignees is/are required!.'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_runchecklist-coaching').formValidation('disableSubmitButtons', true);
      // runChecklist();
      runChecklist_forcl();
      // tests();
    });

    $('#form_checklistnameupdate').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        updatechecklistName: {
          validators: {
            stringLength: {
              max: 80,
              message: 'The checklist name must be less than 80 characters'
            },
            notEmpty: {
              message: 'Oops! checklist name is required!'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_checklistnameupdate').formValidation('disableSubmitButtons', true);
      var checklistid = $('#form_checklistnameupdate').data('id');
      saveChangesChecklist(checklistid);
    });


    //FORM VALIDATION

  });

  function checklistpermission_access() {
    var uid = $("#user_id").val();
    var processid = $("#pid").val();
    var checklist = "checklist";
    var displayid = 0;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_access",
      // data: {
      //   uid:uid,
      // },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        // checklists_display();
        //IF ADMIN
        if (result.length > 0) {
          $("#runcheckbutton").attr("hidden", false);
          displayid = 1;
          $("#displaychecklist_id").val(displayid);
          displaychecklists();
        } else {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/fetch_assignment_checklist_pending",
            data: {
              process_ID: processid,
              checkinprocess: 1
            },
            cache: false,
            success: function(res) {
              var res = JSON.parse(res.trim());
              if (res == 0) {
                // $("#runcheckbutton").remove();
                $("#runcheckbutton").attr("hidden", true);
                //DISPLAY OWN
                displayid = 2;
                $("#displaychecklist_id").val(displayid);
                displaychecklists();
              } else {
                $("#runcheckbutton").attr("hidden", false);
                displayid = 3;
                $("#displaychecklist_id").val(displayid);
                displaychecklists();
              }
            }
          });
        }
      }
    });

  }

  //CHARACTER COUNTER

  $('#addchecklistName').keyup(function() {
    var max = 80;
    var len = $(this).val().length;
    if (len >= max) {
      $('#charNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#charNum').text(char + '/80 characters remaining');
    }
  });


  $('#updatechecklistName').keyup(function() {
    var max = 80;
    var len = $(this).val().length;
    if (len >= max) {
      $('#updatechecklistcharNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#updatechecklistcharNum').text(char + '/80 characters remaining');
    }
  });

  //END OF CHARACTER COUNTER


  function noresults() {
    $("div#noresult").remove();
    $("<div class='col-lg-12' id='noresult' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records Found!</strong></h5></div>").appendTo("div#checkrow");
  }
  //JQUERY DISPLAY OF CHECKLISTS
  function displaychecklists(word) {
    var processid = $("#pid").val();
    var d_id = $("#displaychecklist_id").val();
    var uid = $("#user_id").val();
    var selectedval = $("#selectedchecklistStatus").val();

    var whichfunction = $("#checkLoadMore").data('whichfunction');
    var assignmentRuleid = 0;

    var link = (d_id === "2") ? "display_checklist" : (d_id === "1") ? "display_checklist" : (d_id === "3") ? "display_assignedchecklists_insideprocess" : " ";
    // display_ownchecklist_underprocess
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/" + link,
      data: {
        process_ID: processid,
        word: word,
        statval: selectedval,
        check_ids: check_ids,
        whichfunction: whichfunction
      },
      beforeSend: function() {
        $('.loading-icon').show();
      },
      cache: false,
      success: function(res) {
        $('.loading-icon').hide();

        var stringx = "";
        var result = JSON.parse(res.trim());

        if (result.data.length < 6) {
          $("#checkLoadMore").hide();
          $("#runcheckbutton").attr("hidden", false);
        } else {
          $("#runcheckbutton").attr("hidden", true);
        }
        $("#checkLoadMore").data('whichfunction', result.whichfunction);

        if (result.data.length != 0) {
          $.each(result.data, function(key, item) {
            check_ids.push(item.checklist.checklist_ID);
            var cid = item.checklist.checklist_ID;
            var checklist_name = item.checklist.checklistTitle;

            var substringname = "";

            if (item.checklist.processruleid == "1" || item.checklist.processruleid == "2" || item.checklist.processruleid == "3" || item.checklist.processruleid == "4") {
              $("#runcheckbutton").attr("hidden", false);
            } else if (item.checklist.folderruleid == "1" || item.checklist.folderruleid == "2" || item.checklist.folderruleid == "3" || item.checklist.folderruleid == "4") {
              $("#runcheckbutton").attr("hidden", false);
            }

            if (checklist_name.length > 22) {
              substringname = checklist_name.substring(0, 22) + "...";
            } else {
              substringname = checklist_name.substring(0, 22);
            }
            let cklistLayout = 'checklistcolor';
            let chklistIcon = 'coloricons'
            if (item.checklist.processType_ID == 2) {
              cklistLayout = 'coachingLogcolor';
              chklistIcon = 'coachingLogicons'
            }
            stringx += "<div class='col-lg-4 mb-4'>";
            stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm portletLink mb-0' data-portlet='true' id='m_portlet_tools_7' data-linkpage='<?php echo site_url('process/checklist_answerable') ?>" + '/' + processid + '/' + item.checklist.checklist_ID + "'>";
            stringx += "<div class='m-portlet__head border-accent rounded " + cklistLayout + "'>";

            stringx += "<div class='row' style='margin-top:1em'>";
            stringx += "<div class='m-portlet__head-caption'>";
            stringx += "<div class='m-portlet__head-title'>";

            if (item.checklist.status_ID == "2") {
              stringx += "<span class='m-portlet__head-icon p-0 mr-2 fa-spin'>";
              stringx += "<i class='fa fa-spinner " + chklistIcon + "' id=''></i>";
              stringx += "</span>&nbsp;&nbsp;";
            } else if (item.checklist.status_ID == "3") {
              stringx += "<span class='m-portlet__head-icon'>";
              stringx += "<i class='fa fa-check " + chklistIcon + "' id=''></i>";
              stringx += "</span>";
            }
            stringx += "<a data-toggle='tooltip' data-placement='bottom' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/checklist_answerable') ?>" + '/' + processid + '/' + item.checklist.checklist_ID + "' class='foldname'>" + substringname + "</a>";
            stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
            stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";

            stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
            stringx += " <div class='col-md-12 p-0'>";
            stringx += "<div class='progress'>"

            var alltask = item.statuses.all;
            var allcomp = item.statuses.completed;
            var allpending = item.statuses.pending;

            if (alltask == allcomp) {
              stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success cklistProgressBar' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-title='Completed'></div>"
              stringx += "</div>"
            } else if (allcomp == 0) {
              stringx += "<div class='progress-bar progress-bar progress-bar-animated cklistProgressBar' style='background:pink' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-title='Not Yet Answered'></div>"
              stringx += "</div>"
            } else {
              var percentage_pending = (allcomp / alltask) * 100;
              stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger cklistProgressBar' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-title='" + allcomp + "/" + alltask + "'></div>"
              stringx += "</div>"
            }

            stringx += "</div>";
            stringx += "</div>";

            stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
            stringx += "<div class='m-portlet__head-tools'>";
            stringx += "<ul class='m-portlet__nav'>";

            if (item.checklist.processruleid == 1 || item.checklist.processruleid == 2 || item.checklist.folderruleid == 1 || item.checklist.folderruleid == 2 || d_id == 1 || d_id == 2 || uid == item.checklist.createdBy) {

              // $("#runcheckbutton").attr("hidden", false);
              stringx += "<li class='m-portlet__nav-item p-0' data-toggle='m-tooltip' data-original-title='Change, Assign, and Remove'>";
              stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
              stringx += "<i class='la la-cog bottom-icons'></i></a>";
              stringx += "<div class='dropdown-menu dropdownstyle p-0' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
              stringx += "<button style='padding:0.6rem 1.3rem' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='updateChecklistFetch(this)' data-toggle='modal' data-target='#updateChecklistmodal'>";
              stringx += "<i class='flaticon-file'></i>&nbsp;Change Checklist name</button>";

              stringx += "<button style='padding:0.6rem 1.3rem' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_IDforAssignment(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
              stringx += "<i class='la la-users'></i>";
              stringx += "&nbsp;Assign this checklist</button>";

              stringx += "<button style='padding:0.6rem 1.3rem' data-type='Remove' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='remove_checklist(this)'>";
              stringx += "<i class='la la-trash'></i>&nbsp;Delete this Checklist</button>";


              stringx += "<button style='padding:0.6rem 1.3rem' data-type='Duplicate' data-checklistname='" + checklist_name + "' data-checklistid='" + item.checklist.checklist_ID + "' data-toggle='modal' data-target='#run_checklist-modal' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='defaultchecklist_name()'>";
              stringx += "<i class='fa fa-copy'></i>&nbsp;Duplicate this Checklist (assignees)</button>";

              stringx += "</div>";
              stringx += "</li>";

              stringx += "<li class='m-portlet__nav-item p-0' data-toggle='m-tooltip' data-original-title='Archive'>";
              stringx += "<a href='#' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_toarchive(this)' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-archive bottom-icons'></i></a> ";
              stringx += "</li>";
            }
            var createdbynames = item.statuses.names.fname + " " + item.statuses.names.lname;
            stringx += "<li class='m-portlet__nav-item p-0 dateCreatedDetails' data-datecreated='" + item.checklist.dateTimeCreated + "' data-cname='" + createdbynames + "'>";
            stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
            stringx += "<i class='la la-info-circle bottom-icons'></i>";
            stringx += "</a>";
            stringx += "</li>";

            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<span class='m-menu__link-badge'>";

            if (item.checklist.dueDate != null) {
              var dateString2 = item.checklist.dueDate;
              var dateObj2 = new Date(dateString2);
              var dateObj3 = new Date();
              var datediff = new Date(dateObj2 - dateObj3);
              var days = datediff / 1000 / 60 / 60 / 24;
              var rounddays = Math.round(days);

              var momentObj2 = moment(dateObj2);
              var momentString2 = momentObj2.format('ll');

              if (dateObj2 > dateObj3) {
                stringx += "<span class='m-badge m-badge--success m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date:" + rounddays + "left'>" + momentString2 + "</span>";
              } else if (dateObj2 < dateObj3) {
                stringx += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue!'>" + momentString2 + "</span>";
              } else if (dateObj2 == dateObj3) {
                stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";
              }
              // stringx +="<span class='m-badge m-badge--success m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date: left'>"+momentString2+"</span>";
            }
            stringx += "</span>";
            stringx += "</li>";

            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";

            stringx += "</div>";
            stringx += "</div>";
            if (item.checklist.processType_ID == 2) {
              stringx += '<span class="m-badge m-badge--brand m-badge--wide float-right" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Contains assigned tasks" style="margin-top: -1rem;background-color: #673AB7;">Coaching Log</span>';
            } else {
              stringx += '<span class="m-badge m-badge--brand m-badge--wide float-right" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Contains assigned tasks" style="margin-top: -1rem;background-color: #4caf50;;">Checklist</span>';
            }
            stringx += "</div>";
          });
        } else {
          stringx += "<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records</strong></h5></div>";
        }
        $("#checkrow").append(stringx);
      }
    });
  }

  function remove_checklist(element) {
    var pid = $("#pid").val();
    var type = "checklist";
    swal({
      title: 'Are you sure?',
      text: "All tasks under this checklist will be deleted, too.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/delete_folder_process_checklist",
          data: {
            all_ID: $(element).data('id'),
            type: type
          },
          cache: false,
          success: function() {
            // displaychecklists();
            // window.location.href = "<?php echo base_url('process/checklist_pending') ?>"+"/"+pid;
            $("#checkrow-perpage").change();
            swal(
              'Deleted!',
              'Task successfully deleted.',
              'success'
            )
          },
          error: function(res) {
            swal(
              'Oops!',
              'You cannot delete this checklist',
              'error'
            )
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Deleting a checklist has been cancelled',
          'error'
        )
      }
    });
  }

  //remove
  // function remove_checklist(element) {
  //   var pid = $("#pid").val();
  //   swal({
  //     title: 'Are you sure?',
  //     text: "All under this task will be deleted, too.",
  //     type: 'warning',
  //     showCancelButton: true,
  //     confirmButtonText: 'Yes, delete it!'
  //   }).then(function (result) {
  //     if (result.value) {
  //       $.ajax({
  //         type: "POST",
  //         url: "<?php echo base_url(); ?>process/delete_one_checklist",
  //         data: {
  //           checklist_ID: $(element).data('id')
  //         },
  //         cache: false,
  //         success: function () {
  //           displaychecklists();
  // // window.location.href = "<?php echo base_url('process/checklist_pending') ?>"+"/"+pid;
  // swal(
  //   'Deleted!',
  //   'Task successfully deleted.',
  //   'success'
  //   )
  // },
  // error: function (res) {
  //   swal(
  //     'Oops!',
  //     'You cannot delete this task ',
  //     'error'
  //     )
  // }
  // });
  //     } else if (result.dismiss === 'cancel') {
  //       swal(
  //         'Cancelled',
  //         'Deleting a process has been cancelled',
  //         'error'
  //         )
  //     }
  //   });
  // }


  function fetch_IDforAssignment(element) {
    var idforassignment = $(element).data('id');
    var typename = $(element).data('type');
    $("#folprotaskcheck_id").val(idforassignment);
    $("#assign_type").val(typename);
  }

  // function display_addedmembers(element) {
  //   var idforassignment = $(element).data('id');
  //   var typename = $(element).data('type');
  //   var stringx = "";
  //   $.ajax({
  //     type: "POST",
  //     url: "<?php echo base_url(); ?>process/display_addedmembers",
  //     data: {
  //       folProCheTask_ID: idforassignment,
  //       type: typename
  //     },
  //     beforeSend: function () {
  //       $('.load-style').show();
  //     },
  //     cache: false,
  //     success: function (res) {
  //       $('.load-style').hide();
  //       var result = JSON.parse(res.trim());
  //       $.each(result, function (key, item) {
  //         var assignID = item.assign_ID;
  //         var memberid = item.assignedTo;
  //         var assignmentRule = item.assignmentRule_ID;
  //         $.ajax({
  //           type: "POST",
  //           url: "<?php echo base_url(); ?>process/fetch_membersadded",
  //           data: {
  //             uid: item.assignedTo
  //           },
  //           cache: false,
  //           success: function (res) {
  //             var re = JSON.parse(res.trim());
  //             $.each(re, function (key, data) {
  //               stringx += "<tr>";
  //               stringx += "<td>" + data.fname + " " + data.lname + "</td>";
  //               stringx += "<td>" + data.acc_name + "</td>";
  //               stringx += "<td><select id='assRule" + assignID + "' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
  //               if (data.assignmentRule_ID == "5") {
  //                 stringx += "<option value='" + data.assignmentRule_ID + "'>Can Answer</option>";
  //                 stringx += "<option value='6'>Can Edit and Answer</option>";
  //               } else if (data.assignmentRule_ID == "6") {
  //                 stringx += "<option value='" + data.assignmentRule_ID + "'>Can Edit and Answer</option>";
  //                 stringx += "<option value='5'>Can Answer</option>";
  //               }
  //               stringx += "</optgroup></select></td>";
  //               stringx += "<td><button onclick='removemember_assignment(this)' data-toggle='m-tooltip' data-id='" + assignID + "' title='Remove this member' data-original-title='Tooltip title' class='btn btn-danger m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-remove'></i></button></td>";
  //               stringx += "</tr>";
  //             });
  //             $("#added_members tbody").html(stringx);
  //             $("#assRule" + assignID).val(assignmentRule);
  //           }
  //         });
  //       });
  //     }
  //   });
  // }

  function addmember_assignment(element) {
    var apid = $(element).data('id');
    var pid = $("#process_d").val();
    var fptc_id = $("#folprotaskcheck_id").val();
    // var assigntype = $("#assign_type").val();
    var assigntype = "checklist";
    var assignrule = $("#assignmentRule :selected").val();

    swal({
      title: 'Are you sure?',
      text: "This member will be assigned to this folder/process template/checklist",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/add_assignment",
          data: {
            type: assigntype,
            assignmentRule_ID: assignrule,
            userid: apid,
            folprotaskcheck_id: fptc_id,
            pro: pid,
            ptype: 0
          },
          cache: false,
          success: function(res) {
            var resObj = $.parseJSON(res.trim());
            if (resObj.checker == 1) {
              console.log("checklist assigned");
              swal("This employee cannot be added", "(This employee is being assigned to checklist/s under this process template)", "info");
            } else if (resObj.checker == 2) {
              console.log("task assigned");
              swal("This employee cannot be added", "(This employee is being assigned to task/s under this process template)", "info");
            } else if (resObj.checker == 3) {
              swal("This employee cannot be added", "(This employee is being assigned to process template/s of these checklists or tasks)", "info");
            } else {
              console.log("should saved");
              systemNotification(resObj.notifid.notif_id);
              // addmember_assignment();
              $.notify({
                message: 'Successfully Assigned'
              }, {
                type: 'success',
                timer: 1000,
                z_index: 99999
              });
              $("#memberdetails tbody").html("");
              $("#members").html("");
              show_lists_toassign();
            }
            // $("#assign_membersmodal").modal('hide');
          },
          error: function(res) {
            swal(
              'Oops!',
              'Cannot assign',
              'error'
            )
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Assigning user has been cancelled',
          'error'
        )
      }
    });

    //  $.ajax({
    //   type: "POST",
    //   url: "<?php echo base_url(); ?>process/add_assignment",
    //   data: {
    //     type: assigntype,
    //     assignmentRule_ID: assignrule,
    //     applicantid: apid,
    //     folprotaskcheck_id: fptc_id
    //   },
    //   cache: false,
    //   success: function (res) {
    //     swal({
    //       position: 'center',
    //       type: 'success',
    //       title: 'Successfully Assigned a member',
    //       showConfirmButton: false,
    //       timer: 1500
    //     });
    //     var result = JSON.parse(res.trim());
    //   }
    // });
  }

  function removemember_assignment(element) {
    swal({
      title: 'Are you sure?',
      text: "This member will be removed from accessing this file.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/delete_assignment",
          data: {
            assign_ID: $(element).data('id')
          },
          cache: false,
          success: function() {
            reinitAssigneeDatatable();
            swal(
              'Deleted!',
              'Member successfully Removed.',
              'success'
            )
          },
          error: function(res) {
            swal(
              'Oops!',
              'Something is wrong with your code!',
              'error'
            )
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Removing member has been cancelled',
          'error'
        )
      }
    });
  }

  function unable_toremove() {
    swal("Removal of assignee is not allowed", "(This is a coaching log checklist)", "info");
  }

  // function getaccmembers(access) {
  //   var membersIds = 0;
  //   var link = "";
  //   if (access == "multi") {
  //     membersIds = $("#members").val();
  //     link = "fetch_allemployeedetails";
  //   } else {
  //     membersIds = $("#ass_empnames").val();
  //     link = "fetch_oneemployeedetails";
  //   }
  //   // var membersIds = $("#members").val();
  //   // var assigntype = $("#assign_type").val();
  //   // var folprotaskcheckID = $("#folprotaskcheck_id").val();
  //   var accountname = $("#account :selected").text();
  //   var pro_id = $("#process_d").val();
  //   $.ajax({
  //     type: "POST",
  //     url: "<?php echo base_url(); ?>process/" + link,
  //     data: {
  //       membersIds: membersIds
  //     },
  //     beforeSend: function() {
  //       $('.spinner-icon').show();
  //     },
  //     cache: false,
  //     success: function(res) {
  //       $('.spinner-icon').remove();
  //       var result = JSON.parse(res.trim());
  //       console.log(result);
  //       var stringx = "";
  //       $.each(result.employees, function(key, data) {
  //         stringx += "<tr>";
  //         stringx += "<td>" + data.fname + " " + data.lname + "</td>";
  //         stringx += "<td>" + data.acc_name + "</td>";
  //         stringx += "<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
  //         stringx += "<option value='5'>Can Answer</option>";
  //         stringx += "<option value='6'>Can Edit and Answer</option>";
  //         stringx += "</optgroup></select></td>";
  //         stringx += "<td><button onclick='addmember_assignment(this)' data-toggle='m-tooltip' data-pro='" + pro_id + "' data-id='" + data.uid + "' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
  //         stringx += "</tr>";
  //       });
  //       $("#memberdetails tbody").html(stringx);
  //     }
  //   });
  // }

  function resetform() {
    $("#account").prop("selectedIndex", 0).change();
    $("#member").prop("selectedIndex", 0).change();
    $("#memberdetails tbody").html("");
    $("#added_members tbody").html("");
  }

  function getchecklistval(element) {
    $('.checkcontent').hide();
    if (element.value == "100") {
      $('.checkcontent').show();
    }
    $('.checkcontent .checkName').each(function() {
      if (element.value == "2") {
        $('.chli2').show();
      } else if (element.value == "3") {
        $('.chli3').show();
      }
    });
  }

  $('.dueAfter').on('click', function() {
    var vidType = $(".dueAfter:checked").val();
    if (vidType == 1) {
      $("#divDeadline").css("display", "block");
    } else {
      $("#divDeadline").css("display", "none");
    }
  });

  function runChecklist() {
    var pid = <?php echo $processID; ?>;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_anotherchecklist",
      data: {
        process_ID: pid,
        checklistTitle: $("#addchecklistName").val()
      },
      cache: false,
      success: function(res) {
        $('#run_checklist-modal').modal('hide');
        // displaychecklists();
        $("#checkrow-perpage").change();
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added checklist',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
  }
  //nicca codes
  function runChecklistWithAssignees(checkListId) {
    var pid = <?php echo $processID; ?>;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>Process/add_anotherchecklist_withassignees",
      data: {
        process_ID: pid,
        checkListId: checkListId,
        checklistTitle: $("#addchecklistName").val()
      },
      cache: false,
      success: function(res) {
        $('#run_checklist-modal').modal('hide');
        // displaychecklists();
        $("#checkrow-perpage").change();
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added checklist',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
  }
  //end nicca codes
  function updateChecklistFetch(element) {
    var checklistTitle = $("#updatechecklistName").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetchchecklist_update",
      data: {
        checklist_ID: $(element).data('id'),
        checklistTitle: checklistTitle
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        console.log(res);
        var data = res;
        $("#updatechecklistName").val(data.checklistTitle);
        $("#form_checklistnameupdate").data('id', data.checklist_ID);
      }
    });
  }

  function saveChangesChecklist(checklistid) {
    var pid = $("#pid").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_checklist",
      data: {
        checklist_ID: checklistid,
        checklistTitle: $("#updatechecklistName").val()
      },
      cache: false,
      success: function(res) {
        resetCheck();
        // displaychecklists();
        $("#checkrow-perpage").change();
        swal({
          position: 'center',
          type: 'success',
          title: 'checklist successfully updated',
          showConfirmButton: false,
          timer: 1500
        });
        $("#updateChecklistmodal").modal('hide');
      }
    });
  }

  function checklistset_toarchive(element) {
    var chid = $(element).data('id');
    var isArchived = 1;
    swal({
      title: 'Are you sure?',
      text: "This checklist will be inactive.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, set to archive!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/updatechecklist_archive",
          data: {
            checklist_ID: chid,
            isArchived: isArchived
          },
          cache: false,
          success: function(res) {
            var result = JSON.parse(res.trim());
            swal({
              position: 'center',
              type: 'success',
              title: 'Archived the checklist',
              showConfirmButton: false,
              timer: 1500
            });
            // displaychecklists();
            $("#checkrow-perpage").change();
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Setting checklist to archive has been cancelled',
          'error'
        )
      }
    });
  }
  //ASSIGNING
  // function display_addedmembers(element) {
  //   var idforassignment = $(element).data('id');
  //   var typename = $(element).data('type');
  //   var folprotaskcheckID = $("#folprotaskcheck_id").val();
  //   var stringx = "";

  //   $.ajax({
  //     type: "POST",
  //     url: "<?php echo base_url(); ?>process/display_addedmembers",
  //     data: {
  //       folProCheTask_ID: idforassignment,
  //       type: typename
  //     },
  //     beforeSend: function () {
  //       $('.spinner-icon').show();
  //     },
  //     cache: false,
  //     success: function (res) {
  //       $('.spinner-icon').hide();
  //       var result = JSON.parse(res.trim());
  //       $.each(result, function (key, item) {
  //         var assignID = item.assign_ID;
  //         var memberid = item.assignedTo;
  //         var assignmentRule = item.assignmentRule_ID;
  //         $.ajax({
  //           type: "POST",
  //           url: "<?php echo base_url(); ?>process/fetch_membersadded",
  //           data: {
  //             uid: item.assignedTo,
  //             fptc_id: folprotaskcheckID,
  //             type: typename
  //           },
  //           cache: false,
  //           success: function (res) {
  //             var re = JSON.parse(res.trim());
  //             $.each(re, function (key, data) {
  //               stringx += "<tr>";
  //               stringx += "<td>" + data.fname + " " + data.lname + "</td>";
  //               stringx += "<td>" + data.acc_name + "</td>";
  //               stringx += "<td><select data-id='" + assignID + "' onchange='access_permissionChange(this)' id='assRule" + assignID + "' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
  //               if (data.assignmentRule_ID == "5") {
  //                 stringx += "<option value='" + data.assignmentRule_ID + "'>Can Answer</option>";
  //                 stringx += "<option value='6'>Can Edit and Answer</option>";
  //               } else if (data.assignmentRule_ID == "6") {
  //                 stringx += "<option value='" + data.assignmentRule_ID + "'>Can Edit and Answer</option>";
  //                 stringx += "<option value='5'>Can Answer</option>";
  //               }
  //               stringx += "</optgroup></select></td>";
  //               stringx += "<td><button onclick='removemember_assignment(this)' data-toggle='m-tooltip' data-id='" + assignID + "' title='Remove this member' data-original-title='Tooltip title' class='btn btn-danger m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-remove'></i></button></td>";
  //               stringx += "</tr>";
  //             });
  //             $("#added_members tbody").html(stringx);
  //             $("#assRule" + assignID).val(assignmentRule);
  //           }
  //         });
  //       });
  //     }
  //   });
  // }


  function getaccmembers(access) {
    var membersIds = 0;
    var link = "";
    if (access == "multi") {
      membersIds = $("#members").val();
      link = "fetch_allemployeedetails";
    } else {
      membersIds = $("#ass_empnames").val();
      link = "fetch_oneemployeedetails";
    }
    // var membersIds = $("#members").val();
    var assigntype = $("#assign_type").val();
    // var folprotaskcheckID = $("#folprotaskcheck_id").val();
    var accountname = $("#account :selected").text();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/" + link,
      data: {
        membersIds: membersIds
      },
      beforeSend: function() {
        $('.spinner-icon').show();
      },
      cache: false,
      success: function(res) {
        $('.spinner-icon').remove();
        var result = JSON.parse(res.trim());
        var stringx = "";
        $.each(result.employees, function(key, data) {
          stringx += "<tr>";
          stringx += "<td>" + data.fname + " " + data.lname + "</td>";
          stringx += "<td>" + data.acc_name + "</td>";
          stringx += "<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
          stringx += "<option value='5'>Can Answer</option>";
          stringx += "<option value='6'>Can Edit and Answer</option>";
          stringx += "</optgroup></select></td>";
          stringx += "<td><button onclick='addmember_assignment(this)' data-reload='1' data-toggle='m-tooltip' data-id='" + data.uid + "' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
          stringx += "</tr>";
        });
        $("#memberdetails tbody").html(stringx);
      }
    });
  }

  function getmember_name() {
    var empid = $("#member").val();
    var empname = $("#member :selected").text();
    var assigntype = $("#assign_type").val();
    var folprotaskcheckID = $("#folprotaskcheck_id").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_oneemployee",
      data: {
        emp_id: empid,
        fptcID: folprotaskcheckID,
        assign_type: assigntype
      },
      cache: false,
      success: function(res) {
        var stringx = "";
        var result = JSON.parse(res.trim());
        $.each(result, function(key, data) {
          stringx += "<tr>";
          stringx += "<td>" + data.fname + " " + data.lname + "</td>";
          stringx += "<td>" + data.acc_name + "</td>";
          stringx += "<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
          stringx += "<option value='5'>Can Answer</option>";
          stringx += "<option value='6'>Can Edit and Answer</option>";
          stringx += "</optgroup></select></td>";
          stringx += "<td><button onclick='addmember_assignment(this)' data-reload='2' data-toggle='m-tooltip' data-id='" + data.uid + "' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
          stringx += "</tr>";
        });
        $("#memberdetails tbody").html(stringx);
      }
    });
  }

  function fetch_IDforAssignment(element) {
    var idforassignment = $(element).data('id');
    var typename = $(element).data('type');
    $("#folprotaskcheck_id").val(idforassignment);
    $("#assign_type").val(typename);
  }

  function access_permissionChange(element) {
    var assignmentid = $(element).data('assignid');
    var changepermission = $(element).val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_assignmentpermission",
      data: {
        assid: assignmentid,
        changepermission: changepermission
      },
      cache: false,
      success: function(res) {
        reinitAssigneeDatatable();
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Changed Acces Permission  ',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  }
  //DATATABLE
  function reinitAssigneeDatatable() {
    $('#assigned_memberdisplay').mDatatable('destroy');
    assigneeDatatable.init($('#assigneeSearch').val(), $('#assign_membersmodal').data('id'), $('#assign_membersmodal').data('type'));
  }
  $('#assigneeSearch').on('keyup', function() {
    reinitAssigneeDatatable();
  })

  function initAssignee(element) {
    console.log("init assignee");
    $('#assigned_memberdisplay').mDatatable('destroy');
    $('#assign_membersmodal').data('type', $(element).data('type'));
    $('#assign_membersmodal').data('id', $(element).data('id'));
    assigneeDatatable.init($('#assigneeSearch').val(), $(element).data('id'), $(element).data('type'));
  }

  function selectChange(element) {
    console.log($(element).val())
    console.log($(element).data('assignid'));
  }
  var assigneeDatatable = function() {
    var assignee = function(searchVal, folProcIdVal, typeval) {
      var options = {
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'POST',
              url: baseUrl + "/process/list_assignee_datatable",
              params: {
                query: {
                  folProcId: folProcIdVal,
                  type: typeval,
                  assigneeSearch: searchVal
                },
              },
            }
          },
          saveState: {
            cookie: false,
            webstorage: false
          },
          pageSize: 5,
          serverPaging: true,
          serverFiltering: true,
          serverSorting: true,
        },
        layout: {
          theme: 'default', // datatable theme
          class: '', // custom wrapper class
          scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
          height: 550, // datatable's body's fixed height
          footer: false // display/hide footer
        },
        sortable: true,
        pagination: true,
        toolbar: {
          // toolbar placement can be at top or bottom or both top and bottom repeated
          placement: ['bottom'],

          // toolbar items
          items: {
            // pagination
            pagination: {
              // page size select
              pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
            },
          }
        },
        search: {
          input: $('#assigneeSearch'),
        },
        rows: {
          afterTemplate: function(row, data, index) {},
        },
        // columns definition
        columns: [{
          field: "fname",
          title: "Employee Name",
          width: 180,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var html = row.fname + " " + row.lname;
            return html;
          },
        }, {
          field: "acc_name",
          title: "Team",
          width: 160,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var html = row.acc_name;
            return html;
          },
        }, {
          field: "lname",
          title: "Access",
          width: 220,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var pt = $("#process_type").attr('data-protype');
            var rule1 = '';
            var rule2 = '';
            var rule3 = '';
            var rule4 = '';
            var rule5 = '';
            var rule6 = '';
            var rule7 = '';
            var rule8 = '';
            if (row.assignmentRule_ID == 1) {
              rule1 = "selected";
            } else if (row.assignmentRule_ID == 2) {
              rule2 = "selected";
            } else if (row.assignmentRule_ID == 3) {
              rule3 = "selected";
            } else if (row.assignmentRule_ID == 4) {
              rule4 = "selected";
            } else if (row.assignmentRule_ID == 5) {
              rule5 = "selected";
            } else if (row.assignmentRule_ID == 6) {
              rule6 = "selected";
            } else if (row.assignmentRule_ID == 7) {
              rule7 = "selected";
            } else if (row.assignmentRule_ID == 8) {
              rule8 = "selected";
            }
            var html = '';
            if (row.type == "folder" || row.type == "process") {
              html = '<select onchange="access_permissionChange(this)" data-assignid="' + row.assign_ID + '" class="form-control m-input m-input--square">' +
                '<option value="1" ' + rule1 + '>Can Edit, View and Run</option>' +
                '<option value="2" ' + rule2 + '>Can Edit, View Own and Run</option>' +
                '<option value="3" ' + rule3 + '>Can View and Run</option>' +
                '<option value="4" ' + rule4 + '>Can View Own and Run</option>' +
                '<option value="7" ' + rule7 + '>Can View Own</option>' +
                '<option value="8" ' + rule8 + '>Can View All</option>' +
                '</select>';
            } else if (row.type == "checklist" || row.type == "task") {
              if (pt == 2) {
                html = '<select onchange="access_permissionChange(this)" data-assignid="' + row.assign_ID + '" class="form-control m-input m-input--square">' +
                  '<option  disabled value="9" selected="">Coaching Log Assigned</option>' +
                  '</select>';
              } else {
                html = '<select onchange="access_permissionChange(this)" data-assignid="' + row.assign_ID + '" class="form-control m-input m-input--square">' +
                  '<option value="5" ' + rule5 + '>Can Answer</option>' +
                  '<option value="6" ' + rule6 + '>Can Edit and Answer</option>' +
                  '</select>';
              }
            }
            return html;
          },
        }, {
          field: "assign_ID",
          title: "Action",
          width: 90,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var html = '';
            var pt = $("#process_type").attr('data-protype');
            if (pt == 2) {
              html = '<button onclick="unable_toremove()" data-toggle="m-tooltip" data-id="' + row.assign_ID + '" title="Remove this member" data-original-title="Tooltip title" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="fa fa-remove"></i></button>';
            } else {
              html = '<button onclick="removemember_assignment(this)" data-toggle="m-tooltip" data-id="' + row.assign_ID + '" title="Remove this member" data-original-title="Tooltip title" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="fa fa-remove"></i></button>';
            }
            return html;
          },
        }],
      };
      var datatable = $('#assigned_memberdisplay').mDatatable(options);
    };
    return {
      init: function(searchVal, folProcIdVal, typeval) {
        assignee(searchVal, folProcIdVal, typeval);
      }
    };
  }();

  var getMembersByAccount = function() {
    var acc_id = $("#account").val();
    var c = "checklist";
    if (acc_id !== null) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Process/fetch_membersPerAccount_2nd",
        data: {
          acc_id: acc_id,
          assigned_pct: $("#folprotaskcheck_id").val(),
          ass_type: c
        },
        cache: false,
        success: function(res) {
          res = JSON.parse(res.trim());
          $("#members").html("");
          var str = "";
          $.each(res.members, function(key, obj) {
            str += '<option value="' + obj.uid + '" class="text-capitalize">' + obj.lname + ' ' + obj.fname + '</option>';
          });
          $("#members").html(str);
          $('#members').bootstrapDualListbox('refresh');
        }
      });
    }

  };

  $("#assign_membersmodal").on('hide.bs.modal', function() {
    $("#account").val('');
    // $('#members option:selected').each(function() {
    //   $(this).prop('selected', false);
    // });
    $("#members").html('');
    $('#members').bootstrapDualListbox('refresh');
    $("#myTable").html("");

    $('.add_empnamez').removeClass("show active");
    $('.added_empnamez').addClass("show active");
    $('#tab-addmember').removeClass("show active");
    $('#tab-member').addClass("show active");
  })

  $("#run_checklist-modal").on('show.bs.modal', function(e) {
    var type = $(e.relatedTarget).data('type'); //Duplicate
    if (type === 'Duplicate') {
      var checklistname = $(e.relatedTarget).data('checklistname');
      $("#modalnotename").html(checklistname);
      $("#modalnote").show();
      var checkListId = $(e.relatedTarget).data('checklistid');
      $('#form_runanotherchecklist').data('checklistid', checkListId);
      $('#form_runanotherchecklist').data('type', type);
    } else {
      $("#modalnote").hide();
    }
  });

  // REVISED CODES

  function fetch_processtemp_permission() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/Checkprocess_templatepermission",
      data: {
        process_id: $("#pid").val()
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx1 = "";
        var stringx2 = "";
        console.log(result);
        stringx1 += '<div class="m-input-icon m-input-icon--left">';
        stringx1 += '<button class="btn btn-brand dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bullhorn"></i>&nbsp; Schedule Checklist</button>';
        stringx1 += '<div class="dropdown-menu dropdownstyle"x-placement="bottom-start">';
        stringx1 += '<a class="dropdown-item" href="<?php echo base_url("process/schedule_checklist"); ?>">View Scheduled Checklist</a>';
        stringx1 += '<div class="dropdown-divider"></div>';
        stringx1 += '<a id="runcheckbutton2" href="#" class="dropdown-item" data-toggle="modal" data-target="#run_schedule-checklist-modal" onclick="resetDeadlineForm()">Schedule Checklist</a>';
        stringx1 += ' </div></div>';

        stringx2 += '<div class="m-input-icon m-input-icon--left">';
        stringx2 += '<button id="runcheckbutton" href="#" onclick="defaultchecklist_name()" class="btn btn-success m-btn m-btn--icon">';
        stringx2 += '<span><i class="fa fa-play"></i><span>Run Another Checklist</span></span></button>';
        stringx2 += '</div>';
        // checklists_display();
        if (result == 1) {
          $("#run_a_checklist").html(stringx2);
          $("#schedule_a_checklist").html(stringx1);
        }
      }
    });
  }


  function checklists_display(limiter, perPage, callback) {
    var activeSearch = $("#selectedchecklistStatus").val();
    var inputSearch = $("#searchChecklist").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/checklists_display",
      data: {
        limiter: limiter,
        perPage: perPage,
        activeSearch: activeSearch,
        inputSearch: inputSearch,
        pageaccess: 2,
        process_id: $("#pid").val()
      },
      beforeSend: function() {
        mApp.block('#content', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'brand',
          size: 'lg'
        });
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        $("#process_type").attr('data-protype', res.process_type[0].processType_ID);
        $("#checkrow-total").html(res.total);
        $("#checkrow-total").data('count', res.total);
        $("#checkrow").html("");
        var stringx = "";

        $.each(res.checklist_details, function(key, item) {
          var checklist_name = item.checklist.checklistTitle;
          var substringname = "";
          var duedate ="";
          var str_ddate ="";
          substringname = checklist_name;
          let cklistLayout = 'checklistcolor';
          let chklistIcon = 'coloricons'
          var dty= new Date();
                 
          if(item.checklist.dueDate!=null){
            var das = new Date(item.checklist.dueDate);
            var diffday = new Date(das - dty);
            var caldays = diffday / 1000 / 60 / 60 / 24;
            var rounddays = Math.round(caldays);

            var ob_j = moment(das);
            str_ddate = ob_j.format('MMM DD, YYYY');

            if(das < dty){
              duedate += "<li class='m-portlet__nav-item p-0'>";
              duedate += "<span class='m-menu__link-badge ml-1'>";
              duedate += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='"+str_ddate+"'>Overdued</span>";
              duedate += "</span>";
              duedate += "</li>";
            }
          }

          if (item.checklist.processType_ID == 2) {
            cklistLayout = 'coachingLogcolor';
            chklistIcon = 'coachingLogicons'
          }

          stringx += "<div id='gettype_cvalue"+item.checklist.checklist_ID+"' data-tit='"+item.checklist.checklistTitle+"' class='col-lg-4 mb-4'>";
          stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm portletLink mb-0 " + cklistLayout + "' data-portlet='true' id='m_portlet_tools_7' data-linkpage='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "'>";
          stringx += "<div class='m-portlet__head border-accent rounded ' style='display: inline-block;border-bottom:unset'>";
          stringx += "<div class='row' style='margin-top:1em'>";
          stringx += "<div class='m-portlet__head-caption'>";
          stringx += "<div class='m-portlet__head-title' style='width: 100%;'>";
          // stringx += "<span class='m-portlet__head-icon'>";

          // Check or clock icon
          // if (item.checklist.status_ID == 2) {
          //   stringx += "<span class='m-portlet__head-icon p-0 mr-2 fa-spin'>";
          //   stringx += "<i class='fa fa-spinner " + chklistIcon + "' id=''></i>";
          //   stringx += "</span>&nbsp;&nbsp;";
          // } else if (item.checklist.status_ID == 3) {
          //   stringx += "<span class='m-portlet__head-icon p-0 mr-2'>";
          //   stringx += "<i class='fa fa-check " + chklistIcon + "' id=''></i>";
          //   stringx += "</span>";
          // }

          // stringx += "</span>";
          if (item.checklist.processType_ID == 2) {
            stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-finalchcklisttitle=\"" + checklist_name + "\" style='color:#484747;display: inline-block;font-size: 1.2rem;font-weight: 500;width: 100%;' href='<?php echo site_url('process/coaching_log') ?>" + '/' + item.checklist.checklist_ID + "' class='foldname checklistTitleInfo text-truncate'>" + substringname + "</a>";
          } else {
            stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-finalchcklisttitle=\"" + checklist_name + "\" style='color:#484747;display: inline-block;font-size: 1.2rem;font-weight: 500;width: 100%;' href='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "' class='foldname checklistTitleInfo text-truncate'>" + substringname + "</a>";
          }
          stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
          stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
          stringx += "</div>";
          stringx += "</div>";
          stringx += "</div>";

          // stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";
          stringx +='<div class="row">';
            stringx +='<div class="createdby pro-name text-truncate" style=" width: 100%; display: inline-block;">'+item.checklist.processTitle+'</div>';
          stringx +='</div>';
          stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
          stringx += " <div class='col-md-12 p-0'>";
          stringx += "<div class='progress'>"

          // Start Status Bar
          // stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
          // stringx += "</div>"
          // End

          if (item.statuses.all == item.statuses.completed) {
            stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
            stringx += "</div>"
          } else if (item.statuses.completed == 0) {
            stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
            stringx += "</div>"
          } else {
            var percentage_pending = (item.statuses.completed / item.statuses.all) * 100;
            stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-toggle='m-tooltip' data-placement='top' data-original-title='" + item.statuses.completed + "/" + item.statuses.all + "'></div>"
            stringx += "</div>";
          }

          stringx += "</div>";
          stringx += "</div>";

          stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;margin-left: -1.4em;'>";
          stringx += "<div class='m-portlet__head-tools pb-2'>";
          stringx += "<ul class='m-portlet__nav'>";

          // if

          if (item.checklist.assignmentRule_ID == "owned" || item.checklist.assignmentRule_ID == "admin" || item.checklist.assignmentRule_ID == 1 || item.checklist.assignmentRule_ID == 2 || item.checklist.assignmentRule_ID == 6) {
            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
            stringx += "<i class='la la-cog bottom-icons'></i></a>";
            stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";

            // Change checklist name
            stringx += "<button style='padding:0.6rem 1.3rem' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='updateChecklistFetch(this)' data-toggle='modal' data-target='#updateChecklistmodal'>";
            stringx += "<i class='flaticon-file'></i>&nbsp;Change Checklist name</button>";

            // Assign
            stringx += "<button style='padding:0.6rem 1.3rem' class='dropdown-item' type='button' data-process='" + item.checklist.process_ID + "' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_proIDforAssignment(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
            stringx += "<i class='la la-users'></i>";
            stringx += "&nbsp;Assign this checklist</button>";

            // Delete
            stringx += "<button style='padding:0.6rem 1.3rem' data-type='Remove' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='remove_checklist(this)'>";
            stringx += "<i class='la la-trash'></i>&nbsp;Delete this Checklist</button>";

            stringx += "<button style='padding:0.6rem 1.3rem' data-type='Duplicate' data-checklistname='" + checklist_name + "' data-checklistid='" + item.checklist.checklist_ID + "' data-toggle='modal' data-target='#run_checklist-modal' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='defaultchecklist_name()'>";
            stringx += "<i class='fa fa-copy'></i>&nbsp;Duplicate this Checklist (assignees)</button>";
            stringx += "</div>";
            stringx += "</li>";

            //ARCHIVE ICON
            stringx += "<li class='m-portlet__nav-item p-0' data-toggle='m-tooltip' data-original-title='Archive'>";
            stringx += "<a href='#' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_toarchive(this)' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-archive bottom-icons'></i></a> ";
            stringx += "</li>";
            //end ARCHIVE ICON
          }
          // end if
          var createdbynames = item.statuses.names.fname + " " + item.statuses.names.lname;
          stringx += "<li class='m-portlet__nav-item p-0 dateCreatedDetails' data-datecreated='" + item.checklist.dateTimeCreated + "' data-cname='" + createdbynames + "'>";
          stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
          stringx += "<i class='la la-info-circle bottom-icons'></i>";
          stringx += "</a>";
          stringx += "</li>";
          let notes = item.checklist.remarks;
          if (item.checklist.remarks == null) {
            notes = "No Remarks Found";
          }
          stringx += "<li class='m-portlet__nav-item p-0 checklistNotes' data-notes='" + notes + "'>";
          stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
          stringx += "<i class='la la-commenting bottom-icons'></i>";
          stringx += "</a>";
          stringx += "</li>";

          stringx += "<li class='m-portlet__nav-item p-0'>";
          stringx += "<span class='m-menu__link-badge'>";

          //options
          // stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'> no date yet</span>";
          // stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";

          // options

          stringx += "</span>";
          stringx += "</li>";

          // if
          if(item.checklist.dueDate!=null && item.checklist.task_permission == "yes"){
            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<span class='m-menu__link-badge ml-1'>";
            stringx += "<span class='m-badge m-badge--brand m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Contains assigned tasks and has due date'>Task - "+str_ddate+"</span>";
            stringx += "</span>";
            stringx += "</li>";
          }else if(item.checklist.dueDate!=null && duedate==""){
            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<span class='m-menu__link-badge ml-1'>";
            stringx += "<span class='m-badge m-badge--info m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date'>"+str_ddate+"</span>";
            stringx += "</span>";
            stringx += "</li>";
          }else if(item.checklist.task_permission == "yes") {
            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<span class='m-menu__link-badge ml-1'>";
            stringx += "<span class='m-badge m-badge--brand m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Contains assigned tasks'>Assigned Tasks</span>";
            stringx += "</span>";
            stringx += "</li>";
          }else if(duedate!=""){
            stringx += duedate;
          }
          // if

          stringx += "</ul>";
          stringx += "</div>";
          stringx += "</div>";

          stringx += "</div>";
          stringx += "</div>";
          let spin = "";
          let cklistStatIcon = "fa fa-check "+ chklistIcon;
          let checklistStat = "already completed"
          if (item.checklist.status_ID == 2) {
            spin = "fa-spin";
            cklistStatIcon = "fa fa-spinner " + chklistIcon;
            checklistStat = "is still ending"
          } 

          if (item.checklist.processType_ID == 2) {
            stringx += '<div class="btn float-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;margin-top: -1rem;background-color: #673AB7;border-top-right-radius: 0;">';
                stringx += '<div class="btn btn-light m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+spin+' processCountBadge" data-title="Coaching Log '+checklistStat+'" style="width: 25px !important;height: 25px !important;"><i class="fa '+cklistStatIcon+'" style="font-size: 17 px;"></i></div><span class="button-content text-light text-capitalize pl-2" style="font-size: 0.9rem;">Coaching Log</span>';
            stringx += '</div>';
            // stringx += '<span class="m-badge m-badge--brand m-badge--wide float-right" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Contains assigned tasks" style="margin-top: -1rem;background-color: #673AB7;">Coaching Log</span>';
          } else {
            stringx += '<div class="btn float-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;margin-top: -1rem;background-color: #4caf50;border-top-right-radius: 0;">';
                stringx += '<div class="btn btn-light m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+spin+' processCountBadge" data-title="Checklist '+checklistStat+'" style="width: 25px !important;height: 25px !important;"><i class="fa '+cklistStatIcon+'" style="font-size: 17 px;"></i></div><span class="button-content text-light text-capitalize pl-2" style="font-size: 0.9rem;">Checklist</span>';
            stringx += '</div>';
            // stringx += '<span class="m-badge m-badge--brand m-badge--wide float-right" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Contains assigned tasks" style="margin-top: -1rem;background-color: #4caf50;;">Checklist</span>';
          }
          stringx += "</div>";
        })

        //================ Old Code here
        // $.each(res.checklist_details, function(key, item) {
        //   var checklist_name = item.checklist.checklistTitle;
        //   var substringname = "";
        //   if (checklist_name.length > 22) {
        //     substringname = checklist_name.substring(0, 22) + "...";
        //   } else {
        //     substringname = checklist_name.substring(0, 22);
        //   }
        //   let cklistLayout = 'checklistcolor';
        //   let chklistIcon = 'coloricons'
        //   if (item.checklist.processType_ID == 2) {
        //     cklistLayout = 'coachingLogcolor';
        //     chklistIcon = 'coachingLogicons'
        //   }
        //   stringx += "<div class='col-lg-4 mb-4'>";
        //   stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm portletLink mb-0' data-portlet='true' id='m_portlet_tools_7' data-linkpage='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "'>";
        //   stringx += "<div class='m-portlet__head border-accent rounded " + cklistLayout + "'>";
        //   stringx += "<div class='row' style='margin-top:1em'>";
        //   stringx += "<div class='m-portlet__head-caption'>";
        //   stringx += "<div class='m-portlet__head-title'>";

        //   // Check or clock icon
        //   if (item.checklist.status_ID == 2) {
        //     stringx += "<span class='m-portlet__head-icon p-0 fa-spin mr-2'>";
        //     stringx += "<i class='fa fa-spinner " + chklistIcon + "' id=''></i>";
        //     stringx += "</span>&nbsp;&nbsp;";
        //   } else if (item.checklist.status_ID == 3) {
        //     stringx += "<span class='m-portlet__head-icon'>";
        //     stringx += "<i class='fa fa-check " + chklistIcon + "' id=''></i>";
        //     stringx += "</span>";

        //   }

        //   if (item.checklist.processType_ID == 2) {
        //     stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-finalchcklisttitle=\"" + checklist_name + "\" data-original-title='" + item.checklist.checklistTitle + "' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/coaching_log') ?>" + '/' + item.checklist.checklist_ID + "' class='foldname checklistTitleInfo'>" + substringname + "</a>";
        //   } else {
        //     stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-finalchcklisttitle=\"" + checklist_name + "\" data-original-title='" + item.checklist.checklistTitle + "' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "' class='foldname checklistTitleInfo'>" + substringname + "</a>";
        //   }
        //   stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
        //   stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
        //   stringx += "</div>";
        //   stringx += "</div>";
        //   stringx += "</div>";

        //   stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";
        //   stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
        //   stringx += " <div class='col-md-12 p-0'>";
        //   stringx += "<div class='progress'>"

        //   // Start Status Bar
        //   // stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
        //   // stringx += "</div>"
        //   // End

        //   if (item.statuses.all == item.statuses.completed) {
        //     stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
        //     stringx += "</div>"
        //   } else if (item.statuses.completed == 0) {
        //     stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
        //     stringx += "</div>"
        //   } else {
        //     var percentage_pending = (item.statuses.completed / item.statuses.all) * 100;
        //     stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-toggle='m-tooltip' data-placement='top' data-original-title='" + item.statuses.completed + "/" + item.statuses.all + "'></div>"
        //     stringx += "</div>"
        //   }

        //   stringx += "</div>";
        //   stringx += "</div>";

        //   stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
        //   stringx += "<div class='m-portlet__head-tools'>";
        //   stringx += "<ul class='m-portlet__nav'>";

        //   // if

        //   if (item.checklist.assignmentRule_ID == "owned" || item.checklist.assignmentRule_ID == "admin" || item.checklist.assignmentRule_ID == 1 || item.checklist.assignmentRule_ID == 2 || item.checklist.assignmentRule_ID == 6) {

        //     stringx += "<li class='m-portlet__nav-item p-0' data-toggle='m-tooltip' data-original-title='Change, Assign, and Remove'>";
        //     stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        //     stringx += "<i class='la la-cog bottom-icons'></i></a>";
        //     stringx += "<div class='dropdown-menu dropdownstyle p-0' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
        //     stringx += "<button style='padding:0.6rem 1.3rem' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='updateChecklistFetch(this)' data-toggle='modal' data-target='#updateChecklistmodal'>";
        //     stringx += "<i class='flaticon-file'></i>&nbsp;Change Checklist name</button>";

        //     // stringx += "<button style='padding:0.6rem 1.3rem' class='dropdown-item' type='button' data-process='"+item.checklist.process_ID+"' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_IDforAssignment(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
        //     stringx += "<button style='padding:0.6rem 1.3rem' class='dropdown-item' type='button' data-process='" + item.checklist.process_ID + "' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_proIDforAssignment(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
        //     stringx += "<i class='la la-users'></i>";
        //     stringx += "&nbsp;Assign this checklist</button>";

        //     stringx += "<button style='padding:0.6rem 1.3rem' data-type='Remove' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='remove_checklist(this)'>";
        //     stringx += "<i class='la la-trash'></i>&nbsp;Delete this Checklist</button>";


        //     stringx += "<button style='padding:0.6rem 1.3rem' data-type='Duplicate' data-checklistname='" + checklist_name + "' data-checklistid='" + item.checklist.checklist_ID + "' data-toggle='modal' data-target='#run_checklist-modal' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='defaultchecklist_name()'>";
        //     stringx += "<i class='fa fa-copy'></i>&nbsp;Duplicate this Checklist (assignees)</button>";

        //     stringx += "</div>";
        //     stringx += "</li>";

        //     stringx += "<li class='m-portlet__nav-item p-0' data-toggle='m-tooltip' data-original-title='Archive'>";
        //     stringx += "<a href='#' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_toarchive(this)' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-archive bottom-icons'></i></a> ";
        //     stringx += "</li>";

        //     // stringx += "<li class='m-portlet__nav-item'>";
        //     // stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        //     // stringx += "<i class='la la-cog bottom-icons'></i></a>";
        //     // stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";

        //     // // Change checklist name
        //     // stringx += "<button class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='updateChecklistFetch(this)' data-toggle='modal' data-target='#updateChecklistmodal'>";
        //     // stringx += "<i class='flaticon-file'></i>&nbsp;Change Checklist name</button>";

        //     // // Assign
        //     // stringx += "<button class='dropdown-item' type='button' data-title='" + item.checklist.checklistTitle + "' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
        //     // stringx += "<i class='la la-users'></i>";
        //     // stringx += "&nbsp;Assign this checklist</button>";

        //     // // Delete
        //     // stringx += "<button data-type='checklist' class='dropdown-item' data-type='checklist' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='delete_Folprocheck(this)'>";
        //     // stringx += "<i class='la la-trash'></i>&nbsp;Delete this Checklist</button>";

        //     // // Duplicate 
        //     // stringx += "<button style='padding:0.6rem 1.3rem' data-type='Duplicate' data-checklistname='" + checklist_name + "' data-checklistid='" + item.checklist.checklist_ID + "' data-toggle='modal' data-target='#run_checklist-modal' class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='defaultchecklist_name()'>";
        //     // stringx += "<i class='fa fa-copy'></i>&nbsp;Duplicate this Checklist (assignees)</button>";

        //     // stringx += "</div>";
        //     // stringx += "</li>";

        //     // //ARCHIVE ICON
        //     // stringx += "<li class='m-portlet__nav-item'>";
        //     // stringx += "<a href='#' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_toarchive(this)' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-archive bottom-icons'></i></a> ";
        //     // stringx += "</li>";
        //     // //end ARCHIVE ICON
        //   }
        //   // end if
        //   var createdbynames = item.statuses.names.fname + " " + item.statuses.names.lname;
        //   stringx += "<li class='m-portlet__nav-item p-0 dateCreatedDetails' data-datecreated='" + item.checklist.dateTimeCreated + "' data-cname='" + createdbynames + "'>";
        //   stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
        //   stringx += "<i class='la la-info-circle bottom-icons'></i>";
        //   stringx += "</a>";
        //   stringx += "</li>";

        //   stringx += "<li class='m-portlet__nav-item p-0'>";
        //   stringx += "<span class='m-menu__link-badge'>";

        //   //options
        //   // stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'> no date yet</span>";
        //   // stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";

        //   // options

        //   stringx += "</span>";
        //   stringx += "</li>";

        //   // if
        //   if (item.checklist.task_permission == "yes") {
        //     stringx += "<li class='m-portlet__nav-item p-0'>";
        //     stringx += "<span class='m-menu__link-badge'>";
        //     stringx += "<span class='m-badge m-badge--brand m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Contains assigned tasks'>Assigned Tasks</span>";
        //     stringx += "</span>";
        //     stringx += "</li>";
        //   }
        //   // if

        //   stringx += "</ul>";
        //   stringx += "</div>";
        //   stringx += "</div>";

        //   stringx += "</div>";
        //   stringx += "</div>";
        //   if (item.checklist.processType_ID == 2) {
        //     stringx += '<span class="m-badge m-badge--brand m-badge--wide float-right" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Contains assigned tasks" style="margin-top: -1rem;background-color: #673AB7;">Coaching Log</span>';
        //   } else {
        //     stringx += '<span class="m-badge m-badge--brand m-badge--wide float-right" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Contains assigned tasks" style="margin-top: -1rem;background-color: #4caf50;;">Checklist</span>';
        //   }
        //   stringx += "</div>";
        // })

        $("#checkrow").html(stringx);
        if ($("#checkrow").html() == "") {
          $("#checkrow").html('<div class="col-12"><div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline text-center"><div class="m-alert__text"><strong>No Checklist Available</strong><br>Run a checklist and it will show up here</div></div></div>');
        }
        var count = $("#checkrow-total").data('count');
        var result = parseInt(limiter) + parseInt(perPage);
        if (count === 0) {
          $("#checkrow-count").html(0 + ' - ' + 0);
        } else if (count <= result) {
          $("#checkrow-count").html((limiter + 1) + ' - ' + count);
        } else if (limiter === 0) {
          $("#checkrow-count").html(1 + ' - ' + perPage);
        } else {
          $("#checkrow-count").html((limiter + 1) + ' - ' + result);
        }

        mApp.unblock('#content');
        callback(res.total);
      },
      error: function() {
        swal("Error", "Please contact admin", "error");
        mApp.unblock('#content');
      }
    });
  }
  $("#checkrow-perpage").change(function() {
    var perPage = $("#checkrow-perpage").val();
    checklists_display(0, perPage, function(total) {
      $("#checkrow-pagination").pagination('destroy');
      $("#checkrow-pagination").pagination({
        items: total, //default
        itemsOnPage: $("#checkrow-perpage").val(),
        hrefTextPrefix: "#",
        cssStyle: 'light-theme',
        displayedPages: 10,
        onPageClick: function(pagenumber) {
          var perPage = $("#checkrow-perpage").val();
          checklists_display((pagenumber * perPage) - perPage, perPage, function() {});
        }
      });
    });
  });

  function fetch_proIDforAssignment(element) {
    var processid = $(element).data("process");
    var pt = $("#process_type").attr('data-protype');
    var cid = $(element).data("id");
    $("#process_d").val(processid);
    $("#folprotaskcheck_id").val(cid);
    if (pt == 2) {
      $("#addmem_tab").remove();
    }
  }

  // Additional 
  function display_accountsforcl() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_account",
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx = "<option disabled selected=''>Select a Team</option><option value='0'>All Employees</option>";
        $.each(result, function(key, item) {
          stringx += "<option value=" + item.acc_id + ">" + item.acc_name + "</option>";
        });
        $("#team_names").html(stringx);
      }
    });
  }

  function runChecklist_forcl() {
    var process_ID = <?php echo $processID; ?>;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_checklistdetails_cl",
      data: {
        checklist_remarks: $("#addchecklistName_e").val(),
        assignees: $("#names_assignee_cl").val().toString(),
        process_ID: process_ID
      },
      cache: false,
      success: function(res) {
        swal({
          position: 'center',
          type: 'success',
          title: 'Checklist On the run!',
          showConfirmButton: false,
          timer: 1500
        });
        $("#checkrow-perpage").change();
        $("#run_checklist-modal-coaching").modal("hide");
      }
    });
  }

  $("#team_names").change(function() {
    // $("#names_assignee_cl").selectpicker("destroy");
    // $("#names_assignee_cl").trigger('change');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_membersPerAccount",
      data: {
        acc_id: $("#team_names").val()
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx = "";
        $.each(result.members, function(key, item) {
          stringx += "<option value=" + item.uid + ">" + item.fname + " " + item.lname + "</option>";
        });
        $("#names_assignee_cl").html(stringx);
        select_mul();
        $('#names_assignee_cl').selectpicker('refresh');
      }
    });
  })

  function select_mul() {
    $('#names_assignee_cl').selectpicker({
      title: "Select Names",
      liveSearch: true,
      liveSearchStyle: 'contains',
      selectedTextFormat: "count",
      countSelectedText: '{0} Subordinates',
      // container: ".m-content",
      // dropupAuto: 'true',
      // virtualScroll: 'true',
      size: 'auto'
    });

  }
  $(".add_empnamez").click(function() {
    show_lists_toassign();
  });
  $("#ass_empnames").change(function() {
    getaccmembers('single');
  });

  function show_lists_toassign() {
    var c = "checklist";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_empsnotassigned",
      data: {
        assigned_pct: $("#folprotaskcheck_id").val(),
        ass_type: c
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        console.log(result);
        var stringx = "<option disabled selected=''>Select a Name</option>";
        $.each(result.members, function(key, item) {
          stringx += "<option value=" + item.uid + ">" + item.lname + ", " + item.fname + "</option>";
        });
        $("#ass_empnames").html(stringx);
      }
    });
  }
  //End Additional
  // $('#checkrow').on('click', '.portletLink', function() {
  //   window.location.href = $(this).data('linkpage') ;
  // })
  //END REVISED CODES
</script>