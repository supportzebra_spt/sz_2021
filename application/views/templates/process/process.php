<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css" />
<style type="text/css">
  .tabtopcolor {
    background-color: #3d3e4d !important;
  }

  .icon-color {
    color: #f1c483 !important;
  }

  .dropdownstyle {
    position: absolute;
    transform: translate3d(0px, 37px, 0px);
    top: 0px;
    left: 0px;
    will-change: transform;
  }

  .foldercolor {
    background-color: #4d5167;
    /*#585858;*/
    border-width: 15px
  }

  .scrollstyle {
    overflow: hidden;
    height: 200px;
  }

  .notestyle {
    font-size: 0.9em;
    color: #4aab69;
  }

  .insidefolder {
    background: #e2ebec;
    margin-top: 3em;
  }

  .colorblue {
    /* color: #3297e4 !important; */
    color: #607D8B !important;
  }

  .colorlightblue {
    /* background-color: #9cdfda; */
    background-color: #79d6e270;
    border-bottom: 4px solid #898f92 !important;
  }

  .colorwhitesize {
    color: white !important;
    font-size: 0.9em;
  }

  .colorwhiteicons {
    color: white;
  }

  .coloricons {
    color: #009688 !important;
  }

  .coachingLogicons {
    color: #2196F3 !important;
  }

  .coloricons-checklist {
    color: #3c803f !important;
  }

  .modallabel {
    padding: 20px;
    font-size: 1.3em;
  }

  .dropdownwidth {
    width: 100% !important;
  }

  .topdistance {
    margin-top: 50px;
  }

  .load-style {
    padding: 20px;
  }

  div.container-forms {
    background: #f3f0f0 !important;
    padding: 10px !important;
  }

  .checklistcolor {
    background: #4cc34a54;
    border-right: 7px solid #4CAF50;
  }

  .coachingLogcolor {
    background: #3f51b54a;
    border-right: 7px solid #673AB7;
  }

  .bottom-icons {
    color: #3c803f !important;
  }

  .pro-name {
    color: #416d90;
    font-size: 0.9em;
    font-weight: bold;
  }

  #bootstrap-duallistbox-nonselected-list_from option,
  #bootstrap-duallistbox-selected-list_from option {
    font-size: 13px !important;
  }

  .m-portlet.m-portlet--collapsed.m-portlet--head-sm:hover {
    box-shadow: rgb(136, 135, 135) 2px 3px 10px 1px !important;
    cursor: pointer;
  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="#" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <!-- <li class="m-nav__separator">
            <</li> <li class="m-nav__item">
              <a href="<?php echo base_url("process"); ?>" class="m-nav__link">
                <span class="m-nav__link-text">Folder</span>
              </a>
          </li> -->
          <!-- <li class="m-nav__separator">
            <</li> <li class="m-nav__item">
              <a href="#" class="m-nav__link">
                <span class="m-nav__link-text">Process</span>
              </a>
          </li> -->
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content" id="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--tabs">
          <div class="m-portlet__head tabtopcolor">
            <div class="m-portlet__head-tools">
              <input type="hidden" id="user_id" value="<?php echo $user; ?>">
              <input type="hidden" id="display_id">
              <input type="hidden" id="displayprocess_id">
              <input type="hidden" id="displaychecklist_id">
              <?php foreach ($user_info as $ui) { ?>
                <input type="hidden" class="form-control" id="addchecklist_defaultName" value="<?php echo $ui->fname;
                                                                                                  echo "&nbsp;";
                                                                                                  echo $ui->lname;
                                                                                                  echo "'s";
                                                                                                  echo "&nbsp;";
                                                                                                  echo date("h:i a");
                                                                                                  echo "&nbsp;";
                                                                                                  echo "checklist" ?>">
              <?php } ?>
              <ul class="nav nav-tabs m-tabs-line m-tabs-line--accent m-tabs-line--2x" role="tablist">
                <!-- <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active folderTrigger" data-toggle="tab" href="#foldertab" onclick="folderpermission_access()" role="tab">
                    <i class="fa fa-folder"></i>Folders</a>
                </li> -->
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active processTrigger" id="processTab" data-toggle="tab" href="#processtab" role="tab">
                    <i class="fa fa-file-text-o"></i>Process Templates</a>
                </li>
                <li class="nav-item m-tabs__item">
                  <!-- <a class="nav-link m-tabs__link checklistTrigger" data-toggle="tab" href="#checklisttab" onclick="checklistpermission_access()" role="tab"> -->
                  <a class="nav-link m-tabs__link checklistTrigger" id="checklistTab" data-toggle="tab" href="#checklisttab" role="tab">
                    <i class="fa fa-check-square-o"></i>Checklist</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="m-portlet__body">
            <div class="tab-content">

              <div>
              </div>
              <div class="tab-pane fade show active col-md-12" id="processtab" role="tabpanel">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-9 order-2 order-xl-1 container-forms">
                      <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-5 mb-2 mb-md-0">
                          <div class="m-input-icon m-input-icon--left">
                            <input id="searchProcess" type="text" class="form-control m-input  m-input--square" placeholder="Search by process name...">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                              <span><i class="la la-search"></i></span>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="m-input-icon">
                            <select id="selectedprocessStatus" class="form-control m-input m-input--square">
                              <option value="">All</option>
                              <option value="10">Active</option>
                              <option value="2">Pending</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <select id="proc_type_select" class="form-control m-input m-input--square"></select>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 order-1 order-xl-2 m--align-right">
                      <button class="btn btn-accent m-btn m-btn--custom m-btn--icon mb-3 n_blanktemplate" type="button" data-toggle="modal" data-target="#blank-modal" id="add_newtbutton"><span><i class="fa fa-plus"></i><span>Add Template</span></span></button>

                      <!--          <button class="btn btn-info"><i class="fa fa-archive"></i>&nbsp;Archived</button> -->


                      <!-- <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp; -->
                      <!-- New</button> -->


                      <!-- <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu3" x-placement="bottom-start"> -->
                      <!-- <button class="dropdown-item n_blanktemplate" type="button" data-toggle="modal" data-target="#blank-modal" id="blankTemplate3"><i class="flaticon-file"></i>&nbsp;New Blank Template</button> -->
                      <!-- <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button>-->
                      <!-- <hr>
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Folder</button> -->
                      <!-- </div> -->
                      <!-- <div class="m-separator m-separator--dashed d-xl-none"></div> -->


                    </div>
                  </div>
                </div>


                <!--end: Search Form -->
                <!-- <div class="m-loader m-loader--info loading-proicon load-style"></div> -->
                <div id="prorow" class="row">
                </div>
                <div class="row no-gutters">
                  <div class="col-md-8 col-12">
                    <ul id="prorow-pagination">
                    </ul>
                  </div>
                  <div class="col-md-1 text-right col-4">
                    <select class="form-control m-input form-control-sm m-input--air" id="prorow-perpage">
                      <option>6</option>
                      <option>12</option>
                      <option>24</option>
                      <option>60</option>
                      <option>120</option>
                    </select>
                  </div>
                  <div class="col-md-3 text-right col-8">
                    <p style="font-family:Poppins;font-size:13px;text-align:right;margin-top:3px">Showing <span id="prorow-count"></span> of <span id="prorow-total"></span> records</p>
                  </div>
                </div>
                <div class="text-center">
                  <!-- <button class="btn btn-metal" id="processLoadMore" data-whichfunction='0'>Load More</button> -->
                </div>
              </div>
              <!--    CHECKLIST DISPLAY -->
              <div class="tab-pane fade col-md-12" id="checklisttab" role="tabpanel">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                  <div class="row align-items-center">
                    <!-- SEARCH SECTION -->
                    <div class="col-xl-10 order-2 order-xl-1 container-forms">
                      <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-5 mb-2 mb-md-0">
                          <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input m-input--square" id="searchChecklist" placeholder="Search by checklist title...">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                              <span><i class="la la-search"></i></span>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="m-input-icon">
                            <select id="selectedchecklistStatus" class="form-control m-input m-input--square">
                              <option value="">All</option>
                              <option value="3">Completed</option>
                              <option value="16">Overdue</option>
                              <option value="2">Pending</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <select id="procheck_type_select" class="form-control m-input m-input--square"></select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- TAB CONTENT -->
                <!-- <div class="m-loader m-loader--info loading-icon-check load-style"></div> -->
                <div id="checkrow" class="row">
                </div>
                <div class="row no-gutters">
                  <div class="col-md-8 col-12">
                    <ul id="checkrow-pagination">
                    </ul>
                  </div>
                  <div class="col-md-1 text-right col-4">
                    <select class="form-control m-input form-control-sm m-input--air" id="checkrow-perpage">
                      <option>6</option>
                      <option>12</option>
                      <option>24</option>
                      <option>60</option>
                      <option>120</option>
                    </select>
                  </div>
                  <div class="col-md-3 text-right col-8">
                    <p style="font-family:Poppins;font-size:13px;text-align:right;margin-top:3px">Showing <span id="checkrow-count"></span> of <span id="checkrow-total"></span> records</p>
                  </div>
                </div>
                <!-- <div class="text-center">
                  <button class="btn btn-metal" id="checklistLoadMore" data-whichfunction='0'>Load More</button>
                </div> -->
              </div>

              <!-- <div class="tab-pane fade col-md-12" id="checklisttab" role="tabpanel">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-6 order-2 order-xl-1">
                      <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-8">
                          <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                              <span><i class="la la-search"></i></span>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="m-input-icon">
                            <select class="form-control m-input m-input--square" id="">
                              <option>All</option>
                              <option>Active</option>
                              <option>Inactive</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-6 order-1 order-xl-2 m--align-right">
                      <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display:none"> <i class="fa fa-plus"></i>&nbsp;
                        New
                      </button>
                      <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu2" x-placement="bottom-start">
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                     </div>
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
      <!--begin::Modal-->
      <div class="modal fade" id="folder-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
          <form id="folder_addform" name="folder_addform" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Folder</h5>
                <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label colorblue">Folder Name:</label>
                  <input type="text" class="form-control errorprompt" id="folderName" name="folderName" placeholder="Enter Folder Name">
                  <span style="font-size:0.9em;color:gray" id="foldercharNum"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_foldernameupdate" name="form_foldernameupdate" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Folder Name</h5>
                <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Folder Name:</label>
                  <input type="text" class="form-control" id="updatefolderName" name="updatefolderName" placeholder="Enter Folder Name">
                  <span style="font-size:0.9em;color:gray" id="updatefoldercharNum"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent">Save Changes</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="assign_membersmodal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Add Members to <span style="color:#a8e9f1" id="fpc_title"></span>&nbsp;<span id='type-assigned' style="color:#83d686"></span></h5>
              <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <input type="hidden" id="folprotaskcheck_id" name="">
              <input type="hidden" id="assign_type">
              <input type="hidden" id="process_d">
              <input type="hidden" id="protype_id">
            </div>
            <div class="modal-body">
              <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active show added_empnamez" onclick="reinitAssigneeDatatable()" data-toggle="tab" href="#tab-member" role="tab" aria-selected="true"><i class="la la-users"></i>Members</a>
                </li>
                <li id="addmembers_tab" class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link add_empnamez" data-toggle="tab" href="#tab-addmember" role="tab" aria-selected="false"><i class="la la-users"></i>Add Members</a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-member">
                  <div class="row">
                    <label class="modallabel colorblue">Members</label>
                    <div class="col-12">
                      <div class="form-group m-form__group" style="margin-bottom: 8px;">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Search Employee Name" id="assigneeSearch">
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <i class="fa fa-search"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-12" id="assigned_memberdisplay">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="tab-addmember">
                  <div class="row topdistance">
                    <div class="col-xl-6">
                      <form id="" class="" name="formupdate" method="post">
                        <label for="recipient-name" class="form-control-label colorblue">Select Members by Team</label>
                        <select id="account" class="selectpicker dropdownwidth" data-live-search="true" onchange="getMembersByAccount()">
                          <optgroup data-max-options="1">
                            <option disabled selected="">Click to select</option>
                            <?php foreach ($account as $acc) { ?>
                              <option value='<?php echo $acc->acc_id ?>'><?php echo $acc->acc_name; ?></option>
                            <?php } ?>
                          </optgroup>
                        </select>
                      </form>
                      <br>
                    </div>
                    <div class="col-xl-6">
                      <label for="recipient-name" class="form-control-label colorblue">Select Names</label>
                      <select id="ass_empnames" class="m-select dropdownwidth ass_getempnames" data-live-search="true">
                      </select>
                    </div>
                    <div class="col-12">
                      <div class="m-form__group form-group">
                        <select name="from" id="members" size="20" multiple="multiple" onchange="getaccmembers('multi')">
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class='row'>
                    <label class="modallabel colorblue">Search results</label>
                    <div class="col-12">
                      <table class="table m-table table-hover" id="memberdetails">
                        <thead align="center">
                          <tr>
                            <th>Name</th>
                            <th>Team</th>
                            <th>Access</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody id="myTable" align="center">
                        </tbody>
                      </table>
                      <!-- <div class="m-loader m-loader--info spinner-icon load-style" style="display: none;" ></div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="updateProcessmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_processnameupdate" name="form_processnameupdate" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Process Name</h5>
                <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Process Name:</label>
                  <input type="text" class="form-control" id="updateprocessName" name="updateprocessName" placeholder="Enter Process Name">
                  <span style="font-size:0.9em;color:gray" id="updateprocesscharNum"></span>
                </div>

              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent">Save Changes</button>
              </div>
            </div>
          </form>

        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="blank-modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <form id="form_addblanktemplate" name="form_addblanktemplate" method="post">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Process Template</h5>
                <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class='row'>
                  <div class='col-xl-6'>
                    <div class="form-group">
                      <label for="recipient-name" class="form-control-label colorblue">Template Name:</label>
                      <input type="text" class="form-control" id="processTitle" name="processTitle" style='background:#f0f0f0 !important'>
                      <span style="font-size:0.9em;color:gray" id="charNum"></span>
                    </div>
                  </div>
                  <div class='col-xl-6'>
                    <div class="form-group">
                      <label class="colorblue">Choose Template Type:</label>
                      <select class="form-control m-select2" name="templatetype_select" id="templatetype_select">
                      </select>
                      <!-- <p class="m-form__help text-center notestyle" id="leave_description"><i><strong>Note:</strong> You are going to customize your own template.</i></p> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!--end::Modal-->


        <!-- Duplicate template Modal  -->
        
            <!--begin::Modal-->
            <div class="modal fade" id="tempduplicate_modal" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true" data-backdrop="static" data-proid="" data-protype="">
                <div class="modal-dialog modal-m" role="document">
                    <div class="modal-content">
                        <form id="form_addduptemplate" name="form_addduptemplate" method="post">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Process Template Duplicate</h5>
                                <button onclick="resetform()" type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class='row'>
                                    <div class='col-xl-12'>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label colorblue">Template
                                                Name:</label><span style="font-size:smaller;color:green"> *You can
                                                change the template name below.</span>
                                            <input type="text" class="form-control" id="processDupTitle"
                                                name="processDupTitle" style='background:#f0f0f0 !important'>
                                            <span style="font-size:0.9em;color:gray" id="dup_charNum"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-accent">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end::Modal-->



      <!--begin::Modal-->
      <div class="modal fade" id="blankroot-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_addblanktemplate_specific" name="form_addblanktemplate_specific" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Blank Template for <span style="color:#a8e9f1" id="fdrname"></span></h5>
                <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Template Name:</label>
                  <input type="text" class="form-control" name="process_folderTitle" id="process_folderTitle">
                  <span style="font-size:0.9em;color:gray" id="specific_charNum"></span>
                  <input type="hidden" id="rootfolderID" name="rootfolderID">
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent">Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="updateChecklistmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_checklistnameupdate" name="form_checklistnameupdate" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Checklist Name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Checklist Name:</label>
                  <input type="text" class="form-control" id="updatechecklistName" name="updatechecklistName" placeholder="Enter Process Name">
                  <span style="font-size:0.9em;color:gray" id="updatechecklistcharNum"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent">Save Changes</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="run_checklist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_runanotherchecklist" name="form_runanotherchecklist" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Let's run another checklist!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <input type="hidden" id="processtemp_id">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <h5 class="processtemplate_title" style="color:#34bfa3"></h5>
                  <br>
                  <label for="recipient-name" class="form-control-label">Name of your checklist (For example, the name of the employee you’re onboarding) :</label>
                  <input type="text" class="form-control" id="addchecklistName" name="addchecklistName" placeholder="Enter Checklist Name">
                  <span style="font-size:0.9em;color:gray" id="checkcharNum"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="run_checklist-modal-coaching" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_runchecklist-coaching" name="form_runchecklist-coaching" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Let's run checklists!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <h5 class="processtemplate_title" style="color:#34bfa3"></h5>
                <br>
                <div class="row">
                  <div class="col-xl-6">
                    <div class="form-group">
                      <label>Select a Team</label>
                      <select class="form-control m-select2" id='team_names'>
                      </select>
                    </div>
                  </div>
                  <div class="col-xl-6">
                    <div class="form-group">
                      <label>Names from selected team</label>
                      <select class="form-control m-bootstrap-select" id="names_assignee_cl" name="names_assignee_cl" data-live-search='true' data-actions-box='true' multiple>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <br>
                  <label for="recipient-name" class="form-control-label"> Notes: </label>
                  <input type="text" class="form-control" id="addchecklistName_e" name="addchecklistName_e" placeholder="Enter Checklist Name">
                  <span style="font-size:0.9em;color:gray" id="charNum"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

    </div>
  </div>
</div>

<link href="<?php echo base_url(); ?>assets/src/custom/css/simplePagination.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.simplePagination.js"></script>
<link href="<?php echo base_url(); ?>assets/src/custom/css/bootstrap-duallistbox.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/src/custom/js/jquery.bootstrap-duallistbox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/process/process_checklist_pending.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/process/processtemp_duplicate.js"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script> -->
<script type="text/javascript">
  var folder_ids = [];
  var process_ids = [];
  var checklist_ids = [];
  $("#selectedprocessStatus").change(function() {
    $("#prorow-perpage").change();
  });
  $("#selectedchecklistStatus").change(function() {
    $("#checkrow-perpage").change();
  });

  function resetFolder() {
    $("#folderLoadMore").show();
    $("#folderLoadMore").data('whichfunction', 0);
    $("#folrow").html("");
    folder_ids = [];
  }

  function resetProcess() {
    $("#processLoadMore").show();
    $("#processLoadMore").data('whichfunction', 0);
    $("#prorow").html("");
    process_ids = [];
  }

  function resetChecklist() {
    $("#checkrow").html("");
    $("#checklistLoadMore").data('whichfunction', 0);
    $("#checklistLoadMore").show();
    checklist_ids = [];
  }
  const display_perPage = 6;
  $('[href="#foldertab"]').click(function() {
    resetFolder();
  });
  $('[href="#processtab"]').click(function() {
    resetProcess();
  });
  $('[href="#checklisttab"]').click(function() {
    resetChecklist();
  });

  $(function() {
    // var pathName = window.location.pathname;
    // var pathArray = pathName.split('/');
    // console.log(pathArray);
    // console.log(pathArray[3]);
    // if (pathArray[3] != undefined){
    //   console.log('with specific tab link');
    // }eles
    $('#templatetype_select').select2({
      placeholder: "Select Template Type",
      width: '100%'
    });
    $('#team_names').select2({
      placeholder: "Select Account",
      width: '100%'
    });
    $('#ass_empnames').select2({
      placeholder: "Select Names",
      width: '100%'
    });
    $('#names_assignee_cl').selectpicker({
      placeholder: "Select Names",
      width: '100%',
      allowClear: true
    });
    // $('#proc_type_select').select2({
    //   placeholder: "Template Type",
    //   width: '100%'
    // });
    // $('#procheck_type_select').select2({
    //   placeholder: "Template Type",
    //   width: '100%'
    // });

    // =========== =========== INITIALIZE =========== =========== ===========
    $('#prorow-perpage').change();
    $('[href="#processtab"]').click();
    // =========== =========== INITIALIZE =========== =========== ===========
    var dualListBox_members = $('#members').bootstrapDualListbox({
      nonSelectedListLabel: '<span class="m--font-bolder m--font-danger">Non-selected:</span>',
      selectedListLabel: '<span class="m--font-bolder m--font-success">Selected Employee:</span>',
      selectorMinimalHeight: 150
    });

    var dualListContainer = dualListBox_members.bootstrapDualListbox('getContainer');
    dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
    dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
    dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
    dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');

    $('#folder_select').select2({
      placeholder: "Select a folder",
      width: '100%'
    });
    // folderpermission_access();


    $("#folderLoadMore").click(function() {
      displayfolders();
    });
    $("#processLoadMore").click(function() {
      displayprocesstemp();
    });
    $("#checklistLoadMore").click(function() {
      displaychecklists();
    });

    $('#member').select2({
      placeholder: "Select your schedule.",
      width: '100%'
    });

    //MODAL DISMISS AND RESET
    $('#blank-modal').on('hidden.bs.modal', function() {
      formReset('#form_addblanktemplate');
      $('#charNum').text("");
      $('#processTitle').val("");
    });

    $('#blankroot-modal').on('hidden.bs.modal', function() {
      formReset('#form_addblanktemplate_specific');
      $('#specific_charNum').text("");
      $('#process_folderTitle').val("");
    });

    $('#folder-modal').on('hidden.bs.modal', function() {
      formReset('#folder_addform');
      $('#foldercharNum').text("");
      $('#folderName').val("");
    });

    $('#run_checklist-modal').on('hidden.bs.modal', function() {
      formReset('#form_runanotherchecklist');
      $('#checkcharNum').text("");
      $('#addchecklistName').val("");
    });

    $('#updatemodal').on('hidden.bs.modal', function() {
      formReset('#form_foldernameupdate');
      $('#updatefoldercharNum').text("");
      $('#updatefolderName').val("");
    });

    $('#updateProcessmodal').on('hidden.bs.modal', function() {
      formReset('#form_processnameupdate');
      $('#updateprocesscharNum').text("");
      $('#updateprocessName').val("");
    });

    $('#updateChecklistmodal').on('hidden.bs.modal', function() {
      formReset('#form_checklistnameupdate');
      $('#updatechecklistcharNum').text("");
      $('#updatechecklistName').val("");
    });

    $('#run_checklist-modal-coaching').on('hidden.bs.modal', function() {
      formReset('#form_runchecklist-coaching');
      $('#addchecklistName_e').val("");
      $("#names_assignee_cl").selectpicker("destroy");
      $("#names_assignee_cl").empty();
      $('#names_assignee_cl').selectpicker({
        placeholder: "Select Names",
        width: '100%'
      });
    })
    //MODAL DISMISS AND RESET END CODE


    //VALIDATION STARTS  HERE
    $('#folder_addform').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        folderName: {
          validators: {
            stringLength: {
              max: 45,
              message: 'The folder name must be less than 45 characters'
            },
            notEmpty: {
              message: 'Oops! folder name is required!'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#folder_addform').formValidation('disableSubmitButtons', true);
      add_newfolder();
    });

    $('#form_foldernameupdate').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        updatefolderName: {
          validators: {
            stringLength: {
              max: 45,
              message: 'The folder name must be less than 45 characters'
            },
            notEmpty: {
              message: 'Oops! folder name is required!'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_foldernameupdate').formValidation('disableSubmitButtons', true);
      var folderid = $('#form_foldernameupdate').data('id');
      saveChangesFolder(folderid);
    });

    //PROCESS TEMPLATE
    $('#form_addblanktemplate').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        processTitle: {
          validators: {
            stringLength: {
              max: 45,
              message: 'The Process name must be less than 45 characters'
            },
            // regexp: {
            //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
            //   message: 'Letters with number combinations only'
            // },
            notEmpty: {
              message: 'Oops! Process name is required!'
            },
          }
        },
        templatetype_select: {
          validators: {
            notEmpty: {
              message: 'Oops! process type is required!.'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_addblanktemplate').formValidation('disableSubmitButtons', true);
      add_newblanktemplate();
    });

    $('#form_runchecklist-coaching').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        addchecklistName_e: {
          validators: {
            stringLength: {
              max: 80,
              message: 'The remarks must be less than 80 characters'
            },
            notEmpty: {
              message: 'Oops! remarks is required!'
            },
          }
        },
        names_assignee_cl: {
          validators: {
            notEmpty: {
              message: 'Oops! Assignees is/are required!.'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_runchecklist-coaching').formValidation('disableSubmitButtons', true);
      runChecklist_forcl();
    });

    $(function() {
      $('#form_addblanktemplate_specific').formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        // live: 'disabled',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          process_folderTitle: {
            validators: {
              stringLength: {
                max: 45,
                message: 'The Process name must be less than 45 characters'
              },
              // regexp: {
              //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
              //   message: 'Letters with number combinations only'
              // },
              notEmpty: {
                message: 'Oops! Process name is required!'
              },
            }
          },
        }
      }).on('success.form.fv', function(e, data) {
        e.preventDefault();
        $('#form_addblanktemplate_specific').formValidation('disableSubmitButtons', true);
        add_newblanktemplate_specific();
      });
    })


    $('#form_processnameupdate').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        updateprocessName: {
          validators: {
            stringLength: {
              max: 45,
              message: 'The Process name must be less than 45 characters'
            },
            // regexp: {
            //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
            //   message: 'Letters with number combinations only'
            // },
            notEmpty: {
              message: 'Oops! Process name is required!'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_processnameupdate').formValidation('disableSubmitButtons', true);
      var processid = $('#form_processnameupdate').data('id');
      saveChangesProcess(processid);
    });

    //CHECKLIST
    $('#form_runanotherchecklist').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        addchecklistName: {
          validators: {
            stringLength: {
              max: 80,
              message: 'The checklist name must be less than 80 characters'
            },
            notEmpty: {
              message: 'Oops! checklist name is required!'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_runanotherchecklist').formValidation('disableSubmitButtons', true);
      runChecklist();
    });

    $('#form_checklistnameupdate').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
      // live: 'disabled',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        updatechecklistName: {
          validators: {
            stringLength: {
              max: 80,
              message: 'The checklist name must be less than 80 characters'
            },
            notEmpty: {
              message: 'Oops! checklist name is required!'
            },
          }
        },
      }
    }).on('success.form.fv', function(e, data) {
      e.preventDefault();
      $('#form_checklistnameupdate').formValidation('disableSubmitButtons', true);
      var checklistid = $('#form_checklistnameupdate').data('id');
      saveChangesChecklist(checklistid);
    });

  });

  function getfolder_nameid(element) {
    var folid = $(element).data('id');
    var foldername = $(element).data('folname');

    $("#rootfolderID").val(folid);
    $("#fdrname").text(foldername);
  }
  $("#searchString").on("change", function() {
    var str = $(this).val();
    resetFolder();
    displayfolders(str);
  });

  function getaccmembers(access) {
    var membersIds = 0;
    var link = "";
    if (access == "multi") {
      membersIds = $("#members").val();
      link = "fetch_allemployeedetails";
    } else {
      membersIds = $("#ass_empnames").val();
      link = "fetch_oneemployeedetails";
    }
    var assigntype = $("#assign_type").val();
    // var folprotaskcheckID = $("#folprotaskcheck_id").val();
    var accountname = $("#account :selected").text();
    var pro_id = $("#process_d").val();

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/" + link,
      data: {
        membersIds: membersIds
      },
      beforeSend: function() {
        $('.spinner-icon').show();
      },
      cache: false,
      success: function(res) {
        // $('.spinner-icon').remove();
        var result = JSON.parse(res.trim());
        var stringx = "";
        $.each(result.employees, function(key, data) {
          stringx += "<tr>";
          stringx += "<td>" + data.fname + " " + data.lname + "</td>";
          stringx += "<td>" + data.acc_name + "</td>";
          stringx += "<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";

          if (assigntype == 'folder' || assigntype == 'process') {
            stringx += "<option value='8'>Can View All</option>";
            stringx += "<option value='7'>Can View Own</option>";
            stringx += "<option value='4'>Can View Own and Run</option>";
            stringx += "<option value='3'>Can View and Run</option>";
            stringx += "<option value='1'>Can Edit, View and Run</option>";
            stringx += "<option value='2'>Can Edit, View Own and Run</option>";
          } else {
            stringx += "<option value='5'>Can Answer</option>";
            stringx += "<option value='6'>Can Edit and Answer</option>";
          }
          stringx += "</optgroup></select></td>";
          stringx += "<td><button onclick='addmember_assignment(this)' data-reload='1' data-pro='" + pro_id + "' data-toggle='m-tooltip' data-id='" + data.uid + "' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
          stringx += "</tr>";
        });
        $("#memberdetails tbody").html(stringx);
      }
    });
  }

  function fetch_runcheck_details(element) {
    var processid = $(element).data('id');
    var processtitle = $(element).data('processtitle');
    var protype = $(element).data('pt');
    var default_checkname = $("#addchecklist_defaultName").val();
    if (protype == "2") {
      display_accountsforcl();
    }
    $("#processtemp_id").val(processid);
    $(".processtemplate_title").text(processtitle);
    $("#addchecklistName").val(default_checkname);
  }

  function display_membernames(element) {
    var val = $(element).data("type");
    var id = $(element).data("id");
    var processid = $(element).data("process");
    $("#process_d").val(processid);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/get_allemployeesname",
      data: {
        val: val,
        id: id,
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res);
        var stringx = "<option disabled selected=''>Click to select</option>";
        // stringx+="<optgroup data-max-options='1'>";
        $.each(result, function(key, data) {
          stringx += "<option value=" + data.emp_id + ">" + data.lname + " " + data.fname + "</option>";
        });
        // stringx+="</optgroup>";
        $("#member").html(stringx);
        $("#member").trigger("change");
      }
    });
  }

  function getmember_name() {
    var empid = $("#member").val();
    var empname = $("#member :selected").text();
    var assigntype = $("#assign_type").val();
    var folprotaskcheckID = $("#folprotaskcheck_id").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_oneemployee",
      data: {
        emp_id: empid,
        fptcID: folprotaskcheckID,
        assign_type: assigntype
      },
      beforeSend: function() {
        $('.spinner-icon').show();
      },
      cache: false,
      success: function(res) {
        var stringx = "";
        var result = JSON.parse(res.trim());
        $(".spinner-icon").remove();
        if (res != 0) {
          $.each(result, function(key, data) {
            stringx += "<tr>";
            stringx += "<td>" + data.fname + " " + data.lname + "</td>";
            stringx += "<td>" + data.acc_name + "</td>";
            stringx += "<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";

            if (assigntype == "folder" || assigntype == "process") {
              stringx += "<option value='8'>Can View All</option>";
              stringx += "<option value='7'>Can View Own</option>";
              stringx += "<option value='4'>Can View Own and Run</option>";
              stringx += "<option value='3'>Can View and Run</option>";
              stringx += "<option value='1'>Can Edit, View and Run</option>";
              stringx += "<option value='2'>Can Edit, View Own and Run</option>";
            } else {
              stringx += "<option value='5'>Can Answer</option>";
              stringx += "<option value='6'>Can Edit and Answer</option>";
            }
            stringx += "</optgroup></select></td>";
            stringx += "<td><button onclick='addmember_assignment(this)' data-reload='2' data-toggle='m-tooltip' data-id='" + data.apid + "' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
            stringx += "</tr>";
          });
        } else {
          stringx = " ";
        }
        $("#memberdetails tbody").html(stringx);
      }
    });
  }

  function resetform() {
    $("#formadd").trigger("reset");
    $("#formupdate").trigger("reset");
    $("#processadd").trigger("reset");
    $("#process_foladd").trigger("reset");
    $("#account").prop("selectedIndex", 0).change();
    $("#member").prop("selectedIndex", 0).change();
    $("#memberdetails tbody").html("");
    $("#added_members tbody").html("");
  }

  function fetch_IDforAssignment(element) {
    var idforassignment = $(element).data('id');
    var typename = $(element).data('type');
    var name = $(element).data('title');
    var protype = $(element).data('protype');

    $("#folprotaskcheck_id").val(idforassignment);
    $("#assign_type").val(typename);
    $("#protype_id").val(protype);
    $("#fpc_title").text(name);
    $("#type-assigned").text(typename);
    if (protype == 2) {
      $("#addmembers_tab").hide();
    } else {
      $("#addmembers_tab").show();
    }
  }


  $("#proc_type_select").on("change", function() {
    $("#prorow-perpage").change();
  });
  $("#searchProcess").keyup(function() {
    $("#prorow-perpage").change();
  })

  function access_permissionChange(element) {
    var assignmentid = $(element).data('assignid');
    var changepermission = $(element).val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_assignmentpermission",
      data: {
        assid: assignmentid,
        changepermission: changepermission
      },
      cache: false,
      success: function(res) {
        reinitAssigneeDatatable();
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Changed Acces Permission  ',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      }
    });
  }
  //Change Status

  function folderset_toarchive(element) {
    var selected_folid = $(element).data('id');
    var status = '15';

    swal({
      title: 'Are you sure?',
      text: "All Process templates and checklists will be keep to archive section and cannot be used for the meantime.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, set to archive!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/updatefolder_archive",
          data: {
            folder_ID: selected_folid,
            status_ID: status
          },
          cache: false,
          success: function(res) {
            var result = JSON.parse(res.trim());
            swal({
              position: 'center',
              type: 'success',
              title: 'Archived the folder',
              showConfirmButton: false,
              timer: 1500
            });
            $(".folderTrigger").trigger("click");
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Setting folder to archive has been cancelled',
          'error'
        )
      }
    });
  }

  function processset_toarchive(element) {
    var processid = $(element).data('id');
    var status = '15';

    swal({
      title: 'Are you sure?',
      text: "All Checklists under this process template will be unaccessible.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, set to archive!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/updateprocess_archive",
          data: {
            process_ID: processid,
            status_ID: status
          },
          cache: false,
          success: function(res) {
            var result = JSON.parse(res.trim());
            swal({
              position: 'center',
              type: 'success',
              title: 'Archived the Process Template',
              showConfirmButton: false,
              timer: 1500
            });
            $(".processTrigger").trigger("click");
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Setting Process Template to archive has been cancelled',
          'error'
        )
      }
    });
  }

  function checklistset_toarchive(element) {
    var chid = $(element).data('id');
    var isArchived = 1;
    swal({
      title: 'Are you sure?',
      text: "This checklist will be inactive.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, set to archive!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/updatechecklist_archive",
          data: {
            checklist_ID: chid,
            isArchived: isArchived
          },
          cache: false,
          success: function(res) {
            var result = JSON.parse(res.trim());
            $("#checkrow-perpage").change();
            swal({
              position: 'center',
              type: 'success',
              title: 'Archived the checklist',
              showConfirmButton: false,
              timer: 1500
            });
            $(".checklistTrigger").trigger("click");
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Setting checklist to archive has been cancelled',
          'error'
        )
      }
    });
  }
  //JQUERY DISPLAY
  function folderpermission_access() {
    var uid = $("#user_id").val();
    var folder = "folder";
    var displayid = 0;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_access",
      // data: {
      //   uid:uid,
      // },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());

        if (result.length > 0) {
          // $("#blankTemplate2").show();
          // $("#blankTemplate2").css("display","block");

          // 1 if admin
          displayid = 1;
          $("#display_id").val(displayid);
          displayfolders();
        } else {
          //SEARCH IF THERE'S ASSIGNED FOLDER 
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/fetch_assignment",
            data: {
              type: folder
            },
            cache: false,
            success: function(res) {
              var res = JSON.parse(res.trim());

              if (res == 0) {
                //temp remove
                // $("#blankTemplate2").remove();
                // 2 if no assignment
                displayid = 2;
                $("#display_id").val(displayid);
                displayfolders();
              } else {
                //3 if assignment exist
                displayid = 3;
                $("#display_id").val(displayid);
                displayfolders();
              }
            }
          });
        }
      }
    });
  }

  function processpermission_access() {
    var uid = $("#user_id").val();
    var processtemp = "process";
    var displayid = 0;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_access",
      // data: {
      //   uid:uid,
      // },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        //temporary admin
        if (result.length > 0) {
          // $("#blankTemplate3").attr("hidden", false);
          //  $("#blankTemplate3").css("display","block");
          displayid = 1;
          $("#displayprocess_id").val(displayid);
          // changed from displayprocesstem()
          displayprocesstemp();
        } else {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/fetch_assignment",
            data: {
              type: processtemp
            },
            cache: false,
            success: function(res) {
              var res = JSON.parse(res.trim());
              if (res == 0) {
                // $("#blankTemplate3").remove();
                displayid = 2;
                $("#displayprocess_id").val(displayid);
                displayprocesstemp();
              } else {
                displayid = 3;
                $("#displayprocess_id").val(displayid);
                displayprocesstemp();
              }
            }
          });
        }
      }
    });
  }


  //NEW CODES FOR DISPLAY OF RECORDS
  function processtemp_display(limiter, perPage, callback) {
    var templateType = $("#proc_type_select").val();
    var activeSearch = $("#selectedprocessStatus").val();
    var inputSearch = $("#searchProcess").val();
    // fetch_active_processtype(2);xxx
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/processtemplate_display",
      data: {
        limiter: limiter,
        perPage: perPage,
        activeSearch: activeSearch,
        inputSearch: inputSearch,
        templateType: templateType
      },
      beforeSend: function() {
        mApp.block('#content', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'brand',
          size: 'lg'
        });
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        $("#prorow-total").html(res.total);
        $("#prorow-total").data('count', res.total);
        $("#prorow").html("");
        var stringx = "";
        
        $.each(res.process_details, function(key, item) {
         
          var processname = item.processtemp.processTitle;
          var substringprocess = "";
          let link_page_val = '';
          let titleLink = "";
          let toolsDisplay = "";
          let permissionDisplay = '';

          //=================nEW START
          // GET LINK 
          if (item.processtemp.processstatus_ID == 10) {
            link_page_val = "<?php echo site_url('process/checklist') ?>" + "/" + item.processtemp.process_ID;
          } else if (item.processtemp.processstatus_ID == 2 || item.processtemp.processstatus_ID == 3) {
            // if Template has checklists
            link_page_val = "<?php echo site_url('process/checklist_pending') ?>" + "/" + item.processtemp.process_ID;
          }          
          // GET PROCESS NAME 
           substringprocess = processname;
          // if (processname.length > 22) {
          //   substringprocess = processname.substring(0, 21) + "...";
          // } else {
          //   substringprocess = processname.substring(0, 21);
          // }
          let processType = "Default Template";
          let backgroundColor = '#009688ba';
          if (parseInt(item.processtemp.processType_ID) == 2) {
            processType = "CL Template";
            backgroundColor = '#3f51b5c9';
          }

          if (item.processtemp.processstatus_ID == 10) {
            titleLink +='<a data-toggle="tooltip" data-placement="top" data-skin="dark" data-title=\"' + item.processtemp.processTitle + '\" data-finalchcklisttitle="' + item.processtemp.processTitle + '\" style="color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;display: inline-block;width: 100%;" href="<?php echo site_url("/process/checklist") ?>' + "/" + item.processtemp.process_ID +'" class="foldname checklistTitleInfo protitle text-truncate" data-original-title="" title="">'+ substringprocess +'</a>';
          } else if (item.processtemp.processstatus_ID == 2 || item.processtemp.processstatus_ID == 3) {
            // if Template has checklists
            titleLink +='<a data-toggle="tooltip" data-placement="top" data-skin="dark" data-title=\"'+item.processtemp.processTitle+'"\ data-finalchcklisttitle="'+item.processtemp.processTitle+'" style="color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;display: inline-block;width: 100%;" href="<?php echo site_url("process/checklist_pending") ?>' + "/" + item.processtemp.process_ID + '" class="foldname checklistTitleInfo protitle text-truncate" data-original-title="" title="">'+ substringprocess +'</a>';        
          }       
          // if Admin Access 
          
          if (item.processtemp.assignmentRule_ID == 1 || item.processtemp.assignmentRule_ID == 2 || item.processtemp.assignmentRule_ID == "owned" || item.processtemp.assignmentRule_ID == "admin") {
            permissionDisplay +='<li class="m-portlet__nav-item">';
            // Settings icon (log)
            permissionDisplay += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
            permissionDisplay += "<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";
            //  Change process name
            permissionDisplay += '<button class="dropdown-item" type="button" data-id="' + item.processtemp.process_ID + '" onclick="updateProcessFetch(this)" data-toggle="modal" data-target="#updateProcessmodal"><i class="flaticon-edit-1"></i>&nbsp;Change Process Name</button>';          
            // Delete this process
            permissionDisplay += "<button class='dropdown-item' type='button' data-type='process' data-id='" + item.processtemp.process_ID + "' onclick='delete_Folprocheck(this)'><i class='flaticon-cancel'></i>&nbsp;Delete this template</button>";           
            // Duplicate this template
            permissionDisplay +=
                        "<button class='dropdown-item duplicate_temp' type='button' data-type='process' data-id='" +
                        item.processtemp.process_ID +
                        "' data-toggle='modal' data-target='#tempduplicate_modal'><i class='flaticon-file-1'></i>&nbsp;Duplicate this template</button>";
            
            //  Archive this template
            if (item.processtemp.processstatus_ID == 2) {
               // Copy Link
              permissionDisplay +=
                            "<button class='dropdown-item copytemplink' type='button' data-type='process' data-id='" +
                            item.processtemp.process_ID +
                            "'><i class='flaticon-tool-1'></i>&nbsp;Copy Template Link</button>";
              permissionDisplay += "<button class='dropdown-item' type='button' data-title='" + item.processtemp.processTitle + "' data-type='process' data-id='" + item.processtemp.process_ID + "' onclick='fetch_IDforAssignment(this),initAssignee(this),display_membernames(this)' data-toggle='modal' data-target='#assign_membersmodal'><i class='flaticon-user-add'></i>&nbsp;Assign this Process</button>";
              permissionDisplay += "<button class='dropdown-item' type='button' data-type='process' data-id='" + item.processtemp.process_ID + "' onclick='processset_toarchive(this)' data-target='#assign_membersmodal'><i class='flaticon-tabs'></i>&nbsp;Archive this template</button>";
              permissionDisplay += "<a href='<?php echo site_url('process/checklist') ?>" + '/' + item.processtemp.process_ID + "' class='dropdown-item' data-type='process' data-id='" + item.processtemp.process_ID + "' data-target='#assign_membersmodal'><i class='flaticon-edit'></i>&nbsp;Update template</a>";
              permissionDisplay += "</div> ";
              permissionDisplay += "</li>";
              // LOG ICON END
              // if
              // REPORT ICON START
              permissionDisplay += "<li class='m-portlet__nav-item'>";
              if (item.processtemp.processType_ID == 2) {
                permissionDisplay += "<a href='#' data-toggle='modal' data-id='" + item.processtemp.process_ID + "' data-pt='2' data-processtitle='" + item.processtemp.processTitle + "' onclick='fetch_runcheck_details(this)' data-target='#run_checklist-modal-coaching' class='m-portlet__nav-link m-portlet__nav-link--icon' data-toggle='m-tooltip' data-placement='top' title='Run Another Checklist'><i class='la la-plus-circle coloricons'></i></a>";
              } else {
                permissionDisplay += "<a href='#' data-toggle='modal' data-id='" + item.processtemp.process_ID + "' data-pt='1' data-processtitle='" + item.processtemp.processTitle + "' onclick='fetch_runcheck_details(this)' data-target='#run_checklist-modal' class='m-portlet__nav-link m-portlet__nav-link--icon' data-toggle='m-tooltip' data-placement='top' title='Run Another Checklist'><i class='la la-plus-circle coloricons'></i></a>";
              }
              permissionDisplay += "</li>";
              permissionDisplay += "<li class='m-portlet__nav-item'>";
              permissionDisplay += "<a href='<?php echo site_url('process/report') ?>" + '/' + item.processtemp.process_ID + "' class='m-portlet__nav-link m-portlet__nav-link--icon processCountBadge' data-title='View Report'><i class='la la-area-chart coloricons'></i></a> ";
              permissionDisplay += "</li>";
              // stringx += "</li> &nbsp;|&nbsp;";
            }
          }

          if (item.processtemp.processstatus_ID == 2 || item.processtemp.processstatus_ID == 3) {
            toolsDisplay += "<li class='m-portlet__nav-item'>";
            toolsDisplay += " <span class='m-menu__link-badge'>";
            toolsDisplay += " <span class='m-badge m-badge--metal processCountBadge' data-title='All Checklist'>" + item.statuses.all + "</span>";
            toolsDisplay += "</span>";
            toolsDisplay += "</li> ";
            toolsDisplay += "<li class='m-portlet__nav-item'>";
            toolsDisplay += " <span class='m-menu__link-badge'>";
            toolsDisplay += "<span class='m-badge m-badge--info processCountBadge' data-title='Pending Checklist'>" + item.statuses.pending + "</span>";
            toolsDisplay += "</span>";
            toolsDisplay += "</li>";
            toolsDisplay += "<li class='m-portlet__nav-item'>";
            toolsDisplay += "<span class='m-menu__link-badge'>";
            toolsDisplay += "<span class='m-badge m-badge--danger processCountBadge' data-title='Overdue Checklist'>" + item.statuses.overdue + "</span>";
            toolsDisplay += "</span>";
            toolsDisplay += "</li>";
            toolsDisplay += "<li class='m-portlet__nav-item'>";
            toolsDisplay += "<span class='m-menu__link-badge'>";
            toolsDisplay += "<span class='m-badge m-badge--success processCountBadge' data-title='Completed Checklist'>" + item.statuses.completed + "</span>";
            toolsDisplay += " </span>";
            toolsDisplay += "</li>";
          }else{
            toolsDisplay += " <li class='m-portlet__nav-item'>";
            toolsDisplay += " <span class='m-badge m-badge--info m-badge--wide processCountBadge' data-title='Click process template to add, modify and run a checklist' >Run a Checklist</span>";
            toolsDisplay += "</li> ";
          }
          stringx +='<div id="gettype_pvalue'+item.processtemp.process_ID+'" data-tit="'+item.processtemp.processTitle+'" data-ptype="'+item.processtemp.processType_ID+'" class="col-md-6 col-lg-4 col-lg-6 col-xl-4 mb-4">';
            stringx +='<span class="m-badge m-badge--brand m-badge--wide float-right" style="margin-top: -1rem;background-color:'+backgroundColor+';border-top-right-radius: 4px;border-top-left-radius: 10px;border-bottom-left-radius: 0;border-bottom-right-radius: 0;">'+processType+'</span>';
            stringx +='<div class="m-portlet m-portlet--collapsed m-portlet--head-sm portletLink mb-0" data-portlet="true" id="m_portlet_tools_7" data-linkpage="http://3.18.221.50/sz/process/checklist_answerable/49/133" style="background-color: #79d6e270;border-bottom: 4px solid #898f92 !important;">';
                stringx +='<div class="m-portlet__head border-accent rounded">';
                    stringx +='<div class="row p-3"';
                        stringx +='style="margin-left: -2.1em !important;margin-right: -2.15em !important;margin-bottom: -1rem !important;background: #aacfe0;">';
                        stringx +='<div class="m-portlet__head-caption">';
                            stringx +='<div class="m-portlet__head-title" style="width: 100%;">';
                              stringx +=titleLink;
                            stringx +='</div>';
                        stringx +='</div>';
                    stringx +='</div>';
                    stringx +='<div class="row" style="margin-left: -2rem;margin-right: -2rem;">';
                        stringx +='<div class="col-12 p-0">';
                            stringx +='<hr style="margin-top: 0.8rem !important;">';
                            stringx += "<input type='hidden' class='proStat' value='" + item.processtemp.processstatus_ID + "'>";
                            stringx += "<input type='hidden' class='eachprocessid' value=" + item.processtemp.process_ID + ">";
                        stringx +='</div>';
                    stringx +='</div>';
                    stringx +='<div class="row mt-0 mb-3" style="margin-left: -1.7em;">';
                        stringx +='<div class="m-portlet__head-tools">';
                            stringx +='<ul class="m-portlet__nav">';
                                stringx += permissionDisplay;
                                stringx += toolsDisplay;
                            stringx +='</ul>';
                        stringx +='</div>';
                    stringx +='</div>';
                stringx +='</div>';
            stringx +='</div>';
          stringx +='</div>';
        });
        $("#prorow").html(stringx);
        if ($("#prorow").html() == "") {
          $("#prorow").html('<div class="col-12"><div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline text-center"><div class="m-alert__text"><strong>No Template Available</strong><br>Create a template and it will show up here</div></div></div>');
        }
        var count = $("#prorow-total").data('count');
        var result = parseInt(limiter) + parseInt(perPage);
        if (count === 0) {
          $("#prorow-count").html(0 + ' - ' + 0);
        } else if (count <= result) {
          $("#prorow-count").html((limiter + 1) + ' - ' + count);
        } else if (limiter === 0) {
          $("#prorow-count").html(1 + ' - ' + perPage);
        } else {
          $("#prorow-count").html((limiter + 1) + ' - ' + result);
        }
        mApp.unblock('#content');
        callback(res.total);
      },
      error: function() {
        swal("Error", "Please contact admin", "error");
        mApp.unblock('#content');
      }
    });
  }

  function checklists_display(limiter, perPage, callback) {
    var templateType = $("#procheck_type_select").val();
    var activeSearch = $("#selectedchecklistStatus").val();
    var inputSearch = $("#searchChecklist").val();
    // fetch_active_processtype(3);xxx
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/checklists_display",
      data: {
        limiter: limiter,
        perPage: perPage,
        activeSearch: activeSearch,
        inputSearch: inputSearch,
        templateType: templateType,
        pageaccess: 1
      },
      beforeSend: function() {
        mApp.block('#content', {
          overlayColor: '#000000',
          type: 'loader',
          state: 'brand',
          size: 'lg'
        });
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        $("#checkrow-total").html(res.total);
        $("#checkrow-total").data('count', res.total);
        $("#checkrow").html("");
        var stringx = "";
        $.each(res.checklist_details, function(key, item) {
          var checklist_name = item.checklist.checklistTitle;
          var substringname = "";
          var duedate ="";
          var str_ddate ="";
          substringname = checklist_name;
          // if (checklist_name.length > 22) {
          //   substringname = checklist_name.substring(0, 21) + "...";
          // } else {
          //   substringname = checklist_name.substring(0, 21);
          // }
          let cklistLayout = 'checklistcolor';
          let chklistIcon = 'coloricons';
          var dty= new Date();

          if(item.checklist.dueDate!=null){
            var das = new Date(item.checklist.dueDate);
            var diffday = new Date(das - dty);
            var caldays = diffday / 1000 / 60 / 60 / 24;
            var rounddays = Math.round(caldays);

            var ob_j = moment(das);
            str_ddate = ob_j.format('MMM DD, YYYY');

            if(das < dty){
              duedate += "<li class='m-portlet__nav-item p-0'>";
              duedate += "<span class='m-menu__link-badge ml-1'>";
              duedate += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='"+str_ddate+"'>Overdued</span>";
              duedate += "</span>";
              duedate += "</li>";
            }
          }

          if (item.checklist.processType_ID == 2) {
            cklistLayout = 'coachingLogcolor';
            chklistIcon = 'coachingLogicons'
          }

          stringx += "<div id='gettype_cvalue"+item.checklist.checklist_ID+"' data-tit='"+item.checklist.checklistTitle+"' class='col-lg-4 mb-4'>";
          stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm portletLink mb-0 " + cklistLayout + "' data-portlet='true' id='m_portlet_tools_7' data-linkpage='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "'>";
          stringx += "<div class='m-portlet__head border-accent rounded ' style='display: inline-block;border-bottom:unset'>";
          stringx += "<div class='row' style='margin-top:1em'>";
          stringx += "<div class='m-portlet__head-caption'>";
          stringx += "<div class='m-portlet__head-title' style='width: 100%;'>";
          // stringx += "<span class='m-portlet__head-icon'>";

          // Check or clock icon
          // if (item.checklist.status_ID == 2) {
          //   stringx += "<span class='m-portlet__head-icon p-0 mr-2 fa-spin'>";
          //   stringx += "<i class='fa fa-spinner " + chklistIcon + "' id=''></i>";
          //   stringx += "</span>&nbsp;&nbsp;";
          // } else if (item.checklist.status_ID == 3) {
          //   stringx += "<span class='m-portlet__head-icon p-0 mr-2'>";
          //   stringx += "<i class='fa fa-check " + chklistIcon + "' id=''></i>";
          //   stringx += "</span>";
          // }

          // stringx += "</span>";
          if (item.checklist.processType_ID == 2) {
            stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-finalchcklisttitle=\"" + checklist_name + "\" style='color:#484747;display: inline-block;font-size: 1.2rem;font-weight: 500;width: 100%;' href='<?php echo site_url('process/coaching_log') ?>" + '/' + item.checklist.checklist_ID + "' class='foldname checklistTitleInfo text-truncate'>" + substringname + "</a>";
          } else {
            stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-finalchcklisttitle=\"" + checklist_name + "\" style='color:#484747;display: inline-block;font-size: 1.2rem;font-weight: 500;width: 100%;' href='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "' class='foldname checklistTitleInfo text-truncate'>" + substringname + "</a>";
          }
          stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
          stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
          stringx += "</div>";
          stringx += "</div>";
          stringx += "</div>";

          // stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";
          stringx +='<div class="row">';
            stringx +='<div class="createdby pro-name text-truncate" style=" width: 100%; display: inline-block;">'+item.checklist.processTitle+'</div><br>';
          stringx +='</div>';
          stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
          stringx += " <div class='col-md-12 p-0'>";
          stringx += "<div class='progress'>"

          // Start Status Bar
          // stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
          // stringx += "</div>"
          // End

          if (item.statuses.all == item.statuses.completed) {
            stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success cklistProgressBar' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-title='Completed'></div>";
            stringx += "</div>";
          } else if (item.statuses.completed == 0) {
            stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light cklistProgressBar' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-title='Not Yet Answered'></div>";
            stringx += "</div>";
          } else {
            var percentage_pending = (item.statuses.completed / item.statuses.all) * 100;
            stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger cklistProgressBar' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-title='" + item.statuses.completed + "/" + item.statuses.all + "'></div>";
            stringx += "</div>";  
          }

          stringx += "</div>";
          stringx += "</div>";

          stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;margin-left: -1.4em;'>";
          stringx += "<div class='m-portlet__head-tools pb-2'>";
          stringx += "<ul class='m-portlet__nav'>";

          // if

          if (item.checklist.assignmentRule_ID == "owned" || item.checklist.assignmentRule_ID == "admin" || item.checklist.assignmentRule_ID == 1 || item.checklist.assignmentRule_ID == 2 || item.checklist.assignmentRule_ID == 6) {
            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
            stringx += "<i class='la la-cog bottom-icons'></i></a>";
            stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";

            // Change checklist name
            stringx += "<button class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='updateChecklistFetch(this)' data-toggle='modal' data-target='#updateChecklistmodal'>";
            stringx += "<i class='flaticon-file'></i>&nbsp;Change Checklist name</button>";

            // Assign
            stringx += "<button class='dropdown-item' type='button' data-protype='" + item.checklist.processType_ID + "' data-process='" + item.checklist.process_ID + "' data-title='" + item.checklist.checklistTitle + "' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
            stringx += "<i class='la la-users'></i>";
            stringx += "&nbsp;Assign this checklist</button>";

            // Delete
            stringx += "<button data-type='checklist' class='dropdown-item' data-type='checklist' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='delete_Folprocheck(this)'>";
            stringx += "<i class='la la-trash'></i>&nbsp;Delete this Checklist</button>";
            stringx += "</div>";
            stringx += "</li>";

            //ARCHIVE ICON
            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<a href='#' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_toarchive(this)' class='m-portlet__nav-link m-portlet__nav-link--icon' data-toggle='m-tooltip' data-placement='top' title='Archive this Record'><i class='la la-archive bottom-icons'></i></a> ";
            stringx += "</li>";
            //end ARCHIVE ICON
          }
          // end if
          var createdbynames = item.statuses.names.fname + " " + item.statuses.names.lname;
          stringx += "<li class='m-portlet__nav-item p-0 dateCreatedDetails' data-datecreated='" + item.checklist.dateTimeCreated + "' data-cname='" + createdbynames + "'>";
          stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
          stringx += "<i class='la la-info-circle bottom-icons'></i>";
          stringx += "</a>";
          stringx += "</li>";
          let notes = item.checklist.remarks;
          if (item.checklist.remarks == null) {
            notes = "No Remarks Found";
          }
          stringx += "<li class='m-portlet__nav-item p-0 checklistNotes' data-notes='" + notes + "'>";
          stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
          stringx += "<i class='la la-commenting bottom-icons'></i>";
          stringx += "</a>";
          stringx += "</li>";

          stringx += "<li class='m-portlet__nav-item p-0'>";
          stringx += "<span class='m-menu__link-badge'>";
          stringx += "</span>";
          stringx += "</li>";

          // if
        if(item.checklist.dueDate!=null && item.checklist.task_permission == "yes"){
          stringx += "<li class='m-portlet__nav-item p-0'>";
          stringx += "<span class='m-menu__link-badge ml-1'>";
          stringx += "<span class='m-badge m-badge--brand m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Contains assigned tasks and has due date'>Task - "+str_ddate+"</span>";
          stringx += "</span>";
          stringx += "</li>";
        }else if(item.checklist.dueDate!=null && duedate==""){
          stringx += "<li class='m-portlet__nav-item p-0'>";
          stringx += "<span class='m-menu__link-badge ml-1'>";
          stringx += "<span class='m-badge m-badge--info m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date'>"+str_ddate+"</span>";
          stringx += "</span>";
          stringx += "</li>";
        }else if(item.checklist.task_permission == "yes") {
          stringx += "<li class='m-portlet__nav-item p-0'>";
          stringx += "<span class='m-menu__link-badge ml-1'>";
          stringx += "<span class='m-badge m-badge--brand m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Contains assigned tasks'>Assigned Tasks</span>";
          stringx += "</span>";
          stringx += "</li>";
        }else if(duedate!=""){
          stringx += duedate;
        }
          // if

          stringx += "</ul>";
          stringx += "</div>";
          stringx += "</div>";

          stringx += "</div>";
          stringx += "</div>";
          let spin = "";
          let cklistStatIcon = "fa fa-check "+ chklistIcon;
          let checklistStat = "already completed"
          if (item.checklist.status_ID == 2) {
            spin = "fa-spin";
            cklistStatIcon = "fa fa-spinner " + chklistIcon;
            checklistStat = "is still ending"
          } 
          if (item.checklist.processType_ID == 2) {
            stringx += '<div class="btn float-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;margin-top: -1rem;background-color: #673AB7;border-top-right-radius: 0;">';
                stringx += '<div class="btn btn-light m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+spin+' processCountBadge" data-title="Coaching Log '+checklistStat+'" style="width: 25px !important;height: 25px !important;"><i class="fa '+cklistStatIcon+'" style="font-size: 17 px;"></i></div><span class="button-content text-light text-capitalize pl-2" style="font-size: 0.9rem;">Coaching Log</span>';
            stringx += '</div>';
          } else {
            stringx += '<div class="btn float-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;margin-top: -1rem;background-color: #4caf50;border-top-right-radius: 0;">';
                stringx += '<div class="btn btn-light m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+spin+' processCountBadge" data-title="Checklist '+checklistStat+'" style="width: 25px !important;height: 25px !important;"><i class="fa '+cklistStatIcon+'" style="font-size: 17 px;"></i></div><span class="button-content text-light text-capitalize pl-2" style="font-size: 0.9rem;">Checklist</span>';
            stringx += '</div>';
          }
          stringx += "</div>";
        })
        $("#checkrow").html(stringx);
        if ($("#checkrow").html() == "") {
          $("#checkrow").html('<div class="col-12"><div class="alert alert-metal alert-dismissible fade show   m-alert m-alert--air m-alert--outline text-center"><div class="m-alert__text"><strong>No Checklist Available</strong><br>Run a checklist and it will show up here</div></div></div>');
        }
        var count = $("#checkrow-total").data('count');
        var result = parseInt(limiter) + parseInt(perPage);
        if (count === 0) {
          $("#checkrow-count").html(0 + ' - ' + 0);
        } else if (count <= result) {
          $("#checkrow-count").html((limiter + 1) + ' - ' + count);
        } else if (limiter === 0) {
          $("#checkrow-count").html(1 + ' - ' + perPage);
        } else {
          $("#checkrow-count").html((limiter + 1) + ' - ' + result);
        }

        mApp.unblock('#content');
        callback(res.total);
      },
      error: function() {
        swal("Error", "Please contact admin", "error");
        mApp.unblock('#content');
      }
    });
  }
  // END OF NEW CODES

  function checklistpermission_access() {
    var uid = $("#user_id").val();
    var checklist = "checklist";
    var displayid = 0;

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_access",
      // data: {
      //   uid:uid,
      // },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        //temporary admin
        if (result.length > 0) {
          // $("#dropdownMenu3").attr("hidden", false);
          displayid = 1;
          $("#displaychecklist_id").val(displayid);
          displaychecklists();
        } else {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/fetch_assignment_checklist",
            data: {
              type: checklist
            },
            cache: false,
            success: function(res) {
              var res = JSON.parse(res.trim());
              if (res == 0) {
                // $("#dropdownMenu3").remove();
                //owned
                displayid = 2;
                $("#displaychecklist_id").val(displayid);
                displaychecklists();
              } else {
                // with assigned
                displayid = 3;
                $("#displaychecklist_id").val(displayid);
                displaychecklists();
              }
            }
          });
        }
      }
    });
  }

  function getassignmentrule(folderid, type) {
    var id;
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetchone_assignment",
      data: {
        folderid: folderid,
        typename: type
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
      }
    });
  }

  function displayfolders(word) {
    var d_id = $("#display_id").val();
    var uid = $("#user_id").val();

    var whichfunction = $("#folderLoadMore").data('whichfunction');

    var link = (d_id === "2") ? "display_ownfolders" : (d_id === "1") ? "display_folders" : (d_id === "3") ? "display_assignedfolders" : " ";

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/" + link,
      data: {
        word: word,
        folder_ids: folder_ids,
        whichfunction: whichfunction
      },
      beforeSend: function() {
        $('.loading-icon').show();
      },
      cache: false,
      success: function(res) {
        $('.loading-icon').hide();
        var stringx = "";
        var stringlastid = "";
        var folderid;
        var type = "folder";
        var assignment;

        var result = JSON.parse(res.trim());



        if (result.data.length < display_perPage) {
          $("#folderLoadMore").hide();
        }
        $("#folderLoadMore").data('whichfunction', result.whichfunction);
        var lastid = 0;

        if (result.data.length != 0) {
          $.each(result.data, function(key, item) {
            folder_ids.push(item.folder_ID);
            lastid = item.folder_ID;
            var foldername = item.folderName;
            var substringname = "";
            if (foldername.length > 22) {
              substringname = foldername.substring(0, 21) + "...";
            } else {
              substringname = foldername.substring(0, 21);
            }
            //get access permission for each folder  

            var assignmentRuleid = item.assignmentRule_ID;

            stringx += "<div class='col-lg-4 foldercontent' id='" + item.status_ID + "'>";
            stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
            stringx += "<div class='m-portlet__head border-accent rounded foldercolor'>";

            stringx += "<div class='row' style='margin-top:1em'>";
            stringx += "<div class='m-portlet__head-caption'>";
            stringx += "<div class='m-portlet__head-title'>";
            stringx += "<span class='m-portlet__head-icon'>";
            stringx += "<i class='fa fa-folder icon-color' id='" + item.folder_ID + "'></i>";
            stringx += "</span>";

            stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-original-title='" + item.folderName + "' title='" + item.folderName + "' style='color:#9ce4f5;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/subfolder') ?>" + '/' + item.folder_ID + "' class='foldname'>" + substringname + "</a>";

            stringx += "<input type='hidden' value='" + item.folder_ID + "' id='eachfolderid'>";
            stringx += "<input type='hidden' class='folStat' value='" + item.status_ID + "'";
            stringx += "<input type='hidden' class='fetchval' id='fetchval'>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "&nbsp;&nbsp;&nbsp;&nbsp;<span class='colorwhitesize createdby'>" + item.fname + "&nbsp;" + item.lname + "</span>";

            stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
            stringx += "<div class='m-portlet__head-tools'>";
            stringx += "<ul class='m-portlet__nav'>";

            // if admin OR can edit, view and run OR can edit, view own and run

            if (assignmentRuleid == "1" || assignmentRuleid == "2" || d_id == "1" || item.createdBy == uid) {
              stringx += "<li class='m-portlet__nav-item' data-toggle='m-tooltip' data-original-title='Change, Archive, and Remove'>";

              stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
              stringx += "<i class='la la-cog'></i></a>";
              stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
              stringx += "<button class='dropdown-item' type='button' data-id='" + item.folder_ID + "' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
              stringx += "<i class='flaticon-file'></i>&nbsp;Change folder name</button>";

              stringx += "<button class='dropdown-item' type='button' data-id=" + item.folder_ID + " onclick='folderset_toarchive(this)'>";
              stringx += "<i class='la la-archive'></i>";
              stringx += "&nbsp;Archive this folder</button>";

              stringx += "<button class='dropdown-item' type='button' data-type='folder' data-id='" + item.folder_ID + "' onclick='delete_Folprocheck(this)'>";
              stringx += "<i class='la la-trash'></i>&nbsp;Delete this folder</button>";

              stringx += "</div>";
              stringx += "</li>";

              stringx += "<li class='m-portlet__nav-item' data-toggle='modal' data-title='" + item.folderName + "' data-target='#assign_membersmodal' data-type='folder' data-id='" + item.folder_ID + "' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='m-tooltip' data-original-title='Assign Members'>";
              stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
              stringx += "<i class='la la-users'></i>";
              stringx += "</a>";
              stringx += "</li>";

              stringx += "<li class='m-portlet__nav-item'>";
              stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' onclick='getfolder_nameid(this)' data-toggle='modal' data-target='#blankroot-modal' data-folname='" + item.folderName + "' data-id='" + item.folder_ID + "'><i class='la la-plus-circle'></i></a> ";
              stringx += "</li>";
            } else if (d_id === "2") {
              stringx += "<li class='m-portlet__nav-item'>";
              stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
              stringx += "<i class='la la-cog'></i></a>";
              stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
              stringx += "<button class='dropdown-item' type='button' data-id='" + item.folder_ID + "' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
              stringx += "<i class='flaticon-file'></i>&nbsp;Change folder name</button>";

              stringx += "<button class='dropdown-item' type='button' data-id=" + item.folder_ID + " onclick='folderset_toarchive(this)'>";
              stringx += "<i class='la la-archive'></i>";
              stringx += "&nbsp;Archive this folder</button>";

              stringx += "<button class='dropdown-item' type='button' data-type='folder' data-id='" + item.folder_ID + "' onclick='delete_Folprocheck(this)'>";
              stringx += "<i class='la la-trash'></i>&nbsp;Delete this folder</button>";

              stringx += "</div>";
              stringx += "</li>";

              stringx += "<li class='m-portlet__nav-item' data-toggle='modal' data-target='#assign_membersmodal' data-type='folder' data-id='" + item.folder_ID + "' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='m-tooltip' data-original-title='Assign Members'>";
              stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
              stringx += "<i class='la la-users'></i>";
              stringx += "</a>";
              stringx += "</li>";

              stringx += "<li class='m-portlet__nav-item'>";
              stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' onclick='getfolder_nameid(this)' data-toggle='modal' data-target='#blankroot-modal' data-folname='" + item.folderName + "' data-id='" + item.folder_ID + "'><i class='la la-plus-circle'></i></a> ";
              stringx += "</li>";
            }
            stringx += "<li class='m-portlet__nav-item'>";
            stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle'></i></a> ";
            stringx += "</li>";

            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";

            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
          });
        } else {
          stringx += "<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records</strong></h5></div>";
        }
        $("#folrow").append(stringx);
        $('[data-toggle="tooltip"]').tooltip("dispose");
        $('[data-toggle="tooltip"]').tooltip();

        stringlastid += "<div class='load-more col-lg-12' lastID='" + lastid + "' style='display: none;'>";
        stringlastid += "<h5 style='text-align: center;'>";
        stringlastid += "<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
        stringlastid += "</div> Loading more folder...</h5></div>";
        $("#folrow").append(stringlastid);
      }
    });
  }


  function displaychecklists(word) {
    var whichfunction = $("#checklistLoadMore").data('whichfunction');

    var d_id = $("#displaychecklist_id").val();
    var prod_id = $("#displayprocess_id").val();
    var fold_id = $("#display_id").val();
    var selectedval = $("#selectedchecklistStatus").val();

    var uid = $("#user_id").val();
    var assignmentRuleid = 0;

    var link = (d_id == "2" && prod_id == "2" && fold_id == "2") ? "display_ownchecklists" : (d_id == "1") ? "display_allchecklists" : ((d_id == "3" && prod_id == "3" && fold_id == "3") || (d_id == "3" && prod_id == "3" && fold_id == "2") || (d_id == "3" && prod_id == "2" && fold_id == "2") || (d_id == "2" && prod_id == "2" && fold_id == "3") || (d_id == "2" && prod_id == "3" && fold_id == "2") || (d_id == "2" && prod_id == "3" && fold_id == "3") || (d_id == "3" && prod_id == "2" && fold_id == "3")) ? "display_assignedchecklists" : " ";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/" + link,
      data: {
        checklist_ids: checklist_ids,
        whichfunction: whichfunction,
        word: word,
        statval: selectedval
      },
      beforeSend: function() {
        $('.loading-icon-check').show();
      },
      cache: false,
      success: function(res) {
        $('.loading-icon-check').hide();
        var stringx = "";
        var checklistrule = "";


        var result = JSON.parse(res.trim());

        if (result.data.length < display_perPage) {
          $("#checklistLoadMore").hide();
        }
        $("#checklistLoadMore").data('whichfunction', result.whichfunction);
        if (result.data.length != 0) {

          $.each(result.data, function(key, item) {
            checklist_ids.push(item.checklist.checklist_ID);
            if (d_id == "3" || prod_id == "3" || fold_id == "3") {
              assignmentRuleid = item.checklist.assignmentRule_ID;
            }

            if (d_id == "3") {
              checklistrule = item.checklist.checklist_assrule;
            }

            var cid = item.checklist.checklist_ID;
            var checklist_name = item.checklist.checklistTitle;

            var substringname = "";
            if (checklist_name.length > 22) {
              substringname = checklist_name.substring(0, 21) + "...";
            } else {
              substringname = checklist_name.substring(0, 21);
            }
            stringx += "<div class='col-lg-4'>";
            stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm portletLink mb-0' data-portlet='true' id='m_portlet_tools_7' data-linkpage='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "'>";
            stringx += "<div class='m-portlet__head border-accent rounded checklistcolor'>";

            stringx += "<div class='row' style='margin-top:1em'>";
            stringx += "<div class='m-portlet__head-caption'>";
            stringx += "<div class='m-portlet__head-title'>";
            stringx += "<span class='m-portlet__head-icon'>";

            if (item.checklist.status_ID == "2") {
              stringx += "<span class='m-portlet__head-icon p-0 mr-2 fa-spin'>";
              stringx += "<i class='fa fa-spinner coloricons' id=''></i>";
              stringx += "</span>&nbsp;&nbsp;";
            } else if (item.checklist.status_ID == "3") {
              stringx += "<span class='m-portlet__head-icon p-0 mr-2'>";
              stringx += "<i class='fa fa-check coloricons' id=''></i>";
              stringx += "</span>";
            }
            stringx += "</span>";
            stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-original-title='" + item.checklist.checklistTitle + "' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "' class='foldname'>" + substringname + "</a>";
            stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
            stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";

            // var dateString=item.dateTimeCreated;
            // var dateObj = new Date(dateString);
            // var momentObj = moment(dateObj);
            // var momentString = momentObj.format('lll');
            // stringx +="&nbsp;&nbsp;&nbsp;&nbsp;<span class='createdby' style='color:#043106;font-size:0.9em'>"+momentString+"</span><br>";

            stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";

            // stringx +="<span style='font-size: 0.9em;margin-left:1em'>";
            // stringx +="<span class='m-badge m-badge--accent m-badge--wide'>CH</span>";
            // stringx +="<span class='m-badge m-badge--accent m-badge--wide'>SB</span>";
            // stringx +="</span>";

            stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
            stringx += " <div class='col-md-12 p-0'>";
            stringx += "<div class='progress'>"


            var alltask = item.statuses.all;
            var allcomp = item.statuses.completed;
            var allpending = item.statuses.pending;

            if (alltask == allcomp) {
              stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
              stringx += "</div>"
            } else if (allcomp == 0) {
              stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
              stringx += "</div>"
            } else {
              var percentage_pending = (allcomp / alltask) * 100;
              stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-toggle='m-tooltip' data-placement='top' data-original-title='" + allcomp + "/" + alltask + "'></div>"
              stringx += "</div>"
            }
            stringx += "</div>";
            stringx += "</div>";

            stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
            stringx += "<div class='m-portlet__head-tools'>";
            stringx += "<ul class='m-portlet__nav'>";

            if (d_id == "1" || assignmentRuleid == "1" || item.checklist.ownid == uid || checklistrule == "6") {
              stringx += "<li class='m-portlet__nav-item p-0'>";
              stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
              stringx += "<i class='la la-cog bottom-icons'></i></a>";
              stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
              stringx += "<button class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='updateChecklistFetch(this)' data-toggle='modal' data-target='#updateChecklistmodal'>";
              stringx += "<i class='flaticon-file'></i>&nbsp;Change Checklist name</button>";
              stringx += "<button class='dropdown-item' type='button' data-process='0' data-title='" + item.checklist.checklistTitle + "' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
              stringx += "<i class='la la-users'></i>";
              stringx += "&nbsp;Assign this checklist</button>";

              stringx += "<button data-type='checklist' class='dropdown-item' data-type='checklist' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='delete_Folprocheck(this)'>";
              stringx += "<i class='la la-trash'></i>&nbsp;Delete this Checklist</button>";
              stringx += "</div>";
              stringx += "</li>";

              stringx += "<li class='m-portlet__nav-item p-0'>";
              stringx += "<a href='#' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_toarchive(this)' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-archive bottom-icons'></i></a> ";
              stringx += "</li>";
            }
            stringx += "<li class='m-portlet__nav-item' data-toggle='modal' data-original-title='Tooltip title'>";
            stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
            stringx += "<i class='la la-info-circle bottom-icons'></i>";
            stringx += "</a>";
            stringx += "</li>";

            stringx += "<li class='m-portlet__nav-item p-0'>";
            stringx += "<span class='m-menu__link-badge'>";

            if (item.checklist.dueDate != null) {
              var dateString2 = item.checklist.dueDate;
              var dateObj2 = new Date(dateString2);
              var dateObj3 = new Date();
              var datediff = new Date(dateObj2 - dateObj3);
              var days = datediff / 1000 / 60 / 60 / 24;
              var rounddays = Math.round(days);

              var momentObj2 = moment(dateObj2);
              var momentString2 = momentObj2.format('ll');

              if (dateObj2 > dateObj3) {
                stringx += "<span class='m-badge m-badge--success m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date:" + rounddays + "left'>" + momentString2 + "</span>";
              } else if (dateObj2 < dateObj3) {
                stringx += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue!'>" + momentString2 + "</span>";
              } else if (dateObj2 == dateObj3) {
                stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";
              }
            }
            stringx += "</span>";
            stringx += "</li>";

            if (item.checklist.taskruleid == "5" || item.checklist.taskruleid == "6") {
              stringx += "<li class='m-portlet__nav-item p-0'>";
              stringx += "<span class='m-menu__link-badge'>";
              stringx += "<span class='m-badge m-badge--brand m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Contains assigned tasks'>Assigned Task</span>";
              stringx += "</span>";
              stringx += "</li>";
            }
            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";

            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
          });
        } else {
          stringx += "<div class='col-lg-12' style='color:#7d7d7d'><h5 style='text-align: center;'><i style='color:#e4ac05'class='fa fa-exclamation-circle'></i><strong> No Records</strong></h5></div>";
        }

        $("#checkrow").append(stringx);
        $('[data-toggle="tooltip"]').tooltip("dispose");
        $('[data-toggle="tooltip"]').tooltip();

      }
    });
  }

  //END OF JQUERY DISPLAY

  //CHARACTERS COUNTER
  $('#processTitle').keyup(function() {
    var max = 45;
    var len = $(this).val().length;
    if (len >= max) {
      $('#charNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#charNum').text(char + '/45 characters remaining');
    }
  });

  $('#process_folderTitle').keyup(function() {
    var max = 45;
    var len = $(this).val().length;
    if (len >= max) {
      $('#specific_charNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#specific_charNum').text(char + '/45 characters remaining');
    }
  });

  $('#folderName').keyup(function() {
    var max = 45;
    var len = $(this).val().length;
    if (len >= max) {
      $('#foldercharNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#foldercharNum').text(char + '/45 characters remaining');
    }
  });

  $('#addchecklistName').keyup(function() {
    var max = 80;
    var len = $(this).val().length;
    if (len >= max) {
      $('#checkcharNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#checkcharNum').text(char + '/80 characters remaining');
    }
  });

  // UPDATE

  $('#updatefolderName').keyup(function() {
    var max = 45;
    var len = $(this).val().length;
    if (len >= max) {
      $('#updatefoldercharNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#updatefoldercharNum').text(char + '/45 characters remaining');
    }
  });

  $('#updateprocessName').keyup(function() {
    var max = 45;
    var len = $(this).val().length;
    if (len >= max) {
      $('#updateprocesscharNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#updateprocesscharNum').text(char + '/45 characters remaining');
    }
  });

  $('#updatechecklistName').keyup(function() {
    var max = 80;
    var len = $(this).val().length;
    if (len >= max) {
      $('#updatechecklistcharNum').text('You have reached the limit |');
    } else {
      var char = max - len;
      $('#updatechecklistcharNum').text(char + '/80 characters remaining');
    }
  });
  //END CHARACTERS COUNTER


  function getprocessval(element) {
    var eachprocessid = $("#eachprocessid").val();
    $('.processcontent').hide();
    if (element.value == "100") {
      $('.processcontent').show();
    }
    $('.processcontent .proStat').each(function() {
      var folderStatus = $(".proStat").val();
      if (element.value == "10") {
        $('.pr10').show();
      } else if (element.value == "2") {
        $('.pr2').show();
      } else if (element.value == "11") {
        $('.pr11').show();
      }
    });
  }
  $(document).ready(function() {
    $('#formadd').formValidation({
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        folderName: {
          message: 'Your folder name is not valid',
          validators: {
            notEmpty: {
              message: 'Your folder name is required and can\'t be empty'
            },
            regexp: {
              regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
              message: 'Folder name can only consist of letters and spaces'
            }
          }
        }
      }
    }).on('success.form.bv', function(e) {
      e.preventDefault();
    });
    $('#subformadd').formValidation({
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        subfolderName: {
          message: 'Your sub-folder name is not valid',
          validators: {
            notEmpty: {
              message: 'Your sub-folder name is required and can\'t be empty'
            },
            regexp: {
              regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
              message: 'Sub-folder name can only consist of letters and spaces'
            }
          }
        }
      }
    }).on('success.form.bv', function(e) {
      e.preventDefault();
    });
    $('#processadd').formValidation({
      message: 'This value is not valid',
      icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        processTitle: {
          message: 'Your template/process name is not valid',
          validators: {
            notEmpty: {
              message: 'Your template/process name is required and can\'t be empty'
            },
            regexp: {
              regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
              message: 'template/process name can only consist of letters and spaces'
            }
          }
        }
      }
    }).on('success.form.bv', function(e) {
      e.preventDefault();
    });
  });

  function emptyField() {
    $(".errormessage").remove();
    $("input.errorprompt").after("<span class='errormessage' style='color:red;font-size:0.8em'>*This field can't be empty</span>");
  }

  function PemptyField() {
    $(".pErrormessage").remove();
    $("input.pErrorprompt").after("<span class='pErrormessage' style='color:red;font-size:0.8em'>*This field can't be empty</span>");
  }

  function unable_toremove() {
    swal("Removal of assignee is not allowed", "(This is a coaching log checklist)", "info");
  }

  function removemember_assignment(element) {
    swal({
      title: 'Are you sure?',
      text: "This member will be removed from accessing this file.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/delete_assignment",
          data: {
            assign_ID: $(element).data('id')
          },
          cache: false,
          success: function() {
            reinitAssigneeDatatable();
            swal(
              'Deleted!',
              'Member successfully Removed.',
              'success'
            )
            // $("#assign_membersmodal").modal('hide');
          },
          error: function(res) {
            swal(
              'Oops!',
              'Something is wrong with your code!',
              'error'
            )
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Removing member has been cancelled',
          'error'
        )
      }
    });
  }
  //SAVE or ADD functionalities

  function addmember_assignment(element) {
    var uid = $(element).data('id');
    var reload = $(element).data('reload');
    var pid = $(element).data('pro');
    var fptc_id = $("#folprotaskcheck_id").val();
    var assigntype = $("#assign_type").val();
    var assignrule = $("#assignmentRule :selected").val();
    // var fpc_title = $("#fpc_title").text();
    var fpc_title ="";
    var protype = 0;
    if (assigntype == "process") {
      protype = $("#gettype_pvalue" + fptc_id).data('ptype');
      fpc_title =  $("#gettype_pvalue" + fptc_id).data('tit');
    }else{
      fpc_title =  $("#gettype_cvalue" + fptc_id).data('tit');
    }
    swal({
      title: 'Are you sure?',
      text: "This member will be assigned to this folder/process template/checklist",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/add_assignment",
          data: {
            type: assigntype,
            assignmentRule_ID: assignrule,
            userid: uid,
            folprotaskcheck_id: fptc_id,
            title: fpc_title,
            pro: pid,
            ptype: protype
          },
          cache: false,
          success: function(res) {
            var resObj = $.parseJSON(res.trim());
            if (resObj.checker == 1) {
              swal("This employee cannot be added", "(This employee is being assigned to checklist/s under this process template)", "info");
            } else if (resObj.checker == 2) {
              swal("This employee cannot be added", "(This employee is being assigned to task/s under this process template)", "info");
            } else if (resObj.checker == 3) {
              swal("This employee cannot be added", "(This employee is being assigned to process template/s under this process template)", "info");
            } else {
              systemNotification(resObj.notifid.notif_id);
              if (reload == 1) {
                getaccmembers("multi");
              } else if (reload == 2) {
                getmember_name();
              }
              // addmember_assignment(); nicca: why gitawag again? basin mag infiniteloop ni
              $("#memberdetails tbody").html("");
              $("#members").html("");
              show_lists_toassign();
              $.notify({
                message: 'Successfully Assigned'
              }, {
                type: 'success',
                timer: 1000,
                z_index: 99999
              });

            }
            // $("#assign_membersmodal").modal('hide'); nicca:hide temp only
          },
          error: function(res) {
            swal(
              'Oops!',
              'Cannot assign',
              'error'
            )
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Assigning user has been cancelled',
          'error'
        )
      }
    });

  }

  function add_newfolder() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_folder",
      data: {
        folderName: $("#folderName").val(),
      },
      cache: false,
      success: function(res) {
        $('#folder-modal').modal('hide');
        displayfolders();
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Folder',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
  }

  function add_newblanktemplate() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_processfolder",
      data: {
        processTitle: $("#processTitle").val(),
        processtype_ID: $("#templatetype_select").val()
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        // console.log(res);
        if (res > 0) {
          // displayfolders();
          // processpermission_access()
          $("#prorow-perpage").change();
          swal({
            position: 'center',
            type: 'success',
            title: 'Successfully Added Process',
            showConfirmButton: false,
            timer: 1500
          });
          $("#blank-modal").modal("hide");
        } else {
          swal({
            position: 'center',
            type: 'error',
            title: 'There was an error upon saving.',
            showConfirmButton: true,
          });
        }
      },
      error: function(res) {
        console.log(res);
      }
    });
  }

  function add_newblanktemplate_specific() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_processfolder",
      data: {
        processTitle: $("#process_folderTitle").val(),
        folder_ID: $("#rootfolderID").val()
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        $('#blankroot-modal').modal('hide');
        displayfolders();
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Process',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
  }
  $("#btn-subfoldersave").click(function() {
    if ($.trim($("#subfolderName,#rootfolder_ID").val()) === "") {
      e.preventDefault();
    } else {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/add_subfolder",
        data: {
          folderName: $("#subfolderName").val(),
          folder_ID: $("#rootfolder_ID").val()
        },
        cache: false,
        success: function(res) {
          window.location.href = '<?php echo base_url("process") ?>';
          swal({
            position: 'center',
            type: 'success',
            title: 'Successfully Added Sub folder',
            showConfirmButton: false,
            timer: 1500
          });
          var result = JSON.parse(res.trim());
        },
        error: function(res) {
          console.log(res);
        }
      });
    }
  });

  function foldericon(element) {
    var iconid = $(element).data('id');
    $("#foldericon" + iconid).toggleClass("fa fa-folder-open").toggleClass("fa fa-folder");
  }

  function delete_Folprocheck(element) {
    var type_fpc = $(element).data('type');
    swal({
      title: 'Are you sure?',
      text: "All data under this " + type_fpc + " will be deleted, too.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/delete_folder_process_checklist",
          data: {
            all_ID: $(element).data('id'),
            type: $(element).data('type')
          },
          cache: false,
          success: function() {
            $("#prorow-perpage").change();
            $("#checkrow-perpage").change();
            if (type_fpc == "folder") {
              $(".folderTrigger").trigger("click");
            } else if (type_fpc == "process") {
              $(".processTrigger").trigger("click");
            } else {
              $(".checklistTrigger").trigger("click");
            }
            swal(
              'Deleted!',
              type_fpc + ' successfully deleted.',
              'success'
            )
          },
          error: function(res) {
            swal(
              'Oops!',
              'You cant delete!',
              'error'
            )
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Deleting a ' + type_fpc + ' has been cancelled',
          'error'
        )
      }
    });
    console.log($(element).data('id'));
  }

  function updateFetch(element) {
    var folderName = $("#updatefolderName").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_update",
      data: {
        folder_ID: $(element).data('id'),
        folderName: folderName
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        var data = res;
        $("#updatefolderName").val(data.folderName);
        $("#form_foldernameupdate").data('id', data.folder_ID);
      }
    });
  }

  function updateProcessFetch(element) {
    var processTitle = $("#updateprocessName").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetchprocess_update",
      data: {
        process_ID: $(element).data('id'),
        processTitle: processTitle
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        var data = res;
        $("#updateprocessName").val(data.processTitle);
        $("#form_processnameupdate").data('id', data.process_ID);
      }
    });
  }

  function updateChecklistFetch(element) {
    var checklistTitle = $("#updatechecklistName").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetchchecklist_update",
      data: {
        checklist_ID: $(element).data('id'),
        checklistTitle: checklistTitle
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        var data = res;
        $("#updatechecklistName").val(data.checklistTitle);
        $("#form_checklistnameupdate").data('id', data.checklist_ID);
      }
    });
  }

  function saveChangesFolder(folderid) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_folder",
      data: {
        folder_ID: folderid,
        folderName: $("#updatefolderName").val()
      },
      cache: false,
      success: function(res) {
        $(".folderTrigger").trigger("click");
        swal({
          position: 'center',
          type: 'success',
          title: 'folder successfully updated',
          showConfirmButton: false,
          timer: 1500
        });
        $("#updatemodal").modal('hide');

      }
    });
  }

  function saveChangesProcess(processid) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_process",
      data: {
        process_ID: processid,
        processTitle: $("#updateprocessName").val()
      },
      cache: false,
      success: function(res) {
        swal({
          position: 'center',
          type: 'success',
          title: 'Process successfully updated',
          showConfirmButton: false,
          timer: 1500
        });
        $("#updateProcessmodal").modal('hide');
        $(".processTrigger").trigger("click");

      }
    });
  }

  function saveChangesChecklist(checklistid) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_checklist",
      data: {
        checklist_ID: checklistid,
        checklistTitle: $("#updatechecklistName").val()
      },
      cache: false,
      success: function(res) {
        swal({
          position: 'center',
          type: 'success',
          title: 'checklist successfully updated',
          showConfirmButton: false,
          timer: 1500
        });
        $("#updateChecklistmodal").modal('hide');
        $(".checklistTrigger").trigger("click");
      }
    });
  }
  $("#procheck_type_select").on("change", function() {
    $("#checkrow-perpage").change();
  });
  $("#searchChecklist").keyup(function() {
    $("#checkrow-perpage").change();
  })

  function runChecklist() {
    var pid = $("#processtemp_id").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_anotherchecklist",
      data: {
        process_ID: pid,
        checklistTitle: $("#addchecklistName").val()
      },
      cache: false,
      success: function(res) {
        $('#run_checklist-modal').modal('hide');
        $("#prorow-perpage").change();
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added checklist',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
  }

  function runChecklist_forcl() {
    var process_ID = $("#processtemp_id").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_checklistdetails_cl",
      data: {
        checklist_remarks: $("#addchecklistName_e").val(),
        assignees: $("#names_assignee_cl").val().toString(),
        process_ID: process_ID
      },
      cache: false,
      success: function(res) {
        swal({
          position: 'center',
          type: 'success',
          title: 'Checklist On the run!',
          showConfirmButton: false,
          timer: 1500
        });
        $("#run_checklist-modal-coaching").modal('hide');
        $("#prorow-perpage").change();
        $("#checkrow-perpage").change();
      }
    });
  }

  //DATATABLE
  function reinitAssigneeDatatable() {
    $('#assigned_memberdisplay').mDatatable('destroy');
    assigneeDatatable.init($('#assigneeSearch').val(), $('#assign_membersmodal').data('id'), $('#assign_membersmodal').data('type'));
  }
  $('#assigneeSearch').on('keyup', function() {
    reinitAssigneeDatatable();
  })

  function initAssignee(element) {
    $('#assigned_memberdisplay').mDatatable('destroy');
    $('#assign_membersmodal').data('type', $(element).data('type'));
    $('#assign_membersmodal').data('id', $(element).data('id'));
    assigneeDatatable.init($('#assigneeSearch').val(), $(element).data('id'), $(element).data('type'));
  }
  var assigneeDatatable = function() {
    var assignee = function(searchVal, folProcIdVal, typeval) {
      var options = {
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'POST',
              url: baseUrl + "/process/list_assignee_datatable",
              params: {
                query: {
                  folProcId: folProcIdVal,
                  type: typeval,
                  assigneeSearch: searchVal
                },
              },
            }
          },
          saveState: {
            cookie: false,
            webstorage: false
          },
          pageSize: 5,
          serverPaging: true,
          serverFiltering: true,
          serverSorting: true,
        },
        layout: {
          theme: 'default', // datatable theme
          class: '', // custom wrapper class
          scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
          height: 550, // datatable's body's fixed height
          footer: false // display/hide footer
        },
        sortable: true,
        pagination: true,
        toolbar: {
          // toolbar placement can be at top or bottom or both top and bottom repeated
          placement: ['bottom'],

          // toolbar items
          items: {
            // pagination
            pagination: {
              // page size select
              pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
            },
          }
        },
        search: {
          input: $('#assigneeSearch'),
        },
        rows: {
          afterTemplate: function(row, data, index) {},
        },
        // columns definition
        columns: [{
          field: "fname",
          title: "Employee Name",
          width: 180,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var html = row.fname + " " + row.lname;
            return html;
          },
        }, {
          field: "acc_name",
          title: "Team",
          width: 160,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var html = row.acc_name;
            return html;
          },
        }, {
          field: "lname",
          title: "Access",
          width: 220,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var pt = $("#protype_id").val();
            var rule1 = '';
            var rule2 = '';
            var rule3 = '';
            var rule4 = '';
            var rule5 = '';
            var rule6 = '';
            var rule7 = '';
            var rule8 = '';
            if (row.assignmentRule_ID == 1) {
              rule1 = "selected";
            } else if (row.assignmentRule_ID == 2) {
              rule2 = "selected";
            } else if (row.assignmentRule_ID == 3) {
              rule3 = "selected";
            } else if (row.assignmentRule_ID == 4) {
              rule4 = "selected";
            } else if (row.assignmentRule_ID == 5) {
              rule5 = "selected";
            } else if (row.assignmentRule_ID == 6) {
              rule6 = "selected";
            } else if (row.assignmentRule_ID == 7) {
              rule7 = "selected";
            } else if (row.assignmentRule_ID == 8) {
              rule8 = "selected";
            }
            var html = '';
            if (row.type == "folder" || row.type == "process") {
              html = '<select onchange="access_permissionChange(this)" data-assignid="' + row.assign_ID + '" class="form-control m-input m-input--square">' +
                '<option value="1" ' + rule1 + '>Can Edit, View and Run</option>' +
                '<option value="2" ' + rule2 + '>Can Edit, View Own and Run</option>' +
                '<option value="3" ' + rule3 + '>Can View and Run</option>' +
                '<option value="4" ' + rule4 + '>Can View Own and Run</option>' +
                '<option value="7" ' + rule7 + '>Can View Own</option>' +
                '<option value="8" ' + rule8 + '>Can View All</option>' +
                '</select>';
            } else if (row.type == "checklist" || row.type == "task") {
              if (pt == 2) {
                html = '<select onchange="access_permissionChange(this)" data-assignid="' + row.assign_ID + '" class="form-control m-input m-input--square">' +
                  '<option  disabled value="9" selected="">Coaching Log Assigned</option>' +
                  '</select>';
              } else {
                html = '<select onchange="access_permissionChange(this)" data-assignid="' + row.assign_ID + '" class="form-control m-input m-input--square">' +
                  '<option value="5" ' + rule5 + '>Can Answer</option>' +
                  '<option value="6" ' + rule6 + '>Can Edit and Answer</option>' +
                  '</select>';
              }
            }
            return html;
          },
        }, {
          field: "assign_ID",
          title: "Action",
          width: 90,
          selector: false,
          // sortable: 'asc',
          textAlign: 'left',
          template: function(row, index, datatable) {
            var pt = $("#protype_id").val();
            if (pt == 2) {
              html = '<button onclick="unable_toremove()" data-toggle="m-tooltip" data-id="' + row.assign_ID + '" title="Remove this member" data-original-title="Tooltip title" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="fa fa-remove"></i></button>';
            } else {
              html = '<button onclick="removemember_assignment(this)" data-toggle="m-tooltip" data-id="' + row.assign_ID + '" title="Remove this member" data-original-title="Tooltip title" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="fa fa-remove"></i></button>';
            }
            return html;
          },
        }],
      };
      var datatable = $('#assigned_memberdisplay').mDatatable(options);
    };
    return {
      init: function(searchVal, folProcIdVal, typeval) {
        assignee(searchVal, folProcIdVal, typeval);
      }
    };
  }();

  $('.n_blanktemplate').on('click', function() {
    $.ajax({
      type: "POST",
      url: baseUrl + "/process/get_allowedfolders",
      // data: {
      //     offense_id: $(this).val(),
      //     tofetch: "1"
      // },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res);
        var stringx = "";
        stringx = "<option disabled selected=''>Select a Folder</option>";
        $.each(result, function(key, item) {
          stringx += "<option value=" + item.folder_ID + ">" + item.folderName + "</option>";
        });

        $("#folder_select").html(stringx);
      }
    });

  });


  var getMembersByAccount = function() {
    var acc_id = $("#account").val();
    if (acc_id != null) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>Process/fetch_membersPerAccount_2nd",
        data: {
          acc_id: acc_id,
          assigned_pct: $("#folprotaskcheck_id").val(),
          ass_type: $("#assign_type").val()
        },
        cache: false,
        success: function(res) {
          res = JSON.parse(res.trim());
          $("#members").html("");
          var str = "";
          $.each(res.members, function(key, obj) {
            str += '<option value="' + obj.uid + '" class="text-capitalize">' + obj.lname + ' ' + obj.fname + '</option>';
          });
          $("#members").html(str);
          $('#members').bootstrapDualListbox('refresh');
        }
      });
    }
  };

  $("#assign_membersmodal").on('hide.bs.modal', function() {
    $("#account").val('');
    // $('#members option:selected').each(function() {
    //   $(this).prop('selected', false);
    // });
    $("#members").html('');
    $('#members').bootstrapDualListbox('refresh');
    $("#myTable").html("");

    $('.add_empnamez').removeClass("show active");
    $('.added_empnamez').addClass("show active");
    $('#tab-addmember').removeClass("show active");
    $('#tab-member').addClass("show active");
  })

  $("#prorow-perpage").change(function() {
    var perPage = $("#prorow-perpage").val();
    processtemp_display(0, perPage, function(total) {
      $("#prorow-pagination").pagination('destroy');
      $("#prorow-pagination").pagination({
        items: total, //default
        itemsOnPage: $("#prorow-perpage").val(),
        hrefTextPrefix: "#",
        cssStyle: 'light-theme',
        displayedPages: 10,
        onPageClick: function(pagenumber) {
          var perPage = $("#prorow-perpage").val();
          processtemp_display((pagenumber * perPage) - perPage, perPage, function() {});
        }
      });
    });
  });
  $("#checkrow-perpage").change(function() {
    var perPage = $("#checkrow-perpage").val();
    checklists_display(0, perPage, function(total) {
      $("#checkrow-pagination").pagination('destroy');
      $("#checkrow-pagination").pagination({
        items: total, //default
        itemsOnPage: $("#checkrow-perpage").val(),
        hrefTextPrefix: "#",
        cssStyle: 'light-theme',
        displayedPages: 10,
        onPageClick: function(pagenumber) {
          var perPage = $("#checkrow-perpage").val();
          checklists_display((pagenumber * perPage) - perPage, perPage, function() {});
        }
      });
    });
  });

  $('[href="#processtab"]').click(function() {
    $('#prorow-perpage').change();
    fetch_active_processtype(2); //yyy
  });
  $('[href="#checklisttab"]').click(function() {
    $('#checkrow-perpage').change();
    fetch_active_processtype(3); //yyy
  });

  $("#add_newtbutton").click(function() {
    fetch_active_processtype(1);
  });

  function fetch_active_processtype(toinsert) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>Process/fetch_processtype",
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        // var stringx = "<option disabled selected=''>Template Type</option>";
        var stringx = "<option value=''>All</option>";
        $.each(result, function(key, item) {
          stringx += "<option value=" + item.processType_ID + ">" + item.processType + "</option>";
        });
        // if add template btn 
        if (toinsert == 1) {
          $("#templatetype_select").html(stringx);
        } else if (toinsert == 2) {
          $("#proc_type_select").html(stringx);
        } else {
          $("#procheck_type_select").html(stringx);
        }
      }
    })
  }

  function display_accountsforcl() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_account",
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx = "<option disabled selected=''>Select a Team</option><option value='0'>All Employees</option>";
        $.each(result, function(key, item) {
          stringx += "<option value=" + item.acc_id + ">" + item.acc_name + "</option>";
        });
        $("#team_names").html(stringx);
      }
    });
  }
  $("#team_names").change(function() {
    // $("#names_assignee_cl").selectpicker("destroy");
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_membersPerAccount",
      data: {
        acc_id: $("#team_names").val()
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx = "";
        $.each(result.members, function(key, item) {
          stringx += "<option value=" + item.uid + ">" + item.fname + " " + item.lname + "</option>";
        });
        $("#names_assignee_cl").html(stringx);
        select_mul();
        $('#names_assignee_cl').selectpicker('refresh');
      }
    });
  })

  function select_mul() {
    $('#names_assignee_cl').selectpicker({
      title: "Select Names",
      liveSearch: true,
      liveSearchStyle: 'contains',
      selectedTextFormat: "count",
      countSelectedText: '{0} Subordinates',
      // container: ".m-content",
      // dropupAuto: 'true',
      // virtualScroll: 'true',
      size: 'auto'
    });
  }
  $(".add_empnamez").click(function() {
    show_lists_toassign();
  });
  $("#ass_empnames").change(function() {
    getaccmembers('single');
  });

  function show_lists_toassign() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_empsnotassigned",
      data: {
        assigned_pct: $("#folprotaskcheck_id").val(),
        ass_type: $("#assign_type").val()
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx = "<option disabled selected=''>Select a Name</option>";
        $.each(result.members, function(key, item) {
          stringx += "<option value=" + item.uid + ">" + item.lname + ", " + item.fname + "</option>";
        });
        $("#ass_empnames").html(stringx);
      }
    });
  }
  // $('#checkrow,#prorow').on('click', '.portletLink', function() {
  //   window.location.href = $(this).data('linkpage');
  // })

  // NEW CODES for IRecruit 

  // if copy link is clicked 
$('#prorow').on('click', '.copytemplink', function() {
    $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/copy_thislink",
        data: {
            process_id: $(this).data('id')
        },
        cache: false,
        success: function(res) {
            var result = JSON.parse(res.trim());
            copyToClipboard(result);
            toastr.success("Successfully copied link to clipboard.");
        }
    });
});

function copyToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}
</script>