<style>
    th,
    td {
        white-space: nowrap;
    }

    .first-col {
        position: absolute;
        width: 20em;
        margin-left: -20em;
        margin-top: -1px;
        background: #d4d4d4b3;
    }

    .table-wrapper {
        overflow-x: scroll;
        margin: 0 0 0 20rem;
        border: 1px solid #d2d2d2;
    }

    .bootstrap-select.btn-group .dropdown-menu.inner {
        max-height: 250px !important;
    }

    .progress {
        position: relative;
    }

    .progress span {
        position: absolute;
        display: block;
        width: 100%;
        color: black;
    }


    .table-bordered td {
        border: 1px solid #dadada;
    }

    p:before {
        content: '';
        display: inline-block;
        width: 20px;
        height: 20px;
        //background:red;
        float: left;
    }

    .uploadedFiles:hover {
        text-decoration: none;
    }


    /* to be removed */
    .coachingLogicons {
        color: #2196F3 !important;
    }

    .checklistcolor {
        background: #4cc34a54;
        border-right: 7px solid #4CAF50;
    }

    .coachingLogcolor {
        background: #3f51b54a;
        border-right: 7px solid #673AB7;
    }

    .pro-name {
        color: #416d90;
        font-size: 0.9em;
        font-weight: bold;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader" style="background: #6d6d6d;padding-bottom: 7rem;">
        <div class=" align-items-center">
            <div class="mr-auto">
                <h2 class="m-subheader__title m-subheader__title--separator" style="color: white;"><i class="fa fa-file-text-o mr-2" style="font-size: 1.5rem;"></i><span id="checklistTitle" s></span></h2>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('/process') ?>" class="m-nav__link" id="backLink">
                            <span class="m-nav__link-text text-light">Back to List</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-12 text-md-right" style="color: white;font-size: 0.8rem;" id="checklistDates">
            <!-- <span class="">Run last January 21, 2019 12:00:02 PM</span><br>
            <span class="">Confirmed last January 29, 2019 12:00:02 PM</span> -->
        </div>
    </div>
    <div class="m-content pt-0 pl-5 pr-5 ml-5 mr-5" style="margin-top: -6rem;">
        <div class="m-portlet mt-0 ml-5 mr-5">
            <div class="m-portlet__body  m-portlet__body--no-padding" style="box-shadow: 0 1px 15px 1px rgba(69,65,78,.08);">
                <div class="row m-row--no-padding m-row--col-separator-xl" id="coachingLogDetails">
                    <!-- <div class="col-12 p-3" style="background: #f1f1f1;border-bottom: 1px solid #bbbbbb;">
                        <div class="row">
                            <div class="col-12 col-md-9">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4 col-lg-3"><img src="http://3.18.221.50/sz/uploads/profiles/szprofile_207.jpg" onerror="noImageFound(this)" width="75" alt="" class="rounded-circle mx-auto d-block" id="annexSubPic"></div>
                                    <div class="col-sm-12 col-md-8 col-lg-9 pl-sm-0">
                                        <div class="row">
                                            <div class="col-12 col-md-12 mb-1 text-center text-md-left" style="font-size: 19px;" id="annexSubName">Jaycebel Dungog Rulona</div>
                                            <div class="col-md-12 text-center text-md-left" style="font-size: 13px;margin-top: -3px;font-weight: bolder;color: darkcyan;" id="annexSubPosition">Organizational Development Supervisor<span style="font-size: 11px;color: #0012ff;" id="annexSubPositionStat"></span></div>
                                            <div class="col-md-12 mb-1 text-center text-md-left" style="font-size: 12px" id="annexSubAccount">HR Systems and Organizational Development Team</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-3">
                                <div class="row mx-2 mt-md-0 mt-2" style="border-style: dashed !important;border: 2px #ff9393;background: #ffdada;height: 100%;align-items: center;">
                                    <div class="col-12 text-center" style="font-size: 1.5rem;">
                                        Missed To Confirm
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 p-4">
                        <h3>TASK TITLE HERE</h3>
                        <div class="col-12 py-3">
                            <div class="col-12 text-center" style="font-size: 2rem;">
                                Sample Header Here
                            </div>
                            <h5>Subtask title here</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <div class="col-12 mb-4 pl-5 pr-5">
                                <figure style="margin:0 !important;text-align:center"><img src="http://3.18.221.50/sz/uploads/process/form_images/1572874074.jpg" class="gambar img-responsiv" id="item-img-output274" style="background:white;width:50%!important;object-fit:cover;"></figure>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <div class="col-12" style="padding: 1rem 5rem 1rem 5rem;">
                                <div class="row iframepadding">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <video width="320" height="240" controls="" __idm_id__="379351041">
                                            <source src="http://3.18.221.50/sz/uploads/process/form_videos/1572880108sharap.mp4" type="video/mp4">
                                            <source src="http://3.18.221.50/sz/uploads/process/form_videos/1572880108sharap.mp4" type="video/m4v">
                                            <source src="http://3.18.221.50/sz/uploads/process/form_videos/1572880108sharap.mp4" type="video/avi">
                                            <source src="http://3.18.221.50/sz/uploads/process/form_videos/1572880108sharap.mp4" type="video/mpg">
                                            <source src="http://3.18.221.50/sz/uploads/process/form_videos/1572880108sharap.mp4" type="video/mov">
                                            <source src="http://3.18.221.50/sz/uploads/process/form_videos/1572880108sharap.mp4" type="video/mpg">
                                            <source src="http://3.18.221.50/sz/uploads/process/form_videos/1572880108sharap.mp4" type="video/mpeg">
                                        </video>
                                    </div>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <div class="row px-3 pt-2 pb-4">
                                <div class="col-md-3"></div>
                                <div class="col-12 col-md-6 text-center p-2" style="border: 2px dashed #03a9f46b;background: #03a9f41c;">
                                    <div href="#" class="">
                                        <span><i class="fa fa-file-text-o mr-2"></i><span>DOC</span></span>
                                    </div>
                                    <div style="font-size:1.1em;color:#7e61b1">1572879982ero manual proofread.docx</div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row px-3 pt-2 pb-4">
                                <div class="col-md-3"></div>
                                <div class="col-12 col-md-6 text-center p-2" style="border: 2px dashed #f443367d;background: #f443360d;">
                                    <div href="#" class="">
                                        <span><i class="fa fa-file-text-o mr-2"></i><span>PPT</span></span>
                                    </div>
                                    <div style="font-size:1.1em;color:#7e61b1">1572879982ero manual proofread.ppt</div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <div class="row px-3 pt-2 pb-4">
                                <div class="col-md-3"></div>
                                <div class="col-12 col-md-6 text-center p-2" style="border: 2px dashed #4caf5082;background: #4caf500d;">
                                    <div href="#" class="">
                                        <span><i class="fa fa-file-text-o mr-2"></i><span>EXCEL</span></span>
                                    </div>
                                    <div style="font-size:1.1em;color:#7e61b1">1572879982ero manual proofread.xlx</div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                    <div class="col-12 px-4 py-0">
                        <hr style="margin-top: 0.8rem !important;">
                    </div>
                    <div class="col-12 p-4">
                        <h3>TASK TITLE HERE</h3>
                        <h5>Subtask title here</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/process/coaching_log.js"></script>