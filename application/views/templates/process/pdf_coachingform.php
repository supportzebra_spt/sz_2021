<style>
    .fa {
        font-family: fontawesome;
        font-style: normal;
        color: #555;

    }
</style>
<!-- 1 inch and 1/2 inch-->
<h3 style="text-transform:uppercase;;width:100%;text-align:center">COACHING LOG</h3>
<?php foreach ($details as $tasks) : ?>
    <h4 style="text-transform:uppercase;"><!--<i class="fa">&#xf111;</i>--> <span style="color:#222"><?php echo $tasks['task_title']; ?></span></h4>
    
    <?php if ($tasks['sub_tasks'] == null) : ?>
        <table style="width:100%;margin:0;padding:0;border-collapse: collapse;">
            <tr>
                <td style="border: 1px solid #ddd;padding:8px;;background:#efefef">
                    <h5 style="color:gray;font-style:italic">No data to display</h5>
                </td>
            </tr>
        </table>
    <?php else : ?>
        <?php foreach ($tasks['sub_tasks'] as $subtask) : ?>

            <?php if ($subtask['type'] == 'divider') : ?>
                <hr style="border-color:#aaa;">
                <br>
            <?php elseif ($subtask['type'] == 'heading') : ?>
                <h4 style="color:#6C97CC;font-weight:bold;margin:20px 0;font-size:15px;text-transform:capitalize"><?php echo $subtask['label']; ?></h4>
            <?php elseif ($subtask['type'] == 'file') : ?>
                <table style="width:100%;margin:10px 0;padding:0;border-collapse: collapse;">
                    <tr>
                        <td style="border: 1px solid #ddd;padding:8px;;text-transform:uppercase;font-size:12px;background:#efefef;font-weight:bold">
                            <?php echo $subtask['label']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding:8px;font-size:12px;">
                            <?php echo $subtask['answer']; ?>
                        </td>
                    </tr>
                </table>
            <?php elseif ($subtask['type'] == 'image') : ?>
                <?php if ($subtask['label'] !== null && $subtask['answer'] !== null) : ?>

                    <table style="width:100%;margin:10px 0;padding:0;border-collapse: collapse;">
                        <tr>
                            <td style="border: 1px solid #ddd;padding:8px;;text-transform:uppercase;font-size:12px;background:#efefef;font-weight:bold">
                                <?php echo $subtask['label']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid #ddd;padding:16px 8px;font-size:12px;;">
                                <?php echo $subtask['answer']; ?>
                            </td>
                        </tr>
                    </table>
                <?php else : ?>
                    <table style="width:100%;margin:10px 0;padding:0;border-collapse: collapse;">
                        <tr>
                            <td style="border: 1px solid #ddd;padding:16px 8px;;text-transform:uppercase">
                                <?php if ($subtask['label'] === null) : ?>
                                    <?php echo $subtask['answer']; ?>
                                <?php elseif ($subtask['answer'] === null) : ?>
                                    <?php echo $subtask['label']; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                <?php endif; ?>
            <?php else : ?>
                <table style="width:100%;margin:10px 0;padding:0;border-collapse: collapse;">
                    <tr>
                        <td style="border: 1px solid #ddd;padding:8px;;text-transform:uppercase;font-size:12px;background:#efefef;font-weight:bold">
                            <?php echo $subtask['label']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding:8px;font-size:12px">
                            <?php echo $subtask['answer']; ?>
                        </td>
                    </tr>
                </table>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <br>
    <div style="border-top:1px dashed #aaa;width:100%;"></div>
    <br>
<?php endforeach; ?>