<style type="text/css">
  .topdistance {
    margin-top: 15px;
  }
  .coached_namez{
    font-weight: 600;
    color: cadetblue;
  }
  .placeholdercolor::placeholder {
    color: #d8dce8;
    opacity: 1;
  }

  .iframepadding {
    padding-left: 15px !important;
    padding-right: 15px !important;
  }

  .droparea_outside {
    border-style: dashed;
    padding: 10px;
    margin: 10px;
    border-color: #a5a6ad;
  }

  .droparea_append {
    padding: 10px;
    margin: 10px;
  }

  .buttonform {
    text-align: center;
  }

  .distance {
    margin-bottom: 35px;
  }

  .formdistance {
    margin-top: 10px;
  }

  .taskdistance {
    margin-top: 15px;
  }

  .label {
    font-size: 1.2em;
    color: #00c5dc;
  }

  .inputcolor {
    background: #e5e6ef;
  }

  .bodytextcolor {
    color: black;
  }

  .label {
    font-size: 1.2em;
    color: #0992a2;
  }

  .labelheading {
    font-size: 1.4em;
    font-weight: bold;
    color: #00c5dc;
  }

  .butpadding {
    padding: 45px;
  }

  .load-style {
    width: 30px;
    display: none;
    margin-top: 45%;
    margin-left: 45%;
  }

  .load-style-task {
    width: 30px;
    display: none;
    margin-top: 25%;
    margin-left: 45%;
  }

  .sublabel-color {
    color: #6a6c71;
  }

  .padding_formbutton {
    padding-left: 45px;
    padding-top: 20px;
  }

  .paddingtop {
    padding-top: 20px;
  }

  .dotted_border {
    padding: 15px;
    border: 2px dashed gainsboro;
  }

  .sublabel-detail {
    color: #5a5c61;
    font-size: 0.9em;
  }

  .radiopadding {
    padding-left: 9%;
    padding-bottom: 5%;
  }

  .nolabel_class {
    color: #777;
    font-size: 0.9em;
  }

  /* NEW CLASSES */
  .underline_class {
    text-decoration: line-through;
    font-style: italic;
    color: white;
    background-color: rgb(81, 175, 249);
  }

  .not_underlined_class {
    /* background-color: rgb(81, 175, 249); */
    color: white;
    text-decoration: none;
  }

  #checkgroup>.input-group.m-form__group:hover {
    border: 1px solid #c1c1c1;
    border-style: dashed;
  }
  .inputtask:hover {
    cursor: pointer;

  }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo base_url('process') ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">
          </li>
          <li id="link_pendingpage" class="m-nav__item">           
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet">
          <!--begin: Portlet Head-->
          <div class="m-portlet__head pt-3 pl-1 pb-3 pr-3" id="h_id" style="background: #e6e6e6;background: #505a6b;">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <!-- <div class="col-12"> -->
                <div class="row m-portlet__head-text">
                  <div class="col-12">

                    <h5 style="color:white">
                      <i class="fa fa-check-square-o text-success" style="font-size: 1.5rem;"></i> <strong><?php echo $checklist->checklistTitle; ?></strong></h5>
                  </div>
                  <?php if ($checklist->dueDate != NULL) { ?>
                    <div class="col-12">
                      <h6 style="color:white">
                        <span style="font-size:0.9em;"><strong style="color:#34bfa3"> &nbsp;Checklist Due Date &nbsp;</strong>
                          <?php
                            $datetime = $checklist->dueDate;
                            $date = DateTime::createFromFormat("Y-m-d H:i:s", $datetime);
                            echo sprintf($date->format("D, M j, Y")) . "&nbsp; at &nbsp;";
                            echo sprintf($date->format("g:ia"));
                            ?>
                        </span>
                      </h6>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="m-portlet__head-tools">
              <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                  <div id="update_checklist_button">
                  </div>
                </li>
                <!-- removed check all button  -->
                <!-- <li class="m-portlet__nav-item" id="appendbutton">
                </li> -->
              </ul>
            </div>
          </div>
          <!--end: Portlet Head-->
          <div class="m-portlet__body m-portlet__body--no-padding">
            <div class="m-wizard m-wizard--4 m-wizard--brand m-wizard--step-first" id="m_wizard">
              <!-- <div class="row m-row--no-padding">
                <div class="col-12">
                 
                </div>
              </div> -->
              <div class="row m-row--no-padding">
                <div id="div1" class="col-xl-6 col-lg-12 border dueDate">
                  <?php $alltask = count($task); ?>
                  <input type="hidden" id="user_id" value="<?php echo $user; ?>">
                  <input type="hidden" id="countTask" value="<?php echo $alltask; ?>">
                  <input type="hidden" id="checklistID" value="<?php echo $checklist->checklist_ID; ?>">
                  <input type="hidden" id="processID" value="<?php echo $processinfo->process_ID; ?>">
                  <input type="hidden" id="display_id">
                  <input type="hidden" id="process_permission">
                  <input type="hidden" id="folder_permission">
                  <input type="hidden" id="checklist_permission">
                  <div class="col-xl-12" id="progressBrr" style="margin-top: 1em;margin-bottom: 2em;">
                  </div>
                  <hr>
                  <div class="m-portlet__body py-3 first_scroll" style="height:500px;overflow-y:scroll; overflow-x:auto;background-color:">
                    <div class="m-loader m-loader--info loading-icon load-style-task"></div>
                    <!-- All tasks will be placed -->
                    <div class="form-group m-form__group topdistance" id="checkgroup">
                      <br><br>
                    </div>
                  </div>
                </div>

                <!-- <form id="formtaskid"> -->
                
                <div id="f" class="col-xl-6 col-lg-12 border">
                    <div id="duedate_display" class="col-xl-12 p-0 m-2 text-center">
                </div>
                    
                <!-- <div class="col-12" style="height:480px;overflow-y:scroll; overflow-x:auto;background: #00bcd40f;"> -->
                      <div id="contentdiv" class="col-12 p-0">
                        
                      </div>
                    <!-- </div> -->
                    <!-- <div class="col-12">
                      <div class="row p-3" style="border-top: 1px solid #dadada;">
                        <span id="content_buttondiv" class="col">
                        </span>
                      </div>
                    </div> -->
                </div>
                <!-- </form> -->
                <!-- <div class='col-5'>
                <div class='summernote' id='m_summernote_1' style='display: none;'></div>
              </div> -->
                <div id="div3" class="col-xl-2 col-lg-12 border" style="visibility: hidden;display: none">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                        <h6 class="m-portlet__head-text">
                          Menu
                        </h6>
                      </div>
                    </div>
                  </div>
                  <div class="m-portlet__body">
                    <div class="row">
                      <button type="button" class="btn btn-success btn-sm">Complete Checklist</button>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-12">
                        <ul class="list-inline text-center align-items-center">
                          <li class="list-inline-item align-middle"><a href="<?php echo base_url('process/checklist_update') . '/' . $checklist->checklist_ID . '/' . $processinfo->process_ID; ?>">Edit this Template</a></li><br>
                          <hr>
                          <li class="list-inline-item align-middle"><a href="">Delete this Template</a></li><br>
                          <hr>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="add_file-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-subtaskid="">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload a File</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-12">
            <label class="btn btn-accent btn-sm btn-block"><i class="fa fa-paperclip"></i>Upload your file here.
              <form id="formFilez" enctype="multipart/form-data">
                <input type="file" style="display: none;" name="upload_files" id="upload_files">
              </form>
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--end::Modal-->
<script type="text/javascript">
  var timer = null;
  var userid = $("#user_id").val();
  var cid = $("#checklistID").val();
  var prid = $("#processID").val();
  var status = "";
  var a = 0;
  $(function() {
    // getassignment_permission();
    display_tasks_answerable_new(function(result) {
      // check_uncheck_button_appearance();
    });
    SummernoteDemo.init();

    $('#ass_empnames').select2({
              placeholder: "Select Names",
              width: '100%'
      });
    // customPopoverDatePicker("#custom-checklist-duedate");
    // var dualListBox_members = $('#members').bootstrapDualListbox({
    //   nonSelectedListLabel: '<span class="m--font-bolder m--font-danger">Non-selected:</span>',
    //   selectedListLabel: '<span class="m--font-bolder m--font-success">Selected Employee:</span>',
    //   selectorMinimalHeight: 150
    // });
    // var dualListContainer = dualListBox_members.bootstrapDualListbox('getContainer');
    // dualListContainer.find('.moveall i').removeClass().addClass('fa fa-arrow-right');
    // dualListContainer.find('.removeall i').removeClass().addClass('fa fa-arrow-left');
    // dualListContainer.find('.move i').removeClass().addClass('fa fa-arrow-right');
    // dualListContainer.find('.remove i').removeClass().addClass('fa fa-arrow-left');

    // var $image_crop, tempFilename, rawImg, imageId;
    // // init_task();
    // var mouseCopy = $.extend({}, $.ui.mouse.prototype);

    // $.extend($.ui.mouse.prototype, {
    //   _mouseInit: function() {
    //     var that = this;
    //     if (!this.options.mouseButton) {
    //       this.options.mouseButton = 1;
    //     }

    //     mouseCopy._mouseInit.apply(this, arguments);

    //     if (this.options.mouseButton === 3) {
    //       this.element.bind("contextmenu." + this.widgetName, function(event) {
    //         if (true === $.data(event.target, that.widgetName + ".preventClickEvent")) {
    //           $.removeData(event.target, that.widgetName + ".preventClickEvent");
    //           event.stopImmediatePropagation();
    //           return false;
    //         }
    //         event.preventDefault();
    //         return false;
    //       });
    //     }

    //     this.started = false;
    //   },
    //   _mouseDown: function(event) {

    //     // we may have missed mouseup (out of window)
    //     (this._mouseStarted && this._mouseUp(event));

    //     this._mouseDownEvent = event;

    //     var that = this,
    //       btnIsLeft = (event.which === this.options.mouseButton),
    //       // event.target.nodeName works around a bug in IE 8 with
    //       // disabled inputs (#7620)
    //       elIsCancel = (typeof this.options.cancel === "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false);
    //     if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) {
    //       return true;
    //     }

    //     this.mouseDelayMet = !this.options.delay;
    //     if (!this.mouseDelayMet) {
    //       this._mouseDelayTimer = setTimeout(function() {
    //         that.mouseDelayMet = true;
    //       }, this.options.delay);
    //     }

    //     if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
    //       this._mouseStarted = (this._mouseStart(event) !== false);
    //       if (!this._mouseStarted) {
    //         event.preventDefault();
    //         return true;
    //       }
    //     }

    //     // Click event may never have fired (Gecko & Opera)
    //     if (true === $.data(event.target, this.widgetName + ".preventClickEvent")) {
    //       $.removeData(event.target, this.widgetName + ".preventClickEvent");
    //     }

    //     // these delegates are required to keep context
    //     this._mouseMoveDelegate = function(event) {
    //       return that._mouseMove(event);
    //     };
    //     this._mouseUpDelegate = function(event) {
    //       return that._mouseUp(event);
    //     };
    //     $(document)
    //       .bind("mousemove." + this.widgetName, this._mouseMoveDelegate)
    //       .bind("mouseup." + this.widgetName, this._mouseUpDelegate);

    //     event.preventDefault();

    //     mouseHandled = true;
    //     return true;
    //   }
    // });

  });
  $('h3#appendbutton').on('click', '#checkall', function() {
    checkall();
  });
  var SummernoteDemo = function() {
    //== Private functions
    var demos = function() {
      $('.summernote').summernote({
        height: 150
      });
    }
    return {
      // public functions
      init: function() {
        demos();
      }
    };
  }();
  //NOTIFS
  function doNotif() {
    $.notify("Auto-saved!");
    setTimeout(function() {
      $.notifyClose('top-right');
    }, 1000);
  }

  function doNotif2() {
    $.notify("Saved!");
    setTimeout(function() {
      $.notifyClose('top-right');
    }, 1000);
  }
  $('#m_datepicker_1').datepicker({
    todayHighlight: true,
    orientation: "bottom left",
    templates: {
      leftArrow: '<i class="la la-angle-left"></i>',
      rightArrow: '<i class="la la-angle-right"></i>'
    }
  });

  //  function check_ifchecklist_owned(){
  //    $.ajax({
  //      type: "POST",
  //      url: "<?php echo base_url(); ?>process/check_ifowned",
  //      data: {
  //       checklist_ID: cid
  //     },
  //     cache: false,
  //     success: function (res) {
  //       var result = JSON.parse(res.trim());
  //       var data=result;
  //       var createdby=data.createdBy;
  //       if(createdby==userid){
  //         $("#edit_checklistbutton").attr("hidden", false);
  //         console.log("Checklist owned");
  //       }
  //     }
  //   });
  //  }



  //  Get Permission for template - Can Edit or Not

  // NEW checker
  function getassignment_permission() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_access_new2",
      data: {
        checklist_ID: cid,
        process_ID: prid
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringxx_check = "";
        var stringxx_link = "";
        var stringxx_names ="";

        stringxx_link +="<a href='<?php echo base_url(); ?>process/checklist_pending/" + prid + "' class='m-nav__link'><span>- <?php echo $processinfo->processTitle; ?></span></a>";
        // if(result.checklist_comp=="1"){
        //   stringxx_check += "<button type='button' id='edit_checklistbutton' class='btn btn-info btn-sm updatetemp_btn' onclick='restricted_update()' data-toggle='m-tooltip' data-placement='top' title data-original-title='Edit Checklist Template'><span><i class='fa fa-pencil'></i></span></button>";
        // }else{
        // }
        stringxx_check += "<a id='edit_checklistbutton' href='<?php echo base_url(); ?>process/checklist_update/" + cid + "/" + prid + "' class='btn btn-info btn-sm' data-toggle='m-tooltip' data-placement='top' title data-original-title='Edit Checklist Template'><span><i class='fa fa-pencil'></i></span></a>";

        if (result.process_edit_access == "1") {
          $("#update_checklist_button").html(stringxx_check);
        } else if (result.process_permission == "1") {
          if (result.process_details['0'].assignmentRule_ID == "1") {
            $("#update_checklist_button").html(stringxx_check);
          }
        } else if (result.checklist_permission == "1") {
          if (result.checklist_details['0'].assignmentRule_ID == "6") {
            $("#update_checklist_button").html(stringxx_check);
          }
        } else if (result.task_permission == "1") {
          // $.each(result.task_details, function (key, item) {

          // }
        }

        if(result.checklist_pending_access=="1"){
          $("#link_pendingpage").html(stringxx_link);
        }    
        if(result.process_type=="2"){
          $("#coached_per").show();
          $(".first_scroll").css("height", "694px");
          $(".sec_scroll").css("height", "640px");
          stringxx_names +='<div class="col-12 col-md-12 px-2 mb-1 text-center text-md-left" style="font-size: 13px;">';
          stringxx_names +='<span class="coached_namez">Name: </span>&nbsp;&nbsp;'+result.coached_person_details.fname+' '+result.coached_person_details.mname+' '+result.coached_person_details.lname;
          stringxx_names +='</div>';
          stringxx_names +='<div class="col-12 col-md-12 px-2 mb-1 text-center text-md-left" style="font-size: 13px;">';
          stringxx_names +='<span class="coached_namez">Employee Status: </span>&nbsp;&nbsp;'+result.coached_person_details.status;
          stringxx_names +='</div>';
          stringxx_names +='<div class="col-12 col-md-12 px-2 mb-1 text-center text-md-left" style="font-size: 13px;">';
          stringxx_names +='<span class="coached_namez">Team/Department: </span>&nbsp;&nbsp;'+result.coached_person_details.acc_name;
          stringxx_names +='</div>';
          $("#coached_per").html(stringxx_names);
        }
      }
    });
  }
  //  function getassignment_permission(){
  //    $.ajax({
  //      type: "POST",
  //      url: "<?php echo base_url(); ?>process/fetch_access",
  //      cache: false,
  //      success: function (res) {
  //        var result = JSON.parse(res.trim());

  //       //  if admin
  //        if(result.length>0){
  //          $("#edit_checklistbutton").show();
  //          $("#checkall").attr("hidden", false);
  //          displayid=1;
  //          $("#display_id").val(displayid);
  //          console.log("I'm an Admin!");
  //        }else{
  //         //  Not an Admin
  //          $.ajax({
  //            type: "POST",
  //            url: "<?php echo base_url(); ?>process/fetch_assignment_task",
  //            data: {
  //             checklist_ID: cid,
  //             process_ID:prid
  //           },
  //           cache: false,
  //           success: function (res) {
  //            var result = JSON.parse(res.trim());

  //            if(result.length>0){
  //             $.each(result, function (key, item) {
  //               var pr=item.processrule;
  //               var fr=item.folderrule;
  //               var cr=item.checkrule;
  //               var tr=item.taskrule;   

  //               if(fr!=null){
  //                $("#folder_permission").val(fr);
  //                if(fr=="1" || fr=="2"){
  //                 $("#edit_checklistbutton").attr("hidden", false);
  //               }
  //             }
  //             if(pr!=null){              
  //               $("#process_permission").val(pr);
  //               if((pr=="1" || fr=="2")){
  //                 $("#edit_checklistbutton").attr("hidden", false);
  //               }
  //             }
  //             if(cr!=null){
  //               $("#checklist_permission").val(cr);
  //               if(cr=="1" || cr=="2"){
  //                 $("#edit_checklistbutton").attr("hidden", false);
  //               }
  //             }
  //             if(tr!=null){
  //               if(tr=="6"){
  //                 $("#edit_checklistbutton").attr("hidden", false);
  //                 $("#checkall").remove();
  //               }
  //             }
  //             check_ifchecklist_owned();    
  //           });
  //           }else{
  //             console.log("no permission");
  //           } 
  //         }
  //       });
  //        }
  //      }
  //    });
  //  }


  function check_taskassignment() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_asstask",
      data: {
        checklist_ID: cid,
      },
      cache: false,
      success: function(res) {
        var rt = JSON.parse(res.trim());
        if (rt.length > 0) {
          $.each(rt, function(key, data) {
            $("#inputbox_id" + data.folProCheTask_ID).attr("disabled", false);
            $("#checkbox_id" + data.folProCheTask_ID).attr("disabled", false);
            $("#badge-assigned" + data.folProCheTask_ID).append("<li class='m-nav__item'><span class='m-badge m-badge--brand m-badge--wide'>Assigned Task</span></li>");
          });
        } else {
          $(".inputtask").attr("disabled", false);
          $(".check_st").attr("disabled", false);
          $(".badge-ass").append("<li class='m-nav__item'><span class='m-badge m-badge--metal m-badge--wide'>Not Assigned Task</span></li>");
        }
      }
    });
  }

  function get_subtask(taskid, callback) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_allsubtask",
      data: {
        task_ID: taskid
      },
      beforeSend: function() {
        $('.loading-icon').show();
      },
      cache: false,
      success: function(res) {
        $('.loading-icon').hide();
      }
    });
  }

  // NEW 
  // Changes : removed class and disabled for the meantime
  //Changes : removed onchange='changecheckbox(this)' on input checkbox
  function display_tasks_answerable_new(callback) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/display_tasks_answerable",
      data: {
        checklist_ID: cid,
      },
      beforeSend: function() {
        $('.loading-icon').show();
      },
      cache: false,
      success: function(res) {
        $('.loading-icon').hide();
        var result = JSON.parse(res.trim());
        var stringx = "";
        var sduedate = "";
        var stringbutton = "";
        var stringhidden = "";
        var taskdd = "";
        var stringprogress ="";
        var label_pbr="";
        var progress_cont="";
        // PROGRESS BAR 

        if(result.all == result.completed){
          progress_cont += "<div id='progress_width_pbr' class='progress-bar progress-bar bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>";
          label_pbr="Completed";
        }else if(result.completed==0){
          progress_cont += "<div id='progress_width_pbr' class='progress-bar progress-bar-striped progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:0%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
          label_pbr="Progress";
        }else{
          var percentage_pending = (result.completed/ result.all) * 100;
          progress_cont += '<div id="progress_width_pbr" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width:'+percentage_pending+'%" data-toggle="m-tooltip" data-placement="top" data-original-title="'+result.completed+'/'+result.all+'"></div>';
          label_pbr="Progress";
        }

        stringprogress += '<div class="text-center"><label style=""><span id="label_for_pbr" style="font-weight: bold;">'+label_pbr+':</span>&nbsp;&nbsp;<span id="num_completed_pbr" style="color: darksalmon;font-weight: bold;">'+result.completed+'</span> / <span id="num_all_pbr" style="color: #34bfa3;font-weight: bold;">'+result.all+'</span></label></div>';
        stringprogress += '<div class="col-xl-9 p-0" style="margin:auto;">';
        stringprogress += '<div class="progress">';
        stringprogress += progress_cont;
        stringprogress += '</div>';
        stringprogress += '</div>';

        $.each(result.chtask, function(key, item) {

          // if Task not yet checked
          if (item.taskdueDate == null) {
            taskdd = "";
          } else {
            taskdd = item.taskdueDate;
          }
          if (item.isCompleted == 2) {
            stringx += "<div class='input-group m-form__group mb-1 class_tdd' id='addcheckbox" + item.checklistStatus_ID + "' data-tdd='" + taskdd + "'>";
            stringx += "<div class='input-group-prepend'>";
            stringx += "<span class='input-group-text'>";
            // stringx+="<label class='m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-accent'>";
            stringx += "<label>";
            // stringx+="<input type='checkbox' data-checklist='"+cid+"' data-task='"+item.task_ID+"' class='c_boxx' data-id='"+item.checklistStatus_ID+"' id='checkbox_id"+item.checklistStatus_ID+"'>";
            stringx += "<span></span>";
            stringx += "</label>";
            stringx += "</span>";
            stringx += "</div>";
            // if task title is null or empty
            if (item.taskTitle == null) {
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' type='text' class='form-control crashout inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='No Task Label!' readonly='readonly'>";
            } else {
              //If task title has label
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' type='text' class='form-control crashout inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='" + item.taskTitle + "' readonly='readonly'>";
            }
            stringx += "<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='" + item.process_ID + "'>";
            stringx += "<input type='hidden' id='staskID' name='staskID' value='" + item.checklistStatus_ID + "'>";

            stringx += "<div class='input-group-append'>";
            stringx += "<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
            stringx += "<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
            stringx += "<i class='fa fa-delete'></i>";
            stringx += "</a>";
            stringx += "<div class='m-dropdown__wrapper'>";
            stringx += "<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
            stringx += "<div class='m-dropdown__inner'>";
            stringx += "<div class='m-dropdown__body'>";
            stringx += "<div class='m-dropdown__content'>";
            stringx += "<ul id='badge-assigned" + item.checklistStatus_ID + "' class='badge-ass m-nav'>";
            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";

          } else if (item.isCompleted == 3) {
            //  if task checked
            stringx += "<div class='input-group m-form__group class_tdd' id='addcheckbox" + item.checklistStatus_ID + "' data-tdd='" + taskdd + "'>";
            stringx += "<div class='input-group-prepend'>";
            stringx += "<span class='input-group-text'>";
            //  stringx+="<label class='m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-success'>"
            stringx += "<label>";
            //  stringx+="<input type='checkbox' data-checklist='"+cid+"' data-task='"+item.task_ID+"' class='c_boxx' data-id='"+item.checklistStatus_ID+"' id='checkbox_id"+item.checklistStatus_ID+"' checked>";
            stringx += "<span></span>";
            stringx += "</label>";
            stringx += "</span>";
            stringx += "</div>";
            if (item.taskTitle == null) {
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' style='' type='text' class='form-control inputtask underline_class' placeholder='Empty...' aria-label='Text input with checkbox' value='No Task Label!' readonly='readonly'>";
            } else {
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' style='' type='text' class='form-control inputtask underline_class' placeholder='Empty...' aria-label='Text input with checkbox' value='" + item.taskTitle + "' readonly='readonly'>";
            }
            stringx += "<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='" + item.process_ID + "'>";
            stringx += "<input type='hidden' id='staskID' name='staskID' value='" + item.checklistStatus_ID + "'>";

            stringx += "<div class='input-group-append'>";
            stringx += "<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
            stringx += "<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
            stringx += "<i class='fa fa-delete'></i>";
            stringx += "</a>";
            stringx += "<div class='m-dropdown__wrapper'>";
            stringx += "<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
            stringx += "<div class='m-dropdown__inner'>";
            stringx += "<div class='m-dropdown__body'>";
            stringx += "<div class='m-dropdown__content'>";
            stringx += "<ul id='badge-assigned" + item.checklistStatus_ID + "' class='badge-ass m-nav'>";
            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
          }
          callback(res);
        });
        //END OF FOREACH

        $("#checkgroup").html(stringx);
        $("#progressBrr").html(stringprogress);
        $("#duedate_display").html(sduedate);
        a = $('.checkedC:checked').length;

        $(".inputtask:first").css({
          "background-color": "#51aff9",
          "color": "white"
        });
        $(".inputtask:first").addClass("placeholdercolor");
        var firstaskid = $(".inputtask:first").attr('data-taskid');
        var firstcheckstatid = $(".inputtask:first").attr('data-id');
        var firstdd = $(".class_tdd:first").attr('data-tdd');
        var d = "";
        if (firstdd != "") {
          // d = moment(firstdd).format("ddd, MMM Do YYYY, h:mm:ss a");
          var dateObj = new Date(firstdd);
          var momentObj = moment(dateObj);
          // var momentString = momentObj.format('llll');
          var momentString = momentObj.format('dddd, MMMM DD, YYYY');
          // sduedate += '<div class="col-12 py-1 text-center"><label style="font-size:0.9em; margin-bottom:unset!important"> <i class="fa fa-calendar-check-o"></i><strong style="color:#34bfa3;"> Task Due Date&nbsp;</strong>' + momentString + '</label></div>';
          sduedate += '<label style="font-size:0.9em; margin-bottom:unset!important"> <i class="fa fa-calendar-check-o"></i><strong style="color:#34bfa3;"> Task Due Date&nbsp;</strong>' + momentString + '</label>';
          // sduedate += '<div class="col-2"><button type="button" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" onclick="display_assign_duedate('+firstaskid+')" ><i class="fa fa-angle-double-down"></i></button></div>';
          $("#duedate_display").show();
        }else{
          $("#duedate_display").hide();
        }
        $("#duedate_display").html(sduedate);
        display_allcomponents(firstaskid, firstcheckstatid, function(result) {
          getassignment_permission();
          dynamicProcessStrValidation(firstaskid, firstcheckstatid);
        });
        displayduedate(firstaskid);
        $(".formcontent:first").show();
        $("#id-cs").val(firstcheckstatid);
        check_taskassignment();
      }
    });
  }

  // To Check and uncheck function
  //OLD : onclick= changecheckbox
  $('#checkgroup').on('click', '.c_boxx', function() {
    var checkstattask_id = $(this).data('id');
    var checklist_id = $(this).data('checklist');
    change_checkstatus(checkstattask_id, checklist_id, function(res) {
      check_if_allchecked(checklist_id);
    });
  });


  // Check if all tasks are checked
  function check_if_allchecked(checklist_id) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/checklist_completedchecker",
      data: {
        check_ID: checklist_id,
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringxx = "";
        if (result.completed == "1") {
          swal({
            position: 'top-center',
            type: 'success',
            title: 'Successfully Completed This Checklist!',
            showConfirmButton: false,
            timer: 1500
          });
        }else if(result.completed == "3") {
          swal({
            position: 'top-center',
            type: 'success',
            title: 'Successfully Completed This Checklist!',
            showConfirmButton: false,
            timer: 1500
          });
          window.location.reload(true);
          // window.location.href = "<?php echo base_url('process/coaching_log') ?>" + "/" + cid;
        }
        // $("#appendbutton").html(stringxx);
      }
    })
  }

  function check_uncheck_button_appearance() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/checklist_completedchecker",
      data: {
        check_ID: cid,
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringxx = "";
        if (result.completed == "1") {
          stringxx += "<button id='uncheckall_btn' class='btn btn-metal btn-sm m-btn m-btn--icon'>";
          stringxx += "<span><i class='fa fa-repeat'></i>";
          stringxx += "<span>Unchecked all checkboxes</span>";
          stringxx += "</span>";
          stringxx += "</button>";
        } else {
          stringxx += "<button id='checkall_btn' onclick='' class='btn btn-success btn-sm complete_check_btn'>";
          stringxx += "<span><i class='fa fa-check mr-1'></i>";
          stringxx += "<span>Complete this checklist</span>";
          stringxx += "</span>";
          stringxx += "</button>";
        }
        // $("#appendbutton").html(stringxx);
      }
    })
  }
  // Function to change checklist status
  function change_checkstatus(checkstattask_id, checklistid, callback) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/task_change_checkbox",
      data: {
        checklistStat_ID: checkstattask_id
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx_complete = "";
        if (result.check_status == "not checked") {
          //Underline
          $("#checkbox_id" + checkstattask_id).prop("checked", true);
          $("#inputbox_id" + checkstattask_id).addClass("underline_class");
          $("#inputbox_id" + checkstattask_id).removeClass("not_underlined_class");
          stringx_complete += "<button type='submit'  data-checklist='" + checklistid + "' data-ctid='" + checkstattask_id + "' id='completetask" + checkstattask_id + "' class='btn btn-metal m-btn  m-btn m-btn--icon detect_unbutton pull-right'>Uncheck this task</span></span></button>";
          $("#content_buttondiv").html(stringx_complete);
        } else {
          //Remove underline
          $("#checkbox_id" + checkstattask_id).prop("checked", false);
          $("#inputbox_id" + checkstattask_id).addClass("not_underlined_class");
          $("#inputbox_id" + checkstattask_id).removeClass("underline_class");
          stringx_complete += "<button type='submit'  data-checklist='" + checklistid + "' data-ctid='" + checkstattask_id + "' id='completetask" + checkstattask_id + "' class='btn btn-success m-btn detect_button pull-right triggerz'>Check this task</button>";
          $("#content_buttondiv").html(stringx_complete);
        }
        changeof_progressbar(checklistid);
        callback(res)
      }
    });
  }
  function changeof_progressbar(checklist_ID){
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_status_progressbar",
      data: {
        checklist_id: checklist_ID
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var number_pbr=0;
        if(result.all == result.completed){
          number_pbr="100%";
          $("#label_for_pbr").text("Completed:");
          $("#progress_width_pbr").removeClass("progress-bar-striped");
        }else if(result.completed==0){
          number_pbr="0%";
          $("#label_for_pbr").text("Progress:");
        }else{
          var percentage_pending = (result.completed/ result.all) * 100;
          number_pbr=percentage_pending+"%";
          $("#progress_width_pbr").addClass("progress-bar-striped");
          $("#label_for_pbr").text("Progress:");
        }
        $("#progress_width_pbr").css("width", number_pbr);
        $("#num_completed_pbr").text(result.completed);
        $("#num_all_pbr").text(result.all);
      }
    });
  } 
  function changeof_button() {
    var checktask_id=$("#h_id").attr("data-ctid");
    var checklist_id=$("#h_id").attr("data-checklist");
    change_checkstatus(checktask_id, checklist_id, function(res) {
      check_if_allchecked(checklist_id);
    });
  }

  $('#contentdiv').on('click', '.detect_button', function() {
    var checktask_id = $(this).data('ctid');
    var checklist_id = $(this).data('checklist');
    $("#h_id").attr("data-ctid", checktask_id);
    $("#h_id").attr("data-checklist", checklist_id);
  })

  $('#contentdiv').on('click', '.triggerz', function(e) {
    e.preventDefault();
    var checktask_id = $(this).data('ctid');
    var checklist_id = $(this).data('checklist');
    console.log($(this).data('ctid'));
    console.log($(this).data('checklist'));
    change_checkstatus(checktask_id, checklist_id, function(res) {
    check_if_allchecked(checklist_id);
    });
  })

  // Get ID from 'Complete this task' button
  $('#contentdiv').on('click', '.detect_unbutton', function(e) {
    e.preventDefault();
    var checktask_id = $(this).data('ctid');
    var checklist_id = $(this).data('checklist');
    change_checkstatus(checktask_id, checklist_id, function(res) {
      check_if_allchecked(checklist_id);
    });
  });

  $('#appendbutton').on('click', '#checkall_btn', function() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_taskAllStatus",
      data: {
        checklist_ID: cid,
        stat: 3
      },
      cache: false,
      success: function(res) {
        var stringxx = "";
        stringxx += "<button id='uncheckall_btn' class='btn btn-metal btn-sm m-btn m-btn--icon'>";
        stringxx += "<span><i class='fa fa-repeat'></i>";
        stringxx += "<span>Unchecked all checkboxes</span>";
        stringxx += "</span>";
        stringxx += "</button>";
        // $("#appendbutton").html(stringxx);
        $(".inputtask").removeClass("not_underlined_class");
        $(".inputtask").addClass("underline_class");
        swal({
          position: 'top-center',
          type: 'success',
          title: 'Successfully Completed This Checklist!',
          showConfirmButton: false,
          timer: 1500
        });
      }
    });
  });

  $('#appendbutton').on('click', '#uncheckall_btn', function() {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_taskAllStatus",
      data: {
        checklist_ID: cid,
        stat: 2
      },
      cache: false,
      success: function(res) {
        var stringxx = "";
        stringxx += "<button id='checkall_btn' onclick='' class='btn btn-success btn-sm complete_check_btn'>";
        stringxx += "<span><i class='fa fa-check mr-1'></i>";
        stringxx += "<span>Complete this checklist</span>";
        stringxx += "</span>";
        stringxx += "</button>";
        // $("#appendbutton").html(stringxx);
        $(".inputtask").removeClass("underline_class");
        $(".inputtask").addClass("not_underlined_class");
      }
    });
  });

  // Complete this task button add and change

  function completethistask_button(checklistStatusId, checklistid) {
    console.log('ADD BTN HERE');
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/Checktask_status",
      data: {
        checklist_ID: checklistStatusId,
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        var stringx_complete = "";
        if (result.check_status == "not checked") {
          stringx_complete += "<button type='submit' data-checklist='" + checklistid + "' data-ctid='" + checklistStatusId + "' id='completetask" + checklistStatusId + "' class='btn btn-success m-btn  m-btn m-btn--icon detect_button pull-right'>Check this task</button>";
        } else {
          stringx_complete += "<button type='submit' data-checklist='" + checklistid + "' data-ctid='" + checklistStatusId + "' id='completetask" + checklistStatusId + "' class='btn btn-metal m-btn  m-btn m-btn--icon detect_unbutton pull-right'>Uncheck this task</span></span></button>";
        }
        $("#content_buttondiv").html(stringx_complete)
      }
    });
  }


  // NEW END

  //OLD
  function display_tasks_answerable(callback) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/display_tasks_answerable",
      data: {
        checklist_ID: cid,
      },
      beforeSend: function() {
        $('.loading-icon').show();
      },
      cache: false,
      success: function(res) {
        $('.loading-icon').hide();
        var result = JSON.parse(res.trim());
        var stringx = "";
        var stringcon = "";
        var stringbutton = "";
        var stringhidden = "";

        $.each(result, function(key, item) {
          if (item.isCompleted == 2) {
            // Task not checked
            stringx += "<div class='input-group m-form__group' id='addcheckbox'>";
            stringx += "<div class='input-group-prepend'>";
            stringx += "<span class='input-group-text'>";
            stringx += "<label class='m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-accent'>"
            stringx += "<input type='checkbox' data-task='" + item.task_ID + "' class='check_st checkedC c_boxx' data-id='" + item.checklistStatus_ID + "' onchange='changecheckbox(this)' id='checkbox_id" + item.checklistStatus_ID + "' disabled>";
            stringx += "<span></span>";
            stringx += "</label>";
            stringx += "</span>";
            stringx += "</div>";
            if (item.taskTitle == null) {
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' type='text' class='form-control crashout inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='No Task Label!' readonly='readonly' disabled>";
            } else {
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' type='text' class='form-control crashout inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='" + item.taskTitle + "' readonly='readonly' disabled>";
            }
            stringx += "<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='" + item.process_ID + "'>";
            stringx += "<input type='hidden' id='staskID' name='staskID' value='" + item.checklistStatus_ID + "'>";

            stringx += "<div class='input-group-append'>";
            stringx += "<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
            stringx += "<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
            stringx += "<i class='fa fa-delete'></i>";
            stringx += "</a>";
            stringx += "<div class='m-dropdown__wrapper'>";
            stringx += "<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
            stringx += "<div class='m-dropdown__inner'>";
            stringx += "<div class='m-dropdown__body'>";
            stringx += "<div class='m-dropdown__content'>";
            stringx += "<ul id='badge-assigned" + item.checklistStatus_ID + "' class='badge-ass m-nav'>";
            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";

            stringx += "</div>";

          } else if (item.isCompleted == 3) {
            // Task checked
            stringx += "<div class='input-group m-form__group' id='addcheckbox'>";

            stringx += "<div class='input-group-prepend'>";
            stringx += "<span class='input-group-text'>";
            stringx += "<label class='m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-success'>"
            stringx += "<input type='checkbox' data-task='" + item.task_ID + "' class='check_st checkedC c_boxx' data-id='" + item.checklistStatus_ID + "' onchange='changecheckbox(this)' id='checkbox_id" + item.checklistStatus_ID + "' disabled checked>";
            stringx += "<span></span>";
            stringx += "</label>";
            stringx += "</span>";
            stringx += "</div>";

            if (item.taskTitle == null) {
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' style='text-decoration: line-through;font-style: italic;color:#8e8585;' type='text' class='form-control inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='No Task Label!' readonly='readonly' disabled>";
            } else {
              stringx += "<input data-taskid='" + item.task_ID + "' data-id='" + item.checklistStatus_ID + "' id='inputbox_id" + item.checklistStatus_ID + "' onclick='displayformdiv(this)' style='text-decoration: line-through;font-style: italic;color:#8e8585;' type='text' class='form-control inputtask' placeholder='Empty...' aria-label='Text input with checkbox' value='" + item.taskTitle + "' readonly='readonly' disabled>";
            }
            stringx += "<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='" + item.process_ID + "'>";
            stringx += "<input type='hidden' id='staskID' name='staskID' value='" + item.checklistStatus_ID + "'>";

            stringx += "<div class='input-group-append'>";
            stringx += "<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
            stringx += "<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
            stringx += "<i class='fa fa-delete'></i>";
            stringx += "</a>";
            stringx += "<div class='m-dropdown__wrapper'>";
            stringx += "<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
            stringx += "<div class='m-dropdown__inner'>";
            stringx += "<div class='m-dropdown__body'>";
            stringx += "<div class='m-dropdown__content'>";
            stringx += "<ul id='badge-assigned" + item.checklistStatus_ID + "' class='badge-ass m-nav'>";
            stringx += "</ul>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";
            stringx += "</div>";

            stringx += "</div>";
          }
          callback(res);
        });
        //END OF FOREACH
        $("#checkgroup").html(stringx);
        a = $('.checkedC:checked').length;
        $(".inputtask:first").css({
          "background-color": "#51aff9",
          "color": "white"
        });
        $(".inputtask:first").addClass("placeholdercolor");

        var firstaskid = $(".inputtask:first").attr('data-taskid');
        var firstcheckstatid = $(".inputtask:first").attr('data-id');
        display_allcomponents(firstaskid, firstcheckstatid, function(result) {
          getassignment_permission();
          dynamicProcessStrValidation(firstaskid, firstcheckstatid);
        });
        $("#contentdiv").append(stringcon);
        $(".formcontent:first").show();
        $("#id-cs").val(firstcheckstatid);
        check_taskassignment();
      }
    });
  }

  //display answers
  function checkOnetask() {
    var csid = $("#id-cs").val();
    $("#checkbox_id" + csid).prop("checked", true);

    var ct = $("#countTask").val();
    if ($("#checkbox_id" + csid).is(':checked')) {
      status = "3";
      $("#inputbox_id" + csid).css({
        "text-decoration": "line-through",
        "font-style": "italic",
        "color": "#8e8585"
      });
      a++;
    }
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_taskStatus",
      data: {
        checklistStatus_ID: csid,
        isCompleted: status
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
    if (a == ct) {
      status = "3";
      console.log("completed");
      checkcompleted();
    } else {
      console.log("incomplete");
      checknotcomplete();
    }
  }

  function display_allcomponents(taskid, checklistStatusId, callback) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_allsubtask1",
      data: {
        task_ID: taskid,
        checklist_ID: cid
      },
      cache: false,
      success: function(res) {
     
        var result = JSON.parse(res.trim());
        var stringx = "";
        var stringcon = "";
        var stringbutton = "";
        stringcon += "<form id='formtag" + taskid + "' data-checkliststatusid='" + checklistStatusId + "'>";
        stringcon +='<div class="col-xl-12 px-1" id="coached_per" style="padding-top: 0.6rem;padding-bottom: 0.6rem;box-shadow: 0 1.5px 0 0 rgba(44, 44, 45, 0.08);border-bottom: 1px solid gainsboro;display:none">';
        stringcon +='</div>';
        stringcon += "<div class='col-12 px-3 sec_scroll' style='height:535px;overflow-y:scroll; overflow-x:auto;background: #bbc5e226;'>";
        stringcon += '<div class="m-loader m-loader--info loading-icon load-style"></div>';
        stringcon += "<div id='formaddcontent" + taskid + "' class='m-portlet__body formcontent' style='padding: 2rem 0.2rem 2rem 0.2rem !important;'>";
        stringcon += "<input type='hidden' class='hiddentask' value='" + checklistStatusId + "'>";
        stringcon += "</div>";
        stringcon += "<div id='button_complete'>";
        stringcon += "</div>";
        stringcon += "</div>";
        stringcon += '<div class="col-12">';
        stringcon += '<div class="row p-3" style="border-top: 1px solid #dadada;">';
        stringcon += '<span id="content_buttondiv" class="col">';
        stringcon += '</span>';
        stringcon += '</div>';
        stringcon += '</div>';
        stringcon += "</form>";

        var dd = $("#addcheckbox" + checklistStatusId).attr('data-tdd');
        var sduedate = "";
        var d = "";

        var dateObj = new Date(dd);
        var momentObj = moment(dateObj);
        var momentString = momentObj.format('dddd, MMMM DD, YYYY');
        if (dd != "") {
          // sduedate += '<div class="col-12 py-1 text-center"><label style="font-size:0.9em; margin-bottom:unset!important"> <i class="fa fa-calendar-check-o"></i><strong style="color:#34bfa3;"> Task Due Date&nbsp;</strong>' +  momentString+ '</label></div>';
          sduedate += '<label style="font-size:0.9em; margin-bottom:unset!important"> <i class="fa fa-calendar-check-o"></i><strong style="color:#34bfa3;"> Task Due Date&nbsp;</strong>' + momentString + '</label>';    
          // sduedate += '<div class="col-2"><button type="button" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" onclick="display_assign_duedate('+taskid+')"><i class="fa fa-angle-double-down"></i></button></div>';
          $("#duedate_display").show();
        }else{
          $("#duedate_display").hide();
        }
        $("#duedate_display").html(sduedate);
        $("#contentdiv").html(stringcon);
        $('#f').find('form').attr('id', 'formtag' + taskid);
        $('#f').find('form').data('checkliststatusid', checklistStatusId);

        // stringbutton +="<div class='col-12 butpadding'>";
        // stringbutton +="<hr><button type='submit' id='completetask' onclick='checkOnetask()' class='btn btn-success m-btn  m-btn m-btn--icon'><span><i class='fa fa-check'></i><span>Complete this task</span></span></button>&nbsp;&nbsp;";
        // stringbutton +="<input type='hidden' id='id-cs'>";

        // $("#button_complete").html(stringbutton);
        // stringbutton +="</div>";

        //----

        if(result.length == 0){
          stringx+='<div style="text-align: center;margin-top:12em">';
          stringx+='<i class="flaticon-open-box" style="font-size: 4em;"></i><br>';
          stringx+='<span style="font-weight: 500;">No Form Content Available</span><br>';
          stringx+='<span>Update this template to add contents here</span></div>';
        }else{
          $.each(result, function(key1, item1) {
          $.each(item1, function(key, item) {
            //INPUT TEXT
            if (item.component_ID == "1") {
              stringx += "<div id='" + item.subTask_ID + "'class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel) {

                if (item.required == "yes") {
                  stringx += item.compLabel + "<span style='color:red'>*</span>";
                } else {
                  stringx += item.compLabel;
                }
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Short Text )</span>";
              }
              stringx += "</div>"
              var ph = (item.placeholder == null) ? "" : item.placeholder;
              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<input type='text' data-component='" + item.component_ID + "' placeholder='" + ph + "' value='" + item3.answer + "' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' name='form_component" + item.subTask_ID + "' class='form-control m-input m-input--air m-input--pill formdistance inputcolor classformfield'>";
                });
              } else {
                stringx += "<input type='text' data-component='" + item.component_ID + "' placeholder='" + ph + "' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' name='form_component" + item.subTask_ID + "' class='form-control m-input m-input--air m-input--pill formdistance inputcolor classformfield'>";
              }

              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='formdistance sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //TEXT AREA
            else if (item.component_ID == "2") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Long Text )</span>";
              }
              stringx += "</div>";
              stringx += "<form>";

              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<textarea class='classformfield form-control m-input' rows='13' name='form_component" + item.subTask_ID + "' id='answer" + item.subTask_ID + "' data-component='" + item.component_ID + "' data-id='" + item.subTask_ID + "' class='inputcolor form-control m-input m-input--air m-input--pill'>" + item3.answer;
                  stringx += "</textarea>";
                });
              } else {
                stringx += "<textarea class='classformfield form-control m-input' rows='13' name='form_component" + item.subTask_ID + "' id='answer" + item.subTask_ID + "' data-component='" + item.component_ID + "' data-id='" + item.subTask_ID + "' class='inputcolor form-control m-input m-input--air m-input--pill'>";
                stringx += "</textarea>";
              }
              stringx += "</form>";
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //HEADING
            else if (item.component_ID == "3") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              if (item.text_size != null) {
                var textsize = "";
                if (item.text_size == "Default") {
                  textsize = "1.5em";
                } else if (item.text_size == "Small") {
                  textsize = "1.3em";
                } else if (item.text_size == "Large") {
                  textsize = "2em";
                }
              }
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='labelheading' style='text-align:" + item.text_alignment + ";font-size:" + textsize + "'>";

              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "Heading";
              }
              stringx += "</div>";
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail' style='text-align:" + item.text_alignment + "'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //TEXT
            else if (item.component_ID == "4") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='bodytextcolor'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "You have not yet typed any text in this field.";
              }
              stringx += "</div>";
              stringx += "</div>";
            }
            //NUMBER
            else if (item.component_ID == "5") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Number Field )</span>";
              }
              stringx += "</div>";
              var ph = (item.placeholder == null) ? "" : item.placeholder;

              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<input style='width:100%' name='form_component" + item.subTask_ID + "' value='" + item3.answer + "' type='number' data-component='" + item.component_ID + "' placeholder='" + ph + "' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' class='form-control m-input m-input--air m-input--pill formdistance inputcolor classformfield'>";
                });
              } else {
                stringx += "<input style='width:100%' placeholder='" + ph + "' name='form_component" + item.subTask_ID + "' type='number' data-component='" + item.component_ID + "' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' class='form-control m-input m-input--air m-input--pill formdistance inputcolor classformfield'>";
              }
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //RADIO BUTTON
            else if (item.component_ID == "6") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label formdistance'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Radio Button Label )</span>";
              }
              stringx += "</div>";

              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail'>" + item.sublabel + "</div>";
              }

              stringx += "<div id='radiolist" + item.subTask_ID + "' class='row m-radio-list formdistance'>";
              stringx += "<div class='col-lg-10 radiopadding'>";

              $.each(item.subComponent, function(key2, item2) {
                stringx += "<label id='optiondetails" + item2.componentSubtask_ID + "' class='m-radio m-radio--bold m-radio--state-primary'>";
                if (item2.compcontent != null) {
                  stringx += item2.compcontent;
                } else {
                  stringx += "No option label...";
                }

                    if(item2.checklist_ID==cid){
                      if(item2.answer_ID!=null){
                      stringx += "<input name='form_component" + item.subTask_ID + "' id='radioopt" + item2.componentSubtask_ID + "' data-id='" + item.subTask_ID + "' data-component='" + item.component_ID + "' data-subtask='" + item.subTask_ID + "' type='radio' value='" + item2.compcontent + "' checked>";
                      }else{
                      stringx += "<input name='form_component" + item.subTask_ID + "' id='radioopt" + item2.componentSubtask_ID + "' data-id='" + item.subTask_ID + "' data-component='" + item.component_ID + "' data-subtask='" + item.subTask_ID + "' type='radio' value='" + item2.compcontent + "'>";
                      }
                    }else{
                      stringx += "<input name='form_component" + item.subTask_ID + "' id='radioopt" + item2.componentSubtask_ID + "' data-id='" + item.subTask_ID + "' data-component='" + item.component_ID + "' data-subtask='" + item.subTask_ID + "' type='radio' value='" + item2.compcontent + "'>";
                    }      
                stringx += "<span></span>";
                stringx += "</label>";
              });
              stringx += "</div>";
              stringx += "</div>";
              stringx += "</div>";
            }
            //DROPDOWN
            else if (item.component_ID == "7") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Dropdown Label )</span>";
              }
              stringx += "</div>";
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail' >" + item.sublabel + "</div>";
              }
              stringx += "<div class='form-group m-form__group'>";
              stringx += "<select name='form_component" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' data-component='" + item.component_ID + "' id='itemlist" + item.subTask_ID + "' class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1'>";

              $.each(item.subComponent, function(key2, item2) {

                if ((item.answer_details).length > 0) {
                  $.each(item.answer_details, function(key3, item3) {
                    if (item2.compcontent == item3.answer) {
                      stringx += "<option selected='selected'>";
                      if (item2.compcontent != null) {
                        stringx += item2.compcontent;
                      } else {
                        stringx += "Item list...";
                      }
                      stringx += "</option>";
                    } else {
                      stringx += "<option>";

                      if (item2.compcontent != null) {
                        stringx += item2.compcontent;
                      } else {
                        stringx += "Item list...";
                      }
                      stringx += "</option>";
                    }
                  });
                } else {
                  stringx += "<option>";
                  if (item2.compcontent != null) {
                    stringx += item2.compcontent;
                  } else {
                    stringx += "Item list...";
                  }
                  stringx += "</option>";
                }
              });
              stringx += "</select>";
              stringx += "</div>";

              stringx += "<div class='form-group m-form__group formdistance'>";
              stringx += "<ul  class='list-group'>";
              stringx += "</ul>";
              stringx += "</div>";
              stringx += "</div>";
            }
            //Checkbox
            else if (item.component_ID == "8") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Multi-select Label )</span>";
              }
              stringx += "</div>";
              //------

              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "<div id='checkoption" + item.subTask_ID + "' class='row m-checkbox-list formdistance'>";

              //----------

              $.each(item.subComponent, function(key2, item2) {
                var a_display="";
                var checked_h="";
         
                if (item2.compcontent != null) {
                  a_display= item2.compcontent;
                } else {
                  a_display= "No option label...";
                }

                stringx += "<div class='col-lg-10'>";
                stringx += "<label id='optiondetails" + item2.componentSubtask_ID + "' data-id='" + item2.componentSubtask_ID + "' class='m-checkbox m-checkbox--bold m-checkbox--state-brand'>";
                
                if(item2.checklist_ID==cid){  
                  if(item2.answer_ID!=null){
                    stringx += "<input id='checkBoxinput" + item2.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + item.subTask_ID + "' name='checkboxanswer' data-component='" + item.component_ID + "' type='checkbox' data-check='" + item2.componentSubtask_ID + "' value='" + a_display + "' checked>";
                  }else{
                   stringx += "<input id='checkBoxinput" + item2.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + item.subTask_ID + "' name='checkboxanswer' data-component='" + item.component_ID + "' type='checkbox' data-check='" + item2.componentSubtask_ID + "' value='" + a_display + "'>"; 
                  }             
                }else{
                  stringx += "<input id='checkBoxinput" + item2.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + item.subTask_ID + "' name='checkboxanswer' data-component='" + item.component_ID + "' type='checkbox' data-check='" + item2.componentSubtask_ID + "' value='" + a_display + "'>"; 
                }
                // $.each(item.answer_details, function(key3, item3) {
                //   if (item2.compcontent == item3.answer) {
                     
                //   }else{
                //       stringx += "<input id='checkBoxinput" + item2.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + item.subTask_ID + "' name='checkboxanswer' data-component='" + item.component_ID + "' type='checkbox' data-check='" + item2.componentSubtask_ID + "' value='" + item2.compcontent + "'>";
                //   }
                //      });
               
               
                stringx += item2.compcontent;
                //===
                stringx += "<span></span>";
                stringx += "</label>";
                stringx += "</div>";
            
              });
              // $.each(item.subComponent, function(key2, item2) {
              //   stringx += "<div class='col-lg-10'>";
              //   stringx += "<label id='optiondetails" + item2.componentSubtask_ID + "' data-id='" + item2.componentSubtask_ID + "' class='m-checkbox m-checkbox--bold m-checkbox--state-brand'>";

              //   if ((item.answer_details).length > 0) {
              //     $.each(item.answer_details, function(key3, item3) {
              //       if (item2.compcontent == item3.answer) {
              //         stringx += "<input id='checkBoxinput" + item2.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + item.subTask_ID + "' name='checkboxanswer' data-component='" + item.component_ID + "' type='checkbox' data-check='" + item2.componentSubtask_ID + "' value='" + item2.compcontent + "' checked>";
              //       } else {
              //         stringx += "<input id='checkBoxinput" + item2.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + item.subTask_ID + "' name='checkboxanswer' data-component='" + item.component_ID + "' type='checkbox' data-check='" + item2.componentSubtask_ID + "' value='" + item2.compcontent + "'>";
              //       }
              //     });
              //   } else {
              //     stringx += "<input id='checkBoxinput" + item2.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + item.subTask_ID + "' name='checkboxanswer' data-component='" + item.component_ID + "' type='checkbox' data-check='" + item2.componentSubtask_ID + "' value='" + item2.compcontent + "'>";
              //   }
              //   if (item2.compcontent != null) {
              //     stringx += item2.compcontent;
              //   } else {
              //     stringx += "No label...";
              //   }
              //   stringx += "<span></span>";
              //   stringx += "</label>";
              //   stringx += "</div>";
              // });
              stringx += "</div>";
              stringx += "</div>";
            }
            //DIVIDER
            else if (item.component_ID == "9") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 distance'>";
              stringx += "<hr>";
              stringx += "</div>";
            }
            //DATE PICKER
            else if (item.component_ID == "10") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "No Date label";
              }
              stringx += "</div>";
              stringx += "<div class='input-group formdistance'>";
              var ph = (item.placeholder == null) ? "" : item.placeholder;
              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<input name='form_component" + item.subTask_ID + "' placeholder='" + ph + "' type='text' data-component='" + item.component_ID + "' value='" + item3.answer + "' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' onmouseover='fetchdatepicker(this)' data-id='" + item.subTask_ID + "' name='date' id='getdate" + item.subTask_ID + "' id='' aria-describedby='m_datepicker-error' aria-invalid='false'>";
                });
              } else {
                stringx += "<input name='form_component" + item.subTask_ID + "' placeholder='" + ph + "' type='text' data-component='" + item.component_ID + "' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' onmouseover='fetchdatepicker(this)' data-id='" + item.subTask_ID + "' name='date' id='getdate" + item.subTask_ID + "' id='' aria-describedby='m_datepicker-error' aria-invalid='false'>";
              }
              stringx += "<div class='input-group-append'>";
              stringx += "<span class='input-group-text'>";
              stringx += "<i class='la la-calendar-check-o'></i>";
              stringx += "</span>";
              stringx += "</div>";
              stringx += "</div>";
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //NUMBER SPINNER
            else if (item.component_ID == "11") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Number Spinner Label )</span>";
              }
              stringx += "</div>";
              var ph = (item.placeholder == null) ? "" : item.placeholder;

              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<input style='width:100%' placeholder='" + ph + "' name='form_component" + item.subTask_ID + "' value='" + item3.answer + "' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' class='form-control m-input m-input--air m-input--pill formdistance classformfield' type='number' value='0' min='" + item.min + "' max='" + item.max + "' step='1'/>";
                });
              } else {
                stringx += "<input style='width:100%' placeholder='" + ph + "' name='form_component" + item.subTask_ID + "' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' class='form-control m-input m-input--air m-input--pill formdistance classformfield' type='number' value='0' min='" + item.min + "' max='" + item.max + "' step='1'/>";
              }

              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='formdistance sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //EMAIL
            else if (item.component_ID == "12") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Email Label )</span>";
              }
              stringx += "</div>";
              // var ph = (item.placeholder == null) ? "" : item.placeholder;

              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<input name='form_component" + item.subTask_ID + "' value='" + item3.answer + "' type='email' data-component='" + item.component_ID + "' placeholder='Type email here' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' class='form-control m-input m-input--air m-input--pill inputcolor classformfield'>";
                });
              } else {
                stringx += "<input name='form_component" + item.subTask_ID + "' type='email' data-component='" + item.component_ID + "' placeholder='" + item.placeholder + "' id='answer" + item.subTask_ID + "' data-id='" + item.subTask_ID + "' placeholder='Type email here' class='form-control m-input m-input--air m-input--pill inputcolor classformfield'>";
              }

              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='formdistance sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //TIME PICKER
            else if (item.component_ID == "13") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Time Label )</span>";
              }
              stringx += "</div>";
              var ph = (item.placeholder == null) ? "" : item.placeholder;
              stringx += "<div class='input-group formdistance'>";

              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<input name='form_component" + item.subTask_ID + "' placeholder='" + ph + "' value='" + item3.answer + "' data-component='" + item.component_ID + "' type='text' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' onmouseover='time_getvalsdisz(this)' data-id='" + item.subTask_ID + "' id='gettime" + item.subTask_ID + "' aria-describedby='m_datepicker-error' aria-invalid='false'>";
                // stringx += "<input name='form-component"+item.subTask_ID+"' onmouseover='time_getvalsdisz(this)' data-component='"+item.component_ID+"'  data-id='"+item.subTask_ID+"' id='gettime"+item.subTask_ID+"' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' placeholder='"+ph+"' value='" + item3.answer + "'>";
                });
              } else {
                // stringx += "<input name='form-component"+item.subTask_ID+"' onmouseover='time_getvalsdisz(this)' data-id='"+item.subTask_ID+"' id='gettime"+item.subTask_ID+"' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' placeholder='"+ph+"'>";
                stringx += "<input name='form_component" + item.subTask_ID + "' placeholder='" + ph + "' data-component='" + item.component_ID + "' type='text' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' onmouseover='time_getvalsdisz(this)' data-id='" + item.subTask_ID + "' name='date' id='gettime" + item.subTask_ID + "' aria-describedby='m_datepicker-error' aria-invalid='false'>";
              }
              stringx += "<div class='input-group-append'>";
              stringx += "<span class='input-group-text'>";
              stringx += "<i class='la la-clock-o'></i>";
              stringx += "</span>";
              stringx += "</div>";
              stringx += "</div>";
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='sublabel-detail'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //DATE TIME PICKER
            else if (item.component_ID == "14") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              if (item.compLabel != null) {
                stringx += item.compLabel;
              } else {
                stringx += "<span class='font-bold nolabel_class'>( Untitled Date and Time Label )</span>";
              }
              stringx += "</div>";
              stringx += "<div class='input-group formdistance'>";
              var ph = (item.placeholder == null) ? "" : item.placeholder;

              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  stringx += "<input name='form_component" + item.subTask_ID + "' value='" + item3.answer + "' type='text' data-component='" + item.component_ID + "' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' onmouseover='fetchdatetimepicker(this)' data-id='" + item.subTask_ID + "' name='date' id='getdatetime" + item.subTask_ID + "'placeholder='" + ph + "' id='' aria-describedby='m_datepicker-error' aria-invalid='false'>";
                });
              } else {
                stringx += "<input name='form_component" + item.subTask_ID + "' type='text' data-component='" + item.component_ID + "' class='form-control m-input m-input--air m-input--pill inputcolor classformfield' onmouseover='fetchdatetimepicker(this)' data-id='" + item.subTask_ID + "' name='date' id='getdatetime" + item.subTask_ID + "' placeholder='" + ph + "' id='' aria-describedby='m_datepicker-error' aria-invalid='false'>";
              }
              stringx += "<div class='input-group-append'>";
              stringx += "<span class='input-group-text'>";
              stringx += "<i class='la la-calendar-check-o'></i>";
              stringx += "</span>";
              stringx += "</div>";
              stringx += "</div>";

              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='formdistance sublabel-detail' style='font-size:0.9em;color:#b9bbc1'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //FILE UPLOADER
            else if (item.component_ID == "15") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance paddingtop dotted_border'>";
              stringx += "<div class='row showOutputFileUpload" + item.subTask_ID + "'>";

              if ((item.answer_details).length > 0) {
                $.each(item.answer_details, function(key3, item3) {
                  var answer_file = item3.answer;
                  var file_type = "";
                  if (answer_file.indexOf('xlsx') != -1) {
                    file_type = "XLSX";
                  } else if (answer_file.indexOf('xls') != -1) {
                    file_type = "XLS";
                  } else if (answer_file.indexOf('zip') != -1) {
                    file_type = "ZIP";
                  } else if (answer_file.indexOf('doc') != -1) {
                    file_type = "DOC";
                  } else if (answer_file.indexOf('mov') != -1) {
                    file_type = "MOV";
                  } else if (answer_file.indexOf('docx') != -1) {
                    file_type = "DOCX";
                  } else if (answer_file.indexOf('ppt') != -1) {
                    file_type = "PPT";
                  } else if (answer_file.indexOf('pdf') != -1) {
                    file_type = "PDF";
                  } else if (answer_file.indexOf('pptx') != -1) {
                    file_type = "PPTX";
                  } else if (answer_file.indexOf('rar') != -1) {
                    file_type = "RAR";
                  } else if (answer_file.indexOf('png') != -1) {
                    file_type = "PNG";
                  } else if (answer_file.indexOf('jpg') != -1) {
                    file_type = "JPG";
                  }
                  answer_file = answer_file.replace("uploads/process/form_dynamic/", " ");
                  stringx += "<div class='col-12 text-center filedisplay_area" + item.subTask_ID + "'>";
                  if (file_type == "PNG" || file_type == "JPG" || file_type == "MOV") {
                    stringx += "<a href='<?php echo base_url() ?>" + item3.answer + "' class='btn btn-outline-brand btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
                    stringx += "<span>";
                    stringx += "<i class='fa fa-file-movie-o'></i>";
                  } else if (file_type == "XLSX" || file_type == "XLS") {
                    stringx += "<a href='<?php echo base_url() ?>" + item3.answer + "' class='btn btn-outline-success btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
                    stringx += "<span>";
                    stringx += "<i class='fa fa-file-excel-o'></i>";
                  } else if (file_type == "ZIP" || file_type == "DOC" || file_type == "DOCX" || file_type == "RAR" || file_type == "PDF") {
                    stringx += "<a href='<?php echo base_url() ?>" + item3.answer + "' class='btn btn-outline-focus btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
                    stringx += "<span>";
                    stringx += "<i class='fa fa-file-text-o'></i>";
                  } else if (file_type == "PPT" || file_type == "PPTX") {
                    stringx += "<a href='<?php echo base_url() ?>" + item3.answer + "' class='btn btn-outline-warning btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
                    stringx += "<span>";
                    stringx += "<i class='fa fa-file-powerpoint-o'></i>";
                  } else {
                    stringx += "<a href='<?php echo base_url() ?>" + item3.answer + "' class='btn btn-outline-primary btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
                    stringx += "<span>";
                    stringx += "<i class='fa fa-file-text-o'></i>";
                  }
                  stringx += "<span>" + file_type + "</span>";
                  stringx += "</span>";
                  stringx += "</a>";

                  stringx += "&nbsp;&nbsp;<a href='#' onclick='remove_fileupload(" + item3.answer_ID + "," + item.subTask_ID + ")'>"
                  stringx += "<i class='fa fa-remove' style='color:#f4516c'></i>"
                  stringx += "</a>&nbsp;";
                  stringx += "<br><div style='font-size:1.1em;color:#7e61b1'>" + answer_file + "</div>";
                  stringx += "<a href='<?php echo base_url() ?>" + item3.answer + "' class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' download>";
                  stringx += "<span><i class='fa fa-download'></i><span>Download this file</span></span>";
                  stringx += "</a>";
                });
              } else {
                stringx += "<div class='col-12  text-center  fileUploadDesc" + item.subTask_ID + "' display>";
                stringx += "<label class='btn btn-outline-success btn-sm m-btn--icon' onclick='showModalFileUpload(" + item.subTask_ID + ")'>";
                stringx += "<span>";
                stringx += "<i class='fa fa-upload'></i>";
                stringx += "<span>Upload a file</span>";
                stringx += '</span>';
                stringx += "</label>";
              }
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='formdistance' style='font-size:0.9em;color:#0992a2'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";


              stringx += "</div>";
              stringx += "</div>";
            }
            //IMAGE UPLOAD
            else if (item.component_ID == "16") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div data-id='" + item.subTask_ID + "' id='label" + item.subTask_ID + "' class='label'>";
              stringx += "<figure style='margin:0 !important;text-align:center'>";
              stringx += "<img src='<?php echo base_url() ?>" + item.compLabel + "' class='gambar img-responsiv ' id='item-img-output" + item.subTask_ID + "' style='background:white;width:80%!important;object-fit:cover;'/>"
              stringx += "</figure>";
              stringx += "</div>"
              //sublabel
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='formdistance' style='font-size:1.1em;color:#0992a2'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            } else if (item.component_ID == "17") {
              stringx += "<div id='" + item.subTask_ID + "' class='col-lg-12 evenadding distance'>";
              stringx += "<div class='row iframepadding'>";
              stringx += "<div class='embed-responsive embed-responsive-16by9'>";
              if (item.compLabel.indexOf('uploads/process/form_videos/') != -1) {
                stringx += "<video width='320' height='240' controls>";
                stringx += "<source src='<?php echo base_url() ?>" + item.compLabel + "' type='video/mp4'>";
                stringx += "<source src='<?php echo base_url() ?>" + item.compLabel + "' type='video/m4v'>";
                stringx += "<source src='<?php echo base_url() ?>" + item.compLabel + "' type='video/avi'>";
                stringx += "<source src='<?php echo base_url() ?>" + item.compLabel + "' type='video/mpg'>";
                stringx += "<source src='<?php echo base_url() ?>" + item.compLabel + "' type='video/mov'>";
                stringx += "<source src='<?php echo base_url() ?>" + item.compLabel + "' type='video/mpg'>";
                stringx += "<source src='<?php echo base_url() ?>" + item.compLabel + "' type='video/mpeg'>";
                stringx += "</video>";
              } else {
                stringx += item.compLabel;
              }
              stringx += "</div>";
              stringx += "</div>";
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' class='formdistance' style='font-size:1.1em;color:#0992a2'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
            }
            //FILE CONTENT
            else if (item.component_ID == "18") {
              var complabel = item.compLabel;
              var complabel_name = item.compLabel;

              complabel_name = complabel_name.replace("uploads/process/form_file/", "");

              if (complabel.indexOf('.xlsx') != -1) {
                complabel = "XLSX";
              } else if (complabel.indexOf('.xls') != -1) {
                complabel = "XLS";
              } else if (complabel.indexOf('.zip') != -1) {
                complabel = "ZIP";
              } else if (complabel.indexOf('.doc') != -1) {
                complabel = "DOC";
              } else if (complabel.indexOf('.docx') != -1) {
                complabel = "DOCX";
              } else if (complabel.indexOf('.ppt') != -1) {
                complabel = "PPT";
              } else if (complabel.indexOf('.pptx') != -1) {
                complabel = "PPTX";
              } else if (complabel.indexOf('.pdf') != -1) {
                complabel = "PDF";
              } else if (complabel.indexOf('.rar') != -1) {
                complabel = "RAR";
              }
              stringx += "<div id='' class='col-lg-12 evenadding distance paddingtop dotted_border'>";

              stringx += "<div class='row'>";

              stringx += "<div class='col-12 text-center'>";
              stringx += "<a href='<?php echo base_url() ?>" + item.compLabel + "' class='btn btn-outline-info btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
              stringx += "<span>";
              stringx += "<i class='fa fa-file-text-o'></i>";
              stringx += "<span>" + complabel + "</span>";
              stringx += "</span>";
              stringx += "</a>";

              stringx += "<br><div style='font-size:1.1em;color:#7e61b1'>" + complabel_name + "</div>";
              stringx += "<a href='<?php echo base_url() ?>" + item.compLabel + "' class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' download>";
              stringx += "<span><i class='fa fa-download'></i><span>Download this file</span></span>";
              stringx += "</a>";
              if (item.sublabel != null) {
                stringx += "<div data-id='" + item.subTask_ID + "' id='sublabel" + item.subTask_ID + "' style='font-size:1.1em;color:#0992a2'>" + item.sublabel + "</div>";
              }
              stringx += "</div>";
              stringx += "</div>";

              stringx += "</div>";
            }
          });
        });
        }
        $("#formaddcontent" + taskid).html(stringx);
        completethistask_button(checklistStatusId, cid);
        callback(res);
      }
    });
  }


  function displayformdiv(element) {
    // console.log($('#f').find('form').attr('class'));
    // if($('#f').find('form').attr('class') == 'answerableForm fv-form fv-form-bootstrap'){
    //   formValidation.destroy();
    // }else{
    //   console.log('without validation');
    // }
    var taskid = $(element).data('taskid');
    var checkstatid = $(element).data('id');
    display_allcomponents(taskid, checkstatid, function(result) {
      getassignment_permission();
      dynamicProcessStrValidation(taskid, checkstatid);
      displayduedate(taskid);
    });

    $(".inputtask").css({
      "background-color": "",
      "color": "#8e8585"
    });
    $(".inputtask").removeClass("placeholdercolor");
    $("#inputbox_id" + checkstatid).css({
      "background-color": "#51aff9",
      "color": "white"
    });
    $("#inputbox_id" + checkstatid).addClass("placeholdercolor");
    $("#id-cs").val(checkstatid);
    if ($("#checkbox_id" + checkstatid).is(':checked')) {
      // $("button#completetask"+checkstatid).prop("disabled",true);
    }
  }

  function displayduedate(taskid) {
    var tid = taskid;
    var dd = "";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_duedate",
      data: {
        task_ID: tid,
        check_ID: cid,
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        var data = res;
        if (data.taskdueDate != null) {
          var dateString = data.taskdueDate;
          var dateObj = new Date(dateString);
          var momentObj = moment(dateObj);
          var momentString = momentObj.format('llll');
          dd += "<div class='row taskduedate' style='padding:15px;'>";
          dd += "<label style='font-size:0.9em'> <i class='fa fa-calendar-check-o'></i><strong style='color:#34bfa3;'> Task Due Date:</strong> " + momentString + "</label>";
          dd += "</div>";
          $(".taskduedate").remove();
          $("#rowdisplay" + tid).after(dd);
        }
      }
    });
  }

  function checkall() {
    status = "3";
    $('.checkedC').prop("checked", true);
    $(".crashout").css({
      "text-decoration": "line-through",
      "font-style": "italic",
      "color": "#8e8585"
    });
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_taskAllStatus",
      data: {
        checklist_ID: cid,
        isCompleted: status
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
    checkcompleted();
  }

  function uncheckall() {
    status = "2";
    $('.checkedC').prop("checked", false);
    $(".crashout").css({
      "text-decoration": "",
      "font-style": "",
      "color": ""
    });
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_taskAllStatus",
      data: {
        checklist_ID: cid,
        isCompleted: status
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        swal({
          position: 'center',
          type: 'success',
          title: 'Reactivate Checklist!',
          showConfirmButton: false,
          timer: 1500
        });
        window.location.href = "<?php echo base_url('process/checklist_answerable') ?>" + "/" + prid + "/" + cid;
      },
      error: function(res) {
        console.log(res);
      }
    });
    checknotcomplete();
  }

  function changecheckbox(element) {
    var statusID = $(element).data('id');
    var displayID = $("#display_id").val();
    var ct = $("#countTask").val();
    if ($("#checkbox_id" + statusID).is(':checked')) {
      status = "3";
      $("#inputbox_id" + statusID).css({
        "text-decoration": "line-through",
        "font-style": "italic",
        "color": "#8e8585"
      });
      a++;
    } else {
      status = "2";
      $("#inputbox_id" + statusID).css({
        "text-decoration": "",
        "font-style": "",
        "color": "#8e8585"
      });
      $("#uncheckall").remove();
      $("#checkall").remove();

      if (displayID == "1") {
        $("h3#appendbutton").append("<button id='checkall' class='btn btn-success btn-sm m-btn m-btn--icon'><span><i class='fa fa-check mr-1'></i><span>Complete this checklist</span></span></button>");
      }
      a--;
    };
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_taskStatus",
      data: {
        checklistStatus_ID: statusID,
        isCompleted: status
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
    if (a == ct) {
      status = "3";
      checkcompleted();
    } else {
      checknotcomplete();
    }
  }

  function checkcompleted() {
    status = "3";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_checklistStatus",
      data: {
        checklist_ID: cid,
        status_ID: status
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        swal({
          position: 'center',
          type: 'success',
          title: 'Checklist Completed!',
          showConfirmButton: false,
          timer: 1500
        });
        window.location.href = "<?php echo base_url('process/checklist_answerable') ?>" + "/" + prid + "/" + cid;
      },
      error: function(res) {
        console.log(res);
      }
    });
  }

  function checknotcomplete() {
    status = "2";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_checklistStatus",
      data: {
        checklist_ID: cid,
        status_ID: status
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
      },
      error: function(res) {
        console.log(res);
      }
    });
  }

  function radio_option(subtaskid, componentid) {
    var stringcomp = "";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
      data: {
        subTask_ID: subtaskid,
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());

        $.each(result, function(key, item) {
          stringcomp += "<div class='col-lg-10'>";
          stringcomp += "<label id='optiondetails" + item.componentSubtask_ID + "' class='m-radio m-radio--bold m-radio--state-primary'>";
          if (item.compcontent != null) {
            stringcomp += item.compcontent;
          } else {
            stringcomp += "Double click and type option...";
          }
          stringcomp += "<input name='form_component" + subtaskid + "' id='radioopt" + item.componentSubtask_ID + "' data-id='" + subtaskid + "' data-component='" + componentid + "' data-subtask='" + subtaskid + "' type='radio' value='" + item.compcontent + "'>";
          stringcomp += "<span></span>";
          stringcomp += "</label>";
          stringcomp += "</div>";
        });
        $("#radiolist" + subtaskid).html(stringcomp);
      }
    });
  }

  function dropdown_list(subtaskid) {
    var stringcomp = "";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
      data: {
        subTask_ID: subtaskid,
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());

        $.each(result, function(key, item) {
          stringcomp += "<option>";
          if (item.compcontent != null) {
            stringcomp += item.compcontent;
          } else {
            stringcomp += "Item list...";
          }
          stringcomp += "</option>";
        });
        $("#itemlist" + subtaskid).html(stringcomp);
      }
    });
  }

  function checkbox_option(subtaskid, componentid) {
    var stringcomp = "";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
      data: {
        subTask_ID: subtaskid,
      },
      cache: false,
      success: function(res) {
        var result = JSON.parse(res.trim());
        // var countform=Object.keys(result).length;
        $.each(result, function(key, item) {
          stringcomp += "<div class='col-lg-10'>";
          stringcomp += "<label id='optiondetails" + item.componentSubtask_ID + "' data-id='" + item.componentSubtask_ID + "' class='m-checkbox m-checkbox--bold m-checkbox--state-brand'>";
          stringcomp += "<input id='checkBoxinput" + item.componentSubtask_ID + "' onchange='savemultipleanswer(this)' class='checkedBox' data-id='" + subtaskid + "' name='checkboxanswer' data-component='" + componentid + "' type='checkbox' data-check='" + item.componentSubtask_ID + "' value='" + item.compcontent + "'>";
          if (item.compcontent != null) {
            stringcomp += item.compcontent;
          } else {
            stringcomp += "No label...";
          }
          stringcomp += "<span></span>";
          stringcomp += "</label>";
          stringcomp += "</div>";
        });
        $("#checkoption" + subtaskid).html(stringcomp);
      }
    });
  }

  function fetchdatepicker(element) {
    var subtaskkid = $(element).data('id');
    $("#getdate" + subtaskkid).datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayBtn: true
    });
  }

  function fetchdatetimepicker(element) {
    var subtaskkid = $(element).data('id');
    $("#getdatetime" + subtaskkid).datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      autoclose: true,
      todayBtn: true
    });
  }

  function time_getvalsdisz(element){
    // alert("HEKKK");
    console.log($(element).data('id'));
    var subtaskkid = $(element).data('id');
      $("#gettime" + subtaskkid).timepicker({
      format: 'hh:ii',
      autoclose: true,
      todayBtn: true
    });
  }

  function fetchtimepicker() {
    // var subtaskkid = $(element).data('id');
    console.log(subtaskkid);
    console.log("hello");
    // $("#gettime" + subtaskkid).timepicker({
    //   format: 'hh:ii',
    //   autoclose: true,
    //   todayBtn: true
    // });
  }
  //Save Answers
  function savemultipleanswer(element) {
    var subtaskid = $(element).data('id');
    var compsubtaskid = $(element).data('check');
    var answercontent = "";
    var link = ($("#checkBoxinput" + compsubtaskid).is(':checked')) ? "add_multiple_answer" : "fetch_ansid";
    answercontent = $("#checkBoxinput" + compsubtaskid).val();
    console.log(link);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/" + link,
      data: {
        subtask_ID: subtaskid,
        checklistid: cid,
        answer: answercontent
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        var data = res;
        clearTimeout(timer);
        timer = setTimeout(doNotif, 1000);
      },
      error: function(res) {
        console.log(res);
      }
    });
  }

  function autosaveanswer(element) {
    var subtaskid = $(element).data('id');
    var comp = $(element).data('component');
    var answercontent = "";
    if (comp == "7") {
      answercontent = $("#itemlist" + subtaskid).val();
    } else if (comp == "10") {
      answercontent = $("#getdate" + subtaskid).val();
    } else if (comp == "13") {
      answercontent = $("#gettime" + subtaskid).val();
    } else if (comp == "14") {
      answercontent = $("#getdatetime" + subtaskid).val();
    } else if (comp == "6") {
      answercontent = $('input[name=form_component' + subtaskid + ']:checked').val();
    } else {
      answercontent = $("#answer" + subtaskid).val();
    }
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_answer",
      data: {
        subtask_ID: subtaskid,
        checklistid: cid,
        answer: answercontent
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        var data = res;
        clearTimeout(timer);
        timer = setTimeout(doNotif, 1000);
      },
      error: function(res) {
        console.log(res);
      }
    });
  }
  //CUSTOM VALIDATION
  function dynamicProcessStrValidation(taskIdVal, checkListIdVal) {
    $.when(fetchPostData({
      taskId: taskIdVal,
    }, '/process/generate_validation')).then(function(customValidation) {
      var customValidationObj = $.parseJSON(customValidation.trim());
      // console.log(customValidationObj);
      // console.log(customValidationObj.stat);
      if (parseInt(customValidationObj.stat)) {
        // console.log($('#f').find('form'));
        $('#checkbox_id' + $('#formtag' + taskIdVal).data('checkliststatusid')).prop('disabled', true);
        $('#formtag' + taskIdVal).formValidation(customValidationObj.option).on('success.form.fv', function(e, data) {
        //SUCCESS
        e.preventDefault();
        changeof_button();
        }).on('error.field.fv', function(e, data) { // ERROR
          $('#checkbox_id' + $('#formtag' + taskIdVal).data('checkliststatusid')).prop('checked', false);

          $("#inputbox_id" + $('#formtag' + taskIdVal).data('checkliststatusid')).css({
            "text-decoration": "",
            "font-style": "",
            "color": "white"
          });
          $('#checkbox_id' + $('#formtag' + taskIdVal).data('checkliststatusid')).prop('disabled', true);
          e.preventDefault();
          // console.log('error.field.fv-->', data);
        }).on('success.field.fv', function(e, data) { //SUCCESS EACH FIELD
          console.log("auto save answer");
          e.preventDefault();
          $('#checkbox_id' + $('#formtag' + taskIdVal).data('checkliststatusid')).prop('disabled', false);
          autosaveanswer(data.element);
        });
        // var formvalidation = $($('#f').find('form')).data('formValidation');
        // console.log(formvalidation);
        // formvalidation.destroy();
        if (customValidationObj.no_validation_fields.length > 0) {
          var no_validation_form_fields = [];
          $.each(customValidationObj.no_validation_fields, function(index, value) {
            no_validation_form_fields[index] = '[name=' + value + ']';
          })
          $("" + no_validation_form_fields.join(",")).attr('onchange', 'autosaveanswer(this)');
          $("" + no_validation_form_fields.join(",")).attr('onkeypress', 'autosaveanswer(this)');
        }
      } else {
        if (customValidationObj.no_validation_fields.length > 0) {
          var no_validation_form_fields = [];
          $.each(customValidationObj.no_validation_fields, function(index, value) {
            no_validation_form_fields[index] = '[name=' + value + ']';
          })
          $("" + no_validation_form_fields.join(",")).attr('onkeypress', 'autosaveanswer(this)');
          $("" + no_validation_form_fields.join(",")).attr('onchange', 'autosaveanswer(this)');
        }
        // code here to complete task withougt validation
        //empty form
 
        // console.log(this);
        $('#formtag' + taskIdVal).formValidation().on('success.form.fv', function(e, data) {
          // console.log(e);
          e.preventDefault();
          $('.detect_button').addClass('triggerz');
          changeof_button();
        });
        // $('.detect_button').click(function(e) {
        //   e.preventDefault();
        //   $('.detect_button').addClass('triggerz');
        //   });
      }
    });
  }
</script>
<!--Marks Code-->
<script>
  function showModalFileUpload(id) {
    $("#add_file-modal").data("subtaskid", id);
    $("#add_file-modal").modal("show");
  }

  function readFileType(input) {
    if (input.files && input.files[0]) {
      var file = input.files[0];
      var fileTypes = file["type"];
      var ValidImageTypes = ["application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/x-zip-compressed", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "image/gif", "image/jpeg", "image/png", "image/jpg"];
      if ($.inArray(fileTypes, ValidImageTypes) < 0) {
        swal("Ooops!", "Only Word/Excel/PowerPoint/Zip Files are allowed to be uploaded.", "error");
      } else {
        var reader = new FileReader();
        reader.onload = function(e) {
          // $('.upload-demo').addClass('ready');
          // rawImg = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
        reader.onloadend = function() {
          $("#formFilez").submit();
        };
      }
    } else {
      swal("Sorry - you're browser doesn't support the FileReader API");
    }
  }
  $('#upload_files').on('change', function() {
    tempFilename = $(this).val();
    readFileType(this);
  });

  function restricted_update(){
    swal("Updating of process template is restricted!", "(The referred process template of this checklist has completed checklist/s so updating is restricted.)", "info");
  }

  $("#formFilez").on('submit', function(e) {

    var subtaskid2 = $("#add_file-modal").data("subtaskid");
    var cid2 = $("#checklistID").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url(); ?>process/display_dynamicfilefetch/" + subtaskid2 + "/" + cid2,
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      success: function(data) {
        res = JSON.parse(data.trim());
        var data = res;
        var uploaded_dynamicfilepath = data.complabel;
        var stringx = "";
        if (data != "Invalid") {
          $("#add_file-modal").modal("hide");
          $(".fileUploadDesc" + data.subtask_ID).hide();

          var answer_file = data.complabel;
          var file_type = "";
          if (answer_file.indexOf('xlsx') != -1) {
            file_type = "XLSX";
          } else if (answer_file.indexOf('xls') != -1) {
            file_type = "XLS";
          } else if (answer_file.indexOf('zip') != -1) {
            file_type = "ZIP";
          } else if (answer_file.indexOf('doc') != -1) {
            file_type = "DOC";
          } else if (answer_file.indexOf('mov') != -1) {
            file_type = "MOV";
          } else if (answer_file.indexOf('docx') != -1) {
            file_type = "DOCX";
          } else if (answer_file.indexOf('ppt') != -1) {
            file_type = "PPT";
          } else if (answer_file.indexOf('pdf') != -1) {
            file_type = "PDF";
          } else if (answer_file.indexOf('pptx') != -1) {
            file_type = "PPTX";
          } else if (answer_file.indexOf('rar') != -1) {
            file_type = "RAR";
          } else if (answer_file.indexOf('png') != -1) {
            file_type = "PNG";
          } else if (answer_file.indexOf('jpg') != -1) {
            file_type = "JPG";
          }
          answer_file = answer_file.replace("uploads/process/form_dynamic/", " ");
          stringx += "<div class='col-12 text-center filedisplay_area" + data.subtask_ID + "'>";
          if (file_type == "PNG" || file_type == "JPG" || file_type == "MOV") {
            stringx += "<a href='<?php echo base_url() ?>" + uploaded_dynamicfilepath + "' class='btn btn-outline-brand btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
            stringx += "<span>";
            stringx += "<i class='fa fa-file-movie-o'></i>";
          } else if (file_type == "XLSX" || file_type == "XLS") {
            stringx += "<a href='<?php echo base_url() ?>" + uploaded_dynamicfilepath + "' class='btn btn-outline-success btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
            stringx += "<span>";
            stringx += "<i class='fa fa-file-excel-o'></i>";
          } else if (file_type == "ZIP" || file_type == "DOC" || file_type == "DOCX" || file_type == "RAR" || file_type == "PDF") {
            stringx += "<a href='<?php echo base_url() ?>" + uploaded_dynamicfilepath + "' class='btn btn-outline-focus btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
            stringx += "<span>";
            stringx += "<i class='fa fa-file-text-o'></i>";
          } else if (file_type == "PPT" || file_type == "PPTX") {
            stringx += "<a href='<?php echo base_url() ?>" + uploaded_dynamicfilepath + "' class='btn btn-outline-warning btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
            stringx += "<span>";
            stringx += "<i class='fa fa-file-powerpoint-o'></i>";
          } else {
            stringx += "<a href='<?php echo base_url() ?>" + uploaded_dynamicfilepath + "' class='btn btn-outline-primary btn-lg  m-btn m-btn--icon m-btn--outline-2x' download>";
            stringx += "<span>";
            stringx += "<i class='fa fa-file-text-o'></i>";
          }
          stringx += "<span>" + file_type + "</span>";
          stringx += "</span>";
          stringx += "</a>";

          stringx += "&nbsp;&nbsp;<a href='#' onclick='remove_fileupload(" + data.answer_ID + "," + data.subtask_ID + ")'>"
          stringx += "<i class='fa fa-remove' style='color:#f4516c'></i>"
          stringx += "</a>&nbsp;";
          stringx += "<br><div style='font-size:1.1em;color:#7e61b1'>" + answer_file + "</div>";
          stringx += "<a href='<?php echo base_url() ?>" + uploaded_dynamicfilepath + "' class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' download>";
          stringx += "<span><i class='fa fa-download'></i><span>Download this file</span></span>";
          stringx += "</a>";
          $(".showOutputFileUpload" + data.subtask_ID).html(stringx);
        } else {
          swal("Ooops!", "There is an error during uploading.", "error");
        }
      }
    });
  });

  function remove_fileupload(answer_ID, subtask_ID) {
    var stringx = "";
    swal({
      title: 'Are you sure?',
      text: "The file you uploaded will be removed.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then(function(result) {
      if (result.value) {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/delete_file_answer",
          data: {
            answer_ID: answer_ID
          },
          cache: false,
          success: function() {
            swal(
              'Removed!',
              'File successfully Removed.',
              'success'
            )
            stringx += "<div class='col-12  text-center fileUploadDesc" + subtask_ID + "'>";
            stringx += "<label class='btn btn-outline-success btn-sm m-btn--icon' onclick='showModalFileUpload(" + subtask_ID + ")'>";
            stringx += "<span>";
            stringx += "<i class='fa fa-upload'></i>";
            stringx += "<span>Upload a file</span>";
            stringx += '</span>';
            stringx += "</label>";
            $(".showOutputFileUpload" + subtask_ID).html(stringx);
          },
          error: function(res) {
            swal(
              'Oops!',
              'Something is wrong with your code!',
              'error'
            )
          }
        });
      } else if (result.dismiss === 'cancel') {
        swal(
          'Cancelled',
          'Removing uploaded file has been cancelled',
          'error'
        )
      }
    });
  }

  function displayduedate(taskid) {
    var tid = taskid;
    var dd = "";
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_duedate",
      data: {
        task_ID: tid,
        check_ID: $("#checklistID").val()
      },
      cache: false,
      success: function(res) {
        res = JSON.parse(res.trim());
        var data = res;
        var dateString = data.taskdueDate;
        if (dateString == null || dateString == '') {
          var momentString = '';
        } else {
          var dateObj = new Date(dateString);
          var momentObj = moment(dateObj);
          // var momentString = momentObj.format('llll');
          var momentString = momentObj.format('dddd, MMMM DD, YYYY');
        }

        // if (data.taskdueDate != null) {
        dd += "<div class='row taskduedate'>";
        dd += "<div class='col-12 py-1 text-center'>";
        dd += "<label style='font-size:0.9em; margin-bottom:unset!important'> <i class='fa fa-calendar-check-o'></i><strong style='color:#34bfa3;'> Task Due Date:</strong> ";
        dd += '<a href="#" id="custom-task-duedate-' + tid + '" data-withcheduedate="<?php echo ($checklist->dueDate == '' || $checklist->dueDate == null) ? '' : $checklist->dueDate; ?>" data-id="' + tid + '" data-type="task" data-value="' + dateString + '" class="custom-editable custom-task-duedate">' + momentString + '</a>';
        dd += "</label>";
        dd += "</div>";
        dd += "</div>";
        // } else {
        //   dd += "<div class='row taskduedate' style='border-top: 1px solid #e4e4e4;'>";
        //   dd += "<div class='col-12 py-1 text-center'>";
        //   dd += "<label style='font-size:0.9em; margin-bottom:unset!important'> <strong style='color:#34bfa3;'>No Task due date set</strong></label>";
        //   dd += "</div>";
        //   dd += "</div>";
        // }
        $(".taskduedate").remove();
        // $("#rowdisplay" + tid).after(dd);
        $("#custom-task-duedate-container-" + tid).html(dd);
        customPopoverDatePicker('#custom-task-duedate-' + tid);
      }
    });
  }

  //Nicca's Pop over start
  $("html").on('click', '.popover-reject', function() {
    $(this).closest('.popover').popover('hide');
  });
  $("html").on('click', '.popover-clearfield', function() {
    $(this).closest('.popover').find('.form-control').val('');
  });
  $("html").on('click', '.popover-approve', function() {
    var that = this;
    var elem = $(that).closest('.popover').find('.form-control');
    var id = $(that).closest('.popover').find('.row').data('id');
    var type = $(that).closest('.popover').find('.row').data('type');
    var original_value = $(elem).data('value');
    var value_moment = ($(elem).val() === '') ? null : moment($(elem).val().trim(), "MM/DD/YYYY").format('YYYY-MM-DD');
    console.log("ORIGINAL:",$(elem).val().trim()); 
    console.log(moment($(elem).val().trim(), "MM/DD/YYYY").format('YYYY-MM-DD'));
    var orig_moment = moment(original_value).format('YYYY-MM-DD');
    if (orig_moment === value_moment) {
      // console.log("Same values. Dont save");
      $(that).closest('.popover').popover('hide');
    } else {
      console.log("Please save now!!");
      if (type === 'checklist') {
        console.log(value_moment);
        saveduedate(value_moment, id);
      } else {
        console.log("save task");
        console.log(value_moment);
        savetaskduedate(value_moment, id);
      }
    }
  });
  var customPopoverDatePicker = function(elemx) {
    var elem = $(elemx);
    var id = elem.data('id');
    var type = elem.data('type');
    var withCheDueDate = elem.data('withcheduedate');
    var value = elem.data("value");
    if (value === null) {
      value = '';
    }
    var buttons = '<button title="Save Changes" class="btn btn-outline-info btn-sm py-1 px-1 popover-approve" type="button"><i class="fa fa-check" style="width:20px;font-size:15px;line-height: 1;"></i></button> <button title="Cancel Changes" class="btn btn-outline-danger btn-sm  py-1 px-1 popover-reject" type="button"><i class="fa  fa-times"  style="width:20px;font-size:15px;line-height: 1;"></i></button>';
    var content = '<input type="text"  name="' + id + '"  data-value="' + value + '" class="form-control form-control-sm  custom-datepicker" readonly="" value="' + value + '">';
    var clearfield = '<button title="Clear Input" class="btn btn-outline-warning btn-sm py-1 px-1 popover-clearfield" type="button" style="margin-right:3px"><i class="fa fa-repeat" style="width:20px;font-size:15px;line-height: 1;"></i></button>';
    if (value === "" || value === null) {
      elem.html('Empty');
      elem.addClass('m--font-danger font-italic');
    }
    var title = (type == 'checklist') ? "Set Checklist Due Date" : "Set Task Due Date";
    var checklistOptions = {
      orientation: "bottom left",
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
      },
      dateFormat: 'MM dd, yy',
      minDate: new Date(moment().format('YYYY-MM-DD'))
    };
    if (type === 'task') {
      if (withCheDueDate != undefined && withCheDueDate != '') {
        var taskOptions = {
          orientation: "bottom left",
          templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
          },
          dateFormat: 'MM dd, yy',
          minDate: new Date(moment().subtract(3, 'months').format('YYYY-MM-DD')),
          maxDate: new Date(moment(withCheDueDate).format('YYYY-MM-DD'))
        };
      } else {
        var taskOptions = {
          orientation: "bottom left",
          templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
          },
          dateFormat: 'MM dd, yy',
          minDate: new Date(moment().format('YYYY-MM-DD'))
        };
      }
    }
    var datepickerOptions = (type === 'checklist') ? checklistOptions : taskOptions;
    elem.popover({
      trigger: 'click',
      placement: 'bottom',
      title: "<span style='color:white'>" + title + "</span>",
      html: true,
      template: '<div class="m-popover popover" role="tooltip" style="z-index:100!important">\
                    <div class="arrow"></div>\
                    <h3 class="popover-header"></h3>\
                    <div class="popover-body p-2"></div>\
                    </div>',
      content: '<div class="row no-gutters" data-id="' + id + '" data-type="' + type + '"><div class="col-md-7 col-xs-6">' + content + '</div><div class="col-md-5 col-xs-6 text-right">' + clearfield + buttons + '</div></div>'
    }).on('click', function(e) {
      $('[data-original-title]').not(elem).popover('hide');
      e.preventDefault();
    }).on('inserted.bs.popover', function() {
      var popover_elem = $(this).data("bs.popover").tip;
      var input_elem = $(popover_elem).find('.custom-datepicker');
      input_elem.datepicker(datepickerOptions);
      if (value !== "" && value !== null) {
        var momentDate = moment(value).format("YYYY-MM-DD");
        input_elem.datepicker('setDate', new Date(momentDate));
      }
    }).on("show.bs.popover", function() {
      var el = $(this).data("bs.popover").tip;
      $(el).css("max-width", "500px");
    });
  };
  
  //Nicca's pop over end
  function display_assign_duedate(taskid){
    console.log(taskid);
  }
  function savetaskduedate(dueDate, taskid) {
    // var taskid = $(element).data('id');
    // var dueDate = $("#taskduedate" + taskid).val();
    var pid = $("#hiddentaskID").val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/update_taskDuedate",
      data: {
        checklist_ID: cid,
        task_ID: taskid,
        taskdueDate: dueDate
      },
      cache: false,
      success: function(res) {
        swal({
          position: 'center',
          type: 'success',
          title: 'Due Date Set and Updated!',
          showConfirmButton: false,
          timer: 1500
        });
        displayduedate(taskid);
        $('.popover-reject').closest('.popover').popover('hide');
      },
      error: function(res) {
        doNotif2();
      }
    });
  }
</script>