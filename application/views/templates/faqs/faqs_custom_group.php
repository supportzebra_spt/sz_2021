<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">FAQ</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url()?>Faqs/faqs_custom_group_page" class="m-nav__link">
                            <span class="m-nav__link-text">Custom Group</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content page_here">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-6">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" autocomplete="off" class="form-control m-input m-input--solid" placeholder="Search..." id="searchField">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <a href="#" class="btn-sm btn m-btn--gradient-from-brand m-btn--gradient-to-info m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill addNewButton">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add New</span>
                                </span>
                            </a>                    
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <div id="offenseDatatable"></div>
            </div>
        </div>
    </div>
</div>
<form id="addDistro" method="post">
    <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Custom Group </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label for="recipient-name" class="col-form-label col-lg-2 col-sm-12">Name:</label>
                        <div class="col-lg-10 col-md-9 col-sm-9">
                            <div class="input-group">
                                <input type="text" autocomplete="off" maxlength="30" class="form-control m-input" id="custom_name_add" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="la la-tag glyphicon-th"></i></span>
                                </div>
                            </div>
                            <span style="font-size: 11px" class="m-form__help"><i>(Maximum length of 30)</i></span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="recipient-name" class="col-form-label col-lg-2 col-sm-12">Employee:</label>
                        <div class="col-lg-10 col-md-9 col-sm-9">
                            <select name="framework" data-show-subtext="true" id="list_emp_id_add" class="form-control selectpicker" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})" data-actions-box="true">
                            </select>
                            <span style="font-size: 11px" class="m-form__help"><i>(Select at least 1 that will be part of this Custom Group.)</i></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="updateDistro" method="post">
    <div class="modal fade" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Custom Group</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label for="recipient-name" class="col-form-label col-lg-2 col-sm-12">Name:</label>
                        <div class="col-lg-10 col-md-9 col-sm-9">
                            <div class="input-group">
                                <input type="text" autocomplete="off" maxlength="30" class="form-control m-input" id="distro_name_edit" required>
                                <input type="hidden" id="distro_id_edit" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="la la-tag glyphicon-th"></i></span>
                                </div>
                            </div>
                            <span style="font-size: 11px" class="m-form__help"><i>(Maximum length of 30)</i></span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="recipient-name" class="col-form-label col-lg-2 col-sm-12">Employee:</label>
                        <div class="col-lg-10 col-md-9 col-sm-9">
                            <select name="framework" id="list_acc_id_edit" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" multiple data-show-subtext="true" data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})" data-actions-box="true">
                            </select>
                            <span style="font-size: 11px" class="m-form__help"><i>(Select Accounts that will be part of this Custom Group.)</i></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Employee List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <a style="text-decoration: none;" href="#" class="m-portlet__nav-link m-btn--pill">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" autocomplete="off" class="form-control m-input m-input--solid" id="searchbar" onkeyup="search()" placeholder="Search here...">
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search m--font-brand"></i></span></span>
                            </div>
                        </a>
                    </div>
                </div>
                <div id="number_of_employee" style="float: right; padding: 10px"></div>
                <br>
                <hr>
                <div style="max-height: 250px; padding: 0 10px;" class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true">
                    <div id="modalBody1"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_custom_page.js"></script>
<script type="text/javascript">
    var pass_emp_id = '<?php echo $this->session->emp_id; ?>';
</script>
<style>
ul.view_custom li:hover {
  background-color: #ffffffc7;
}
</style>