<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">FAQ</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url();?>Faqs/faqs_server_page" class="m-nav__link">
                            <span class="m-nav__link-text">Server</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url();?>Faqs/faqs_edit_page/<?=$id?>" class="m-nav__link">
                            <span class="m-nav__link-text">Edit</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <label class="col-5 col-form-label">Published:</label>
                <div class="col-7">
                    <div id="isVisible"></div>
                </div>
            </div>
        </div>
    </div>
    <form id="updateSubmitAccount" method="post">
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-1 col-sm-12">Title*</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <div class="input-group">
                                <input type="hidden" id="edit_faqs_id">
                                <input type="text" class="form-control m-input" name="title" id="edit_title" placeholder="Enter FAQ Title here">
                                <div class="input-group-append"><span class="input-group-text"><i class="la la-tag"></i></span></div>
                            </div>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Please enter FAQ title)</i></span>
                        </div>
                        <label class="col-form-label col-lg-1 col-sm-12">Category*</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <div class="m-form__control">
                                <select class="form-control m-bootstrap-select m_selectpicker" name="category" id="edit_category_id">
                                </select>
                            </div>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Select only one FAQ Category)</i></span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label class="col-form-label col-lg-1 col-sm-12">Editor*</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <select id="edit_list_access" data-dropup-auto="false" class="form-control m-bootstrap-select m_selectpicker" data-actions-box="true" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Editor ({0})"></select>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Select employee that could update this FAQ)</i></span>
                        </div>
                        <label class="col-form-label col-lg-1 col-sm-12"></label>
                        <label class="col-form-label col-lg-1 col-sm-12">Viewer*</label>
                        <div style="right: 5px; padding: 10;" class="col-lg-4 col-md-9 col-sm-12">
                            <div class="m-form__control">
                                <select class="form-control m-bootstrap-select m_selectpicker" data-dropup-auto="false" name="list_emp_id" id="edit_list_emp_id" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Viewer ({0})" data-actions-box="true" required>
                                </select>
                            </div>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Select employee that could view this FAQ)</i></span>
                        </div>
                        <div style="padding: 0; position: absolute; right: 0px;" class="col-lg-1 col-md-9 col-sm-12">
                            <button type="button" class="btn btn-info view_account" data-faqs_id="<?= $id;?>"><i class="la la-group"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="m-section">
                        <div class="form-group">
                            <textarea id="details"></textarea>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">FAQs Settings</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <a style="text-decoration: none;" href="#" class="m-portlet__nav-link m-btn--pill">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" class="form-control m-input m-input--solid" id="searchbar" onkeyup="search()" placeholder="Search here...">
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search m--font-brand"></i></span></span>
                            </div>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" style="max-height: 270px">
                    <div style="padding: 0 10px;"id="modalBody1">No Data Available</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>
<!-- <div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Group Editor</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label class="col-form-label col-lg-2 col-sm-12">Editor*</label>
                    <div class="col-lg-10 col-md-9 col-sm-12">
                        <select id="edit_list_access" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Editor ({0})" data-actions-box="true"></select>
                        <span style="font-size: 12px;" class="m-form__help"><i>(Select accounts that could update this FAQ)</i></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="faqsModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Group Viewer</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label class="col-form-label col-lg-2 col-sm-12">Viewer*</label>
                    <div class="col-lg-10 col-md-9 col-sm-12">
                        <div class="m-form__control">
                            <select class="form-control m-bootstrap-select m_selectpicker" name="list_emp_id" id="edit_list_emp_id" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Viewer ({0})" data-actions-box="true" required>
                            </select>
                        </div>
                        <span style="font-size: 12px;" class="m-form__help"><i>(Select employee that could view this FAQ)</i></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->
<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_update.js"></script>
<style>
    /*img {
        max-width: 100%;
        max-height: 100vh;
        height: auto;
    }*/
    
table{
        table-layout: fixed;
    }
    td{
        overflow: hidden;
    }
</style>
<script type="text/javascript">
    var faqs_id = '<?= $id;?>';
    var emp_id = '<?= $this->session->emp_id;?>';
</script>
