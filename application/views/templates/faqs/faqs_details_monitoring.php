<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
 			<div class="mr-auto">
 				<h3 class="m-subheader__title m-subheader__title--separator">FAQ</h3>			
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url();?>Faqs/faqs_monitoring_page" class="m-nav__link">
							<span class="m-nav__link-text">FAQ Monitor</span>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Faqs/faqs_details_monitor_page/<?=$id?>" class="m-nav__link">
							<span class="m-nav__link-text">FAQ Content</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
    <div class="m-content">
        <div id="faqsView"></div>
    </div>
</div>
<div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">FAQs Settings</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalBody1"></div>
                <hr>
                <div id="modalBody2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_details.js"></script>
<style>
img 
{
    max-width: 100%;
    max-height: 100vh;
    height: auto;
}
</style>
<script type="text/javascript">
	var faq_id = <?= $id;?>;
	var emp_id = <?= $this->session->emp_id;?>;
</script>