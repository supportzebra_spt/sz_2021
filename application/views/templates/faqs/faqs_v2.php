
<style>
.text-truncate:hover {
    text-overflow: clip;
    white-space: normal;
    word-break: break-all;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">	
		<div class="d-flex align-items-center">
 			<div class="mr-auto">
 				<h3 class="m-subheader__title m-subheader__title--separator">FAQs</h3>			
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url()?>Faqs/faqs_v2" class="m-nav__link">
							<span class="m-nav__link-text">Frequently Asked Questions</span>
						</a>
					</li>
				</ul>
			</div>
			<div>
			 <!-- <?php  if($access == $session['emp_id']){?>
				<a href="<?php echo base_url('Faqs/faqs_create_page'); ?>" class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">Add New <i class="fa fa-plus"></i></a>
				<?php }?> -->
				<div id="access"></div>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--space">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">                        
						<h3 class="m-portlet__head-text">
						How can we help you today?
						</h3>
					</div>  
					<div class="row">
						<div class="col-xl-6">
							<a style="text-decoration: none;" href="#" class="m-portlet__nav-link m-btn--pill">                                
								<div class="m-input-icon m-input-icon--right">
									<input type="text" class="form-control m-input m-input--solid" id="searchField" placeholder="Enter your search term here...">
									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search m--font-brand"></i></span></span>
								</div> 
							</a>
						</div>
						<div class="form-group col-xl-3">
							<select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="category"></select>
						</div>
						<div class="col-xl-3">
							<select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="visible">
								<option value="2">All Status</option>
                                <option value="0" data-content="<span class='m-badge m-badge--brand m-badge--wide '>Draft</span>">Private</option>
                                <option value="1" data-content="<span class='m-badge m-badge--info m-badge--wide '>Publish</span>">Public</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<div id="faqsView"></div>                  
			</div>
		</div>         
	</div>
</div>
<form id="updateCategory" method="post">
    <div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="faqs_category_name"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
					<a style="text-decoration: none;" href="#" class="m-portlet__nav-link m-btn--pill">                                
						<div class="m-input-icon m-input-icon--right">
							<input type="text" class="form-control m-input m-input--solid" id="searchbar" onkeyup="search()" placeholder="Enter your search term here...">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search m--font-brand"></i></span></span>
						</div> 
					</a>
					<br>
					<div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" style="max-height: 270px">
	                    <div style="padding: 0 10px;"id="faqsView1">No Data Available</div>
	                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_v2.js"></script>
<script type="text/javascript">
	emp_id = '<?php echo $this->session->emp_id; ?>';
</script>

