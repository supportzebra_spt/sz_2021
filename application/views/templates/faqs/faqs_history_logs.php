<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">FAQs</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url()?>Faqs/faqs_history_logs_page" class="m-nav__link">
                            <span class="m-nav__link-text">History Logs</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div style="width:100%" class="m-form__group m-form__group--inline">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input id="searchDate" type="text" class="form-control m-input m-input--solid" readonly="" placeholder="Select time">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span><i class="la la-calendar"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div style="width:100%" class="m-form__group m-form__group--inline">
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="searchType">
                                                <option value="">All Type</option>
                                                <option value="1" data-content="<span class='m-badge m-badge--success m-badge--wide '>Created</span>">Created</option>
                                                <option value="2" data-content="<span class='m-badge m-badge--info m-badge--wide '>Updated</span>">Updated</option>
                                                <option value="3" data-content="<span class='m-badge m-badge--metal m-badge--wide '>Archived</span>">Archived</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div style="width:100%" class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="searchField">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="history_logs"></div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_history.js"></script>