<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">FAQ</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url()?>Faqs/faqs_category" class="m-nav__link">
                            <span class="m-nav__link-text">Category</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content page_here">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-10 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <!-- <div class="col-md-4">
                                    <div style="width: 100%" class="m-form__group m-form__group--inline">
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="searchCategory">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div style="width: 100%" class="m-form__group m-form__group--inline">
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="searchType">
                                                <option value="">All Type</option>
                                                <option value="main" data-content="<span class='m-badge m-badge--info m-badge--wide '>Main Category</span>">Main Category</option>
                                                <option value="sub" data-content="<span class='m-badge m-badge--danger m-badge--wide '>Sub-Category</span>">Sub-Category</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-5">
                                    <div style="width: 100%" class="m-input-icon m-input-icon--left">
                                        <input type="text" autocomplete="off" class="form-control m-input m-input--solid" placeholder="Search..." id="searchField">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 order-1 order-xl-2 m--align-right">
                            <a href="#" class="btn-sm btn m-btn--gradient-from-brand m-btn--gradient-to-info m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill addNewButton">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add New</span>
                                </span>
                            </a>                    
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <div id="offenseDatatable"></div>
            </div>
        </div>
    </div>
</div>
<form id="addCategory" method="post">
    <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create FAQ Category </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Category Name*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <div class='input-group'>
                                <input type="text" maxlength="25" autocomplete="off" class="form-control m-input" id="category_name_add" placeholder="Enter your Category Name" required>
                                <div class="input-group-append"><span class="input-group-text"><i class="la la-tag"></i></span></div>
                            </div>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Maximum length of 25 characters.)</i></span>
                        </div>
                    </div>
                    <!-- <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Type of Category*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework" id="relationship_add" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true">
                                <option value="0" data-content="<span class='m-badge m-badge--info m-badge--wide '>Main Category</span>">Main Category</option>
                                <optgroup label="Sub-Category">
                                    <?php foreach ($category as $n) { ?>
                                        <option value="<?= $n->faqs_category_id; ?>|<?= $n->list_emp_id; ?>"><?=$n->faqs_category_name;?></option>
                                    <?php }?>
                                </optgroup>
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Select only one type of category.)</i></span>
                        </div>
                    </div> -->
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Editor*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework" id="list_access_add" title="Employee List" class="form-control m-bootstrap-select m_selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})">
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Leave blank if none.)</i></span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Viewer*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework"  required id="list_emp_id_add" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" multiple data-selected-text-format="count" data-actions-box="true" data-count-selected-text="Selected Accounts ({0})">
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Select at least 1 that could view this category)</i></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="addSubcategory" method="post">
    <div class="modal fade" id="faqsModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="sub_name"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Category Name*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <div class='input-group'>
                                <input type="hidden" id="sub_category_id_add">
                                <input type="text" autocomplete="off" maxlength="25" class="form-control m-input" id="sub_category_name_add" placeholder="Enter your Category Name" required>
                                <div class="input-group-append"><span class="input-group-text"><i class="la la-tag"></i></span></div>
                            </div>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Maximum length of 25 characters.)</i></span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Editor*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework" id="sub_list_access_add" title="Employee List" class="form-control m-bootstrap-select m_selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})">
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Leave blank if none.)</i></span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Viewer*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework" id="sub_list_emp_id_add" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" multiple data-selected-text-format="count" data-actions-box="true" data-count-selected-text="Selected Accounts ({0})">
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Select at least 1 that could view this category)</i></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="updateCategory" method="post">
    <div class="modal fade" id="faqsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">FAQ Category Settings</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Category Name:</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <div class='input-group'>
                                <input type="hidden" id="category_id_edit">
                                <input type="text" autocomplete="off" maxlength="25" class="form-control m-input" id="category_name_edit" placeholder="Enter your Category Name" required>
                                <div class="input-group-append"><span class="input-group-text"><i class="la la-tag"></i></span></div>
                            </div>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Maximum length of 25 characters.)</i></span>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Editor*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework" id="list_access_edit" title="Employee List" class="form-control m-bootstrap-select m_selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-count-selected-text="Selected Employee ({0})">
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Leave blank if none.)</i></span>
                        </div>
                    </div>
                    <!-- <hr>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Type of Category:</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework" id="relationship_edit" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true">
                                <option value="0" data-content="<span class='m-badge m-badge--info m-badge--wide '>Main Category</span>">Main Category</option>
                                <optgroup label="Sub-Category">
                                    <?php foreach ($category as $n) { ?>
                                        <option value="<?= $n->faqs_category_id; ?>"><?=$n->faqs_category_name;?></option>
                                    <?php }?>
                                </optgroup>
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Select only one type of category.)</i></span>
                        </div>
                    </div> -->
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Viewer*</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                            <select name="framework" id="visible_edit" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true" multiple data-selected-text-format="count" data-actions-box="true" data-count-selected-text="Selected Accounts ({0})">
                            </select>
                            <span style="font-size: 11px; float:right" class="m-form__help"><i>(Select at least 1 that could view this category)</i></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Settings</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <a style="text-decoration: none;" href="#" class="m-portlet__nav-link m-btn--pill">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" autocomplete="off" class="form-control m-input m-input--solid" id="searchbar" onkeyup="search()" placeholder="Search here...">
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search m--font-brand"></i></span></span>
                            </div>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" style="max-height: 270px">
                    <div style="padding: 0 10px;"id="modalBody1">No Data Available</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="faqsModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 900px !important" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title" id="head_sub_category"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <div class="row">
                    <div class="col-xl-12">
                        <a style="text-decoration: none;" href="#" class="m-portlet__nav-link m-btn--pill">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" class="form-control m-input m-input--solid" id="searchbar" onkeyup="search()" placeholder="Search here...">
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search m--font-brand"></i></span></span>
                            </div>
                        </a>
                    </div>
                </div>
                <hr> -->
                <div style="height: 200px; max-height: 300px" class="m-section__content">
                    <table  class="table table-sm table-hover m-table m-table--head-separator-primary">
                        <thead>
                            <tr>
                                <th width="200px">Sub-Category Name</th>
                                <th width="300px;">Editor</th>
                                <th width="300px;">Viewer</th>
                                <th width="100px">Action</th>
                            </tr>
                        </thead>
                        <tbody id="display_here"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_category.js"></script>
<script type="text/javascript">
    var emp_id = '<?php echo $this->session->emp_id; ?>';
</script>
<style>
    ul.view_category li:hover {
      background-color: #ffffffc7;
  }
</style>