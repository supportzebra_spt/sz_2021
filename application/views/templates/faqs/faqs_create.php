<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <form id="m_form_1" method="post">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">FAQ</h3>			
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="<?php echo base_url();?>Faqs/faqs_server_page" class="m-nav__link">
                                <span class="m-nav__link-text">Server</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="<?php echo base_url();?>Faqs/faqs_create_page" class="m-nav__link">
                                <span class="m-nav__link-text">Create</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6"><label class="col-form-label">Published:</label></div>
                            <div class="col-6">
                                <span class="m-switch m-switch--outline m-switch--icon m-switch--accent">
                                    <label>
                                        <input class="get_value" type="checkbox" name="">
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <button type="submit" style="float:right" class="btn m-btn--pill m-btn--air m-btn--gradient-from-brand m-btn--gradient-to-info">Create FAQ</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-1 col-sm-12">Title*</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <div class="input-group">
                                <input type="text" class="form-control m-input" name="title" id="title" placeholder="Enter FAQ Title here">
                                <div class="input-group-append"><span class="input-group-text"><i class="la la-tag"></i></span></div>
                            </div>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Please enter FAQ title)</i></span>
                        </div>
                        <label class="col-form-label col-lg-1 col-sm-12">Category*</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <div class="m-form__control">
                                <select class="form-control m-bootstrap-select m_selectpicker" title="FAQ Category" name="category" id="category_id">
                                </select>
                            </div>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Select only one FAQ Category)</i></span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <!-- <div class="form-group col-lg-2">
                            <div class="m-form__group form-group row">
                                <label class="col-5 col-form-label">Publish*</label>
                                <div class="col-7">
                                    <span class="m-switch m-switch--outline m-switch--icon m-switch--accent">
                                        <label>
                                            <input class="get_value" type="checkbox" name="">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div> -->
                        <label class="col-form-label col-lg-1 col-sm-12">Editor*</label>
                        <div class="col-lg-5 col-md-9 col-sm-12">
                            <select data-dropup-auto="false" id="list_access" class="form-control m-bootstrap-select m_selectpicker" data-actions-box="true" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Editor ({0})"></select>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Select employee that could update this FAQ)</i></span>
                        </div>
                        <label class="col-form-label col-lg-1 col-sm-12">Viewer*</label>
                        <div class="col-lg-5 col-md-9 col-sm-12">
                            <div class="m-form__control">
                                <select data-dropup-auto="false" class="form-control m-bootstrap-select m_selectpicker" id="list_emp_id" data-live-search="true" multiple data-selected-text-format="count" data-count-selected-text="Selected Viewer ({0})" data-actions-box="true" required>
                                </select>
                            </div>
                            <span style="font-size: 12px;" class="m-form__help"><i>(Select employee that could view this FAQ)</i></span>
                        </div>
                    </div>
                    <!-- <button type="submit" style="float:right" class="btn m-btn--pill m-btn--air m-btn--gradient-from-brand m-btn--gradient-to-info">Save Data</button> -->
                    <br>
                </div>
            </div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__body">
                    <div class="m-section">
                        <div class="m-section">
                            <div class="form-group">
                                <textarea id="details"></textarea>
                            </div>
                        </div>  
                    </div>
                    <button type="submit" style="float:right" class="btn m-btn--pill m-btn--air m-btn--gradient-from-brand m-btn--gradient-to-info">Create FAQ</button><br>
                </div>
            </div>
            
        </div>
    </form>
</div>
    <!-- <a href="#" class="float modal_pop_up m-nav__link m-dropdown__toggle">
        <span class="m-nav__link-icon"><i class="la la-link my-float"></i></span>
    </a> -->
<!-- <div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title">Links</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">FAQ List:</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <select name="framework" title="List of FAQ" id="faqs_list" class="form-control m-bootstrap-select m_selectpicker" data-live-search="true">
                        </select>
                        <span style="font-size: 11px; float:right" class="m-form__help"><i>(Select FAQ to display link)</i></span>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">FAQ Link:</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <div class="input-group">
                            <input type="text" readonly class="form-control" id="faqs_link" placeholder="Link here...">
                            <div class="input-group-append">
                                <a href="#" class="btn btn-secondary" data-clipboard="true" data-clipboard-target="#faqs_link"><i class="la la-copy"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->
<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_create.js"></script>
<script type="text/javascript">
    emp_id = '<?php echo $this->session->emp_id?>';
</script>
<style type="text/css">
    .float{
        position:fixed;
        width:60px;
        height:60px;
        bottom:40px;
        right:40px;
        background-color:#0C9;
        color:#FFF;
        border-radius:50px;
        text-align:center;
        box-shadow: 2px 2px 3px #999;
        z-index: 9999999;
    }
    .my-float{
        margin-top:22px;
    }
    table{
        table-layout: fixed;
    }
    td{
        overflow: hidden;
    }
</style>
