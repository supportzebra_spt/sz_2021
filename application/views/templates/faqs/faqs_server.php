<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">FAQ</h3>			
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url()?>Faqs/faqs_server_page" class="m-nav__link">
                            <span class="m-nav__link-text">Server</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content page_here">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-10 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div style="width: 100%" class="m-form__group m-form__group--inline">
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="searchCategory">
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div style="width: 100%" class="m-form__group m-form__group--inline">
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="searchStatus">
                                                <option value="">All Status</option>
                                                <option value="0" data-content="<span class='m-badge m-badge--brand m-badge--wide '>Draft</span>">Draft</option>
                                                <option value="1" data-content="<span class='m-badge m-badge--info m-badge--wide '>Publish</span>">Publish</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="searchField">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 order-1 order-xl-2 m--align-right">
                            <a href="<?php echo base_url('Faqs/faqs_create_page'); ?>" class="btn-sm btn m-btn--gradient-from-brand m-btn--gradient-to-info m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill addNewButton">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add New</span>
                                </span>
                            </a>                    
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <div id="offenseDatatable"></div>
            </div>
        </div>
    </div>
</div>

<form id="addCategory" method="post">
    <div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create FAQs</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Category Name:</label>
                        <select class="form-control" id="category_name" required>
                            <option value="">Select Category</option>
                            <?php foreach ($category as $cat) { ?>
                                <option value="<?= $cat->faqs_category_id; ?>"><?= $cat->faqs_category_name; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">FAQs Settings</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <a style="text-decoration: none;" href="#" class="m-portlet__nav-link m-btn--pill">
                            <div class="m-input-icon m-input-icon--right">
                                <input type="text" class="form-control m-input m-input--solid" id="searchbar" onkeyup="search()" placeholder="Search here...">
                                <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search m--font-brand"></i></span></span>
                            </div>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" style="max-height: 270px">
                    <div style="padding: 0 10px;"id="modalBody1">No Data Available</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<form id="updateCategory" method="post">
    <div class="modal fade" id="faqsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update FAQ Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="recipient-name" class="form-control-label">Category Name:</label>
                    <input type="hidden" maxlength="100" class="form-control" id="category_id_edit" required>
                    <input type="text" maxlength="100" class="form-control" id="category_name_edit" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_server.js"></script>
