<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">FAQ</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="<?php echo base_url()?>" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url()?>Company/history_logs" class="m-nav__link">
                            <span class="m-nav__link-text">Monitoring</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="m-content" id="datatable_div">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-2">
                                    <div style="width: 100%" class="m-form__group m-form__group--inline">
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select m-bootstrap-select--solid m_selectpicker" id="searchType">
                                                <option value="">All Status</option>
                                                <option value="0" data-content="<span class='m-badge m-badge--brand m-badge--wide '>Draft</span>">Draft</option>
                                                <option value="1" data-content="<span class='m-badge m-badge--info m-badge--wide '>Publish</span>">Publish</option>
                                                <option value="2" data-content="<span class='m-badge m-badge--metal m-badge--wide '>Archive</span>">Archive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div style="width: 100%" class="m-form__group m-form__group--inline">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input id="searchDate" type="text" class="form-control m-input m-input--solid" readonly="" placeholder="Select time">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                <span><i class="la la-calendar"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="searchField">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="monitoring"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="faqsModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">FAQ Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modal_monitoring"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/src/custom/js/faqs/faqs_monitoring.js"></script>