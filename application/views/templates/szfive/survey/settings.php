<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Survey Page</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('szfive/survey'); ?>" class="m-nav__link">
                            <span class="m-nav__link-text">Survey</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="#" class="m-nav__link">
                            <span class="m-nav__link-text">Settings</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
            <div>
                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
                    aria-expanded="true">
                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                        <i class="la la-plus m--hide"></i>
                        <i class="la la-ellipsis-h"></i>
                    </a>
                    <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__section m-nav__section--first m--hide">
                                            <span class="m-nav__section-text">Quick Actions</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                <span class="m-nav__link-text">Activity</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                <span class="m-nav__link-text">Messages</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                <span class="m-nav__link-text">FAQ</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                <span class="m-nav__link-text">Support</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__separator m-nav__separator--fit">
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_builder_page" role="tab" aria-selected="true">
                                General
                            </a>
                        </li>
                    </ul>
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover"
                                aria-expanded="true">
                                <button type="submit" class="btn btn-danger" id="saveSettings">Save</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <form class="m-form m-form--label-align-left m-form--fit" action="#" method="POST" id="survey-settings">
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="m_builder_page">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-2 col-form-label">Default Survey</label>
                                <div class="col-lg-10 col-xl-4">
                                    <select class="form-control m-select2" id="settings_survey" name="survey">
                                        <?php foreach ($surveys as $key => $survey): ?>
                                        <option value="<?php echo $survey->surveyId; ?>" <?php if ($survey->isDefault == 1) {echo 'selected';} ;?>>
                                            <?php echo $survey->question; ?>
                                        </option>
                                        <?php endforeach;?>
                                    </select>
                                    <span class="m-form__help">Select a default survey to be displayed on the page.</span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row" style=" padding-bottom: 0; margin-bottom: -10px; ">
                                <label class="col-lg-2 col-form-label">User Permissions</label>
                            </div>
                            <div class="form-group m-form__group">
                                <div class="m-stack m-stack--ver m-stack--general m-stack--demo permission-list">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/src/custom/js/general.js'); ?>"></script>
<script type="text/javascript">

    function getPermissions() {
        $.when(
                fetchGetData('/szfive/get_permissions')
            )
            .then(function (response) {
                var data = jQuery.parseJSON(response.trim());
                $('#user_roles').remove();

                $.each(data.permissions, function (key, value) {

                    $('.permission-list').append(
                        '<div class="m-stack__item" id="user_roles"><label for=""><strong>' +
                        value.role + '</strong></label><input type="hidden" name="role_id" value="' +
                        value.role_id + '" /><div class="m-checkbox-list perm-list' + key + '">');

                    $.each(value.actions, function (key2, value2) {

                        var checked = ((value2[1] == '1') ? 'checked' : 'not');
                        $('.perm-list' + key).append(
                            '<label class="m-checkbox"><input type="checkbox" name="survey_actions" value="' +
                            key2 + '" ' + checked + '> ' + value2[0] + '<span></span></label>');
                    });

                    $('.permission-list').append('</div></div>');
                });
            });
    }
    // Save settings
    $('#saveSettings').on('click', function () {

        var surveyId = $('#settings_survey').val();
        var actions = [];
        var permissions = [];

        var dat = [];

        $("div #user_roles").each(function () {
            var role_id = $(this).find("input[name='role_id']").val();
            var input = $(this).find("input[name='survey_actions']:checked");

            $.each(input, function () {
                actions.push($(this).val());

            });

            data = {
                role_id: parseInt(role_id),
                actions: actions
            };

            permissions.push(data);
            actions = [];
        });

        $.ajax({
            url: baseUrl + "/szfive/save_settings",
            type: 'POST',
            dataType: "json",
            data: {
                surveyId: surveyId,
                permissions: permissions
            },
            success: function (data) {
                $('#select_survey').val(data);
                $('.permission-list').empty();
                getPermissions();

                swal({
                    "title": "",
                    "text": "Saved settings.",
                    "type": "success",
                    "timer": 2000,
                    "showConfirmButton": false
                });
            },
            error: function () {
                swal({
                    "title": "",
                    "text": "Error saving settings",
                    "type": "error",
                    "timer": 2000,
                    "showConfirmButton": false
                });
            }
        });
    });

    var SettingsQuestion = function () {
        var selectQuestion = function () {

            $('#settings_survey, #settings_survey_validate').select2({
                placeholder: "Select survey",
            });
        }
        return {
            init: function () {
                selectQuestion();
            }
        };
    }();

    jQuery(document).ready(function () {
        SettingsQuestion.init();
        getPermissions();

    });
</script>