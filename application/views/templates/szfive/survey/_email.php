<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#FAFAFA" style="margin-top:0px;margin-bottom:0;background-color:#fafafa;color:#4a4a4a;font-family:Helvetica,Arial,Verdana,Trebuchet MS,sans-serif;font-size:14px;line-height:1.4">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;max-width:584px">
                        <tbody>
                            <tr>
                                <td valign="top" style="padding:0 10px">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%;max-width:564px">
                                        <tbody>
                                            <tr>
                                                <td height="67" valign="top" align="left">
                                                </td>
                                                <td height="67" valign="middle" align="right">
                                                    <a href="http://10.200.101.107/sz/login" style="color:#4a4a4a;text-decoration:none" target="_blank">
                                                        <span style="font-weight:600;text-decoration:underline">Sign in
                                                        </span> to
                                                        <span class="il">SZ-System</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0 10px">
                                    <table width="100%" align="center" style="margin:0px;padding:0px;border-collapse:collapse;background-repeat: no-repeat;"
                                        border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" align="center" style="height:195px;margin:0px;padding:0px;border-collapse:collapse;height:195px;max-width:600px;background-repeat: no-repeat;"
                                                        border="0" cellspacing="0" cellpadding="20" background="https://image.ibb.co/bsqb8d/logo.jpg">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top">
                                                                    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="top">

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div style="margin-top:0;background:white">
                                        <div style="padding:8px 30px">
                                            <p style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#2c3b4a;letter-spacing:0.15px;line-height:22px;margin-top:0">
                                                Hi
                                                <?php echo $criteria['username']; ?>,
                                                <br>Here’s a summary of what happened with the
                                                <span class="il">SupportZebra </span> Daily Survey in the past 7 days.
                                            </p>
                                        </div>
                                    </div>
                                    <div style="margin-top:30px;background:white;text-align:center">
                                        <div style="padding:27px 30px;padding:60px 30px 10px">
                                            <p style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#2c3b4a;letter-spacing:0.15px;line-height:22px;margin-top:0;line-height:20px">
                                                There has been
                                                
                                                    <?php if ($criteria['pulse_stat'] == 'increase'): ?>
                                                    <?php echo 'an <span style="color:#c73821">' . $criteria['pulse_stat'] .'</span>'; ?>
                                                    <?php else: ?>
                                                    <?php echo 'a <span style="color:#c73821">' . $criteria['pulse_stat'] .'</span>'; ?>
                                                    <?php endif;?>
                                                in the company-wide
                                                <strong> Pulse </strong>
                                            </p>
                                            <p style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#2c3b4a;letter-spacing:0.15px;line-height:22px;margin-top:0;font-size:14px;line-height:14px;color:#556370">
                                                <?php echo $criteria['current_week']; ?>
                                            </p>
                                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#2c3b4a;letter-spacing:0.15px;line-height:22px;font-size:50px;line-height:50px;margin:25px 0 20px;color:#c73821">
                                                <?php echo $criteria['pulse_avg']; ?>
                                                <br>
                                                <small style="
    font-size: 13px;
">Last week:
                                                    <?php echo $criteria['pulse_last_avg']; ?>
                                                </small>
                                            </div>
                                        </div>
                                        <img src="https://ci4.googleusercontent.com/proxy/RdTZ43JZlYj_-X-GgmnAS6fo2V2uOXymruWAZCDz-1ouXkrtnEAfFammWQ9SbVlhJyjCBa2SbsxpSADgz8ajVSdFb-aUf7sHOrad0J9H2EffW25hXdBQ9AGxTx-RJ7s8S1EWz_lX=s0-d-e1-ft#https://d1k0kynzlzod3k.cloudfront.net/static/images/email/admin-digest/chart.jpg"
                                            style="width:100%" alt="Chart" class="CToWUd a6T" tabindex="0">
                                        <div class="a6S" dir="ltr" style="opacity: 0.01; left: 783.453px; top: 1390.59px;">
                                            <div id=":lw" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V"
                                                data-tooltip="Download">
                                                <div class="aSK J-J5-Ji aYr"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-top:20px;text-align:center">
                                        <?php foreach ($criteria['high_fivers'] as $high_fiver): ?>
                                        <div style="width:100%;max-width:275px;display:inline-block;vertical-align:top">
                                            <table style="width:100%">
                                                <tbody>
                                                    <tr>
                                                        <td style="padding:10px 10px 0 10px">
                                                            <table width="100%" align="center" style="margin:0px;padding:0px;border-collapse:collapse" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" valign="top">
                                                                            <table width="100%" align="center" style="height:195px;margin:0px;padding:0px;border-collapse:collapse;height:195px;max-width:600px;background-color:#efefef"
                                                                                border="0" cellspacing="0" cellpadding="20">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center" valign="top" style="vertical-align:middle">
                                                                                            <table width="100%" align="center" style="margin:0px auto;padding:0px;max-width:100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <div style="width:140px;height:140px;text-align:center;font-size:40px;overflow:hidden;border-radius:50%;border:4px solid white;margin:0 auto 5px">
                                                                                                                <img src="https://preview.ibb.co/iRQ18d/sz.png" width="140" height="140" style="border-radius:50%" class="CToWUd">
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding:0 10px 10px 10px">
                                                            <div style="background:white;text-align:center">
                                                                <div style="padding:27px 30px">
                                                                    <div>
                                                                        <p style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#2c3b4a;letter-spacing:0.15px;line-height:22px;font-size:22px">
                                                                            <?php echo $high_fiver->fname . ' ' . $high_fiver->lname; ?>
                                                                        </p>
                                                                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#2c3b4a;letter-spacing:0.15px;line-height:22px;margin:20px auto 10px;max-width:140px;font-size:16px;color:#556370">
                                                                            <?php echo $high_fiver->msg; ?>
                                                                        </div>
                                                                        <span>
                                                                        </span>
                                                                    </div>
                                                                    <div style="text-align:center;margin-top:20px">
                                                                        <a href="http://10.200.101.107/sz/szfive/highfive" style="display:inline-block;background-color:#ffffff;border:1px solid #e2e2e2;color:#3399cc;font-family:Helvetica,Arial,sans-serif;font-size:14px;line-height:1;padding:12px 20px;text-align:center;text-decoration:none;font-weight:lighter;border-radius:4px 4px 4px 4px;margin:0 0 10px;white-space:nowrap;font-size:16px;padding:18px 30px"
                                                                            target="_blank">
                                                                            See High Fives </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php endforeach;?>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>