<style>
#reviewTextarea {
    display: none;
}

.m-portlet {
    margin-bottom: 10px;
}

.m-portlet__body {
    padding-top: 15px !important;
}

.m-widget5__pic {
    margin: 0 auto;
    text-align: center;
    vertical-align: middle;
}

.m-widget5__title {
    margin-bottom: 0;
    font-size: 24px !important;
    font-weight: 300 !important;
}
/*Charise code*/

.sendfeedback{
    padding-top:10px !important;

}
.autoExpand {
    margin-bottom: 10px;
    height: 80px !important;
    resize: none; 
    overflow: hidden;
}
.autoNonExpand {
    margin-bottom: 10px;
    height: 37px !important;
    resize: none; 
    overflow: hidden;
}
.comment-cwrapper {
    background: #eeffda;
    display: table;
    margin-bottom: 10px;
    border-radius: 10px;
    padding: 13px;
    width: 100%;
}
.comment-cwrapper-agreed{
 background: #e8ffce;
 display: table;
 margin-bottom: 10px;
 border-radius: 20px;
 padding: 13px;
 width: 100%;
}
.comment-cwrapper-disagreed{
 background: #ffedeb;
 display: table;
 margin-bottom: 10px;
 border-radius: 20px;
 padding: 13px;
 width: 100%;
}
.comment-cwrapper_comment{
 background: #f1f0ef;
 display: table;
 margin-bottom: 10px;
 border-radius: 10px;
 padding: 13px;
 width: 96%;
 margin-left: 30px;
}
.comment-pic {
    width: 3rem;
    border-radius: 50%;
    margin: 2px -3px 0 3px;
    height: 3rem;
    border-width: 2px;
    border-style: double;
    border-color: #ded8d8;
}
.comment-info {
    display: table-cell;
    width: 100%;
    padding-left: 1rem;
    font-size: 1rem;
    vertical-align: middle;
}
.comment-name {
    font-size: 12px;
    font-weight: 500;
}
.comment-content {
    font-size: 13px;
    color: #000;
}
.comments-container_class{
 padding-top: 10px;
}
.modal-display-header{
    padding:21px !important;
    background: #2c2e3e !important;
}
.agree_label{
    font-size: 0.9em !important;
    color:#1d9422;
}
.disagree_label{
    font-size: 0.9em !important;
    color:#ff6d6d;
}
.modal_textarea{
    height: 150px !important;
    background: #f1f1f1 !important;
    border-color: #a6abb7 !important;
}
.review_quote {
    border-top: 5px solid #009688!important;
    background: #f3f3f3!important;
}
.modal-t-class{
 font-size: 1.2em !important; 
}
.agree_button{
    color: #4d87b3 !important;
}
.feedback_buttons{
    color:#098c70 !important;
}
.display_survey_answers{
    border-style: dashed;
    padding: 15px;
    background: #ebedf2;
    border-color: #bac1c1 !important;
    margin: auto;
    width: 55%;
    text-align: center;
}
/*End charise code*/
</style>
<script>
    function noImageA(id) {
        $('.user-pic-answer' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }

    function noImageSup(id) {
        $('.supervisor-pic-' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <?php $sub_header = ['title' => 'Survey', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'Daily Survey', 'link' => 'szfive/daily_survey'], ['title' => 'User Answer', 'link' => uri_string()]]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-9">
                <div class="m-portlet m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded" style="margin-bottom: 20px;">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="m-widget5">
                                    <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 0;">
                                        <div class="m-widget5__pic">
                                            <img class="m-widget7__img user-pic-answer<?php echo $entry->uid; ?>" src="<?php echo $entry->pic; ?>" onerror="noImageA(<?php echo $entry->uid; ?>)"
                                            alt="" style="border-radius: 50%; width: 4rem;border: 1px solid #eee;">
                                        </div>
                                        <div class="m-widget5__content">
                                            <h1 class="m-widget5__title">
                                                <?php echo $entry->fullname; ?>
                                            </h1>
                                            <span class="m-widget5__desc">
                                                <?php echo $entry->department; ?>
                                            </span>
                                            <br>
                                            <span class="m-widget5__desc" style="color: #48ad4c;ont-size: 13px;font-weight: 500;">                                               
                                                <?php echo $entry->position; ?>
                                            </span>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-widget5">
                                    <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 0;">
                                        <div class="m-widget5__content" style="padding-top: 10px;padding-left: 0;text-align: right;">
                                            <span class="m-widget5__desc" style="font-size: 11px;">
                                                Submitted on
                                                <span style=" font-weight: 500; ">
                                                    <?php echo $entry->date_answered; ?>
                                                </span>
                                            </span>
                                            <br>
                                            <?php if ($entry->isReviewed == 1): ?>
                                                <span class="m-widget5__desc" style="font-size: 11px;">
                                                    Reviewed by
                                                    <span style=" font-weight: 500; ">
                                                        <?php echo $entry->supervisorFname . ' ' . $entry->supervisorLname; ?>
                                                    </span> on
                                                    <span style=" font-weight: 500; ">
                                                        <?php echo $entry->date_reviewed; ?>
                                                    </span>
                                                </span>
                                                <?php else: ?>
                                                    <?php if($entry->supId == $session['emp_id']) : ?>
                                                        <button type="button" class="btn m-btn--pill btn-outline-success btn-sm " onClick="viewReviewsDetails(<?php echo $entry->answerId; ?>)"
                                                            style="margin-top: 8px;">Review</button>
                                                        <?php endif;?>
                                                    <?php endif;?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded">
                            <div class="m-portlet__body" style="margin-top: 10px;">
                                <div class="answersDisplay display_survey_answers">

                                </div>
                                <!-- #Charise code -->
                                <!-- Display of comments -->
                                <div id="comments-container" style="padding-top: 20px !important;border-top: 1px dashed #d4d4d4;margin-top: 25px !important;">
                                    <!-- Single div for one comment -->  
                                </div>
                                <!-- TEXT AREA -->
                                <form id="commentTextarea" name="form-sendfeedbacks" class="m-form">
                                    <textarea class='form-control m-input highfive_post_comment sending_feedback' name='feedback_desc' id='inputHi5Comment' rows='1' data-min-rows='1' style='resize: none; overflow: hidden; height: 37px;display: none;background-color: #f1f1f1' placeholder='Say something about this...' data-mentions-input='true'></textarea>
                                    <div id="comment-options" class="sendfeedback sending_feedback" style="display: block;display: none;">
                                    </div>
                                </form>
                                <!-- #End charise code -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <div style="text-align: center;">
                                    <div style="margin-bottom: 10px; margin-top: 15px;">
                                        <img class="supervisor-pic-<?php echo $entry->supId; ?>" src="<?php echo base_url('assets/images/') . $entry->supervisorPic; ?>"
                                        onerror="noImageSup(<?php echo $entry->supId; ?>)" style="border-radius: 50%;width: 4rem;border: 1px solid #eee;"
                                        />
                                    </div>
                                    <p style="font-size: 13px;">
                                        <span style="font-weight: 500;"><?php echo $entry->supervisorFname . ' ' . $entry->supervisorLname; ?></span>
                                        <br>
                                        <span>Reviewer</span>
                                        <br>
                                        <?php if($entry->isReviewed == 1){ ?>
                                            <span class="m--font-success">
                                                <i class="fa fa-check m--font-success" style="font-size: 12px;"></i> Reviewed
                                            </span> <br>
                                            <span class="m--font-success">
                                             <small><?php echo date("F d, Y h:i:s A",strtotime($entry->dateCreated)); ?></small>
                                         </span>
                                         <br>
                                         <?php if($entry->message!=null){ ?>
                                             <div class="col-md-12 mb-3">
                                                <div class="col-md-12 review_quote pt-3 pb-1">
                                                   <blockquote>
                                                      <i class="fa fa-quote-left" style="font-size: 10px !important;"></i>
                                                      <span class="mb-0 text-capitalize" id="reasonRecord"><?php echo $entry->message; ?></span>
                                                      <i class="fa fa-quote-right" style="font-size: 10px !important;"></i>
                                                  </blockquote>
                                              </div>
                                          </div>
                                      <?php } ?>
                                  <?php }else{ ?>
                                <!-- <span class="m--font-danger">
                                    <i class="fa fa-close m--font-danger" style="font-size: 12px;"></i> Not yet reviewed</span> -->
                                <?php }?>
                            </p>
                        </div>
                    </div>
                </div>
                        <div class="m-portlet m-portlet--mobile" id="followupDiv" style="disply:none">
                            <div class="m-portlet__body" style="padding: 1px;padding-top: 0px !important;">
                                <div style="text-align: center;">
                                    <p style="font-size: 13px;">
                                        <br>
                                       
                                          
                                             <div class="col-md-12 mb-3">
                                                <div class="col-md-12 review_quote pt-3 pb-1">
 												 <h6 id="followupQuest"></h6>
												 <div id="comments-container" style="border-top: 1px dashed #d4d4d4;"></div>
												 <small id="followupAnswerDate" class="m--font-success"></small>
												 <br>
                                                   <blockquote>
                                                      <i class="fa fa-quote-left" style="font-size: 10px !important;"></i>
                                                      <span class="mb-0 text-capitalize" id="followUpAnswer"></span>
                                                      <i class="fa fa-quote-right" style="font-size: 10px !important;"></i>
													  <br>
													  <small style="float:right;" id="followupAnsEmp"> </small>
                                                  </blockquote>
												  
                                              </div>
                                          </div>
                                      
                                 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- start:: Reviews - View Specific Answer modal -->
        <div class="modal fade" id="view_specific_ans_reviews_modals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: white;padding: 10px;">
                    <div id="view_answer_review_header"></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #599791 !important;margin: 0;">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="view_reviews_answer_content"></div>
                    <div id="mark_as_review_wrapper"></div>
                    <div id="reviewTextarea" style="margin-top: 20px;">
                        <form class="m-form m-form--fit m-form--label-align-right" id="reviewAnswerForm">
                            <div class="form-group m-form__group" style=" padding: 0; ">
                                <input type="hidden" name="answerId" />
                                <textarea class="form-control m-input m-input--air" name="reviewMessage" id="reviewAnswerTextarea" rows="3 "></textarea>
                                <span class="m-form__help">Please note: This field will appear if answer requires a remark/comment.</span>
                                <div class="m-form__actions" style=" padding: 10px 0; ">
                                    <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn-sm"
                                    id="submitReview">Submit</button>
                                    <button type="reset" id="cancelReview" class="btn m-btn--pill btn-secondary btn-sm">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="alreadyReviewed"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="add_comment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="form_comment-disagree" name="form_comment-disagree">
            <div class="modal-content">
              <div class="modal-header modal-display-header">
                <i class="flaticon-speech-bubble" style="color:#5be6f7"></i>&nbsp;&nbsp; <h3 class="modal-title modal-t-class disagreed_toplabel" id="exampleModalLabel">Please let us know why you disagreed to the feedback </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
                <label class="disagreed_label form-control-label" for="message-text message-displayclass" style="font-size: 1.3em"></label>
                <textarea name="comment_reply" class="form-control modal_textarea" id="comment_reply-feedback" placeholder="Feel free to type your reasons here..."></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
            <button type="submit" class="btn btn-accent"><i class="fa fa-send-o"></i> Send your comment</button>
        </div>
    </div>
</form>
</div>
</div>
<!--end::Modal-->
<script>
    var imagePath = baseUrl + '/assets/images/';

    $('#btn-reviewAnswer').on('click', function () {
        $('#reviewTextarea').slideDown();
    });

    $('#cancelReview').on('click', function () {
        $('#reviewTextarea').slideUp();
        $('#submitReview').removeClass('m-loader m-loader--light m-loader--right');

    });

    $('#submitReview').on('click', function () {
        $(this).addClass('m-loader m-loader--light m-loader--right');
    });

    var FormValidation = function () {

        var reviewForm = function () {
            $('#reviewAnswerForm').validate({
                rules: {
                    reviewMessage: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                },
                invalidHandler: function (event, validator) {
                    $('#submitReview').removeClass('m-loader m-loader--light m-loader--right');

                    swal({
                        "title": "",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },
                submitHandler: function (form) {

                    var message = $('#reviewAnswerForm').find('textarea#reviewAnswerTextarea').val();
                    var answerId = $('#reviewAnswerForm').find('input[name=answerId]').val();
                    message = message.replace(/ +(?= )/g, '');

                    var options = {
                        url: baseUrl + '/szfive/submit_review_answer',
                        type: 'POST',
                        dataType: "json",
                        data: {
                            message: message,
                            answerId: answerId
                        },
                        success: function (response) {

                            if (response.status) {

                                swal({
                                    "title": "",
                                    "text": "You have successfully reviewed this answer.",
                                    "type": "success",
                                    "timer": 1000,
                                    "showConfirmButton": false
                                });
                                $('#view_specific_ans_reviews_modals').modal('hide');
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            }
                        },
                        error: function () {
                            alert('error');
                        },
                    };

                    $(form).ajaxSubmit(options);
                }
            })
        }
        // #CHARISE CODE
        //SEND FEEDBACKS
        var send_feedbacks= function(){
          $('#commentTextarea').validate({
              rules: {
                feedback_desc: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {
                var feedback_content=$("#inputHi5Comment").val();
                var intervenee_uid=<?php echo $entry->uid; ?>;
                var answer_id=$("#feeling-answer").data('answerid');
                var intervener_emp_id=$("#feeling-answer").data('intervener_id');
                swal({
                    title: 'Are you sure you want to send this feedback?',
                    text: "You cannot delete or modify this feedback once sent.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, send my feedback.'
                }).then(function(result) {
                    if (result.value) {
                        var options = {
                            url: baseUrl + '/dailypulse/submit_feedback',
                            type: 'POST',
                            dataType: "json",
                            data: {
                                feedback: feedback_content,
                                answer_id: answer_id,
                                intervener_empid: intervener_emp_id
                            },
                            success: function (res) {
                                systemNotification(res.notifid.notif_id,'snapsz');
                                // notification(res.notifid.notif_id);
                                swal(
                                    'Sent!',
                                    'You have successfully submitted your feedback.',
                                    'success'
                                    )
                                display_survey_answersinfo(answer_id);
                                $(".sending_feedback").remove();
                            },
                            error: function (res) {
                             swal(
                                'Oops!',
                                'You cannot send feedback!',
                                'error'
                                )
                         },
                     };
                     $(form).ajaxSubmit(options);
                 }
             });
            }
        })
      }

      var send_comments= function(){
          $('#form_comment-disagree').validate({
              rules: {
                comment_reply: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
            },
            invalidHandler: function (event, validator) {
                swal({
                    "title": "",
                    "text": "The Field cannot be empty. Please type your comment.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },
            submitHandler: function (form) {
                var comment_content=$("#comment_reply-feedback").val();
                var interven_id=$("#comment-content").data('intervenid');
                var empid=$("#comment-content").data('empid');
                var ansid=$("#comment-content").data('answerid');
                swal({
                    title: 'Are you sure you want to send this comment?',
                    text: "You cannot delete or modify this comment once sent.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, send my comment.'
                }).then(function(result) {
                    if (result.value) {
                      var options = {
                        url: baseUrl + '/dailypulse/submit_comment',
                        type: 'POST',
                        dataType: "json",
                        data: {
                            comment_content: comment_content,
                            interven_id: interven_id,
                            empid: empid,
                            answer_id: ansid
                        },
                        success: function (response) {
                           systemNotification(response.notifid.notif_id,'snapsz');
                           swal(
                            'Sent!',
                            'You have successfully submitted your comment.',
                            'success'
                            )
                           display_survey_answersinfo(ansid);
                           $("#add_comment-modal").modal('hide');
                       },
                       error: function (response) {
                        alert("Error!");
                    },
                };
                $(form).ajaxSubmit(options);
            }
        });
            //
        }
    })
      }

      //END OF CHARISE CODE
      return {
        init: function () {
            reviewForm();
            send_feedbacks();
            send_comments();
        }
    };
}();
jQuery(document).ready(function () {
    FormValidation.init();
    display_survey_answersinfo('<?php echo $entry->answerId; ?>');
});

function viewReviewsDetails(answerId) {
    $('#alreadyReviewed').html('');

    $.when(fetchPostData({
        answerId: answerId
    }, '/szfive/get_reviews_specific_answer'))
    .then(function (data) {

        var data = jQuery.parseJSON(data);

        header =
        '<div class="m-widget3__header" style="display: table;padding: 0px 10px;padding-top: 8px; ">';
        header += '        <div class="m-widget3__user-img" style="margin-bottom: .7rem;">';
        header += '            <img class="m-widget3__img answer-pic-dt' + data.uid + '" src="' + data.pic +
        '" onerror="noImage2(' + data.uid + ')" alt="" style="width: 3.2rem;border-radius: 50%;">';
        header += '        </div>';
        header +=
        '        <div class="m-widget3__info" style="display:table-cell;width:100%; padding-left:1rem;font-size:1rem;vertical-align:middle;">';
        header += '            <span class="m-widget3__username" style="font-size: 15px;color: #36817a;">';
        header += '                ' + data.fullname + ' answered ';
        header += '                <u style=" font-size: 17px;">' + data.answer + '</u> out of';
        header += '                <u>' + data.numberOfChoices + '</u> for the daily survey on ' + data.survey_date +
        '.';
        header += '            </span>';
        header += '            <br>';
        header += '            <span class="m-widget3__time" style="font-size: .85rem;">' + data.answer_date +
        '</span>';
        header += '        </div>';
        header += '    </div>';

        $('#view_answer_review_header').html(header);

        str = '<div class="m-widget3__item">';
        str += '    <div class="m-widget3__body" id="review_userAnswerId" data-id="' + answerId +
        '" style=" padding: 0px 10px; margin-bottom: 25px; ">';
        str += '        <blockquote class="blockquote">';
        str += '            <p class="mb-0" style=" font-weight: 400;padding: 0 0 10px 0;">' + data.detailedAnswer +
        '</p>';
        str += '            <footer class="blockquote-footer">Feeling';
        str += '                <img src="' + imagePath + data.choice_img + '" style=" width: 20px; ">';
        str += '                <cite title="Source Title">';
        str += '                    ' + data.choice_label + ' </cite>';
        str += '            </footer>';
        str += '        </blockquote>';
        str += '     </div>';
        str += '    </div>'

        $('.view_reviews_answer_content').html(str);

        var str_review = '';

        if (parseInt(data.isReviewed) == 1) {

            var dateReviewed = moment(data.dateReviewed).format('MMMM D, YYYY hh:mm:ss A');
            var message = data.message ? data.message : 'No remarks';

            str_review +=
            '<span class="m-badge m-badge--success m-badge--wide">Reviewed by</span><span style="color: #649e99;"> <b style="font-weight: 400;">' +
            data.supervisorFname + ' ' + data.supervisorLname + '</b> on <b style="font-weight: 400;">' +
            dateReviewed + '</b></span>';
            str_review +=
            '<p style="border: 4px solid #f7f7fa;padding: 8px;margin-top: 10px;"><b style="color: #000000b5;font-weight: 600;">Remarks:</b> &nbsp;&nbsp;' +
            message + '</p>';
            str_review += '<small>Reviewed <b><i>' + data.time_diff +
            '</i></b> after the team member answered the survey.</small>';
            $('#reviewTextarea').hide();
        } else {
            if (data.reviewerId == data.session_id) {
                $('#reviewTextarea').hide();
                $('#reviewAnswerForm').find('input[name=answerId]').val(answerId);

                str_review +=
                '<button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-info m-btn--gradient-to-accent btn-sm" id="btn-reviewAnswer" onClick="markAsReviewed(' +
                data.answer + ',' + data.numberOfChoices + ')">Mark as Reviewed</button>';
            } else {
                str_review += '<span class="m-badge m-badge--danger m-badge--wide">Unreviewed</span>';
            }
        }

        $('#mark_as_review_wrapper').html(str_review);
    });

setTimeout(function () {
    $('#view_specific_ans_reviews_modals').modal('show');
}, 500);
}

function markAsReviewed(answer, numberOfChoices) {

    var numbers = [1];
    numbers.push(numberOfChoices);

    var median = 0;
    var numsLen = numbers.length;
    numbers.sort();

        if (numsLen % 2 === 0) { // is even
            // average of two middle numbers
            median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
        } else { // is odd
            // middle number only
            median = numbers[(numsLen - 1) / 2];
        }

        if (answer <= median) {
            $('#reviewTextarea').slideDown();
        } else {
            $('#reviewTextarea').css("display", "none");
            $.when(fetchPostData({
                message: '',
                answerId: $('#review_userAnswerId').data('id')
            }, '/szfive/submit_review_answer'))
            .then(function (response) {
                var resp = jQuery.parseJSON(response);

                if (resp.status == true) {

                    var data = resp.data;
                    swal({
                        "title": "",
                        "text": "You have successfully reviewed this answer.",
                        "type": "success",
                        "timer": 1500,
                        "showConfirmButton": false
                    });
                    location.reload();
                } else {
                    swal({
                        "title": "Error",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                }
            });
        }
    }
// #Charise code
$("#inputHi5Comment").on("focus", function () {
    $(this).addClass("autoExpand");
    $(this).removeClass("autoNonExpand");
    var string_button="";
    string_button+="<button type='submit' class='btn btn-accent' id=''><i class='fa fa-send-o'></i> Send Feedback</button>";
    $("#comment-options").html(string_button);
});
 
$('#inputHi5Comment').on('focusout', function () {
        if ($('#inputHi5Comment').val() === '') {
			$(this).addClass("autoNonExpand");
			$(this).removeClass("autoExpand");

			var string_button="";
			$("#comment-options").html(string_button);
        }
    });

   
// DISPLAY ANSWERS
function display_survey_answersinfo(answerid){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dailypulse/fetch_answersinfo/"+answerid,
    cache: false,
    success: function (res) {
        var result= JSON.parse(res.trim());
        console.log(result);
        var string_answer="";
        var string_feedback="";
        var string_box_feedback="";
        var string_comment="";
        var answer_detail='" '+result.answers[0].detailedAnswer+' "';
        var in_comment=0;
        var count_comment=0;
        if(result.answers[0].comment_ID!=null){
            count_comment=result.comment_details.length;
        }
        console.log("count this"+count_comment);
        var gif_name= result.answers[0].gif.replace("img/","");
        var src_emoticon="<?php echo base_url(); ?>assets/images/img/"+gif_name;

        string_answer+="<img  src='"+src_emoticon+"' width='75' alt='' class='rounded-circle mx-auto d-block'>";
        string_answer+="<h5 style='color: #555;font-weight: 400;border-bottom: 1px solid #eee;'>Feeling "+result.answers[0].answer+"/5</h5>";
        string_answer+="<p id='feeling-answer' data-intervener_id='"+result.answers[0].intervenee+"' data-answerid='"+result.answers[0].answerId+"' style='font-size: 16px;margin-bottom: 10px;color: #555;margin-top: 10px; font-weight: 400;color: #579659'>"+answer_detail+"</p>";
        string_answer+="<cite title='Feeling' style'font-style: normal;color: #a7a7a7; font-size: 13px;'> - Feeling "+result.answers[0].choice_label+"";
            // string_answer+="<img src='<?php echo base_url( 'assets/images/') . $entry->choice_img; ?>' style=' width: 20px;'> </cite>";
			
            if(result.answers[0].isAnsweredFollowQues!=null){
 				var st = (result.answers[0].isAnsweredFollowQues).split("|");			
				var fa1 = "Follow-up Question: "+st[2]+"<br>";
 				var fa2 = st[0]+"<br>";
 				var fa3 = st[1]+"<br>";
 				$("#followUpAnswer").html(fa3);
				$("#followupQuest").html(fa1);
				$("#followupAnswerDate").html(fa2);
				$("#followupAnsEmp").html(" - "+result.answer_employeeid[0].fname);
				$("#followupDiv").css("display","block");

				
			}else{
				$("#followUpAnswer").html("");
				$("#followupQuest").html("");
				$("#followupAnswerDate").html("");
				$("#followupAnsEmp").html("");
				$("#followupDiv").css("display","none");
			}
            if(result.answers[0].intervention_ID!=null){
                if(result.answers[0].feedback!=""){

                    if(result.answers[0].employeeConfirmed!="1"){
                        if(count_comment==3){
                           string_feedback+="<div class='m-widget3__header comment-cwrapper-disagreed comment-cwrapper' id=''>";
                       }else{
                           string_feedback+="<div class='m-widget3__header comment-cwrapper comment-cwrapper1085' id=''>";
                       }
                   }
                   else{
                    string_feedback+="<div class='m-widget3__header comment-cwrapper-agreed comment-cwrapper1085' id=''>";
                }

                //Get photo thumbnail for intervener
                var picName = result.feedback_empdetails[0].pic;
                // var picName = result.feedback_empdetails[0].pic.substr(0, result.feedback_empdetails[0].pic.indexOf(".jpg"));
                // var pic = imagePath + "" + picName + "_thumbnail.jpg";
                var pic = imagePath + "" + picName;
                string_feedback+="<div class='m-widget3__user-img' id='comment_type' data-id='1'>";
                string_feedback+="<img class='m-widget3__img comment-pic comment-pic-dt1016' src='"+pic+"' onerror='noImageFound(this)' alt=''>";
                string_feedback+="</div>";
                string_feedback+="<div class='m-widget3__info comment-info'><span class='m-widget3__username comment-name'>"+result.feedback_empdetails[0].fname+" "+result.feedback_empdetails[0].lname+"</span>"; 
                string_feedback+="<span style='font-size: 11px;font-style: italic;margin-left: 5px;'>"+result.interventionDate+" &nbsp;<i class='fa fa-globe' style='font-size: 12px;color: #767e84d6;'></i></span>";
                string_feedback+="<p data-intervenee='"+result.answers[0].emp_id+"' data-intervener='"+result.answers[0].intervenee+"' data-answerid='"+result.answers[0].answerId+"' class='m-widget3__time comment-content' id='comment-content' data-empid="+result.employee[0].emp_id+" data-intervenid="+result.answers[0].intervention_ID+" style='margin-bottom: 0;margin-top:3px;'>"+result.answers[0].feedback+"</p>";
                if(result.answers[0].intervention_ID!=null){
                    // if((result.answers[0].emp_id==result.employee[0].emp_id) && (result.answers[0].employeeConfirmed!="1" && count_comment!=3) ){
                        if(result.answers[0].emp_id==result.employee[0].emp_id && result.answers[0].employeeConfirmed!="1" && count_comment!=3){

                            if(result.answers[0].comment_ID==null){
                             string_feedback+="<hr><div style='margin-top:4px'>";
                             string_feedback+="<button type='button' onclick='feedback_agreed("+result.answers[0].answerId+")' class='reaction tooltipstered feedback_buttons' style='margin-right: 50px;font-size:0.8em;border:none;background:none'><i class='fa fa-thumbs-o-up'></i> Agree</button>";
                             string_feedback+="<a href='' class='reaction tooltipstered feedback_buttons' style='margin-right: 10px;font-size:0.8em' data-toggle='modal' data-target='#add_comment-modal'><i class='fa fa-thumbs-o-down'></i> Disagree</a>";
                             string_feedback+="</div></div></div>";
                         }

                     }

                 }
                 if(result.answers[0].employeeConfirmed=="1"){
                     string_feedback+="<i class='fa fa-check' style='color:#34ca34'></i><span class='agree_label'> Agreed by respondent</span>"
                 }else if(result.answers[0].employeeConfirmed=="0" && count_comment==3){
                    string_feedback+="<i class='fa fa-remove' style='color:#ff6d6d'></i><span class='disagree_label'> Disagreed by respondent</span>"
                }
                string_feedback+="</div>";
                string_feedback+="</div>";
                string_feedback+="</div>";
                string_feedback+="<div class='commentdiv'></div>";

                $.each(result.comment_details, function (key, item) {
                    var picName_com = item.pic;
                    var pic_comment = imagePath + "" + picName_com;
                    console.log(pic_comment);
                    in_comment++;
                    string_comment+="<div class='m-widget3__header comment-cwrapper_comment comment-cwrapper1085' id=''>"
                    string_comment+="<div class='m-widget3__user-img' id='comment_type' data-id='1'>";
                    string_comment+="<img class='m-widget3__img comment-pic comment-pic-dt1016' src='"+pic_comment+"' onerror='noImageFound(this)' alt=''>";
                    string_comment+="</div>";
                    string_comment+="<div class='m-widget3__info comment-info'><span class='m-widget3__username comment-name'>"+item.fname+" "+item.lname+"</span>"; 
                    string_comment+="<span style='font-size: 11px;font-style: italic;margin-left: 5px;'>"+item.elapseTime+" &nbsp;<i class='fa fa-globe' style='font-size: 12px;color: #767e84d6;'></i></span>";
                    string_comment+="<p class='m-widget3__time comment-content' id='comment-content' data-empid='' data-intervenid='' style='margin-bottom: 0;margin-top:3px;'>"+item.comment+"</p>";
                    if(result.answers[0].intervenee==result.employee[0].emp_id || result.answers[0].emp_id==result.employee[0].emp_id){
                        if(result.answers[0].employeeConfirmed!="1"){
                            if(in_comment==count_comment && (count_comment==1 || count_comment==2)){     
                             if(item.emp_id!=result.employee[0].emp_id){
                                 string_comment+="<hr><div style='margin-top:5px'>";

                             //if Intervener is equal to session
                             if(result.answers[0].emp_id==result.employee[0].emp_id && count_comment!=3){
                                 string_comment+="<button type='button' onclick='feedback_agreed("+result.answers[0].answerId+")' class='reaction tooltipstered agree_button' style='margin-right: 50px;font-size:0.8em;border:none;background:none'><i class='fa fa-thumbs-o-up'></i> Agree</button>";
                             }
                             string_comment+="<a href='' id='agree_button' onclick='hide_label()' class='reaction tooltipstered agree_button' style='margin-right: 10px;font-size:0.8em' data-toggle='modal' data-target='#add_comment-modal'><i class='fa fa-comment-o'></i> Comment</a>";
                             string_comment+="</div>";
                         }
                     }
                 }
             }
             string_comment+="</div>";
             string_comment+="</div>";
         });
            }
        }

        $(".answersDisplay").html(string_answer);
        $("#comments-container").html(string_feedback);
        if(result.answer_employeeid[0].emp_id==result.employee[0].emp_id || result.answers[0].feedback!="" || result.answers[0].status_ID==12){
         $(".sending_feedback").remove();
     }else if(result.answers[0].intervenee==result.employee[0].emp_id && result.answers[0].feedback==""){ 
         $(".sending_feedback").show();
     }
     if(result.answers[0].comment_ID!=null){
       $(".commentdiv").html(string_comment);
   }
}
});
}
function feedback_agreed(answerid){
    var intervener=$("#comment-content").data('intervener');
    var intervenee=$("#comment-content").data('intervenee');
    swal({
        title: 'Are you sure you want to agree with the comment?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, I agree!'
    }).then(function(result) {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>dailypulse/feedback_agreed/"+answerid+"/"+intervener+"/"+intervenee,
            cache: false,
            success: function (res) {
                var resObj = $.parseJSON(res.trim())
                // notification(resObj.notifid.notif_id);
                systemNotification(resObj.notifid.notif_id,'snapsz');
                swal(
                    'Agreed!',
                    ' successfully agreed.',
                    'success'
                    )
                display_survey_answersinfo(answerid);
            },
            error: function (res){
              swal(
                'Oops!',
                'You cant agree!',
                'error'
                )
          }
      });
      }else if (result.dismiss === 'cancel') {
          swal(
            'Oops,You changed your mind!',
            'Agreeing a feedback has been cancelled',
            'error'
            )
      }
  });
}
function hide_label(){
    $('.disagreed_toplabel').text();
    $('.disagreed_toplabel').text("What can you say about it?");
    $('#comment_reply-feedback').attr("placeholder", "Type your comment here...");
}

// #End of Charise's code
</script>