<style>

th, td {
white-space: nowrap;
}

.first-col {
position: absolute;
width: 8em;
margin-left: -8em;
margin-top: -1px;
background:#d4d4d4b3;
}

.table-wrapper {
overflow-x: scroll;
margin: 0 0 0 8rem;
border: 1px solid #d2d2d2;
}

.bootstrap-select.btn-group .dropdown-menu.inner {
    max-height: 250px !important;
}

.bs-popover-auto[x-placement^=right] .arrow::after, .bs-popover-right .arrow::after {
border-right-color: #282A3C !important;
}
.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
border-left-color: #282A3C !important;
}
.popover-header{
background-color: rgb(52, 55, 78)!important;
border-bottom: 0px !important;
color: white;
max-width: 100% !important;
}
.popover-body{
background-color: #282A3C!important;
max-width: 100% !important;
padding: 1.0rem 0.5rem !important;
text-align: center;
padding-bottom: 1px !important;
}


.table-bordered td {
    border: 1px solid #dadada;
}

.editableStyle {
    border-bottom: dashed 1px #0088cc;
    cursor: pointer;
}

</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/demo/default/custom/components/custom/toastr/build/toastr.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.css">
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class=" align-items-center">
            <div class="mr-auto">
                <h3 id="processId" class="m-subheader__title m-subheader__title--separator"><span><i class="fa fa-tachometer mr-2" style="font-size: 1.5rem;"></i>Daily Pulse Control Panel</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('dashboard')?>" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content pt-0">
        <div class="m-demo__preview mt-3" style="padding:0px !important">
            <div id="controlPanelHeader" class="row pt-2  mr-1 ml-1 pb-2" style="background: rgb(70, 70, 70);">
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-3 pl-2 pr-2">
                    <select id="choiceSelection" class="form-control m-bootstrap-select m_selectpicker"></select>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-1 pl-2 pr-2 pb-0">
                   <button type="button" class="btn btn-outline-accent m-btn--outline-2x btn-block" onclick="addEscalation()">
                            <i class="fa fa-plus mr-1"></i>Add
                    </button>
                </div>
            </div>
        </div>
        <div class="m-demo__preview mt-3">
            <div class="row" id="noEscalationSettings" style="display:none; margin-right: 0px; margin-left: 0px;">
                <div class="col-md-12" style="border-style: dashed; border-width: 1px; border-color: #e4e4e4; padding-bottom: 14px;">
                    <div  class="col-md-12 text-center">
                        <div class="m-demo-icon__preview">
                            <i style="font-size: 40px;" class="flaticon-notes"></i>
                        </div>
                        <span class="m-demo-icon__class">No Escalation Settings Found</span>
                    </div>
                </div>
            </div>
            <div class="row mr-1 ml-1" id="escalationTable" style="display:none">
                <div class="col-12 p-0 table-responsive">
                    <div class="table-wrapper">
                        <table class="table table-bordered table-hover mb-0">
                            <thead style="background:#d8d8d8;">
                                <tr>
                                    <th id="column-header-1" class="text-truncate text-center resizable-headers first-col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Occurence<div id="column-header-1-sizer"></div></th>
                                    <th id="column-header-2" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Follow Up Question<div id="column-header-2-sizer"></div></th>
                                    <th id="column-header-3" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Message<div id="column-header-3-sizer"></div></th>
                                    <th id="column-header-4" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Intervention<div id="column-header-4-sizer"></div></th>
                                    <th id="column-header-5" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">With Confetti?<div id="column-header-5-sizer"></div></th>
                                    <th id="column-header-6" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">With Icon?<div id="column-header-6-sizer"></div></th>
                                    <!-- <th id="column-header-7" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Created<div id="column-header-7-sizer"></div></th>
                                    <th id="column-header-8" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Last Update<div id="column-header-8-sizer"></div></th>
                                    <th id="column-header-9" class="text-truncate resizable-header text-center col" style="white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width: 1 px;">Updated By<div id="column-header-9-sizer"></div></th> -->
                                </tr>
                            </thead>
                            <tbody id="ecalationTableBody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->


<!--end::Modal-->
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/src/custom/js/daily_pulse/daily_pulse.js"></script>

