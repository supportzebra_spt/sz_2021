<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Daily Pulse Monitoring</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="<?php echo base_url('dashboard')?>" class="m-nav__link">
                            <span class="m-nav__link-text">Dashboard</span>
                        </a>
                    </li>
                </ul>
            </div>
            <br>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show alert-warning d-none"
                    role="alert" id="dueNotif">
                    <div class="m-alert__icon">
                        <i class="fa fa-warning" style="font-size: 30px;"></i>
                        <span></span>
                    </div>
                    <div class="m-alert__text m--font-danger text-center" style="padding: 0.45rem 1.25rem !important; font-size: 16px;">
                       <strong><span class="dueCount" style="font-size: 21px;"></span></strong> Pulse<span id="requestForm"></span>&nbsp;was not intervened on time. Please review below<span id="checkPendingRequest" class="text-info btn" style="font-size: 10px;">(View
                            Request Tab below for Details)</span>
                    </div>
                </div>
                <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head" style="background: #2c2e3eeb;">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#ongoingPulses" role="tab" onclick="showRecord()"> 
                                        <i class="la la-bell"></i>Ongoing</a>
                                </li>
                                <li class="nav-item m-tabs__item" onmouseover="initRecordsTab()">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#recordsPulses" role="tab">
                                        <i class="la la-bar-chart-o"></i>Intervention Records</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="ongoingPulses" role="tabpanel"><!-- ONGOING PULSE INTERVENTION -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2 mb-2">
                                                <select class="custom-select form-control" id="ongoingStatus" name="ongoingStatus" placeholder="Select a Shift" onchange="showRecord()">
                                                    <option value="0">ALL</option>
                                                    <option value="2">Pending</option>
                                                    <option value="12">Missed</option>
                                                </select>
                                            </div>
                                            <div class="col-md-5 mb-2">
                                                <select class="form-control m-select2" id="empSelectIntervene" name="empSelectIntervene" placeholder="Select an employee" ></select>
                                            </div>
                                            <div class="col-md-5 mb-2">
                                                <div class="form-group m-form__group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="Search by ID" id="ongoingSearchs">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">
                                                                <i class="fa fa-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data below.</span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div id="ongoingPulsesTable"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="recordsPulses" role="tabpanel">
                                <div class="row">
                                    <div class="col-sm-6 col-md-3 mb-3">
                                        <select class="custom-select form-control" id="pulseRecordStatus" name="pulseRecordStatus" data-toggle="m-tooltip"
                                            title="Request Status">
                                            <option value="0">ALL</option>
                                            <option value="3">Completed</option>
                                            <option value="12">Missed</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class='input-group pull-right'>
                                                    <input type='text' class="form-control m-input" readonly placeholder="Select date range" id="pulseRecordDateRange" />
                                                    <div class="input-group-append" data-toggle="m-tooltip" title="Date Schedule">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-5 mb-3">
                                        <select class="form-control m-select2 empSelect" id="empSelectPulseRecord" name="empSelectPulseRecord" placeholder="Select a name"></select>
                                    </div>
                                    <div class="col-sm-6 col-md-12">
                                        <div class="form-group m-form__group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search by ID" id="pulseRecordSearch">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">
                                                        <i class="fa fa-search"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <span class="m-form__help pull-right font-italic mb-1" style="font-size: 10px;">NOTE: Search format must be similar to the data shown in the table below.</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="recordsPulseDatatable"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/src/custom/js/daily_pulse/monitoring.js"></script>

<div id="showDetails" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog" role="document">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	   <h4 class="modal-title" id="headerTitle"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
		  <div id="bodyShowDetails">
 		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
		function empSelectInterveneFunc(){
			var choice = $("#ongoingStatus").val();
			 $.ajax({
				type: "POST",
				url: "<?php echo base_url('dailypulse/empSelectIntervene/'); ?>",
				data: {choice:choice},
				cache: false,
				success: function (json) {
				
					var rs = JSON.parse(json);
					var opt = "<option selected value='0'>ALL</option>";
					 $.each(rs, function (key, obj) {
						 
						opt += "<option value='" + obj.emp_id + "' >" + obj.lname+", "+obj.fname + "</option>";
					});
					
					$("#empSelectIntervene").html(opt);
					$("#empSelectIntervene").trigger("change");
				}
			 });
		}
	function showRecord(){
		
		var choice = $("#ongoingStatus").val();
		var empid = $("#empSelectIntervene").val();
		empid = (empid!= null) ? empid : 0;
  		 $('#ongoingPulsesTable').mDatatable('destroy');
         var options = {
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'POST',
                        url: baseUrl + '/dailypulse/showongoingintervene/'+choice+'/'+empid+'/',
                        headers: {
                            'x-my-custom-header': 'some value',
                            'x-test-header': 'the value'
                        },
						 map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    }
                },
				saveState: {
                    cookie: false,
                    webstorage: false
                },
                pageSize: 5,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            layout: {
                theme: 'default',
                class: 'dashed-table',
                scroll: false,
                footer: false,
                header: true
            },
            sortable: true,
            pagination: true,
            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [5, 10, 20, 30, 50]
                    },
                }
            },
            search: {
                input: $('#ongoingSearchs'),
            },			
            columns: [{
                field: "answerId",
                title: "Pulse ID",
                width: 90,
                selector: false,
                sortable: 'desc',
                textAlign: 'center',
                template: function(data) {
                       var answerId = (data.answerId).padLeft(8);
                    var html = "<div class = 'col-md-12'>" +answerId + "</div>";
                    return html;
                }
            },{
                field: "sched_date",
                title: "Date",
                width: 120,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'>" +  data.sched_date + "</div>";
                    return html;
                }
            },{
                field: "lname",
                title: "Respondent",
                width: 180,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'>" +  data.lname+", "+data.fname + "</div>";
                    return html;
                }
            },{
                field: "answer",
                title: "Answer",
                width: 120,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(data) {
                      var html = "<div class = 'col-md-12'>" +  data.answer + "</br><span class='font-italic' style='font-size: 0.9rem;'>("+data.label+")</span></div>";
                    return html;
                }
            },{
                field: "occurence",
                title: "Occurence",
                width: 80,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row, data) {
                    return ordinal(row.occurence);
                }
            },{
                field: "description",
                title: "Status",
                width: 90,
                selector: false,
                sortable: false,
                textAlign: 'center',
                template: function(row) {
					if (parseInt(row.status_ID) == 12) {
						var badgeColor = 'btn-warning';
					} else if (parseInt(row.status_ID) == 2) {
						var badgeColor = 'btn-info';
					} else if (parseInt(row.status_ID) == 5) {
						var badgeColor = 'btn-success';
					} else if (parseInt(row.status_ID) == 6) {
						var badgeColor = 'btn-danger';
					}else if (parseInt(row.status_ID) == 7) {
						var badgeColor = 'btn-primary';
					}
					var html = "<span class='btn btn-sm m-btn--pill " + badgeColor + " m-badge--wide' onclick=showIntervenor("+row.answerId+")>" + row.description + "</span>";

                    return html;
                }
            },{
                field: "",
                title: "Action",
				selector: false,
                sortable: false,
				textAlign: 'center',
				overflow: 'invisible',
                width: 60,
                template: function (data) {
					if(data.status_ID==12){
					 var onclick = 'onclick=makeIntervention('+data.answerId+','+data.employeeId+')';
					 var title = 'Create an intervention details for '+data.lname+'.';
					 var icon = '<i class="fa fa-arrow-circle-o-right" ></i>';
					 var color = 'warning';
					}else{
						var onclick = '';
						var title = 'The intervention is still pending.';
						var icon = '<i class="m-loader m-loader--accent" ></i>';
						 var color = 'accent';
					}
                     var html = '<span class="btn btn-outline-'+color+' m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" '+onclick+' data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="'+title+'">'+icon+'</span>';
					 
                    return html;
                }
            }, 
			],
        };

        $('#ongoingPulsesTable').mDatatable(options);
		}
		function showIntervenor(id){
			 $.ajax({
			type: "POST",
			url: "<?php echo base_url('dailypulse/showIntervenor'); ?>",
			data: {answerId:id},
			cache: false,
			success: function (output) {
				 var rs = JSON.parse(output);
				var icon = "";
				var color = "";
				var animate = "";
				var animateI = "";
				var current = "";
				var stat = "";
				var str = "";
				 $.each(rs, function (index, value) {
					  if(value.feedback == null || value.feedback == " "){
							var note = "No Notes found.";
						}else{
							var note =  value.feedback;
						}
						if (parseInt(value.status_ID) == 5) {
							icon = "fa fa-thumbs-o-up";
							color = "btn-success";
							stat = value.description;
						} else if (parseInt(value.status_ID) == 6) {
							icon = "fa fa-thumbs-o-down";
							color = "btn-danger";
							stat = value.description;
						} else if ((parseInt(value.status_ID) == 2) || (parseInt(value.status_ID) == 4)) {
							icon = "fa fa-spinner";
							color = "btn-info";
							animate = "fa-spin";
							stat = "pending";
								current = '<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--success m-animate-blink ml-2" style="min-height: 7px;min-width: 7px;"></span>';
							
						} else if (parseInt(value.status_ID) == 12){
							icon = "fa fa-exclamation-circle";
							color = "btn-warning";
							animateI = "m-animate-blink";
							stat = value.description;
						}
						else if (parseInt(value.status_ID) == 7){
							icon =  "fa fa-close";
							color = "btn-primary";
							stat =  value.description;
						}else{
							icon = "fa fa-check";
							animateI = ''
							color = "btn-info";
							stat = value.description;
						}
                        var dateIntervened = (value.dateIntervened==null) ? "Not Intervened Yet." :  moment(value.dateIntervened, 'Y-MM-DD HH:mm:ss').format('MMMM D, Y h:mm A') ;
                        var intervenor_level = "Supervisor Level "+value.interveneLevel;
                        if(parseInt(value.interveneEmp_id) == 522){
                            intervenor_level = "HR Manager";
                        }else if(parseInt(value.interveneEmp_id) == 980){
                            intervenor_level = "Managing Director";
                        }
					   str += '<div class="m-portlet bg-secondary">' +
						'<div class="col-md-12 pt-2 pb-2" style="background: #505a6b30;">' +
						'<div class="row">' +
						'<div class="col-7 col-sm-7 col-md-7 pt-2">'+intervenor_level+' '+current+'</div>' +
						'<div class="col-5 col-sm-5 col-md-5">' +
						'<div class="btn pull-right" style="padding: 2px 15px 2px 2px;border-radius: 23px;height: 30px;background: #505a6b;">' +
						'<div class="btn '+color+' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air '+animate+'" style="width: 25px !important;height: 25px !important;">' +
						'<i class="' + icon + ' '+animateI+'" style="font-size: 17 px;"></i>' +
						'</div>' +
						'<span class="button-content text-light text-capitalize pl-2">' + stat + '</span>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>  ' +
						'<div class="m-portlet__body pt-3 pl-4 pb-3 pr-4">' +
						'<div class="row">' +
						'<div class="col-2 col-sm-2 col-md-2 d-none d-sm-block">' +
						'<img class="rounded-circle" id='+value.interveneEmp_id+' src="<?php echo base_url(); ?>/'+value.picIntervene+'" width="58" onerror="noImageFound(this)" alt="" class="mx-auto" style="background: #fff !important;">' +
						'</div>' +
						'<div class="col-12 col-sm-10 col-md-10 pl-1">' +
						'<div class="col-md-12 font-weight-bold" style="font-size: 15px;">'+value.interveneFname+' '+value.interveneLname+'</div>' +
						//'<div class="col-md-12" style="font-size: 12px;margin-top: -3px;font-weight: bolder;color: darkcyan;">123</div>' +
						'<div class="col-md-12" style="font-size: 12px;"> Intervenor '+value.interveneLevel+'</div>' +
						'<span class="col-md-12" style="font-size: 12px;">'+dateIntervened+'</span>' +
						'<div class="col-md-12 mt-3" style="font-size: 12px;"><i> - '+note+'</i>' +
						'</div>' +
						'</div>' +
						'<div class="col-md-12 pt-3">' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>'; 
				 });
				
     
 			$("#bodyShowDetails").html(str);	
 			$("#headerTitle").html("Intervenor");	
			}
		 });
			$("#showDetails").modal("show");
		}
		function insertFeedback(message,answerId,uid){
 			$.ajax({
			url: "<?php echo base_url('Dailypulse/insertFeedback/'); ?>",
			type: 'POST',
			data: {message:message,answerId:answerId,uid:uid},
			success: function(data) {
				// alert(data);
				var rs = JSON.parse(data);
				if(rs["notif_id"][0]>0){
					 systemNotification([rs["notif_id"][0]]);
					 showRecord();
					 
				}else{
					alert("error");
				}
					
			}
		});
		}
		function makeIntervention(answerId,uid){
			swal({
						  title: "",
						  input: 'textarea',
						  inputAttributes: {
							autocapitalize: 'off',
							maxlength: 500,
							minlength: 5,
						  },
						  html:'<div class="swal2-custom-title"><h4>Intervention Details</h4></div>' ,
						  showCancelButton: true,
						  confirmButtonText: 'Submit',
						  showLoaderOnConfirm: true,
						  allowOutsideClick: false,
						  inputValidator: (value) => {
								if (!value) {
								  return 'You need to write something!'
								}
								if (value.length<1) {
								  return 'Please enter at least 15 characters.'
								}
								if (value.length>500) {
								  return 'Only 500 characters are allowed.'
								}
							  },
						}).then((result) => {
							
						  if (result.value){ 
						  insertFeedback(result.value,answerId,uid);
						   
							//location.reload();
							  // notrequiredAnswer = notrequiredAnswer !== undefined ? notrequiredAnswer.replace(/\s\s+/g, ' ') : null;
								 
								
						  }
						});
		}
		
		$(function(){
			$("#empSelectIntervene").on("change",function(){
				 showRecord();
			});
            $("#empSelectIntervene").select2();
            $('#select2-empSelectIntervene-container').parents('.select2-container').tooltip({
                title: "Select a Respondent",
                placement: "top"
            });
			
			showRecord();
 			empSelectInterveneFunc();
		});
 		
</script>
