<style type="text/css">
.quote_class{
  text-align: center;
  margin:auto;
}
.feedback_class{
  text-align: justify;
  margin:auto;
}
.survey_box{
  background: #f2f4f5!important;
  border-style: dashed;
  border-color: #bac1c1;
}
.header_color{
  background:#dffbff !important;
}
.comment-pic {
  width: 2.4rem;
  border-radius: 50%;
  margin: 2px -3px 0 3px;
  height: 2.4rem;
}
.comment-cwrapper {
  background: #eeffda;
  display: table;
  margin-bottom: 10px;
  border-radius: 10px;
  padding: 8px;
  width: 100%;
}
.comment-cwrapper_2 {
  background: #f1f0ef;
  display: table;
  margin-bottom: 10px;
  border-radius: 10px;
  padding: 8px;
  width: 100%;
}
.comment-info {
  display: table-cell;
  width: 100%;
  padding-left: 1rem;
  font-size: 1rem;
  vertical-align: middle;
}
.comment-name {
  font-size: 12px;
  font-weight: 500;
}
.comment-content {
  font-size: 13px;
  color: #000;
}
.comments-container_class{
 padding-top: 10px;
}
.distance_feedback{
  margin-bottom: 1.1rem !important;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">Report Intervention</h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="#" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">-</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('dashboard')?>" class="m-nav__link">
              <span class="m-nav__link-text">Dashboard</span>
            </a>
          </li>
        </ul>
      </div>
      <br>
      <div>

      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">

       <div class="m-portlet m-portlet--tabs">
        <div class="m-portlet__head col-md-12" style="padding: 1.2rem 2.2rem !important;background-color: #3D3E4D;">
          <div class="row">
            <div class="col-9 col-sm-8 col-md-6 col-lg-6 col-xl-6" style="margin-top: 6px;">
              <h5 class=" text-white">
                <i class="fa fa-bar-chart-o text-light mr-3" style="font-size: 26px;"></i>Intervention Records</h5>
              </div>
              <div class="col-3 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                <div class="pull-right">
                  <button class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" id="downloadDailyPulseReportBtn" style="display:none;" onclick="exportRegistrationReport()">
                    <i class="fa fa-download"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="m-portlet__body">
            <div class="tab-content">
              <div class="tab-pane active" id="recordsPulses" role="tabpanel">
                <div class="row">
                  <div class="col-12">
                    <div class="row">
                     <div class="col-sm-6 col-md-4 mb-3">
                      <select class="custom-select form-control" id="ongoingStatus" name="ongoingStatus" placeholder="Select Status" onchange="">
                        <option value="0">ALL</option>
                        <option value="3">Completed</option>
                        <option value="2">Pending</option>
                        <option value="12">Missed</option>
                      </select>
                    </div>
                    <div class="col-sm-6 col-md-4">
                      <div class="form-group m-form__group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <div class='input-group pull-right'>
                            <input type='text' class="form-control m-input" readonly placeholder="Select date range" id="pulseRecordDateRange" />
                            <div class="input-group-append" data-toggle="m-tooltip" title="Date Schedule">
                              <span class="input-group-text">
                                <i class="la la-calendar-check-o"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-4 mb-3">
                      <select class="form-control m-select2 empSelect" id="empSelectPulseRecord" name="empSelectPulseRecord" placeholder="Select a name"></select>
                    </div>
                    <div class="col-sm-6 col-md-12">
                      <div class="form-group m-form__group">
                        <div class="input-group">
                          <input type="text" class="form-control" placeholder="Search by pulse ID" id="reportSearch">
                          <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">
                              <i class="fa fa-search"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div id="reportInterventionDatatable">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- View details Modal START-->
<div class="modal fade" id="feedback_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
style="display: none;">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
     <h5 class="modal-title" id="exampleModalLabel"> <i class="fa fa-weixin"></i> Intervention details</h5>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="400">
      <div id="approvalBody">
        <div id="zebra_emotion">
        </div>
        <!--  -->
        <div id="detailed_answer" class="m-portlet bg-secondary header_color">
        </div>
        <!--  -->
        <!--  -->
        <div id="feedback_det" class="m-portlet distance_feedback">
        </div>
        <!--  -->
        <!--  -->
        <div id="comment_det" class="m-portlet">
        </div>
        <!--  -->
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>

<div class="modal fade" id="loadingExportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
    <div class=" p-3 text-center" style="">
      <i class="fa fa-spinner text-primary fa-spin mr-2" style="font-size:20px"></i>
      EXPORTING EXCEL FILE...
    </div>
  </div>
</div>
</div>
<script src="<?php echo base_url();?>assets/src/custom/plugins/jqueryfileDownload/jqueryfileDownload.js"></script>

<!-- View details Modal END-->
<script>
  $(function(){
    initPulseRecordDateRangePicker(function(dateRanges){
      initEmployeeRecorNameField(dateRanges.start, dateRanges.end, function (obj) {
        initReport(dateRanges.start, dateRanges.end);
      });
    });
  });
// data table
function initReport(startDate, endDate){    
  $('#reportInterventionDatatable').mDatatable('destroy');
  reportDatatable.init(startDate, endDate);
}

var reportDatatable = function () {
  var report = function (start, end) {
    var options = {
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'POST',
            url: baseUrl + "/dailypulse/list_report_datatable",
            params: {
              query: {
                reportVal: $('#reportSearch').val(),
                startDate: start,
                endDate: end,
                empId: $('#empSelectPulseRecord').val(),
                statId: $('#ongoingStatus').val()
              },
            },
            map: function (raw) {
        // sample data mapping
        var dataSet = raw;
        if (typeof raw.data !== 'undefined') {
          dataSet = raw.data;
        }
        if(dataSet.length == 0){
          $('#downloadDailyPulseReportBtn').fadeOut();
        }else{
          $('#downloadDailyPulseReportBtn').fadeIn();
        }
        // $('#pendingCount').text(dataSet.length);
        return dataSet;
      },
    }
  },
  saveState: {
    cookie: false,
    webstorage: false
  },
  pageSize: 5,
  serverPaging: true,
  serverFiltering: true,
  serverSorting: true,
},
layout: {
theme: 'default', // datatable theme
class: '', // custom wrapper class
scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
height: 550, // datatable's body's fixed height
footer: false // display/hide footer
},
sortable: true,
pagination: true,
toolbar: {
// toolbar placement can be at top or bottom or both top and bottom repeated
placement: ['bottom'],

// toolbar items
items: {
// pagination
pagination: {
// page size select
pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
},
}
},
search: {
  input: $('#reportSearch'),
},
rows: {
  afterTemplate: function (row, data, index) {},
},
// columns definition
columns: [{
  field: "answerId",
  title: "Pulse ID",
  width: 110,
  selector: false,
  sortable: 'desc',
  textAlign: 'center',
  template: function (row, index, datatable) {
    var html =row.answerId.padLeft(8);
    return html;
  },
},
{
  field: "fname",
  title: "Respondent",
  width: 120,
  selector: false,
  // sortable: 'asc',
  textAlign: 'center',
  template: function (row, index, datatable) {
    var html = row.fname + " " + row.lname;
    return html;
  },
},{
  field: "acc_name",
  title: "Schedule Date",
  width: 110,
  selector: false,
  // sortable: 'asc',
  textAlign: 'center',
  template: function (row, index, datatable) {
    var html = row.sched_date;
    return html;
  },
},{
  field: "lname",
  title: "Intervenor",
  width: 110,
  selector: false,
  // sortable: 'asc',
  textAlign: 'center',
  template: function (row, index, datatable) {
    var html=row.intervenor_fname + " " + row.intervenor_lname;
    return html;
  },
},{
  field: "interveneLevel",
  title: "Level",
  width: 105,
  selector: false,
  // sortable: 'asc',
  textAlign: 'center',
  template: function (row, index, datatable) {
    var html=row.interveneLevel;
    return html;
  },
},{
  field: "status_ID",
  title: "Status",
  width: 110,
  selector: false,
  // sortable: 'asc',
  textAlign: 'center',
  template: function (row, index, datatable) {
    var html="";
    if(row.status_ID=="2"){
      html='<span style="width: 150px;"><span class="btn btn-sm m-btn--pill btn-info m-badge--wide">'+row.stat_desc+'</span></span>';
    }else if(row.status_ID=="3"){
      html='<span style="width: 150px;"><span class="btn btn-sm m-btn--pill btn-success m-badge--wide">'+row.stat_desc+'</span></span>';
    }else{
      html='<span style="width: 150px;"><span class="btn btn-sm m-btn--pill btn-warning m-badge--wide">'+row.stat_desc+'</span></span>';
    }   
    return html;
  },
},{
  field: "assign_ID",
  title: "Action",
  width: 105,
  selector: false,
  // sortable: 'asc',
  textAlign: 'center',
  template: function (row, index, datatable) {
    var html="";
    if(row.status_ID=="3"){
     html ='<a data-toggle="modal" data-target="#feedback_modal"><span class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick="display_details('+row.intervention_ID+','+row.answerId+')"><i class="fa fa-search" ></i></span></a>';
   }else if(row.status_ID=="2"){
    html ='<span style="width: 100px;"><span class="btn btn-outline-accent m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick="" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="The intervention is still pending"><i class="m-loader m-loader--accent"></i></span></span>';
  }else{
    html ='<span style="width: 100px;"><span class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick="" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="The intervention is missed."><i class="fa fa-exclamation" style="color:#7b7676 !important"></i></span></span>';
  }
  return html;
},
}],
};
var datatable = $('#reportInterventionDatatable').mDatatable(options);
};
return {
  init: function (start, end) {
    report(start, end);
  }
};
}();
function display_details(int_id,ans_id){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>dailypulse/fetch_answerFeedbackComments",
    data: {
     intervention_ID: int_id,
     answerId: ans_id,
   },
   cache: false,
   success: function (res) {
    var result = JSON.parse(res.trim());
    console.log(result);
    var detailed_ans="";
    var one_feedback="";
    var comments_display="";
    var emoticon="";

    detailed_ans+="<div class='m-portlet__body pt-3 pl-4 pb-3 pr-4 survey_box'>";
    detailed_ans+="<div class='row'>";

//Icon base on answer
if(result.answers_feedbacks[0].answer=="2"){
  detailed_ans+="<div class='col-2' style='margin:auto'>";
  detailed_ans+="<img  src='<?php echo base_url("assets/images/img/sad.gif"); ?>' width='75' alt='' class='rounded-circle mx-auto d-block'>";
  detailed_ans+="</div>";
}else if(result.answers_feedbacks[0].answer=="1"){
 detailed_ans+="<div class='col-2' style='margin:auto'>";
 detailed_ans+="<img  src='<?php echo base_url("assets/images/img/demotivated.gif"); ?>' width='75' alt='' class='rounded-circle mx-auto d-block'>";
 detailed_ans+="</div>";
}else if(result.answers_feedbacks[0].answer=="3"){
 detailed_ans+="<div class='col-2' style='margin:auto'>";
 detailed_ans+="<img  src='<?php echo base_url("assets/images/img/normal.gif"); ?>' width='75' alt='' class='rounded-circle mx-auto d-block'>";
 detailed_ans+="</div>";
}
detailed_ans+="</div>";
detailed_ans+="<div class='row'>";
detailed_ans+="<div class='col-8' style='margin:auto;'>";

//Choice Label colors
if(result.answers_feedbacks[0].answer=="1"){
  detailed_ans+="<span style='font-size: 1.2em;color:black;text-align: center;display:block;font-weight: 500;'> Feeling <span style='color:#ed4d50'>"+result.answers_feedbacks[0].choice_label+"</span> ("+result.answers_feedbacks[0].answer+" / 5)</span>";
}else if(result.answers_feedbacks[0].answer=="2"){
  detailed_ans+="<span style='font-size: 1.2em;color:black;text-align: center;display:block;font-weight: 500;'> Feeling <span style='color:#f7b432'>"+result.answers_feedbacks[0].choice_label+"</span> ("+result.answers_feedbacks[0].answer+" / 5)</span>";
}else if(result.answers_feedbacks[0].answer=="3"){
  detailed_ans+="<span style='font-size: 1.2em;color:black;text-align: center;display:block;font-weight: 500;'> Feeling <span style='color:#fcd633'>"+result.answers_feedbacks[0].choice_label+"</span> ("+result.answers_feedbacks[0].answer+" / 5)</span>";
}else if(result.answers_feedbacks[0].answer=="4"){
  detailed_ans+="<span style='font-size: 1.2em;color:black;text-align: center;display:block;font-weight: 500;'> Feeling <span style='color:#c7d45a'>"+result.answers_feedbacks[0].choice_label+"</span> ("+result.answers_feedbacks[0].answer+" / 5)</span>";
}else if(result.answers_feedbacks[0].answer=="5"){
  detailed_ans+="<span style='font-size: 1.2em;color:black;text-align: center;display:block;font-weight: 500;'> Feeling <span style='color:#62d499'>"+result.answers_feedbacks[0].choice_label+"</span> ("+result.answers_feedbacks[0].answer+" / 5)</span>";
}

detailed_ans+="</div>";
detailed_ans+="</div>";

detailed_ans+="<br><div class='row'>";
detailed_ans+="<div class='col-12 col-sm-10 col-md-10 pl-1 quote_class'>";
detailed_ans+="<div class='col-md-12' style='font-size: 15px;'>";
detailed_ans+="<p><i class='fa fa-quote-left'></i>&nbsp; "+result.answers_feedbacks[0].detailedanswer+"&nbsp; <i class='fa fa-quote-right'></i></p></div></div></div></div>";

var dateString=result.answers_feedbacks[0].dateIntervened;
var dateObj = new Date(dateString);
var momentObj = moment(dateObj);
var feedback_date = momentObj.format('llll');

one_feedback+="<div class='m-widget3__header comment-cwrapper comment-cwrapper1085' id=''>";
one_feedback+="<div class='m-widget3__user-img'><img class='m-widget3__img comment-pic comment-pic-dt1016' src='<?php echo base_url("assets/images/img/sz.png"); ?>' onerror='noCommentImage(1016)' alt=''>";
one_feedback+="</div><div class='m-widget3__info comment-info'><span class='m-widget3__username comment-name'>"+result.answers_feedbacks[0].fname+" "+result.answers_feedbacks[0].lname+"</span>";
one_feedback+="<span style='font-size: 11px;font-style: italic;margin-left: 5px;color:#1d8021!important'>"+feedback_date+" &nbsp;<i class='fa fa-comment' style='font-size: 12px;color: #767e84d6;'></i></span>";
one_feedback+="<p class='m-widget3__time comment-content' id='comment-content' style='margin-bottom: 0;margin-top:3px;'><span style='color:#1d8021;font-weight: 500;'> Feedback: &nbsp;</span>"+result.answers_feedbacks[0].feedback+"</p><div style='margin-top:4px'></div></div></div>";

$.each(result.comments, function (key, item) {
  if(item.comment_ID!=null){
    var datecom_string=item.dateCreated;
    var dateObj_com = new Date(datecom_string);
    var momentObj_com = moment(dateObj_com);
    var comment_date = momentObj_com.format('llll');
    comments_display+="<div class='m-widget3__header comment-cwrapper_2 comment-cwrapper1085' id=''>";
    comments_display+="<div class='m-widget3__user-img'><img class='m-widget3__img comment-pic comment-pic-dt1016' src='<?php echo base_url("assets/images/img/sz.png"); ?>' onerror='noCommentImage(1016)' alt=''>";
    comments_display+="</div><div class='m-widget3__info comment-info'><span class='m-widget3__username comment-name'>"+item.fname+" "+item.lname+"</span>";
    comments_display+="<span style='font-size: 11px;font-style: italic;margin-left: 5px;color:#0c6d9c!important'>"+comment_date+" &nbsp;<i class='fa fa-comments' style='font-size: 12px;color: #767e84d6;'></i></span>";
    comments_display+="<p class='m-widget3__time comment-content' id='comment-content' style='margin-bottom: 0;margin-top:3px;'><span style='color:#0c6d9c;font-weight: 500;'> Comment: &nbsp;</span>"+item.comment+"</p><div style='margin-top:4px'></div></div></div>";
  }
});
$("#zebra_emotion").html(emoticon);
$("#detailed_answer").html(detailed_ans);
$("#feedback_det").html(one_feedback);
$("#comment_det").html(comments_display);
}
});
}

//Date filter
function initPulseRecordDateRangePicker(callback) {
  getCurrentDateTime(function (date) {
    start = moment(date, 'Y-MM-DD').startOf('month').format('MM/DD/YYYY');
    end = moment(date, 'Y-MM-DD').endOf('month').format('MM/DD/YYYY');
    $('#pulseRecordDateRange .form-control').val(start + ' - ' + end);
    $('#pulseRecordDateRange').daterangepicker({
      buttonClasses: 'm-btn btn',
      applyClass: 'btn-primary',
      cancelClass: 'btn-secondary',
      startDate: start,
      endDate: end,
      opens: "left",
      ranges: {
        'Today': [moment(date, 'Y-MM-DD'), moment(date, 'Y-MM-DD')],
        'Yesterday': [moment(date, 'Y-MM-DD').subtract(1, 'days'), moment(date, 'Y-MM-DD').subtract(1, 'days')],
        'Last 7 Days': [moment(date, 'Y-MM-DD').subtract(6, 'days'), moment(date, 'Y-MM-DD')],
        'Last 30 Days': [moment(date, 'Y-MM-DD').subtract(29, 'days'), moment(date, 'Y-MM-DD')],
        'This Month': [moment(date, 'Y-MM-DD').startOf('month'), moment(date, 'Y-MM-DD').endOf('month')],
        'Last Month': [moment(date, 'Y-MM-DD').subtract(1, 'month').startOf('month'), moment(date, 'Y-MM-DD').subtract(1, 'month').endOf('month')]
      }
    }, function (start, end, label) {
      $('#pulseRecordDateRange .form-control').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
      initEmployeeRecorNameField(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function(obj){
       initReport(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'));
     });

// initIrAbleDtrVioRecord(moment(start.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), moment(end.format('MM/DD/YYYY'), 'MM/DD/Y').format('Y-MM-DD'), function (notEmpty) {});
});
    var dateRanges = {
      'start': moment(start, 'MM/DD/YYYY').format('Y-MM-DD'),
      'end': moment(end, 'MM/DD/YYYY').format('Y-MM-DD')
    };
    callback(dateRanges);
  });
}
function initEmployeeRecorNameField(start, end, callback) {
  initEmployeeRecorName(start, end, function (empNameObj) {
    init_select2(empNameObj, "#empSelectPulseRecord", "Select a Respondent", function(obj){
      callback(obj);
    });
  });
}
function initEmployeeRecorName(start, end, callback) {
  $.when(fetchPostData({
    startDate: start,
    endDate: end,
    statusID: $('#ongoingStatus').val()
  }, '/dailypulse/get_record_respondent_emp_name')).then(function (empName) {
    var empNameObj = $.parseJSON(empName.trim());
    callback(empNameObj);
  })
}
function init_select2(obj, elementId , label, callback) {
  var optionsAsString = "<option value='0'>ALL</option>";
  if (parseInt(obj.exist)) {
    $.each(obj.record, function (index, value) {
      console.log(value.emp_id);
      optionsAsString += "<option value='" + value.emp_id + "'>" + toTitleCase(value.lname + ", " + value.fname) + "</option>";
    });
  }
  console.log
  $(elementId).html(optionsAsString);
  $(elementId).select2({
    placeholder: "Select an Employee",
    width: '100%'
  }).trigger('change');
  var selectId = elementId.replace('#','');
  $('#select2-'+selectId+'-container').parents('.select2-container').tooltip({
    title: label,
    placement: "top"
  });
  callback(obj);
}
function exportRegistrationReport(){
  var excelFile = baseUrl + "/dailypulse/export_daily_pulse_report";
  var dateRange = $('#pulseRecordDateRange').val().split('-');
  $.fileDownload(excelFile, {
    httpMethod: "POST",
    data: {
      startDate : moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'),
      endDate : moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'),
      empId: $('#empSelectPulseRecord').val(),
      searchVal: $('#reportSearch').val(),
      statusId: $('#ongoingStatus').val()
    },
    prepareCallback: function (url) { 
     toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "0",
      "hideDuration": "0",
      "timeOut": "0",
      "extendedTimeOut": "0",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };
    toastr.info("Exporting Excel File...");
  },
  successCallback: function (url) {
   toastr.clear();
 },
 failCallback: function (responseHtml, url) {
  swal({
    title: 'Export Report Error!',
    text: "Something went wrong while exporting the report. Please immediately inform the system administrator about the error",
    type: 'error',
  }).then(function (result) {
    toastr.clear();

  });
}
})
}
$('#empSelectPulseRecord, #ongoingStatus').on('change', function () {
  var dateRange = $('#pulseRecordDateRange').val().split('-');
  initReport(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
})

$('#reportSearch').on('keyup', function () {
  var dateRange = $('#pulseRecordDateRange').val().split('-');
  initReport(moment(dateRange[0], 'MM/DD/Y').format('Y-MM-DD'), moment(dateRange[1], 'MM/DD/Y').format('Y-MM-DD'));
})
</script>
