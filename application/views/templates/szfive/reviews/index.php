<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
	<?php $sub_header = ['title' => 'Reviews', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'Reviews', 'link' => 'szfive/reviews#']]];?>
	<?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
	<div class="m-content" id="survey-reviews">
		<?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list);?>
		<div class="row">
			<div class="col-lg-12">
				<div class="m-portlet" id="m_blockui_2_portlet" style="margin-bottom: 25px;box-shadow: 0 1px 8px rgba(0,0,0,0.15);border-radius: 3px;border: 1px solid #ffffff;">
					<div class="m-portlet__body" style="padding: 10px;">
						<div class="row">
							<div class="col-lg-5">
								<input type="hidden" value="0" name="employeeStatus" />
								<div class="m-select2 m-select2--pill">
									<select class="form-control m-select2" id="select_reviews_by_department" multiple name="param">
										<option></option>
										<?php foreach ($departments as $department): ?>
										<option value='<?php echo $department->dep_id; ?>'>
											<?php echo $department->dep_name . ' - ' . $department->dep_details; ?>
										</option>
										<?php endforeach;?>
									</select>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="pull-right" style=" width: 100%; ">
									<span class="m-subheader__daterange" id="survey_reviews_daterange">
										<span class="m-subheader__daterange-label">
											<span class="daterange-title m-subheader__daterange-title-reviewers">This Week:</span>
											<span class="daterange-date m-subheader__daterange-date-reviewers m--font-brand">
												<?php echo $this_week; ?>
											</span>
										</span>
										<input type="hidden" name="start_date_reviewer" value="<?php echo date('Y-m-d'); ?>" />
										<input type="hidden" name="end_date_reviewer" value="<?php echo date('Y-m-d'); ?>" />
										<button class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" style="width: 20px;height: 20px;">
											<i class="la la-angle-down" style="color: white;"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-lg-2 col-md-9 col-sm-12">
								<a href="<?php echo base_url('szfive/reviews/review_details'); ?>" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--pill">
									<i class="flaticon-edit"></i> Review Details
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i style="font-size: 22px;" class="flaticon-line-graph"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Reviews
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
								<i class="la la-angle-down"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="main-chart-wrapper">
					<div class="m-stack m-stack--hor m-stack--general m-stack--demo" style="margin-bottom: 25px;">
						<div class="m-stack__item m-stack__item--center" id="display-mainchart">
							<div class='no-data' style='text-align: center;display: inline-block;'>
								<div class='m-demo-icon__preview'>
									<i style='font-size: 22px;' class='flaticon-line-graph'></i>
								</div>
								<span class='m-demo-icon__class'>It looks like there is no reviews done yet.</span>
							</div>
						</div>
					</div>
					<div id="dept_reviews_chart">
						<div id="dept_reviewers_chart"></div>
					</div>
				</div>

			</div>
		</div>
		<div class="m-portlet" id="reviewer_details_portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i style="font-size: 22px;" class="flaticon-users"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Reviewers
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<div class="m-form m-form--label-align-right">
							<div class="m-input-icon m-input-icon--right reviews-filter-search">
								<input type="text" class="form-control m-input m-input--pill" placeholder="Search..." id="reviewerDetailsSearch">
								<span class="m-input-icon__icon m-input-icon__icon--right">
									<span class="m-nav__link-icon">
										<i class="flaticon-search-1" style="color: #b5b4b4 !important;"></i>
									</span>
								</span>
							</div>
						</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body" style="padding: 0;">
				<div class="m-widget5">
					<div class="m_datatable_reviewers"></div>
				</div>
			</div>
		</div>
	</div>
	<?php include '_modals.php';?>
</div>
<script src="<?php echo base_url('assets/src/custom/js/szfive.reviews.main.js'); ?>" type="text/javascript"></script>