<style>
	#top_reviewer_1,
	#top_reviewer_2,
	#no-top-reviewers {
		display: none;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
	<?php $sub_header = ['title' => 'Reviews', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'Reviews', 'link' => 'szfive/reviews#']]];?>
	<?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
	<div class="m-content" id="survey-reviews">
		<?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list);?>
		<div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i style="font-size: 22px;" class="flaticon-line-graph"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Top Reviewers
							<small id="top-reviewer_dates"></small>
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<span class="m-subheader__daterange" id="top_reviewers_daterange">
								<span class="m-subheader__daterange-label">
									<span class="daterange-title daterange-title-top-reviewer"></span>
									<span class="daterange-date daterange-date-top-reviewer m--font-brand"></span>
								</span>
								<a class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
									<i class="la la-angle-down"></i>
								</a>
							</span>
						</li>
						<li class="m-portlet__nav-item">
							<a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
								<i class="la la-angle-down"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="row justify-content-md-center">
					<div class="col-lg-4 col-md-4 col-sm-12" id="top-reviewers-wrapper">
						<div id="top_reviewer_1">
							<div class="m-portlet" id="m_blockui_2_portlet" style="margin-bottom: 25px;box-shadow: 0 2px 1px rgba(0,0,0,0.15);border-radius: 3px;border: 1px solid #f4f3f8;">
								<div class="m-portlet__head" style=" height: 40px; ">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text top_early-head-text" id="top_reviewer_title_1">

											</h3>
										</div>
									</div>
								</div>
								<div class="m-portlet__body" style="text-align: center;padding: 10px;">
									<div class="highfive-user" id="top_reviewer_pic_1" style=" background: #f4f3f8;padding:15px;margin-bottom: 10px;">

									</div>
									<div>
										<p style="line-height: 1.2;">
											<strong id="top_reviewer_name_1"></strong>
											<br>
											<small id="top_reviewer_position_1"></small>
										</p>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12">
						<div id="top_reviewer_2">
							<div class="m-portlet" id="m_blockui_2_portlet" style="margin-bottom: 25px;box-shadow: 0 2px 1px rgba(0,0,0,0.15);border-radius: 3px;border: 1px solid #f4f3f8;">
								<div class="m-portlet__head" style=" height: 40px; ">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<h3 class="m-portlet__head-text top_early-head-text" id="top_reviewer_title_2">

											</h3>
										</div>
									</div>
								</div>
								<div class="m-portlet__body" style="text-align: center;padding: 10px;">
									<div class="highfive-user" id="top_reviewer_pic_2" style=" background: #f4f3f8;padding:15px;margin-bottom: 10px;">

									</div>
									<div>
										<p style="line-height: 1.2;">
											<strong id="top_reviewer_name_2"></strong>
											<br>
											<small id="top_reviewer_position_2"></small>
										</p>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-md-center">
					<div id="no-top-reviewers">
						<p>
							<i class="flaticon-users" style="font-size: 24px;margin: 0 5px;"> </i>There are no top reviewers for the specified date.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="m-portlet m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i style="font-size: 22px;" class="flaticon-line-graph"></i>
						</span>
						<h3 class="m-portlet__head-text">
							Review Details
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#" data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Collapse">
								<i class="la la-angle-down"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="row reviews-form-control" style="margin-bottom: 20px;">
					<div class="col-lg-4">
						<div class="m-select2 m-select2--pill">
							<select class="form-control m-select2" id="select2__top_reviews" name="param" data-placeholder="Pill style">
								<option></option>
								<option value="top_reviews">Top Reviewers</option>
								<option value="late_reviews">Late Reviewers</option>
								<option value="all">All Reviewers</option>
							</select>
						</div>
					</div>
					<div class="col-lg-4">
						<div>
							<span class="m-subheader__daterange" id="details_review_daterange">
								<span class="m-subheader__daterange-label">
									<span class="daterange-title m-subheader__daterange-title-reviews">This Week: </span>
									<span class="daterange-date m-subheader__daterange-date-reviews m--font-brand">
										<?php echo $this_week; ?>
									</span>
								</span>
								<a class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
									<i class="la la-angle-down"></i>
								</a>
							</span>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="m-form m-form--label-align-right">
							<div class="m-input-icon m-input-icon--right">
								<input type="text" class="form-control m-input m-input--pill" placeholder="Search..." id="detailsReviewSearch">
								<span class="m-input-icon__icon m-input-icon__icon--right">
									<span class="m-nav__link-icon">
										<i class="flaticon-search-1" style="color: #b5b4b4 !important;"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div id="reviewers_reviews_portlet">
							<div class="m_datatable_reviewers_reviews"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include '_modals.php';?>
</div>
<script src="<?php echo base_url('assets/src/custom/js/szfive.reviews.sub.js'); ?>" type="text/javascript"></script>