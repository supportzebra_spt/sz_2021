<style>
    .heart {
        background: url("<?php echo base_url('assets/images/img/web_heart_animation.png'); ?>");
        background-repeat: no-repeat;
        height: 50px;
        width: 50px;
        cursor: pointer;
        position: absolute;
        background-size: 2900%;
        margin-left: -7px;
        margin-top: -12px;
    }

    .message {
        background: url("<?php echo base_url('assets/images/img/message.png'); ?>");
        background-position: left;
        background-repeat: no-repeat;
        height: 13px;
        width: 15px;
        cursor: pointer;
        position: absolute;
        background-size: contain;
        margin-left: 9px;
        margin-top: 7px;
    }

    .likeType {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        display: block;
        height: 40px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 40px;
        cursor: pointer;
    }

    .likeTypeSmall {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        float: left;
        margin-right: -3px;
        height: 20px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 20px;
        cursor: pointer;
    }

    .likeIconDefault {
        width: 17px;
        float: left;
        margin-right: 5px;
        background: url("<?php echo base_url('assets/images/img/like.png'); ?>");
        height: 16px;
        background-size: 100%;
    }

    .defaultIcon {
        margin-right: 5px;
    }

    .all-reaction {
        background-position: 0 30px;
        height: 15px;
        width: 15px;
    }

    .mention {
        /* padding-right: 150px !important; */
        font-size: 16px;
        margin-bottom: 12px;
    }

    .post-type {
        width: 115px !important;
        margin: 0 auto;
        display: flex !important;
        justify-content: center;
        align-items: center;
        padding-right: 2px;
        border-left: 1px solid #dcdcdc;
    }

    .bootstrap-select {
        padding-right: 0 !important;
    }

    .dropdown-toggle {
        border: none !important;
    }

    #post_type {
        display: none;
    }

    textarea {
        height: initial !important;
    }

    ` .like-info {
        margin-left: 10px;
    }

    .like-info {
        margin-left: 10px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'SZFive', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content survey-answers" id="highfive-page">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list);?>
        <?php $tabs = ['tabs' => [['title' => 'Highlights', 'link' => 'szfive', 'active' => ''], ['title' => 'Activity', 'link' => 'szfive/activity_public#', 'active' => ''], ['title' => 'Feeling', 'link' => 'szfive/feeling', 'active' => ''], ['title' => 'Comments', 'link' => 'szfive/comments', 'active' => 'active']]];?>
        <?php $this->load->view('templates/szfive/_includes/user_info_bar', $tabs);?>
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--brand m-tabs-line" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <div class="row" style="padding: 1.7rem 0 1.5rem 0;">
                                <select class="form-control m-bootstrap-select m_selectpicker" name="postType" id="select_comment_type">
                                    <option data-icon="fa fa-globe" value="1"> &nbsp;Public</option>
                                    <option data-icon="fa fa-lock" value="0" selected> &nbsp;Private</option>
                                </select>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body" style="padding: 0;">
                <div class="m_datatable_sent_highfive"></div>
            </div>
        </div>
        <div class="row">
            <?php $this->load->view('templates/szfive/highfive/_modals');?>
        </div>
    </div>
</div>
<script>
    function noImageFound(image) {
        $('.' + image).attr('src', baseUrl + '/assets/images/img/sz.png');
    }

    var imagePath = baseUrl + '/assets/images/';

    var commentDatatable = function (param) {
        var receivedComments = function (param) {

            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: baseUrl + '/szfive/get_high_five_comments',
                            params: {
                                query: {
                                    isPublic: param.isPublic,
                                },
                            },
                            map: function (raw) {
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    theme: 'default',
                    scroll: false,
                    footer: false,
                    header: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    placement: ['bottom'],
                    items: {
                        pagination: {
                            pageSizeSelect: [5, 10, 20, 30, 50]
                        },
                    }
                },
                columns: [{
                    width: 400,
                    field: 'commentId',
                    title: 'Name',
                    template: function (data) {
                        console.log(data);
                        var imageClass = "no-image-" + data.senderId;
                        var dateCreated = moment(data.dateCreated).format('MMM D, YYYY hh:mm A');

                        str =
                            '<div class="row" style="margin: 0;margin-left: 20px;" id="highfive-item' +
                            data.commentId + '">';
                        str += '<div style="margin-left: 15px;">';
                        str += '<img class="m-widget3__img no-image-' + data.senderId +
                            '" src="' + baseUrl + '/assets/images/' + data.senderPic +
                            '" onerror="noImageFound(' + "'" + imageClass + "'" +
                            ')" alt="" style="width: 3.7rem;height: 3.7rem;border-radius: 50%;display: inline-block;vertical-align:middle;border: 3px solid #ffffff;margin-left: -12px;">';

                        str += '</div>';
                        str += '<div class="col-md-10">';
                        str +=
                            '<a href="' + baseUrl + '/' + data.link +
                            '" class="m-widget3__username" style="font-size: 13px;font-weight: 500;color: #525252;"><span>' +
                            data.message + '.. More >></span></a><br>';
                        str +=
                            '<span class="m-widget3__time" style="font-size: .85rem;">' +
                            dateCreated +
                            ' &nbsp;<i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i>';
                        str += '</span>';
                        str += '<div class="m-widget3__body">';
                        str +=
                            '<p class="m-widget3__text" style="font-size: 15px; margin-bottom: 0">' +
                            data.content;
                        str += '</p>';
                        str += '</div></div></div>';

                        return str;
                    },
                }],
            };
            $('.m_datatable_sent_highfive').mDatatable(options);
        };

        return {
            init: function (param) {
                receivedComments(param);
            },
        };
    }();

    jQuery(document).ready(function () {
        $('#select_comment_type').selectpicker();
        var param = {
            isPublic: $('#select_comment_type').val()
        };
        console.log(param);
        commentDatatable.init(param);
    });

    $(function () {
        $('#select_comment_type').on('change', function () {
            console.log($(this).val());
            $('.m_datatable_sent_highfive').mDatatable('destroy');
            var param = {
                isPublic: $(this).val()
            };
            commentDatatable.init(param);
            // document.location.href = baseUrl + "/szfive/comments_" + $(this).val();
        });
    });
</script>
<?php $this->load->view('templates/szfive/_includes/scripts');?>