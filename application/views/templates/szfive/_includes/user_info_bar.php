<div class="m-portlet">
    <div class="m-portlet__body" style="padding: 10px 20px;">
        <div class="row">
            <div class="col-lg-10">
                <div class="m-widget5">
                    <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 12px;">
                        <div class="m-widget5__pic">
                            <?php $imageClass = 'main-image-' . $session['uid']; ?>
                            <img class="m-widget7__img main-image-<?php echo $session['uid'];?>" src="<?php echo $user_details->avatar; ?>" onerror='noImageFound("<?php echo $imageClass; ?>")'
                                alt="" style="border-radius: 50%; width: 6rem;border: 1px solid #eee;height: 6rem;">
                        </div>
                        <div class="m-widget5__content" style="padding-top: 14px;">
                            <h1 class="m-widget5__title" style=" font-size: 18px;">
                                <?php echo $user_details->name; ?>
                            </h1>
                            <p>
                                <span class="m-widget5__desc">
                                    <?php echo $user_details->pos_details; ?>
                                </span>
                                <br>
                                <span class="m-widget5__desc">
                                    <?php echo $user_details->dep_name . ' | ' . $user_details->dep_details; ?>
                                </span>
                            </p>
                            <p style="font-size: 12px;">
                                <span style="margin-right: 20px;">
                                    <i class="fa fa-envelope-o"></i>
                                    <?php echo $user_details->email; ?>
                                </span>
                                <span>
                                    <i class="fa fa-user-o"></i> Reviewer:
                                    <?php echo $user_details->supervisorName; ?>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2" style="margin-top: 10px;">
                <!-- <button type="button" class="btn btn-outline-success btn-sm m-btn m-btn--custom">Edit Profile</button> -->
            </div>
        </div>
    </div>
    <div class="m-portlet__head" style="background: #f1f2f7; height: 50px;">
        <ul class="nav nav-tabs m-tabs-line m-tabs-line--brand m-tabs-line--2x" role="tablist" style="margin-bottom: 0 !important;">
        <?php foreach($tabs as $tab) : ?>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link <?php echo $tab['active']; ?>" href="<?php echo base_url() . $tab['link']; ?>" role="tab">
                    <?php echo $tab['title']; ?>
                </a>
            </li>
        <?php endforeach;?>
        </ul>
    </div>
</div>