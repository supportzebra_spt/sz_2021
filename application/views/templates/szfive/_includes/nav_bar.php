<div class="m-portlet" style="margin-bottom: 15px;box-shadow: 0 1px 8px rgba(0,0,0,0.15);border-radius: 3px;border: 1px solid #ffffff;background: #4a4a4a;">
    <div class="m-portlet__body" style="padding: 0;">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <input type="hidden" name="user_type" value="<?php echo $session['role_id']; ?>">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline szfive-nav">
                    <?php foreach($nav_list as $nav) :?>
                    <li class="m-nav__item <?php echo $nav['active'];?>">
                        <a href="<?php echo base_url($nav['link']) ; ?>" class="m-nav__link">
                            <span class="m-nav__link-text"><?php echo $nav['title'];?></span>
                        </a>
                    </li>
                    <?php endforeach ;?>
                </ul>
            </div>
        </div>
    </div>
</div>