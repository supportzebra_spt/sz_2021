<style>
    .heart {
        background: url("<?php echo base_url('assets/images/img/web_heart_animation.png'); ?>");
        background-repeat: no-repeat;
        height: 50px;
        width: 50px;
        cursor: pointer;
        position: absolute;
        background-size: 2900%;
        margin-left: -7px;
        margin-top: -12px;
    }

    .message {
        background: url("<?php echo base_url('assets/images/img/message.png'); ?>");
        background-position: left;
        background-repeat: no-repeat;
        height: 13px;
        width: 15px;
        cursor: pointer;
        position: absolute;
        background-size: contain;
        margin-left: 9px;
        margin-top: 7px;
    }

    .likeType {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        display: block;
        height: 40px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 40px;
        cursor: pointer;
    }

    .likeTypeSmall {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        float: left;
        margin-right: -3px;
        height: 20px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 20px;
        cursor: pointer;
    }

    .likeIconDefault {
        width: 17px;
        float: left;
        margin-right: 5px;
        background: url("<?php echo base_url('assets/images/img/like.png'); ?>");
        height: 16px;
        background-size: 100%;
    }

    .defaultIcon {
        margin-right: 5px;
    }

    .all-reaction {
        background-position: 0 30px;
        height: 15px;
        width: 15px;
    }

    .mention {
        /* padding-right: 150px !important; */
        font-size: 16px;
        margin-bottom: 12px;
    }

    .post-type {
        width: 115px !important;
        margin: 0 auto;
        display: flex !important;
        justify-content: center;
        align-items: center;
        padding-right: 2px;
        border-left: 1px solid #dcdcdc;
    }

    .bootstrap-select {
        padding-right: 0 !important;
    }

    .dropdown-toggle {
        border: none !important;
    }

    #post_type {
        display: none;
    }

    textarea {
        height: initial !important;
    }
	.highfive-user {
		background-image: url(https://image.freepik.com/free-vector/confetti-background_53876-63965.jpg) !important;
	}
	ul#display_highfives li {
		border-bottom: 1px dashed #f0f0f0;
		padding-bottom: 25px;
	}
	.reaction{
		cursor:pointer;
	}
	img.highfive-img {
		background: #fff;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'SZhoutOuts', 'breadcrumbs' => [['title' => 'SZhoutOut', 'link' => 'szfive'], ['title' => 'SZhoutOut', 'link' => 'szfive/high_fives#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content survey-answers" id="highfive-page" data-id="<?php echo $session['uid']; ?>">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet" id="m_blockui_2_portlet" style="margin-bottom: 25px;box-shadow: 0 1px 8px rgba(0,0,0,0.15);border-radius: 3px;border: 1px solid #ffffff;">
                    <div class="m-portlet__body" style="padding: 10px;">
                        <div class="row">
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="m-select2 m-select2--pill">
                                    <input type="hidden" name="stat" />
                                    <select class="form-control m-select2" id="select_highfive_type" name="param" data-placeholder="Pill style">
                                        <option value="all">All High SZhoutOut</option>
                                        <option value='recieved_by'>SZhoutOut received by</option>
                                        <option value='given_by'>SZhoutOut given by</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="m-select2 m-select2--pill">
                                    <select class="form-control m-select2" id="select_highfive_subject" data-placeholder="Pill style">
                                        <option value="company_wide" selected>Company-wide</option>
                                        <?php foreach ($all_users as $user): ?>
                                        <!-- <?php $selected = ($user->uid == $session['emp_id']) ? 'selected' : '';?> -->
                                        <option value="<?php echo $user->uid; ?>">
                                            <?php echo $user->fname . ' ' . $user->lname; ?>
                                        </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <a href="<?php echo base_url('szfive/activity_public'); ?>" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--pill">
                                    <i class="fa fa-hand-stop-o"></i> My SZhoutOut
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-8" style="margin-bottom: 20px;">
                <div class="m-portlet" style="margin-bottom: 15px;">
                    <div class="m-portlet__body" id="portlet_mention_user">
                        <div style="margin-bottom: 20px;">
                            <h4>
                                <img src="<?php echo base_url('/assets/images/img/hi5.png'); ?>" width="30" style="filter: saturate;"> &nbsp;Give a SZhoutOut!
                            </h4>
                            <p>Who did something amazing that made your day? Share a bit about why. Public High Fives will be
                                posted to the feed.
                            </p>
                            <form id="user_mention_form">
                                <div class="m-portlet__body" style="padding: 0;">
                                    <div class="row" id="post_type">
                                        <div class="col-lg-3" style="margin-left: -12px;">
                                            <select class="form-control m-bootstrap-select m_selectpicker" name="postType" id="select_post_type">
                                                <option data-icon="fa fa-globe" value="1"> Public</option>
                                                <option data-icon="fa fa-lock" value="0"> Private</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <textarea class="form-control m-input highfive_mention" placeholder="Write a message that includes an @mention or full name"
                                            rows="1" name="user_highfive"></textarea>
                                    </div>
                                    <div class="m-form__group form-group" id="highfive_options">
                                        <button data-toggle="tooltip" title="Please give a High Five first that includes an '@' symbol" type="submit" class="btn btn-brand"
                                            id="submitMentions">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="m-portlet">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <ul id="display_highfives" style="list-style-type: none; padding-left: 0;">
                                    </ul>
                                    <div id="loadingMore" style="text-align: center;">
                                        <img src="<?php echo base_url('assets/images/img/spinner.gif'); ?>" style="width: 15px;"> Loading more High Fives...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4" id="high-five-stats">
                <?php if ($user_stat): ?>
                <div class="m-portlet" id="m_blockui_2_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo $user_stat->title; ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="text-align: center;">
                        <div class="highfive-user">
                            <img class="m-widget3__img highfive-img hi5img<?php echo $user_stat->uid; ?>" src="<?php echo base_url( $user_stat->pic); ?>"
                                onerror="noHi5Img(<?php echo $user_stat->uid; ?>)" alt="">
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-widget19__info" style=" margin: 8px 0px;">
                                    <span class="m-widget19__username" style="font-weight: 500;font-size: 16px;">
                                        <?php echo $user_stat->username; ?>
                                    </span>
                                    <br>
                                    <?php echo $session['role']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 15px;">
                            <div class="col-md-6">
                                <div class="m-widget19__info">
                                    <span class="m-widget19__username" style="font-weight: 400;font-size: 16px;">
                                        <?php echo $user_stat->sent_count . ' sent'; ?>
                                        <img src="<?php echo base_url('assets/images/img/hi5.png'); ?>" width="30" style="margin-top: -8px;">
                                    </span>
                                    <br>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="m-widget19__info">
                                    <span class="m-widget19__username" style="font-weight: 400;font-size: 16px;">
                                        <?php echo $user_stat->receive_count . ' received'; ?>
                                        <img src="<?php echo base_url('assets/images/img/hi5.png'); ?>" width="30" style="margin-top: -8px;">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif;?>
                <?php foreach ($highfives as $highfive): ?>
                <div class="m-portlet" id="m_blockui_2_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo $highfive->title; ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body" style="text-align: center;">
                        <div class="highfive-user">
                            <img class="m-widget3__img highfive-img hi5img<?php echo $highfive->uid; ?>" src="<?php echo $highfive->pic; ?>" onerror="noHi5Img(<?php echo $highfive->uid; ?>)"
                                alt="">
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="m-widget19__info" style=" margin: 8px 0px;">
                                    <span class="m-widget19__username" style="font-weight: 500;font-size: 16px;">
                                        <?php echo $highfive->fname . ' ' . $highfive->lname; ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div style="padding-bottom: 15px;">
                            <div>
                                <span class="m-widget19__username" style="font-size: 15px;">
                                    <?php echo $highfive->pos_details; ?>
                                </span>
                                <br>
                                <span class="m-widget19__time" style=" font-size: 12px; ">
                                    <?php echo $highfive->acc_name; ?>
                                </span>
                            </div>
                            <div>
                                <span class="m-widget19__comment" style=" font-size: 27px; ">
                                    <?php echo $highfive->count; ?>
                                </span>
                                <img src="<?php echo base_url('assets/images/img/hi5.png'); ?>" width="30" style="margin-top: -8px;">
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
            <?php include '_modals.php';?>
        </div>
    </div>
</div>
<?php $this->load->view('templates/szfive/_includes/scripts');?>
<script src="<?php echo base_url('assets/src/custom/js/szfive.highfive.js'); ?>" type="text/javascript"></script>