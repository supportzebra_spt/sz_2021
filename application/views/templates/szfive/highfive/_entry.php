<style>
    #reviewTextarea {
        display: none;
    }

    .m-portlet {
        margin-bottom: 10px;
    }

    .m-portlet__body {
        padding-top: 15px !important;
    }

    .m-widget5__pic {
        margin: 0 auto;
        text-align: center;
        vertical-align: middle;
    }

    .m-widget5__title {
        margin-bottom: 0;
        font-size: 24px !important;
        font-weight: 300 !important;
    }

    .heart {
        background: url("<?php echo base_url('assets/images/img/web_heart_animation.png'); ?>");
        background-repeat: no-repeat;
        height: 50px;
        width: 50px;
        cursor: pointer;
        position: absolute;
        background-size: 2900%;
        margin-left: -7px;
        margin-top: -12px;
    }

    .message {
        background: url("<?php echo base_url('assets/images/img/message.png'); ?>");
        background-position: left;
        background-repeat: no-repeat;
        height: 13px;
        width: 15px;
        cursor: pointer;
        position: absolute;
        background-size: contain;
        margin-left: 9px;
        margin-top: 7px;
    }

    .likeType {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        display: block;
        height: 40px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 40px;
        cursor: pointer;
    }

    .likeTypeSmall {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        float: left;
        margin-right: -3px;
        height: 20px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 20px;
        cursor: pointer;
    }

    .likeIconDefault {
        width: 17px;
        float: left;
        margin-right: 5px;
        background: url("<?php echo base_url('assets/images/img/like.png'); ?>");
        height: 16px;
        background-size: 100%;
    }

    #comment-options {
        display: none;
        margin-top: 10px;
    }

    .autoExpand {
        margin-bottom: 10px;
    }

    .h-actions {
        padding: 0 10px;
    }

    .m-nav__link {
        cursor: pointer;
    }

    .m-nav__link-icon {
        width: 25px !important;
    }

    textarea {
        resize: none;
    }

    .defaultIcon {
        margin-right: 5px;
    }

    a#jumpToComment {
        color: unset;
        text-decoration: none;
    }

    #highfive-entry-id {
        margin-bottom: 0;
        /* background: #f2f3f8; */
        padding: 10px;
    }

    #hi5commentDetail {
        height: 38px;
    }


    /* #m-scroll-highfive-comments {
        max-height: 450px;
    } */
</style>
<script>
    function noImageA(id) {
        $('.user-pic-answer' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }

    function noImageSup(id) {
        $('.supervisor-pic-' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }

    function noCommentImage(id) {
        $('.comment-pic-dt' + id).attr('src', baseUrl + '/assets/images/img/sz.png');
    }
</script>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'High Fives', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive'], ['title' => 'High Fives', 'link' => 'szfive/high_fives'], ['title' => 'Feeds', 'link' => uri_string()]]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content" id="highfive-page" id="highfive-page" data-id="<?php echo $session['uid']; ?>">
        <div class="row">
            <div class="col-lg-9">
                <div class="m-portlet m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded" style="margin-bottom: 20px;">
                    <div class="m-portlet__body">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="m-widget5">
                                    <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 0;">
                                        <div class="m-widget5__pic">
                                            <img class="m-widget7__img user-pic-answer<?php echo $entry->uid; ?>" src="<?php echo $entry->pic; ?>"
                                                onerror="noImageA(<?php echo $entry->uid; ?>)" alt="" style="border-radius: 50%; width: 4rem;border: 1px solid #eee;">
                                        </div>
                                        <div class="m-widget5__content">
                                            <h1 class="m-widget5__title">
                                                <?php echo $entry->fullname; ?>
                                            </h1>
                                            <span class="m-widget5__desc">
                                                <?php echo $entry->department; ?>
                                            </span>
                                            <br>
                                            <span class="m-widget5__desc" style="color: #00BCD4;ont-size: 13px;font-weight: 500;">
                                                <?php echo $entry->position; ?>
                                            </span>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-widget5">
                                    <div class="m-widget5__item" style="margin-bottom: 0;padding-bottom: 0;">
                                        <div class="m-widget5__content" style="padding-top: 10px;padding-left: 0;text-align: right;">
                                            <span class="m-widget5__desc" style="font-size: 11px;">
                                                Submitted on
                                                <span style=" font-weight: 500; ">
                                                    <?php echo $entry->date_answered; ?>
                                                </span>
                                            </span>
                                            <br>
                                            <?php if ($entry->isReviewed == 1): ?>
                                            <span class="m-widget5__desc" style="font-size: 11px;">
                                                Reviewed by
                                                <span style=" font-weight: 500; ">
                                                    <?php echo $entry->supervisorFname . ' ' . $entry->supervisorLname; ?>
                                                </span> on
                                                <span style=" font-weight: 500; ">
                                                    <?php echo $entry->date_reviewed; ?>
                                                </span>
                                            </span>
                                            <?php else: ?>
                                            <?php if ($entry->supId == $session['emp_id']): ?>
                                            <button type="button" class="btn m-btn--pill btn-outline-success btn-sm "
                                                onClick="reviewHighfive(<?php echo $entry->hiFiveId; ?>)" style="margin-top: 8px;">Review</button>
                                            <?php endif;?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded">
                    <div class="m-portlet__body" style="margin-top: 10px;">

                        <div>
                            <h5 style="color: #555;font-weight: 400;border-bottom: 1px solid #eee;margin-bottom: 10px;">High
                                Fives
                                <?php if($entry->isPublic == '1') :?>
                                <i class="m--font-metal glyphicon fa fa-globe"></i>
                                <?php else :?>
                                <i class="m--font-metal glyphicon fa fa-lock"></i>
                                <?php endif ;?>
                            </h5>
                            <p style="font-size: 16px;margin-bottom: 10px;margin-top: 15px;">
                                <?php echo $entry->message; ?>
                            </p>
                            <?php $comment = '';?>
                            <?php if ($entry->totalComment->count != "0"): ?>
                            <?php $comment = "<span class='like-info pull-right like-info" . $entry->hiFiveId . "'>" . intval($entry->totalComment->count) . ' comments </span>';?>
                            <?php endif;?>
                            <div style="margin-top: 15px;">
                                <span id="all-reaction<?php echo $entry->hiFiveId; ?>">
                                    <?php echo $entry->reactions; ?>
                                </span>
                                <a data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="<?php echo $entry->tooltip; ?>" class="like-info" id="like-info<?php echo $entry->hiFiveId; ?>">
                                    <span style="margin-left: 10px;">
                                        <?php echo $entry->likes_info; ?>
                                    </span>
                                </a>
                                <?php echo $comment; ?>
                            </div>
                            <div class="row" style="text-align: center;margin-left: 0px;margin-top: 5px;font-size: 12px;">
                                <?php if ($entry->my_reaction == ''): ?>

                                <a class="reaction" id="like<?php echo $entry->hiFiveId; ?>" rel="like" style="margin-right: 10px;">
                                    <i class="likeIconDefault"></i> Like
                                    <?php else: ?>
                                    <a class="unLike" id="like<?php echo $entry->hiFiveId; ?>" rel="unlike" style="margin-right: 10px;">
                                        <?php endif;?>
                                        <?php echo $entry->my_reaction; ?>
                                    </a>
                                    <!-- <a href="#commentTextarea" id="jumpToComment">
                                        <i class="fa fa-comment" style="color: #afb4bd;font-size: 1rem;"></i> Comment</a> -->
                            </div>
                            <?php if($entry->totalComment->count > 5) :?>
                            <button type="button" class="btn btn-outline-brand btn-sm" id="viewPreviousComments"
                                onClick="viewPreviousComments(<?php echo $entry->hiFiveId; ?>)" style="margin-top: 15px;">View
                                previous comments
                            </button>
                            <?php endif; ?>
                            <div style="margin-top: 20px;" id="highfive-entry">
                                <!-- <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" id="m-scroll-highfive-comments"> -->
                                <div id="comments-container">
                                    <?php foreach ($child_comments as $comment): ?>
                                    <div class="m-widget3__header comment-cwrapper comment-cwrapper<?php echo $comment->commentId; ?>"
                                        id="user-comment-<?php echo $comment->commentId; ?>">
                                        <div class="m-widget3__user-img" id="comment_type" data-id="<?php echo $comment->isPublic; ?>">
                                            <img class="m-widget3__img comment-pic comment-pic-dt<?php echo $comment->emp_id; ?>"
                                                src="<?php echo base_url('assets/images/') . $comment->pic; ?>" onerror="noCommentImage(<?php echo $comment->emp_id; ?>)"
                                                alt="">
                                        </div>
                                        <div class="m-widget3__info comment-info">
                                            <span class="m-widget3__username comment-name">
                                                <?php echo $comment->fname . ' ' . $comment->lname; ?>
                                            </span>
                                            <span style="font-size: 11px;font-style: italic;margin-left: 5px;">
                                                <?php echo $comment->dateCreated; ?>&nbsp;
                                                <?php if ($comment->isPublic == 1) : ?>
                                                <i class="fa fa-globe" style="font-size: 12px;color: #767e84d6;"></i>
                                                <?php else: ?>
                                                <i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i>
                                                <?php endif ;?>
                                            </span>
                                            <p class="m-widget3__time comment-content" id="comment-content-<?php echo $comment->commentId; ?>-"
                                                style="margin-bottom: 0;">
                                                <?php echo $comment->content; ?>
                                            </p>
                                        </div>
                                        <?php if ($comment->uid == $session['uid']): ?>
                                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                            data-dropdown-toggle="click" aria-expanded="true">
                                            <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link action-btn"
                                                style="margin-right: 20px;">
                                                <i class="la la-ellipsis-h"></i>
                                            </a>
                                            <div class="m-dropdown__wrapper" style="width: 100px;margin-right: -7px;    margin-top: -10px;">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"
                                                    style="left: auto; right: 10px;"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__body" style="padding: 0;">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav">
                                                                <li class="m-nav__item h-actions">
                                                                    <a onclick="showEditComment(<?php echo $comment->commentId; ?>)"
                                                                        class="m-nav__link">
                                                                        <i class="m-nav__link-icon la la-edit"></i>
                                                                        <span class="m-nav__link-text" style="font-size: 12px;">Edit</span>
                                                                    </a>
                                                                </li>
                                                                <li class="m-nav__separator m-nav__separator--fit"
                                                                    style="margin: 0;"></li>
                                                                <li class="m-nav__item h-actions">
                                                                    <a class="m-nav__link" onclick="deleteHi5Comment(<?php echo $comment->commentId; ?>,<?php echo $entry->hiFiveId; ?>)">
                                                                        <i class="m-nav__link-icon la la-trash-o"></i>
                                                                        <span class="m-nav__link-text" style="font-size: 12px;">Delete</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif;?>
                                    </div>
                                    <?php endforeach;?>
                                </div>
                                <!-- </div> -->
                                <form class="m-form" id="commentTextarea">
                                    <div class="form-group m-form__group" style="margin-bottom: 0;background: white !important;overflow: hidden !important;"
                                        id="highfive-entry-id" data-id="<?php echo $entry->hiFiveId; ?>">
                                        <textarea class="form-control m-input autoExpand highfive_post_comment" name="comment"
                                            id="inputHi5Comment" rows="1" data-min-rows='1' style="resize: none;"
                                            placeholder='Write something...'></textarea>
                                        <div id="comment-options">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="m-checkbox-inline">
                                                        <label class="m-checkbox">
                                                            <input type="checkbox" id="checkPrivate"> Private comment
                                                            to
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <select class="form-control m-bootstrap-select m_selectpicker" name="postType"
                                                        id="select_user">
                                                        <?php foreach ($sent_to_users as $key => $users): ?>
                                                        <option value="<?php echo $key; ?>">
                                                            <?php echo $users; ?>
                                                        </option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-brand" id="submitMentions">Post
                                                comment</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
						
                        <?php if ($entry->supId != null): ?>
                        <div style="text-align: center;">
                            <div style="margin-bottom: 10px;margin-top: 15px;">
                                <img class="supervisor-pic-<?php echo $entry->supId; ?>" src="<?php echo base_url('assets/images/') . $entry->supervisorPic; ?>"
                                    onerror="noImageSup(<?php echo $entry->supId; ?>)" style="border-radius: 50%;width: 4rem;border: 1px solid #eee;height: 4rem;" />
                            </div>
                            <p style="font-size: 13px;">
                                <span style="font-weight: 500;">
                                    <?php echo $entry->supervisorFname . ' ' . $entry->supervisorLname; ?>
                                </span>
                                <br>
                                <span>Reviewer</span>
                                <br>
                                <?php if ($entry->isReviewed == 1): ?>
                                <span class="m--font-success">
                                    <i class="fa fa-check m--font-success" style="font-size: 12px;"></i> Reviewed
                                </span>
                                <?php endif;?>
                            </p>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
        <!-- start:: Reviews - View Specific Answer modal -->
        <div class="modal fade" id="view_specific_ans_reviews_modals" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: white;padding: 10px;">
                        <div id="view_answer_review_header"></div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #599791 !important;margin: 0;">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="view_reviews_answer_content"></div>
                        <div id="mark_as_review_wrapper"></div>
                        <div id="reviewTextarea" style="margin-top: 20px;">
                            <form class="m-form m-form--fit m-form--label-align-right" id="reviewAnswerForm">
                                <div class="form-group m-form__group" style=" padding: 0; ">
                                    <input type="hidden" name="answerId" />
                                    <textarea class="form-control m-input m-input--air" name="reviewMessage" id="reviewAnswerTextarea"
                                        rows="3"></textarea>
                                    <span class="m-form__help">Please note: This field will appear if answer requires a
                                        remark/comment.</span>
                                    <div class="m-form__actions" style=" padding: 10px 0; ">
                                        <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn-sm"
                                            id="submitReview">Submit</button>
                                        <button type="reset" id="cancelReview" class="btn m-btn--pill btn-secondary btn-sm">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="alreadyReviewed"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- start :: Edit Comment modal -->
        <div class="modal fade" id="edit_comment_hi5_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #505a6b;padding: 15px;">
                        <h5 class="modal-title">Edit Comment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="la la-remove"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="m-form m-form--fit m-form--label-align-right" id="hi5_comment_form_edit">
                            <div class="m-portlet__body survey-form" style="padding-top: 0 !important;">
                                <div class="m-form__content">
                                    <div class="m-alert m-alert--icon alert alert-warning m--hide" role="alert" id="m_form_2_msg">
                                        <div class="m-alert__icon">
                                            <i class="la la-warning"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            Oh snap! Change a few things up and try submitting again.
                                        </div>
                                        <div class="m-alert__close">
                                            <button type="button" class="close" data-close="alert" aria-label="Close">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group" id="edit-hi5comment">
                                    <input type="hidden" name="commentId" />
                                    <input type="hidden" name="isPublic" />
                                    <textarea class="form-control m-input m-input--air highfive_edit_comment" name="comment_edit"
                                        id="hi5commentDetail" rows="1" data-min-rows='1'></textarea>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button type="submit" class="btn btn-brand">Update</button>
                                            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end :: Edit Comment modal -->
    </div>
</div>
<?php $this->load->view('templates/szfive/_includes/scripts');?>
<script type="text/javascript">
    var offsetCommentCounter = 1;
    var offsetComment = 5;

    $(".likeTypeAction").tipsy({
        gravity: 's',
        live: true
    });

    $(".reaction").livequery(function () {
        var reactionsCode =
            '<span class="likeTypeAction" original-title="Like" data-reaction="1"><i class="likeIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Love" data-reaction="2"><i class="loveIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Haha" data-reaction="3"><i class="hahaIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Wow" data-reaction="4"><i class="wowIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Cool" data-reaction="5"><i class="coolIcon likeType"></i></span>' +
            // '<span class="likeTypeAction" original-title="Confused" data-reaction="6"><i class="confusedIcon likeType"></i></span>' +
            '<span class="likeTypeAction" original-title="Sad" data-reaction="7"><i class="sadIcon likeType"></i></span>' +
            '<span class="likeTypeAction last" original-title="Angry" data-reaction="8"><i class="angryIcon likeType"></i></span>';

        $(this).tooltipster({
            contentAsHTML: true,
            interactive: true,
            content: $(reactionsCode),
        });
    });

    /*Reaction*/
    $("body").on("click", ".likeTypeAction", function () {
        var reactionType = $(this).attr("data-reaction");
        var reactionName = $(this).attr("original-title");
        var rel = $(this).parent().parent().attr("rel");
        var x = $(this).parent().parent().attr("id");
        var sid = x.split("reaction");
        var highfiveId = sid[1];
        var htmlData = '<i class="' + reactionName.toLowerCase() +
            'IconSmall likeTypeSmall defaultIcon"></i>' + reactionName;

        $.when(fetchPostData({
                    highfiveId: highfiveId,
                    reactionType: reactionType
                },
                '/szfive/add_high_five_reaction'))
            .then(function (response) {

                var data = jQuery.parseJSON(response);

                if (data.success == true) {

                    var val = data.details;

                    $('#like-info' + highfiveId).html('<span style="margin-left: 10px;">' + val.likes_info);
                    $('#all-reaction' + highfiveId).html(val.reactions);
                    $("#like" + highfiveId).html(htmlData).removeClass('reaction').removeClass(
                        'tooltipstered').addClass('unLike').attr('rel', 'unlike');
                    $("#" + x).hide();

                    if (data.system_notif_ids.length != 0) {
                        systemNotification(data.system_notif_ids);
                    }
                }
            });

        return false;
    });

    $("body").on("click", ".unLike", function () {
        var reactionType = '1';
        var x = $(this).attr("id");
        var sid = x.split("like");
        var highfiveId = sid[1];
        var htmlData = '<i class="likeIconDefault" ></i>Like';

        $.when(fetchPostData({
                    highfiveId: highfiveId,
                    reactionType: reactionType
                },
                '/szfive/delete_high_five_reaction'))
            .then(function (response) {
                var resp = jQuery.parseJSON(response);

                if (resp.success == 1) {

                    if (resp.data.length == 0) {
                        $('#like-info' + highfiveId).html('');
                        $('#all-reaction' + highfiveId).html("");
                    } else {
                        var val = resp.data;
                        $('#like-info' + highfiveId).html(val.likes_info);
                        $('#all-reaction' + highfiveId).html('<span style="margin-left: 10px;">' +
                            val.reactions);

                    }
                    $("#like" + highfiveId).html(htmlData).addClass('reaction').addClass('tooltipstered').removeClass(
                        'unLike');
                }
            });

        return false;
    });


    $('textarea.autoExpand').on('focus', function () {
        $('#comment-options').show(300);
    });

    function viewPreviousComments(entryId) {

        var param = {
            entryId: entryId,
            offset: offsetComment,
            isPublic: $('#comment_type').data('id'),
        };

        $.when(fetchPostData({
                    param
                },
                '/szfive/load_highfive_entry_comments'))
            .then(function (response) {
                response = jQuery.parseJSON(response);
                str = '';

                $.each(response.comments, function (key, val) {
                    str += '<div class="m-widget3__header comment-cwrapper comment-cwrapper' + val.commentId +
                        '" id="user-comment-' + val.commentId + '">';
                    str += '<div class="m-widget3__user-img">';
                    str += '<img class="m-widget3__img comment-pic comment-pic-dt' + val.emp_id + '" src="' +
                        baseUrl + '/assets/images/' + val.pic + '" onerror="noCommentImage(' + val.emp_id + ')" alt="">';
                    str += '</div>';
                    str += '<div class="m-widget3__info comment-info">';
                    str += '<span class="m-widget3__username comment-name">' + val.fname + ' ' + val.lname;
                    str += '</span>';
                    str += '<span style="font-size: 11px;font-style: italic;margin-left: 5px;">' + val.dateCreated;
                    if (val.isPublic == 1) {
                        str +=
                            '&nbsp; <i class="fa fa-globe" style="font-size: 12px;color: #767e84d6;"></i>';
                    } else {
                        str +=
                            '&nbsp; <i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i>';
                    }
                    str += '</span>';
                    str += '<p class="m-widget3__time comment-content" id="comment-content-' + val.commentId +
                        '-" style="margin-bottom: 0;">' + val.content;
                    str += '</p>';
                    str += '</div>';
                    if (val.uid == response.session_id) {
                        str +=
                            '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right  m-dropdown--align-push" data-dropdown-toggle="click" aria-expanded="true">';
                        str +=
                            '<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link action-btn" style="margin-right: 20px;">';
                        str += '<i class="la la-ellipsis-h"></i>';
                        str += '</a>';
                        str += '<div class="m-dropdown__wrapper" style="width: 100px;margin-right: -7px;margin-top: -10px;">';
                        str +=
                            '<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 29.7032px;"></span>';
                        str += '<div class="m-dropdown__inner">';
                        str += '<div class="m-dropdown__body" style="padding: 0;">';
                        str += '<div class="m-dropdown__content">';
                        str += '<ul class="m-nav">';
                        str += '<li class="m-nav__item h-actions">';
                        str += '<a onclick="showEditComment(' + val.commentId + ')" class="m-nav__link">';
                        str += '<i class="m-nav__link-icon la la-edit"></i>';
                        str += '<span class="m-nav__link-text" style="font-size: 12px;">Edit</span>';
                        str += '</a>';
                        str += '</li>';
                        str +=
                            '<li class="m-nav__separator m-nav__separator--fit" style="margin: 0;"></li>';
                        str += '<li class="m-nav__item h-actions">';
                        str += '<a class="m-nav__link" onclick="deleteHi5Comment(' + val.commentId + ',' +
                            val.hiFiveId + '">';
                        str += '<i class="m-nav__link-icon la la-trash-o"></i>';
                        str += '<span class="m-nav__link-text" style="font-size: 12px;">Delete</span>';
                        str += '</a></li></ul></div></div> </div></div></div>';
                    }
                    str += '</div>';
                });

                $('#comments-container').prepend(str);
                $("#comments-container").animate({
                    scrollTop: 0
                }, "slow");

                offsetCommentCounter = offsetCommentCounter + 1;
                offsetComment = offsetCommentCounter * 5;

                if (response.total < offsetComment) {
                    $('#viewPreviousComments').remove();
                    offsetCommentCounter = 0;
                    offsetComment = 0;
                }
            });
    };

    function reviewHighfive(id) {

        $.when(fetchPostData({
                id: id
            }, '/szfive/review_highfive'))
            .then(function (response) {

                response = jQuery.parseJSON(response);

                if (response.status == 1) {
                    swal({
                        "title": "",
                        "text": "Reviewed High Five!",
                        "type": "success",
                        "timer": 1500,
                        "showConfirmButton": false
                    });

                    location.reload();
                }
            });
    }

    function deleteHi5Comment(id, postId) {
        swal({
            title: '',
            text: 'Are you sure you want to permanently remove this comment?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel'
        }).then(function (result) {
            if (result.value === true) {

                $.ajax({
                    url: baseUrl + "/szfive/delete_hi5_comment",
                    type: 'POST',
                    dataType: "json",
                    data: {
                        id: id
                    },
                    success: function (data) {
                        mApp.blockPage({
                            overlayColor: '#000000',
                            type: 'loader',
                            state: 'success',
                            message: 'Please wait...'
                        });

                        setTimeout(function () {
                            mApp.unblockPage();
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };

                            toastr.success("Comment deleted.");
                            $('.comment-cwrapper' + id).remove();
                            var C = parseInt($(".like-info" + postId).html());
                            $(".like-info" + postId).html(C - 1 + ' comments');

                        }, 1000);
                    },
                    error: function () {
                        swal({
                            "title": "",
                            "text": "Error",
                            "type": "error",
                            "timer": 2000,
                            "showConfirmButton": false
                        });
                    }
                });
            }
        });
    }

    function showEditComment(id) {
        $('textarea.highfive_edit_comment').mentionsInput({
            onDataRequest: function (mode, query, callback) {
                $.getJSON(baseUrl + '/szfive/user_comment_mentions', function (responseData) {
                    responseData = _.filter(responseData, function (item) {
                        return item.name.toLowerCase().indexOf(query.toLowerCase()) >
                            -1
                    });
                    callback.call(this, responseData);
                });
            },
            templates: {
                mentionItemSyntax: _.template('<%= name %>'),
            }
        });
        $.when(fetchPostData({
                id: id
            }, '/szfive/get_hi5_comment_details'))
            .then(function (response) {

                var data = jQuery.parseJSON(response);

                $('textarea#hi5commentDetail').val(data.content);
                $('input[name="commentId"]').val(data.id);
                $('input[name="isPublic"]').val(data.isPublic);
            });

        $('#edit_comment_hi5_modal').modal('show');

    }
    // Applied globally on all textareas with the "autoExpand" class

    var Hi5CommentValidation = function () {

        var addComment = function () {

            $("#commentTextarea").validate({
                rules: {
                    comment: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                },
                invalidHandler: function (event, validator) {
                    swal({
                        "title": "",
                        "text": "Comment must not be empty.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },
                submitHandler: function (form) {

                    // var val = $('#commentTextarea').find('textarea#inputHi5Comment').val();
                    // var comment = val.replace(/\s\s+/g, ' ');
                    var hiFiveId = $("#highfive-entry-id").data('id');
                    var privateToUser = $('#select_user').val();
                    var isPublic = 1;
                    if ($('#checkPrivate').is(':checked')) {
                        isPublic = 0;
                    }
                    var content = '';
                    var usermention = 0;

                    $('textarea.highfive_post_comment').mentionsInput('val', function (userMention) {
                        if (userMention != '') {
                            content = userMention;
                            $(this).val("");
                        }
                    });

                    $('textarea.highfive_post_comment').mentionsInput('getMentions', function (data) {
                        if (data.length != 0) {
                            usermention = data;
                        }
                    });

                    var options = {
                        url: baseUrl + "/szfive/add_highfive_comment",
                        type: 'POST',
                        dataType: "json",
                        data: {
                            hiFiveId: hiFiveId,
                            privateToUser: privateToUser,
                            isPublic: isPublic,
                            content: content,
                            usermention: usermention
                        },
                        success: function (resp) {
                            var val = resp.data;

                            if (resp.system_notif_ids.length != 0) {
                                systemNotification(resp.system_notif_ids);
                            }

                            $('#inputHi5Comment').val("");

                            var userId = $('#highfive-page').data('id');
                            var img = $(".header-userpic" + userId).attr('src')

                            str =
                                '<div class="m-widget3__header comment-cwrapper comment-cwrapper' +
                                val.commentId +
                                '" id="user-comment-' +
                                val.commentId +
                                '">';
                            str += '<div class="m-widget3__user-img">';
                            str += '<img class="m-widget3__img comment-pic comment-pic-dt' +
                                val.emp_id + '" src="' +
                                img + '" onerror="noCommentImage(' + val.emp_id +
                                ')" alt="">';
                            str += '</div>';
                            str += '<div class="m-widget3__info comment-info">';
                            str += '<span class="m-widget3__username comment-name">' + val.fullname +
                                '</span><span style="font-size: 11px;font-style: italic;margin-left: 5px;">' +
                                val.dateCreated;
                            if (val.isPublic == 1) {
                                str +=
                                    '&nbsp; <i class="fa fa-globe" style="font-size: 12px;color: #767e84d6;"></i>';
                            } else {
                                str +=
                                    '&nbsp; <i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i>';
                            }
                            str += '</span>';
                            str +=
                                '<p class="m-widget3__time comment-content" id="comment-content-' +
                                val.commentId +
                                '-" style="margin-bottom: 0;">' + val.content + '</p>';
                            str += '</div>';
                            str +=
                                '<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="click" aria-expanded="true">';
                            str +=
                                '<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn m-btn m-btn--link action-btn" style="margin-right: 20px;">';
                            str += '<i class="la la-ellipsis-h"></i>';
                            str += ' </a>';
                            str +=
                                ' <div class="m-dropdown__wrapper" style="width: 100px;margin-right: -7px;margin-top: -10px;">';
                            str +=
                                ' <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 29.7032px;"></span>';
                            str += '<div class="m-dropdown__inner">';
                            str += '<div class="m-dropdown__body" style="padding: 0;">';
                            str += ' <div class="m-dropdown__content">';
                            str += '<ul class="m-nav">';
                            str += ' <li class="m-nav__item h-actions">';
                            str += '<a onClick="showEditComment(' + val.commentId +
                                ')" class="m-nav__link">';
                            str += '<i class="m-nav__link-icon la la-edit"></i>';
                            str +=
                                '<span class="m-nav__link-text" style="font-size: 12px;">Edit</span>';
                            str += '</a>';
                            str += '</li>';
                            str +=
                                '<li class="m-nav__separator m-nav__separator--fit" style="margin: 0;"></li>';
                            str += ' <li class="m-nav__item h-actions">';
                            str += '<a class="m-nav__link" onClick="deleteHi5Comment(' +
                                val.commentId +
                                ',' + val.post_id + ')">';
                            str += ' <i class="m-nav__link-icon la la-trash-o"></i>';
                            str +=
                                ' <span class="m-nav__link-text" style="font-size: 12px;">Delete</span>';
                            str += '</a>';
                            str += '</li>';
                            str += ' </ul>';
                            str += ' </div>';
                            str += '</div>';
                            str += '</div>';
                            str += ' </div>';
                            str += '</div>';
                            str += '</div>';

                            mApp.blockPage({
                                overlayColor: '#000000',
                                type: 'loader',
                                state: 'success',
                                message: 'Please wait...'
                            });

                            setTimeout(function () {
                                mApp.unblockPage();
                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-center",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "2000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                };

                                toastr.success("Comment successfully sent!");

                                $('#comments-container').append(str);
                                var C = parseInt($(".like-info" + hiFiveId).html());
                                $(".like-info" + hiFiveId).html(C + 1 + ' comments');
                                $('#checkPrivate').prop('checked', false);
                            }, 1000);
                        },
                        error: function () {
                            alert('error');
                        },
                    };
                    $(form).ajaxSubmit(options);
                }
            });
        }

        var editComment = function () {

            $("#hi5_comment_form_edit").validate({
                rules: {
                    comment_edit: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                },
                invalidHandler: function (event, validator) {
                    swal({
                        "title": "",
                        "text": "Comment must not be empty.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },
                submitHandler: function (form) {

                    var content = $('#highfive_edit_comment_form').find(
                        'input[name="comment_edit"]').val();
                    var commentId = $('input[name="commentId"]').val();
                    var usermention = 0;
                    var hiFiveId = $("#highfive-entry-id").data('id');
                    var isPublic = $('input[name="isPublic"]').val();

                    $('textarea.highfive_edit_comment').mentionsInput('val', function (userMention) {
                        if (userMention != '') {
                            content = userMention;
                        }
                    });

                    $('textarea.highfive_edit_comment').mentionsInput('getMentions', function (data) {
                        if (data.length != 0) {
                            usermention = data;
                        }
                    });

                    var options = {
                        url: baseUrl + "/szfive/update_hi5_comment",
                        type: 'POST',
                        dataType: "json",
                        data: {
                            hiFiveId: hiFiveId,
                            commentId: commentId,
                            content: content,
                            usermention: usermention,
                            privateToUser: 0,
                            isPublic: isPublic,
                        },
                        success: function (response) {

                            if (response.status) {

                                if (response.system_notif_ids.length != 0) {
                                    systemNotification(response.system_notif_ids);
                                }

                                mApp.block('#edit_comment_hi5_modal .modal-content', {
                                    overlayColor: '#000000',
                                    type: 'loader',
                                    state: 'primary',
                                    message: 'Processing...'
                                });

                                setTimeout(function () {
                                    mApp.unblock(
                                        '#edit_comment_hi5_modal .modal-content'
                                    );

                                    toastr.options = {
                                        "closeButton": false,
                                        "debug": false,
                                        "newestOnTop": false,
                                        "progressBar": false,
                                        "positionClass": "toast-top-center",
                                        "preventDuplicates": false,
                                        "onclick": null,
                                        "showDuration": "300",
                                        "hideDuration": "1000",
                                        "timeOut": "2000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    };
                                    $('#comment-content-' + commentId + '-').html(
                                        response.comment);
                                    $('#edit_comment_hi5_modal').modal('hide');

                                    toastr.success("Comment successfully updated.");

                                }, 1000);

                            } else {
                                swal({
                                    "title": "Error",
                                    "text": "There was an error updating your comment!",
                                    "type": "error",
                                    "timer": 1500,
                                    "showConfirmButton": false
                                });
                            }

                        },
                        error: function () {
                            alert('error');
                        },
                    };
                    $(form).ajaxSubmit(options);
                }
            });
        }

        return {
            init: function () {
                addComment();
                editComment();
            }
        };
    }();

    jQuery(document).ready(function () {
        $('#select_user').selectpicker();
        $('.like-info').tooltip();
        Hi5CommentValidation.init();
        // var height = $('#comments-container').height() + 10;
        // $('#m-scroll-highfive-comments').css('max-height', height);
        $(document)
            .on('focus', 'textarea', function () {
                var savedValue = this.value;
                this.value = '';
                this.baseScrollHeight = this.scrollHeight;
                this.value = savedValue;
            })
            .on('input', 'textarea', function () {
                var minRows = this.getAttribute('data-min-rows') | 0,
                    rows;
                this.rows = minRows;
                rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
                this.rows = minRows + rows;
            });

        $('textarea.highfive_post_comment').mentionsInput({
            onDataRequest: function (mode, query, callback) {
                $.getJSON(baseUrl + '/szfive/user_comment_mentions', function (responseData) {
                    responseData = _.filter(responseData, function (item) {
                        return item.name.toLowerCase().indexOf(query.toLowerCase()) >
                            -1
                    });
                    callback.call(this, responseData);
                });
            },
            templates: {
                mentionItemSyntax: _.template('<%= name %>'),
            }
        });

        $('#edit_comment_hi5_modal').on('hidden.bs.modal', function () {
            $('textarea.highfive_edit_comment').mentionsInput('reset', function () {});
        });

    });
</script>