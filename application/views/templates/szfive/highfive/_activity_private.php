<style>
    .heart {
        background: url("<?php echo base_url('assets/images/img/web_heart_animation.png'); ?>");
        background-repeat: no-repeat;
        height: 50px;
        width: 50px;
        cursor: pointer;
        position: absolute;
        background-size: 2900%;
        margin-left: -7px;
        margin-top: -12px;
    }

    .message {
        background: url("<?php echo base_url('assets/images/img/message.png'); ?>");
        background-position: left;
        background-repeat: no-repeat;
        height: 13px;
        width: 15px;
        cursor: pointer;
        position: absolute;
        background-size: contain;
        margin-left: 9px;
        margin-top: 7px;
    }

    .likeType {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        display: block;
        height: 40px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 40px;
        cursor: pointer;
    }

    .likeTypeSmall {
        background-image: url("<?php echo base_url('assets/images/img/like_types.png'); ?>");
        background-size: 100%;
        float: left;
        margin-right: -3px;
        height: 20px;
        image-rendering: crisp-edges;
        line-height: 1;
        width: 20px;
        cursor: pointer;
    }

    .likeIconDefault {
        width: 17px;
        float: left;
        margin-right: 5px;
        background: url("<?php echo base_url('assets/images/img/like.png'); ?>");
        height: 16px;
        background-size: 100%;
    }

    .defaultIcon {
        margin-right: 5px;
    }

    .all-reaction {
        background-position: 0 30px;
        height: 15px;
        width: 15px;
    }

    .mention {
        /* padding-right: 150px !important; */
        font-size: 16px;
        margin-bottom: 12px;
    }

    .post-type {
        width: 115px !important;
        margin: 0 auto;
        display: flex !important;
        justify-content: center;
        align-items: center;
        padding-right: 2px;
        border-left: 1px solid #dcdcdc;
    }

    .bootstrap-select {
        padding-right: 0 !important;
    }

    .dropdown-toggle {
        border: none !important;
    }

    #post_type {
        display: none;
    }

    textarea {
        height: initial !important;
    }

    ` .like-info {
        margin-left: 10px;
    }

    .like-info {
        margin-left: 10px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper" id="szfive-page">
    <?php $sub_header = ['title' => 'SZFive', 'breadcrumbs' => [['title' => 'SZFive', 'link' => 'szfive#']]];?>
    <?php $this->load->view('templates/szfive/_includes/sub_header', $sub_header);?>
    <div class="m-content survey-answers" id="highfive-page">
        <?php $this->load->view('templates/szfive/_includes/nav_bar', $nav_list); ?>
        <?php $tabs = ['tabs' => [['title' => 'Highlights', 'link' => 'szfive', 'active' => ''], ['title' => 'Activity', 'link' => 'szfive/activity_public#', 'active' => 'active'], ['title' => 'Feeling', 'link' => 'szfive/feeling', 'active' => ''], ['title' => 'Comments', 'link' => 'szfive/comments', 'active' => '']]];?>
        <?php $this->load->view('templates/szfive/_includes/user_info_bar', $tabs);?>
        <div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--brand m-tabs-line" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <div class="row" style="padding: 1.7rem 0 1.5rem 0;">
                                <select class="form-control m-bootstrap-select m_selectpicker" name="postType" id="select_post_type">
                                    <option data-icon="fa fa-globe" value="public"> &nbsp;Public</option>
                                    <option data-icon="fa fa-lock" value="private" selected> &nbsp;Private</option>
                                </select>
                            </div>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_recieved_highfive"
                                role="tab">
                                People Who Commends You
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_send_highfive" role="tab">
                                People You Commend
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body" style="padding: 0;">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_tabs_recieved_highfive" role="tabpanel">
                        <div class="m_datatable_recieved_highfive"></div>
                    </div>
                    <div class="tab-pane" id="m_tabs_send_highfive" role="tabpanel">
                        <div class="m_datatable_sent_highfive"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $this->load->view('templates/szfive/highfive/_modals');?>
        </div>
    </div>
</div>
<script>
    var imagePath = baseUrl + '/assets/images/';

    var highFiveDatatable = function () {

        var sentHighFives = function () {

            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: baseUrl + '/szfive/get_sent_highfives',
                            params: {
                                query: {
                                    isPublic: 0,
                                },
                            },
                            map: function (raw) {
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    theme: 'default',
                    scroll: false,
                    footer: false,
                    header: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    placement: ['bottom'],
                    items: {
                        pagination: {
                            pageSizeSelect: [5, 10, 20, 30, 50]
                        },
                    }
                },
                columns: [{
                    width: 400,
                    field: 'senderId',
                    title: 'Name',
                    template: function (value) {
                        var imageClass = "receipient-pic-dt-" + value.hi5recepientId;
                        var empClass = "user-pic-dt-" + value.emp_id;
                        var pic = value.pic ? value.pic : "images/img/sz.png";
                        var comments = (value.totalComment.count != 0) ?
                            '<a onClick="commentHi5(' + value.hiFiveId +
                            ')" class="like-info pull-right like-info' + value.hiFiveId +
                            '">' +
                            value.totalComment.count + ' comments</a>' : '0 comment';
                        var likes_info = (value.likes_info != '') ?
                            '<span style="margin-left: 10px;">' + value.likes_info +
                            '</span>' :
                            '';
                        str = '';
                        str += '<div class="row" style="margin: 0;">';
                        str += '<div class="col-lg-2" style="max-width: fit-content;">';
                        str +=
                            '<div style="margin-bottom: .7rem;white-space: nowrap!important;text-align: center;"><img class="m-widget3__img user-pic-dt-' +
                            value.emp_id + '"   src="' +
                            imagePath + pic + '" onerror="noImageFound(' + "'" + empClass + "'" +
                            ')" alt="" style="width: 3.5rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #fff;height: 3.5rem;">';
                        if (value.moreThanOne == true) {
                            str +=
                                ' <span class="symbol" style="background-color: #00a4b9;">';
                            str +=
                                ' <span style="margin-top: 6px;display: block;color: #fff;font-size: 22px;font-weight: 500;">' +
                                value.noOfRecepients + '</span></span>';
                        } else {
                            str += '<img class="m-widget3__img receipient-pic-dt-' + value.hi5recepientId +
                                '" src="' +
                                value.hi5recepient + '" onerror="noImageFound(' + "'" +
                                imageClass + "'" +
                                ')" alt="" style="width: 3.7rem;height: 3.7rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #ffffff;margin-left: -12px;">';
                        }
                        str += '</div></div>';
                        str += '<div class="col-lg-9">';
                        str += '<a href="' + baseUrl + '/' + value.link +
                            '" class="m-widget3__username" style="font-size: 13px;font-weight: 500;color: #525252;">' +
                            value.user_name +
                            '<span style="font-weight: 400;margin: 0 2px;"> to </span> <span>' +
                            value.header_msg + '</span></a>';
                        str +=
                            '<br><span class="m-widget3__time" style="font-size: .85rem;">' +
                            value.dateAnswered + ', ' + value.dateSubmitted;
                        if (value.isPublic == 1) {
                            str +=
                                '&nbsp;<i class="fa fa-globe" style="font-size: 12px;color: #767e84d6;"></i></span>';
                        } else {
                            str +=
                                '&nbsp;<i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i></span>';
                        }
                        str += '<div class="m-widget3__body">';
                        str +=
                            '<p class="m-widget3__text" style="font-size: 15px; margin-bottom: 0">' +
                            value.message + '</p>';
                        str += '</div>';
                        str += '<div class="col-lg-9">';
                        str += '<div style="margin-top: 15px;"><span id="all-reaction' + value.hiFiveId +
                            '">' + value.reactions +
                            '</span><a data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="'+value.tooltip+'" class="like-info" id="like-info' + value.hiFiveId +
                            '">' + value.likes_info +
                            '</a>';
                        str += '</div>';
                        str +=
                            '<div class="row" style="text-align: center;margin-left: 0px;margin-top: 5px;font-size: 12px;">';
                        if (value.my_reaction == '') {

                            str += '<a class="reaction" id="like' + value.hiFiveId +
                                '" rel="like" style="margin-right: 10px;cursor: pointer !important;">';
                            str += '<i class="likeIconDefault" ></i> Like';
                        } else {
                            str += '<a class="unLike" id="like' + value.hiFiveId +
                                '" rel="unlike" style="margin-right: 10px;cursor: pointer !important;">';
                        }
                        str += value.my_reaction;
                        str += '</a>';
                        str += '<a style="cursor: pointer;" onClick="commentHi5(' + value.hiFiveId +
                            ')"><i class="fa fa-comment" style="color: #afb4bd;font-size: 1rem;"></i>  &nbsp;' +
                            comments + '</a>';
                        if (value.senderId == value.sessionId) {
                            str +=
                                '<a style="margin-left: 10px;cursor: pointer;" onClick="deleteHiFivepost_1(' +
                                value.hiFiveId +
                                ')"><i class="fa fa-trash-o" style="color: #afb4bd;font-size: 1rem;"></i> Delete</a> ';
                        }
                        str += '</div></div></div>';
                        str += '</div>';
                        str += '<div class="row" style="margin: 0;">';
                        str += '<div class="col-lg-2"></div>';
                        str += '</div>';
                        return str;
                    },
                }],
            };
            $('.m_datatable_sent_highfive').mDatatable(options);
        };

        var receivedHighFives = function () {

            var options = {
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            method: 'POST',
                            url: baseUrl + '/szfive/get_received_highfives',
                            params: {
                                query: {
                                    isPublic: 0,
                                },
                            },
                            map: function (raw) {
                                var dataSet = raw;
                                if (typeof raw.data !== 'undefined') {
                                    dataSet = raw.data;
                                }
                                return dataSet;
                            },
                        },
                    },
                    saveState: {
                        cookie: false,
                        webstorage: false
                    },
                    pageSize: 5,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                },
                layout: {
                    theme: 'default',
                    scroll: false,
                    footer: false,
                    header: false
                },
                sortable: true,
                pagination: true,
                toolbar: {
                    placement: ['bottom'],
                    items: {
                        pagination: {
                            pageSizeSelect: [5, 10, 20, 30, 50]
                        },
                    }
                },
                columns: [{
                    field: 'senderId',
                    title: 'Name',
                    template: function (value) {
                        var imageClass = "receipient-pic-dt-" + value.hi5recepientId;
                        var empClass = "user-pic-dt-" + value.emp_id;
                        var pic = value.pic ? value.pic : "images/img/sz.png";
                        var comments = (value.totalComment.count != 0) ?
                            '<a onClick="commentHi5(' + value.hiFiveId +
                            ')" class="like-info pull-right like-info' + value.hiFiveId +
                            '">' +
                            value.totalComment.count + ' comments</a>' : '0 comment';
                        var likes_info = (value.likes_info != '') ?
                            '<span style="margin-left: 10px;">' + value.likes_info +
                            '</span>' :
                            '';
                        str = '';
                        str += '<div class="row" style="margin: 0;">';
                        str += '<div class="col-lg-2" style="max-width: fit-content;">';
                        str +=
                            '<div style="margin-bottom: .7rem;white-space: nowrap!important;text-align: center;"><img class="m-widget3__img user-pic-dt-' +
                            value.emp_id + '"   src="' +
                            imagePath + pic + '" onerror="noImageFound(' + "'" + empClass + "'" +
                            ')" alt="" style="width: 3.5rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #fff;height: 3.5rem;">';
                        if (value.moreThanOne == true) {
                            str +=
                                ' <span class="symbol" style="background-color: #00a4b9;">';
                            str +=
                                ' <span style="margin-top: 6px;display: block;color: #fff;font-size: 22px;font-weight: 500;">' +
                                value.noOfRecepients + '</span></span>';
                        } else {
                            str += '<img class="m-widget3__img receipient-pic-dt-' + value.hi5recepientId +
                                '" src="' +
                                value.hi5recepient + '" onerror="noImageFound(' + "'" +
                                imageClass + "'" +
                                ')" alt="" style="width: 3.7rem;height: 3.7rem;border-radius: 50%;display: inline-block;vertical-align: middle;border: 3px solid #ffffff;margin-left: -12px;">';
                        }
                        str += '</div></div>';
                        str += '<div class="col-lg-9">';
                        str += '<a href="' + baseUrl + '/' + value.link +
                            '" class="m-widget3__username" style="font-size: 13px;font-weight: 500;color: #525252;">' +
                            value.user_name +
                            '<span style="font-weight: 400;margin: 0 2px;"> to </span> <span>' +
                            value.header_msg + '</span></a>';
                        str +=
                            '<br><span class="m-widget3__time" style="font-size: .85rem;">' +
                            value.dateAnswered + ', ' + value.dateSubmitted;
                        if (value.isPublic == 1) {
                            str +=
                                '&nbsp;<i class="fa fa-globe" style="font-size: 12px;color: #767e84d6;"></i></span>';
                        } else {
                            str +=
                                '&nbsp;<i class="fa fa-lock" style="font-size: 12px;color: #767e84d6;"></i></span>';
                        }
                        str += '<div class="m-widget3__body">';
                        str +=
                            '<p class="m-widget3__text" style="font-size: 15px; margin-bottom: 0">' +
                            value.message + '</p>';
                        str += '</div>';
                        str += '<div class="col-lg-9">';
                        str += '<div style="margin-top: 15px;"><span id="all-reaction' + value.hiFiveId +
                            '">' + value.reactions +
                            '</span><a  data-html="true"  data-placement="top" data-toggle="tooltip" href="#" data-original-title="'+value.tooltip+'" class="like-info" id="like-info' + value.hiFiveId +
                            '">' + value.likes_info +
                            '</a>';
                        str += '</div>';
                        str +=
                            '<div class="row" style="text-align: center;margin-left: 0px;margin-top: 5px;font-size: 12px;">';
                        if (value.my_reaction == '') {

                            str += '<a class="reaction" id="like' + value.hiFiveId +
                                '" rel="like" style="margin-right: 10px;">';
                            str += '<i class="likeIconDefault" ></i> Like';
                        } else {
                            str += '<a class="unLike" id="like' + value.hiFiveId +
                                '" rel="unlike" style="margin-right: 10px;">';
                        }
                        str += value.my_reaction;
                        str += '</a>';
                        str += '<a style="cursor: pointer;" onClick="commentHi5(' + value.hiFiveId +
                            ')"><i class="fa fa-comment" style="color: #afb4bd;font-size: 1rem;"></i>  &nbsp;' +
                            comments + '</a>';
                        if (value.senderId == value.sessionId) {
                            str +=
                                '<a style="margin-left: 10px;cursor: pointer;" onClick="deleteHiFivepost_1(' +
                                value.hiFiveId +
                                ')"><i class="fa fa-trash-o" style="color: #afb4bd;font-size: 1rem;"></i> Delete</a> ';
                        }
                        str += '</div></div></div>';
                        str += '</div>';
                        str += '<div class="row" style="margin: 0;">';
                        str += '<div class="col-lg-2"></div>';
                        str += '</div>';
                        return str;
                    },
                }],
            };

            $('.m_datatable_recieved_highfive').mDatatable(options);
        };

        return {
            init: function () {
                receivedHighFives();
                sentHighFives();
            },
        };
    }();

    jQuery(document).ready(function () {
        highFiveDatatable.init();
        $('#select_post_type').selectpicker();
        $('.like-info').tooltip();
    });

    $(function () {
        $('#select_post_type').on('change', function () {
            document.location.href = baseUrl + "/szfive/activity_" + $(this).val();
        });
    });

    function deleteHiFivepost_1(id) {
        swal({
            title: '',
            text: 'Are you sure you want to delete this High Five?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No, cancel'
        }).then(function (result) {
            if (result.value === true) {

                $.ajax({
                    url: baseUrl + "/szfive/delete_hi5_post",
                    type: 'POST',
                    dataType: "json",
                    data: {
                        id: id
                    },
                    success: function (data) {

                        mApp.blockPage({
                            overlayColor: '#000000',
                            type: 'loader',
                            state: 'success',
                            message: 'Please wait...'
                        });

                        setTimeout(function () {
                            mApp.unblockPage();
                            $('#highfive-item' + id).remove();
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "2000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            $('.m_datatable_recieved_highfive').mDatatable('reload');
                            $('.m_datatable_sent_highfive').mDatatable('reload');
                            toastr.success("Successfully removed High Five.");
                        }, 1000);
                    },
                    error: function () {
                        swal({
                            "title": "",
                            "text": "Error",
                            "type": "error",
                            "timer": 2000,
                            "showConfirmButton": false
                        });
                    }
                });
            }
        })
    }
</script>
<?php $this->load->view('templates/szfive/_includes/scripts');?>