<style type="text/css">
.colorgray{
  color:#797c88 !important;
}
.colorblue{
  color:#3297e4 !important;
}
.coloricons{
  color:#4371a2 !important;
}
.checklistcolor{
  background: #97e2af;
}
.colorlightblue{
  background-color:#9cdfda;
}
.foldercolor{
  background: #616892;
}
.colorcreatedby{
  color: #f3d57a;
  font-size: 0.9em;
}
.icon-color{
  color:#f1c483 !important;
}
.modallabel{
  padding: 20px;
  font-size:1.3em;
}
</style> 
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto"> 
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo site_url('process'); ?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url("process"); ?>" class="m-nav__link">
              <span class="m-nav__link-text">Folder</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">  
        <!--begin::Portlet-->
        <div class="m-portlet">
          <div class="m-portlet__head portlet-header-report" style="background-color:#2c2e3eeb">
            <div class="m-portlet__head-caption">   
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon font-white">
                  <i class="flaticon-folder-2"></i>
                </span>
                <a href="<?php echo site_url('process'); ?>" class="m-portlet__head-text font-poppins"  style="color:white !important;font-size:16px">
                  <?php echo $folderinfo->folderName; ?>  
                </a>
                <input type="hidden" id="user_id" value="<?php echo $user; ?>">
                <input type="hidden" id="folID" name="folID" value="<?php echo $folderID ?>">
                <input type="hidden" id="displayprocess_id">
                <input type="hidden" id="displaysubfolder_id">
              </div>
            </div>
          </div>
          <div class="m-portlet__body">
            <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
              <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-subfolders" role="tab" aria-selected="true" onclick="subfolderpermission()" ><i class="la   la-folder-o"></i> Sub folders</a>
              </li>
              <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-processtemp" role="tab" aria-selected="false" onclick="processpermission_access()" ><i class="la  la-file-text-o"></i> Process Templates</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active show fade" id="tab-subfolders" role="tabpanel">

                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 container-forms">
                      <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-8">
                          <div class="m-input-icon m-input-icon--left">
                            <input id="searchSubfolder" type="text" class="form-control m-input" placeholder="Search by publisher name or folder name...">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                              <span><i class="la la-search"></i></span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp;
                        New 
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenu2" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blankroot-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template in subfolder</button>
                        <!-- <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button> -->
                        <hr>
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Sub folder</button>
                      </div>                 
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
                <div class="m-loader m-loader--info loading-proicon load-style" style="display: none;"></div>
                <div id="folrow" class="row">
                </div>
                <div class="text-center">
                  <button class="btn btn-metal" id="folderLoadMore" data-whichfunction='0'>Load More</button>
                </div>
              </div>

              <!-- DISPLAY OF PROCESS TEMPLATES -->
              <div class="tab-pane fade" id="tab-processtemp" role="tabpanel">

                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1 container-forms">
                      <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-8">
                          <div class="m-input-icon m-input-icon--left">
                            <input id="searchProcess" type="text" class="form-control m-input" placeholder="Search by process, folder name...">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                              <span><i class="la la-search"></i></span>
                            </span>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="m-input-icon">
                            <select id="selectedprocessStatus" class="form-control m-input m-input--square">
                              <option value="">All</option>
                              <option value="10">Active</option>
                              <option value="2">Pending</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp;
                        New 
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenu2" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blankroot-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template in subfolder</button>
                        <!-- <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button> -->
                        <hr>
                        <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Sub folder</button>
                      </div>                 
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>

                <div class="m-loader m-loader--info loading-proicon load-style" style="display: none;"></div>
                <div id="prorow" class="row">
                </div>
                <div class="text-center">
                  <button class="btn btn-metal" id="processLoadMore" data-whichfunction='0'>Load More</button>
                </div>
              </div>
            </div>
          </div>
        </div>  
        <!--end::Portlet-->
      </div>
      <!--begin::Modal-->
      <div class="modal fade" id="folder-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="folder_addform" name="folder_addform" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Sub folder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Sub Folder Name:</label>
                  <input type="text" class="form-control" name="subfolderName" id="subfolderName">
                  <span style="font-size:0.9em;color:gray" id="subfoldercharNum"></span>
                  <input type="hidden" id="rootfolderID" name="rootfolderID" value="<?php echo $folderID ?>">
                </div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-accent">Save</button>
              </div>
            </div>
          </form>

        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_foldernameupdate" name="form_foldernameupdate" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Folder Name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Folder Name:</label>
                  <input type="text" class="form-control" id="updatefolderName" name="updatefolderName" placeholder="Enter Folder Name">
                  <span style="font-size:0.9em;color:gray" id="updatefoldercharNum"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-accent">Save Changes</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="updateProcessmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_processnameupdate" name="form_processnameupdate" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Process Name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Process Name:</label>
                  <input type="text" class="form-control" id="updateprocessName" name="updateprocessName" placeholder="Enter Process Name">
                  <span style="font-size:0.9em;color:gray" id="updateprocesscharNum"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-accent">Save Changes</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="blank-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_addsubblanktemplate" name="form_addsubblanktemplate" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Blank Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="recipient-name" class="form-control-label">Template Name:</label>
                  <input type="text" class="form-control" id="subprocessTitle" name="subprocessTitle">
                  <span style="font-size:0.9em;color:gray" id="subprocesscharNum"></span>
                </div>
                <div class="form-group">
                  <label>Choose folder:</label>
                  <select class="form-control m-input" id="subprocessfolder" name="subprocessfolder">
                    <?php
                    foreach ($subfolders as $subfolder)
                    {
                      ?>
                      <option value='<?php echo $subfolder->folder_ID ?>'><?php echo $subfolder->folderName ?></option>
                    <?php } ?>
                  </select>
                  <p class="m-form__help text-center" id="leave_description" style="text-transform: capitalize;"></p>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-accent">Save</button>
              </div>
            </div>
          </form>

        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="blankroot-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <form id="form_addblanktemplate" name="form_addblanktemplate" method="post">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create Blank Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="subformadd2" name="subformadd2" method="post">
                  <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Template Name:</label>
                    <input type="text" class="form-control" id="processTitle" name="processTitle">
                    <span style="font-size:0.9em;color:gray" id="charNum"></span>
                    <input type="hidden" id="rootprocessID" name="rootprocessID" value="<?php echo $folderID ?>">
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-accent">Save</button>
              </div>
            </div>
          </form>

        </div>
      </div>
      <!--end::Modal-->

      <!--begin::Modal-->
      <div class="modal fade" id="assign_membersmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Add Members to <span style="color:#a8e9f1" id="fpc_title"></span>&nbsp;<span id='type-assigned' style="color:#83d686"></span></h5>
              <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <input type="hidden" id="folprotaskcheck_id" name="">
              <input type="hidden" id="assign_type">
            </div>
            <div class="modal-body">
              <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active show" onclick="reinitAssigneeDatatable()" data-toggle="tab" href="#tab-member" role="tab" aria-selected="true"><i class="la la-users"></i>Members</a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-addmember" role="tab" aria-selected="false"><i class="la la-users"></i>Add Members</a>
                </li>
              </ul>
              <div class="tab-content"> 

                <div class="tab-pane fade show active" id="tab-member">
                  <div class="row">
                   <label class="modallabel colorblue">Members</label>
                   <div class="col-12">
                     <div class="form-group m-form__group" style="margin-bottom: 8px;">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search Employee Name" id="assigneeSearch">
                        <div class="input-group-append">
                          <span class="input-group-text">
                            <i class="fa fa-search"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12" id="assigned_memberdisplay">
                  </div>  
                </div>
              </div>
              <div class="tab-pane fade" id="tab-addmember">
                <div class="row topdistance">
                  <div class="col-6">
                    <form id="" class="" name="formupdate" method="post">
                      <label for="recipient-name" class="form-control-label colorblue">Select Members by Team</label>
                      <select id="account" onchange="getaccmembers()" class="selectpicker dropdownwidth" data-live-search="true">
                        <optgroup data-max-options="1">
                          <option disabled selected="">Click to select</option>
                          <?php
                          foreach ($account as $acc)
                          {
                            ?>
                            <option value='<?php echo $acc->acc_id ?>'><?php echo $acc->acc_name; ?></option>
                          <?php } ?>
                        </optgroup>                
                      </select>
                    </form>
                  </div>
                  <div class="col-6">
                    <form id="formupdate" name="formupdate" method="post">
                      <label for="recipient-name" class="form-control-label colorblue">Select Members by Name</label>
                      <select id="member" onchange="getmember_name()" class="selectpicker dropdownwidth" data-live-search="true">
                        <optgroup data-max-options="1">
                          <option disabled selected="">Click to select</option>
                          <?php
                          foreach ($members as $mem)
                          {
                            ?>
                            <option value='<?php echo $mem->emp_id ?>'><?php
                            echo $mem->lname;
                            echo ", &nbsp;";
                            echo $mem->fname;
                            ?></option>
                          <?php } ?>
                        </optgroup>                
                      </select>
                    </form>
                  </div>
                </div>
                <div class='row topdistance'>
                  <label class="modallabel colorblue">Search results</label>
                  <div class="col-12">
                    <table class="table m-table table-hover" id="memberdetails"> 
                      <thead align="center">
                        <tr>
                          <th>Name</th>
                          <th>Team</th>
                          <th>Access</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="myTable" align="center">
                      </tbody>
                    </table>
                    <div class="m-loader m-loader--info spinner-icon load-style" style="display: none;" ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end::Modal-->

  </div>
</div>
</div>
<script type="text/javascript">
  var folder_ids = [];
  var process_ids = [];
  $("#selectedprocessStatus").change(function () {
    resetProcess();
    displayprocesstemp();
  });
  function resetFolder() {
    $("#folderLoadMore").show();
    $("#folderLoadMore").data('whichfunction', 0);
    $("#folrow").html("");
    folder_ids = [];
  }
  function resetProcess() {
    $("#processLoadMore").show();
    $("#processLoadMore").data('whichfunction', 0);
    $("#prorow").html("");
    process_ids = [];
  }
  $('[href="#tab-subfolders"]').click(function () {
    resetFolder();
  });
  $('[href="#tab-processtemp"]').click(function () {
    resetProcess();
  });
  $("#folderLoadMore").click(function () {
    displaysubfolders();
  });
  $("#processLoadMore").click(function () {
    displayprocesstemp();
  });
  $(document).ready(function () {
    subfolderpermission();
    $('#form_addblanktemplate').formValidation({
      message: 'This value is not valid',
      excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  processTitle: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The Process name must be less than 45 characters'
      },
                        // regexp: {
                        //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
                        //   message: 'Letters with number combinations only'
                        // },
                        notEmpty: {
                          message: 'Oops! Process name is required!'
                        },
                      }
                    },
                  }
                }).on('success.form.fv', function (e, data) {
                  e.preventDefault();
                  $('#form_addblanktemplate').formValidation('disableSubmitButtons', true);
                  add_newblanktemplate();
                });
                $('#folder_addform').formValidation({                  
                  message: 'This value is not valid',
                  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  subfolderName: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The folder name must be less than 45 characters'
      },
      notEmpty: {
        message: 'Oops! folder name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#folder_addform').formValidation('disableSubmitButtons', true);
  add_newsubfolder();
});

$('#form_addsubblanktemplate').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  subprocessTitle: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The Process name must be less than 45 characters'
      },
                            // regexp: {
                            //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
                            //   message: 'Letters with number combinations only'
                            // },
                            notEmpty: {
                              message: 'Oops! Process name is required!'
                            },
                          }
                        },
                      }
                    }).on('success.form.fv', function (e, data) {
                      e.preventDefault();
                      $('#form_addsubblanktemplate').formValidation('disableSubmitButtons', true);
                      add_newsubblanktemplate();
                    });

                    $('#form_foldernameupdate').formValidation({
                      message: 'This value is not valid',
                      excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  updatefolderName: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The folder name must be less than 45 characters'
      },
      notEmpty: {
        message: 'Oops! folder name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#form_foldernameupdate').formValidation('disableSubmitButtons', true);
  var folderid = $('#form_foldernameupdate').data('id');
  saveChangesFolder(folderid);
});

$('#form_processnameupdate').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  updateprocessName: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The Process name must be less than 45 characters'
      },
                            // regexp: {
                            //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
                            //   message: 'Letters with number combinations only'
                            // },
                            notEmpty: {
                              message: 'Oops! Process name is required!'
                            },
                          }
                        },
                      }
                    }).on('success.form.fv', function (e, data) {
                      e.preventDefault();
                      $('#form_processnameupdate').formValidation('disableSubmitButtons', true);
                      var processid = $('#form_processnameupdate').data('id');
                      saveChangesProcess(processid);
                    });

                  });


//SEARCH FUNCTIONALITY 

$("#searchSubfolder").on("change", function () {
  var str = $(this).val();
  resetFolder();
  displaysubfolders(str);
});

$("#searchProcess").on("change", function () {
  var str = $(this).val();
  resetProcess();
  displayprocesstemp(str);
});

//END OF SEARCH FUNCTIONALITY
function add_newsubfolder() {
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_subfolder",
    data: {
      folderName: $("#subfolderName").val(),
      folder_ID: $("#rootfolderID").val()
    },
    cache: false,
    success: function (res) {
      resetFolder();
      displaysubfolders();
      $("#folder-modal").modal('hide');
      // window.location.href = '<?php echo site_url('process/subfolder') . '/' . $folderID; ?>';
      swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Added Sub folder',
        showConfirmButton: false,
        timer: 1500
      });
      var result = JSON.parse(res.trim());
    },
    error: function (res) {
      console.log(res);
    }
  });
}

$("#btn-processsave").click(function () {
  if ($.trim($("#processTitle,#rootprocessID").val()) === "") {
    e.preventDefault();
  } else {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_processfolder",
      data: {
        processTitle: $("#processTitle").val(),
        folder_ID: $("#rootprocessID").val()
      },
      cache: false,
      success: function (res) {
        res = JSON.parse(res.trim());
        console.log(res);

        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Process',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function (res) {
        console.log(res);
      }
    });
  }
});

function updateFetch(element) {
  var folderName = $("#updatefolderName").val();

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_update",
    data: {
      folder_ID: $(element).data('id'),
      folderName: folderName
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      $("#updatefolderName").val(data.folderName);
      $("#form_foldernameupdate").data('id', data.folder_ID);
    }
  });
}
function updateProcessFetch(element) {
  var processTitle = $("#updateprocessName").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetchprocess_update",
    data: {
      process_ID: $(element).data('id'),
      processTitle: processTitle
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      $("#updateprocessName").val(data.processTitle);
      $("#form_processnameupdate").data('id', data.process_ID);
    }
  });
}
function saveChangesFolder(folderid) {
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_folder",
    data: {
      folder_ID: folderid,
      folderName: $("#updatefolderName").val()
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'folder successfully updated',
        showConfirmButton: false,
        timer: 1500
      });
      $("#updatemodal").modal('hide');
      resetFolder();
      displaysubfolders();
                    // window.location.href = '<?php echo base_url('process/subfolder') . '/' . $folderID ?>';
                  }
                });
}
function saveChangesProcess(processid) {
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_process",
    data: {
      process_ID: processid,
      processTitle: $("#updateprocessName").val()
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'Process successfully updated',
        showConfirmButton: false,
        timer: 1500
      });
      $("#updateProcessmodal").modal('hide');
      resetProcess();
      displayprocesstemp();
                    // window.location.href = '<?php echo base_url('process/subfolder') . '/' . $folderID ?>';
                  }
                });
}
function deleteFolder(element) {
  swal({
    title: 'Are you sure?',
    text: "All data under this folder will be deleted, too. Except for your subfolders with process template",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function (result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_folder",
        data: {
          folder_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
          window.location.href = '<?php echo base_url('process/subfolder') . '/' . $folderID ?>';
          swal(
            'Deleted!',
            'Folder successfully deleted.',
            'success'
            )
        },
        error: function (res) {
          swal(
            'Oops!',
            'You have subfolders with process template/s to be deleted first! Please Check!',
            'error'
            )
        }
      });
    } else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a folder has been cancelled',
        'error'
        )
    }
  });
  console.log($(element).data('id'));
}

function processpermission_access() {
  var processtemp = "process";
  var displayid = 0;

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_access",
                // data: {
                //   uid:uid,
                // },
                cache: false,
                success: function (res) {
                  var result = JSON.parse(res.trim());
                    //temporary admin
                    if (result.length > 0) {
                      $("#dropdownMenu3").attr("hidden", false);
                      displayid = 1;
                      $("#displayprocess_id").val(displayid);
                      resetProcess();
                      displayprocesstemp();
                    } else {
                      $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>process/fetch_assignment_subfolder",
                        cache: false,
                        success: function (res) {
                          var res = JSON.parse(res.trim());
                          if (res == 0) {
                            $("#dropdownMenu3").remove();
                            displayid = 2;
                            $("#displayprocess_id").val(displayid);
                            resetProcess();
                            displayprocesstemp();
                          } else {
                            displayid = 3;
                            $("#displayprocess_id").val(displayid);
                            resetProcess();
                            displayprocesstemp();
                          }
                        }
                      });

                    }
                  }
                });
}
function subfolderpermission() {
  var folderid = $("#folID").val();
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_access",
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
                    //temporary admin
                    if (result.length > 0) {
                      $("#dropdownMenu3").attr("hidden", false);
                      displayid = 1;
                      $("#displaysubfolder_id").val(displayid);
                      resetFolder();
                      displaysubfolders();
                    } else {
                      $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>process/fetch_assignment_folder_subfolders",
                        data: {
                          folder_ID: <?php echo $folderID ?>
                        },
                        cache: false,
                        success: function (res) {
                          if (res != 0) {
                            displayid = 3;
                            $("#displaysubfolder_id").val(displayid);
                            resetFolder();
                            displaysubfolders();
                          }
                        }
                      });
                    }
                  }
                });
}
function displayprocesstemp(word) {
  // var folderid = $("#folID").val();
  var folderid = <?php echo $folderID ?>;
  var processdisplay_id = $("#displayprocess_id").val();
  var usid = $("#user_id").val();
  var selectedval = $("#selectedprocessStatus").val();


  var whichfunction = $("#processLoadMore").data('whichfunction');
  var link = (processdisplay_id === "1") ? "display_processtemplates_subfolder" : (processdisplay_id === "2") ? "display_processtemplates_subfolder" : (processdisplay_id === "3") ? "display_assignedprocess_insidefolder" : " ";

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/" + link,
    data: {
      word: word,
      folder_ID: folderid,
      statval: selectedval,
      process_ids: process_ids,
      whichfunction: whichfunction
    },
    beforeSend: function () {
      $('.loading-proicon').show();
    },
    cache: false,
    success: function (res) {
      $('.loading-proicon').hide();
      var stringx = "";

      var result = JSON.parse(res.trim());
      if (result.data.length < 6) {
        $("#processLoadMore").hide();
      }
      $("#processLoadMore").data('whichfunction', result.whichfunction);
      $.each(result.data, function (key, item) {
        process_ids.push(item.process.process_ID);
        lastid = item.process.process_ID;
        var processname = item.process.processTitle;
        var substringprocess = "";

        if (processname.length > 22) {
          substringprocess = processname.substring(0, 22) + "...";
        } else {
          substringprocess = processname.substring(0, 22);
        }
        stringx += "<div class='col-lg-4'>";
        stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
        stringx += "<div class='m-portlet__head border-accent rounded colorlightblue'>";

        stringx += "<div class='row' style='margin-top:1em'>";
        stringx += "<div class='m-portlet__head-caption'>";
        stringx += "<div class='m-portlet__head-title'>";
        stringx += "<span class='m-portlet__head-icon'>";
        stringx += "<i class='fa fa-file-text-o colorblue'></i>";
        stringx += "</span>";
        if (item.process.processstatus_ID == 10) {
          stringx += "<a data-toggle='tooltip' data-placement='bottom' title='" + item.process.processTitle + "' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist') ?>" + '/' + item.process.process_ID + "' class='m-portlet__head-text protitle'>" + substringprocess + "</a>";
        } else if (item.process.processstatus_ID == 2) {
          stringx += "<a data-toggle='tooltip' data-placement='bottom' title='" + item.process.processTitle + "' style='display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist_pending') ?>" + '/' + item.process.process_ID + "' class='m-portlet__head-text protitle'>" + substringprocess + "</a>";
        }
        stringx += "</div>";
        stringx += "</div>";
        stringx += "</div>";

        stringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>" + item.process.folderName + "</b></span>";
        stringx += "<input type='hidden' class='proStat' value='" + item.process.processstatus_ID + "'>";
        stringx += "<input type='hidden' class='eachprocessid' value=" + item.process.process_ID + ">";

        stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
        stringx += "<div class='m-portlet__head-tools'>";
        stringx += "<ul class='m-portlet__nav'>";

        if (processdisplay_id == "1" || item.process.processruleid == "1" || item.process.processruleid == "2" || item.process.folderruleid == "1" || item.process.folderruleid == "2" || item.process.createdBy == usid) {
          stringx += "<li class='m-portlet__nav-item'>";
          stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
          stringx += "<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";
          stringx += "<button class='dropdown-item' type='button' data-id='" + item.process.process_ID + "' onclick='updateProcessFetch(this)' data-toggle='modal' data-target='#updateProcessmodal'><i class='flaticon-file'></i>&nbsp;Change Process Name</button>";
          stringx += "<button class='dropdown-item' type='button' data-title='" + item.process.processTitle + "' data-type='process' data-id='" + item.process.process_ID + "' onclick='fetch_IDforAssignment(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'><i class='flaticon-file'>";
          stringx += "</i>&nbsp;Assign this Process</button>";
          stringx += "<button class='dropdown-item' type='button' data-type='process' data-id='" + item.process.process_ID + "' onclick='delete_folprocheck(this)'><i class='flaticon-file'>";
          stringx += "</i>&nbsp;Delete this template</button>";
          if (item.process.processstatus_ID == "2") {
            stringx += "<button class='dropdown-item' type='button' data-type='process' data-id='" + item.process.process_ID + "' onclick='processset_toarchive(this)' data-target='#assign_membersmodal'><i class='flaticon-file'>";
            stringx += "</i>&nbsp;Archive this template</button>";
          }
          stringx += "</div> ";
          stringx += "</li>";

          stringx += "<a href='<?php echo site_url('process/report')?>"+'/'+item.process.process_ID+"' class='m-portlet__nav-link m-portlet__nav-link--icon' data-toggle='tooltip' data-placement='top' title='Report'><i class='la la-area-chart coloricons'></i></a> " ;
          stringx += "</li> &nbsp;|&nbsp;";
        }
       //  else if(item.process.processruleid=="3" || item.process.processruleid=="4" || item.process.folderruleid == "3" || item.process.folderruleid == "4"){
       //   stringx += "<li class='m-portlet__nav-item'>";
       //   stringx += "<a href='#' data-toggle='modal' data-id='" + item.process.process_ID + "' data-processtitle='" + item.process.processTitle + "' onclick='fetch_runcheck_details(this)' data-target='#run_checklist-modal' class='m-portlet__nav-link m-portlet__nav-link--icon'  data-toggle='m-tooltip' data-placement='top' title='Run Another Checklist'><i class='la la-plus-circle coloricons'></i></a>";
       //   stringx += "</li>";
       // }
       if (item.process.processstatus_ID == 10) {
        stringx += " <li class='m-portlet__nav-item'>";
        stringx += " <span class='m-badge m-badge--info m-badge--wide' data-toggle='m-tooltip' data-placement='top' title data-original-title='Click process template to add, modify and run a checklist'>Run a Checklist</span>";
        stringx += "</li> ";
      } else if (item.process.processstatus_ID == 2) {
        stringx += "<li class='m-portlet__nav-item'>";
        stringx += " <span class='m-menu__link-badge'>";
        stringx += " <span class='m-badge m-badge--metal' data-toggle='tooltip' data-placement='top' title='All Checklist'>" + item.statuses.all + "</span>";
        stringx += "</span>";
        stringx += "</li> ";
        stringx += "<li class='m-portlet__nav-item'>";
        stringx += " <span class='m-menu__link-badge'>";
        stringx += "<span class='m-badge m-badge--info' data-toggle='tooltip' data-placement='top' title='Pending Checklist'>" + item.statuses.pending + "</span>";
                            // stringx +="<span class='m-badge m-badge--info' data-toggle='m-tooltip' data-placement='top' title data-original-title='Pending Checklist'>"+item.statuses.pending+"</span>";
                            stringx += "</span>";
                            stringx += "</li>";
                            stringx += "<li class='m-portlet__nav-item'>";
                            stringx += "<span class='m-menu__link-badge'>";
                            stringx += "<span class='m-badge m-badge--danger' data-toggle='tooltip' data-placement='top' title='Overdue Checklist'>" + item.statuses.overdue + "</span>";
                            stringx += "</span>";
                            stringx += "</li>";
                            stringx += "<li class='m-portlet__nav-item'>";
                            stringx += "<span class='m-menu__link-badge'>";
                            stringx += "<span class='m-badge m-badge--success' data-toggle='tooltip' data-placement='top' title='Completed Checklist'>" + item.statuses.completed + "</span>";
                            stringx += " </span>";
                            stringx += "</li>";
                          }
                          stringx += "</ul>";
                          stringx += "</div>";
                          stringx += "</div>";

                          stringx += "</div>";
                          stringx += "</div>";
                          stringx += "</div>";
                        });

$("#prorow").append(stringx);
}
});
}

function displaysubfolders(word) {
  var folderid = $("#folID").val();
  var subfolderid = $("#displaysubfolder_id").val();
   var whichfunction = $("#folderLoadMore").data('whichfunction');

  var link = (subfolderid === "3") ? "display_assigned_subfolders" : (subfolderid === "1") ? "displaysubfolders" : " ";

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/" + link,
    data: {
      folder_ID: folderid,
      word: word,
      folder_ids: folder_ids,
      whichfunction: whichfunction
    },
    beforeSend: function () {
      $('.loading-proicon').show();
    },
    cache: false,
    success: function (res) {
      $('.loading-proicon').hide();
      var lastid = 0;
      var stringx = "";
      var result = JSON.parse(res.trim());
      if (result.data.length < 6) {
        $("#folderLoadMore").hide();
      }
      $("#folderLoadMore").data('whichfunction', result.whichfunction);
      $.each(result.data, function (key, item) {
        folder_ids.push(item.folder_ID);
        lastid = item.folder_ID;
        var foldername = item.folderName;
        var substringname = "";

        if (foldername.length > 22) {
          substringname = foldername.substring(0, 22) + "...";
        } else {
          substringname = foldername.substring(0, 22);
        }
        stringx += "<div class='col-lg-4 foldercontent' id='" + item.status_ID + "'>";
        stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
        stringx += "<div class='m-portlet__head border-accent rounded foldercolor'>";

        stringx += "<div class='row' style='margin-top:1em'>";
        stringx += "<div class='m-portlet__head-caption'>";
        stringx += "<div class='m-portlet__head-title'>";
        stringx += "<span class='m-portlet__head-icon'>";
        stringx += "<i class='fa fa-folder icon-color' id='" + item.folder_ID + "'></i>";
        stringx += "</span>";
        stringx += "<a data-toggle='tooltip' data-placement='bottom' title='" + item.folderName + "' style='color:white;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/subfolder') ?>" + '/' + item.folder_ID + "' class='foldname'>" + substringname + "</a>";
        stringx += "<input type='hidden' value='" + item.folder_ID + "' id='eachfolderid'>";
        stringx += "<input type='hidden' class='folStat' value='" + item.status_ID + "'";
        stringx += "<input type='hidden' class='fetchval' id='fetchval'>";
        stringx += "</div>";
        stringx += "</div>";
        stringx += "</div>";
        stringx += "&nbsp;&nbsp;&nbsp;&nbsp;<span class='colorcreatedby createdby'>" + item.fname + "&nbsp;" + item.lname + "</span>";

        stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
        stringx += "<div class='m-portlet__head-tools'>";
        stringx += "<ul class='m-portlet__nav'>";

                        // if admin OR can edit, view and run OR can edit, view own and run
                        if (subfolderid == "1" || item.folassign == "1" || item.folassign == "2") {
                          stringx += "<li class='m-portlet__nav-item'>";
                          stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
                          stringx += "<i class='la la-cog'></i></a>";
                          stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
                          stringx += "<button class='dropdown-item' type='button' data-id='" + item.folder_ID + "' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
                          stringx += "<i class='flaticon-file'></i>&nbsp;Change folder name</button>";

                          stringx += "<button class='dropdown-item' type='button' data-id=" + item.folder_ID + " onclick='folderset_toarchive(this)'>";
                          stringx += "<i class='la la-archive'></i>";
                          stringx += "&nbsp;Archive this folder</button>";

                          stringx += "<button class='dropdown-item' type='button' data-type='folder' data-id='"+item.folder_ID+"' onclick='delete_folprocheck(this)'>";
                          stringx += "<i class='la la-trash'></i>&nbsp;Delete this subfolder</button>";

                          stringx += "</div>";
                          stringx += "</li>";

                          stringx += "<li class='m-portlet__nav-item' data-toggle='modal'  data-title='" + item.folderName + "' data-target='#assign_membersmodal' data-type='folder' data-id='" + item.folder_ID + "' onclick='fetch_IDforAssignment(this),initAssignee(this)' data-toggle='m-tooltip' title='Assign members' data-original-title='Tooltip title'>";
                          stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
                          stringx += "<i class='la la-users'></i>";
                          stringx += "</a>";
                          stringx += "</li>";

                          stringx += "<li class='m-portlet__nav-item'>";
                          stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' onclick='getfolder_nameid(this)' data-toggle='modal' data-target='#blankroot-modal' data-folname='" + item.folderName + "' data-id='" + item.folder_ID + "'><i class='la la-plus-circle'></i></a> ";
                          stringx += "</li>";
                        }

                        stringx += "<li class='m-portlet__nav-item'>";
                        stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle'></i></a> ";
                        stringx += "</li>";

                        stringx += "</ul>";
                        stringx += "</div>";
                        stringx += "</div>";

                        stringx += "</div>";
                        stringx += "</div>";
                        stringx += "</div>";
                      });
$("#folrow").append(stringx);
}
});
}

function fetch_IDforAssignment(element) {
  var idforassignment = $(element).data('id');
  var typename = $(element).data('type');
  var name = $(element).data('title');

  $("#folprotaskcheck_id").val(idforassignment);
  $("#assign_type").val(typename);
  $("#fpc_title").text(name);
  $("#type-assigned").text(typename);
}

function getaccmembers() {
  var accountid = $("#account").val();
  var assigntype = $("#assign_type").val();
  var folprotaskcheckID = $("#folprotaskcheck_id").val();
  var accountname = $("#account :selected").text();

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_allemployeeact",
    data: {
      acc_id: accountid
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      var stringx = "";
      $.each(result, function (key, item) {
        var applicantid = item.apid;
        var userid = item.uid;
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/fetch_empdetails",
          data: {
            apid: applicantid,
            fptcID: folprotaskcheckID,
            assign_type: assigntype,
            user_ID: userid
          },
          beforeSend: function () {
            $('.spinner-icon').show();
          },
          cache: false,
          success: function (res) {
            $('.spinner-icon').remove();
            var rr = JSON.parse(res.trim());
            console.log("READ RESULT:" + res);
            $.each(rr, function (key, data) {
              stringx += "<tr>";
              stringx += "<td>" + data.fname + " " + data.lname + "</td>";
              stringx += "<td>" + accountname + "</td>";
              stringx += "<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";

              if (assigntype == 'folder' || assigntype == 'process') {
                stringx += "<option value='8'>Can View All</option>";
                stringx += "<option value='7'>Can View Own</option>";
                stringx += "<option value='4'>Can View Own and Run</option>";
                stringx += "<option value='3'>Can View and Run</option>";
                stringx += "<option value='1'>Can Edit, View and Run</option>";
                stringx += "<option value='2'>Can Edit, View Own and Run</option>";
              } else {
                stringx += "<option value='5'>Can Answer</option>";
                stringx += "<option value='6'>Can Edit and Answer</option>";
              }
              stringx += "</optgroup></select></td>";
              stringx += "<td><button onclick='addmember_assignment(this)' data-reload='1' data-toggle='m-tooltip' data-id='" + data.apid + "' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
              stringx += "</tr>";
            });
            $("#memberdetails tbody").html(stringx);
          }
        });
      });
    }
  });
}

function getmember_name() {
  var empid = $("#member").val();
  var empname = $("#member :selected").text();
  var assigntype = $("#assign_type").val();
  var folprotaskcheckID = $("#folprotaskcheck_id").val();

  console.log(assigntype);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_oneemployee",
    data: {
      emp_id: empid,
      fptcID: folprotaskcheckID,
      assign_type: assigntype
    },
    beforeSend: function () {
      $('.spinner-icon').show();
    },
    cache: false,
    success: function (res) {
      var stringx = "";
      var result = JSON.parse(res.trim());
      $(".spinner-icon").remove();
      if (res != 0) {
        $.each(result, function (key, data) {
          stringx += "<tr>";
          stringx += "<td>" + data.fname + " " + data.lname + "</td>";
          stringx += "<td>" + data.acc_name + "</td>";
          stringx += "<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";

          if (assigntype == "folder" || assigntype == "process") {
            stringx += "<option value='8'>Can View All</option>";
            stringx += "<option value='7'>Can View Own</option>";
            stringx += "<option value='4'>Can View Own and Run</option>";
            stringx += "<option value='3'>Can View and Run</option>";
            stringx += "<option value='1'>Can Edit, View and Run</option>";
            stringx += "<option value='2'>Can Edit, View Own and Run</option>";
          } else {
            stringx += "<option value='5'>Can Answer</option>";
            stringx += "<option value='6'>Can Edit and Answer</option>";
          }
          stringx += "</optgroup></select></td>";
          stringx += "<td><button onclick='addmember_assignment(this)' data-reload='2' data-toggle='m-tooltip' data-id='" + data.apid + "' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
          stringx += "</tr>";
        });
      } else {
        stringx = " ";
      }
      $("#memberdetails tbody").html(stringx);
    }
  });
}

function addmember_assignment(element) {
  var apid = $(element).data('id');
  var reload = $(element).data('reload');
  var fptc_id = $("#folprotaskcheck_id").val();
  var assigntype = $("#assign_type").val();
  var assignrule = $("#assignmentRule :selected").val();
  var fpc_title = $("#fpc_title").text();

  swal({
    title: 'Are you sure?',
    text: "This member will be assigned to this folder/process template/checklist",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes'
  }).then(function (result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/add_assignment",
        data: {
          type: assigntype,
          assignmentRule_ID: assignrule,
          applicantid: apid,
          folprotaskcheck_id: fptc_id,
          title: fpc_title
        },
        cache: false,
        success: function (res) {
          var resObj = $.parseJSON(res.trim())
          console.log(resObj.notifid);
          systemNotification(resObj.notifid.notif_id);
          if (reload == 1) {
            getaccmembers();
          } else if (reload == 2) {
            getmember_name();
          }
          addmember_assignment();
          swal(
            'Added!',
            'User successfully assigned.',
            'success'
            )
          $("#assign_membersmodal").modal('hide');
        },
        error: function (res) {
          swal(
            'Oops!',
            'Cannot assign',
            'error'
            )
        }
      });
    } else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Assigning user has been cancelled',
        'error'
        )
    }
  });
}
function access_permissionChange(element){
  var assignmentid=$(element).data('assignid');
  var changepermission=$(element).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_assignmentpermission",
    data: {
      assid:assignmentid,
      changepermission:changepermission
    },
    cache: false,
    success: function (res) {
      reinitAssigneeDatatable();
      swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Changed Acces Permission  ',
        showConfirmButton: false,
        timer: 1500
      });
      var result = JSON.parse(res.trim());
    }
  });
}

function removemember_assignment(element) {
  swal({
    title: 'Are you sure?',
    text: "This member will be removed from accessing this file.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function (result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_assignment",
        data: {
          assign_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
          reinitAssigneeDatatable();
          swal(
            'Deleted!',
            'Member successfully Removed.',
            'success'
            )
        },
        error: function (res) {
          swal(
            'Oops!',
            'Something is wrong with your code!',
            'error'
            )
        }
      });
    } else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Removing member has been cancelled',
        'error'
        )
    }
  });
}
function resetform() {
  $("#account").prop("selectedIndex", 0).change();
  $("#member").prop("selectedIndex", 0).change();
  $("#memberdetails tbody").html("");
  $("#added_members tbody").html("");
}
function processset_toarchive(element) {
  var processid = $(element).data('id');
  var status = '15';

  swal({
    title: 'Are you sure?',
    text: "All Checklists under this process template will be unaccessible.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, set to archive!'
  }).then(function (result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/updateprocess_archive",
        data: {
          process_ID: processid,
          status_ID: status
        },
        cache: false,
        success: function (res) {
          var result = JSON.parse(res.trim());
          swal({
            position: 'center',
            type: 'success',
            title: 'Archived the Process Template',
            showConfirmButton: false,
            timer: 1500
          });
          resetProcess();
          displayprocesstemp();
        }
      });
    } else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Setting Process Template to archive has been cancelled',
        'error'
        )
    }
  });
}

function folderset_toarchive(element) {
  var selected_folid = $(element).data('id');
  var status = '15';
  swal({
    title: 'Are you sure?',
    text: "All Process templates and checklists will be keep to archive section and cannot be used for the meantime.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, set to archive!'
  }).then(function (result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/updatefolder_archive",
        data: {
          folder_ID: selected_folid,
          status_ID: status
        },
        cache: false,
        success: function (res) {
          var result = JSON.parse(res.trim());
          swal({
            position: 'center',
            type: 'success',
            title: 'Archived the folder',
            showConfirmButton: false,
            timer: 1500
          });
          resetFolder();
          displaysubfolders();
          resetProcess();
          displayprocesstemp();
        }
      });
    } else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Setting folder to archive has been cancelled',
        'error'
        )
    }
  });
}
function add_newblanktemplate() {
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_processfolder",
    data: {
      processTitle: $("#processTitle").val(),
      folder_ID: $("#rootprocessID").val()
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      resetProcess();
      displayprocesstemp();
      $('#blankroot-modal').modal('hide');
      swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Added Process',
        showConfirmButton: false,
        timer: 1500
      });
    },
    error: function (res) {
      console.log(res);
    }
  });
}

function add_newsubblanktemplate() {
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_processfolder",
    data: {
      processTitle: $("#subprocessTitle").val(),
      folder_ID: $("#subprocessfolder").val()
    },
    cache: false,
    success: function (res) {
     resetFolder();
     displaysubfolders();
     resetProcess();
     displayprocesstemp();
     swal({
      position: 'center',
      type: 'success',
      title: 'Successfully Added Process',
      showConfirmButton: false,
      timer: 1500
    });
     var result = JSON.parse(res.trim());
   },
   error: function (res) {
    console.log(res);
  }
});
}


function delete_folprocheck(element){
  var type_fpc=$(element).data('type');
  swal({
    title: 'Are you sure?',
    text: "All data under this "+type_fpc+" will be deleted, too.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_folder_process_checklist",
        data: {
          all_ID: $(element).data('id'),
          type: $(element).data('type')
        },
        cache: false,
        success: function () {
          swal(
            'Deleted!',
            type_fpc+' successfully deleted.',
            'success'
            ) 
          if(type_fpc=="folder"){
            resetFolder();
            displaysubfolders();
          }else{
            resetProcess();
            displayprocesstemp();
          }
        },
        error: function (res){
          swal(
            'Oops!',
            'You cant delete!',
            'error'
            )
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a '+type_fpc+' has been cancelled',
        'error'
        )
    }
  });
  console.log($(element).data('id'));
}




//MODAL DISMISS AND RESET
$('#blankroot-modal').on('hidden.bs.modal', function () {
  formReset('#form_addblanktemplate');
  $('#charNum').text("");
  $('#processTitle').val("");
});

$('#blank-modal').on('hidden.bs.modal', function () {
  formReset('#form_addsubblanktemplate');
  $('#subprocesscharNum').text("");
  $('#subprocessTitle').val("");
});

$('#folder-modal').on('hidden.bs.modal', function () {
  formReset('#folder_addform');
  $('#subfoldercharNum').text("");
  $('#subfolderName').val("");
});

$('#updatemodal').on('hidden.bs.modal', function () {
  formReset('#form_foldernameupdate');
  $('#updatefoldercharNum').text("");
  $('#updatefolderName').val("");
});

$('#updateProcessmodal').on('hidden.bs.modal', function () {
  formReset('#form_processnameupdate');
  $('#updateprocesscharNum').text("");
  $('#updateprocessName').val("");
});

//CHARACTERS COUNTER
$('#processTitle').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#charNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#charNum').text(char+'/45 characters remaining');
  }
});

$('#subprocessTitle').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#subprocesscharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#subprocesscharNum').text(char+'/45 characters remaining');
  }
});

$('#subfolderName').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#subfoldercharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#subfoldercharNum').text(char+'/45 characters remaining');
  }
});

$('#updatefolderName').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#updatefoldercharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#updatefoldercharNum').text(char+'/45 characters remaining');
  }
});

$('#updatefolderName').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#updatefoldercharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#updatefoldercharNum').text(char+'/45 characters remaining');
  }
});

$('#updateprocessName').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#updateprocesscharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#updateprocesscharNum').text(char+'/45 characters remaining');
  }
});

//DATATABLE
function reinitAssigneeDatatable(){    
  $('#assigned_memberdisplay').mDatatable('destroy');
  assigneeDatatable.init($('#assigneeSearch').val(), $("#folprotaskcheck_id").val(),$('#assign_membersmodal').data('type'));
}
$('#assigneeSearch').on('keyup', function(){
  reinitAssigneeDatatable();
})
function initAssignee(element){
  console.log("init assignee");
  $('#assigned_memberdisplay').mDatatable('destroy');
  $('#assign_membersmodal').data('type', $(element).data('type'));
  assigneeDatatable.init($('#assigneeSearch').val(), $("#folprotaskcheck_id").val(),$(element).data('type'));
}
function selectChange(element){
  console.log($(element).val())
  console.log( $(element).data('assignid'));
}
var assigneeDatatable = function () {
  var assignee = function (searchVal, folProcIdVal,typeval) {
    var options = {
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'POST',
            url: baseUrl + "/process/list_assignee_datatable",
            params: {
              query: {
                folProcId: folProcIdVal,
                type:typeval,
                assigneeSearch: searchVal
              },
            },
          }
        },
        saveState: {
          cookie: false,
          webstorage: false
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },
      layout: {
theme: 'default', // datatable theme
class: '', // custom wrapper class
scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
height: 550, // datatable's body's fixed height
footer: false // display/hide footer
},
sortable: true,
pagination: true,
toolbar: {
// toolbar placement can be at top or bottom or both top and bottom repeated
placement: ['bottom'],

// toolbar items
items: {
// pagination
pagination: {
// page size select
pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
},
}
},
search: {
  input: $('#assigneeSearch'),
},
rows: {
  afterTemplate: function (row, data, index) {},
},
// columns definition
columns: [{
  field: "fname",
  title: "Employee Name",
  width: 180,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = row.fname + " " + row.lname;
    return html;
  },
},{
  field: "acc_name",
  title: "Team",
  width: 160,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = row.acc_name;
    return html;
  },
},{
  field: "lname",
  title: "Access",
  width: 220,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var rule1 = '';
    var rule2 = '';
    var rule3 = '';
    var rule4 = '';
    var rule5 = '';
    var rule6 = '';
    var rule7 = '';
    var rule8 = '';
    if(row.assignmentRule_ID == 1){
      rule1 = "selected";
    }else if(row.assignmentRule_ID == 2){
      rule2 = "selected";
    }else if(row.assignmentRule_ID == 3){
      rule3 = "selected";
    }else if(row.assignmentRule_ID == 4){
      rule4 = "selected";
    }
    else if(row.assignmentRule_ID == 5){
      rule5 = "selected";
    }
    else if(row.assignmentRule_ID == 6){
      rule6 = "selected";
    }
    else if(row.assignmentRule_ID == 7){
      rule7 = "selected";
    }
    else if(row.assignmentRule_ID == 8){
      rule8 = "selected";
    }
    var html='';
    if(row.type=="folder" || row.type=="process"){
     html ='<select onchange="access_permissionChange(this)" data-assignid="'+row.assign_ID+'" class="form-control m-input m-input--square">'+
     '<option value="1" '+rule1+'>Can Edit, View and Run</option>'+
     '<option value="2" '+rule2+'>Can Edit, View Own and Run</option>'+
     '<option value="3" '+rule3+'>Can View and Run</option>'+
     '<option value="4" '+rule4+'>Can View Own and Run</option>'+
     '<option value="7" '+rule7+'>Can View Own</option>'+
     '<option value="8" '+rule8+'>Can View All</option>'+
     '</select>';
   }else if(row.type=="checklist" || row.type=="task"){
     html ='<select onchange="access_permissionChange(this)" data-assignid="'+row.assign_ID+'" class="form-control m-input m-input--square">'+
     '<option value="5" '+rule5+'>Can Answer</option>'+
     '<option value="6" '+rule6+'>Can Edit and Answer</option>'+
     '</select>';
   }
   return html;
 },
},{
  field: "assign_ID",
  title: "Action",
  width: 90,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = '<button onclick="removemember_assignment(this)" data-toggle="m-tooltip" data-id="'+row.assign_ID+'" title="Remove this member" data-original-title="Tooltip title" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="fa fa-remove"></i></button>';
    return html;
  },
}],
};
var datatable = $('#assigned_memberdisplay').mDatatable(options);
};
return {
  init: function (searchVal, folProcIdVal,typeval) {
    assignee(searchVal, folProcIdVal,typeval);
  }
};
}();
</script>