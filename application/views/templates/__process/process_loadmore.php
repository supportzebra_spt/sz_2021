<?php if(!empty($processes)){ foreach ($processes as $pr) {?>
  <!--begin::Portlet-->
  <div class="col-lg-4 processcontent pr<?php echo $pr->processstatus_ID;?>">
    <?php 
    $countall=0;
    $countpen=0;
    $countoverdue=0;
    $countcomp=0;
    foreach ($checklists as $chs) {
      if($pr->process_ID==$chs->process_ID){
        $countall++;
        if($chs->status_ID=="2"){
          $countpen++;
        }
        if($chs->status_ID=="15"){
          $countoverdue++;
        }
        if($chs->status_ID=="3"){
         $countcomp++;
       }
     }
   }
   ?>  
   <div class="m-portlet m-portlet--collapsed m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_7">
    <div class="m-portlet__head border-accent rounded colorlightblue">
      <div class="row" style="margin-top:1em">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon">
              <i class="fa fa-file-text-o colorblue"></i>
            </span> 
            <?php if($pr->processstatus_ID=="10"){?>
              <a style="display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;" href="<?php echo base_url('process/checklist').'/'.$pr->process_ID;?>" class="m-portlet__head-text protitle">
                <?php echo $pr->processTitle?>
              </a>
            <?php } else if($pr->processstatus_ID=="2"){?>
             <a style="display:block;text-overflow: ellipsis;width: 240px;overflow: hidden; white-space: nowrap;" href="<?php echo base_url('process/checklist_pending').'/'.$pr->process_ID?>" class="m-portlet__head-text protitle">
              <?php echo $pr->processTitle?>
            </a>
          <?php }?>
        </div>          
      </div>
    </div>
    <span style="font-size: 0.9em;margin-left:1em">
      <?php 
      $datetime=$pr->dateTimeCreated;
      $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
      echo sprintf($date->format("M j, Y"))."&nbsp; at &nbsp;";
      echo sprintf($date->format("g:ia"));
      ?>
      <br>
    </span>
    <span class="folname" style="color:#088594;font-size: 0.9em;margin-left:1em"><b><?php echo $pr->folderName?></b></span>
    <input type="hidden" class="proStat" value="<?php echo $pr->processstatus_ID;?>">
    <input type="hidden" class="eachprocessid" value="<?php echo $pr->process_ID;?>">
    <div class="row" style="margin-top:1em;margin-bottom: 1em;">
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav" >
          <li class="m-portlet__nav-item">
           <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon" id="dropdownOption" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-cog coloricons"></i></a>
           <div class="dropdown-menu" aria-labelledby="dropdownOption" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
            <button class="dropdown-item" type="button" data-id="<?php echo $pr->process_ID; ?>" onclick="updateProcessFetch(this)" data-toggle="modal" data-target="#updateProcessmodal"><i class="flaticon-file"></i>&nbsp;Edit Process Name</button>
            <button class="dropdown-item" type="button"><i class="flaticon-file">
            </i>&nbsp;Assign this Process</button>
          </div> 
        </li>
        <li class="m-portlet__nav-item">
          <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-info-circle coloricons"></i></a>  
        </li>
        <li class="m-portlet__nav-item">
          <a href="#" data-portlet-tool="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="fa fa-trash-o coloricons"></i></a>    
        </li> &nbsp;|&nbsp;
        <?php if($pr->processstatus_ID=="10"){?>
         <li class="m-portlet__nav-item">
          <span class="m-badge m-badge--info m-badge--wide" data-toggle="m-tooltip" data-placement="top" title data-original-title="Click <?php echo $pr->processTitle;?> to add, modify and run a checklist">Run a Checklist</span>
        </li> 
      <?php }else if($pr->processstatus_ID=="2"){?>         
        <li class="m-portlet__nav-item">
          <span class="m-menu__link-badge">
            <span class="m-badge m-badge--metal" data-toggle="m-tooltip" data-placement="top" title data-original-title="All Checklist"><?php echo $countall;?></span>
          </span>   
        </li>  
        <li class="m-portlet__nav-item">
          <span class="m-menu__link-badge">
            <span class="m-badge m-badge--info" data-toggle="m-tooltip" data-placement="top" title data-original-title="Pending Checklist"><?php echo $countpen?></span>
          </span>   
        </li>
        <li class="m-portlet__nav-item">
          <span class="m-menu__link-badge">
            <span class="m-badge m-badge--danger" data-toggle="m-tooltip" data-placement="top" title data-original-title="Overdue Checklist"><?php echo $countoverdue?></span>
          </span>   
        </li>
        <li class="m-portlet__nav-item">
          <span class="m-menu__link-badge">
            <span class="m-badge m-badge--success" data-toggle="m-tooltip" data-placement="top" title data-original-title="Completed Checklist"><?php echo $countcomp?></span>
          </span>   
        </li>
      <?php }?>
    </ul>
  </div>
</div>
</div>
</div>
</div>
<?php } ?>
<!--end::Portlet-->
<div class="processload-more col-lg-12" processlastID="<?php echo $pr->process_ID;?>" style="display: none;">
  <h5 style="text-align: center;"> <div class="m-loader m-loader--info" style="width: 30px; display: inline-block;"></div> Loading more process templates...</h5>
</div>
<?php } ?>
<div class="processload-more col-lg-12" processlastID="0">
</div>

