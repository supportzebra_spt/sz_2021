<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
<style type="text/css">
.tabtopcolor{
  background-color: #3d3e4d !important;
}
.icon-color{
  color:#f1c483 !important; 
}
.dropdownstyle{
  position: absolute; 
  transform: translate3d(0px, 37px, 0px); 
  top: 0px; 
  left: 0px; 
  will-change: transform;
}
.foldercolor{
  background-color:#4d5167;
  /*#585858;*/
  border-width: 15px
}
.scrollstyle{
  overflow:hidden; 
  height: 200px;
}
.notestyle{
  font-size:0.9em;
  color:#4aab69;
}
.insidefolder{
  background:#e2ebec;
  margin-top: 3em;
}
.colorblue{
  color:#3297e4 !important;
}
.colorlightblue{
  background-color:#9cdfda;
}
.colorwhitesize{
  color:white !important;
  font-size:0.9em;
}
.colorwhiteicons{
  color:white;
}
.coloricons{
  color:#4371a2 !important;
}
.coloricons-checklist{
  color:#3c803f !important;
}
.modallabel{
  padding: 20px;
  font-size:1.3em;
}
.dropdownwidth{
  width:100% !important;
}
.topdistance{
  margin-top: 50px;
}
.load-style{
  padding:20px;
}
div.container-forms{
  background: #f3f0f0 !important;
  padding: 10px !important;
}
.checklistcolor{
  background: #97e2af;
}
.bottom-icons{
  color: #3c803f !important;
}
.pro-name{
  color:#416d90;
  font-size: 0.9em;
  margin-left:1em;
  font-weight: bold;
}

</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>       
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="#" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url("process"); ?>" class="m-nav__link">
              <span class="m-nav__link-text">Folder</span>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="#" class="m-nav__link">
              <span class="m-nav__link-text">Process</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--tabs">
          <div class="m-portlet__head tabtopcolor">
            <div class="m-portlet__head-tools">
              <input type="hidden" id="user_id" value="<?php echo $user;?>">
              <input type="hidden" id="display_id">
              <input type="hidden" id="displayprocess_id">
              <input type="hidden" id="displaychecklist_id">
              <?php foreach($user_info as $ui) {?>
                <input type="hidden" class="form-control" id="addchecklist_defaultName" value="<?php echo $ui->fname;echo "&nbsp;";echo $ui->lname;echo "'s";echo "&nbsp;" ;echo date("h:i a");echo "&nbsp;";echo "checklist"?>">
              <?php }?>
              <ul class="nav nav-tabs m-tabs-line m-tabs-line--accent m-tabs-line--2x" role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active folderTrigger" data-toggle="tab" href="#foldertab" onclick="folderpermission_access()" role="tab">
                    <i class="fa fa-folder"></i>Folders</a>
                  </li>
                  <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link processTrigger" data-toggle="tab" href="#processtab" onclick="processpermission_access()" role="tab">
                      <i class="fa fa-file-text-o"></i>Process Templates</a>
                    </li>
                    <li class="nav-item m-tabs__item">
                      <a class="nav-link m-tabs__link checklistTrigger" data-toggle="tab" href="#checklisttab" onclick="checklistpermission_access()" role="tab">
                        <i class="fa fa-check-square-o"></i>Checklist</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="m-portlet__body">                   
                  <div class="tab-content">
                    <div class="tab-pane fade show active col-md-12" id="foldertab" role="tabpanel">
                      <!--Begin- Search and filter -->
                      <div class="m-form m-form--label-align-right m--margin-bottom-30">
                        <div class="row align-items-center">
                          <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                              <div class="col-md-8 container-forms">
                                <div class="m-input-icon m-input-icon--left">
                                  <input type="text" class="form-control m-input " id="searchString" placeholder="Search by publisher and folder name..." >
                                  <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <!--  <button class="btn btn-info"><i class="fa fa-archive"></i>&nbsp;Archived</button> -->
                            <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp; New </button>
                            <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu2" x-placement="bottom-start">
                              <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"  id="blankTemplate2" style="display:none"><i class="flaticon-file" ></i>&nbsp;New Blank Template</button>
                              <!-- <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button> -->
                              <hr>
                              <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Folder</button>
                            </div>                 
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                          </div>
                        </div>
                      </div>
                      <!--end: Search and Filter -->
                      <!--start folder dispaly-->
                      <div class="m-loader m-loader--info loading-icon load-style"></div>
                      <div id="folrow" class="row">
                      </div>
                      <div class="text-center">
                        <button class="btn btn-metal" id="folderLoadMore" data-whichfunction='0'>Load More</button>
                      </div>
                      <!--End folder display-->
                    </div>
                    <div>             
                    </div>
                    <div class="tab-pane fade col-md-12" id="processtab" role="tabpanel">
                      <div class="m-form m-form--label-align-right m--margin-bottom-30">
                        <div class="row align-items-center">
                          <div class="col-xl-8 order-2 order-xl-1 container-forms">
                            <div class="form-group m-form__group row align-items-center">
                              <div class="col-md-8">
                                <div class="m-input-icon m-input-icon--left">
                                  <input id="searchProcess" type="text" class="form-control m-input" placeholder="Search by process and folder name...">
                                  <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                  </span>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="m-input-icon">
                                  <select id="selectedprocessStatus" class="form-control m-input m-input--square">
                                    <option value="">All</option>
                                    <option value="10">Active</option>
                                    <option value="2">Pending</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                            <!--          <button class="btn btn-info"><i class="fa fa-archive"></i>&nbsp;Archived</button> -->
                            <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i>&nbsp;
                            New</button>
                            <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu3" x-placement="bottom-start">
                              <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"  style="display:none" id="blankTemplate3"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                              <!-- <button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button>-->
                              <hr>
                              <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Folder</button>
                            </div>                 
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                          </div>
                        </div>
                      </div>
                      <!--end: Search Form -->
                      <div class="m-loader m-loader--info loading-proicon load-style"></div>
                      <div id="prorow" class="row">
                      </div>
                      <div class="text-center">
                        <button class="btn btn-metal" id="processLoadMore" data-whichfunction='0'>Load More</button>
                      </div>
                    </div>
                    <!--    CHECKLIST DISPLAY -->
                    <div class="tab-pane fade col-md-12" id="checklisttab" role="tabpanel">
                      <div class="m-form m-form--label-align-right m--margin-bottom-30">
                        <div class="row align-items-center">
                          <!-- SEARCH SECTION -->
                          <div class="col-xl-8 order-2 order-xl-1 container-forms">
                            <div class="form-group m-form__group row align-items-center">
                              <div class="col-md-8">
                                <div class="m-input-icon m-input-icon--left">
                                  <input type="text" class="form-control m-input " id="searchChecklist" placeholder="Search by checklist name..." >
                                  <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                  </span>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="m-input-icon">
                                  <select id="selectedchecklistStatus" class="form-control m-input m-input--square">
                                    <option value="">All</option>
                                    <option value="3">Completed</option>
                                    <option value="16">Overdue</option>
                                    <option value="2">Pending</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- TAB CONTENT -->
                      <div class="m-loader m-loader--info loading-icon-check load-style"></div>
                      <div id="checkrow" class="row">
                      </div>
                      <div class="text-center">
                        <button class="btn btn-metal" id="checklistLoadMore"  data-whichfunction='0'>Load More</button>
                      </div>
                    </div>

                    <div class="tab-pane fade col-md-12" id="checklisttab" role="tabpanel">
                      <!--Begin- Search and filter -->
                      <div class="m-form m-form--label-align-right m--margin-bottom-30">
                        <div class="row align-items-center">
                          <div class="col-xl-6 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                              <div class="col-md-8">
                                <div class="m-input-icon m-input-icon--left">
                                  <input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
                                  <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                  </span>
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="m-input-icon">
                                  <select class="form-control m-input m-input--square" id="">
                                    <option>All</option>
                                    <option>Active</option>
                                    <option>Inactive</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-xl-6 order-1 order-xl-2 m--align-right">
                            <button class="btn btn-accent dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display:none"> <i class="fa fa-plus"></i>&nbsp;
                              New
                            </button>
                            <div class="dropdown-menu dropdownstyle" aria-labelledby="dropdownMenu2" x-placement="bottom-start">
                              <button class="dropdown-item" type="button" data-toggle="modal" data-target="#blank-modal"><i class="flaticon-file"></i>&nbsp;New Blank Template</button>
                              <!--<button class="dropdown-item" type="button"><i class="flaticon-file"></i>&nbsp;New Premade Template</button>-->
                              <hr>
                              <button class="dropdown-item" type="button" data-toggle="modal" data-target="#folder-modal"><i class="flaticon-folder"></i>&nbsp;New Folder</button>
                            </div>                 
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                          </div>
                        </div>
                      </div>
                      <!--end: Search and Filter -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--begin::Modal-->
            <div class="modal fade" id="folder-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
              <div class="modal-dialog modal-lg" role="document">
                <form id="folder_addform" name="folder_addform" method="post">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Create Folder</h5>
                      <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <label for="recipient-name" class="form-control-label colorblue">Folder Name:</label>
                        <input type="text" class="form-control errorprompt" id="folderName" name="folderName" placeholder="Enter Folder Name">
                        <span style="font-size:0.9em;color:gray" id="foldercharNum"></span>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-accent">Save</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!--end::Modal-->

            <!--begin::Modal-->
            <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <form id="form_foldernameupdate" name="form_foldernameupdate" method="post">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Update Folder Name</h5>
                      <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Folder Name:</label>
                        <input type="text" class="form-control" id="updatefolderName" name="updatefolderName" placeholder="Enter Folder Name">
                        <span style="font-size:0.9em;color:gray" id="updatefoldercharNum"></span>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-accent">Save Changes</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!--end::Modal-->

            <!--begin::Modal-->
            <div class="modal fade" id="assign_membersmodal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Members to <span style="color:#a8e9f1" id="fpc_title"></span>&nbsp;<span id='type-assigned' style="color:#83d686"></span></h5>
                    <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <input type="hidden" id="folprotaskcheck_id" name="">
                    <input type="hidden" id="assign_type">
                  </div>
                  <div class="modal-body">
                    <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
                      <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active show" onclick="reinitAssigneeDatatable()" data-toggle="tab" href="#tab-member" role="tab" aria-selected="true"><i class="la la-users"></i>Members</a>
                      </li>
                      <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-addmember" role="tab" aria-selected="false"><i class="la la-users"></i>Add Members</a>
                      </li>
                    </ul>
                    <div class="tab-content"> 
                      <div class="tab-pane fade show active" id="tab-member">
                        <div class="row">
                          <label class="modallabel colorblue">Members</label>
                          <div class="col-12">
                           <div class="form-group m-form__group" style="margin-bottom: 8px;">
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search Employee Name" id="assigneeSearch">
                              <div class="input-group-append">
                                <span class="input-group-text">
                                  <i class="fa fa-search"></i>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-12" id="assigned_memberdisplay">
                        </div>             
                      </div>
                    </div>
                    <div class="tab-pane fade" id="tab-addmember">
                      <div class="row topdistance">
                        <div class="col-6">
                          <form id="" class="" name="formupdate" method="post">
                            <label for="recipient-name" class="form-control-label colorblue">Select Members by Team</label>
                            <select id="account" onchange="getaccmembers()" class="selectpicker dropdownwidth" data-live-search="true">
                              <optgroup data-max-options="1">
                                <option disabled selected="">Click to select</option>
                                <?php foreach ($account as $acc) { ?>
                                  <option value='<?php echo $acc->acc_id ?>'><?php echo $acc->acc_name;?></option>
                                <?php } ?>
                              </optgroup>                
                            </select>
                          </form>
                        </div>
                        <div class="col-6">

                          <label for="recipient-name" class="form-control-label colorblue">Select Members by Name</label>
    <!--<select id="member" onchange="getmember_name()" class="selectpicker dropdownwidth" data-live-search="true">
    <optgroup data-max-options="1">

    <option disabled selected="">Click to select</option>
    <?php foreach ($members as $mem) { ?>
      <option value='<?php echo $mem->emp_id ?>'><?php echo $mem->lname;echo ", &nbsp;";echo $mem->fname;?></option>
      <?php } ?>

      </optgroup>                
    </select>--> 
    <select class="form-control m-select2" id="member" name="listschedule" onchange="getmember_name()"></select>

  </div>
</div>
<div class='row topdistance'>
  <label class="modallabel colorblue">Search results</label>
  <div class="col-12">
    <table class="table m-table table-hover" id="memberdetails"> 
      <thead align="center">
        <tr>
          <th>Name</th>
          <th>Team</th>
          <th>Access</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody id="myTable" align="center">
      </tbody>
    </table>
    <!-- <div class="m-loader m-loader--info spinner-icon load-style" style="display: none;" ></div> -->
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="updateProcessmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <form id="form_processnameupdate" name="form_processnameupdate" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Process Name</h5>
          <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div class="form-group">
            <label for="recipient-name" class="form-control-label">Process Name:</label>
            <input type="text" class="form-control" id="updateprocessName" name="updateprocessName" placeholder="Enter Process Name">
            <span style="font-size:0.9em;color:gray" id="updateprocesscharNum"></span>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-accent">Save Changes</button>
        </div>
      </div>
    </form>

  </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="blank-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <form id="form_addblanktemplate" name="form_addblanktemplate" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Blank Template</h5>
          <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="form-control-label colorblue">Template Name:</label>
            <input type="text" class="form-control" id="processTitle" name="processTitle">
            <span style="font-size:0.9em;color:gray" id="charNum"></span>
          </div>
          <div class="form-group">
            <label class="colorblue">Choose folder:</label>
            <select class="selectpicker dropdownwidth dropup" data-live-search="true" id="folder_select" name="folder_select">
              <optgroup data-max-options="1">
                <?php foreach ($folders as $folder) { ?>
                  <option value='<?php echo $folder->folder_ID ?>'><?php echo $folder->folderName ?></option>
                <?php } ?>
              </select>
              <p class="m-form__help text-center notestyle" id="leave_description"><i><strong>Note:</strong> You are going to customize your own template.</i></p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="blankroot-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form id="form_addblanktemplate_specific" name="form_addblanktemplate" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Blank Template for <span style="color:#a8e9f1" id="fdrname"></span></h5>
            <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Template Name:</label>
              <input type="text" class="form-control" name="process_folderTitle" id="process_folderTitle">
              <span style="font-size:0.9em;color:gray" id="specific_charNum"></span>
              <input type="hidden" id="rootfolderID" name="rootfolderID">
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="updateChecklistmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form id="form_checklistnameupdate" name="form_checklistnameupdate" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Update Checklist Name</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="recipient-name" class="form-control-label">Checklist Name:</label>
              <input type="text" class="form-control" id="updatechecklistName" name="updatechecklistName" placeholder="Enter Process Name">
              <span style="font-size:0.9em;color:gray" id="updatechecklistcharNum"></span>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent">Save Changes</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--end::Modal-->

  <!--begin::Modal-->
  <div class="modal fade" id="run_checklist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form id="form_runanotherchecklist" name="form_runanotherchecklist" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Let's run another checklist!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <input type="hidden" id="processtemp_id">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <h5 id="processtemplate_title" style="color:#34bfa3"></h5>
              <br>
              <label for="recipient-name" class="form-control-label">Name of your checklist (For example, the name of the employee you’re onboarding) :</label>
              <input type="text" class="form-control" id="addchecklistName" name="addchecklistName" placeholder="Enter Checklist Name">
              <span style="font-size:0.9em;color:gray" id="checkcharNum"></span>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--end::Modal-->
</div>
</div>
</div>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script> -->
<script type="text/javascript">

  var folder_ids = [];
  var process_ids = [];
  var checklist_ids = [];
  $("#selectedprocessStatus").change(function () {
    resetProcess();
    displayprocesstemp();
  });
  $("#selectedchecklistStatus").change(function () {
    resetChecklist();
    displaychecklists();
  });
  function resetFolder() {
    $("#folderLoadMore").show();
    $("#folderLoadMore").data('whichfunction', 0);
    $("#folrow").html("");
    folder_ids = [];
  }
  function resetProcess() {
    $("#processLoadMore").show();
    $("#processLoadMore").data('whichfunction', 0);
    $("#prorow").html("");
    process_ids = [];
  }
  function resetChecklist() {
    $("#checkrow").html("");
    $("#checklistLoadMore").data('whichfunction', 0);
    $("#checklistLoadMore").show();
    checklist_ids = [];
  }
  const display_perPage = 6;
  $('[href="#foldertab"]').click(function () {
    resetFolder();
  });
  $('[href="#processtab"]').click(function () {
    resetProcess();
  });
  $('[href="#checklisttab"]').click(function () {
    resetChecklist();
  });

  $(function(){
    folderpermission_access();
    processpermission_access();

    $("#folderLoadMore").click(function () {
      displayfolders();
    });
    $("#processLoadMore").click(function () {
      displayprocesstemp();
    });
    $("#checklistLoadMore").click(function () {
      displaychecklists();
    });

    $('#member').select2({
      placeholder: "Select your schedule.",
      width: '100%'
    });

//MODAL DISMISS AND RESET
$('#blank-modal').on('hidden.bs.modal', function () {
  formReset('#form_addblanktemplate');
  $('#charNum').text("");
  $('#processTitle').val("");
});

$('#blankroot-modal').on('hidden.bs.modal', function () {
  formReset('#form_addblanktemplate_specific');
  $('#specific_charNum').text("");
  $('#process_folderTitle').val("");
});

$('#folder-modal').on('hidden.bs.modal', function () {
  formReset('#folder_addform');
  $('#foldercharNum').text("");
  $('#folderName').val("");
});

$('#run_checklist-modal').on('hidden.bs.modal', function () {
  formReset('#form_runanotherchecklist');
  $('#checkcharNum').text("");
  $('#addchecklistName').val("");
});

$('#updatemodal').on('hidden.bs.modal', function () {
  formReset('#form_foldernameupdate');
  $('#updatefoldercharNum').text("");
  $('#updatefolderName').val("");
});

$('#updateProcessmodal').on('hidden.bs.modal', function () {
  formReset('#form_processnameupdate');
  $('#updateprocesscharNum').text("");
  $('#updateprocessName').val("");
});

$('#updateChecklistmodal').on('hidden.bs.modal', function () {
  formReset('#form_checklistnameupdate');
  $('#updatechecklistcharNum').text("");
  $('#updatechecklistName').val("");
});

//MODAL DISMISS AND RESET END CODE


//VALIDATION STARTS  HERE
$('#folder_addform').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  folderName: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The folder name must be less than 45 characters'
      },
      notEmpty: {
        message: 'Oops! folder name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#folder_addform').formValidation('disableSubmitButtons', true);
  add_newfolder();
});

$('#form_foldernameupdate').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  updatefolderName: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The folder name must be less than 45 characters'
      },
      notEmpty: {
        message: 'Oops! folder name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#form_foldernameupdate').formValidation('disableSubmitButtons', true);
  var folderid=$('#form_foldernameupdate').data('id');
  saveChangesFolder(folderid);
});

//PROCESS TEMPLATE
$('#form_addblanktemplate').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  processTitle: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The Process name must be less than 45 characters'
      },
      // regexp: {
      //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
      //   message: 'Letters with number combinations only'
      // },
      notEmpty: {
        message: 'Oops! Process name is required!'
      },
    }
  },
  folder_select: {
    validators: {
      notEmpty: {
        message: 'Oops! folder allocation is required!If no list of folder options are displayed, create one first.'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#form_addblanktemplate').formValidation('disableSubmitButtons', true);
  add_newblanktemplate();
});

$('#form_addblanktemplate_specific').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  process_folderTitle: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The Process name must be less than 45 characters'
      },
      // regexp: {
      //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
      //   message: 'Letters with number combinations only'
      // },
      notEmpty: {
        message: 'Oops! Process name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#form_addblanktemplate_specific').formValidation('disableSubmitButtons', true);
  add_newblanktemplate_specific();
});

$('#form_processnameupdate').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  updateprocessName: {
    validators: {
      stringLength: {
        max: 45,
        message: 'The Process name must be less than 45 characters'
      },
      // regexp: {
      //   regexp:  /^\d*[a-z][a-z\d\s]*$/,
      //   message: 'Letters with number combinations only'
      // },
      notEmpty: {
        message: 'Oops! Process name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#form_processnameupdate').formValidation('disableSubmitButtons', true);
  var processid=$('#form_processnameupdate').data('id');
  saveChangesProcess(processid);
});

//CHECKLIST
$('#form_runanotherchecklist').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  addchecklistName: {
    validators: {
      stringLength: {
        max: 80,
        message: 'The checklist name must be less than 80 characters'
      },
      notEmpty: {
        message: 'Oops! checklist name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#form_runanotherchecklist').formValidation('disableSubmitButtons', true);
  runChecklist();
});

$('#form_checklistnameupdate').formValidation({
  message: 'This value is not valid',
  excluded: ':disabled',
// live: 'disabled',
feedbackIcons: {
  valid: 'glyphicon glyphicon-ok',
  invalid: 'glyphicon glyphicon-remove',
  validating: 'glyphicon glyphicon-refresh'
},
fields: {
  updatechecklistName: {
    validators: {
      stringLength: {
        max: 80,
        message: 'The checklist name must be less than 80 characters'
      },
      notEmpty: {
        message: 'Oops! checklist name is required!'
      },
    }
  },
}
}).on('success.form.fv', function (e, data) {
  e.preventDefault();
  $('#form_checklistnameupdate').formValidation('disableSubmitButtons', true);
  var checklistid =$('#form_checklistnameupdate').data('id');
  saveChangesChecklist(checklistid);
});

});

function getfolder_nameid(element){
  var folid=$(element).data('id');
  var foldername=$(element).data('folname');

  $("#rootfolderID").val(folid);
  $("#fdrname").text(foldername);
}
$("#searchString").on("change", function () {
  var str = $(this).val();
  resetFolder();
  displayfolders(str);
});
function getaccmembers(){
  var accountid=$("#account").val();
  var assigntype=$("#assign_type").val();
  var folprotaskcheckID=$("#folprotaskcheck_id").val();
  var accountname=$("#account :selected").text();

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_allemployeeact",
    data: {
      acc_id: accountid
    },
    cache: false,
    success: function (res) {
      var result= JSON.parse(res.trim());
      var stringx="";
      $.each(result, function (key, item) {
        var applicantid=item.apid;
        var userid=item.uid;
        $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>process/fetch_empdetails",
          data: {
            apid: applicantid,
            fptcID: folprotaskcheckID,
            assign_type: assigntype,
            user_ID: userid
          },
          beforeSend:function(){
            $('.spinner-icon').show();
          },
          cache: false,
          success: function (res) {
            $('.spinner-icon').remove();
            var rr= JSON.parse(res.trim());
            $.each(rr, function (key, data) {
              stringx +="<tr>";
              stringx +="<td>" +data.fname+" "+data.lname+"</td>";
              stringx +="<td>" +accountname+"</td>";
              stringx +="<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";

              if(assigntype=='folder' || assigntype=='process'){
                stringx +="<option value='8'>Can View All</option>";       
                stringx +="<option value='7'>Can View Own</option>"; 
                stringx +="<option value='4'>Can View Own and Run</option>";
                stringx +="<option value='3'>Can View and Run</option>";
                stringx +="<option value='1'>Can Edit, View and Run</option>";
                stringx +="<option value='2'>Can Edit, View Own and Run</option>";  
              }else{
                stringx +="<option value='5'>Can Answer</option>";
                stringx +="<option value='6'>Can Edit and Answer</option>";  
              }
              stringx +="</optgroup></select></td>";
              stringx +="<td><button onclick='addmember_assignment(this)' data-reload='1' data-toggle='m-tooltip' data-id='"+data.apid+"' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
              stringx +="</tr>";
            });    
            $("#memberdetails tbody").html(stringx);
          }
        });
      });
    }
  });
}
function fetch_runcheck_details(element){
  var processid=$(element).data('id');
  var processtitle=$(element).data('processtitle');
  var default_checkname=$("#addchecklist_defaultName").val();
  $("#processtemp_id").val(processid);
  $("#processtemplate_title").text(processtitle);
  $("#addchecklistName").val(default_checkname);
}
function display_membernames(element){
	var val = $(element).data("type");
	var id = $(element).data("id");
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/get_allemployeesname",
	data: {
		val: val,
		id: id,
 	},
    cache: false,
    success: function (res) {
      var result= JSON.parse(res);
      var stringx="<option disabled selected=''>Click to select</option>";
    // stringx+="<optgroup data-max-options='1'>";
    $.each(result, function (key, data) {
      stringx+="<option value="+data.emp_id+">"+data.lname+" "+data.fname+"</option>";
    });
    // stringx+="</optgroup>";
    $("#member").html(stringx);
    $("#member").trigger("change");
  }
});
}
function getmember_name(){
  var empid=$("#member").val();
  var empname=$("#member :selected").text();
  var assigntype=$("#assign_type").val();
  var folprotaskcheckID=$("#folprotaskcheck_id").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_oneemployee",  
    data: {
      emp_id: empid,
      fptcID: folprotaskcheckID,
      assign_type: assigntype
    },
    beforeSend:function(){
      $('.spinner-icon').show();
    },
    cache: false,
    success: function (res) {
      var stringx="";
      var result= JSON.parse(res.trim());
      $(".spinner-icon").remove();
      if(res!=0){
        $.each(result, function (key, data) {
          stringx +="<tr>";
          stringx +="<td>" +data.fname+" "+data.lname+"</td>";
          stringx +="<td>" +data.acc_name+"</td>";
          stringx +="<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";

          if(assigntype=="folder" || assigntype=="process"){
            stringx +="<option value='8'>Can View All</option>";       
            stringx +="<option value='7'>Can View Own</option>"; 
            stringx +="<option value='4'>Can View Own and Run</option>";
            stringx +="<option value='3'>Can View and Run</option>";
            stringx +="<option value='1'>Can Edit, View and Run</option>";
            stringx +="<option value='2'>Can Edit, View Own and Run</option>";
          }
          else{
            stringx +="<option value='5'>Can Answer</option>";
            stringx +="<option value='6'>Can Edit and Answer</option>";  
          }      
          stringx +="</optgroup></select></td>";
          stringx +="<td><button onclick='addmember_assignment(this)' data-reload='2' data-toggle='m-tooltip' data-id='"+data.apid+"' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
          stringx +="</tr>";
        }); 
      }else{
        stringx=" ";
      }
      $("#memberdetails tbody").html(stringx);
    }
  });
}
function resetform(){
  $("#formadd").trigger( "reset" );
  $("#formupdate").trigger( "reset" );
  $("#processadd").trigger( "reset" );
  $("#process_foladd").trigger( "reset" );
  $("#account").prop("selectedIndex", 0).change();
  $("#member").prop("selectedIndex", 0).change();
  $("#memberdetails tbody").html("");
  $("#added_members tbody").html("");
}
function fetch_IDforAssignment(element){
  var idforassignment=$(element).data('id');
  var typename=$(element).data('type');
  var name=$(element).data('title');

  $("#folprotaskcheck_id").val(idforassignment);
  $("#assign_type").val(typename);
  $("#fpc_title").text(name);
  $("#type-assigned").text(typename);
}
$("#searchProcess").on("change", function () {
  var str = $(this).val();
  resetProcess();
  displayprocesstemp(str);
});

function access_permissionChange(element){
  var assignmentid=$(element).data('assignid');
  var changepermission=$(element).val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_assignmentpermission",
    data: {
      assid:assignmentid,
      changepermission:changepermission
    },
    cache: false,
    success: function (res) {
      reinitAssigneeDatatable();
      swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Changed Acces Permission  ',
        showConfirmButton: false,
        timer: 1500
      });
      var result = JSON.parse(res.trim());
    }
  });
}
//Change Status

function folderset_toarchive(element){
  var selected_folid=$(element).data('id');
  var status='15';

  swal({
    title: 'Are you sure?',
    text: "All Process templates and checklists will be keep to archive section and cannot be used for the meantime.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, set to archive!'
  }).then(function(result) {
    if (result.value) { 
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/updatefolder_archive",
        data: {
          folder_ID:selected_folid,
          status_ID:status
        },
        cache: false,
        success: function (res) {
          var result = JSON.parse(res.trim());
          swal({
            position: 'center',
            type: 'success',
            title: 'Archived the folder',
            showConfirmButton: false,
            timer: 1500
          });
          $(".folderTrigger").trigger("click");
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Setting folder to archive has been cancelled',
        'error'
        )
    }
  });
}

function processset_toarchive(element){
  var processid=$(element).data('id');
  var status='15';

  swal({
    title: 'Are you sure?',
    text: "All Checklists under this process template will be unaccessible.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, set to archive!'
  }).then(function(result) {
    if (result.value) { 
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/updateprocess_archive",
        data: {
          process_ID:processid,
          status_ID:status
        },
        cache: false,
        success: function (res) {
          var result = JSON.parse(res.trim());
          swal({
            position: 'center',
            type: 'success',
            title: 'Archived the Process Template',
            showConfirmButton: false,
            timer: 1500
          });
          $(".processTrigger").trigger("click");
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Setting Process Template to archive has been cancelled',
        'error'
        )
    }
  });
}
function checklistset_toarchive(element){
  var chid=$(element).data('id');
  var isArchived=1;
  swal({
    title: 'Are you sure?',
    text: "This checklist will be inactive.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, set to archive!'
  }).then(function(result) {
    if (result.value) { 
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/updatechecklist_archive",
        data: {
          checklist_ID:chid,
          isArchived:isArchived
        },
        cache: false,
        success: function (res) {
          var result = JSON.parse(res.trim());
          swal({
            position: 'center',
            type: 'success',
            title: 'Archived the checklist',
            showConfirmButton: false,
            timer: 1500
          });
          $(".checklistTrigger").trigger("click");
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Setting checklist to archive has been cancelled',
        'error'
        )
    }
  });
}
//JQUERY DISPLAY
function folderpermission_access(){
  var uid= $("#user_id").val();
  var folder="folder";
  var displayid=0;

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_access",
    // data: {
    //   uid:uid,
    // },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());

      if(result.length>0){
        $("#blankTemplate2").show();
     // $("#blankTemplate2").css("display","block");
       // 1 if admin
       displayid=1;
       $("#display_id").val(displayid);
       displayfolders();
     }
     else{
      //SEARCH IF THERE'S ASSIGNED FOLDER 
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/fetch_assignment",
        data: {
          type:folder
        },
        cache: false,
        success: function (res) {
          var res = JSON.parse(res.trim());

          if(res==0){
            $("#blankTemplate2").remove();
            // 2 if no assignment
            displayid=2;
            $("#display_id").val(displayid);
            displayfolders();
          }
          else{
            //3 if assignment exist
            displayid=3;
            $("#display_id").val(displayid);
            displayfolders();
          }
        }
      });
    }
  }
});
}
function processpermission_access(){
  var uid= $("#user_id").val();
  var processtemp="process";
  var displayid=0;

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_access",
  // data: {
  //   uid:uid,
  // },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());
   //temporary admin
   if(result.length>0){
     // $("#blankTemplate3").attr("hidden", false);
     $("#blankTemplate3").css("display","block");
     displayid=1;
     $("#displayprocess_id").val(displayid);
     displayprocesstemp();
   }
   else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_assignment",
      data: {
        type:processtemp
      },
      cache: false,
      success: function (res) {
        var res = JSON.parse(res.trim());
        if(res==0){
          $("#blankTemplate3").remove();
          displayid=2;
          $("#displayprocess_id").val(displayid);
          displayprocesstemp();
        }
        else{
          displayid=3;
          $("#displayprocess_id").val(displayid);
          displayprocesstemp();
        }
      }
    });
  }
}
});
}
function checklistpermission_access(){
  var uid= $("#user_id").val();
  var checklist="checklist";
  var displayid=0;

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_access",
  // data: {
  //   uid:uid,
  // },
  cache: false,
  success: function (res) {
    var result = JSON.parse(res.trim());
   //temporary admin
   if(result.length>0){
    $("#dropdownMenu3").attr("hidden", false);
    displayid=1;
    $("#displaychecklist_id").val(displayid);
    displaychecklists();
  }
  else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/fetch_assignment_checklist",
      data: {
        type:checklist
      },
      cache: false,
      success: function (res) {
        var res = JSON.parse(res.trim());
        if(res==0){
          $("#dropdownMenu3").remove();
          displayid=2;
          $("#displaychecklist_id").val(displayid);
          displaychecklists();
        }
        else{
          displayid=3;
          $("#displaychecklist_id").val(displayid);
          displaychecklists();
        }
      }
    });
  }
}
});
}
function getassignmentrule(folderid,type){
  var id;
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetchone_assignment",
    data: {
      folderid:folderid,
      typename:type
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
    }
  });
}

function displayfolders(word) {
  var d_id = $("#display_id").val();
  var uid = $("#user_id").val();

  var whichfunction = $("#folderLoadMore").data('whichfunction');

  var link = (d_id === "2") ? "display_ownfolders" : (d_id === "1") ? "display_folders" : (d_id === "3") ? "display_assignedfolders" : " ";

  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/" + link,
    data: {
      word: word,
      folder_ids: folder_ids,
      whichfunction: whichfunction
    },
    beforeSend: function () {
      $('.loading-icon').show();
    },
    cache: false,
    success: function (res) {
      $('.loading-icon').hide();
      var stringx = "";
      var stringlastid = "";
      var folderid;
      var type = "folder";
      var assignment;

      var result = JSON.parse(res.trim());

      if (result.data.length < display_perPage) {
        $("#folderLoadMore").hide();
      }
      $("#folderLoadMore").data('whichfunction', result.whichfunction);
      var lastid = 0;

      $.each(result.data, function (key, item) {
        folder_ids.push(item.folder_ID);
        lastid = item.folder_ID;
        var foldername = item.folderName;
        var substringname = "";
        if (foldername.length > 22) {
          substringname = foldername.substring(0, 22) + "...";
        } else {
          substringname = foldername.substring(0, 22);
        }
                                                    //get access permission for each folder  

                                                    var assignmentRuleid = item.assignmentRule_ID;

                                                    stringx += "<div class='col-lg-4 foldercontent' id='" + item.status_ID + "'>";
                                                    stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
                                                    stringx += "<div class='m-portlet__head border-accent rounded foldercolor'>";

                                                    stringx += "<div class='row' style='margin-top:1em'>";
                                                    stringx += "<div class='m-portlet__head-caption'>";
                                                    stringx += "<div class='m-portlet__head-title'>";
                                                    stringx += "<span class='m-portlet__head-icon'>";
                                                    stringx += "<i class='fa fa-folder icon-color' id='" + item.folder_ID + "'></i>";
                                                    stringx += "</span>";

                                                    stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-original-title='" + item.folderName + "' title='" + item.folderName + "' style='color:#9ce4f5;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/subfolder') ?>" + '/' + item.folder_ID + "' class='foldname'>" + substringname + "</a>";

                                                    stringx += "<input type='hidden' value='" + item.folder_ID + "' id='eachfolderid'>";
                                                    stringx += "<input type='hidden' class='folStat' value='" + item.status_ID + "'";
                                                    stringx += "<input type='hidden' class='fetchval' id='fetchval'>";
                                                    stringx += "</div>";
                                                    stringx += "</div>";
                                                    stringx += "</div>";
                                                    stringx += "&nbsp;&nbsp;&nbsp;&nbsp;<span class='colorwhitesize createdby'>" + item.fname + "&nbsp;" + item.lname + "</span>";

                                                    stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
                                                    stringx += "<div class='m-portlet__head-tools'>";
                                                    stringx += "<ul class='m-portlet__nav'>";

                                                    // if admin OR can edit, view and run OR can edit, view own and run

                                                    if (assignmentRuleid == "1" || assignmentRuleid == "2" || d_id == "1" || item.createdBy == uid) {
                                                      stringx += "<li class='m-portlet__nav-item' data-toggle='m-tooltip' data-original-title='Change, Archive, and Remove'>";

                                                      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
                                                      stringx += "<i class='la la-cog'></i></a>";
                                                      stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
                                                      stringx += "<button class='dropdown-item' type='button' data-id='" + item.folder_ID + "' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
                                                      stringx += "<i class='flaticon-file'></i>&nbsp;Change folder name</button>";

                                                      stringx += "<button class='dropdown-item' type='button' data-id=" + item.folder_ID + " onclick='folderset_toarchive(this)'>";
                                                      stringx += "<i class='la la-archive'></i>";
                                                      stringx += "&nbsp;Archive this folder</button>";

                                                      stringx += "<button class='dropdown-item' type='button' data-type='folder' data-id='" + item.folder_ID + "' onclick='delete_Folprocheck(this)'>";
                                                      stringx += "<i class='la la-trash'></i>&nbsp;Delete this folder</button>";

                                                      stringx += "</div>";
                                                      stringx += "</li>";

                                                      stringx += "<li class='m-portlet__nav-item' data-toggle='modal' data-title='" + item.folderName + "' data-target='#assign_membersmodal' data-type='folder' data-id='"+item.folder_ID+"' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='m-tooltip' data-original-title='Assign Members'>";
                                                      stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
                                                      stringx += "<i class='la la-users'></i>";
                                                      stringx += "</a>";
                                                      stringx += "</li>";

                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' onclick='getfolder_nameid(this)' data-toggle='modal' data-target='#blankroot-modal' data-folname='" + item.folderName + "' data-id='" + item.folder_ID + "'><i class='la la-plus-circle'></i></a> ";
                                                      stringx += "</li>";
                                                    } else if (d_id === "2") {
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
                                                      stringx += "<i class='la la-cog'></i></a>";
                                                      stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
                                                      stringx += "<button class='dropdown-item' type='button' data-id='" + item.folder_ID + "' onclick='updateFetch(this)' data-toggle='modal' data-target='#updatemodal'>";
                                                      stringx += "<i class='flaticon-file'></i>&nbsp;Change folder name</button>";

                                                      stringx += "<button class='dropdown-item' type='button' data-id=" + item.folder_ID + " onclick='folderset_toarchive(this)'>";
                                                      stringx += "<i class='la la-archive'></i>";
                                                      stringx += "&nbsp;Archive this folder</button>";

                                                      stringx += "<button class='dropdown-item' type='button' data-type='folder' data-id='" + item.folder_ID + "' onclick='delete_Folprocheck(this)'>";
                                                      stringx += "<i class='la la-trash'></i>&nbsp;Delete this folder</button>";

                                                      stringx += "</div>";
                                                      stringx += "</li>";

                                                      stringx += "<li class='m-portlet__nav-item' data-toggle='modal' data-target='#assign_membersmodal' data-type='folder' data-id='" + item.folder_ID + "' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='m-tooltip' data-original-title='Assign Members'>";
                                                      stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
                                                      stringx += "<i class='la la-users'></i>";
                                                      stringx += "</a>";
                                                      stringx += "</li>";

                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' onclick='getfolder_nameid(this)' data-toggle='modal' data-target='#blankroot-modal' data-folname='" + item.folderName + "' data-id='" + item.folder_ID + "'><i class='la la-plus-circle'></i></a> ";
                                                      stringx += "</li>";
                                                    }
                                                    stringx += "<li class='m-portlet__nav-item'>";
                                                    stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-info-circle'></i></a> ";
                                                    stringx += "</li>";

                                                    stringx += "</ul>";
                                                    stringx += "</div>";
                                                    stringx += "</div>";

                                                    stringx += "</div>";
                                                    stringx += "</div>";
                                                    stringx += "</div>";
                                                  });

$("#folrow").append(stringx);
$('[data-toggle="tooltip"]').tooltip("dispose");
$('[data-toggle="tooltip"]').tooltip();

stringlastid += "<div class='load-more col-lg-12' lastID='" + lastid + "' style='display: none;'>";
stringlastid += "<h5 style='text-align: center;'>";
stringlastid += "<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
stringlastid += "</div> Loading more folder...</h5></div>";
$("#folrow").append(stringlastid);
}
});
}
function displayprocesstemp(word) {
  var uid = $("#user_id").val();
  var d_id = $("#displayprocess_id").val();
  var fold_id = $("#display_id").val();
  var selectedval = $("#selectedprocessStatus").val();

  var whichfunction = $("#processLoadMore").data('whichfunction');

  var assignmentRuleid;
  var folderassignmentRule;

                                        //IF d_id==1 -> THE USER IS AN ADMIN
                                        var link = (fold_id == "2" && d_id == "2") ? "display_ownprocess" : ((fold_id == "3" && d_id == "3") || (fold_id == "2" && d_id == "3") || (fold_id == "3" && d_id == "2")) ? "display_assignedprocess" : (d_id == "1") ? "display_processtemplates" : " ";

                                        $.ajax({
                                          type: "POST",
                                          url: "<?php echo base_url(); ?>process/" + link,
                                          data: {
                                            word: word,
                                            process_ids: process_ids,
                                            statval: selectedval,
                                            whichfunction: whichfunction
                                          },
                                          beforeSend: function () {
                                            $('.loading-proicon').show();
                                          },
                                          cache: false,
                                          success: function (res) {
                                            $('.loading-proicon').hide();
                                            var stringx = "";
                                            var stringlastid = "";
                                            var lastid = 0;

                                            var result = JSON.parse(res.trim());
                                            if (result.data.length < display_perPage) {
                                              $("#processLoadMore").hide();
                                            }
                                            $("#processLoadMore").data('whichfunction', result.whichfunction);


                                            $.each(result.data, function (key, item) {
                                              process_ids.push(item.process.process_ID);
                                              lastid = item.process.process_ID;
                                              var processname = item.process.processTitle;
                                              var substringprocess = "";


                                              if (d_id == 3 || fold_id == 3) {
                                                assignmentRuleid = item.process.assignmentRule_ID;
                                              }

                                              if (processname.length > 22) {
                                                substringprocess = processname.substring(0, 22) + "...";
                                              } else {
                                                substringprocess = processname.substring(0, 22);
                                              }
                                                    //check process under assigned folders
                                                    if (item.process.folProCheTask_ID == item.process.processfolderid) {
                                                      if (item.process.assignmentRule_ID != null) {
                                                        folderassignmentRule = item.process.assignmentRule_ID;
                                                      }
                                                    }
                                                    stringx += "<div class='col-lg-4 processcontent pr+processtatusid'>";
                                                    stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>"
                                                    stringx += "<div class='m-portlet__head border-accent rounded colorlightblue'>";
                                                    stringx += "<div class='row' style='margin-top:1em'>";
                                                    stringx += "<div class='m-portlet__head-caption'>";
                                                    stringx += "<div class='m-portlet__head-title'>";
                                                    stringx += "<span class='m-portlet__head-icon'>";
                                                    stringx += "<i class='fa fa-file-text-o colorblue'></i>";
                                                    stringx += "</span>";
                                                    if (item.process.processstatus_ID == 10) {
                                                      stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-original-title='" + item.process.processTitle + "'  title='" + item.process.processTitle + "' style='display:block;text-overflow: ellipsis;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist') ?>" + '/' + item.process.process_ID + "' class='m-portlet__head-text protitle'>" + substringprocess + "</a>";
                                                    } else if (item.process.processstatus_ID == 2) {
                                                      stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' dat  a-original-title='" + item.process.processTitle + "'  title='" + item.process.processTitle + "' style='display:block;text-overflow: ellipsis;overflow: hidden; white-space: nowrap;' href='<?php echo site_url('process/checklist_pending') ?>" + '/' + item.process.process_ID + "' class='m-portlet__head-text protitle'>" + substringprocess + "</a>";
                                                    }
                                                    stringx += "</div>";
                                                    stringx += "</div>";
                                                    stringx += "</div>";

                                                    stringx += "<span class='folname' style='color:#088594;font-size: 0.9em;margin-left:1em'><b>" + item.process.folderName + "</b></span>";
                                                    stringx += "<input type='hidden' class='proStat' value='" + item.process.processstatus_ID + "'>";
                                                    stringx += "<input type='hidden' class='eachprocessid' value=" + item.process.process_ID + ">";
                                                    stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
                                                    stringx += "<div class='m-portlet__head-tools'>";
                                                    stringx += "<ul class='m-portlet__nav'>";

                                                    //ADMIN ACCESS
                                                    if (d_id == "1" || assignmentRuleid == "1" || assignmentRuleid == "2") {
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
                                                      stringx += "<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";
                                                      stringx += "<button class='dropdown-item' type='button' data-id='" + item.process.process_ID + "' onclick='updateProcessFetch(this)' data-toggle='modal' data-target='#updateProcessmodal'><i class='flaticon-file'></i>&nbsp;Change Process Name</button>";
                                                      stringx += "<button class='dropdown-item' type='button' data-title='" + item.process.processTitle + "' data-type='process' data-id='" + item.process.process_ID + "' onclick='fetch_IDforAssignment(this),initAssignee(this),display_membernames(this)' data-toggle='modal' data-target='#assign_membersmodal'><i class='flaticon-file'>";
                                                      stringx += "</i>&nbsp;Assign this Process</button>";
                                                      stringx += "<button class='dropdown-item' type='button' data-type='process' data-id='" + item.process.process_ID + "' onclick='delete_Folprocheck(this)'><i class='flaticon-file'>";
                                                      stringx += "</i>&nbsp;Delete this template</button>";
                                                      if (item.process.processstatus_ID == "2") {
                                                        stringx += "<button class='dropdown-item' type='button' data-type='process' data-id='" + item.process.process_ID + "' onclick='processset_toarchive(this)' data-target='#assign_membersmodal'><i class='flaticon-file'>";
                                                        stringx += "</i>&nbsp;Archive this template</button>";
                                                      }
                                                      stringx += "</div> ";
                                                      stringx += "</li>";     
                                                      if (item.process.processstatus_ID == "2" || item.process.ownid == uid) {
                                                        stringx += "<li class='m-portlet__nav-item'>";
                                                        stringx += "<a href='#' data-toggle='modal' data-id='" + item.process.process_ID + "' data-processtitle='" + item.process.processTitle + "' onclick='fetch_runcheck_details(this)' data-target='#run_checklist-modal' class='m-portlet__nav-link m-portlet__nav-link--icon' data-toggle='m-tooltip' data-placement='top' title='Run Another Checklist'><i class='la la-plus-circle coloricons'></i></a>";
                                                        stringx += "</li>";
                                                        stringx +="<li class='m-portlet__nav-item'>";
                                                        stringx += "<a href='<?php echo site_url('process/report')?>"+'/'+item.process.process_ID+"' class='m-portlet__nav-link m-portlet__nav-link--icon' data-toggle='tooltip' data-placement='top' title='Report'><i class='la la-area-chart coloricons'></i></a> " ;
                                                        stringx +="</li>";
                                                        stringx +="</li> &nbsp;|&nbsp;";
                                                      }
                                                    }else if(item.process.ownid == uid && item.process.processstatus_ID == "2"){
                                                      //OWNED ACCESS
                                                      
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='la la-cog coloricons'></i></a>";
                                                      stringx += "<div class='dropdown-menu' aria-labelledby='dropdownOption' x-placement='bottom-start' style='position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;'>";
                                                      stringx += "<button class='dropdown-item' type='button' data-id='" + item.process.process_ID + "' onclick='updateProcessFetch(this)' data-toggle='modal' data-target='#updateProcessmodal'><i class='flaticon-file'></i>&nbsp;Change Process Name</button>";
                                                      stringx += "<button class='dropdown-item' type='button' data-title='" + item.process.processTitle + "' data-type='process' data-id='" + item.process.process_ID + "' onclick='fetch_IDforAssignment(this),initAssignee(this),display_membernames(this)' data-toggle='modal' data-target='#assign_membersmodal'><i class='flaticon-file'>";
                                                      stringx += "</i>&nbsp;Assign this Process</button>";
                                                      if (item.process.processstatus_ID == "2") {
                                                        stringx += "<button class='dropdown-item' type='button' data-type='process' data-id='" + item.process.process_ID + "' onclick='processset_toarchive(this)' data-target='#assign_membersmodal'><i class='flaticon-file'>";
                                                        stringx += "</i>&nbsp;Archive this template</button>";
                                                      }
                                                      stringx += "</div> ";
                                                      stringx += "</li>";
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' data-portlet-tool='#' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='fa fa-trash-o coloricons'></i></a>";

                                                      stringx +="<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='<?php echo site_url('process/report')?>"+'/'+item.process.process_ID+"' class='m-portlet__nav-link m-portlet__nav-link--icon' data-toggle='tooltip' data-placement='top' title='Report'><i class='la la-area-chart coloricons'></i></a> " ;
                                                      stringx +="</li>";
                                                      stringx +="</li> &nbsp;|&nbsp;";
                                                    }

                                                    if (assignmentRuleid == "3" || assignmentRuleid == "4" || (item.process.ownid == uid && item.process.processstatus_ID == "2")) {
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' data-toggle='modal' data-id='" + item.process.process_ID + "' data-processtitle='" + item.process.processTitle + "' onclick='fetch_runcheck_details(this)' data-target='#run_checklist-modal' class='m-portlet__nav-link m-portlet__nav-link--icon'  data-toggle='m-tooltip' data-placement='top' title='Run Another Checklist'><i class='la la-plus-circle coloricons'></i></a>";
                                                      stringx += "</li>";
                                                    }
                                                    if (item.process.processstatus_ID == 10) {
                                                      stringx += " <li class='m-portlet__nav-item'>";
                                                      stringx += " <span class='m-badge m-badge--info m-badge--wide' data-toggle='m-tooltip' data-placement='top' title data-original-title='Click process template to add, modify and run a checklist' >Run a Checklist</span>";
                                                      stringx += "</li> ";
                                                    } else if (item.process.processstatus_ID == 2) {
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += " <span class='m-menu__link-badge'>";
                                                      stringx += " <span class='m-badge m-badge--metal' data-toggle='tooltip' data-placement='top' title='All Checklist'>" + item.statuses.all + "</span>";
                                                      stringx += "</span>";
                                                      stringx += "</li> ";
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += " <span class='m-menu__link-badge'>";
                                                      stringx += "<span class='m-badge m-badge--info' data-toggle='tooltip' data-placement='top' title='Pending Checklist'>" + item.statuses.pending + "</span>";
                                                        // stringx +="<span class='m-badge m-badge--info' data-toggle='m-tooltip' data-placement='top' title data-original-title='Pending Checklist'>"+item.statuses.pending+"</span>";
                                                        stringx += "</span>";
                                                        stringx += "</li>";
                                                        stringx += "<li class='m-portlet__nav-item'>";
                                                        stringx += "<span class='m-menu__link-badge'>";
                                                        stringx += "<span class='m-badge m-badge--danger' data-toggle='tooltip' data-placement='top' title='Overdue Checklist'>" + item.statuses.overdue + "</span>";
                                                        stringx += "</span>";
                                                        stringx += "</li>";
                                                        stringx += "<li class='m-portlet__nav-item'>";
                                                        stringx += "<span class='m-menu__link-badge'>";
                                                        stringx += "<span class='m-badge m-badge--success' data-toggle='tooltip' data-placement='top' title='Completed Checklist'>" + item.statuses.completed + "</span>";
                                                        stringx += " </span>";
                                                        stringx += "</li>";
                                                      }
                                                      stringx += "</ul>";
                                                      stringx += "</div>";
                                                      stringx += "</div>";
                                                      stringx += "</div>";
                                                      stringx += "</div>"
                                                      stringx += "</div>";
                                                });//end of each

$("#prorow").append(stringx);
$('[data-toggle="tooltip"]').tooltip("dispose");
$('[data-toggle="tooltip"]').tooltip();
console.log(lastid);
stringlastid += "<div class='processload-more col-lg-12' processlastID='" + lastid + "' style='display: none;'>";
stringlastid += "<h5 style='text-align: center;'>";
stringlastid += "<div class='m-loader m-loader--info' style='width: 30px; display: inline-block;'>";
stringlastid += "</div> Loading more process template...</h5></div>";
$("#prorow").append(stringlastid);
                                            }//end of success
                                          });
}
function displaychecklists(word) {
  var whichfunction = $("#checklistLoadMore").data('whichfunction');

  var d_id = $("#displaychecklist_id").val();
  var prod_id = $("#displayprocess_id").val();
  var fold_id = $("#display_id").val();
  var selectedval = $("#selectedchecklistStatus").val();

  var uid = $("#user_id").val();
  var assignmentRuleid = 0;

  var link = (d_id == "2" && prod_id == "2" && fold_id == "2") ? "display_ownchecklists" : (d_id == "1") ? "display_allchecklists" : ((d_id == "3" && prod_id == "3" && fold_id == "3") || (d_id == "3" && prod_id == "3" && fold_id == "2") || (d_id == "3" && prod_id == "2" && fold_id == "2") || (d_id == "2" && prod_id == "2" && fold_id == "3") || (d_id == "2" && prod_id == "3" && fold_id == "2") || (d_id == "2" && prod_id == "3" && fold_id == "3") || (d_id == "3" && prod_id == "2" && fold_id == "3")) ? "display_assignedchecklists" : " ";
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/" + link,
    data: {
      checklist_ids: checklist_ids,
      whichfunction: whichfunction,
      word: word,
      statval: selectedval
    },
    beforeSend: function () {
      $('.loading-icon-check').show();
    },
    cache: false,
    success: function (res) {
      $('.loading-icon-check').hide();
      var stringx = "";
      var checklistrule = "";

      var result = JSON.parse(res.trim());
      if (result.data.length < display_perPage) {
        $("#checklistLoadMore").hide();
      }
      $("#checklistLoadMore").data('whichfunction', result.whichfunction);
      $.each(result.data, function (key, item) {
        checklist_ids.push(item.checklist.checklist_ID);
        if (d_id == "3" || prod_id == "3" || fold_id == "3") {
          assignmentRuleid = item.checklist.assignmentRule_ID;
        }

        if (d_id == "3") {
          checklistrule = item.checklist.checklist_assrule;
        }

        var cid = item.checklist.checklist_ID;
        var checklist_name = item.checklist.checklistTitle;

        var substringname = "";
        if (checklist_name.length > 22) {
          substringname = checklist_name.substring(0, 22) + "...";
        } else {
          substringname = checklist_name.substring(0, 22);
        }
        stringx += "<div class='col-lg-4'>";
        stringx += "<div class='m-portlet m-portlet--collapsed m-portlet--head-sm' data-portlet='true' id='m_portlet_tools_7'>";
        stringx += "<div class='m-portlet__head border-accent rounded checklistcolor'>";

        stringx += "<div class='row' style='margin-top:1em'>";
        stringx += "<div class='m-portlet__head-caption'>";
        stringx += "<div class='m-portlet__head-title'>";
        stringx += "<span class='m-portlet__head-icon'>";

        if (item.checklist.status_ID == "2") {
          stringx += "<i class='fa fa-clock-o coloricons-checklist' id=''></i>";
        } else if (item.checklist.status_ID == "3") {
          stringx += "<i class='fa fa-check coloricons-checklist' id=''></i>";
        }
        stringx += "</span>";
        stringx += "<a data-toggle='tooltip' data-placement='top' data-skin='dark' data-original-title='" + item.checklist.checklistTitle + "' title='" + item.checklist.checklistTitle + "' style='color:#484747;display:table-cell;font-size: 1.2rem;font-weight: 500;' href='<?php echo site_url('process/checklist_answerable') ?>" + '/' + item.checklist.process_ID + '/' + item.checklist.checklist_ID + "' class='foldname'>" + substringname + "</a>";
        stringx += "<input type='hidden' name='chid' id='chID' value='" + item.checklist.checklist_ID + "'>";
        stringx += "<input type='hidden' id='ch" + item.checklist.checklist_ID + "'>";
        stringx += "</div>";
        stringx += "</div>";
        stringx += "</div>";

                                                    // var dateString=item.dateTimeCreated;
                                                    // var dateObj = new Date(dateString);
                                                    // var momentObj = moment(dateObj);
                                                    // var momentString = momentObj.format('lll');
                                                    // stringx +="&nbsp;&nbsp;&nbsp;&nbsp;<span class='createdby' style='color:#043106;font-size:0.9em'>"+momentString+"</span><br>";

                                                    stringx += "&nbsp;<span class='createdby pro-name'>" + item.checklist.processTitle + "</span><br>";

                                                    // stringx +="<span style='font-size: 0.9em;margin-left:1em'>";
                                                    // stringx +="<span class='m-badge m-badge--accent m-badge--wide'>CH</span>";
                                                    // stringx +="<span class='m-badge m-badge--accent m-badge--wide'>SB</span>";
                                                    // stringx +="</span>";

                                                    stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>"
                                                    stringx += " <div class='col-md-12'>";
                                                    stringx += "<div class='progress'>"


                                                    var alltask = item.statuses.all;
                                                    var allcomp = item.statuses.completed;
                                                    var allpending = item.statuses.pending;

                                                    if (alltask == allcomp) {
                                                      stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-success' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Completed'></div>"
                                                      stringx += "</div>"
                                                    } else if (allcomp == 0) {
                                                      stringx += "<div class='progress-bar progress-bar progress-bar-animated bg-light' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:100%' data-toggle='m-tooltip' data-placement='top' data-original-title='Not Yet Answered'></div>"
                                                      stringx += "</div>"
                                                    } else {
                                                      var percentage_pending = (allpending / alltask) * 100;
                                                      stringx += "<div class='progress-bar progress-bar-striped progress-bar-animated bg-danger' role='progressbar' aria-valuenow='65' aria-valuemin='0' aria-valuemax='100' style='width:" + percentage_pending + "%' data-toggle='m-tooltip' data-placement='top' data-original-title='" + allpending + "/" + alltask + "'></div>"
                                                      stringx += "</div>"
                                                    }
                                                    stringx += "</div>";
                                                    stringx += "</div>";

                                                    stringx += "<div class='row' style='margin-top:1em;margin-bottom: 1em;'>";
                                                    stringx += "<div class='m-portlet__head-tools'>";
                                                    stringx += "<ul class='m-portlet__nav'>";

                                                    if (d_id == "1" || assignmentRuleid == "1" || item.checklist.ownid == uid || checklistrule == "6") {
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' class='m-portlet__nav-link m-portlet__nav-link--icon' id='dropdownOption' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
                                                      stringx += "<i class='la la-cog bottom-icons'></i></a>";
                                                      stringx += "<div class='dropdown-menu dropdownstyle' aria-labelledby='dropdownOption' x-placement='bottom-start'>";
                                                      stringx += "<button class='dropdown-item' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='updateChecklistFetch(this)' data-toggle='modal' data-target='#updateChecklistmodal'>";
                                                      stringx += "<i class='flaticon-file'></i>&nbsp;Change Checklist name</button>";
                                                      stringx += "<button class='dropdown-item' type='button' data-title='" + item.checklist.checklistTitle + "' data-id='" + item.checklist.checklist_ID + "' data-type='checklist' onclick='fetch_IDforAssignment(this),display_membernames(this),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'>";
                                                      stringx += "<i class='la la-users'></i>";
                                                      stringx += "&nbsp;Assign this checklist</button>";

                                                      stringx += "<button data-type='checklist' class='dropdown-item' data-type='checklist' type='button' data-id='" + item.checklist.checklist_ID + "' onclick='delete_Folprocheck(this)'>";
                                                      stringx += "<i class='la la-trash'></i>&nbsp;Delete this Checklist</button>";
                                                      stringx += "</div>";
                                                      stringx += "</li>";

                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<a href='#' data-id='" + item.checklist.checklist_ID + "' onclick='checklistset_toarchive(this)' class='m-portlet__nav-link m-portlet__nav-link--icon'><i class='la la-archive bottom-icons'></i></a> ";
                                                      stringx += "</li>";
                                                    }
                                                    stringx += "<li class='m-portlet__nav-item' data-toggle='modal' data-original-title='Tooltip title'>";
                                                    stringx += "<a class='m-portlet__nav-link m-portlet__nav-link--icon'>";
                                                    stringx += "<i class='la la-info-circle bottom-icons'></i>";
                                                    stringx += "</a>";
                                                    stringx += "</li>";

                                                    stringx += "<li class='m-portlet__nav-item'>";
                                                    stringx += "<span class='m-menu__link-badge'>";

                                                    if (item.checklist.dueDate != null) {
                                                      var dateString2 = item.checklist.dueDate;
                                                      var dateObj2 = new Date(dateString2);
                                                      var dateObj3 = new Date();
                                                      var datediff = new Date(dateObj2 - dateObj3);
                                                      var days = datediff / 1000 / 60 / 60 / 24;
                                                      var rounddays = Math.round(days);

                                                      var momentObj2 = moment(dateObj2);
                                                      var momentString2 = momentObj2.format('ll');

                                                      if (dateObj2 > dateObj3) {
                                                        stringx += "<span class='m-badge m-badge--success m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Due Date:" + rounddays + "left'>" + momentString2 + "</span>";
                                                      } else if (dateObj2 < dateObj3) {
                                                        stringx += "<span class='m-badge m-badge--danger m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Overdue!'>" + momentString2 + "</span>";
                                                      } else if (dateObj2 == dateObj3) {
                                                        stringx += "<span class='m-badge m-badge--warning m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Today is this checklist Due date! Please finish this before this day ends.'>" + momentString2 + "</span>";
                                                      }
                                                    }
                                                    stringx += "</span>";
                                                    stringx += "</li>";

                                                    if (item.checklist.taskruleid == "5" || item.checklist.taskruleid == "6") {
                                                      stringx += "<li class='m-portlet__nav-item'>";
                                                      stringx += "<span class='m-menu__link-badge'>";
                                                      stringx += "<span class='m-badge m-badge--brand m-badge--wide'  data-toggle='m-tooltip' data-placement='top' title data-original-title='Contains assigned tasks'>Assigned Task</span>";
                                                      stringx += "</span>";
                                                      stringx += "</li>";
                                                    }
                                                    stringx += "</ul>";
                                                    stringx += "</div>";
                                                    stringx += "</div>";

                                                    stringx += "</div>";
                                                    stringx += "</div>";
                                                    stringx += "</div>";
                                                  });

$("#checkrow").append(stringx);
$('[data-toggle="tooltip"]').tooltip("dispose");
$('[data-toggle="tooltip"]').tooltip();

}
});
}

//END OF JQUERY DISPLAY

//CHARACTERS COUNTER
$('#processTitle').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#charNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#charNum').text(char+'/45 characters remaining');
  }
});

$('#process_folderTitle').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#specific_charNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#specific_charNum').text(char+'/45 characters remaining');
  }
});

$('#folderName').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#foldercharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#foldercharNum').text(char+'/45 characters remaining');
  }
});

$('#addchecklistName').keyup(function () {
  var max = 80;
  var len = $(this).val().length;
  if (len >= max) {
    $('#checkcharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#checkcharNum').text(char+'/80 characters remaining');
  }
});

// UPDATE

$('#updatefolderName').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#updatefoldercharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#updatefoldercharNum').text(char+'/45 characters remaining');
  }
});

$('#updateprocessName').keyup(function () {
  var max = 45;
  var len = $(this).val().length;
  if (len >= max) {
    $('#updateprocesscharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#updateprocesscharNum').text(char+'/45 characters remaining');
  }
});

$('#updatechecklistName').keyup(function () {
  var max = 80;
  var len = $(this).val().length;
  if (len >= max) {
    $('#updatechecklistcharNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#updatechecklistcharNum').text(char+'/80 characters remaining');
  }
});
//END CHARACTERS COUNTER


function getprocessval(element)
{
  var eachprocessid=$("#eachprocessid").val();
  $('.processcontent').hide();
  if(element.value=="100"){
    $('.processcontent').show();
  }
  $('.processcontent .proStat').each(function(){
    var folderStatus=$(".proStat").val();
    if(element.value=="10"){
      $('.pr10').show();
    }
    else if(element.value=="2"){
      $('.pr2').show();
    } 
    else if(element.value=="11"){
      $('.pr11').show();
    }   
  });
}
$(document).ready(function () {
  $('#formadd').formValidation({
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      folderName: {
        message: 'Your folder name is not valid',
        validators: {
          notEmpty: {
            message: 'Your folder name is required and can\'t be empty'
          },
          regexp: {
            regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
            message: 'Folder name can only consist of letters and spaces'
          }
        }
      }
    }
  }).on('success.form.bv', function(e) {
    e.preventDefault();
  });
  $('#subformadd').formValidation({
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      subfolderName: {
        message: 'Your sub-folder name is not valid',
        validators: {
          notEmpty: {
            message: 'Your sub-folder name is required and can\'t be empty'
          },
          regexp: {
            regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
            message: 'Sub-folder name can only consist of letters and spaces'
          }
        }
      }
    }
  }).on('success.form.bv', function(e) {
    e.preventDefault();
  });
  $('#processadd').formValidation({
    message: 'This value is not valid',
    icon: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      processTitle: {
        message: 'Your template/process name is not valid',
        validators: {
          notEmpty: {
            message: 'Your template/process name is required and can\'t be empty'
          },
          regexp: {
            regexp: /^([a-zA-Z]+\s)*[a-zA-Z]+$/,
            message: 'template/process name can only consist of letters and spaces'
          }
        }
      }
    }
  }).on('success.form.bv', function(e) {
    e.preventDefault();
  });
});
function emptyField(){
  $(".errormessage").remove();  
  $("input.errorprompt").after("<span class='errormessage' style='color:red;font-size:0.8em'>*This field can't be empty</span>");
}
function PemptyField(){
  $(".pErrormessage").remove();  
  $("input.pErrorprompt").after("<span class='pErrormessage' style='color:red;font-size:0.8em'>*This field can't be empty</span>");
}
function removemember_assignment(element){
  swal({
    title: 'Are you sure?',
    text: "This member will be removed from accessing this file.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_assignment",
        data: {
          assign_ID:$(element).data('id')
        },
        cache: false,
        success: function () {
         reinitAssigneeDatatable();
         swal(
          'Deleted!',
          'Member successfully Removed.',
          'success'
          )
          // $("#assign_membersmodal").modal('hide');
        },
        error: function (res){
          swal(
            'Oops!',
            'Something is wrong with your code!',
            'error'
            )
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Removing member has been cancelled',
        'error'
        )
    }
  });
}
//SAVE or ADD functionalities

function addmember_assignment(element){
  var apid=$(element).data('id');
  var reload=$(element).data('reload');
  var fptc_id=$("#folprotaskcheck_id").val();
  var assigntype=$("#assign_type").val();
  var assignrule=$("#assignmentRule :selected").val();
  var fpc_title=$("#fpc_title").text();

  swal({
    title: 'Are you sure?',
    text: "This member will be assigned to this folder/process template/checklist",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes'
  }).then(function(result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/add_assignment",
        data: {
          type: assigntype,
          assignmentRule_ID: assignrule,
          applicantid: apid,
          folprotaskcheck_id: fptc_id,
          title: fpc_title
        },
        cache: false,
        success: function (res) {
          var resObj = $.parseJSON(res.trim())
          console.log(resObj.notifid);
          systemNotification(resObj.notifid.notif_id);
          if(reload==1){
            getaccmembers();
          }else if(reload==2){
            getmember_name();
          }
          addmember_assignment();
          swal(
            'Added!',
            'User successfully assigned.',
            'success'
            )
          $("#assign_membersmodal").modal('hide');
        },
        error: function (res){
          swal(
            'Oops!',
            'Cannot assign',
            'error'
            )
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Assigning user has been cancelled',
        'error'
        )
    }
  });
}
function add_newfolder(){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_folder",
    data: {
      folderName: $("#folderName").val(),
    },
    cache: false,
    success: function (res) {
      $('#folder-modal').modal('hide');
      displayfolders();
      swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Added Folder',
        showConfirmButton: false,
        timer: 1500
      });
      var result = JSON.parse(res.trim());
    },
    error: function (res){
      console.log(res);
    }
  });
}

function add_newblanktemplate(){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_processfolder",
    data: {
      processTitle: $("#processTitle").val(),
      folder_ID: $("#folder_select").val()
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      // console.log(res);
      if(res>0){
        displayfolders();
        processpermission_access()
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Process',
          showConfirmButton: false,
          timer: 1500
        });
        $("#blank-modal").modal("hide");
      }else{
        swal({
          position: 'center',
          type: 'error',
          title: 'There was an error upon saving.',
          showConfirmButton: true,
        });
      }


    },
    error: function (res){
      console.log(res);
    }
  });
}
function add_newblanktemplate_specific(){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_processfolder",
    data: {
      processTitle: $("#process_folderTitle").val(),
      folder_ID: $("#rootfolderID").val()
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      $('#blankroot-modal').modal('hide');
      displayfolders();
      swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Added Process',
        showConfirmButton: false,
        timer: 1500
      });
      var result = JSON.parse(res.trim());
    },
    error: function (res){
      console.log(res);
    }
  });
}
$("#btn-subfoldersave").click(function(){
  if ($.trim($("#subfolderName,#rootfolder_ID").val()) === "") {
    e.preventDefault();
  }else{
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/add_subfolder",
      data: {
        folderName: $("#subfolderName").val(),
        folder_ID: $("#rootfolder_ID").val()
      },
      cache: false,
      success: function (res) {
        window.location.href = '<?php echo base_url('process')?>';
        swal({
          position: 'center',
          type: 'success',
          title: 'Successfully Added Sub folder',
          showConfirmButton: false,
          timer: 1500
        });
        var result = JSON.parse(res.trim());
      },
      error: function (res){
        console.log(res);
      }
    });
  }
});
function foldericon(element){
  var iconid=$(element).data('id');
  $("#foldericon"+iconid).toggleClass("fa fa-folder-open").toggleClass("fa fa-folder");
}

function delete_Folprocheck(element){
  var type_fpc=$(element).data('type');
  console.log("type:"+type_fpc);
  swal({
    title: 'Are you sure?',
    text: "All data under this "+type_fpc+" will be deleted, too.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_folder_process_checklist",
        data: {
          all_ID: $(element).data('id'),
          type: $(element).data('type')
        },
        cache: false,
        success: function () {
          if(type_fpc=="folder"){
            $(".folderTrigger").trigger("click");
          }else if(type_fpc=="process"){
            $(".processTrigger").trigger("click");
          }else{
            $(".checklistTrigger").trigger("click");
          }
          swal(
            'Deleted!',
            type_fpc+' successfully deleted.',
            'success'
            )
        },
        error: function (res){
          swal(
            'Oops!',
            'You cant delete!',
            'error'
            )
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a '+type_fpc+' has been cancelled',
        'error'
        )
    }
  });
  console.log($(element).data('id'));
}



function updateFetch(element){
  var folderName = $("#updatefolderName").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_update",
    data: {
      folder_ID: $(element).data('id'),
      folderName:folderName
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      var data = res;
      $("#updatefolderName").val(data.folderName);
      $("#form_foldernameupdate").data('id',data.folder_ID);
    }
  });
}
function updateProcessFetch(element){
  var processTitle = $("#updateprocessName").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetchprocess_update",
    data: {
      process_ID: $(element).data('id'),
      processTitle:processTitle
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      $("#updateprocessName").val(data.processTitle);
      $("#form_processnameupdate").data('id',data.process_ID);
    }
  });
}
function updateChecklistFetch(element){
  var checklistTitle = $("#updatechecklistName").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetchchecklist_update",
    data: {
      checklist_ID: $(element).data('id'),
      checklistTitle:checklistTitle
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      $("#updatechecklistName").val(data.checklistTitle);
      $("#form_checklistnameupdate").data('id',data.checklist_ID);
    }
  });
}
function saveChangesFolder(folderid){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_folder",
    data: {
      folder_ID: folderid,
      folderName: $("#updatefolderName").val()
    },
    cache: false,
    success: function (res) {
    // displayfolders();
    $(".folderTrigger").trigger("click");
    swal({
      position: 'center',
      type: 'success',
      title: 'folder successfully updated',
      showConfirmButton: false,
      timer: 1500
    });
    $("#updatemodal").modal('hide');

  }
});
}
function saveChangesProcess(processid){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_process",
    data: {
      process_ID: processid,
      processTitle: $("#updateprocessName").val()
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'Process successfully updated',
        showConfirmButton: false,
        timer: 1500
      });
      $("#updateProcessmodal").modal('hide');
      //displayprocesstemp();
      $(".processTrigger").trigger("click");

    }
  });
}
function saveChangesChecklist(checklistid){
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_checklist",
    data: {
      checklist_ID: checklistid,
      checklistTitle: $("#updatechecklistName").val()
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'checklist successfully updated',
        showConfirmButton: false,
        timer: 1500
      });
      $("#updateChecklistmodal").modal('hide');
      $(".checklistTrigger").trigger("click");
     // displaychecklists();
     
   }
 });
}
$("#searchChecklist").on("change", function () {
  var str = $(this).val();
  resetChecklist();
  displaychecklists(str);
});

function runChecklist(){
  var pid=$("#processtemp_id").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_anotherchecklist",
    data: {
      process_ID:pid,
      checklistTitle: $("#addchecklistName").val()
    },
    cache: false,
    success: function (res) {
      $('#run_checklist-modal').modal('hide');
      displaychecklists();
      displayprocesstemp();
      swal({
        position: 'center',
        type: 'success',
        title: 'Successfully Added checklist',
        showConfirmButton: false,
        timer: 1500
      });
      var result = JSON.parse(res.trim());
    },
    error: function (res){
      console.log(res);
    }
  });
}

//DATATABLE
function reinitAssigneeDatatable(){    
  $('#assigned_memberdisplay').mDatatable('destroy');
  assigneeDatatable.init($('#assigneeSearch').val(), $('#assign_membersmodal').data('id'),$('#assign_membersmodal').data('type'));
}
$('#assigneeSearch').on('keyup', function(){
  reinitAssigneeDatatable();
})
function initAssignee(element){
  console.log("init assignee");
  $('#assigned_memberdisplay').mDatatable('destroy');
  $('#assign_membersmodal').data('type', $(element).data('type'));
  $('#assign_membersmodal').data('id', $(element).data('id'));
  assigneeDatatable.init($('#assigneeSearch').val(), $(element).data('id'),$(element).data('type'));
}
var assigneeDatatable = function () {
  var assignee = function (searchVal, folProcIdVal,typeval) {
    var options = {
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'POST',
            url: baseUrl + "/process/list_assignee_datatable",
            params: {
              query: {
                folProcId: folProcIdVal,
                type:typeval,
                assigneeSearch: searchVal
              },
            },
          }
        },
        saveState: {
          cookie: false,
          webstorage: false
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },
      layout: {
theme: 'default', // datatable theme
class: '', // custom wrapper class
scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
height: 550, // datatable's body's fixed height
footer: false // display/hide footer
},
sortable: true,
pagination: true,
toolbar: {
// toolbar placement can be at top or bottom or both top and bottom repeated
placement: ['bottom'],

// toolbar items
items: {
// pagination
pagination: {
// page size select
pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
},
}
},
search: {
  input: $('#assigneeSearch'),
},
rows: {
  afterTemplate: function (row, data, index) {},
},
// columns definition
columns: [{
  field: "fname",
  title: "Employee Name",
  width: 180,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = row.fname + " " + row.lname;
    return html;
  },
},{
  field: "acc_name",
  title: "Team",
  width: 160,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = row.acc_name;
    return html;
  },
},{
  field: "lname",
  title: "Access",
  width: 220,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var rule1 = '';
    var rule2 = '';
    var rule3 = '';
    var rule4 = '';
    var rule5 = '';
    var rule6 = '';
    var rule7 = '';
    var rule8 = '';
    if(row.assignmentRule_ID == 1){
      rule1 = "selected";
    }else if(row.assignmentRule_ID == 2){
      rule2 = "selected";
    }else if(row.assignmentRule_ID == 3){
      rule3 = "selected";
    }else if(row.assignmentRule_ID == 4){
      rule4 = "selected";
    }
    else if(row.assignmentRule_ID == 5){
      rule5 = "selected";
    }
    else if(row.assignmentRule_ID == 6){
      rule6 = "selected";
    }
    else if(row.assignmentRule_ID == 7){
      rule7 = "selected";
    }
    else if(row.assignmentRule_ID == 8){
      rule8 = "selected";
    }
    var html='';
    if(row.type=="folder" || row.type=="process"){
     html ='<select onchange="access_permissionChange(this)" data-assignid="'+row.assign_ID+'" class="form-control m-input m-input--square">'+
     '<option value="1" '+rule1+'>Can Edit, View and Run</option>'+
     '<option value="2" '+rule2+'>Can Edit, View Own and Run</option>'+
     '<option value="3" '+rule3+'>Can View and Run</option>'+
     '<option value="4" '+rule4+'>Can View Own and Run</option>'+
     '<option value="7" '+rule7+'>Can View Own</option>'+
     '<option value="8" '+rule8+'>Can View All</option>'+
     '</select>';
   }else if(row.type=="checklist" || row.type=="task"){
     html ='<select onchange="access_permissionChange(this)" data-assignid="'+row.assign_ID+'" class="form-control m-input m-input--square">'+
     '<option value="5" '+rule5+'>Can Answer</option>'+
     '<option value="6" '+rule6+'>Can Edit and Answer</option>'+
     '</select>';
   }
   return html;
 },
},{
  field: "assign_ID",
  title: "Action",
  width: 90,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = '<button onclick="removemember_assignment(this)" data-toggle="m-tooltip" data-id="'+row.assign_ID+'" title="Remove this member" data-original-title="Tooltip title" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="fa fa-remove"></i></button>';
    return html;
  },
}],
};
var datatable = $('#assigned_memberdisplay').mDatatable(options);
};
return {
  init: function (searchVal, folProcIdVal,typeval) {
    assignee(searchVal, folProcIdVal,typeval);
  }
};
}();

</script>