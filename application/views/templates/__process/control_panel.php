<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">
<style type="text/css">
.portlet-display{
	background: #4B4C54 !important;
	color: white !important;
	height: 50px !important;
}
.padding-distance{
	padding: 15px;
}
.button-padding-distance{
	padding: 40px;
}
.div_selection{
	color: #555555;
	width: 100%;
	height:113px;
	background-color:#f5f5f5; 
	overflow-y:scroll;
	overflow-x: auto;
}
.labels{
	color:#00bcd4;
	font-size: 18px;
}	
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator font-poppins">Control Panel</h3>       
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<!-- <li class="m-nav__separator"><</li>
					<li class="m-nav__item">
						<a href="http://localhost/supportz/process" class="m-nav__link">
							<span class="m-nav__link-text">Folder</span>
						</a>
					</li>
					<li class="m-nav__separator"><</li>
					<li class="m-nav__item">
						<a href="#" class="m-nav__link">
							<span class="m-nav__link-text">Process</span>
						</a>
					</li> -->
				</ul>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head portlet-header-report portlet-display" >
						<div class="m-portlet__head-caption">	
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon font-white">
									<i class="fa fa-drivers-license-o"></i>
								</span>
								<h3 class="m-portlet__head-text font-poppins" style="color:white !important;font-size:16px">
									Access Role
								</h3>
							</div>
						</div>
					</div>
					<div class="m-portlet__body">
						<div class="row">
							<div class="col-lg-12 padding-distance">
								<label class="labels">Select names to add member as admin</label>							
								<!-- Select -->
								<select multiple="multiple" class="form-control m-input m-input--air" name="duallistbox_live[]" id="multipleselect_employees">
								</select>
								<div class="text-center padding-distance">
									<!-- <button type="button" onclick="add_as_admin()" class="btn btn-accent btn-sm m-btn m-btn--custom">Add as admin</button> -->
									<span style="color:green;font-style: italic;">*Search and select names from the left box that will be added on the right box. Click 'Save admin list' to save the list of names on the right box to be assigned as admin.</span><br><br>
									<button type="button" onclick="add_as_admin()" class="btn btn-accent btn-sm m-btn m-btn--custom">Save admin list</button>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>
<script type="text/javascript">
	var timer=null;
	$(function(){
		display_listofemployees();		
	})
	function display_listofemployees(){
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>process/fetch_allemployees",
			cache: false,
				// beforeSend:function(){
				// 	$('.processload-more').show();
				// },
				success: function (res) {
					var stringx="";
					var result= JSON.parse(res.trim());
					$.each(result, function (key, data) {
						var selected = (data.adminAccess_ID!=null) ? "selected": "";

						stringx+="<option data-name='"+data.lname+", "+data.fname+"' data-id='"+data.emp_id+"' value="+data.emp_id+" "+selected+">"+data.lname+", "+data.fname+"</option>";
					});
					$("#multipleselect_employees").html(stringx);
					$("#multipleselect_employees").bootstrapDualListbox('refresh');
				}
			});
	}
	function add_as_admin(){
		var selectedemp_id=$("select#multipleselect_employees").val();
		console.log(selectedemp_id);
		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>process/add_admin",
			data: {
				emp_id: selectedemp_id
			},
			cache: false,
			success: function (res)
			{ 
				display_listofemployees();
				res = JSON.parse(res.trim());
				clearTimeout(timer); 
				timer = setTimeout(doNotif, 1000)
			},
			error: function (res){
				console.log(res);
			}
		});
	}
	//NOTIFS
	function doNotif() {
		$.notify("Saved!");
		setTimeout(function() {
			$.notifyClose('top-right');
		}, 2000);
	}		
	$('select[name="duallistbox_live[]"]').bootstrapDualListbox();
</script>