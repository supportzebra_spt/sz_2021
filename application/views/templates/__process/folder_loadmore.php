<?php if(!empty($folders)){ foreach ($folders as $folder) {?>
  <div class="col-lg-4 foldercontent fc<?php echo $folder->status_ID;?>" id="<?php echo 'foldercon'.$folder->folder_ID; ?>">    
    <?php $countprocess=0;
    $countchecklist=0;
    $countchecklistpending=0; 
    foreach ($processes as $prs) {
      if($folder->folder_ID==$prs->folder_ID){
        $countprocess++;
        foreach ($checklists as $chl) {
          if($prs->process_ID==$chl->process_ID){
           if($chl->status_ID!="10"){
             $countchecklist++;
           }
           if($chl->status_ID=="2"){
             $countchecklistpending++;
           }
         }
       }
     }
   }
   ?>
   <div class="m-portlet m-portlet--collapsed m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_7">
     <div class="m-portlet__head border-accent rounded foldercolor">
       <div class="row" style="margin-top:1em">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon">
              <i class="fa fa-folder" id="<?php echo 'foldericon'.$folder->folder_ID; ?>"></i>
            </span> 
            <a href="<?php echo site_url('process/subfolder').'/'.$folder->folder_ID;?>" class="foldname" style="color:#9ce4f5;display:table-cell;font-size: 1.3rem;font-weight: 500;"><?php echo $folder->folderName; ?></a>
            <input type="hidden" value="<?php echo $folder->folder_ID;?>" id="eachfolderid">
            <input type="hidden" class="folStat" value="<?php echo $folder->status_ID;?>">
            <input type="hidden" class="fetchval" id="fetchval"> 
          </div>          
        </div>
      </div>
     <!--  <span style="color:white">
        <?php 
        $datetime=$folder->dateTimeCreated;
        $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
        echo sprintf($date->format("M j, Y"))."&nbsp; at &nbsp;";
        echo sprintf($date->format("g:ia"));
        ?>
        <br>
      </span> -->&nbsp;&nbsp;&nbsp;&nbsp;
      <span class="colorwhitesize createdby"><?php echo $folder->fname;?>&nbsp; 
       <?php echo $folder->lname ?></span>
       <div class="row" style="margin-top:1em;margin-bottom: 1em;">
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav" >
            <li class="m-portlet__nav-item">
             <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon" id="dropdownOption" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="la la-cog"></i></a>
             <div class="dropdown-menu" aria-labelledby="dropdownOption" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
               <button class="dropdown-item" type="button" data-id="<?php echo $folder->folder_ID; ?>" onclick="updateFetch(this)" data-toggle="modal" data-target="#updatemodal"><i class="flaticon-file"></i>&nbsp;Change folder name</button>
               <button class="dropdown-item" type="button"><i class="la la-archive">
               </i>&nbsp;Archive this folder</button>
               <button class="dropdown-item" type="button" data-id="<?php echo $folder->folder_ID; ?>" onclick="deleteFolder(this)"><i class="la la-trash">
               </i>&nbsp;Delete this folder</button>
             </div>
           </li>
           <li class="m-portlet__nav-item">
            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-info-circle"></i></a> 
          </li>
          <li class="m-portlet__nav-item">
            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-users"></i></a>
          </li>
          <li class="m-portlet__nav-item colorwhiteicons">
           <span>|</span>
         </li>
                       <!--   <li class="m-portlet__nav-item">
                          <a href="#" data-id="<?php echo $folder->folder_ID; ?>" onclick="deleteFolder(this)" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-trash"></i></a> 
                        </li> -->       
                        <li class="m-portlet__nav-item">
                          <span class="m-menu__link-badge">
                            <span class="m-badge m-badge--metal" data-toggle="m-tooltip" data-placement="top" title data-original-title="All Process Templates"><?php echo $countprocess;?></span>
                          </span>  
                        </li>  
                        <li class="m-portlet__nav-item">
                         <span class="m-menu__link-badge">
                          <span class="m-badge m-badge--success" data-toggle="m-tooltip" data-placement="top" title data-original-title="All Checklists"><?php echo $countchecklist;?></span>
                        </span> 
                      </li>
                      <li class="m-portlet__nav-item">
                        <span class="m-menu__link-badge">
                          <span class="m-badge m-badge--danger" data-toggle="m-tooltip" data-placement="top" title data-original-title="All Pending Checklists"><?php echo $countchecklistpending;?></span>
                        </span>  
                      </li>
                    </ul>
                  </div>
                </div>     
              </div>
            </div>
          </div>
        <?php }?>
        <div class="load-more col-lg-12" lastID="<?php echo $folder->folder_ID;?>" style="display: none;">
          <h5 style="text-align: center;"> <div class="m-loader m-loader--info" style="width: 30px; display: inline-block;"></div> Loading more folder...</h5>
        </div>
      <?php }?>  
      <div class="load-more col-lg-12" lastID="0">
      </div>