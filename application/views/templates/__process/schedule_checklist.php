<style>
.siteDiv {
  display: block;
  position: relative;
  color: #fff;
  text-align: center;
  text-transform: uppercase;
  border: none;
  
}
.siteDiv.one:after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 0;
  height: 100%;
  background-color: rgba(255,255,255,0.4);
	-webkit-transition: none;
	   -moz-transition: none;
	        transition: none;
}
.siteDiv.one:hover:after {
  width: 120%;
  background-color: rgba(255,255,255,0);
  cursor:pointer;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
	        transition: all 0.4s ease-in-out;
}
.txtUp{
	text-transform: uppercase;
}
.txtIndent{
	    text-indent: 20px;
}
div#payslipRs {
    background: #fff;
    border: 1px solid #a9a9a9;
    padding: 16px;
}
tr td {
    padding: 17px;
}
thead {
    background: #4CAF50;;
}
thead td {
    text-align: center;
    font-weight: bold;
	color: #fff;
}
tbody tr td {
	background: #8bc34a1a;
    border: 1px solid #4caf502e;
}
</style>
<link href="<?php echo base_url();?>assets/multi-select/bootstrap-duallistbox.css" rel="stylesheet" type="text/css" media="all">

<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">Scheduled Process Checklist</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">-</li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("dashboard"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Home</span>
						</a>
					</li>
					<li class="m-nav__separator"> > </li>
					<li class="m-nav__item">
						<a href="<?php echo base_url("process"); ?>" class="m-nav__link">
							<span class="m-nav__link-text">Process Templates</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			
			
            <div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-12">
					<!--begin::Portlet-->
					<div class="m-portlet">
						<div class="m-portlet__head" style="background: #2c2e3eeb;">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text" style="color: #34bfa3 !important; ">
										<i class="fa fa-bullhorn"></i>  Records
									</h3>
								</div>
							</div>
						</div>
						<!--begin::Form-->
  								 <div id="resultCron" style="padding: 16px;">
								 </div>
							 
							 <br>
					 
						<!--end::Form-->
					</div>
					<!--end::Portlet-->
			 
				</div>

			</div>
						   
				
            </div>
			
        </div>
	
			
		</div>
	</div>
<script src="<?php echo base_url();?>assets/multi-select/jquery.bootstrap-duallistbox.js"></script>
<script src="https://unpkg.com/cronstrue@latest/dist/cronstrue.min.js" async></script>

 <script>
function delCron(id,pid){
 
  swal({
    title: 'Are you sure?',
    text: "Automatic schedule for assigning Checklist will be removed.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {
      $.ajax({
        type:'POST',
        url: "<?php echo base_url(); ?>process/deleteCron",
        data: {
          process_ID:pid,
          pstCron_ID:id,
        },
        success:function(html){ 
			if(html.trim()=="found"){
				loadCron();
			}else{
				swal("Error!","Something went wrong, Please contact System Administrator.","error")
			}
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Attempting to delete an automatic assigning of checklist has been cancelled',
        'error'
        )
    }
  });
 
	
}
function loadCron(){
	 $.ajax({
        type:'POST',
        url: "<?php echo base_url(); ?>process/getRecords",
        success:function(html){
			var result = JSON.parse(html);
		var  rs= "<table style='width: 100%;'>"
		rs+= "<thead><tr><td>TASK ID</td><td>ASSIGNEES</td><td>TEMPLATE</td><td>TASK SUMMARY</td><td>ADDED ON</td><td>ACTION</td></tr></thead>"
		if(html!=0){
		$.each(result.cron, function (key, item) {
		   
		    rs+="<tr>"
				   rs+="<td>"+item["pstCron_ID"].padLeft(8)+"</td>"
				   var ass = "";
				    $.each(item["assignees"], function (key2, item2) {
						ass+=item2["fname"]+" "+item2["lname"]+", ";
					});
					rs+="<td>"+ass+"</td>";
				   rs+="<td>"+item["processTitle"]+"</td>";
				   rs+="<td>"+cronstrue.toString(item["cron_sched"])+"</td>";
				   rs+="<td>"+item["added_on"]+"</td>";
 				   rs+='<td> <a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" onclick="delCron('+item["pstCron_ID"]+','+item["process_ID"]+')"><i class="la la-trash"></i></a></td>';
			rs+="</tr>"
	   });
	  
			
        }else{
			rs+="<tr><td colspan=6 style='text-align: center;font-size: large;font-weight: 500;'>No Records Found</td></tr>"
		}
		 $("#resultCron").html(rs);
        }
      });
}
	$(function(){
		var emp_id = <?php echo $_SESSION["emp_id"]; ?>;
		
		$('.m-select2').select2({ width: '100%' });
 	loadCron();

	});
 
 </script>