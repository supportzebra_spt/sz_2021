<link href="<?php echo base_url(); ?>assets/src/custom/css/croppie-2.6.2.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.css">

<style type="text/css">
.active:focus {
  background: #e9eeef;
  border-color: #00c5dc;
  color: #3f4047;
}
.iframepadding{
  padding-left: 15px !important;
  padding-right: 15px !important;
}
.activeinput:focus{
 background: #1d86da;
}
.droparea{
  text-align: center;
}
.droparea_outside{
  border-style: dashed;
  padding: 10px;
  margin: 10px;
  border-color:#a5a6ad;
  background: #fff4d2;
}
.row.droparea_outside:hover {
    background: #ffebb0;
}
.droparea_append{
  padding: 10px;
  margin: 10px;
}
.colorgreen{
  color: green;
  font-style: italic;
  font-size:0.9em;
}
.evenadding{
  padding: 15px;
  background:#f1f0f0;
}
.oddadding{
  padding: 15px;
  background: #f9ecec;
}
.buttonform{
  text-align: center;
}
.distance{
  margin-bottom: 20px;
}
.formdistance{
  margin-top: 10px;
}
.taskdistance{
  margin-top: 15px;
}
.label{
  font-size:1.2em;
  color:#00c5dc;
}
.labelheading{
  font-size:1.4em;
  font-weight: bold;
  color:#00c5dc;
}
.placeholdercolor::placeholder { 
  color: #d8dce8;
  opacity: 1;
}
button.h{
  border:1px solid blue;
  background:blue;
  color:white;
}
body.dragging, body.dragging * {
  cursor: move !important;
}
.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}
/*temp*/
.placeholder {
  border: 1px solid green;
  background-color: white;
  -webkit-box-shadow: 0px 0px 10px #888;
  -moz-box-shadow: 0px 0px 10px #888;
  box-shadow: 0px 0px 10px #888;
}
.tile {
  height: 100px;
}
.grid {
  margin-top: 1em;
}
.formpadding
{
  padding: 30px;
  margin-left: 15px;
}
.editable{
  text-decoration: underline !important;
  text-decoration-style: dashed !important;
  text-decoration-color: #50af54 !important;
}
.load-style{
  width: 30px;
  display: none;
  margin-top:45%;
  margin-left: 45%;
}
.load-style-task{
 width: 30px;
 display: none;
 margin-top:25%;
 margin-left: 45%;
}
.sublabel-color{
  color:#6a6c71;
}
.padding_formbutton{
  padding-left: 45px;
  padding-top: 20px;
}
.buttonform {
    background: #4CAF50;
    border: 1px solid #38803b;
}
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
        <ul class="m-subheader__breadreadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo site_url('process');?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('process/subfolder').'/'.$processinfo->folder_ID;?>" class="m-nav__link">
              <span class="m-nav__link-text">Source Folder</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet">
          <!--begin: Portlet Head-->
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  <a href="#" onclick="doNotif2()" class="btn btn-outline-success btn-sm m-btn m-btn--icon">
                    <span>
                      <i class="fa fa-save"></i>
                      <span>Save</span>
                    </span>
                  </a>
                  <?php if($processinfo->processstatus_ID=="10"){?>
                    <button onclick="defaultchecklist_name()" data-id="<?php echo $processinfo->process_ID;?>" class="btn btn-success btn-sm m-btn m-btn m-btn--icon" data-toggle="modal" data-target="#run_checklist-modal">   <span>
                      <i class="fa fa-play-circle-o"></i>
                      <span>Run checklist</span>
                    </span>
                  </button>
                <?php }?>
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
            </ul>
          </div> 
        </div>
        <!--end: Portlet Head-->
        <div class="m-portlet__body m-portlet__body--no-padding">
          <div class="m-wizard m-wizard--4 m-wizard--brand m-wizard--step-first" id="m_wizard">
            <div class="row m-row--no-padding">
              <div class="col-xl-6 col-lg-12 border"><br>
                &nbsp;&nbsp;<h5 style="color:#777;text-align:center;font-size:1.3em;"><strong><?php echo  $processinfo->processTitle;?> Template</strong></h5>
                <div class="taskdistance" style="text-align: center">
                  <a href="#" data-id="<?php echo $processinfo->process_ID;?>" onclick="addtask(this)" class="btn btn-outline-accent btn-sm  m-btn m-btn--icon m-btn--pill">
                    <span>
                      <i class="fa fa-plus"></i>
                      <span>Add Task</span>
                    </span>
                  </a>      
                </div><br>
                <input type="hidden" id="hiddenprocessID" name="hiddenprocessID" value="<?php echo $processinfo->process_ID;?>"> 
                <?php foreach($user_info as $ui) {?>
                  <input type="hidden" class="form-control" id="default_addchecklistName" name="default_addchecklistName" value="<?php echo $ui->fname;echo "&nbsp;";echo $ui->lname;echo "'s";echo "&nbsp;" ;echo date("h:i a");echo "&nbsp;";echo "checklist"?>">
                <?php }?>
                <?php foreach ($checklist as $ch){?>
                 <input type="hidden" id="hiddencheckid" name="hiddencheckid" value="<?php echo $ch->checklist_ID; ?>"> 
               <?php }?>
               <div class="m-portlet__body" style="height:350px;overflow-y:scroll; overflow-x:auto;background-color:">
                 <div class="m-loader m-loader--info loading-icon load-style-task"></div>
                 <div class="movetask form-group m-form__group" id="checkgroup" >
                 </div>
               </div>             
             </div>
             <div id="contentdiv" class="col-xl-6 col-lg-12 border">
              <div class="m-loader m-loader--info loading-icon load-style"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- END-->
</div>

<!--begin::Modal-->
<div class="modal fade" id="run_checklist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <form id="form_runchecklist" name="form_runchecklist" method="post">
      <div class="modal-content"> 
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Let's run a checklist!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <h5 style="color:#34bfa3">Base on <strong>'<?php echo $processinfo->processTitle; ?> Template'</strong></h5>
            <br>
            <label for="recipient-name" class="form-control-label">Name of your checklist (For example, the name of the employee you’re onboarding) :</label>
            <input type="text" class="form-control" id="addchecklistName" name="addchecklistName" placeholder="Enter Checklist Name">
            <span style="font-size:0.9em;color:gray" id="charNum"></span>
            <input type="hidden" value="<?php echo $processinfo->process_ID;?>" id="fetchprocessid">
          </div>
        </div>
        <?php foreach ($checklist as $ch){?>
          <div class="modal-footer">
            <button type="submit" class="btn btn-accent"><i class="fa fa-play-circle-o"></i> Run This Checklist</button>
          </div>
        <?php }?>
      </div>
    </form>
  </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="add_content-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create a Form!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Form</a>
          </li>
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
          <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span>
          <div class="row">
            <input type="hidden" id="taskformid" value="">
            <input type="hidden" id="sequenceid" value="">
            <div class="col-lg-6">
              <button type="button" onclick="add_component('1')" data-id="forminputtext" id="forminputtext" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Short Text Entry</button>
              <button type="button" onclick="add_component('2')" data-id="formtextarea" id="formtextarea" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Long Text Entry</button> 
              <button type="button" onclick="add_component('6')" data-id="formsingle" id="formsingle" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-dot-circle-o"></i> Single Select</button>
              <button type="button" onclick="add_component('8')" data-id="formmulti" id="formmulti" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-check-square-o"></i> Multiple Select</button>
              <button type="button" onclick="add_component('7')" data-id="formdropdown" id="formdropdown" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-toggle-down"></i> Dropdown Select</button>
              <button type="button" onclick="add_component('10')" data-id="formdate" id="formdate" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date Picker</button>
              <button type="button" onclick="add_component('13')" data-id="formtime" id="formtime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Time Picker</button>
              <button type="button" onclick="add_component('14')" data-id="formdatetime" id="formdatetime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date and Time Picker</button>
              <button type="button" onclick="add_component('12')" data-id="formemail" id="formemail" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-toggle-down"></i> Email</button>      
              <br>
            </div>
            <div class="col-lg-6">
              <button type="button" onclick="add_component('3')" data-id="formheading" id="formheading" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-header"></i> Heading</button>
              <button type="button" onclick="add_component('4')" data-id="formtext" id="formtext" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-text-width"></i> Text</button>
              <button type="button" onclick="add_component('5')" data-id="formnumber" id="formnumber" class="btn btn-accent btn-sm btn-block buttonform"><i class="la la-sort-numeric-desc"></i> Number</button> 
              <button type="button" onclick="add_component('11')" data-id="formspinner" id="formspinner" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-sort-numeric-asc"></i> Number Spinner</button>
              <button type="button" onclick="add_component('9')" data-id="formdivider" id="formdivider" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-i-cursor"></i> Divider</button>
              <button type="button" onclick="add_component('15')" data-id="formfile" id="formfile" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;File Uploader</button><br>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-insert">
          <div class="row">
            <div class="col-lg-12">
              <label class="btn btn-accent btn-sm btn-block"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Add file as content  
               <form id="formFilez" enctype="multipart/form-data">
                <input type="file" style="display: none;" name="upload_files" id="upload_files">
              </form>
            </label>
            <label class="btn btn-accent btn-sm btn-block"><i class="fa fa-image"></i>&nbsp;&nbsp;Add photo as content<input type="file" style="display: none;" name="upload_image" id="upload_image"> </label>
            <button type="button" data-id="file_uploader" id="video_uploader" class="btn btn-accent btn-sm btn-block" ><i class="fa fa-video-camera"></i>&nbsp;&nbsp;Add video as content</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<!--end::Modal-->

<!-- Upload video modal -->
<div id="modalVideoUpload" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #03A9F4;">
        <h5 class="modal-title" id="exampleModalLabel">Upload Video</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="form-group m-form__group">
         <label class="m-radio m-radio--bold m-radio--state-success">
           <input type="radio" name="vidType" id="vidType" class="vidType" value="1"> URL
           <span></span>
         </label>
         <div class="m-input-icon m-input-icon--left m-input-icon--right">
          <input type="text" class="form-control m-input m-input--pill"  id="videoUrl" placeholder="Paste Youtube URL Link here.">
          <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-tag"></i></span></span>
          <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comments-o"></i></span></span>
        </div>
      </div>

      <hr>
      <div class="form-group m-form__group">
        <label class="m-radio m-radio--bold m-radio--state-success">
         <input type="radio" name="vidType" id="vidType" class="vidType" value="2" checked> Video Upload
         <span></span>
       </label>
       <div class="m-input-icon m-input-icon--left m-input-icon--right">
        <form id="formVidz" enctype="multipart/form-data">
          <input type="file" class="form-control m-input m-input--pill" name="videoFile" id="videoFile">
        </form>
        <input type="text" id="taskid_reference" name="taskid_reference" hidden>
      </div>
    </div>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-info" id="btnVidUpload">Save</button>
    <button type="button" class="btn btn-info" id="btnVidUrl" style="display:none">Save</button>
  </div>
</div>
</div>
</div>
<!-- End video modal -->

<!--begin::Modal-->
<div class="modal fade" id="formproperties-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Entry Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="labelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="sublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Set Placeholder</label>
              <input type="text" id="placeholderform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Your Placeholder...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label class="col-3 col-form-label">Set to required</label>
              <div class="col-6">
                <span class="m-switch m-switch--lg m-switch--icon">
                  <label>
                    <input class="requirecheckbox" id="requirecheckbox" type="checkbox" name="">
                    <span></span>
                  </label>
                </span>
              </div>
            </div>
            <div class="m-form__group form-group row">
              <label>Text Minimum / Number Range From</label>
              <input id="textmax" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Maximum / Number Range To</label>
              <input id="textmin" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger m-btn m-btn--icon" type="button" data-dismiss="modal">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-formdetailssave" onclick="saveformproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="formtextheading-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Header and Text Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="ht_labelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div id="sublab" class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="ht_sublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Alignment</label>
              <select class="form-control m-input m-input--solid" id="textalignment">
                <option>Left</option>
                <option>Center</option>
                <option>Right</option>
              </select>
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label>Text Size</label>
              <select class="form-control m-input m-input--solid" id="textsize">
                <option>Default</option>
                <option>Large</option>
                <option>Small</option>
              </select>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger m-btn m-btn--icon" type="button" data-dismiss="modal">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-htdetailsave" onclick="savehtproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="formoptionlist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Radio, Checkbox and Dropdown Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="optionlabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="optionsublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
           <div class="m-form__group form-group row">
            <label class="col-3 col-form-label">Set to required</label>
            <div class="col-6">
              <span class="m-switch m-switch--lg m-switch--icon">
                <label>
                  <input class="requirecheckbox" id="optionrequirecheckbox" type="checkbox" name="">
                  <span></span>
                </label>
              </span>
            </div>
          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button class="btn btn-danger m-btn m-btn--icon" type="button" data-dismiss="modal">
    <span>
      <i class="fa fa-remove"></i>
      <span>Discard</span>
    </span>
  </button>
  <button type="submit" name="submit" id="btn-oldetailsave" onclick="saveolproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
    <span>
      <i class="fa fa-check"></i>
      <span>Save</span>
    </span>
  </button>
</div>
</div>
</div>
</div>
<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="formdatetime-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Date and Time Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="dtlabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="dtsublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Set Placeholder</label>
              <input type="text" id="dtplaceholderform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Your Placeholder...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label class="col-3 col-form-label">Set to required</label>
              <div class="col-6">
                <span class="m-switch m-switch--lg m-switch--icon">
                  <label>
                    <input class="requirecheckbox" id="dtrequirecheckbox" type="checkbox" name="">
                    <span></span>
                  </label>
                </span>
              </div>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger m-btn m-btn--icon" type="button" data-dismiss="modal">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-formdtdetailssave" onclick="saveformdtproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->

<!-- Start crop image modal -->
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Change Profile Image</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body text-center">
        <div id="image-demo"></div>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-outline-warning m-btn m-btn--icon croppie-rotate" data-deg="-90"><i class="fa fa-rotate-right"></i> Right</button>
        <button class="btn btn-outline-warning m-btn m-btn--icon croppie-rotate" data-deg="90"><i class="fa fa-rotate-left"></i> Left</button>
        <button type="button" id="cropImageBtn" class="btn btn-green crop_image"><i class="fa fa-upload"></i> Crop and Upload</button>
      </div>
    </div>
  </div>
</div>
<!-- End crop image modal -->

<script src="<?php echo base_url(); ?>assets/src/custom/js/croppie-2.6.2.js" ></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript">
  var timer=null;
  var process_id=$("#hiddenprocessID").val();

  $(function(){
    var $image_crop, tempFilename, rawImg, imageId;
    displaytasks();

      //FORM VALIDATION
      $('#form_runchecklist').formValidation({
        message: 'This value is not valid',
        excluded: ':disabled',
        // live: 'disabled',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          addchecklistName: {
            validators: {
              stringLength: {
                max: 80,
                message: 'The checklist name must be less than 80 characters'
              },
              notEmpty: {
                message: 'Oops! checklist name is required!'
              },
            }
          },
        }
      }).on('success.form.fv', function (e, data) {
        e.preventDefault();
        $('#form_runchecklist').formValidation('disableSubmitButtons', true);
        runChecklist();
      });

      var mouseCopy = $.extend({}, $.ui.mouse.prototype);
      $.extend($.ui.mouse.prototype, {
        _mouseInit: function () {
          var that = this;
          if (!this.options.mouseButton) {
            this.options.mouseButton = 1;
          }

          mouseCopy._mouseInit.apply(this, arguments);

          if (this.options.mouseButton === 3) {
            this.element.bind("contextmenu." + this.widgetName, function (event) {
              if (true === $.data(event.target, that.widgetName + ".preventClickEvent")) {
                $.removeData(event.target, that.widgetName + ".preventClickEvent");
                event.stopImmediatePropagation();
                return false;
              }
              event.preventDefault();
              return false;
            });
          }

          this.started = false;
        },
        _mouseDown: function (event) {

            // we may have missed mouseup (out of window)
            (this._mouseStarted && this._mouseUp(event));

            this._mouseDownEvent = event;

            var that = this,
            btnIsLeft = (event.which === this.options.mouseButton),
                // event.target.nodeName works around a bug in IE 8 with
                // disabled inputs (#7620)
                elIsCancel = (typeof this.options.cancel === "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false);
                if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) {
                  return true;
                }

                this.mouseDelayMet = !this.options.delay;
                if (!this.mouseDelayMet) {
                  this._mouseDelayTimer = setTimeout(function () {
                    that.mouseDelayMet = true;
                  }, this.options.delay);
                }

                if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
                  this._mouseStarted = (this._mouseStart(event) !== false);
                  if (!this._mouseStarted) {
                    event.preventDefault();
                    return true;
                  }
                }

            // Click event may never have fired (Gecko & Opera)
            if (true === $.data(event.target, this.widgetName + ".preventClickEvent")) {
              $.removeData(event.target, this.widgetName + ".preventClickEvent");
            }

            // these delegates are required to keep context
            this._mouseMoveDelegate = function (event) {
              return that._mouseMove(event);
            };
            this._mouseUpDelegate = function (event) {
              return that._mouseUp(event);
            };
            $(document)
            .bind("mousemove." + this.widgetName, this._mouseMoveDelegate)
            .bind("mouseup." + this.widgetName, this._mouseUpDelegate);

            event.preventDefault();

            mouseHandled = true;
            return true;
          }
        });
    // Start CropImage
   //IMAGE UPLOAD
   var $image_crop = $('#image-demo').croppie({
    viewport: {
      width: 250,
      height: 250
    },
    boundary: {
      width: 250,
      height: 250
    },
    enforceBoundary: false,
    enableExif: true,
    enableOrientation: true
  });

   $('#upload_image').on('change', function () {
    imageId = $(this).data('id');
    tempFilename = $(this).val();
    $('#cancelCropBtn').data('id', imageId);
    readFile(this);
  });

   $('.crop_image').click(function(event){
    var taskid=$("#taskformid").val();
    countallforms(taskid);
    $image_crop.croppie('result',{
      type:'canvas',
      format: 'jpeg',
      size:'viewport'
    }).then(function(response){  
      var sequencenum=$("#sequenceid").val();
      var seqnum=0;
      if(sequencenum==0){
        seqnum=1;
      }else{
        seqnum=sequencenum;
        seqnum++;
      }
      console.log(response);
      $.ajax({
        url: "<?php echo base_url(); ?>process/display_imagefetch",
        type: "POST",
        data: {
          image:response,
          task_ID:taskid,
          sequence: seqnum
        },
        success:function(data){
          $("#cropImagePop").modal('hide');
          $("#add_content-modal").modal('hide');
          res = JSON.parse(data.trim());
          var data=res;
          var subtaskid=data.subTask_ID;   
          add_form_image(taskid,subtaskid);
          $('#item-img-output'+subtaskid).attr('src', response);
        }
      });
    })
  });

   function readFile(input) {
    if (input.files && input.files[0]) {
      var file = input.files[0];
      var fileType = file["type"];
      var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
      if ($.inArray(fileType, ValidImageTypes) < 0) {
        swal("File selected is not an image");
      } else {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('.upload-demo').addClass('ready');
          rawImg = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
        reader.onloadend = function () {
          $('#cropImagePop').modal('show');
        };
      }
    } else {
      swal("Sorry - you're browser doesn't support the FileReader API");
    }
  }
  
  $('#cropImagePop').on('shown.bs.modal', function(){
    // alert('Shown pop');
    $image_crop.croppie('bind', {
      url: rawImg
    }).then(function(){
      console.log('jQuery bind complete');
    });
  });

  //MODAL DISMISS AND RESET
$('#run_checklist-modal').on('hidden.bs.modal', function () {
    formReset('#form_runchecklist');
    $('#charNum').text("");
    $('#addchecklistName').val("");
})

});
 // End CropImage

  // Start File Upload
  $('#upload_files').on('change', function () {
    tempFilename = $(this).val();
    readFileType(this);
  });
  function readFileType(input) {
    if (input.files && input.files[0]) {
      var file = input.files[0];
      var fileTypes = file["type"];

      var ValidImageTypes = ["application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/x-zip-compressed", "application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/msword","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.presentationml.presentation"];
      if ($.inArray(fileTypes, ValidImageTypes) < 0) {
        swal("Ooops!","Only Word/Excel/PowerPoint/Zip Files are allowed to be uploaded.","error");
      }else{
        var reader = new FileReader();
        reader.onload = function (e) {
          // $('.upload-demo').addClass('ready');
          // rawImg = e.target.result;
      // alert(e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
    reader.onloadend = function () {
         // alert("OK sya");
         $("#formFilez").submit();
       };
     }
   } else {
    swal("Sorry - you're browser doesn't support the FileReader API");
  }
}

$("#formFilez").on('submit',function(e) {
  var taskformid= $("#taskformid").val();
  e.preventDefault();
  countallforms(taskformid);
  var sequencenum=$("#sequenceid").val();
  var seqnum=0;
  if(sequencenum==0){
    seqnum=1;
  }else{
    seqnum=sequencenum;
    seqnum++;
  }
  $.ajax({
   url: "<?php echo base_url(); ?>process/display_filefetch/"+taskformid+"/"+seqnum,
   type: "POST",
   data:  new FormData(this),
   contentType: false,
   cache: false,
   processData:false,
   success: function(data)
   { 
    res = JSON.parse(data.trim());
    var info=res;
    var filetype="";
    var subtaskid=info.subtask;
    var uploadfile_src=info.complabel;

    if(uploadfile_src.indexOf('.xlsx')!= -1){
      filetype="XLSX";
    }else if(uploadfile_src.indexOf('.xls')!= -1){
      filetype="XLS";
    }else if(uploadfile_src.indexOf('.zip')!= -1){
      filetype="ZIP";
    }
    else if(uploadfile_src.indexOf('.doc')!= -1){
      filetype="DOC";
    }
    else if(uploadfile_src.indexOf('.docx')!= -1){
      filetype="DOCX";
    }
    else if(uploadfile_src.indexOf('.ppt')!= -1){
      filetype="PPT";
    }
    else if(uploadfile_src.indexOf('.pptx')!= -1){
      filetype="PPTX";
    }
    else if(uploadfile_src.indexOf('.pdf')!= -1){
      filetype="PDF";
    }
    else if(uploadfile_src.indexOf('.rar')!= -1){
      filetype="RAR";
    }
    if(data!="Invalid"){
     uploadfile_src=uploadfile_src.replace("uploads/process/form_file/","");
     add_form_file(taskformid,subtaskid,filetype,uploadfile_src);
     $("#add_content-modal").modal("hide");
   }else{
     swal("Ooops!","There is an error during uploading.","error");
   }
 }         
});
});
  // End File Upload
  // Start Video Upload
  $("#video_uploader").click(function(){
   var taskformid= $("#taskformid").val();
   $("#modalVideoUpload").modal('show'); 
   $("#videoFile").val('');
   $("#videoUrl").val('');
   $("#taskid_reference").val(taskformid);
 });
    // $('#videoFile').on('change', fileUpload);
    $('.vidType').on('click', function(){
      var vidType = $("#vidType:checked"). val();
      if(vidType==1){
       $("#btnVidUpload").css("display","none");
       $("#btnVidUrl").css("display","block");
     }else{
       $("#btnVidUpload").css("display","block");
       $("#btnVidUrl").css("display","none");

     }
   });

    $('#btnVidUrl').on('click', function(){
      var url_str =  $("#videoUrl").val();
      var add = $("#taskid_reference").val();
      var taskid=add;
      var vidtype="url";
      countallforms(taskid);
      var sequencenum=$("#sequenceid").val();
      var seqnum=0;
      if(sequencenum==0){
        seqnum=1;
      }else{
        seqnum=sequencenum;
        seqnum++;
      }
      $.ajax({
       url: "<?php echo base_url(); ?>process/display_vidfetch/1/"+add+"/"+seqnum,
       type: "POST",
       data: {
        url_str:url_str
      },
      dataType: "html",
      success: function(data)
      {
        if(data>0){
          url_str = url_str.replace("watch?v=","embed/");
          var subtaskid=data;
          add_form_video(taskid,subtaskid,vidtype,url_str);
          $("#modalVideoUpload").modal("hide");
          $("#add_content-modal").modal("hide");
        }else{
         alert("Not SaveD");
       }
     }         
   });
    });

    $('#btnVidUpload').on('click', function(){
      var vidType = $("#vidType:checked"). val();
   if(vidType==2){ //if Video Upload
    var vid = $("#videoFile").val();
    var file = $("#videoFile")[0].files[0];
    if (isVideo(vid)){
         // fileUpload($("#videoFile").val());
        // console.log($("#videoFile")[0]);
        $("#formVidz").submit();
      }
      else
      {
       alert("Only video files are allowed to upload.")
     } 
   }else{
     var vid = $("#videoUrl").val();
   }
 });
    $("#formVidz").on('submit',function(e) {
      var add = $("#taskid_reference").val();
      var taskid=add;
      var vidtype="upload";
      countallforms(taskid);
      var sequencenum=$("#sequenceid").val();
      var seqnum=0;
      if(sequencenum==0){
        seqnum=1;
      }else{
        seqnum=sequencenum;
        seqnum++;
      }
      e.preventDefault();
      $.ajax({
       url: "<?php echo base_url(); ?>process/display_vidfetch/2/"+add+"/"+seqnum,
       type: "POST",
       data: new FormData(this),
       contentType: false,
       cache: false,
       processData:false,
       success: function(data)
       { 
         res = JSON.parse(data.trim());
         var info=res;
         var upload_src=info.complabel;
         var subtaskid=info.subtask;
         if(data!="invalid"){
           add_form_video(taskid,subtaskid,vidtype,upload_src);
           $("#modalVideoUpload").modal("hide");
           $("#add_content-modal").modal("hide");
         }else{
           alert("Not SaveD");
         }
       }         
     });
    });
    function isVideo(filename) {
      var ext = getExtension(filename);
      switch (ext.toLowerCase()) {
        case 'm4v':
        case 'avi':
        case 'mpg':
        case 'mp4':
        case 'mov':
        case 'mpg':
        case 'mpeg':
        // etc
        return true;
      }

      return false;
    }

    function getExtension(filename) {
      var parts = filename.split('.');
      return parts[parts.length - 1];
    }

  // End Video Upload
  

  function gettaskid(taskid){
    $.ajax({
     type: "POST",
     url: "<?php echo base_url(); ?>process/taskid_forupload",
     data: {
      task_ID: taskid
    },
    cache: false,
    success: function (res) {
     console.log('success');
   }
 }); 
  }
  function save_docu(element){
    var fd = new FormData();
    var taskid=$(element).data('id');
    var files = $('#file')[0].files[0];
    fd.append('file',files);
    gettaskid(taskid);

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/do_upload",
      data: fd,
      contentType: false,
      processData: false,
      success: function(response){
        if(response != 0){
          $("#img").attr("src",response); 
                    $(".preview img").show(); // Display image element
                    console.log('SUCCESS~');
                  }else{
                    alert('file not uploaded');
                  }
                },
              });
  }

  function saveformproperties_changes(element){
    var taskid=$(element).data('id');
    var subtaid=$("#subtaskformid").val();
    var labelcontent=$("#labelform").val();
    var sublabelcontent=$("#sublabelform").val();
    var placeholdercontent=$("#placeholderform").val();
    var max=$("#textmax").val();
    var min=$("#textmin").val();
    var require="";
    if ($("#requirecheckbox").is(':checked'))
    {
      require="yes";
    }
    else
    {
      require="no";
    }
    $.ajax({
     type: "POST",
     url: "<?php echo base_url(); ?>process/update_inputformdetails",
     data: {
      subTask_ID: subtaid,
      complabel: labelcontent,
      sublabel: sublabelcontent,
      placeholder: placeholdercontent,
      required: require,
      min:min,
      max:max
    },
    cache: false,
    success: function (res) {
     var result = JSON.parse(res.trim());
     display_allcomponents(taskid);
     swal({
      position: 'center',
      type: 'success',
      title: 'Successfully Updated Properties!',
      showConfirmButton: false,
      timer: 1500
    });
     $("#formproperties-modal").modal('hide');
   }
 }); 
  }
  function savehtproperties_changes(element){
    var taskid=$(element).data('id');
    var subtaid=$("#subtaskformid").val();
    var htlabelform=$("#ht_labelform").val();
    var htsublabelform=$("#ht_sublabelform").val();
    var textalignment=$("#textalignment").val();
    var textsize=$("#textsize").val();
    $.ajax({
     type: "POST",
     url: "<?php echo base_url(); ?>process/update_headingtextdetails",
     data: {
      subTask_ID: subtaid,
      complabel: htlabelform,
      sublabel: htsublabelform,
      text_alignment: textalignment,
      text_size: textsize
    },
    cache: false,
    success: function (res) {
     var result = JSON.parse(res.trim());
     display_allcomponents(taskid);
     swal({
      position: 'center',
      type: 'success',
      title: 'Successfully Updated Properties!',
      showConfirmButton: false,
      timer: 1500
    });
     $("#formtextheading-modal").modal('hide');
   }
 }); 
  }
  function saveolproperties_changes(element){
   var taskid=$(element).data('id');
   var subtaid=$("#subtaskformid").val();
   var labelcontent=$("#optionlabelform").val();
   var sublabelcontent=$("#optionsublabelform").val();
   var require="";
   if ($("#optionrequirecheckbox").is(':checked'))
   {
    require="yes";
  }
  else
  {
    require="no";
  }
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/update_optiondetails",
   data: {
    subTask_ID: subtaid,
    complabel: labelcontent,
    sublabel: sublabelcontent,
    required: require,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   display_allcomponents(taskid);
   swal({
    position: 'center',
    type: 'success',
    title: 'Successfully Updated Properties!',
    showConfirmButton: false,
    timer: 1500
  });
   $("#formoptionlist-modal").modal('hide');
 }
}); 
}
function saveformdtproperties_changes(element){
 var taskid=$(element).data('id');
 var subtaid=$("#subtaskformid").val();
 var labelcontent=$("#dtlabelform").val();
 var sublabelcontent=$("#dtsublabelform").val();
 var placeholdercontent=$("#dtplaceholderform").val();
 var require="";
 if ($("#dtrequirecheckbox").is(':checked'))
 {
  require="yes";
}
else
{
  require="no";
}
$.ajax({
 type: "POST",
 url: "<?php echo base_url(); ?>process/update_datetimedetails",
 data: {
  subTask_ID: subtaid,
  complabel: labelcontent,
  sublabel: sublabelcontent,
  placeholder: placeholdercontent,
  required: require
},
cache: false,
success: function (res) {
 var result = JSON.parse(res.trim());
 display_allcomponents(taskid);
 swal({
  position: 'center',
  type: 'success',
  title: 'Successfully Updated Properties!',
  showConfirmButton: false,
  timer: 1500
});
 $("#formdatetime-modal").modal('hide');
}
}); 
}


//START ADDING FORM FOR IMAGE
function add_form_image(taskid,subtaskid){
  var stringx="";
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";

  stringx+="<div id='uploaded_image' class='col-12'>";
  stringx+="<figure style='margin:0 !important;text-align:center;padding-top:25px'>"
  stringx+="<img src='' class='gambar img-responsiv ' id='item-img-output"+subtaskid+"' style='background:white;width:85%!important;object-fit:cover;'/>"
  stringx+="</figure>"
  stringx+="</div></div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button type='button' data-id='"+subtaskid+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--airbtn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2'>";
  stringx+="<i class='fa fa-upload'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  $("#formtag"+taskid).append(stringx);
}
//END ADDING FORM FOR IMAGE

//START ADDING FORM FOR VIDEO
function add_form_video(taskid,subtaskid,videotype,video_src){
  var stringx="";
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row iframepadding'>";
  stringx+="<div class='embed-responsive embed-responsive-16by9'>";
  if(videotype=="url"){
    stringx+="<iframe id='item-vid-output"+subtaskid+"' src='"+video_src+"' class='embed-responsive-item' frameborder='0' allowfullscreen ></iframe>";
  }else{
   stringx+= "<video width='320' height='240' controls>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mp4'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/m4v'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/avi'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mpg'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mov'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mpg'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mpeg'>";
   stringx+= "</video>";
 }
 stringx+="</div></div>";
 stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";

 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button type='button' data-id='"+subtaskid+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2'>";
 stringx+="<i class='fa fa-upload'></i>";
 stringx+="</button>";
 stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
 $("#formtag"+taskid).append(stringx);
}
//END ADDING FORM FOR VIDEO

function add_form_file(taskid,subtaskid,formtype,uploadfile_src){
  var stringx="";
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";

  stringx+="<div class='col-12 text-center'>";
  stringx+="<button href='#' class='btn btn-outline-info btn-lg  m-btn m-btn--icon m-btn--outline-2x'>";
  stringx+="<span>";
  stringx+="<i class='fa fa-file-text-o'></i>";
  stringx+="<span>"+formtype+"</span>";
  stringx+="</span>";
  stringx+="</button>";
  stringx+="<br><div style='font-size:1.1em;color:#7e61b1'>"+uploadfile_src+"</div>";
  stringx+="<button class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' disabled>";
  stringx+="<span><i class='fa fa-download'></i><span>Download this file</span></span>";
  stringx+="</button>";

  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  
  stringx+="</div>";
  stringx+="<div class='input-group-append formdistance style='padding-left:15px;'>";
  stringx+="<button type='button' data-id='' onclick='' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2'><i class='fa fa-download'></i></button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  $("#formtag"+taskid).append(stringx);
}
//START ADDING FORM COMPONENTS
function add_component(componentType){
  var taskid=$("#taskformid").val();
  countallforms(taskid);
  var sequencenum=$("#sequenceid").val();
  var seqnum=0;
  if(sequencenum==0){
    seqnum=1;
  }else{
    seqnum=sequencenum;
    // seqnum++;
  }
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/add_subtask",
   data: {
    component_ID: componentType,
    task_ID: taskid,
    sequence: seqnum
  },
  cache: false,
  success: function (res) {
    $("#add_content-modal").modal('hide');
    display_allcomponents(taskid);
  }
});
}
//END ADDING FORM COMPONENTS
function countallforms(taskid){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/fetch_allsubtask_count",
  data: {
    task_ID:taskid
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   // var countform=Object.keys(result).length;
   
   var countform= (result[0].max!=null) ? parseInt(result[0].max) + 1 : 1;
   // alert(countform);
   $("#sequenceid").val(countform);
 }
});
}

//checklist name
function defaultchecklist_name(){
  var check_defaultname=$("#default_addchecklistName").val();
  $("#addchecklistName").val(check_defaultname);
}

function display_allcomponents(taskid){
  $('#contentdiv').data('activetaskid', taskid)
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_allsubtask",
    data: {
      task_ID:taskid
    },
    cache: false,
    success: function (res) {
     var result =JSON.parse(res.trim());
     var stringx ="";

     $.each(result, function (key, item) {
      var sequence_button="";
     /* if (key == result.length-1) { //last na subtask
        sequence_button+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' class='btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-chevron-up'></i></button>";

      } else if (key== 0) {//first na  subtask
        sequence_button+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' class='btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-chevron-down'></i></button>";
      } else{
        sequence_button+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' class='btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-chevron-up'></i></button>";
        sequence_button+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' class='btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-chevron-down'></i></button>";
      }*/
      if(item.component_ID==1){
        stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"'class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label editable' onkeyup='autosavelabel(this)'>";
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Type a label";
        }
        stringx+="</div>";
        if(item.placeholder){
          stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='"+item.placeholder+"' disabled>";
        }else{
          stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='Something will be typed here...' disabled>";
        }
        if(item.sublabel){
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
        }else{
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
        }
        stringx+="<div class='input-group-append formdistance'>";
        stringx+=sequence_button;
        stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
        stringx+="<i class='fa fa-edit'></i>";
        stringx+="</button>";
        stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-close'></i>";
        stringx+="</button>";
        stringx+="</div>";
        stringx+="</div>";
      }
      else if(item.component_ID==2){
        stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label editable' onkeyup='autosavelabel(this)'>";
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Type a label";
        }
        stringx+="</div>";
        stringx+="<form>";

        if(item.placeholder){
         stringx+="<textarea class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled></textarea>";
       }else{
         stringx+="<textarea class='form-control m-input m-input--air m-input--pill' placeholder='Long text will be typed here...' disabled></textarea>";
       }
       stringx+="</form>";
       if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
      }
      stringx+="<div class='input-group-append formdistance'>";
      stringx+=sequence_button;
      stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formproperties-modal' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' onclick='fetch_inputtypeformdetails(this)'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    //HEADING
    else if(item.component_ID==3){
      stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='labelheading editable'>";
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Heading";
      }
      stringx+="</div>";
      if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
      }
      stringx+="<div class='input-group-append formdistance'>";
      stringx+=sequence_button;
      stringx+="<button type='button' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    //TEXT
    else if(item.component_ID==4){
      stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>"; 
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Click to edit this text...";
      }
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+=sequence_button;
      stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    else if(item.component_ID==5){
     stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
     stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
     if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Number label";
    }
    stringx+="</div>";
    if(item.placeholder){
      stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled>";
    }else{
      stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='A number or numbers will be typed here...' disabled>";
    }
    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
    }
    stringx+="<div class='input-group-append formdistance'>";
    stringx+=sequence_button;
    stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(item.component_ID==6){
    stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance editable'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
    }
    stringx+="<div id='radiolist"+item.subTask_ID+"' class='row m-radio-list formdistance'>";
    stringx+="</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
    stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Option</label>";
    stringx+="<a>";
    stringx+="</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+=sequence_button;
    stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
    radio_option(item.subTask_ID);
  }
  else if(item.component_ID==7){
    stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
    }
    stringx+="<div class='form-group m-form__group'>";
    stringx+="<select class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1' disabled>";
    stringx+="<option></option>";
    stringx+="</select>";
    stringx+="</div>";
    stringx+="<div class='form-group m-form__group formdistance'>";
    stringx+="<ul id='itemlist"+item.subTask_ID+"' class='list-group'>";
    stringx+="</ul>";
    stringx+="</div>";

    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
    stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add List</label>";
    stringx+="</a>";
    stringx+="</div>";

    stringx+="<div class='input-group-append formdistance'>";
    stringx+=sequence_button;
    stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
    dropdown_list(item.subTask_ID);
  }
  else if(item.component_ID==8){
    stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
    stringx+="<div id='checkoption"+item.subTask_ID+"' class='row m-checkbox-list formdistance'>";
    stringx+="</div>";

    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
    stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Checkbox</label>";
    stringx+="<a>";
    stringx+="</div>";
    stringx+="<div class='input-group-append formdistance'>";
    stringx+=sequence_button;
    stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
    checkbox_option(item.subTask_ID);
  }
  else if(item.component_ID==9){
   stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 distance'>";
   stringx+="<hr>";
   stringx+="<div class='input-group-append formdistance'>";
   stringx+=sequence_button;
   stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
   stringx+="<i class='fa fa-close'></i>";
   stringx+="</button>";
   stringx+="</div>";
   stringx+="</div>";
 }
 else if(item.component_ID==10){
   stringx+="<div id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
   stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
   if(item.complabel){
    stringx+=item.complabel;
  }else{
    stringx+="Type Date label";
  }
  stringx+="</div>";
  stringx+="<div class='input-group formdistance'>";

  if(item.placeholder){
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  }else{
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  }    
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+=sequence_button;
  stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==11){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
  if(item.complabel){
    stringx+=item.complabel;
  }else{
    stringx+="Type a label";
  }
  stringx+="</div>";
  stringx+="<input class='form-control m-input m-input--air m-input--pill formdistance' type='number' value='0' min='0' max='100' step='1'/>";

  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+=sequence_button;
  stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==12){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
  if(item.complabel){
    stringx+=item.complabel;
  }else{
    stringx+="Email Label";
  }
  stringx+="</div>";

  if(item.placeholder){
   stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled>";
 }else{
  stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='An email will be typed here...' disabled>";
}
if(item.sublabel){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
}else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
}
stringx+="<div class='input-group-append formdistance'>";
stringx+=sequence_button;
stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==13){
 stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="Type Time Label";
}
stringx+="</div>";
stringx+="<div class='input-group formdistance'>"
if(item.placeholder){
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
}else{
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
} 
stringx+="<div class='input-group-append'>";
stringx+="<span class='input-group-text'>";
stringx+="<i class='la la-clock-o'></i>";
stringx+="</span>";
stringx+="</div>";
stringx+="</div>";
if(item.sublabel){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
}else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
}
stringx+="<div class='input-group-append formdistance'>";
stringx+=sequence_button;
stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==14){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
  if(item.complabel){
    stringx+=item.complabel;
  }else{
    stringx+="Type Date and Time Label";
  }
  stringx+="</div>";
  stringx+="<div class='input-group formdistance'>";
  if(item.placeholder){
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  }else{
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date and time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  } 
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";
  if(item.sublabel){
   stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
 }else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a Sub label...</div>";
}
stringx+="<div class='input-group-append formdistance'>";
stringx+=sequence_button;
stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==15){
 stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div class='row'>";
 stringx+="<div class='col-12  text-center'>";
 stringx+="<button class='btn btn-outline-success m-btn m-btn--icon' disabled>";
 stringx+="<span>";
 stringx+="<i class='fa fa-upload'></i>";
 stringx+="<span>File will be uploaded here</span>";
 stringx+='</span>';
 stringx+='</button>';
 if(item.sublabel){
   stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:1em;color:#6a6c71'>"+item.sublabel+"</div>";
 }else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:1em;color:#6a6c71'> Type a description...</div><br>";
}
stringx+="</div></div>";
stringx+="<div class='input-group-append formdistance'>";
stringx+=sequence_button;
stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==16){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";

  stringx+="<div id='uploaded_image' class='col-12'>";
  stringx+="<figure style='margin:0 !important;text-align:center;padding-top:25px'>"
  stringx+="<img src='<?php echo base_url()?>"+item.complabel+"' class='gambar img-responsiv ' id='item-img-output"+item.subTask_ID+"' style='background:white;width:85%!important;object-fit:cover;'/>"
  stringx+="</figure>"
  stringx+="</div></div>";
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+=sequence_button;
  stringx+="<button type='button' data-id='"+item.subTask_ID+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2'>";
  stringx+="<i class='fa fa-upload'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==17){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row iframepadding'>";
  stringx+="<div class='embed-responsive embed-responsive-16by9'>";

  if(item.complabel.indexOf('uploads/process/form_videos/')!= -1 ){
    stringx+= "<video width='320' height='240' controls>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mp4'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/m4v'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/avi'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mpg'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mov'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mpg'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mpeg'>";
    stringx+= "</video>";
  }else{
    stringx+=item.complabel;
  }
  stringx+="</div></div>";
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+=sequence_button;
  stringx+="<button type='button' data-id='"+item.subTask_ID+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2'>";
  stringx+="<i class='fa fa-upload'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==18){
  var complabel=item.complabel;
  var complabel_name=item.complabel;

  complabel_name=complabel_name.replace("uploads/process/form_file/","");

  if(complabel.indexOf('.xlsx')!= -1){
    complabel="XLSX";
  }else if(complabel.indexOf('.xls')!= -1){
    complabel="XLS";
  }else if(complabel.indexOf('.zip')!= -1){
    complabel="ZIP";
  }
  else if(complabel.indexOf('.doc')!= -1){
    complabel="DOC";
  }
  else if(complabel.indexOf('.docx')!= -1){
    complabel="DOCX";
  }
  else if(complabel.indexOf('.ppt')!= -1){
    complabel="PPT";
  }
  else if(complabel.indexOf('.pptx')!= -1){
    complabel="PPTX";
  }
  else if(complabel.indexOf('.pdf')!= -1){
    complabel="PDF";
  }
  else if(complabel.indexOf('.rar')!= -1){
    complabel="RAR";
  }

  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";

  stringx+="<div class='col-12 text-center'>";
  stringx+="<button href='#' class='btn btn-outline-info btn-lg  m-btn m-btn--icon m-btn--outline-2x'>";
  stringx+="<span>";
  stringx+="<i class='fa fa-file-text-o'></i>";
  stringx+="<span>"+complabel+"</span>";
  stringx+="</span>";
  stringx+="</button>";
  stringx+="<br><div style='font-size:1.1em;color:#7e61b1'>"+complabel_name+"</div>";
  stringx+="<button class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' disabled>";
  stringx+="<span><i class='fa fa-download'></i><span>Download this file</span></span>";
  stringx+="</button>";
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  }
  stringx+="</div>";
  stringx+="<div class='input-group-append formdistance' style='padding-left:15px;'>";
  stringx+=sequence_button;
  stringx+="<button type='button' data-id='' onclick='' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-download'></i></button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</div>";
  stringx+="</div>";
  stringx+="</div>";

}
});
$("#add_content-modal").modal('hide');
$("#formtag"+taskid).html(stringx);
$("#formtag"+taskid).sortable({
  cancel: '.ui-state-default .editable',
  mouseButton: 3,
  stop: function( event, ui ) {
    reorder_update_subtask(ui);
  }
});
// $("#formtag"+taskid).disableSelection();
$('.ui-sortable').data('taskid', taskid);
}
});
}
function upload_file(element){
 var subtaskid=$(element).data('id');
 var browsed_file=$("#file-upload"+subtaskid).val();
 console.log(browsed_file);
}
//fetch input and textarea
function fetch_inputtypeformdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      console.log(data.required);
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#labelform").val(data.complabel);
      $("#sublabelform").val(data.sublabel);
      $("#placeholderform").val(data.placeholder);
      $("#textmin").val(data.min);
      $("#textmax").val(data.max);
      $("#btn-formdetailssave").data('id',data.task_ID);
    }
  });
}
//fetch text and heading
function fetch_headertextdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      console.log('header:'+subtid);
      $("#ht_labelform").val(data.complabel);
      if(compid==3){
        $("#ht_sublabelform").val(data.sublabel);
      }else if(compid==4){
        $("#sublab").hide();
      }
      $("#textalignment").val(data.text_alignment).prop('selected', true);
      $("#textsize").val(data.text_size).prop('selected', true);
      $("#btn-htdetailsave").data('id',data.task_ID);
    }
  });
}
//fetch radio button, checkbox and dropdown
function fetch_optionlistdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#optionlabelform").val(data.complabel);
      $("#optionsublabelform").val(data.sublabel);   
      $("#btn-oldetailsave").data('id',data.task_ID);
    }
  });
}
//fetch date and time
function fetch_datetimeformdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      console.log(data.required);
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#dtlabelform").val(data.complabel);
      $("#dtsublabelform").val(data.sublabel);
      $("#dtplaceholderform").val(data.placeholder);
      $("#btn-formdtdetailssave").data('id',data.task_ID);
    }
  });
}
//RADIO, MULTIPLE, DROPDOWN
function radio_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-radio m-radio--bold m-radio--state-primary editable' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Double click and type option...";
   }
   stringcomp+="<input type='radio' name='formrad' value='' disabled>";
   stringcomp+="<span></span>";
   stringcomp+="</label>";
   stringcomp+="</div>";
   stringcomp+="<div class='col-lg-2'>";
   stringcomp+="<a href='#' data-componentid='6' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)' ><i class='fa fa-remove'></i></a>";
   stringcomp+="</div>";
 });
   $("#radiolist"+subtaskid).html(stringcomp);
 }
});
}
function checkbox_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-checkbox m-checkbox--bold m-checkbox--state-brand editable' contenteditable='true'>";
    stringcomp+="<input type='checkbox' disabled>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
    stringcomp+="Type Checklist Label...";
  }
  stringcomp+="<span></span>";
  stringcomp+="</label>";
  stringcomp+="</div>";
  stringcomp+="<div class='col-lg-2'>";
  stringcomp+="<a href='#' data-componentid='8' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='fa fa-remove'></i></a>";
  stringcomp+="</div>";
});
   $("#checkoption"+subtaskid).html(stringcomp);
 }
});
}
function dropdown_list(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<li class='list-group-item justify-content-between'>";
    stringcomp+="<span class='editable' id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Item list...";
   }
   stringcomp+="</span>";
   stringcomp+="<a href='#' data-componentid='7' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='float-right fa fa-remove'></i></a>";
   stringcomp+="</li>";
 });
   $("#itemlist"+subtaskid).html(stringcomp);
 }
});
}
//END
//Add option
function add_componentoption(element){
  var subtaskid=$(element).data('id');
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/add_subtaskcomponent",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var res = JSON.parse(res.trim());
   var data = res;
   var component=data.component_ID;
   console.log('success');
   console.log(component);
   if(component==6){
    radio_option(subtaskid);
  }
  else if(component==7){
    dropdown_list(subtaskid);
  }
  else if(component==8){
    checkbox_option(subtaskid);
  }
}
});
}
function delete_option(element){
  var compsubtaskid=$(element).data('id');
  var subtask=$(element).data('subtaskid');
  var componentid=$(element).data('componentid');
  $.ajax({
    type: "POST",
    url:"<?php echo base_url(); ?>process/delete_option",
    data: {
      componentSubtask_ID: compsubtaskid,
    },
    cache: false,
    success: function () {
      if(componentid=='6'){
        radio_option(subtask);
      }
      else if(componentid=='7'){
        dropdown_list(subtask);
      }
      else if(componentid=='8'){
        checkbox_option(subtask);
      }
    }
  });
}
function delete_component(element){
  var subtaskid=$(element).data('id');
  var task_id=$(element).data('compid');
  swal({
    title: 'Are you sure you want to delete this form element?',
    text: "",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url:"<?php echo base_url(); ?>process/delete_component",
        data: {
          subTask_ID: subtaskid,
          task_id: task_id,
        },
        cache: false,
        success: function () {
         display_allcomponents(task_id);
         swal(
          'Deleted!',
          'Successfully deleted.',
          'success'
          )
       },
       error: function (res){
        swal(
          'Oops!',
          'Please Check!',
          'error'
          )
      }
    });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deletion has been cancelled',
        'error'
        )
    }
  });
}
function displaytasks(){
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/display_tasks",
   data: {
    process_ID: process_id,
  },
  beforeSend:function(){
    $('.loading-icon').show();
  },
  cache: false,
  success: function (res) {
    $('.loading-icon').hide();
    var result = JSON.parse(res.trim());
    var stringx = "";
    var stringcon= "";
    $.each(result, function (key, item) {
     stringx += "<div class='input-group m-form__group sortable' id='addcheckbox'>";
     stringx +="<div class='input-group-prepend'>";
     stringx +="<span class='input-group-text'>";
     stringx +="<label class=''>";
     stringx +="<span></span>";
     stringx +="</label>";
     stringx +="</span>";
     stringx +="</div>";
     if(item.taskTitle==null){
       stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value=''>"; 
     }else{
      stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask sampletask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value='"+item.taskTitle+"'>";
    } 
    stringx +="<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='"+item.process_ID+"'>";
    stringx +="<input type='hidden' id='htaskID' name='htaskID' value='"+item.task_ID+"'>";
    stringx +="<div class='input-group-append'>";
    stringx +="<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
    stringx +="<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
    stringx +="<i class='fa fa-delete'></i>";
    stringx +="</a>";
    stringx +="<div class='m-dropdown__wrapper'>";
    stringx +="<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
    stringx +="<div class='m-dropdown__inner'>";
    stringx +="<div class='m-dropdown__body'> ";             
    stringx +="<div class='m-dropdown__content'>";
    stringx +="<ul class='m-nav'>";
    stringx +="<li class='m-nav__item'>";
    stringx +="<a data-id='"+item.task_ID+"' onclick='deleteTask(this)' class='m-nav__link'>";
    stringx +="<i class='m-nav__link-icon fa fa-trash'></i>";
    stringx +="<span class='m-nav__link-text'>Delete</span>";
    stringx +="</a>";
    stringx +="</li>";
    stringx +="<li class='m-nav__item'>";
    stringx +="<a href='#' class='m-nav__link'>";
    stringx +="<i class='m-nav__link-icon fa fa-angle-double-right'></i>";
    stringx +="<span class='m-nav__link-text'>Task details</span>";
    stringx +="</a>";
    stringx +="</li>";
    stringx +="</ul>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";

    stringcon +="<div id='formcontent"+item.task_ID+"'class='m-portlet__body formcontent' style='height:480px;overflow-y:scroll; overflow-x:auto'>";
    // stringcon +="<div id='formaddcontent"+item.task_ID+"' class='row droparea_append'>";
    stringcon +="<form id='formtag"+item.task_ID+"' method='post'>";
    stringcon +="</form>";
    // stringcon +="</div>";
    stringcon += "<div class='row droparea_outside'>";
    stringcon += "<div class='col-lg-12 droparea' >";
    stringcon += "<a href='#' data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='taskid(this)' class='btn m-btn m-btn--icon m-btn--pill m-btn--air' data-toggle='modal' data-target='#add_content-modal'>";
    stringcon += "<span>";
    stringcon += "<i class='fa fa-plus-circle'></i>";
    stringcon += "<span>Add Content</span>";
    stringcon += "</span>";
    stringcon += "</a>";
    stringcon += "</div>";
    stringcon += "</div>";
    stringcon += "</div>";
  });

    $("#checkgroup").html(stringx);
    $(".inputtask:first").focus();
     $(".m-dropdown.m-dropdown--inline.m-dropdown--arrow.m-dropdown--align-right:first").hide();
    $(".inputtask:first").css({"background-color":"#51aff9","color":"white"});
    $(".inputtask:first").addClass("placeholdercolor");
    var firstaskid=$(".inputtask:first").attr('id');
    display_allcomponents(firstaskid);
    $("#contentdiv").append(stringcon);
    $(".formcontent").hide();
    $(".formcontent:first").show();
  }
});
}
function displayformdiv(element){
  var taskid=$(element).data('id');
  display_allcomponents(taskid);
  $(".formcontent").hide();
  $("#formcontent"+taskid).show();
  $(".inputtask").css({"background-color":"","color":""});
  $(".inputtask").removeClass("placeholdercolor");
  $("input#"+taskid).css({"background-color":"#51aff9","color":"white"});
  $("input#"+taskid).addClass("placeholdercolor");
}

function taskid(element){
  var taskid=$(element).data('id');
  $("#taskformid").val(taskid);
  countallforms(taskid);
}
//NOTIFS
function doNotif() {
 $.notify("Auto-saved!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}
function doNotif2() {
 $.notify("Saved!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}

//CHARACTER COUNTER
$('#addchecklistName').keyup(function () {
  var max = 80;
  var len = $(this).val().length;
  if (len >= max) {
    $('#charNum').text('You have reached the limit |');
  } else {
    var char = max - len;
    $('#charNum').text(char+'/80 characters remaining');
  }
});
function runChecklist(){
  var checklistTitle = $("#addchecklistName").val();
  var process_ID = $("#fetchprocessid").val();
  var check_ID = $("#hiddencheckid").val();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_checklistdetails",
    data: {
      checklist_ID: check_ID,
      checklistTitle:checklistTitle,
      process_ID:process_ID
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      swal({
        position: 'center',
        type: 'success',
        title: 'Checklist On the run!',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.href = "<?php echo base_url('process/checklist_pending')?>"+"/"+process_ID;
    }
  });
}
//autosave function
function autosaveoptionlabel(element){
  var compsubtaskid=$(element).data('id');
  var optionlabel=$("#optiondetails"+compsubtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_optiondetails",
    data: {
      componentSubtask_ID: compsubtaskid,
      compcontent: optionlabel
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavesublabel(element){
 var subtaskid=$(element).data('id');
 var sublabel=$("#sublabel"+subtaskid).text();
 console.log(subtaskid);
 console.log(sublabel);
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_sublabeldetails",
  data: {
    subTask_ID: subtaskid,
    sublabel: sublabel
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000)
  },
  error: function (res){
    console.log(res);
  }
});
}
function autosavelabel(element){
  var subtaskid=$(element).data('id');
  var label=$("#label"+subtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_subtaskdetails",
    data: {
      subTask_ID: subtaskid,
      complabel: label
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavetask(element){
 var asid=$(element).data('id');
 console.log(asid);
 var taskTitle = $("input#"+asid).val();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_taskdetails",
  data: {
    task_ID: asid,
    taskTitle:taskTitle
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000)
  },
  error: function (res){
    console.log(res);
  }
});
}
function addtask(element){
  var processID=$(element).data('id');
  console.log(processID);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_onetask",
    data: {
      process_ID: $(element).data('id')
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      displaytasks();
      // window.location.href = "<?php echo base_url('process/checklist')?>"+"/"+processID;
    },
    error: function (res){
      console.log(res);
    }
  });
}
function deleteTask(element){
  swal({
    title: 'Are you sure?',
    text: "All under this task will be deleted, too.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_task",
        data: {
          task_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
         displaytasks();
         swal(
          'Deleted!',
          'Task successfully deleted.',
          'success'
          )
       },
       error: function (res){
        swal(
          'Oops!',
          'You cannot delete this task ',
          'error'
          )
      }
    });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a process has been cancelled',
        'error'
        )
    }
  });
}
function update_subtask_order(prevVal, currentVal, nextVal, subtaskIdVal){
  var prev = prevVal;
  var next = nextVal;
  if(prevVal == undefined){
    prev = 0;
  }
  if(nextVal == undefined){
    next = 0
  }
  $.when(fetchPostData({
    taskId: $('#contentdiv').data('activetaskid'),
    subtask_id: subtaskIdVal,
    prevOrder: prev,
    currentOrder: currentVal,
    nextOrder: next
  }, '/process/update_subtask_sequence')).then(function (order) {
    var orderObj = $.parseJSON(order.trim());
    console.log(orderObj);
  })
}

function reorder_update_subtask(ui){
  var subtask_id =  $(ui.item).attr('id');
  var current =  $(ui.item).data('order');
  var prev =  $($(ui.item).prev()).data('order');
  var next = $($(ui.item).next()).data('order');
  var subtasksDiv = $('.ui-sortable-handle');

  update_subtask_order(prev, current, next, subtask_id); 
  
  if(prev == undefined && next != undefined){
    // increment all orders less than to current
    // move as first order data
    console.log("move as first order data");
    $.each(subtasksDiv, function(index, value){
      if(($(value).data('order') < current) && ($(value).data('order') >= 1)){
        $(value).data('order', $(value).data('order')+1);
        console.log($(value).data('order'));
      }
    })
    $(ui.item).data('order', 1);
    console.log("NEW ORDERING");
    console.log($($(ui.item).prev()).data('order'));
    console.log($(ui.item).data('order'));
    console.log($($(ui.item).next()).data('order'));
  }else if(prev != undefined && next == undefined)
  {
    // decrement order greater than from current and less than of equal to prev
    // change order to prev plus 
    $.each(subtasksDiv, function(index, value){
      if(($(value).data('order') > current) && ($(value).data('order') <= prev)){
        $(value).data('order', $(value).data('order')-1);
        console.log($(value).data('order'));
      }
    })
    $(ui.item).data('order', prev);
    console.log("NEW ORDERING");
    console.log($($(ui.item).prev()).data('order'));
    console.log($(ui.item).data('order'));
    console.log($($(ui.item).next()).data('order'));
  }
  else
  {
    if((prev <current) && (next < current)){

      console.log("change to next");
      // increment next orders and less than to current
      $.each(subtasksDiv, function(index, value){
        if(($(value).data('order') >=next) && ($(value).data('order') < current)){
          $(value).data('order', $(value).data('order')+1);
          console.log($(value).data('order'));
        }
      });
      $(ui.item).data('order', next);
      console.log("NEW ORDERING");
      console.log($($(ui.item).prev()).data('order'));
      console.log($(ui.item).data('order'));
      console.log($($(ui.item).next()).data('order'));
    }else if((prev > current) && (next > current)){
      console("change to previous");
      // change to prev
      // decrement prev orders and greater than to current
      $.each(subtasksDiv, function(index, value){
        if(($(value).data('order') > current) && ($(value).data('order') <= prev)){
          $(value).data('order', $(value).data('order')-1);
          console.log($(value).data('order'));
        }
      });
      $(ui.item).data('order', prev);
      console.log("NEW ORDERING");
      console.log($($(ui.item).prev()).data('order'));
      console.log($(ui.item).data('order'));
      console.log($($(ui.item).next()).data('order'));
    }
  }
  console.log("subtask id: "+subtask_id);
  console.log("PREV: "+prev);
  console.log("current: "+current);
  console.log("nex: "+next);
  console.log("reorder");
}
</script>



