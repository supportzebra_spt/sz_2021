<?php if(!empty($checklist)){ foreach ($checklist as $ch) {?>
	<div class="checkcontent col-lg-4 chli<?php echo $ch->status_ID;?>">
		<?php 
		$countChecklist=0;
		$countCompleted=0;
		foreach ($checklist_status as $chs) {
			if($chs->checklist_ID==$ch->checklist_ID){
				$countChecklist++;
				if($chs->isCompleted=="3"){
					$countCompleted++;
				}
			}
		}
		$divide=$countCompleted/$countChecklist;
		$result=$divide*100;
		$finalcal=round($result);
		?> 
		<div class="m-portlet m-portlet--collapsed m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_7">
			<div class="m-portlet__head border-accent rounded" style="background-color:#91e4b5">
				<!--  <div class="row" style="background-color: #86b4d8">hi</div> -->
				<div class="row" style="margin-top:1em">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon">
								<?php if($countCompleted==$countChecklist){?>
									<i class="fa fa-check coloricons"></i>
								<?php }else{?>
									<i class="fa fa-clock-o coloricons"></i>
								<?php }?>       
							</span>
							<a style="display:block;text-overflow: ellipsis;width: 250px;overflow: hidden; white-space: nowrap;" href="<?php echo site_url('process/checklist_answerable').'/'.$processinfo->process_ID.'/'.$ch->checklist_ID;?>" class="checkName m-portlet__head-text">
								<?php echo $ch->checklistTitle;?>
							</a>
							<input type="hidden" name="chid" id="chID" value="<?php echo $ch->checklist_ID;?>">                                       
						</div>          
					</div>
				</div>
				<span style="font-size: 0.9em;margin-left:1em">
					<?php 
					$datetime=$ch->dateTimeCreated;
					$date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
					echo sprintf($date->format("M j, Y"))."&nbsp; at &nbsp;";
					echo sprintf($date->format("g:ia"));
					?>
				</span><br>
				<span style="font-size: 0.9em;margin-left:1em">
					<span class="m-badge m-badge--accent m-badge--wide">CH</span>
					<span class="m-badge m-badge--accent m-badge--wide">SB</span>
				</span>
				<div class="row" style="margin-top:1em;margin-bottom: 1em;">
					<div class=" col-md-12">
						<?php if($countCompleted<$countChecklist){?>
							<div class="progress">
								<div class="progress-bar progress-bar-striped progress-bar-animated  bg-success" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $finalcal;?>%" data-toggle="m-tooltip" data-placement="top" title data-original-title="<?php echo $countCompleted;echo "/";echo $countChecklist;?> tasks completed"></div>
							</div>
						<?php }else if($countCompleted==$countChecklist){?>
							<div class="progress">
								<div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $finalcal;?>%" data-toggle="m-tooltip" data-placement="top" title data-original-title="Completed" aria-valuenow="0" aria-valuemin="2" aria-valuemax="5"></div>
							</div>
						<?php }?>
					</div>
				</div>
				<div class="row" style="margin-top:1em;margin-bottom: 1em;">
					&nbsp;&nbsp;
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav" >
							<li class="m-portlet__nav-item">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon" id="dropdownOption" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i 
									class="la la-cog coloricons"></i></a>
									<div class="dropdown-menu" aria-labelledby="dropdownOption" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">
										<button class="dropdown-item" type="button" data-id="<?php echo $ch->checklist_ID; ?>" onclick="updateChecklistFetch(this)" data-toggle="modal" data-target="#updateChecklistmodal"><i class="flaticon-file"></i>&nbsp;Edit checklist name</button>
										<button class="dropdown-item" type="button"><i class="flaticon-file">
										</i>&nbsp;Assign this checklist</button>
									</div> 
								</li>
								<li class="m-portlet__nav-item">
									<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i data-toggle="m-tooltip" data-placement="top" title data-original-title="Info" class="la la-info-circle coloricons"></i></a>  
								</li>
								<li class="m-portlet__nav-item">
									<a href="#" data-portlet-tool="#" class="m-portlet__nav-link m-portlet__nav-link--icon"><i data-toggle="m-tooltip" data-placement="top" title data-original-title="Delete" class="fa fa-trash-o coloricons"></i></a>    
								</li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<?php if($ch->dueDate!=NULL){?>
									<li class="m-portlet__nav-item">
										<span class="m-menu__link-badge">
											<?php
											$dt=$ch->dueDate;
											$date = DateTime::createFromFormat("Y-m-d H:i:s",$dt);
											$date2=date_create($date->format("Y-m-d"));
											$date1=date_create(date("Y-m-d"));
											$diff=date_diff($date1,$date2);
											if($date2 > $date1){
												?>
												<span class="m-badge m-badge--success m-badge--wide"  data-toggle="m-tooltip" data-placement="top" title data-original-title="Due Date: <?php echo $diff->format("%a day/s");?> left">
													<?php 
													$datetime=$ch->dueDate;
													$date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
													echo sprintf($date->format("M j, Y"));
													/*echo sprintf($date->format("g:ia"))*/;
													?>
												</span>
											<?php }else if($date2 < $date1){?>
												<span class="m-badge m-badge--danger m-badge--wide"  data-toggle="m-tooltip" data-placement="top" title data-original-title="Overdue!"> 
													<?php 
													$datetime=$ch->dueDate;
													$date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
													echo sprintf($date->format("M j, Y"));
													/*echo sprintf($date->format("g:ia"))*/;
													?>
												</span>
											<?php } else if($date2 == $date1){?>
												<span class="m-badge m-badge--warning m-badge--wide"  data-toggle="m-tooltip" data-placement="top" title data-original-title="Today is this checklist Due date! Please finish this before this day ends.">
													<?php 
													$datetime=$ch->dueDate;
													$date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
													echo sprintf($date->format("M j, Y"));
													/*echo sprintf($date->format("g:ia"))*/;
													?>
												</span>
											<?php }?>
										</span>   
									</li> 
								<?php }?> 
							</ul>
						</div>
					</div>
				</div>
				<div class="m-portlet__body" style="margin-top: 3em">
					<div class="col-md-12">
						<div class="m-input-icon m-input-icon--left">
							<input type="text" class="form-control m-input" placeholder="Search..." id="input-search">
							<span class="m-input-icon__icon m-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
					<br> 
				</div>
			</div>		
		</div>
	<?php }?>
	<div class="load-more col-lg-12" lastID="<?php echo $ch->checklist_ID;?>" style="display: none;">
		<h5 style="text-align: center;"> <div class="m-loader m-loader--info" style="width: 30px; display: inline-block;"></div> Loading more checklist...</h5>
	</div>
<?php }?>
<div class="load-more col-lg-12" lastID="0">
</div>