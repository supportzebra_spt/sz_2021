<link href="<?php echo base_url(); ?>assets/src/custom/css/croppie-2.6.2.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.css">

<style type="text/css">
.active:focus {
  background: #e9eeef;
  border-color: #00c5dc;
  color: #3f4047;
}
.activeinput:focus{
 background: #1d86da;
}
.iframepadding{
  padding-left: 15px !important;
  padding-right: 15px !important;
}
.droparea{
  text-align: center;
}
.droparea_outside{
  border-style: dashed;
  padding: 10px;
  margin: 10px;
  border-color:#a5a6ad;
  background: #fff4d2;
}
.row.droparea_outside:hover {
  background: #ffebb0;
}
.droparea_append{
  padding: 10px;
  margin: 10px;
}
.colorgreen{
  color: green;
  font-style: italic;
  font-size:0.9em;
}
.evenadding{
  padding: 15px;
  background:#f1f0f0;
}
.oddadding{
  padding: 15px;
  background: #f9ecec;
}
.buttonform{
  text-align: center;
}
.distance{
  margin-bottom: 20px;
}
.formdistance{
  margin-top: 10px;
}
.taskdistance{
  margin-top: 15px;
}
.label{
  font-size:1.2em;
  color:#00c5dc;
}
.labelheading{
  font-size:1.4em;
  font-weight: bold;
  color:#00c5dc;
}
.placeholdercolor::placeholder { 
  color: #d8dce8;
  opacity: 1;
}
/*this is to test*/
body.dragging, body.dragging * {
  cursor: move !important;
}
.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}
ol.example li.placeholder {
  position: relative;
  /** More li styles **/
}
ol.example li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}
.tile {
  height: 100px;
}
.grid {
  margin-top: 1em;
}
.formpadding
{
  padding: 30px;
  margin-left: 15px;
}
.editable{
  text-decoration: underline !important;
  text-decoration-style: dashed !important;
  text-decoration-color: #50af54 !important;
}
.load-style{
  width: 30px;
  display: none;
  margin-top:45%;
  margin-left: 45%;
}
.load-style-task{
 width: 30px;
 display: none;
 margin-top:25%;
 margin-left: 45%;
}
.colorblue{
  color:#3297e4 !important;
}
.modallabel{
  padding: 20px;
  font-size:1.3em;
}
.dropdownwidth{
  width:100% !important;
}
.topdistance{
  margin-top: 50px;
}
.sublabel-color{
  color:#6a6c71;
}
.padding_formbutton{
  padding-left: 45px;
  padding-top: 20px;
}
td.day.disabled {
  background: #e5e5e5 !important;
  text-decoration: line-through;
}
.buttonform {
  background: #4CAF50;
  border: 1px solid #38803b;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator font-poppins">Process Module</h3>         
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="<?php echo base_url('process')?>" class="m-nav__link m-nav__link--icon">
              <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('process/checklist_pending').'/'.$processinfo->process_ID;?>" class="m-nav__link">
              <span class="m-nav__link-text"><?php echo $processinfo->processTitle;?></span>
            </a>
          </li>
          <li class="m-nav__separator"><</li>
          <li class="m-nav__item">
            <a href="<?php echo base_url('process/checklist_answerable').'/'.$processinfo->process_ID.'/'.$checklist->checklist_ID;?>" class="m-nav__link">
              <span class="m-nav__link-text"><?php echo $checklist->checklistTitle;?></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="m-content">
    <div class="row">
      <div class="col-xl-12">
        <div class="m-portlet">
          <!--begin: Portlet Head-->
          <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  <a href="#" onclick="doNotif2()" class="btn btn-success btn-sm m-btn m-btn--icon">
                    <span>
                      <i class="fa fa-save"></i>
                      <span>Save</span>
                    </span>
                  </a>
                  <!-- <input type="text" id="datetime" readonly> -->
                  <?php if($processinfo->processstatus_ID=="10"){?>
                    <a href="#" data-id="<?php echo $processinfo->process_ID;?>" class="btn btn-success btn-sm m-btn m-btn m-btn--icon" data-toggle="modal" data-target="#run_checklist-modal">   <span>
                      <i class="fa fa-play-circle-o"></i>
                      <span>Run checklist</span>
                    </span>
                  </a>
                <?php }?>
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <div class="input-group m-input-icon m-input-icon--left">          
                  <input readonly="true" type="text" id="datetime" class="form-control form-control-sm m-input" placeholder="Set Due Date">
                  <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-calendar-plus-o"></i></span></span>  
                  <div class="input-group-append">
                    <button class="btn btn-success btn-sm m-btn m-btn--custom" data-id="<?php echo $checklist->checklist_ID;?>" onclick="saveduedate(this)" type="button">Set</button>
                  </div>
                </div>
              </li>   
            </ul>
          </div> 
        </div>
        <!--end: Portlet Head-->
        <div class="m-portlet__body m-portlet__body--no-padding">
          <div class="m-wizard m-wizard--4 m-wizard--brand m-wizard--step-first" id="m_wizard">
            <div class="row m-row--no-padding">
              <div class="col-xl-6 col-lg-12 border">
                <br>
                &nbsp;&nbsp;<h5 style="color:#777;text-align:center;font-size:1.3em;"><strong><?php echo  $processinfo->processTitle;?> Template</strong></h5>
                <div class="taskdistance" style="text-align: center">
                  <a href="#" data-id="<?php echo $processinfo->process_ID;?>" onclick="addtask(this)" class="btn btn-outline-accent btn-sm  m-btn m-btn--icon m-btn--pill">
                    <span>
                      <i class="fa fa-plus"></i>
                      <span>Add Task</span>
                    </span>
                  </a>
                </div>
                <?php if($checklist->dueDate!=NULL){?>
                  <div class="col-xl-12" style="text-align:center;padding:20px">
                    <i class="fa fa-calendar-check-o"></i><span style="font-size:0.9em;"><strong style="color:#34bfa3"> &nbsp;Due Date &nbsp;</strong>
                      <?php 
                      $datetime=$checklist->dueDate;
                      $date = DateTime::createFromFormat("Y-m-d H:i:s",$datetime);
                      echo sprintf($date->format("D, M j, Y"))."&nbsp; at &nbsp;";
                      echo sprintf($date->format("g:ia"));
                      ?>
                    </span>
                  </div>
                <?php }?>
                <input type="hidden" id="hiddenprocessID" name="hiddenprocessID" value="<?php echo $processinfo->process_ID;?>">
                <div class="m-portlet__body" style="height:390px;overflow-y:scroll; overflow-x:auto;background-color:">
                 <div class="m-loader m-loader--info loading-icon load-style-task"></div>
                 <div class="form-group m-form__group" id="checkgroup">
                 </div>
                 <input type="hidden" id="checkID" name="checkID" value="<?php echo $checklist->checklist_ID;?>">
               </div>              
             </div>
             <div id="contentdiv" class="col-xl-6 col-lg-12 border">
               <div class="m-loader m-loader--info loading-icon load-style"></div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="add_content-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create a Form!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Form</a>
          </li>
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
          <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span>
          <div class="row">
            <input type="hidden" id="taskformid" value="">
            <input type="hidden" id="sequenceid" value="">
            <div class="col-lg-6">
              <button type="button" onclick="add_component('1')" data-id="forminputtext" id="forminputtext" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Short Text Entry</button>
              <button type="button" onclick="add_component('2')" data-id="formtextarea" id="formtextarea" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-font"></i> Long Text Entry</button> 
              <button type="button" onclick="add_component('6')" data-id="formsingle" id="formsingle" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-dot-circle-o"></i> Single Select</button>
              <button type="button" onclick="add_component('8')" data-id="formmulti" id="formmulti" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-check-square-o"></i> Multiple Select</button>
              <button type="button" onclick="add_component('7')" data-id="formdropdown" id="formdropdown" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-toggle-down"></i> Dropdown Select</button>
              <button type="button" onclick="add_component('10')" data-id="formdate" id="formdate" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date Picker</button>
              <button type="button" onclick="add_component('13')" data-id="formtime" id="formtime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Time Picker</button>
              <button type="button" onclick="add_component('14')" data-id="formdatetime" id="formdatetime" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-calendar"></i> Date and Time Picker</button>
              <br>
            </div>
            <div class="col-lg-6">
              <button type="button" onclick="add_component('3')" data-id="formheading" id="formheading" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-header"></i> Heading</button>
              <button type="button" onclick="add_component('4')" data-id="formtext" id="formtext" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-text-width"></i> Text</button>
              <button type="button" onclick="add_component('5')" data-id="formnumber" id="formnumber" class="btn btn-accent btn-sm btn-block buttonform"><i class="la la-sort-numeric-desc"></i> Number</button> 
              <button type="button" onclick="add_component('11')" data-id="formspinner" id="formspinner" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-sort-numeric-asc"></i> Number Spinner</button>
              <button type="button" onclick="add_component('9')" data-id="formdivider" id="formdivider" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-i-cursor"></i> Divider</button>
              <button type="button" onclick="add_component('15')" data-id="formfile" id="formfile" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;File Uploader</button>
              <button type="button" onclick="add_component('12')" data-id="formemail" id="formemail" class="btn btn-accent btn-sm btn-block buttonform"><i class="fa fa-toggle-down"></i> Email</button>      

            </div>  
          </div>
        </div>
        <div class="tab-pane" id="tab-insert">
          <div class="row">
           <div class="col-lg-12">
            <label class="btn btn-accent btn-sm btn-block"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Add file as content
             <form id="formFilez" enctype="multipart/form-data">
              <input type="file" style="display: none;" name="upload_files" id="upload_files">
            </form>
          </label>
          <label class="btn btn-accent btn-sm btn-block"><i class="fa fa-image"></i>&nbsp;&nbsp;Add photo as content<input type="file" style="display: none;" name="upload_image" id="upload_image"> </label>
          <button type="button" data-id="file_uploader" id="video_uploader" class="btn btn-accent btn-sm btn-block" ><i class="fa fa-video-camera"></i>&nbsp;&nbsp;Add video as content</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="formproperties-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Data Entry Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="labelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="sublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Set Placeholder</label>
              <input type="text" id="placeholderform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Your Placeholder...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label class="col-3 col-form-label">Set to required</label>
              <div class="col-6">
                <span class="m-switch m-switch--lg m-switch--icon">
                  <label>
                    <input class="requirecheckbox" id="requirecheckbox" type="checkbox" name="">
                    <span></span>
                  </label>
                </span>
              </div>
            </div>
            <div class="m-form__group form-group row">
              <label>Text Minimum / Number Range From</label>
              <input id="textmin" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Maximum / Number Range To</label>
              <input id="textmax" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger m-btn m-btn--icon" type="button" data-dismiss="modal">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-formdetailssave" onclick="saveformproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade modalrem" id="formtextheading-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Header and Text Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="ht_labelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div id="sublab" class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="ht_sublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Alignment</label>
              <select class="form-control m-input m-input--solid" id="textalignment">
                <option>Left</option>
                <option>Center</option>
                <option>Right</option>
              </select>
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label>Text Size</label>
              <select class="form-control m-input m-input--solid" id="textsize">
                <option selected="">Default</option>
                <option>Large</option>
                <option>Small</option>
              </select>
            </div>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="submit" name="submit" id="btn-htdetailsave" onclick="savehtproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->
<!--begin::Modal-->
<div class="modal fade" id="formoptionlist-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Radio, Checkbox and Dropdown Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="optionlabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="optionsublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
           <div class="m-form__group form-group row">
            <label class="col-3 col-form-label">Set to required</label>
            <div class="col-6">
              <span class="m-switch m-switch--lg m-switch--icon">
                <label>
                  <input class="requirecheckbox" id="optionrequirecheckbox" type="checkbox" name="">
                  <span></span>
                </label>
              </span>
            </div>
          </div>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button class="btn btn-danger m-btn m-btn--icon" type="button" data-dismiss="modal">
    <span>
      <i class="fa fa-remove"></i>
      <span>Discard</span>
    </span>
  </button>
  <button type="submit" name="submit" id="btn-oldetailsave" onclick="saveolproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
    <span>
      <i class="fa fa-check"></i>
      <span>Save</span>
    </span>
  </button>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="formdatetime-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Date and Time Properties</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#tab-form" role="tab" aria-selected="true"><i class="la la-file-text"></i>Properties</a>
          </li>
    <!--       <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-insert" role="tab" aria-selected="false"><i class="la la-image"></i>Upload</a>
          </li> -->
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane active show" id="tab-form">
           <!--    <span class="colorgreen">*Click the button that corresponds to a form field you want to add in your form</span> -->
           <div class="row">
            <input type="hidden" id="subtaskformid" value="">
            <div class="col-lg-5 formpadding">
             <div class="m-form__group form-group row">
              <label>Label</label>
              <input type="text" id="dtlabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type form label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Sub Label</label>
              <input type="text" id="dtsublabelform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Type sub label...">
            </div>
            <div class="m-form__group form-group row">
              <label>Set Placeholder</label>
              <input type="text" id="dtplaceholderform" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="Your Placeholder...">
            </div>
          </div>
          <div class="col-lg-5 formpadding">
            <div class="m-form__group form-group row">
              <label class="col-3 col-form-label">Set to required</label>
              <div class="col-6">
                <span class="m-switch m-switch--lg m-switch--icon">
                  <label>
                    <input class="requirecheckbox" id="dtrequirecheckbox" type="checkbox" name="">
                    <span></span>
                  </label>
                </span>
              </div>
            </div>
          <!--   <div class="m-form__group form-group row">
              <label>Text Minimum / Number Range From</label>
              <input id="textmax" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div>
            <div class="m-form__group form-group row">
              <label>Text Maximum / Number Range To</label>
              <input id="textmin" type="Number" class="form-control m-input m-input--air m-input--pill formdistance" placeholder="numbers/integers only...">
            </div> -->
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-danger m-btn m-btn--icon" type="button" data-dismiss="modal">
      <span>
        <i class="fa fa-remove"></i>
        <span>Discard</span>
      </span>
    </button>
    <button type="submit" name="submit" id="btn-formdtdetailssave" onclick="saveformdtproperties_changes(this)" class="btn btn-accent m-btn m-btn--icon">
      <span>
        <i class="fa fa-check"></i>
        <span>Save</span>
      </span>
    </button>
  </div>
</div>
</div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="assign_membersmodal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Members to <span style="color:#a8e9f1" id="fpc_title"></span>&nbsp;<span id='type-assigned' style="color:#83d686"></span></h5>
        <button onclick="resetform()" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <input type="hidden" id="folprotaskcheck_id" name="">
        <input type="hidden" id="assign_type">
        <!--      <input type="text" class="condition"> -->
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs m-tabs-line--2x m-tabs-line m-tabs-line--brand " role="tablist">
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link active show" onclick="" data-toggle="tab" href="#tab-member" role="tab" aria-selected="true"><i class="la la-users"></i>Assigned/Members</a>
          </li>
          <li class="nav-item m-tabs__item">
            <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab-addmember" role="tab" aria-selected="false"><i class="la la-users"></i>Add Members</a>
          </li>
        </ul>
        <div class="tab-content"> 
         <div class="tab-pane fade show active" id="tab-member">
          <div class="row">
            <label class="modallabel colorblue">Members</label>
            <div class="col-12">
             <div class="form-group m-form__group" style="margin-bottom: 8px;">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search Employee Name" id="assigneeSearch">
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="fa fa-search"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12" id="assigned_memberdisplay">
          </div>  
        </div>
      </div>
      <div class="tab-pane fade" id="tab-addmember">
        <div class="row topdistance">
         <div class="col-6">
          <form id="" class="" name="formupdate" method="post">
            <label for="recipient-name" class="form-control-label colorblue">Select Members by Team</label>
            <select id="account" onchange="getaccmembers()" class="selectpicker dropdownwidth" data-live-search="true">
             <optgroup data-max-options="1">
              <option disabled selected="">Click to select</option>
              <?php foreach ($account as $acc) { ?>
                <option value='<?php echo $acc->acc_id ?>'><?php echo $acc->acc_name;?></option>
              <?php } ?>
            </optgroup>                
          </select>
        </form>
      </div>
      <div class="col-6">
        <form id="formupdate" name="formupdate" method="post">
          <label for="recipient-name" class="form-control-label colorblue">Select Members by Name</label>
          <!--<select id="member" onchange="getmember_name()" class="selectpicker dropdownwidth" data-live-search="true">
           <optgroup data-max-options="1">
            <option disabled selected="">Click to select</option>
            <?php foreach ($members as $mem) { ?>
              <option value='<?php echo $mem->emp_id ?>'><?php echo $mem->lname;echo ", &nbsp;";echo $mem->fname;?></option>
            <?php } ?>
          </optgroup>                
        </select>-->
        <select class="form-control m-select2" id="member" name="listschedule" onchange="getmember_name()"></select>

      </form>
    </div>
  </div>
  <div class='row topdistance'>
   <label class="modallabel colorblue">Search results</label>
   <div class="col-12">
    <table class="table m-table table-hover" id="memberdetails"> 
      <thead align="center">
        <tr>
          <th>Name</th>
          <th>Team</th>
          <th>Access</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody id="myTable" align="center">
      </tbody>
    </table>
    <div class="m-loader m-loader--info spinner-icon load-style-search" hidden="hidden"></div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--end::Modal-->

<!-- Upload video modal -->
<div id="modalVideoUpload" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #03A9F4;">
        <h5 class="modal-title" id="exampleModalLabel">Upload Video</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="form-group m-form__group">
         <label class="m-radio m-radio--bold m-radio--state-success">
           <input type="radio" name="vidType" id="vidType" class="vidType" value="1"> URL
           <span></span>
         </label>
         <div class="m-input-icon m-input-icon--left m-input-icon--right">
          <input type="text" class="form-control m-input m-input--pill"  id="videoUrl" placeholder="Paste Youtube URL Link here.">
          <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-tag"></i></span></span>
          <span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comments-o"></i></span></span>
        </div>
      </div>

      <hr>
      <div class="form-group m-form__group">
        <label class="m-radio m-radio--bold m-radio--state-success">
         <input type="radio" name="vidType" id="vidType" class="vidType" value="2" checked> Video Upload
         <span></span>
       </label>
       <div class="m-input-icon m-input-icon--left m-input-icon--right">
        <form id="formVidz" enctype="multipart/form-data">
          <input type="file" class="form-control m-input m-input--pill" name="videoFile" id="videoFile">
        </form>
        <input type="text" id="taskid_reference" name="taskid_reference" hidden>
      </div>
    </div>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-info" id="btnVidUpload">Save</button>
    <button type="button" class="btn btn-info" id="btnVidUrl" style="display:none">Save</button>
  </div>
</div>
</div>
</div>
<!-- End video modal -->

<!-- Start crop image modal -->
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Change Profile Image</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body text-center">
        <div id="image-demo"></div>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-outline-warning m-btn m-btn--icon croppie-rotate" data-deg="-90"><i class="fa fa-rotate-right"></i> Right</button>
        <button class="btn btn-outline-warning m-btn m-btn--icon croppie-rotate" data-deg="90"><i class="fa fa-rotate-left"></i> Left</button>
        <button type="button" id="cropImageBtn" class="btn btn-green crop_image"><i class="fa fa-upload"></i> Crop and Upload</button>
      </div>
    </div>
  </div>
</div>
<!-- End crop image modal -->
</div>
<script src="<?php echo base_url(); ?>assets/src/custom/js/croppie-2.6.2.js" ></script>
<script src="<?php echo base_url(); ?>assets/src/custom/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript">
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes();
  var dateTime = date+' '+time;
  var pro_id=$("#hiddenprocessID").val();
  var checkid=$("#checkID").val();
  function init_task(){
    display_tasks(function(res){
      display_allcomponents();
    });
  }
  function display_membernames(){
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/get_allemployeesname",
    cache: false,
    success: function (res) {
      var result= JSON.parse(res);
      var stringx="<option disabled selected=''>Click to select</option>";
    // stringx+="<optgroup data-max-options='1'>";
    $.each(result, function (key, data) {
      stringx+="<option value="+data.emp_id+">"+data.lname+" "+data.fname+"</option>";
    });
    // stringx+="</optgroup>";
    $("#member").html(stringx);
    $("#member").trigger("change");
    $("#myTable").html("");
  }
});
 }
 $(function(){

  var $image_crop, tempFilename, rawImg, imageId;
  init_task();
  var mouseCopy = $.extend({}, $.ui.mouse.prototype);

  $.extend($.ui.mouse.prototype, {
    _mouseInit: function () {
      var that = this;
      if (!this.options.mouseButton) {
        this.options.mouseButton = 1;
      }

      mouseCopy._mouseInit.apply(this, arguments);

      if (this.options.mouseButton === 3) {
        this.element.bind("contextmenu." + this.widgetName, function (event) {
          if (true === $.data(event.target, that.widgetName + ".preventClickEvent")) {
            $.removeData(event.target, that.widgetName + ".preventClickEvent");
            event.stopImmediatePropagation();
            return false;
          }
          event.preventDefault();
          return false;
        });
      }

      this.started = false;
    },
    _mouseDown: function (event) {

            // we may have missed mouseup (out of window)
            (this._mouseStarted && this._mouseUp(event));

            this._mouseDownEvent = event;

            var that = this,
            btnIsLeft = (event.which === this.options.mouseButton),
                // event.target.nodeName works around a bug in IE 8 with
                // disabled inputs (#7620)
                elIsCancel = (typeof this.options.cancel === "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false);
                if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) {
                  return true;
                }

                this.mouseDelayMet = !this.options.delay;
                if (!this.mouseDelayMet) {
                  this._mouseDelayTimer = setTimeout(function () {
                    that.mouseDelayMet = true;
                  }, this.options.delay);
                }

                if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
                  this._mouseStarted = (this._mouseStart(event) !== false);
                  if (!this._mouseStarted) {
                    event.preventDefault();
                    return true;
                  }
                }

            // Click event may never have fired (Gecko & Opera)
            if (true === $.data(event.target, this.widgetName + ".preventClickEvent")) {
              $.removeData(event.target, this.widgetName + ".preventClickEvent");
            }

            // these delegates are required to keep context
            this._mouseMoveDelegate = function (event) {
              return that._mouseMove(event);
            };
            this._mouseUpDelegate = function (event) {
              return that._mouseUp(event);
            };
            $(document)
            .bind("mousemove." + this.widgetName, this._mouseMoveDelegate)
            .bind("mouseup." + this.widgetName, this._mouseUpDelegate);

            event.preventDefault();

            mouseHandled = true;
            return true;
          }
        });
    // Start CropImage
   //IMAGE UPLOAD
   var $image_crop = $('#image-demo').croppie({
    viewport: {
      width: 250,
      height: 250
    },
    boundary: {
      width: 250,
      height: 250
    },
    enforceBoundary: false,
    enableExif: true,
    enableOrientation: true
  });

   $('#upload_image').on('change', function () {
    imageId = $(this).data('id');
    tempFilename = $(this).val();
    $('#cancelCropBtn').data('id', imageId);
    readFile(this);
  });

   $('.crop_image').click(function(event){
    var taskid=$("#taskformid").val();
    countallforms(taskid);
    $image_crop.croppie('result',{
      type:'canvas',
      format: 'jpeg',
      size:'viewport'
    }).then(function(response){  
      var sequencenum=$("#sequenceid").val();
      var seqnum=0;
      if(sequencenum==0){
        seqnum=1;
      }else{
        seqnum=sequencenum;
        seqnum++;
      }
      $.ajax({
        url: "<?php echo base_url(); ?>process/display_imagefetch",
        type: "POST",
        data: {
          image:response,
          task_ID:taskid,
          sequence: seqnum
        },
        success:function(data){
          $("#cropImagePop").modal('hide');
          $("#add_content-modal").modal('hide');
          res = JSON.parse(data.trim());
          var data=res;
          var subtaskid=data.subTask_ID;      
          add_form_image(taskid,subtaskid);
          $('#item-img-output'+subtaskid).attr('src', response);
        }
      });
    })
  });


   function readFile(input) {
    if (input.files && input.files[0]) {
      var file = input.files[0];
      var fileType = file["type"];
      var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
      if ($.inArray(fileType, ValidImageTypes) < 0) {
        swal("File selected is not an image");
      } else {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('.upload-demo').addClass('ready');
          rawImg = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
        reader.onloadend = function () {
          $('#cropImagePop').modal('show');
        };
      }
    } else {
      swal("Sorry - you're browser doesn't support the FileReader API");
    }
  }

  $('#cropImagePop').on('shown.bs.modal', function(){
    // alert('Shown pop');
    $image_crop.croppie('bind', {
      url: rawImg
    }).then(function(){
      console.log('jQuery bind complete');
    });
  });
});

 // End CropImage
 
  // Start File Upload
  $('#upload_files').on('change', function () {
    tempFilename = $(this).val();
    readFileType(this);
  });
  function readFileType(input) {
    if (input.files && input.files[0]) {
      var file = input.files[0];
      var fileTypes = file["type"];

      var ValidImageTypes = ["application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/x-zip-compressed", "application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/msword","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.presentationml.presentation"];
      if ($.inArray(fileTypes, ValidImageTypes) < 0) {
        swal("Ooops!","Only Word/Excel/PowerPoint/Zip Files are allowed to be uploaded.","error");
      }else{
        var reader = new FileReader();
        reader.onload = function (e) {
          // $('.upload-demo').addClass('ready');
          // rawImg = e.target.result;
      // alert(e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
    reader.onloadend = function () {
         // alert("OK sya");
         $("#formFilez").submit();
       };
     }
   } else {
    swal("Sorry - you're browser doesn't support the FileReader API");
  }
}
$('#member').select2({
  placeholder: "Select your schedule.",
  width: '100%'
});
$("#formFilez").on('submit',function(e) {
 var taskformid= $("#taskformid").val();
 countallforms(taskformid);
 e.preventDefault();
 var sequencenum=$("#sequenceid").val();
 var seqnum=0;
 if(sequencenum==0){
  seqnum=1;
}else{
  seqnum=sequencenum;
  seqnum++;
}
$.ajax({
  url: "<?php echo base_url(); ?>process/display_filefetch/"+taskformid+"/"+seqnum,
  type: "POST",
  data:  new FormData(this),
  contentType: false,
  cache: false,
  processData:false,
  success: function(data)
  { 
    res = JSON.parse(data.trim());
    var info=res;
    var filetype="";
    var subtaskid=info.subtask;
    var uploadfile_src=info.complabel;

    if(uploadfile_src.indexOf('.xlsx')!= -1){
      filetype="XLSX";
    }else if(uploadfile_src.indexOf('.xls')!= -1){
      filetype="XLS";
    }else if(uploadfile_src.indexOf('.zip')!= -1){
      filetype="ZIP";
    }
    else if(uploadfile_src.indexOf('.doc')!= -1){
      filetype="DOC";
    }
    else if(uploadfile_src.indexOf('.docx')!= -1){
      filetype="DOCX";
    }
    else if(uploadfile_src.indexOf('.ppt')!= -1){
      filetype="PPT";
    }
    else if(uploadfile_src.indexOf('.pptx')!= -1){
      filetype="PPTX";
    }
    else if(uploadfile_src.indexOf('.pdf')!= -1){
      filetype="PDF";
    }
    else if(uploadfile_src.indexOf('.rar')!= -1){
      filetype="RAR";
    }
    if(data!="Invalid"){
     uploadfile_src=uploadfile_src.replace("uploads/process/form_file/","");

     add_form_file(taskformid,subtaskid,filetype,uploadfile_src);
     $("#add_content-modal").modal("hide");
   }else{
     swal("Ooops!","There is an error during uploading.","error");
   }
 }         
});
});
  // End File Upload


 // Start Video Upload
 $("#video_uploader").click(function(){
   var taskformid= $("#taskformid").val();
   $("#modalVideoUpload").modal('show'); 
   $("#videoFile").val('');
   $("#videoUrl").val('');
   $("#taskid_reference").val(taskformid);
 });
    // $('#videoFile').on('change', fileUpload);
    $('.vidType').on('click', function(){
      var vidType = $("#vidType:checked"). val();
      if(vidType==1){
       $("#btnVidUpload").css("display","none");
       $("#btnVidUrl").css("display","block");
     }else{
       $("#btnVidUpload").css("display","block");
       $("#btnVidUrl").css("display","none");
     }
   });

    $('#btnVidUrl').on('click', function(){
      var url_str =  $("#videoUrl").val();
      var add = $("#taskid_reference").val();
      var taskid=add;
      var vidtype="url";
      countallforms(taskid);
      var sequencenum=$("#sequenceid").val();
      var seqnum=0;
      if(sequencenum==0){
        seqnum=1;
      }else{
        seqnum=sequencenum;
        seqnum++;
      }
      $.ajax({
       url: "<?php echo base_url(); ?>process/display_vidfetch/1/"+add+"/"+seqnum,
       type: "POST",
       data: {
        url_str:url_str
      },
      dataType: "html",
      success: function(data)
      {
        if(data>0){
          url_str = url_str.replace("watch?v=","embed/");
          var subtaskid=data;
          add_form_video(taskid,subtaskid,vidtype,url_str);
          $("#modalVideoUpload").modal("hide");
          $("#add_content-modal").modal("hide");
        }else{
         alert("Not SaveD");
       }
     }         
   });
    });

    $('#btnVidUpload').on('click', function(){
      var vidType = $("#vidType:checked"). val();
   if(vidType==2){ //if Video Upload
    var vid = $("#videoFile").val();
    var file = $("#videoFile")[0].files[0];
    if (isVideo(vid)){
         // fileUpload($("#videoFile").val());
        // console.log($("#videoFile")[0]);
        $("#formVidz").submit();
      }
      else
      {
       alert("Only video files are allowed to upload.")
     } 
   }else{
     var vid = $("#videoUrl").val();
   }
 });
    $("#formVidz").on('submit',function(e) {
      var add = $("#taskid_reference").val();
      var taskid=add;
      var vidtype="upload";
      countallforms(taskid);
      var sequencenum=$("#sequenceid").val();
      var seqnum=0;
      if(sequencenum==0){
        seqnum=1;
      }else{
        seqnum=sequencenum;
        seqnum++;
      }
      e.preventDefault();
      $.ajax({
       url: "<?php echo base_url(); ?>process/display_vidfetch/2/"+add+"/"+seqnum,
       type: "POST",
       data: new FormData(this),
       contentType: false,
       cache: false,
       processData:false,
       success: function(data)
       { 
         res = JSON.parse(data.trim());
         var info=res;
         var upload_src=info.complabel;
         var subtaskid=info.subtask;
         if(data!="invalid"){
           add_form_video(taskid,subtaskid,vidtype,upload_src);
           $("#modalVideoUpload").modal("hide");
           $("#add_content-modal").modal("hide");
         }else{
           alert("Not SaveD");
         }
       }         
     });
    });
    function isVideo(filename) {
      var ext = getExtension(filename);
      switch (ext.toLowerCase()) {
        case 'm4v':
        case 'avi':
        case 'mpg':
        case 'mp4':
        case 'mov':
        case 'mpg':
        case 'mpeg':
        // etc
        return true;
      }

      return false;
    }

    function getExtension(filename) {
      var parts = filename.split('.');
      return parts[parts.length - 1];
    }

  // End Video Upload

  $("#datetime").datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true,
    todayBtn: true,
    startDate : dateTime
  });
  function duedate(element){
   var taskid=$(element).data('id');
   $("#taskduedate"+taskid).datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true,
    todayBtn: true,
    startDate : dateTime
  });
 }

 function check_taskassignment(){
  $.ajax({   
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_asstask",
   data: {
    checklist_ID: checkid,
  },
  cache: false,
  success: function (res) {
    var rt = JSON.parse(res.trim());
    console.log(rt);
    if(rt.length>0){
      $.each(rt, function (key, data) {
        if(data.taskrule=="6"){
          $("#"+data.taskid).attr("disabled", false);
        }
      });
    }else{
     $(".inputtask").attr("disabled", false);
   }
 }
});
}
// SAVE FORM PROPERTIES
function saveformproperties_changes(element){
  var taskid=$(element).data('id');
  var subtaid=$("#subtaskformid").val();
  var labelcontent=$("#labelform").val();
  var sublabelcontent=$("#sublabelform").val();
  var placeholdercontent=$("#placeholderform").val();
  var max=$("#textmax").val();
  var min=$("#textmin").val();
  var require="";
  if ($("#requirecheckbox").is(':checked'))
  {
    require="yes";
  }
  else
  {
    require="no";
  }
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/update_inputformdetails",
   data: {
    subTask_ID: subtaid,
    complabel: labelcontent,
    sublabel: sublabelcontent,
    placeholder: placeholdercontent,
    required: require,
    min:min,
    max:max
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   display_allcomponents();
   swal({
    position: 'center',
    type: 'success',
    title: 'Successfully Updated Properties!',
    showConfirmButton: false,
    timer: 1500
  });
   $("#formproperties-modal").modal('hide');
 }
}); 
}
function savehtproperties_changes(element){
  var taskid=$(element).data('id');
  var subtaid=$("#subtaskformid").val();
  var htlabelform=$("#ht_labelform").val();
  var htsublabelform=$("#ht_sublabelform").val();
  var textalignment=$("#textalignment").val();
  var textsize=$("#textsize").val();
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/update_headingtextdetails",
   data: {
    subTask_ID: subtaid,
    complabel: htlabelform,
    sublabel: htsublabelform,
    text_alignment: textalignment,
    text_size: textsize
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   display_allcomponents();
   swal({
    position: 'center',
    type: 'success',
    title: 'Successfully Updated Properties!',
    showConfirmButton: false,
    timer: 1500
  });
   $("#formtextheading-modal").modal('hide');
 }
}); 
}
function saveolproperties_changes(element){
 var taskid=$(element).data('id');
 var subtaid=$("#subtaskformid").val();
 var labelcontent=$("#optionlabelform").val();
 var sublabelcontent=$("#optionsublabelform").val();
 var require="";
 if ($("#optionrequirecheckbox").is(':checked'))
 {
  require="yes";
}
else
{
  require="no";
}
$.ajax({
 type: "POST",
 url: "<?php echo base_url(); ?>process/update_optiondetails",
 data: {
  subTask_ID: subtaid,
  complabel: labelcontent,
  sublabel: sublabelcontent,
  required: require,
},
cache: false,
success: function (res) {
 var result = JSON.parse(res.trim());
 display_allcomponents();
 swal({
  position: 'center',
  type: 'success',
  title: 'Successfully Updated Properties!',
  showConfirmButton: false,
  timer: 1500
});
 $("#formoptionlist-modal").modal('hide');
}
}); 
}
function saveformdtproperties_changes(element){
 var taskid=$(element).data('id');
 var subtaid=$("#subtaskformid").val();
 var labelcontent=$("#dtlabelform").val();
 var sublabelcontent=$("#dtsublabelform").val();
 var placeholdercontent=$("#dtplaceholderform").val();
 var require="";
 if ($("#dtrequirecheckbox").is(':checked'))
 {
  require="yes";
}
else
{
  require="no";
}
$.ajax({
 type: "POST",
 url: "<?php echo base_url(); ?>process/update_datetimedetails",
 data: {
  subTask_ID: subtaid,
  complabel: labelcontent,
  sublabel: sublabelcontent,
  placeholder: placeholdercontent,
  required: require
},
cache: false,
success: function (res) {
 var result = JSON.parse(res.trim());
 display_allcomponents();
 swal({
  position: 'center',
  type: 'success',
  title: 'Successfully Updated Properties!',
  showConfirmButton: false,
  timer: 1500
});
 $("#formdatetime-modal").modal('hide');
}
}); 
}
//START ADDING FORM FOR IMAGE
function add_form_image(taskid,subtaskid){
  var stringx="";
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";

  stringx+="<div id='uploaded_image' class='col-12'>";
  stringx+="<figure style='margin:0 !important;text-align:center;padding-top:25px'>"
  stringx+="<img src='' class='gambar img-responsiv ' id='item-img-output"+subtaskid+"' style='background:white;width:85%!important;object-fit:cover;'/>"
  stringx+="</figure>"
  stringx+="</div></div>";
  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button type='button' data-id='"+subtaskid+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-upload'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  $("#formtag"+taskid).append(stringx);
}
//END ADDING FORM FOR IMAGE

//START ADDING FORM FOR FILE
function add_form_file(taskid,subtaskid,formtype,uploadfile_src){
  var stringx="";
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";

  stringx+="<div class='col-12 text-center'>";
  stringx+="<button href='#' class='btn btn-outline-info btn-lg  m-btn m-btn--icon m-btn--outline-2x'>";
  stringx+="<span>";
  stringx+="<i class='fa fa-file-text-o'></i>";
  stringx+="<span>"+formtype+"</span>";
  stringx+="</span>";
  stringx+="</button>";
  stringx+="<br><div style='font-size:1.1em;color:#7e61b1'>"+uploadfile_src+"</div>";
  stringx+="<button class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' disabled>";
  stringx+="<span><i class='fa fa-download'></i><span>Download this file</span></span>";
  stringx+="</button>";

  stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  
  stringx+="</div>";
  stringx+="<div class='input-group-append formdistance style='padding-left:15px;'>";
  stringx+="<button type='button' data-id='' onclick='' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-download'></i></button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
  $("#formtag"+taskid).append(stringx);
}
//END ADDING FORM FOR FILE

//START ADDING FORM FOR VIDEO
function add_form_video(taskid,subtaskid,videotype,video_src){
  var stringx="";
  stringx+="<div id='"+subtaskid+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row iframepadding'>";
  stringx+="<div class='embed-responsive embed-responsive-16by9'>";
  if(videotype=="url"){
    stringx+="<iframe id='item-vid-output"+subtaskid+"' src='"+video_src+"' class='embed-responsive-item' frameborder='0' allowfullscreen ></iframe>";
  }else{
   stringx+= "<video width='320' height='240' controls>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mp4'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/m4v'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/avi'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mpg'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mov'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mpg'>";
   stringx+= "<source src='<?php echo base_url()?>"+video_src+"' type='video/mpeg'>";
   stringx+= "</video>";
 }
 stringx+="</div></div>";

 stringx+="<div data-id='"+subtaskid+"' id='sublabel"+subtaskid+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>Type a description...</div>";

 stringx+="<div class='input-group-append formdistance'>";
 stringx+="<button type='button' data-id='"+subtaskid+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-upload'></i>";
 stringx+="</button>";
 stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+subtaskid+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
 stringx+="<i class='fa fa-close'></i>";
 stringx+="</button>";
 stringx+="</div>";
 stringx+="</div>";
 $("#formtag"+taskid).append(stringx);
}
//ENDING ADDING FORM FOR VIDEO

function display_tasks(callback){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/display_tasks_update",
  data: {
    process_ID: pro_id,
  },
  beforeSend:function(){
    $('.loading-icon').show();
  },
  cache: false,
  success: function (res) {
    $('.loading-icon').hide();
    var result = JSON.parse(res.trim());
    var stringx="";
    var stringcon="";
    $.each(result, function (key, item) {
      stringx += "<div class='input-group m-form__group' id='addcheckbox'>";
      stringx +="<div class='input-group-prepend'>";
      stringx +="<span class='input-group-text'>";
      stringx +="<label class=''>";
      stringx +=" <span></span>";
      stringx +="</label>";
      stringx +="</span>";
      stringx +="</div> ";
      if(item.taskTitle==null){
       stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value='' disabled>"; 
     }else{
      stringx +="<input data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='displayformdiv(this)' onkeyup='autosavetask(this)' type='text' class='inputtask sampletask form-control active' placeholder='Write task title here...' aria-label='Text input with checkbox' value='"+item.taskTitle+"' disabled>";
    }
    stringx +="<input type='hidden' id='hiddentaskID' name='hiddentaskID' value='"+item.process_ID+"'>";
    stringx +="<input type='hidden' id='htaskID' name='htaskID' value='"+item.task_ID+"'>";
    stringx +="<div class='input-group-append'>";
    stringx +="<div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right' data-dropdown-toggle='hover' aria-expanded='true'>";
    stringx +="<a href='#' class='m-dropdown__toggle btn btn-secondary dropdown-toggle'>";
    stringx +="<i class='fa fa-delete'></i>";
    stringx +="</a>";
    stringx +="<div class='m-dropdown__wrapper'>";
    stringx +="<span class='m-dropdown__arrow m-dropdown__arrow--right'></span>";
    stringx +="<div class='m-dropdown__inner'>";
    stringx +="<div class='m-dropdown__body'> " ;            
    stringx +="<div class='m-dropdown__content'>";
    stringx +="<ul class='m-nav'>";
    stringx +="<li class='m-nav__item'>";
    stringx +="<a data-id='"+item.task_ID+"' onclick='deleteTask(this)' class='m-nav__link'>";
    stringx +="<i class='m-nav__link-icon fa fa-trash'></i>";
    stringx +="<span class='m-nav__link-text'>Delete</span>";
    stringx +="</a>";
    stringx +="</li>";
    stringx +="</ul>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div>";
    stringx +="</div> ";
    stringx +="</div>";

    stringcon +="<div id='formcontent"+item.task_ID+"'class='m-portlet__body formcontent' style='height:560px;overflow-y:scroll; overflow-x:auto'>";
    stringcon +="<input type='hidden' class='hiddentask' value='"+item.task_ID+"'>"
    stringcon +="<div id='rowdisplay"+item.task_ID+"' class='row'>";

    stringcon +="<div class='col-lg-6'>";
    stringcon +="<div class='input-group m-input-icon m-input-icon--left'> ";         
    stringcon +="<input readonly='true' type='text' data-id='"+item.task_ID+"' onmouseover='duedate(this)' id='taskduedate"+item.task_ID+"' class='form-control form-control-sm m-input' placeholder='Task Due Date'>";
    stringcon +="<span class='m-input-icon__icon m-input-icon__icon--left'><span><i class='fa fa-calendar-plus-o'></i></span></span> "; 
    stringcon +="<div class='input-group-append'>";
    stringcon +="<button class='btn btn-brand btn-sm m-btn m-btn--custom' data-id='"+item.task_ID+"' onclick='savetaskduedate(this)' type='button'>Set</button>";
    stringcon +="</div>";
    stringcon +="</div>";
    stringcon += "</div>";

    stringcon +="<div class='col-lg-5' style='padding:9px'>";
    stringcon +="<a href='#' data-tasktitle='"+item.taskTitle+"' data-id='"+item.task_ID+"'  data-type='task' onclick='fetch_IDforAssignment(this),display_membernames(),initAssignee(this)' data-toggle='modal' data-target='#assign_membersmodal'><i class='la la-user-plus'></i> Assign this task</a>";
    stringcon += "</div>";
    stringcon +="</div>";
    stringcon +="<form id='formtag"+item.task_ID+"'>";
    stringcon +="</form>";
    // stringcon +="<div id='formaddcontent"+item.task_ID+"' class='row droparea_append'>";
    // stringcon +="</div>";
    stringcon += "<div class='row droparea_outside'>";
    stringcon += "<div class='col-lg-12 droparea' >";
    stringcon += "<a href='#' data-id='"+item.task_ID+"' id='"+item.task_ID+"' onclick='taskid(this)' class='btn m-btn m-btn--icon m-btn--pill m-btn--air' data-toggle='modal' data-target='#add_content-modal'>";
    stringcon += "<span>";
    stringcon += "<i class='fa fa-plus-circle'></i>";
    stringcon += "<span>Add Content</span>";
    stringcon += "</span>";
    stringcon += "</a>";
    stringcon += "</div>";
    stringcon += "</div>";  
    stringcon += "</div>";
  });
$("#checkgroup").html(stringx);
$(".inputtask:first").focus();
$(".m-dropdown.m-dropdown--inline.m-dropdown--arrow.m-dropdown--align-right:first").hide();
$(".inputtask:first").css({"background-color":"#51aff9","color":"white"});
$(".inputtask:first").addClass("placeholdercolor");
var firstaskid=$(".inputtask:first").attr('id');
$('#contentdiv').data('activetaskid', firstaskid);
  // display_allcomponents(firstaskid);
  $("#contentdiv").append(stringcon);
  $(".formcontent").hide();
  $(".formcontent:first").show();
  var taskid=$(".inputtask:first").attr('id');
  displayduedate(taskid);
  check_taskassignment();
  callback(res);
}
});
}

function taskid(element){
  var taskid=$(element).data('id');
  $("#taskformid").val(taskid);
  countallforms(taskid);
}
function display_allcomponents(){
  var taskid = $('#contentdiv').data('activetaskid')
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_allsubtask",
    data: {
      task_ID:$('#contentdiv').data('activetaskid')
    },
    cache: false,
    success: function (res) {
     var result = JSON.parse(res.trim());
     var stringx = "";
     $.each(result, function (key, item) {
      console.log(item.sequence);
      if(item.component_ID==1){
        stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"'class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label editable' onkeyup='autosavelabel(this)'>";
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Type a label";
        }
        stringx+="</div>";
        if(item.placeholder){
          stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='"+item.placeholder+"' disabled>";
        }else{
          stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill formdistance' placeholder='Something will be typed here...' disabled>";
        }
        if(item.sublabel){
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
        }else{
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
        }
        stringx+="<div class='input-group-append formdistance'>";
        stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
        stringx+="<i class='fa fa-edit'></i>";
        stringx+="</button>";
        stringx+="<button type='button' data-compid='"+taskid+"' type='button' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-close'></i>";
        stringx+="</button>";
        stringx+="</div>";
        stringx+="</div>";
      }
      else if(item.component_ID==2){
        stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' contenteditable='true' class='label editable' onkeyup='autosavelabel(this)'>";
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Type a label";
        }
        stringx+="</div>";

        stringx+="<form>";
        stringx+="<textarea class='form-control m-input m-input--air m-input--pill' rows='10' placeholder='Long text will be typed here...' disabled></textarea>";
        stringx+="</form>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
        stringx+="<div class='input-group-append formdistance'>";
        stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
        stringx+="<i class='fa fa-edit'></i>";
        stringx+="</button>";
        stringx+="<button type='button' data-compid='"+taskid+"' type='button' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-close'></i>";
        stringx+="</button>";
        stringx+="</div>";
        stringx+="</div>";
      }
      else if(item.component_ID==3){
        stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
        stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='labelheading editable'>";
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Heading";
        }
        stringx+="</div>";
        if(item.sublabel){
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
        }else{
          stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
        }
        stringx+="<div class='input-group-append formdistance'>";
        stringx+="<button type='button' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-edit'></i>";
        stringx+="</button>";
        stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-close'></i>";
        stringx+="</button>";
        stringx+="</div>";
        stringx+="</div>";
      }
      else if(item.component_ID==4){
        stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";

        stringx+="<p data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>"; 
        if(item.complabel){
          stringx+=item.complabel;
        }else{
          stringx+="Click to edit this text...";
        }
        stringx+="</p>";

        stringx+="<div class='input-group-append formdistance'>";
        stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formtextheading-modal' onclick='fetch_headertextdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-edit'></i>";
        stringx+="</button>";
        stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
        stringx+="<i class='fa fa-close'></i>";
        stringx+="</button>";
        stringx+="</div>";
        stringx+="</div>";
      }
      else if(item.component_ID==5){
       stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
       stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
       if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Number label";
      }
      stringx+="</div>";
      if(item.placeholder){
        stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled>";
      }else{
        stringx+="<input type='number' class='form-control m-input m-input--air m-input--pill' placeholder='A number or numbers will be typed here...' disabled>";
      }
      if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      }
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
    }
    else if(item.component_ID==6){
      stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label formdistance editable'>";
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
      }else{
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      }
      stringx+="<div id='radiolist"+item.subTask_ID+"' class='row m-radio-list formdistance'>";
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
      stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Option</label>";
      stringx+="<a>";
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
      radio_option(item.subTask_ID);
    }
    else if(item.component_ID==7){
      stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      if(item.sublabel){
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
      }else{ 
        stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      }
      stringx+="<div class='form-group m-form__group'>";
      stringx+="<select class='form-control formdistance m-input m-input--air m-input--pill' id='exampleSelect1' disabled>";
      stringx+="<option></option>";
      stringx+="</select>";
      stringx+="</div>";
      stringx+="<div class='form-group m-form__group formdistance'>";
      stringx+="<ul id='itemlist"+item.subTask_ID+"' class='list-group'>";
      stringx+="</ul>";
      stringx+="</div>";

      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
      stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add List</label>";
      stringx+="</a>";
      stringx+="</div>";

      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
      dropdown_list(item.subTask_ID);
    }
    else if(item.component_ID==8){
      stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
      if(item.complabel){
        stringx+=item.complabel;
      }else{
        stringx+="Type a label";
      }
      stringx+="</div>";
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
      stringx+="<div id='checkoption"+item.subTask_ID+"' class='row m-checkbox-list formdistance'>";
      stringx+="</div>";

      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<a href='#' data-id='"+item.subTask_ID+"' onclick='add_componentoption(this)'>";
      stringx+="<i style='color:#c4c5d6' class='fa fa-plus'></i><label>&nbsp;Add Checkbox</label>";
      stringx+="<a>";
      stringx+="</div>";
      stringx+="<div class='input-group-append formdistance'>";
      stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' data-toggle='modal' data-target='#formoptionlist-modal' onclick='fetch_optionlistdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-edit'></i>";
      stringx+="</button>";
      stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
      stringx+="<i class='fa fa-close'></i>";
      stringx+="</button>";
      stringx+="</div>";
      stringx+="</div>";
      checkbox_option(item.subTask_ID);
    }
    else if(item.component_ID==9){
     stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 distance'>";
     stringx+="<hr>";
     stringx+="<div class='input-group-append formdistance'>";
     stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
     stringx+="<i class='fa fa-close'></i>";
     stringx+="</button>";
     stringx+="</div>";
     stringx+="</div>";
   }
   else if(item.component_ID==10){
     stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
     stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
     if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Type Date label";
    }
    stringx+="</div>";
    stringx+="<div class='input-group formdistance'>";

    if(item.placeholder){
      stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
    }else{
      stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
    }    
    stringx+="<div class='input-group-append'>";
    stringx+="<span class='input-group-text'>";
    stringx+="<i class='la la-calendar-check-o'></i>";
    stringx+="</span>";
    stringx+="</div>";
    stringx+="</div>";
    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    }
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(item.component_ID==11){
    stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Type a label";
    }
    stringx+="</div>";
    stringx+="<input class='form-control m-input m-input--air m-input--pill formdistance' type='number' value='0' min='0' max='100' step='1'/>";

    if(item.sublabel){
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
    }else{
      stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
    }
    stringx+="<div class='input-group-append formdistance'>";
    stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
    stringx+="<i class='fa fa-edit'></i>";
    stringx+="</button>";
    stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
    stringx+="<i class='fa fa-close'></i>";
    stringx+="</button>";
    stringx+="</div>";
    stringx+="</div>";
  }
  else if(item.component_ID==12){
    stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
    stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
    if(item.complabel){
      stringx+=item.complabel;
    }else{
      stringx+="Email Label";
    }
    stringx+="</div>";

    if(item.placeholder){
     stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='"+item.placeholder+"' disabled>";
   }else{
    stringx+="<input type='email' class='form-control m-input m-input--air m-input--pill' placeholder='An email will be typed here...' disabled>";
  }
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_inputtypeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air mr-2' data-toggle='modal' data-target='#formproperties-modal'>";
  stringx+="<i class='fa fa-edit'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==13){
 stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
 if(item.complabel!=null){
  stringx+=item.complabel;
}else{
  stringx+="Type Time Label";
}
stringx+="</div>";
stringx+="<div class='input-group formdistance'>"
if(item.placeholder){
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
}else{
  stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
} 
stringx+="<div class='input-group-append'>";
stringx+="<span class='input-group-text'>";
stringx+="<i class='la la-clock-o'></i>";
stringx+="</span>";
stringx+="</div>";
stringx+="</div>";
if(item.sublabel){
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
}else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
}
stringx+="<div class='input-group-append formdistance'>";
stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==14){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div data-id='"+item.subTask_ID+"' id='label"+item.subTask_ID+"' onkeyup='autosavelabel(this)' contenteditable='true' class='label editable'>";
  if(item.complabel){
    stringx+=item.complabel;
  }else{
    stringx+="Type Date and Time Label";
  }
  stringx+="</div>";
  stringx+="<div class='input-group formdistance'>";
  if(item.placeholder){
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='"+item.placeholder+"' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  }else{
    stringx+="<input type='text' class='form-control m-input m-input--air m-input--pill' name='date' placeholder='Selected date and time here...' id='' aria-describedby='m_datepicker-error' aria-invalid='false' disabled>";
  } 
  stringx+="<div class='input-group-append'>";
  stringx+="<span class='input-group-text'>";
  stringx+="<i class='la la-calendar-check-o'></i>";
  stringx+="</span>";
  stringx+="</div>";
  stringx+="</div>";
  if(item.sublabel){
   stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'>"+item.sublabel+"</div>";
 }else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#b9bbc1'> Type a Sub label...</div>";
}
stringx+="<div class='input-group-append formdistance'>";
stringx+="<button type='button' data-component='"+item.component_ID+"' data-id='"+item.subTask_ID+"' onclick='fetch_datetimeformdetails(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' data-toggle='modal' data-target='#formdatetime-modal'>";
stringx+="<i class='fa fa-edit'></i>";
stringx+="</button>";
stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==15){
 stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
 stringx+="<div class='row'>";
 stringx+="<div class='col-12  text-center'>";
 stringx+="<button class='btn btn-outline-success m-btn m-btn--icon' disabled>";
 stringx+="<span>";
 stringx+="<i class='fa fa-upload'></i>";
 stringx+="<span>File will be uploaded here</span>";
 stringx+='</span>';
 stringx+='</button>';
 if(item.sublabel){
   stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:1em;color:#6a6c71'>"+item.sublabel+"</div>";
 }else{
  stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:1em;color:#6a6c71'> Type a description...</div><br>";
}
stringx+="</div></div>";
stringx+="<div class='input-group-append formdistance'>";
stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
stringx+="<i class='fa fa-close'></i>";
stringx+="</button>";
stringx+="</div>";
stringx+="</div>";
}
else if(item.component_ID==16){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";
  stringx+="<div id='uploaded_image' class='col-12'>";
  stringx+="<figure style='margin:0 !important;text-align:center;padding-top:25px'>"
  stringx+="<img src='<?php echo base_url()?>"+item.complabel+"' class='gambar img-responsiv ' id='item-img-output"+item.subTask_ID+"' style='background:white;width:85%!important;object-fit:cover;'/>"
  stringx+="</figure>"
  stringx+="</div></div>";

  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button type='button' data-id='"+item.subTask_ID+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-upload'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==17){
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row iframepadding'>";
  stringx+="<div class='embed-responsive embed-responsive-16by9'>";

  if(item.complabel.indexOf('uploads/process/form_videos/')!= -1 ){
    stringx+= "<video width='320' height='240' controls>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mp4'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/m4v'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/avi'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mpg'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mov'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mpg'>";
    stringx+= "<source src='<?php echo base_url()?>"+item.complabel+"' type='video/mpeg'>";
    stringx+= "</video>";
  }else{
    stringx+=item.complabel;
  }
  stringx+="</div></div>";
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  }
  stringx+="<div class='input-group-append formdistance'>";
  stringx+="<button type='button' data-id='"+item.subTask_ID+"' onclick='upload_file(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-upload'></i>";
  stringx+="</button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</button>";
  stringx+="</div>";
  stringx+="</div>";
}
else if(item.component_ID==18){
  var complabel=item.complabel;
  var complabel_name=item.complabel;

  complabel_name=complabel_name.replace("uploads/process/form_file/","");

  if(complabel.indexOf('.xlsx')!= -1){
    complabel="XLSX";
  }else if(complabel.indexOf('.xls')!= -1){
    complabel="XLS";
  }else if(complabel.indexOf('.zip')!= -1){
    complabel="ZIP";
  }
  else if(complabel.indexOf('.doc')!= -1){
    complabel="DOC";
  }
  else if(complabel.indexOf('.docx')!= -1){
    complabel="DOCX";
  }
  else if(complabel.indexOf('.ppt')!= -1){
    complabel="PPT";
  }
  else if(complabel.indexOf('.pptx')!= -1){
    complabel="PPTX";
  }
  else if(complabel.indexOf('.pdf')!= -1){
    complabel="PDF";
  }
  else if(complabel.indexOf('.rar')!= -1){
    complabel="RAR";
  }
  stringx+="<div data-order='"+item.sequence+"' id='"+item.subTask_ID+"' class='col-lg-12 evenadding distance'>";
  stringx+="<div class='row'>";

  stringx+="<div class='col-12 text-center'>";
  stringx+="<button href='#' class='btn btn-outline-info btn-lg  m-btn m-btn--icon m-btn--outline-2x'>";
  stringx+="<span>";
  stringx+="<i class='fa fa-file-text-o'></i>";
  stringx+="<span>"+complabel+"</span>";
  stringx+="</span>";
  stringx+="</button>";
  stringx+="<br><div style='font-size:1.1em;color:#7e61b1'>"+complabel_name+"</div>";
  stringx+="<button class='btn btn-secondary btn-sm m-btn  m-btn m-btn--icon m-btn--pill' disabled>";
  stringx+="<span><i class='fa fa-download'></i><span>Download this file</span></span>";
  stringx+="</button>";
  if(item.sublabel){
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'>"+item.sublabel+"</div>";
  }else{
    stringx+="<div data-id='"+item.subTask_ID+"' id='sublabel"+item.subTask_ID+"' onkeyup='autosavesublabel(this)' contenteditable='true' class='formdistance editable' style='font-size:0.9em;color:#6a6c71'> Type a description...</div>";
  }
  stringx+="</div>";
  stringx+="<div id='"+item.subTask_ID+"' class='input-group-append formdistance' style='padding-left:15px;'>";
  stringx+="<button type='button' data-id='' onclick='' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-download'></i></button>";
  stringx+="<button type='button' data-compid='"+taskid+"' data-id='"+item.subTask_ID+"' onclick='delete_component(this)' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'>";
  stringx+="<i class='fa fa-close'></i>";
  stringx+="</div>";
  stringx+="</div>";
  stringx+="</div>";
}
});
$("#add_content-modal").modal('hide');
$("#formtag"+taskid).html(stringx);

$("#formtag"+taskid).sortable({
  cancel: '.ui-state-default .editable',
  mouseButton: 3,
  stop: function( event, ui ) {
    reorder_update_subtask(ui);
  }
});
// $("#formtag"+taskid).disableSelection();
$('.ui-sortable').data('taskid', taskid);
}
});
}
//Properties fetch input and textarea
function fetch_inputtypeformdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      var data = res;
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#labelform").val(data.complabel);
      $("#sublabelform").val(data.sublabel);
      $("#placeholderform").val(data.placeholder);
      $("#textmin").val(data.min);
      $("#textmax").val(data.max);
      $("#btn-formdetailssave").data('id',data.task_ID);
    }
  });
}
//Properties fetch text and heading
function fetch_headertextdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      var data = res;
      $("#ht_labelform").val(data.complabel);
      if(compid==3){
        $("#ht_sublabelform").val(data.sublabel);
      }else if(compid==4){
        $("#sublab").hide();
      }
      $("#textalignment").val(data.text_alignment).prop('selected', true);
      $("#textsize").val(data.text_size).prop('selected', true);
      $("#btn-htdetailsave").data('id',data.task_ID);
    }
  });
}
//Properties fetch radio button, checkbox and dropdown
function fetch_optionlistdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#optionlabelform").val(data.complabel);
      $("#optionsublabelform").val(data.sublabel);   
      $("#btn-oldetailsave").data('id',data.task_ID);
    }
  });
}
//Properties fetch date and time
function fetch_datetimeformdetails(element){
  var subtid=$(element).data('id');
  var compid=$(element).data('component');
  $("#subtaskformid").val(subtid);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_formdetails",
    data: {
      subTask_ID: subtid
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      var data = res;
      console.log(data.required);
      if(data.required=="yes"){
        $(".requirecheckbox").prop("checked",true);
      }
      else{
        $(".requirecheckbox").prop("checked",false);
      }
      $("#dtlabelform").val(data.complabel);
      $("#dtsublabelform").val(data.sublabel);
      $("#dtplaceholderform").val(data.placeholder);
      $("#btn-formdtdetailssave").data('id',data.task_ID);
    }
  });
}

function delete_component(element){
  var subtaskid=$(element).data('id');
  var task_id=$(element).data('compid');
  swal({
    title: 'Are you sure you want to delete this form element?',
    text: "",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url:"<?php echo base_url(); ?>process/delete_component",
        data: {
          subTask_ID: subtaskid,
          task_id: task_id,
        },
        cache: false,
        success: function () {
         display_allcomponents();
         swal(
          'Deleted!',
          'Successfully deleted.',
          'success'
          )
       },
       error: function (res){
        swal(
          'Oops!',
          'Please Check!',
          'error'
          )
      }
    });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deletion has been cancelled',
        'error'
        )
    }
  });
}
//RADIO, MULTIPLE, DROPDOWN
function radio_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-radio m-radio--bold m-radio--state-primary editable' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Double click and type option...";
   }
   stringcomp+="<input type='radio' name='formrad' value='' disabled>";
   stringcomp+="<span></span>";
   stringcomp+="</label>";
   stringcomp+="</div>";
   stringcomp+="<div class='col-lg-2'>";
   stringcomp+="<a href='#' data-componentid='6' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)' ><i class='fa fa-remove'></i></a>";
   stringcomp+="</div>";
 });
   $("#radiolist"+subtaskid).html(stringcomp);
 }
});
}
function checkbox_option(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<div class='col-lg-10'>";
    stringcomp+="<label id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' class='m-checkbox m-checkbox--bold m-checkbox--state-brand editable' contenteditable='true'>";
    stringcomp+="<input type='checkbox' disabled>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
    stringcomp+="Type Checklist Label...";
  }
  stringcomp+="<span></span>";
  stringcomp+="</label>";
  stringcomp+="</div>";
  stringcomp+="<div class='col-lg-2'>";
  stringcomp+="<a href='#' data-componentid='8' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='fa fa-remove'></i></a>";
  stringcomp+="</div>";
});
   $("#checkoption"+subtaskid).html(stringcomp);
 }
});
}
function dropdown_list(subtaskid){
  var stringcomp="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_componentsubtask",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   $.each(result, function (key, item) {
    stringcomp+="<li class='list-group-item justify-content-between'>";
    stringcomp+="<span class='editable' id='optiondetails"+item.componentSubtask_ID+"' data-id='"+item.componentSubtask_ID+"' onkeyup='autosaveoptionlabel(this)' contenteditable='true'>";
    if(item.compcontent!=null){
     stringcomp+=item.compcontent;
   }else{
     stringcomp+="Item list...";
   }
   stringcomp+="</span>";
   stringcomp+="<a href='#' data-componentid='7' data-subtaskid='"+subtaskid+"' data-id='"+item.componentSubtask_ID+"' onclick='delete_option(this)'><i class='float-right fa fa-remove'></i></a>";
   stringcomp+="</li>";
 });
   $("#itemlist"+subtaskid).html(stringcomp);
 }
});
}
function countallforms(taskid){
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/fetch_allsubtask_count",
  data: {
    task_ID:taskid
  },
  cache: false,
  success: function (res) {
   var result = JSON.parse(res.trim());
   // var countform=Object.keys(result).length;
   
   var countform= (result[0].max!=null) ? parseInt(result[0].max) + 1 : 1;
   // alert(countform);
   $("#sequenceid").val(countform);
 }
});
}

function add_component(componentType){
  var taskid=$("#taskformid").val();
  countallforms(taskid);
  var sequencenum=$("#sequenceid").val();
  var seqnum=0;
  if(sequencenum==0){
    seqnum=1;
  }else{
    seqnum=sequencenum;
    // seqnum++;
  }
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/add_subtask",
   data: {
    component_ID: componentType,
    task_ID: taskid,
    sequence: seqnum
  },
  cache: false,
  success: function (res) {
    $("#add_content-modal").modal('hide');
    display_allcomponents();
  }
});
}

function add_componentoption(element){
  var subtaskid=$(element).data('id');
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/add_subtaskcomponent",
   data: {
    subTask_ID: subtaskid,
  },
  cache: false,
  success: function (res) {
   var res = JSON.parse(res.trim());
   var data = res;
   var component=data.component_ID;
   console.log('success');
   console.log(component);
   if(component==6){
    radio_option(subtaskid);
  }
  else if(component==7){
    dropdown_list(subtaskid);
  }
  else if(component==8){
    checkbox_option(subtaskid);
  }
}
});
}
function delete_option(element){
  var compsubtaskid=$(element).data('id');
  var subtask=$(element).data('subtaskid');
  var componentid=$(element).data('componentid');
  $.ajax({
    type: "POST",
    url:"<?php echo base_url(); ?>process/delete_option",
    data: {
      componentSubtask_ID: compsubtaskid,
    },
    cache: false,
    success: function () {
      if(componentid=='6'){
        radio_option(subtask);
      }
      else if(componentid=='7'){
        dropdown_list(subtask);
      }
      else if(componentid=='8'){
        checkbox_option(subtask);
      }
    }
  });
}

function displayformdiv(element){
  var taskid=$(element).data('id');
  console.log(taskid);
  $('#contentdiv').data('activetaskid', taskid);
  display_allcomponents();
  displayduedate(taskid);
  $(".formcontent").hide();
  $("#formcontent"+taskid).show();
  $(".inputtask").css({"background-color":"","color":""});
  $(".inputtask").removeClass("placeholdercolor");
  $("input#"+taskid).css({"background-color":"#51aff9","color":"white"});
  $("input#"+taskid).addClass("placeholdercolor");

}
function displayduedate(taskid){
  var tid=taskid;
  var dd="";
  $.ajax({
   type: "POST",
   url: "<?php echo base_url(); ?>process/fetch_duedate",
   data: {
    task_ID: tid,
    check_ID: checkid,
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    var data = res;
    if(data.taskdueDate!=null){
      var dateString=data.taskdueDate;
      var dateObj = new Date(dateString);
      var momentObj = moment(dateObj);
      var momentString = momentObj.format('llll');
      dd+="<div class='row taskduedate' style='padding:15px;'>";
      dd+="<label style='font-size:0.9em'> <i class='fa fa-calendar-check-o'></i><strong style='color:#34bfa3;'> Task Due Date:</strong> "+momentString+"</label>";
      dd+="</div>";
      $(".taskduedate").remove();
      $("#rowdisplay"+tid).after(dd);
    }
  }
});
}
function savetaskduedate(element){
 var taskid=$(element).data('id');
 var dueDate=$("#taskduedate"+taskid).val();
 var pid=$("#hiddentaskID").val();
 if ($.trim($("#taskduedate"+taskid).val()) === "") {
 }else {
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_taskDuedate",
    data: {
      checklist_ID: checkid,
      task_ID: taskid,
      taskdueDate:dueDate
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'Due Date Set and Updated!',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.href = "<?php echo base_url('process/checklist_update')?>"+"/"+checkid+"/"+pro_id;
    },
    error: function (res){
      console.log(res);
      doNotif2();
    }
  });
 }
}
function saveduedate(element){
  var dueDate=$("#datetime").val();
  var checkID=$(element).data('id');
  var pid=$("#hiddentaskID").val();
  if ($.trim($("#datetime").val()) === "") {
    e.preventDefault();
  }else{
   $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/update_checklistDuedate",
    data: {
      checklist_ID: checkID,
      dueDate:dueDate
    },
    cache: false,
    success: function (res) {
      swal({
        position: 'center',
        type: 'success',
        title: 'Due Date Set and Updated!',
        showConfirmButton: false,
        timer: 1500
      });
      window.location.href = "<?php echo base_url('process/checklist_update')?>"+"/"+checkID+"/"+pid;
    },
    error: function (res){
      console.log(res);
      doNotif2();
    }
  });
 }
}
/* */
var timer=null;
function doNotif() {
 $.notify("Auto-save!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}
function doNotif2() {
 $.notify("Save!");
 setTimeout(function() {
  $.notifyClose('top-right');
}, 1000);
}

//autosave function
function autosaveoptionlabel(element){
  var compsubtaskid=$(element).data('id');
  var optionlabel=$("#optiondetails"+compsubtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_optiondetails",
    data: {
      componentSubtask_ID: compsubtaskid,
      compcontent: optionlabel
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavesublabel(element){
  console.log(element);
  var subtaskid=$(element).data('id');
  var sublabel=$("#sublabel"+subtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_sublabeldetails",
    data: {
      subTask_ID: subtaskid,
      sublabel: sublabel
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavelabel(element){
  var subtaskid=$(element).data('id');
  var label=$("#label"+subtaskid).text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_subtaskdetails",
    data: {
      subTask_ID: subtaskid,
      complabel: label
    },
    cache: false,
    success: function (res) {
      res = JSON.parse(res.trim());
      console.log(res);
      clearTimeout(timer); 
      timer = setTimeout(doNotif, 1000)
    },
    error: function (res){
      console.log(res);
    }
  });
}
function autosavetask(element){
 var asid=$(element).data('id');
 console.log(asid);
 var taskTitle = $("input#"+asid).val();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_taskdetails",
  data: {
    task_ID: asid,
    taskTitle:taskTitle
  },
  cache: false,
  success: function (res) {
    res = JSON.parse(res.trim());
    console.log(res);
    clearTimeout(timer); 
    timer = setTimeout(doNotif, 1000)
  },
  error: function (res){
    console.log(res);
  }
});
}
function addtask(element){
  var processID=$(element).data('id');
  var checkID = $("#checkID").val();
  console.log(processID);
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/add_onetaskStatus",
    data: {
      process_ID: $(element).data('id'),
      checklist_ID: checkID
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      init_task();
      // window.location.href = "<?php echo base_url('process/checklist_update')?>"+"/"+checkID+"/"+processID;
    },
    error: function (res){
      console.log(res);
    }
  });
}
function deleteTask(element){
  swal({
    title: 'Are you sure?',
    text: "All under this task will be deleted, too.",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!'
  }).then(function(result) {
    if (result.value) {

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>process/delete_task_update",
        data: {
          task_ID: $(element).data('id')
        },
        cache: false,
        success: function () {
          init_task();
          swal(
            'Deleted!',
            'Task successfully deleted.',
            'success'
            )
        },
        error: function (res){
          swal(
            'Oops!',
            'You cannot delete this task ',
            'error'
            )
        }
      });
    }else if (result.dismiss === 'cancel') {
      swal(
        'Cancelled',
        'Deleting a process has been cancelled',
        'error'
        )
    }
  });
}
//ASSIGNING TASK
function fetch_IDforAssignment(element){
  var idforassignment=$(element).data('id');
  var typename=$(element).data('type');
  var tasktitle=$(element).data('tasktitle');
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/get_assigningDuedate",
    data: {
      task_ID: idforassignment,
      checklist_ID: checkid
    },
    cache: false,
    success: function (res) {
      var result = JSON.parse(res.trim());
      var checklistStatusid= result.checklistStatus_ID;
      $("#folprotaskcheck_id").val(checklistStatusid);
      $("#assign_type").val(typename);
      $("#fpc_title").text(tasktitle);
      // display_addedmembers();
      reinitAssigneeDatatable();
    }
  });
}

// function display_addedmembers(){
//  // var idforassignment=$(element).data('id');
//  var typename=$("#assign_type").val();
//  var folprotaskcheckID=$("#folprotaskcheck_id").val();
//  var stringx="";

//  console.log("checklist_ID:"+folprotaskcheckID);
//  console.log("type"+typename);

//  $.ajax({
//   type: "POST",
//   url: "<?php echo base_url(); ?>process/display_addedmembers",
//   data: {
//     folProCheTask_ID:folprotaskcheckID,
//     type:typename
//   },
//   beforeSend:function(){
//     $('.spinner-icon').show();
//   },
//   cache: false,
//   success: function (res) {

// 	if(res.length>4){    
//     $('.spinner-icon').hide();
//     var result = JSON.parse(res.trim());
//     $.each(result, function (key, item) {
//       var assignID=item.assign_ID;
//       var memberid=item.assignedTo;
//       var assignmentRule=item.assignmentRule_ID;
//       $.ajax({
//         type: "POST",
//         url: "<?php echo base_url(); ?>process/fetch_membersadded",
//         data: {
//           uid: item.assignedTo,
//           fptc_id: folprotaskcheckID,
//           type: typename
//         },
//         cache: false,
//         success: function (res) {
//           var re = JSON.parse(res.trim());

//            $.each(re, function (key, data) {
//             stringx +="<tr>";
//             stringx +="<td>" +data.fname+" "+data.lname+"</td>";
//             stringx +="<td>" +data.acc_name+"</td>";
//             stringx +="<td><select data-id='"+assignID+"' onchange='access_permissionChange(this)' id='assRule"+assignID+"' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
//             if(data.assignmentRule_ID=="5"){
//               stringx +="<option value='"+data.assignmentRule_ID+"' selected>Can Answer</option>";
//               stringx +="<option value='6'>Can Edit and Answer</option>";
//             }else if(data.assignmentRule_ID=="6"){
//               stringx +="<option value='"+data.assignmentRule_ID+"'>Can Edit and Answer</option>";
//               stringx +="<option value='5'>Can Answer</option>";
//             }
//             stringx +="</optgroup></select></td>";
//             stringx +="<td><button onclick='removemember_assignment(this)' data-toggle='m-tooltip' data-id='"+assignID+"' title='Remove this member' data-original-title='Tooltip title' class='btn btn-danger m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-remove'></i></button></td>";
//             stringx +="</tr>";
//           }); 

//           $("#mymember_table").html(stringx);
//           $("#assRule"+assignID).val(assignmentRule);
//         }
//       });
//     });
//   }else{
// 	$("#mymember_table").html(stringx);  
//   }
//   }
// });
// }

function getaccmembers(){
  var accountid=$("#account").val();
  var assigntype=$("#assign_type").val();
  var folprotaskcheckID=$("#folprotaskcheck_id").val();
  var accountname=$("#account :selected").text();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>process/fetch_allemployeeact",
    data: {
      acc_id: accountid
    },
    // beforeSend:function(){
      //   $('.spinner-icon').show();
      // },
      cache: false,
      success: function (res) {
        $('.spinner-icon').hide();
        var result= JSON.parse(res.trim());
        var stringx="";
        $.each(result, function (key, item) {
          var applicantid=item.apid;
          var userid=item.uid;
          $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>process/fetch_empdetails",
            data: {
              apid: applicantid,
              fptcID: folprotaskcheckID,
              assign_type: assigntype,
              user_ID: userid
            },
            cache: false,
            success: function (res) {
             var rr= JSON.parse(res.trim());
             console.log("READ RESULT:"+res);
             $.each(rr, function (key, data) {
              stringx +="<tr>";
              stringx +="<td>" +data.fname+" "+data.lname+"</td>";
              stringx +="<td>" +accountname+"</td>";
              stringx +="<td><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
              stringx +="<option value='5'>Can Answer</option>"; 
              stringx +="<option value='6'>Can Edit and Answer</option>";       
              stringx +="</optgroup></select></td>";
              stringx +="<td><button onclick='addmember_assignment(this)' data-reload='1' data-toggle='m-tooltip' data-id='"+data.apid+"' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
              stringx +="</tr>";
            });    
             $("#memberdetails tbody").html(stringx);
           }
         });
        });
      }
    });
}
function getmember_name(){
 var empid=$("#member").val();
 var empname=$("#member :selected").text();
 var assigntype=$("#assign_type").val();
 var folprotaskcheckID=$("#folprotaskcheck_id").val();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/fetch_oneemployee",  
  data: {
    emp_id: empid,
    fptcID: folprotaskcheckID,
    assign_type: assigntype
  },
  cache: false,
  success: function (res) {
    var stringx="";
    var stringxx="";
    var result= JSON.parse(res.trim());
    $.each(result, function (key, data) {
      console.log("USSRE:"+data.uid);
      stringx +="<tr>";
      stringx +="<input type='hidden' id='' name='' value=''>";
      stringx +="<td>" +data.fname+" "+data.lname+"</td>";
      stringx +="<td>" +data.acc_name+"</td>";
      stringx +="<td class='sel'><select id='assignmentRule' class='dropdownwidth custom-select form-control' data-live-search='true'><optgroup data-max-options='1'>";
      stringx +="<option value='5'>Can Answer</option>"; 
      stringx +="<option value='6'>Can Edit and Answer</option>";      
      stringx +="</optgroup></select></td>";

      //  $.ajax({
       //    type: "POST",
       //    url: "<?php echo base_url(); ?>process/fetch_filterassignment",  
       //    data: {
         //      fptcID: folprotaskcheckID,
         //      assign_type: assigntype
         //    },
         //    cache: false,
         //    success: function (res) {
           //     var rlt= JSON.parse(res.trim());
           //     $.each(rlt, function (key, ass) {
             //      console.log('RESSSULT:'+ass.assignedTo);
             //      if(ass.assignedTo==data.uid){
               //        $(".condition").val('1');
               //      }else{
                 //        $(".condition").val('2');
                 //      }
                 //    });
           //   }
           // });

           stringx +="<td><button onclick='addmember_assignment(this)' data-reload='2' data-toggle='m-tooltip' data-id='"+data.apid+"' title='Add as a member' data-original-title='Tooltip title' class='btn btn-success m-btn m-btn--icon m-btn--icon-only'><i class='fa fa-plus'></i></button></td>";
           stringx +="</tr>";
         });    
    $("#memberdetails tbody").html(stringx);
  }
});
}
function addmember_assignment(element){
 var apid=$(element).data('id');
 var fptc_id=$("#folprotaskcheck_id").val();
 var assigntype=$("#assign_type").val();
 var assignrule=$("#assignmentRule :selected").val();
 var fpc_title=$("#fpc_title").text();

 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/add_assignment",
  data: {
    type: assigntype,
    assignmentRule_ID: assignrule,
    applicantid: apid,
    folprotaskcheck_id: fptc_id,
    title:fpc_title,
    process_ID:pro_id,
    checklist_ID:checkid
  },
  cache: false,
  success: function (res) {
    var resObj = $.parseJSON(res.trim())
    console.log(resObj.notifid);
    systemNotification(resObj.notifid.notif_id);
    swal({
      position: 'center',
      type: 'success',
      title: 'Successfully Assigned a member',
      showConfirmButton: false,
      timer: 1500
    });
    $("#assign_membersmodal").modal('hide');
  }
});
}
function removemember_assignment(element){
 swal({
  title: 'Are you sure?',
  text: "This member will be removed from accessing this file.",
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Yes, delete it!'
}).then(function(result) {
  if (result.value) {
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>process/delete_assignment",
      data: {
       assign_ID:$(element).data('id')
     },
     cache: false,
     success: function () {
       reinitAssigneeDatatable();
       swal(
        'Deleted!',
        'Member successfully Removed.',
        'success'
        )
     },
     error: function (res){
      swal(
        'Oops!',
        'Something is wrong with your code!',
        'error'
        )
    }
  });
  }else if (result.dismiss === 'cancel') {
    swal(
      'Cancelled',
      'Removing member has been cancelled',
      'error'
      )
  }
});
}
function access_permissionChange(element){
 var assignmentid=$(element).data('assignid');
 var changepermission=$(element).val();
 $.ajax({
  type: "POST",
  url: "<?php echo base_url(); ?>process/update_assignmentpermission",
  data: {
    assid:assignmentid,
    changepermission:changepermission
  },
  cache: false,
  success: function (res) {
    reinitAssigneeDatatable();
    swal({
      position: 'center',
      type: 'success',
      title: 'Successfully Changed Acces Permission  ',
      showConfirmButton: false,
      timer: 1500
    });
    var result = JSON.parse(res.trim());
  }
});
}

function resetform(){
  $("#account").prop("selectedIndex", 0).change();
  $("#member").prop("selectedIndex", 0).change();
  $("#memberdetails tbody").html("");
  $("#added_members tbody").html("");
}

function update_subtask_order(prevVal, currentVal, nextVal, subtaskIdVal){
  var prev = prevVal;
  var next = nextVal;
  if(prevVal == undefined){
    prev = 0;
  }
  if(nextVal == undefined){
    next = 0
  }
  $.when(fetchPostData({
    taskId: $('#contentdiv').data('activetaskid'),
    subtask_id: subtaskIdVal,
    prevOrder: prev,
    currentOrder: currentVal,
    nextOrder: next
  }, '/process/update_subtask_sequence')).then(function (order) {
    var orderObj = $.parseJSON(order.trim());
    console.log(orderObj);
  })
}

function reorder_update_subtask(ui){
  var subtask_id =  $(ui.item).attr('id');
  var current =  $(ui.item).data('order');
  var prev =  $($(ui.item).prev()).data('order');
  var next = $($(ui.item).next()).data('order');
  var subtasksDiv = $('.ui-sortable-handle');

  update_subtask_order(prev, current, next, subtask_id); 
  
  if(prev == undefined && next != undefined){
    // increment all orders less than to current
    // move as first order data
    console.log("move as first order data");
    $.each(subtasksDiv, function(index, value){
      if(($(value).data('order') < current) && ($(value).data('order') >= 1)){
        $(value).data('order', $(value).data('order')+1);
        console.log($(value).data('order'));
      }
    })
    $(ui.item).data('order', 1);
    console.log("NEW ORDERING");
    console.log($($(ui.item).prev()).data('order'));
    console.log($(ui.item).data('order'));
    console.log($($(ui.item).next()).data('order'));
  }else if(prev == undefined && next == undefined){
    // no changes
  }else if(prev != undefined && next == undefined)
  {
    // decrement order greater than from current and less than of equal to prev
    // change order to prev plus 
    $.each(subtasksDiv, function(index, value){
      if(($(value).data('order') > current) && ($(value).data('order') <= prev)){
        $(value).data('order', $(value).data('order')-1);
        console.log($(value).data('order'));
      }
    })
    $(ui.item).data('order', prev);
    console.log("NEW ORDERING");
    console.log($($(ui.item).prev()).data('order'));
    console.log($(ui.item).data('order'));
    console.log($($(ui.item).next()).data('order'));
  }
  else
  {
    if((prev <current) && (next < current)){

      console.log("change to next");
      // increment next orders and less than to current
      $.each(subtasksDiv, function(index, value){
        if(($(value).data('order') >=next) && ($(value).data('order') < current)){
          $(value).data('order', $(value).data('order')+1);
          console.log($(value).data('order'));
        }
      });
      $(ui.item).data('order', next);
      console.log("NEW ORDERING");
      console.log($($(ui.item).prev()).data('order'));
      console.log($(ui.item).data('order'));
      console.log($($(ui.item).next()).data('order'));
    }else if((prev > current) && (next > current)){
      console("change to previous");
      // change to prev
      // decrement prev orders and greater than to current
      $.each(subtasksDiv, function(index, value){
        if(($(value).data('order') > current) && ($(value).data('order') <= prev)){
          $(value).data('order', $(value).data('order')-1);
          console.log($(value).data('order'));
        }
      });
      $(ui.item).data('order', prev);
      console.log("NEW ORDERING");
      console.log($($(ui.item).prev()).data('order'));
      console.log($(ui.item).data('order'));
      console.log($($(ui.item).next()).data('order'));
    }
  }
  console.log("subtask id: "+subtask_id);
  console.log("PREV: "+prev);
  console.log("current: "+current);
  console.log("nex: "+next);
  console.log("reorder");
}

//DATATABLE
function reinitAssigneeDatatable(){    
  $('#assigned_memberdisplay').mDatatable('destroy');
  assigneeDatatable.init($('#assigneeSearch').val(), $("#folprotaskcheck_id").val(),$('#assign_membersmodal').data('type'));
}
$('#assigneeSearch').on('keyup', function(){
  reinitAssigneeDatatable();
})
function initAssignee(element){
  console.log("init assignee");
  $('#assigned_memberdisplay').mDatatable('destroy');
  $('#assign_membersmodal').data('type', $(element).data('type'));
  assigneeDatatable.init($('#assigneeSearch').val(), $("#folprotaskcheck_id").val(),$(element).data('type'));
}
function selectChange(element){
  console.log($(element).val())
  console.log( $(element).data('assignid'));
}
var assigneeDatatable = function () {
  var assignee = function (searchVal, folProcIdVal,typeval) {
    var options = {
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'POST',
            url: baseUrl + "/process/list_assignee_datatable",
            params: {
              query: {
                folProcId: folProcIdVal,
                type:typeval,
                assigneeSearch: searchVal
              },
            },
          }
        },
        saveState: {
          cookie: false,
          webstorage: false
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
      },
      layout: {
theme: 'default', // datatable theme
class: '', // custom wrapper class
scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
height: 550, // datatable's body's fixed height
footer: false // display/hide footer
},
sortable: true,
pagination: true,
toolbar: {
// toolbar placement can be at top or bottom or both top and bottom repeated
placement: ['bottom'],

// toolbar items
items: {
// pagination
pagination: {
// page size select
pageSizeSelect: [5, 10, 20, 30, 50] // display dropdown to select pagination size. -1 is used for "ALl" option
},
}
},
search: {
  input: $('#assigneeSearch'),
},
rows: {
  afterTemplate: function (row, data, index) {},
},
// columns definition
columns: [{
  field: "fname",
  title: "Employee Name",
  width: 180,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = row.fname + " " + row.lname;
    return html;
  },
},{
  field: "acc_name",
  title: "Team",
  width: 160,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = row.acc_name;
    return html;
  },
},{
  field: "lname",
  title: "Access",
  width: 220,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var rule1 = '';
    var rule2 = '';
    var rule3 = '';
    var rule4 = '';
    var rule5 = '';
    var rule6 = '';
    var rule7 = '';
    var rule8 = '';
    if(row.assignmentRule_ID == 1){
      rule1 = "selected";
    }else if(row.assignmentRule_ID == 2){
      rule2 = "selected";
    }else if(row.assignmentRule_ID == 3){
      rule3 = "selected";
    }else if(row.assignmentRule_ID == 4){
      rule4 = "selected";
    }
    else if(row.assignmentRule_ID == 5){
      rule5 = "selected";
    }
    else if(row.assignmentRule_ID == 6){
      rule6 = "selected";
    }
    else if(row.assignmentRule_ID == 7){
      rule7 = "selected";
    }
    else if(row.assignmentRule_ID == 8){
      rule8 = "selected";
    }
    var html='';
    if(row.type=="folder" || row.type=="process"){
     html ='<select onchange="access_permissionChange(this)" data-assignid="'+row.assign_ID+'" class="form-control m-input m-input--square">'+
     '<option value="1" '+rule1+'>Can Edit, View and Run</option>'+
     '<option value="2" '+rule2+'>Can Edit, View Own and Run</option>'+
     '<option value="3" '+rule3+'>Can View and Run</option>'+
     '<option value="4" '+rule4+'>Can View Own and Run</option>'+
     '<option value="7" '+rule7+'>Can View Own</option>'+
     '<option value="8" '+rule8+'>Can View All</option>'+
     '</select>';
   }else if(row.type=="checklist" || row.type=="task"){
     html ='<select onchange="access_permissionChange(this)" data-assignid="'+row.assign_ID+'" class="form-control m-input m-input--square">'+
     '<option value="5" '+rule5+'>Can Answer</option>'+
     '<option value="6" '+rule6+'>Can Edit and Answer</option>'+
     '</select>';
   }
   return html;
 },
},{
  field: "assign_ID",
  title: "Action",
  width: 90,
  selector: false,
  // sortable: 'asc',
  textAlign: 'left',
  template: function (row, index, datatable) {
    var html = '<button onclick="removemember_assignment(this)" data-toggle="m-tooltip" data-id="'+row.assign_ID+'" title="Remove this member" data-original-title="Tooltip title" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"><i class="fa fa-remove"></i></button>';
    return html;
  },
}],
};
var datatable = $('#assigned_memberdisplay').mDatatable(options);
};
return {
  init: function (searchVal, folProcIdVal,typeval) {
    assignee(searchVal, folProcIdVal,typeval);
  }
};
}();
</script>



