<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Anil Labs - Codeigniter mail templates</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
	<style>

	</style>
	<div>
		<div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 32px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header">
			<img style="Margin-left: auto;Margin-right: auto;" src="https://lh3.googleusercontent.com/NZNZzuQogtKpP32IPLfU7ycNwp-VO_a3pqnAB4Owr0sRkJdDEHENGxRulIG2eyEtLcWQdVR5NZOsStRx4zhQePBcFviP2V_F=w1366-h768-rw-no"></div>

			<table border=0>
				<tr>
					<td style="text-align:center">
						<?php if(isset($img)){echo $img;} ?><br/>
						<a href="<?php echo base_url();?>" style="-moz-box-shadow:inset 0px 1px 0px 0px #fff6af;
						-webkit-box-shadow:inset 0px 1px 0px 0px #fff6af;
						box-shadow:inset 0px 1px 0px 0px #fff6af;
						background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffec64), color-stop(1, #ffab23));
						background:-moz-linear-gradient(top, #ffec64 5%, #ffab23 100%);
						background:-webkit-linear-gradient(top, #ffec64 5%, #ffab23 100%);
						background:-o-linear-gradient(top, #ffec64 5%, #ffab23 100%);
						background:-ms-linear-gradient(top, #ffec64 5%, #ffab23 100%);
						background:linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
						filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffec64', endColorstr='#ffab23',GradientType=0);
						background-color:#ffec64;
						-moz-border-radius:6px;
						-webkit-border-radius:6px;
						border-radius:6px;
						border:1px solid #ffaa22;
						display:inline-block;
						cursor:pointer;
						color:#333333;
						font-family:Arial;
						font-size:15px;
						font-weight:bold;
						padding:6px 24px;
						text-decoration:none;
						text-shadow:0px 1px 0px #ffee66;">Reply</a>
					</td>
					<td>
						<blockquote style='min-height:100px;background: #f9f9f9;border-left: 10px solid #ccc;margin: 1.5em 10px;padding: 0.5em 10px;quotes: "\201C""\201D""\2018""\2019";'>
							<p style='display:inline;vertical-align: middle'><?php echo $message; ?></p>
						</blockquote>
					</td>
				</tr>
			</table>
			
			
		</div>

	</body>
	</html> 